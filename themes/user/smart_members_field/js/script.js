$(document).ready(function() {
	if (typeof Relationship !== 'undefined') {
    	Relationship.renderFields();
	}
}),
	Grid.bind("smart_members_field", "display", function(e) {
	    Relationship.renderFields(e);
	}),
	FluidField.on("smart_members_field", "add", function(e) {
	    Relationship.renderFields(e)
});