(function($) {
	function convertToSlug(Text,lowerCase)
	{
		var ret = Text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'_');
		if(lowerCase === false) ret = ret.toUpperCase();

		return ret;
	}

	function iniShortnameSlug() {
		$('.js-shortname').each(function() {
			var $field = $(this);
			var $shortfield = $('#'+$field.data('shortname'));

			$field.on('input', function() {
				if($shortfield.data('raw') === '') $shortfield.val( convertToSlug($field.val()));
			});
		});
	}

	function iniCodeSlug() {
		$('.js-code').each(function() {
			var $field = $(this);
			var $codefield = $('#'+$field.data('code'));

			$field.on('input', function() {
				if($codefield.data('raw') === '') $codefield.val( convertToSlug($field.val(), false));
			});
		});
	}

	function iniDeleteControl() {
		$('.js-delete').each(function() {
			var $link = $(this);

			$link.on('click', function(e) {
				text = 'Are you sure you want to delete this '+$link.data('type')+'?';

				if($link.data('type') == 'Subscription') {
					text = text + ' Any members on this subscription will stay in their current groups, but callbacks from Stripe will be ignored';
				}

				if(!confirm(text)) {
					e.preventDefault();
				}
			});
		});
	}

	/**
	 * Initialises the special form interactions
	 */
	function iniFormExtras() {
		$('.js-enabler').each(function() {
			var $check = $(this);
			var $tr, rel;
			var inactive_class = 'inactive';
			var mode = 'toggle';

			if($check.hasClass('js-hide')) inactive_class = 'hidden';
			if($check.hasClass('js-select')) mode = 'match';

			if($check.hasClass('initialized')) {
				return;
			}

			$check.addClass('initialized');

			rel = $check.attr('rel');
			$tr = $('.'+rel);

			$check.on('change',function() {
				$tr.each(function(){
					var isDisabled = $(this).hasClass(inactive_class);

					if(mode == 'toggle')
					{
						if(isDisabled) {
							// Enable
							$(this).removeClass(inactive_class);
							$(this).find('input').removeAttr('disabled');
							$(this).find('select').removeAttr('disabled');
						} else {
							// Disable
							$(this).addClass(inactive_class);

							$(this).find('input').attr('disabled', 'disabled');
							$(this).find('select').attr('disabled', 'disabled');
						}
					} else {
						// Check the actual value
						if($check.val() == $(this).attr('rel')) {
							$(this).removeClass(inactive_class);
							$(this).find('input').removeAttr('disabled');
							$(this).find('select').removeAttr('disabled');
						} else {
							// Disable
							$(this).addClass(inactive_class);

							$(this).find('input').attr('disabled', 'disabled');
							$(this).find('select').attr('disabled', 'disabled');
						}
					}
				});
			});
		});
	}

	/**
	 * Initialises the 'extended data'.
	 */
	function iniExtendedData() {
		$('.extended_data').each(function() {
			var logId = $(this).data('logid');

			/*
			 * Rather rudimentary way of ensuring that the extended data doesn't get
			 * initialised twice on the main log page, when the accessory is also
			 * present. You'd think EE would be smart enough to not include the same
			 * JS file twice, but apparently not.
			 */

			if ($(this).hasClass('initialized') || $(this).hasClass('empty')) {
				return;
			}

			$(this).addClass('initialized');

			// Create the new HTML elements.
			$(this).prepend('<a href="#" data-logid="'+logId+'" class="extended_data_toggle extended_data_toggle'+logId+'">Show Extended</a>');
		});

		$('.extended_data_toggle').click(function(e) {
			var linkText;
			var linkLogId = $(this).data('logid');
			console.log(linkLogId);

			// Reverse logic because we haven't performed the action yet.
			if($('#extended_data_body'+linkLogId).is(':visible')) {
				linkText = 'Show Extended';
				$(this).closest('.extended_data').find('.extended_data_toggle_bottom').hide();
			} else {
				linkText = 'Hide Extended';
				$(this).closest('.extended_data').find('.extended_data_toggle_bottom').show();
			}

			// Change the expand/collapse text and perform the matching action.
			$('.extended_data_toggle'+linkLogId).text(linkText);
			$('#extended_data_body'+linkLogId).toggle();

			e.preventDefault();
		});
	}

	function testStripeConnection() {
		$('.charge_stripe_test_connection').html('<i class="fa fa-cog fa-spin"></i> Testing...');

		$.post($('.charge_stripe_test_connection').data('testurl'), function(data) {
			var response = JSON.parse(data);

			var tls_status_color = 'green';
			var tls_status_text = 'Compatible';

			// Check whether the TLS test passed or failed.
			if(response.tls_status != '1') {
				tls_status_color = 'red';
				tls_status_text = 'Incompatible!';
			}

			var responseData = '<br /><hr><div id="stripeResponseData"><strong>Time:</strong> '+response.timestamp+'<br />';
			responseData += '<strong>Status:</strong> <span id="stripeResponseStatus">'+response.status+'</span><br />';
			responseData += '<strong>TLS 1.2:</strong> <span style="color:'+tls_status_color+';font-weight:bold;">'+tls_status_text+'</span><br />';
			responseData += '<strong>Charges:</strong> '+response.total_charges+'<br />';
			responseData += '<strong>Coupons:</strong> '+response.total_coupons+'<br />';
			responseData += '<strong>Customers:</strong> '+response.total_customers+'<br />';
			responseData += '<strong>Plans:</strong> '+response.total_plans+'<br />';
			responseData += '<strong>Errors:</strong> '+response.errors+'<br /></div>';

			$('.charge_stripe_test_connection').after(responseData);
			$('.charge_stripe_test_connection').html('Test Connection');
		});
	}

	function deleteTestTransactions() {
		$('.charge_delete_test_transactions').html('<i class="fa fa-cog fa-spin"></i> Deleting...');

		$.post($('.charge_delete_test_transactions').data('acturl'), function(data) {
			var response = JSON.parse(data);

			var status_color = 'green';
			var status_text = 'Test transactions, log entries, member subscriptions, and stripe customer references deleted successfully.';

			// Check whether the TLS test passed or failed.
			if(response.status != '1') {
				status_color = 'red';
				status_text = 'No test transactions to delete.';
			}

			var responseData = '<br /><hr><strong>Status:</strong> <span style="color:'+status_color+';font-weight:bold;">'+status_text+'</span><br />';

			$('.charge_delete_test_transactions').after(responseData);
			$('.charge_delete_test_transactions').html('Delete Test Transactions');
		});
	}

	$('.charge_stripe_test_connection').on('click', function() {
		testStripeConnection();
	});

	$('.charge_delete_test_transactions').on('click', function() {
		if(confirm("Are you sure you want to delete test transactions?")) {
			deleteTestTransactions();
		}
	});

	// Start the ball rolling...
	$(document).ready(function() {
		iniExtendedData();
		iniFormExtras();

		iniShortnameSlug();
		iniCodeSlug();
		iniDeleteControl();
	});

})(jQuery);