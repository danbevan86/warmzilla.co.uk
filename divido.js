myDivido = {};
currentCalculatorBox = "";
productInformation = "";
var wsc = "";

$(document).ready(function() {
	
	if($('.divido_calculator').length > 0)
	{
		wsc = setInterval(startupDivido, 50);
		// startupDivido(1500);
	}

	$(document).on('change', 'input[name="deposit"], select[name="finance"]', function(event) {
		saveProductInformation(currentCalculatorBox, 50);
		productHiddenFields(currentCalculatorBox);
		/*productHiddenFields('product_' + $('#productID').val());*/
	});

	$(document).on('click', '.divido_calculator', function(event) {
		currentCalculatorBox = $(this).attr('id');
		handleCalculatorClick(currentCalculatorBox);
	});

	$(document).on('click', '.view-finance', function(e) {
        e.preventDefault();
        currentCalculatorBox = $(this).data("order-id");
        myDivido[currentCalculatorBox].launchPopup(document.querySelector("#" + $(this).attr('id')), "left");
        handleCalculatorClick(currentCalculatorBox);
    });

});

function productHiddenFields(productID)
{
	/*console.log(myDivido[productID].data.finance.text);*/
	$('#dr_choose_your_plan').val(myDivido[productID].data.finance.text);
    $('#dr_choose_your_deposit').val(myDivido[productID].data.depositPercentage + '%');
    $('#dv_term').val(myDivido[productID].data.agreementDuration + ' months');
    $('#dv_deposit').val(myDivido[productID].data.depositAmount);
    $('#dv_total_payable').val(myDivido[productID].data.totalPayable);
    $('#dv_monthly_instalments').val(myDivido[productID].data.monthlyInstalment);
    $('#dv_cost_of_credit').val(myDivido[productID].data.financeCost);
    $('#dv_total_interest_apr').val(myDivido[productID].data.interestRate);
}

function startupDivido(time)
{

	if(typeof(time) === "undefined")
	{
		time = 0;
	}
	
	if(typeof(Divido) !== "undefined")
	{

		$flag = true;
		$('.divido_calculator').each(function(index, el) {
			if($(this).find('span.duration').length == 0)
			{
				$flag = false;
			}
		});

		if($flag == true)
		{
			clearInterval(wsc);
		}
		else
		{
			productInformation = getCookie('productInformation');
			if(productInformation == "") {
				productInformation = {};
			} else {
				productInformation = JSON.parse(atob(productInformation));
			}

			$('.divido_calculator').each(function(index, el) {
				if($(this).find('span.duration').length == 0)
				{
					_this = $(this);
					myDivido[_this.attr('id')] = {};
					myDivido[_this.attr('id')] = new Divido(document.querySelector("#" + _this.attr('id')));

					spanData = $("#" + _this.attr('id')).children('span.divido-widget-launcher');
					if(typeof(productInformation[_this.attr('id')]) === "undefined")
					{
						productInformation[_this.attr('id')] = {
							installmentAmount : myDivido[_this.attr('id')].data.monthlyInstalment,
							depositAmount : myDivido[_this.attr('id')].data.depositAmount,
							deposit : (typeof(_this.data('divido-deposit-percentage')) !== "undefined" && _this.data('divido-deposit-percentage') != "") ? _this.data('divido-deposit-percentage') : 0,
							finance : (typeof(_this.data('divido-finance')) !== "undefined" && _this.data('divido-finance') != "") ? _this.data('divido-finance') : _this.data('divido-plans').split(",")[_this.data('divido-plans').split(",").length-1],
						};
					}
					else
					{
						myDivido[_this.attr('id')].data.monthlyInstalment = productInformation[_this.attr('id')].installmentAmount;
						myDivido[_this.attr('id')].data.depositPercentage = productInformation[_this.attr('id')].deposit;
						myDivido[_this.attr('id')].setFinance(productInformation[_this.attr('id')].finance);						
						spanData.children('a').text('£' + productInformation[_this.attr('id')]['installmentAmount'] + ' per month');
					}

					/*myDivido[_this.attr('id')].calculate();
					saveProductInformation(_this.attr('id'), 50);*/

					$('.payable_amount_per_month').children('strong').text('£' + productInformation[_this.attr('id')]['installmentAmount']);
					$('.payable_amount_per_month').show();
					spanData.children('a').html(spanData.children('a').html().replace(" per month", "<span class='duration'>per month</span>"));

					_this.addClass('processed');
					_this.show();
					tempArray = _this.attr('id').split('_')
					if($('#product_checkout_' + tempArray[1]).length == 1)
					{
						productHiddenFields(_this.attr('id'));
					}
				}
			});
			setCookie("productInformation", btoa(JSON.stringify(productInformation)), 1);
		}


	}
}

function recalculateBoilerFinanceValue(id, newPrice)
{
	_id = 'product_' + id;
	$("#" + _id).attr('data-divido-amount', newPrice);
	myDivido[_id].data.amount = newPrice;
	myDivido[_id].calculate();
	saveProductInformation(_id, 100);
}

function saveProductInformation(currentCalculatorBox, setTimeoutValue)
{
	console.log(currentCalculatorBox)
	setTimeout(function() {
		productInformation[currentCalculatorBox] = {
			installmentAmount : myDivido[currentCalculatorBox].data.monthlyInstalment,
			depositAmount : myDivido[currentCalculatorBox].data.depositAmount,
			deposit : myDivido[currentCalculatorBox].data.depositPercentage,
			finance : myDivido[currentCalculatorBox].data.finance.id
		};
		setCookie("productInformation", btoa(JSON.stringify(productInformation)), 1);
		$("#" + currentCalculatorBox).children('span').children('a').html('£' + productInformation[currentCalculatorBox]['installmentAmount'] + "<span class='duration'>per month</span>");
		$('.payable_amount_per_month').children('strong').text('£' + productInformation[currentCalculatorBox]['installmentAmount']);
	}, setTimeoutValue);
}

function handleCalculatorClick(currentCalculatorBox)
{
	setTimeout(function() {
		element =  document.querySelector(".divido-widget-wrapper");
		if(element != null)
		{
			infoElements = element.getElementsByClassName('divido-info');

			deposit = productInformation[currentCalculatorBox]['deposit'];
			finance = productInformation[currentCalculatorBox]['finance'];

			myDivido[currentCalculatorBox].setFinance(finance);
			myDivido[currentCalculatorBox].data.depositPercentage = deposit;

			myDivido[currentCalculatorBox].renderChooseFinance(element, infoElements)
			myDivido[currentCalculatorBox].renderChooseDeposit(element, infoElements)
			
			setTimeout(function() {
				myDivido[currentCalculatorBox].recalculate(element, infoElements)
			}, 50);
		}
	}, 10);
}