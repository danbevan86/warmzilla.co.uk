<!doctype html>
<html>
	<head>
		<?=ee()->view->head_title($cp_page_title)?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" lang="en-us" dir="ltr">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"  name="viewport">
		<?=ee()->view->head_link('css/out.min.css'); ?>
	<!--added by Veto--><style>body{background-color:#26394B;background-image:radial-gradient(ellipse at center, #2B465F 0%, #253441 100%),url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwBAMAAAClLOS0AAAAD1BMVEUAAAAkNkwkNkQkOkwkMkQ5cIYCAAAAAXRSTlMAQObYZgAAAp5JREFUOI0dVNkVBCEIswWQBog0INqAaP81bWZ/5lAf5MIm9XZhXSkPPxMyx9Xh2gbuwLMXOJldkVdQfrTBT9mep+IBNZ8gDAZvkQvRy/VpdY1lPF0FtEz4GDymoQDS+agVs5VLjSdT1PSkiOsAy6MhtyHCQ0u+0yYXBNUb9K29sSLE8O7JypwvVptbXWF+PXN8lMTUWaItH+8lJMeWuCqTqKUqm8bFvEd6yAh32Af2EO7Eq2CZTto3Bthuc323E3KQsiSIUr3ifA88lpIhuKerj6jk7pxBzZqoaNejiYpRYO+denDaJEB1IVzywl1UFHpvNS4GlGpxP+OVxh7HezTJHmRIEzRmZ3/CXL61HYwPpVfeOOa+IB7L0YYqiZtRgJE0BbG3YlJE1zpL3IXSC1SP34jS5vxJygbf/oln46zJr4Z3WH3THv750o2hO/I1K4s76j0iztQY5khSb+iW4fgcZhpKjkz9Ntt+c0JBF7BeH07Ps/Su1p/as0PL6UaygXM9Ouggpg8dd9w+qMAxSe0jmStGhNih1hmT190mdfia8/Vl1vVsppNJ7ZTSWtjVS43wxR02kkikspq8K1JBv1XpHsVgJCWq0XcIY0sjg7j9gUcOeVzg46dJUtTK1nwDXXo7rpubLrvrnlTmkcX1Qz9oRtlY+Y2WHeOoVIq2UoIh6/BtSu+ZCM6Jc6I+oATgPXGOLEwlJZEmpLfz1npsxkjKK0Ihc4bvoJgeI4yQEn7enaMFR4pWHApWVDzlo/flCgX6xoZM6bP6RGO8rxDVnfpNYaj7rMPZ/oeeSfRuNyazu0QY5zoZGNF6sHMe53RRy5ivPyZybPZg4UeV3KCTswUM4y3TLk0gw/9VQkRdpe5+90PlughM2cxZPS8n2MV+xXauS1sDmDAAAAAASUVORK5CYII=)}.box{border:0;border-radius:15px;-webkit-box-shadow:0 40px 60px -40px #000;box-shadow:0 40px 60px -40px #000}@media (min-width: 340px){.box{width:300px}}@media (min-width: 450px){.box{width:400px}}h1{border-bottom:0;border-radius:15px 15px 0 0;background:#f1f1f1;padding:20px 20px 12px 20px;font-size:11px;text-transform:uppercase;-webkit-box-shadow:inset 0px 1px 0px #fff;box-shadow:inset 0px 1px 0px #fff}h1 b{font-weight:400}.icon-locked{top:14px;right:20px;font-size:18px}form{border-radius:0 0 15px 15px}fieldset{margin-bottom:20px}fieldset.form-ctrls{border:0;border-radius:0 0 15px 15px;margin:0 -19px -19px;padding:30px;background-color:#e0e0e0}label{font-size:17px;color:#464646;font-weight:700}label a{color:#1f80bd}.options label{font-size:13px}input[type="text"],input[type="password"]{font-size:19px;color:#464646;height:47px;line-height:47px;border:1px solid #dadada;padding-left:13px;padding-right:13px;background:#fff;-webkit-box-shadow:inset 0 6px 10px 0 rgba(0,0,0,0.04);box-shadow:inset 0 6px 10px 0 rgba(0,0,0,0.04)}input[type="submit"].btn{padding:11px 26px;font-size:13px;text-transform:uppercase;-webkit-box-shadow:0 1px rgba(255,255,255,0.5);box-shadow:0 1px rgba(255,255,255,0.5)}.bar{padding-top:30px}.bar p,.bar a{color:#7791a0;font-size:11px}
</style><!--end added by Veto--></head>
	<body>
		<section class="flex-wrap">
			<section class="wrap">

				<?=$child_view?>

				<section class="bar">
					<p class="left"><a href="https://expressionengine.com/" rel="external"><b>ExpressionEngine</b></a></p>
					<p class="right">&copy;<?=ee()->localize->format_date('%Y')?> <a href="https://ellislab.com/" rel="external">EllisLab</a> Corp.</p>
				</section>

			</section>
		</section>
		<?=ee()->view->script_tag('jquery/jquery.js')?>
		<?=ee()->view->script_tag('common.min.js')?>
		<?=ee()->view->script_tag('cp/login.js')?>
		<script type="text/javascript">
			$(document).ready(function()
			{
				document.getElementById('<?=$focus_field?>').focus();
			});
		</script>
	</body>
</html>
