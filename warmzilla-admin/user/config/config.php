<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cache_driver'] = 'file';
$config['show_ee_news'] = 'y';
$config['share_analytics'] = 'y';
$config['prv_msg_upload_url'] = 'https://www.warmzilla.co.uk/images/pm_attachments/';
$config['enable_devlog_alerts'] = 'n';
$config['cookie_prefix'] = '';
$config['require_cookie_consent'] = 'n';
$config['force_redirect'] = 'n';
$config['index_page'] = '';
$config['is_system_on'] = 'y';
$config['multiple_sites_enabled'] = 'n';

// $config['cookie_secure'] = in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1')) ? 'n' : 'y';
$config['app_version'] = '5.3.0';
$config['encryption_key'] = '7d74dea09b135462d513f14e77054e9bd978a556';
$config['session_crypt_key'] = '2fc1499cf6ff3dea255fd237e0491d8f100631fb';
$config['debug'] = '0';
/*$config['cache_driver'] = 'file';
$config['ce_cache_drivers'] = 'redis|file';
$config['ce_cache_redis_servers'] = array(
       array( '/var/run/redis-multi/enzoanic.enzoani.com_redis.sock' )
);*/
$config['ce_cache_drivers'] = 'memcache|file';
$config['ce_cache_licence'] = '0100103400461281351880083';
$config['ce_cache_memcached_servers'] = array(
    array( '/var/run/memcached-multi/memcached.cache.sock', 0, 20 )
);

// $config['ce_cache_secret'] = 'f422c4ca736bdf438bb035bee182c097b5bd3ccf';
$config['ce_cache_off'] = 'no';
$config['ce_cache_file_permissions'] = 0777;
$config['ce_cache_dir_permissions'] = 0777;
// $config['ce_cache_redis_db_index'] = '1';
// $config['ce_cache_async'] = 'no';
$config['mime_whitelist_additions'] = array(
    'image/webp'
);

$base_path = rtrim($_SERVER['DOCUMENT_ROOT'], "/");
$base_url = "https://" . $_SERVER['SERVER_NAME'];
$config['allow_pending_login'] = 'n';
$config['minimee'] = array(
    'cache_path'        => $base_path .'/cache',
    'cache_url'         => $base_url .'/cache',
    'combine_css'       => 'yes',
    'combine_js'        => 'yes',
    'minify_css'        => 'yes',
    'minify_html'       => 'yes',
    'minify_js'         => 'yes',
    'disable'           => 'no',
    'base_path'         => $base_path,
    'base_url'          => $base_url,
    'cachebust'         => '',
    'cleanup'           => 'yes',
    'css_library'       => 'minify',
    'css_prepend_mode'  => 'yes',
    'hash_method'       => 'sha1',
    'js_library'        => 'jsmin',
    'remote_mode'       => 'fgc'
);
switch ($_SERVER['HTTP_HOST'])
{
    case 'warmzilla.test':
        $config['debug'] = '1';
        $config['ce_cache_off'] = 'yes';
        $config['base_url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "/";
        $config['base_path'] = $_SERVER['DOCUMENT_ROOT'] . "/";
        $config['allow_pending_login'] = 'n';
        $config['database'] = array(
            'expressionengine' => array(
                'hostname' => 'localhost',
                'database' => 'warmzilla',
                'username' => 'root',
                'password' => '',
                'dbprefix' => 'exp_',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_unicode_ci',
                'port'     => ''
            ),
        );
        break;

    case 'www.warmzilla.uf-staging.co.uk':
        $config['debug'] = '2';
        $config['ce_cache_off'] = 'yes';
        $config['base_url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "/";
        $config['base_path'] = $_SERVER['DOCUMENT_ROOT'] . "/";
        $config['allow_pending_login'] = 'n';
        $config['database'] = array(
            'expressionengine' => array(
                'hostname' => 'localhost',
                'database' => 'warmzill_staging_2',
                'username' => 'warmzill_staging',
                'password' => '0{L0-kiUX7-i',
                'dbprefix' => 'exp_',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_unicode_ci',
                'port'     => ''
            ),
        );
        break;

    case 'staging.warmzilla.co.uk':
        $config['debug'] = '1';
        $config['ce_cache_off'] = 'yes';
        $config['base_url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "/";
        $config['ce_image_document_root'] = '/chroot' . $_SERVER['DOCUMENT_ROOT'];
        $config['base_path'] = $_SERVER['DOCUMENT_ROOT'] . "/";
        $config['allow_pending_login'] = 'n';
        $config['censored_words'] = 'twat';
        $config['use_category_name'] = 'y';
        $config['minimee'] = array(
            'cache_path'        => '/home/warmzill/warmzilla.co.uk/html/staging/',
            'cache_url'         => 'https://warmzilla.co.uk/staging/cache',
            'combine_css'       => 'yes',
            'combine_js'        => 'yes',
            'minify_css'        => 'yes',
            'minify_html'       => 'yes',
            'minify_js'         => 'yes',
            'disable'           => 'no',
            'base_path'         => '/home/warmzill/warmzilla.co.uk/html/staging/',
            'base_url'          => 'https://warmzilla.co.uk/',
            'cachebust'         => '',
            'cleanup'           => 'yes',
            'css_library'       => 'minify',
            'css_prepend_mode'  => 'yes',
            'hash_method'       => 'sha1',
            'js_library'        => 'jsmin',
            'remote_mode'       => 'fgc'
        );
        $config['database'] = array(
            'expressionengine' => array(
                'hostname' => 'localhost',
                'database' => 'af76c7b9_warmuk',
                'username' => 'af76c7b9_warmuk',
                'password' => 'RoveGrinPhooeyMyth',
                'dbprefix' => 'exp_',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_unicode_ci',
                'port'     => ''
            ),
        );
        break;

    default:
        $config['ce_image_document_root'] = '/chroot' . $_SERVER['DOCUMENT_ROOT'];
        $config['allow_pending_login'] = 'n';
        $config['mime_whitelist_additions'] = array(
            'image/webp'
        );
        $config['database'] = array(
            'expressionengine' => array(
                'hostname' => 'localhost',
                'database' => 'af76c7b9_live',
                'username' => 'af76c7b9_live',
                'password' => 'SingesCroupMurkMelts',
                'dbprefix' => 'exp_',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_unicode_ci', 
                'port'     => ''
            ),
        );
        break;
}

$config['seolite_extra'] = array(
    /* Blog Channel */
    '9' => array(
        'image' => array(
            'field_id' => 81,
            'field_type' => 'file',
        ),
        'desc' => array(
            'field_id' => 82,
            'field_type' => 'text'
        ),
    ),
    /* Pages Channel */
    '8' => array(
        'image' => array(
            'field_id' => 89,
            'field_type' => 'file',
        ),
        'desc' => array(
            'field_id' => 79,
            'field_type' => 'text'
        ),
    ),
    /* Home Channel */
    '7' => array(
        'image' => array(
            'field_id' => 89,
            'field_type' => 'file',
        )
    ),
);

// EOF