<?php
return array(
    'author' => 'John D Wells',
    'author_url' => 'http://johndwells.com/software/versions/minimee',
    'description' => 'Minimee: minimize & combine your CSS and JS files. Minify your HTML. For EE2 only.',
    'docs_url' => 'http://johndwells.github.com/Minimee',
    'name' => 'Minimee',
    'settings_exist' => true,
    'version' => '2.1.13',
    'namespace'   => 'EllisLab\Addons\Minimee',
);
