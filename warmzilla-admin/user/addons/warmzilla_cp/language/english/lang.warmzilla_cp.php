<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

$lang = array(
	'warmzilla_cp' => 'WarmZilla Control Panel',
	'warmzilla_cp_module_name' => 'WarmZilla Control Panel',
	'lable_title_index' => 'WarmZilla Control Panel',
);