<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warmzilla_cp
{

	/* Important globel variables */
	public $errors;
	public $form_settings;
	public $tagdata;
	public $tagparams;
	public $vars;
	public $formData;

	/* Initialize constructor */
	public function __construct()
	{
		/*Load helpful libraries*/
		ee()->load->library('warmzilla_cp_lib', null, 'wcp');
		ee()->load->library('wcp_template_parse', null, 'tp');

		// add the package path so we can access CT scripts & libraries
        ee()->load->add_package_path(PATH_THIRD.'cartthrob/');
        // load CT in. IMPORTANT!
        ee()->load->library('cartthrob_loader');

        ee()->load->library('Cartthrob_payments', null, 'CP');
	}

	function verifyPostCode()
	{
		$postcode = ee()->TMPL->fetch_param('postcode');
		return ee()->wcp->verifyPostCode($postcode);
	}

	function notifyEngineerAdmin()
	{
		$memberID = ee()->TMPL->fetch_param('member_id');
		return ee()->wcp->notifyEngineerAdmin($memberID);
	}

	function getAvailableEngineers()
	{
		$postcode = ee()->TMPL->fetch_param('postcode');
		if(strtoupper($postcode) == "FROM_COOKIE")
		{
			$postcode = $this->boiler_postcode();
		}
		return ee()->wcp->verifyPostCode($postcode, 1);
	}

	function engineerCalendar()
	{
		return ee()->wcp->engineerCalendar();
	}

	function getVat()
	{
		$vat = ee()->TMPL->fetch_param('vat');
		return "dog" + $vat;
	}

	function questionCookie()
	{

		if(! isset($_COOKIE['boilerQuestions']))
		{
			return ee()->TMPL->no_results();
		}

		$boilerQuestions = json_decode($_COOKIE['boilerQuestions'], true);

		if( ( ! isset($boilerQuestions[count($boilerQuestions)-1]['final'])) || (isset($boilerQuestions[count($boilerQuestions)-1]['final']) && $boilerQuestions[count($boilerQuestions)-1]['final'] != "summary"))
		{
			return ee()->TMPL->no_results();
		}

		for ($i = 0; $i < count($boilerQuestions); $i++)
		{
			if($boilerQuestions[$i]['title'] == "")
			{
				unset($boilerQuestions[$i]);
			}
			else
			{
				if(strpos($boilerQuestions[$i]['answer'], "<sup>"))
				{
					$boilerQuestions[$i]['answer'] = strip_tags($boilerQuestions[$i]['answer']) . "<sup>+</sup>";
				}

				if(isset($_COOKIE['editQuestions']) && $_COOKIE['editQuestions'] != "")
				{
					$boilerQuestions[$i]['edit_post'] = "yes";
				}
				else
				{
					$boilerQuestions[$i]['edit_post'] = "no";
				}
			}
		}
		$boilerQuestions = array_values($boilerQuestions);

		$tagdata = ee()->TMPL->tagdata;
		return ee()->TMPL->parse_variables($tagdata, $boilerQuestions);
	}

	/* to render a payment options with tokens */
	function getPaymentOptions()
	{
		ee()->load->helper('form');

		$attrs = array();

		if (ee()->TMPL->fetch_param('encrypt') && bool_string(ee()->TMPL->fetch_param('encrypt'))==FALSE)
		{
 			$encrypt=FALSE;
		}
		else
		{
			$encrypt=TRUE;
		}

		if (ee()->TMPL->fetch_param('id'))
		{
			$attrs['id'] = ee()->TMPL->fetch_param('id');
		}

		if (ee()->TMPL->fetch_param('class'))
		{
			$attrs['class'] = ee()->TMPL->fetch_param('class');
		}

		if (ee()->TMPL->fetch_param('onchange'))
		{
			$attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
		}

		$extra = '';

		if ($attrs)
		{
			$extra .= _attributes_to_string($attrs);
		}

		if (ee()->TMPL->fetch_param('extra'))
		{
			if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ')
			{
				$extra .= ' ';
			}

			$extra .= ee()->TMPL->fetch_param('extra');
		}

		$selectable_gateways = ee()->cartthrob->store->config('available_gateways');

		$name = (ee()->TMPL->fetch_param('name')?  ee()->TMPL->fetch_param('name') : "gateway");
		$selected = (ee()->TMPL->fetch_param('selected') ? ee()->TMPL->fetch_param('selected'): ee()->cartthrob->store->config('payment_gateway') );

 		// get the gateways that the user wants to output
		if (ee()->TMPL->fetch_param('gateways'))
		{
 			foreach (explode("|", ee()->TMPL->fetch_param('gateways')) as $my_gateways)
			{
				$final_g["Cartthrob_".$my_gateways] = "1";
			}
			// Making it so that it's possible to add the default gateway in this parameter without it having been selected as a choosable gateway.
			// if its the default then it's choosable in my book.
			if (isset($final_g[ee()->cartthrob->store->config('payment_gateway')]) && !isset($selectable_gateways[ee()->cartthrob->store->config('payment_gateway')]))
			{
				$selectable_gateways[ee()->cartthrob->store->config('payment_gateway')] = 1;
			}
			$selectable_gateways = array_intersect_key($final_g, $selectable_gateways );
		}
  		// if the users selected gateways is not an option, then we'll use the default
		if (!isset($selectable_gateways[$selected]) && is_array($selectable_gateways) )
		{
			if (isset($selectable_gateways["Cartthrob_". $selected]))
			{
				$selected = "Cartthrob_".$selected;
			}
			elseif (isset($selectable_gateways["Cartthrob_".ee('Encrypt')->decode($selected)]))
			{
				$selected = "Cartthrob_".ee('Encrypt')->decode($selected);
			}
			// make sure this isn't an encoded value.
			elseif (!isset($selectable_gateways[ee('Encrypt')->decode($selected)]))
			{
				$selected =  ee()->cartthrob->store->config('payment_gateway');
				$selectable_gateways = array_merge(array(ee()->cartthrob->store->config('payment_gateway') => '1'), (array) $selectable_gateways);
			}
			else
			{
				$selected = ee('Encrypt')->decode($selected);
			}
		}

		ee()->load->library('api');
		ee()->load->library('api/api_cartthrob_payment_gateways');

		/*if ($this->cart_has_subscription())
		{
			$subscription_gateways = array();

			foreach (ee()->api_cartthrob_payment_gateways->subscription_gateways() as $plugin_data)
			{
				$subscription_gateways[] = $plugin_data['classname'];
			}

			$selectable_gateways = array_intersect_key($selectable_gateways, array_flip($subscription_gateways));
		}*/

 		// if none have been selected, OR if you're not allowed to select, then the default is shown
		if (!ee()->cartthrob->store->config('allow_gateway_selection') || count($selectable_gateways) == 0)
		{
			$selectable_gateways = array(ee()->cartthrob->store->config('payment_gateway') => '1');
			$selected = ee()->cartthrob->store->config('payment_gateway');
 		}

		$gateways = ee()->api_cartthrob_payment_gateways->gateways();

  		$data = array();
		foreach ($gateways as $plugin_data)
		{
 			if (isset($selectable_gateways[$plugin_data['classname']]) )
			{
				ee()->lang->loadfile(strtolower($plugin_data['classname']), 'cartthrob', FALSE);

				if (isset($plugin_data["title"]))
				{
					$title = ee()->lang->line($plugin_data['title']);
				}
				else
				{
					$title = $plugin_data['classname'];
				}
				if ($encrypt)
				{
 					// have to create a variable here, because it'll be used in a spot
					// where it needs to match. each time we encode, the values change.
 					$encoded = ee('Encrypt')->encode($plugin_data['classname']);
					$data[$encoded] = $title;

					if ($plugin_data['classname'] == $selected)
					{
 						$selected = $encoded;
					}
				}
				else
				{
					$data[$plugin_data['classname']] = $title;
				}
			}
		}

		asort($data);

		if (bool_string(ee()->TMPL->fetch_param('add_blank')))
		{
			$data = array_merge(array('' => '---'), $data);
		}

		$html = '';

		$output = array();
		$count = 0;
		$prefix = (ee()->TMPL->fetch_param('prefix') != "") ? ee()->TMPL->fetch_param('prefix') . ":" : "option:";
		foreach ($data as $token => $gateway)
		{
			$output[] = array(
				$prefix . 'token'			=> $token,
				$prefix . 'gateway_name'	=> $gateway,
				$prefix . 'count'			=> $count++,
				$prefix . 'total_results'	=> count($data),
			);
		}
		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $output);
	}

	function get_month()
	{
		$month = array();
		for ($i=1; $i <= 12; $i++)
		{
			$month[] = array('number' => $i);
		}
		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $month);
	}

	function get_year()
	{
		$year = array();
		for ($i=date('Y'); $i <= date('Y') + 20; $i++)
		{
			$year[] = array('number' => $i);
		}
		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $year);
	}

	function availibility_form()
	{
		/* store tagdata and tagparams on variables to easy use*/
		$this->vars = array();

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			if(isset($_POST['cal_date']) && $_POST['cal_date'] != '')
			{
				$getStatus = ee()->db->get_where('wc_engineer_availability', array('cal_date' => ee()->input->post('cal_date', true), 'member_id' => ee()->session->userdata('member_id')))->num_rows();

				if ($getStatus === 1)
				{
					ee()->db->delete('wc_engineer_availability', array('cal_date' => ee()->input->post('cal_date', true), 'member_id' => ee()->session->userdata('member_id')));
				}
				else
				{
					$data = array();
					$data['member_id'] 	= ee()->session->userdata('member_id');
					$data['cal_date'] 	= ee()->input->post('cal_date', true);
					$data['status'] 	= 'available';
					ee()->db->insert('wc_engineer_availability', $data);
				}
			}
			else
			{
				if(isset($_POST['year']) && isset($_POST['month']) && $_POST['year'] != '' && $_POST['month'] != '')
				{
					$start_date =  ee()->input->post('year', true).ee()->input->post('month', true).'01';
					$end_date =  ee()->input->post('year', true).ee()->input->post('month', true).'31';

					$start_time = strtotime($start_date);
					$end_time = strtotime("+1 month", $start_time);
				}

				if(isset($_POST['id']) && $_POST['id'] == 'set_all_off')
				{
					ee()->db->delete('wc_engineer_availability', array('cal_date >=' => $start_date, 'cal_date <=' => $end_date, 'status' => 'available', 'member_id' => ee()->session->userdata('member_id')));
				}

				if(isset($_POST['id']) && $_POST['id'] == 'set_all_on')
				{
					ee()->db->delete('wc_engineer_availability', array('cal_date >=' => $start_date, 'cal_date <=' => $end_date, 'status' => 'available', 'member_id' => ee()->session->userdata('member_id')));

					$temp = ee()->db->get_where('wc_engineer_availability', array('member_id' => ee()->session->userdata('member_id'), 'cal_date >=' => date('Ymd' ,$start_time), 'cal_date <=' => date('Ymd' ,$end_time)))->result();

					$result = array();
					foreach ($temp as $tk => $tv)
					{
						$result[$tv->cal_date] = $tv->status;
					}
					unset($temp);

					$temp = array();
					for($i = $start_time; $i < $end_time; $i += 86400)
					{
						if (!isset($result[date('Ymd', $i)]))
						{
							$temp[] = array(
									"member_id"  	=> ee()->session->userdata('member_id'),
									"cal_date" 		=> date('Ymd', $i),
									"status" 		=> "available"
								);
						}
					}

					ee()->db->insert_batch('wc_engineer_availability', $temp);
				}
			}

		}

		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		$this->tagparams['month_limit'] = isset($this->tagparams['month_limit']) && $this->tagparams['month_limit'] != '' ? $this->tagparams['month_limit'] : 3;

		if(
			(! isset($this->tagparams['member_group']) )  ||
			 ! in_array(ee()->session->userdata('group_id'), explode("|", $this->tagparams['member_group']))
		)
		{
			return ee()->TMPL->no_results();
		}

		$month = isset($this->tagparams['month']) ? $this->tagparams['month'] : "";
		$year = isset($this->tagparams['year']) ? $this->tagparams['year'] : "";

		if($month == "" || $year == "")
		{
			return ee()->TMPL->no_results();
		}

		$start_date = "01-". $this->tagparams['month'] . "-" . $this->tagparams['year'];

		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		if (isset($this->tagparams['month']))
		{
			$dateObj   = DateTime::createFromFormat('!m', $this->tagparams['month']);
			$currentMonthName = $dateObj->format('F');
		}

		$limit  = $this->tagparams['month_limit'];
		$check  = $this->tagparams['month'].'-'.intval($this->tagparams['year']);
		$tempTime = ee()->localize->now;
		$current = "";

		$temp = array();
		$prevURL = '';
		$nextyURL = '';
		for ($i=0; $i < $limit; $i++)
		{
			$temp[] = date('m-Y', $tempTime);
			if(date('m-Y', $tempTime) == $check)
			{
				$current = $tempTime;
			}
			$tempTime = strtotime("+1 month", $tempTime);
		}

		if(! in_array($check, $temp))
		{
		    return ee()->TMPL->no_results();
		}

		$prevURL = date('m-Y', strtotime("-1 month", $current));
		$prevURL = in_array($prevURL, $temp) ? implode("/", explode("-", $prevURL)) : "";

		$nextURL = date('m-Y', strtotime("+1 month", $current));
		$nextURL = in_array($nextURL, $temp) ? implode("/", explode("-", $nextURL)) : "";

		if (isset($this->tagparams['year']))
		{
			$currentYear = $this->tagparams['year'];
		}

		/* query */
		$temp = ee()->db->get_where('wc_engineer_availability', array('member_id' => ee()->session->userdata('member_id'), 'cal_date >=' => date('Ymd' ,$start_time), 'cal_date <=' => date('Ymd' ,$end_time)))->result();
		$result = array();
		foreach ($temp as $tk => $tv)
		{
			$result[$tv->cal_date] = $tv->status;
		}
		unset($temp);

		$this->vars['parseData']['fields'] = array();
		$prefix = 'fields';

		$new_start_time = date('Y-m-d', $start_time);
		$new_start_time = new DateTime($new_start_time);

		$new_end_time = date('Y-m-d', $end_time);
		$new_end_time = new DateTime($new_end_time);

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($new_start_time, $interval, $new_end_time);

		foreach($period as $dt)
		{
			$this->vars['parseData'][$prefix][] = array(
				$prefix . ":name" 	=> $dt->format("Ymd"),
				$prefix . ":date" 	=> $dt->format("l Y-m-d H:i:s\n"),
				$prefix . ":day"  	=> $dt->format("F"),
				$prefix . ":status" => isset($result[$dt->format("Ymd")]) ? $result[$dt->format("Ymd")] : "unavailable",
			);
		}

		for ($i=0; $i < count($this->vars['parseData'][$prefix]); $i++)
		{
			$this->vars['parseData'][$prefix][$i][$prefix . ':count'] = $i + 1;
			$this->vars['parseData'][$prefix][$i][$prefix . ':total_results'] = count($this->vars['parseData'][$prefix]);
		}

		$this->vars['parseData']['prev_url'] = $prevURL;
		$this->vars['parseData']['next_url'] = $nextURL;
		$this->vars['parseData']['current_month_name'] = $currentMonthName;

		/*Call default generate form with current form variables*/
		return $this->generateForm(array("action" => "availibility_form"));

	}

	/*Globel generate form method*/
	public function generateForm($parmas)
	{
		$recaptcha_url = "";

		/*If Recaptcha API entered in backend and enabled from front end form. Generate recaptcha*/
		/*if(isset($this->tagparams['enable_recaptcha']) && $this->tagparams['enable_recaptcha'] == 'yes')
		{
			$recaptcha_url = "<script src='https://www.google.com/recaptcha/api.js'></script>";
			$this->totalFields['recaptcha'] = "<div class='g-recaptcha' data-sitekey='{$this->formSettings['recaptcha_site_key']}'></div>";

		}
		elseif((isset($this->tagparams['enable_recaptcha']) && $this->tagparams['enable_recaptcha'] == 'yes') || ($parmas['source'] == "registration" && (ee()->config->item('use_membership_captcha') == 'y' || ee()->config->item('require_captcha') == 'y')))
		{
			$this->totalFields['captcha'] = ee('Captcha')->create();
		}*/

		/*Load template parsing library*/
		if(isset($this->tagparams['return']))
		{
			if(! class_exists('template_parse'))
	        {
	        	ee()->load->library('template_parse', null, 'tmpl_parse');
	        }

	    	$selectWrapper = ee()->tmpl_parse->template_parser(array('member_id' => '0', 'subject' => '0', 'word_wrap' => '0', 'mailtype' => '0', 'registration_template' => 0, 'message_body' => $this->tagparams['return']));
	    	$this->tagparams['return'] = $selectWrapper['msg_body'];
	    	unset($selectWrapper);
		}

		/*Setting up the hidden variables for current form*/
		$this->vars['hidden_fields']['ACT'] 		= ee()->functions->fetch_action_id(__CLASS__, $parmas['action']);
		$this->vars['hidden_fields']['params'] 		= ee('Encrypt')->encode(serialize($this->tagparams), ee()->config->item('session_crypt_key'));
		$this->vars['hidden_fields']['csrf_token'] 	= XID_SECURE_HASH;
		$this->vars['hidden_fields']['XID'] 		= XID_SECURE_HASH;

		/*Main Action URL of form*/
		$this->vars['action'] = ee()->functions->create_url(ee()->uri->uri_string);

		/*force https if form is secure*/
		if (isset($this->tagparams['secure_action']) && $this->tagparams['secure_action'] == 'yes')
		{
			$this->vars['action'] = str_replace('http://', 'https://', $this->vars['action']);
		}

		/*Method to be called on submit jquery*/
		$this->vars['onsubmit'] = isset($this->tagparams['on_submit']) ? $this->tagparams['on_submit'] : "";

		/*Is form is secure*/
		$this->vars['secure'] = isset($this->tagparams['secure_action']) ? $this->tagparams['secure_action'] == "yes" ? TRUE : FALSE : FALSE;

		/*Attributes listed in code parameters*/
		$form_parameters = "";
		foreach($this->tagparams as $key => $value)
		{

			$exp_key = explode(':', $key);

			if($exp_key[0] == 'attr')
			{
				$form_parameters .= $exp_key[1]."='".$value."' ";
			}

		}

		/*Setting up enctype*/
		$this->vars['enctype'] = $form_parameters.' enctype="multipart/form-data"';

		if (isset($this->vars['parseData'])) {
			$this->tagdata = ee()->TMPL->parse_variables_row($this->tagdata, $this->vars['parseData']);
		}
		/*Return final form*/
		return $recaptcha_url . ee()->functions->form_declaration($this->vars) . $this->tagdata . '</form>';

	}

	/*Default method call every time any form submits*/
	function startFormSubmission()
	{
		/*Get params and decode the value to use in regisration process*/
		$this->formData['params'] = unserialize(ee('Encrypt')->decode(ee()->input->get_post('params'), ee()->config->item('session_crypt_key')));
		$this->formData['data'] = $_POST;
	}

	/*Globel method call every time after final submition of forms ends*/
	function redirect_success($source = "")
	{

		/*Generating return URL*/
		$return_url = isset($this->formData['params']['return']) ? $this->formData['params']['return'] : "";
		$return_url = ee()->functions->create_url($return_url);

		if (isset($this->formData['params']['secure_return']) && $this->formData['params']['secure_return'] == 'yes')
		{

			$return_url = str_replace('http://', 'https://', $return_url);
		}

		/*Redirect when the form submits successfully*/
		ee()->functions->redirect($return_url);

	}

	function get_trv_price_for_current_session()
	{
		ee()->load->helper('inflector');
		ee()->load->helper('array');
		$row_id = ee()->TMPL->fetch_param('row_id');
		$retPrice = ee()->TMPL->fetch_param('base_price');

		$trv_price = 0;
		$wz_total_trv = 0;

		if($row_id != "")
		{
			$item = ee()->cartthrob->cart->item($row_id);
			if ($item && $item->product_id())
			{
				$entry_id = $item->product_id();
			}
			$price_modifiers = ee()->product_model->get_all_price_modifiers($entry_id);

			if ($row_id === FALSE)
			{
	 			if (ee()->cartthrob->cart->meta("all_item_options"))
				{
					$all_keys = ee()->cartthrob->cart->meta("all_item_options");
					foreach ($price_modifiers as $key => $value)
					{
						if (in_array($key, $all_keys))
						{
							unset($price_modifiers[$key]);
						}
					}
				}
			}

			$wz_total_trv = $item->item_options("trv_variations");
			if($wz_total_trv != "")
			{
				foreach ($price_modifiers['trv_variations'] as $data)
				{
					if($data['option_value'] == $wz_total_trv)
					{
						$retPrice += $data['price'];
						$trv_price = $data['price'];
					}
				}
			}
		}

		$tagdata = ee()->TMPL->tagdata;
		if($tagdata != "")
        {
        	$replace = array(
        		'wz_pay_price' 		=> $retPrice,
				'wz_pay_price_format' => number_format($retPrice, 2),
				'wz_pay_price_format1' => number_format($retPrice),
        		'wz_total_trv' => $wz_total_trv,
        		'wz_trv_price' => $trv_price,
        		'wz_trv_price_format' => number_format($trv_price, 2),
        	);
        	return ee()->TMPL->parse_variables_row($tagdata, $replace);
        }
        else
        {
	        if(ee()->TMPL->fetch_param('format') == "yes")
	        {
	        	$basePrice = number_format($basePrice, 2);
	        }
	        return $basePrice;
        }

	}

	function add()
	{
		$operator = ee()->TMPL->fetch_param('operator');
		$format = ee()->TMPL->fetch_param('format');
		$operator = ($operator == "") ? "+" : "-";

		$a1 = ee()->TMPL->fetch_param('a1');
		$a2 = (ee()->TMPL->fetch_param('a2') == "") ? 0 : ee()->TMPL->fetch_param('a2');

		$ret = "";
		switch ($operator) {
			case '-':
				$ret = $a1 - $a2;
				break;

			default:
				$ret = $a1 + $a2;
				break;
		}

		if($format == "yes")
		{
			$ret = number_format($ret);
		}
		return $ret;
	}

	function increase_price()
	{
		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);

		if($boilerQuestions === false)
		{
			return ee()->TMPL->no_results();
		}

		$basePrice = ee()->TMPL->fetch_param('base_price');
		$plumpkit = ee()->TMPL->fetch_param('plumpkit');
		$backboiler = ee()->TMPL->fetch_param('backboiler');

        foreach ($boilerQuestions as $key => $value)
        {
        	if(isset($value['incboilerprice']) && $value['incboilerprice'] != "" && is_numeric($value['incboilerprice']))
        	{
				$basePrice += $value['incboilerprice'];
        	}
        	/*if(isset($value['incinstallcharge']) && $value['incinstallcharge'] != "" && is_numeric($value['incinstallcharge']))
        	{
        		$basePrice += $value['incinstallcharge'];
        	}*/
        }

        if(isset($boilerQuestions['124']) && strtolower($boilerQuestions['124']['answer']) == "no" ||  strtolower($boilerQuestions['124']['answer']) == "unsure" || isset($boilerQuestions['125']) && strtolower($boilerQuestions['125']['answer']) == "yes" && $plumpkit != "" && is_numeric($plumpkit))
        {
			$basePrice += $plumpkit;
		}

        if(isset($boilerQuestions['104']) && strtolower($boilerQuestions['104']['answer']) == "back boiler" && $backboiler != "" && is_numeric($backboiler))
        {
			$basePrice += $backboiler;
        }

		$tagdata = ee()->TMPL->tagdata;
        if($tagdata != "")
        {
        	$replace = array(
        		'wz_final_price' 		=> $basePrice,
        		'wz_final_price_format' => number_format($basePrice, 2),
				'wz_final_price_format1' => number_format($retPrice),
        	);
        	return ee()->TMPL->parse_variables_row($tagdata, $replace);
        }
        else
        {
	        if(ee()->TMPL->fetch_param('format') == "yes")
	        {
	        	$basePrice = number_format($basePrice, 2);
	        }
	        return $basePrice;
        }
	}

	function new_job_offers()
	{
		$this->tagparams = ee()->TMPL->tagparams;
		$company_id = '';
		if(isset($this->tagparams['company_id'])) {
			$company_id = $this->tagparams['company_id'];
		}
		if($company_id) {
			return ee()->wcp->newJobOffers($company_id);
		} else {
			return ee()->wcp->newJobOffers();
		}
	}

	function get_engineers()
	{
		return ee()->wcp->getEngineers();
	}

	function accept_job()
	{
		$this->vars = array();
		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		$this->vars['parseData']['fields'] = array();

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			$data = array();
			$data['order_id'] = ee()->input->post('order_id', true);
			$data['cal_date'] = ee()->input->post('cal_date', true);
			$data['engineer_id'] = ee()->input->post('engineer_id', true);
			$data['company_id'] = ee()->session->userdata('member_id');
			$data['type'] = ee()->input->post('type', true);

			if($_POST['type'] == 'decline')
			{
				$data['decline_reason'] = ee()->input->post('decline_reason', true);
			}
			$site_url = ee()->input->post('site_url', true);

			$result = ee()->wcp->acceptJob($data);

			if($result == 'accepted')
			{
				ee()->functions->redirect($site_url);
			}
			else
			{
				ee()->functions->redirect($site_url.'/'.$result);
			}

		}


		// echo 'string';exit();
		return $this->generateForm(array("action" => "accept_job"));
	}

	function add_service_area()
	{
		return ee()->wcp->addServiceArea();
	}

	function is_accepted_job()
	{
		$data = array();
		$data['order_id'] 			= ee()->TMPL->fetch_param('order_id');
		$data['installation_date'] 	= ee()->TMPL->fetch_param('installation_date');

		return ee()->wcp->isAcceptedJob($data);
	}

	function replace()
	{
		return preg_replace('/^(?:<br\s*\/?>\s*)+/', '', nl2br(ee()->TMPL->tagdata));
	}

	function get_cookie()
	{
		if(isset($_COOKIE['boilerQuestions']))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	function get_declined_member_ids()
	{
		$entry_id = ee()->TMPL->fetch_param('entry_id');
		return ee()->wcp->getDeclinedMemberIDs($entry_id);
	}

	function reassign_engineer()
	{
		$data = array();
		$data['entry_id'] = ee()->TMPL->fetch_param('entry_id');
		$data['zip'] = ee()->TMPL->fetch_param('zip');
		return ee()->wcp->reassignEngineer($data);
	}

	function change_engineer()
	{
		$this->vars = array();
		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			$data = array();
			$data['order_id'] = ee()->input->post('order_id', true);
			$data['site_url'] = ee()->input->post('site_url', true);
			$data['engineer_id'] = ee()->input->post('engineer_id', true);
			$data['engineer_email'] = ee()->input->post('engineer_email', true);
			$data['new_engineer_email'] = ee()->input->post('new_engineer_email', true);
			$data['customer_email'] = ee()->input->post('customer_email', true);
			$data['company_email'] = ee()->input->post('customer_email', true);
			$result = ee()->wcp->changeEngineer($data);

			$globalVariables = array(
				'global_customer_order_id' => ee()->input->post('order_id', true),
			);

			/* Email to New Engineer */
			$messageBody 	= ee()->wcp->getEmailTemplate("reassign_engineer_email");
			$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
			$parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'You have been assigned a new WarmZilla job!';
			$to = $data['new_engineer_email'];
			$message = $parsedHtml;
			ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

			/* Email to Old Engineer */
			$messageBody 	= ee()->wcp->getEmailTemplate("reassign_engineer_email_old");
			$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
			$parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'Your job has been reassigned';
			$to = $data['engineer_email'];
			$message = $parsedHtml;
			ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

			/* Email to Customer */
			$messageBody 	= ee()->wcp->getEmailTemplate("reassign_engineer_email_customer");
			$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
			$parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'Your job has been reassigned to a new engineer';
			$to = $data['customer_email'];
			$message = $parsedHtml;
			ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

			/* Email to Company */
			$messageBody 	= ee()->wcp->getEmailTemplate("reassign_engineer_email_company");
			$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
			$parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'You have reassigned a job to a new Engineer';
			$to = $data['company_email'];
			$message = $parsedHtml;
			ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

			/* Email to Admin */
			$messageBody 	= ee()->wcp->getEmailTemplate("reassign_engineer_email_admin");
			$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
			$parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'A job has been reassigned to another engineer';
			$to = "rhys@warmzilla.co.uk";
			$message = $parsedHtml;
			ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
			ee()->functions->redirect($data['site_url']);
		}

		return $this->generateForm(array("action" => "change_engineer"));
	}

	function send_email()
	{
		return ee()->wcp->sendEmail();
	}

	function send_reminder()
	{
		$email_address = ee()->TMPL->fetch_param('email_address');
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'Email Quote Follow Up';
        $to = $email_address;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}
	
	function send_service_reminder()
	{
		$email_address = ee()->TMPL->fetch_param('email_address');
		$installation_date = ee()->TMPL->fetch_param('installation_date');
		$service_date = strtotime('+ 1 year', $installation_date);
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'Boiler service due';
        $to = $email_address;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}

	function send_discount_email()
	{
		$email_address = ee()->TMPL->fetch_param('email_address');
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'Get up to £100 off your recent boiler quote';
        $to = $email_address;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}

	function send_discount_email_follow_up()
	{
		$email_address = ee()->TMPL->fetch_param('email_address');
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'You have 24hrs before your coupon expires';
        $to = $email_address;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}

	function do_email()
	{
		$email_address = ee()->TMPL->fetch_param('email_address');
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'Important Reminder: Upload Your Current Boiler Photos to WarmZilla';
        $to = $email_address;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}

	function email_reminder_admin()
	{
		$globalVariables = array(
            'current_time_plus_2_day' => strtotime('+2 day', ee()->localize->now),
        );

        $messageBody    = ee()->wcp->getEmailTemplate("email_reminder_to_admin");
        $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
        $parsedHtml     = ee()->tp->template_parser($messageBody);

        if($parsedHtml['msg_body'] != '')
        {
        	echo 'Email reminder runs successfully.';
        }

		exit();
	}

	function email_quote_follow_up()
	{
		$globalVariables = array(
			'current_time_minus_1_day' => date('Y-m-d h:i A', strtotime('-1 day', ee()->localize->now)),
			'current_time_minus_2_day' => date('Y-m-d h:i A', strtotime('-2 day', ee()->localize->now))
		);

		$messageBody    = ee()->wcp->getEmailTemplate("email_quote_follow_up");
		$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		$parsedHtml     = ee()->tp->template_parser($messageBody);

        if($parsedHtml['msg_body'] != '')
        {
        	echo 'Email follow up reminder runs successfully.';
        }

		exit();
	}

	function email_discount()
	{
		$globalVariables = array(
			'current_time_minus_1_day' => date('Y-m-d h:i A', strtotime('-3 day', ee()->localize->now)),
			'current_time_minus_2_day' => date('Y-m-d h:i A', strtotime('-4 day', ee()->localize->now))
		);

		$messageBody    = ee()->wcp->getEmailTemplate("email_discount");
		$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		$parsedHtml     = ee()->tp->template_parser($messageBody);

        if($parsedHtml['msg_body'] != '')
        {
        	echo 'Email discount runs.';
        }

		exit();
	}

	function email_discount_follow_up()
	{
		$globalVariables = array(
			'current_time_minus_1_day' => date('Y-m-d h:i A', strtotime('-10 day', ee()->localize->now)),
			'current_time_minus_2_day' => date('Y-m-d h:i A', strtotime('-11 day', ee()->localize->now))
		);

		$messageBody    = ee()->wcp->getEmailTemplate("email_discount_follow_up");
		$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		$parsedHtml     = ee()->tp->template_parser($messageBody);

        if($parsedHtml['msg_body'] != '')
        {
        	echo 'Email discount follow up runs.';
        }

		exit();
	}

	public static function vendigo()
	{
		$data = $_POST['$data'];
		$secret_key = $_POST['$secret_key'];
		$base64 = base64_encode($data);
		$hash = hash_hmac('sha256', $base64, $secret_key);
		echo "{$base64}.{$hash}";
	}
	
	function email_boiler_service()
	{
		$messageBody    = ee()->wcp->getEmailTemplate("email_boiler_service");
		$date_to_search = strtotime('-1 year', ee()->localize->now);
		$date_to_search_start = strtotime('-43 day', $date_to_search);
		$date_to_search_end = strtotime('-41 day', $date_to_search);
		$service_date = strtotime('+42 day', ee()->localize->now);
		$service_book_date = strtotime('-21 day', $service_date);
		$globalVariables = array(
			'service_due_date' => date('d.m.y', $service_date),
			'service_book_date' => date('d.m.y', $service_book_date),
			'date_to_search_start' => $date_to_search_start,
			'date_to_search_end' => $date_to_search_end
		);
		var_dump("date to search: " . date('d/m/y', $date_to_search) . "<br>");
		var_dump("date to search start: " . date('d/m/y', $date_to_search_start). "<br>");
		var_dump("date to search end: " . date('d/m/y', $date_to_search_end). "<br>");
		var_dump("service date: " . date('d/m/y', $service_date). "<br>");
		var_dump("service book date: " . date('d/m/y', $service_book_date));

		$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		$parsedHtml     = ee()->tp->template_parser($messageBody);
		echo 'all working';

		exit();
	}
	
	function do_email_admin()
	{
		/*$email_address = ee()->TMPL->fetch_param('email_address');*/
		$tagData = ee()->TMPL->tagdata;
		$parsedHtml = ee()->tp->template_parser($tagData);

		$subject = 'Reminder: Contact customer about photo upload';
        // $to = ee()->config->item('webmaster_email');
        $to = ee()->wcp->admin_emails();
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
	}

	function get_searchabe_items()
	{

		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);
		if($boilerQuestions === false)
		{
			return ee()->TMPL->no_results();
		}

		$debug = array();
		$question = array(
			'flue' => array(
				'flue_come_out' 		=> 108,
				'sloped_flat'   		=> 122,
				'flue_roof_position' 	=> 123
			),
			'install_type' => array(
				'old_boiler' 			=> 104,
				'current_boiler_fuel' 	=> 105,
				'conv_combi' 			=> 119,
			),

			'bedroom' => 112,

			'bath_shower' => array(
				'bath_tubs' => 111,
				'bath_showers' => 110
			)
		);

		$category = "";
		if($boilerQuestions[ $question['flue']['flue_come_out'] ]['answer'] == "Wall")
		{
			$category .= "5&"; //Horizontal
			$debug[] = array(
				'debug:type' 		=> "Flue",
				'debug:category' 	=> "Horizontal",
			);
		}
		elseif($boilerQuestions[ $question['flue']['sloped_flat'] ]['answer'] == "Flat")
		{
			$category .= "8&"; //Flat
			$debug[] = array(
				'debug:type' 		=> "Flue",
				'debug:category' 	=> "Flat",
			);
		}
		elseif($boilerQuestions[ $question['flue']['flue_roof_position'] ]['answer'] == "Top Two Thirds")
		{
			$category .= "7&"; //Vertical 2/3
			$debug[] = array(
				'debug:type' 		=> "Flue",
				'debug:category' 	=> "Vertical 2/3",
			);
		}
		else
		{
			$category .= "6&"; //Vertical 1/3
			$debug[] = array(
				'debug:type' 		=> "Flue",
				'debug:category' 	=> "Vertical 1/3",
			);
		}

		/*
		========================================
			Calculate Boiler Type
		========================================
		*/
		if($boilerQuestions[ $question['install_type']['current_boiler_fuel'] ]['answer'] == "LPG")
		{
			$category .= "22&"; //Combi - Combi
			$debug[] = array(
				'debug:type' 		=> "Boiler Type",
				'debug:category' 	=> "LPG - Combi",
			);
		}
		elseif($boilerQuestions[ $question['install_type']['old_boiler'] ]['answer'] == "Combi")
		{
			$category .= "9&"; //Combi - Combi
			$debug[] = array(
				'debug:type' 		=> "Boiler Type",
				'debug:category' 	=> "Combi - Combi",
			);
		}
		elseif($boilerQuestions[ $question['install_type']['old_boiler'] ]['answer'] == "Standard")
		{
			if($boilerQuestions[ $question['install_type']['conv_combi'] ]['answer'] == "Yes")
			{
				$category .= "12&"; //Conventional - Combi
				$debug[] = array(
					'debug:type' 		=> "Boiler Type",
					'debug:category' 	=> "Conventional - Combi",
				);
				$showRewire = "no";
			}
			else
			{
				$category .= "10&"; //Conventional - Conventional
				$debug[] = array(
					'debug:type' 		=> "Boiler Type",
					'debug:category' 	=> "Conventional - Conventional",
				);
			}
		}
		elseif($boilerQuestions[ $question['install_type']['old_boiler'] ]['answer'] == "Back Boiler")
		{
			$category .= "19&"; //Back Boiler - Combi
			$debug[] = array(
				'debug:type' 		=> "Boiler Type",
				'debug:category' 	=> "Back Boiler - Combi",
			);
		}
		else //if($boilerQuestions[ $question['install_type']['old_boiler'] ]['answer'] == "System")
		{
			if($boilerQuestions[ $question['install_type']['conv_combi'] ]['answer'] == "Yes")
			{
				$category .= "12&"; //Conventional - Combi
				$debug[] = array(
					'debug:type' 		=> "Boiler Type",
					'debug:category' 	=> "Conventional - Combi",
				);
				$showRewire = "no";
			}
			else 
			{
				$category .= "11&"; //System - System
				$debug[] = array(
					'debug:type' 		=> "Boiler Type",
					'debug:category' 	=> "System - System",
				);
			}
		}

		if($boilerQuestions[ $question['bedroom'] ]['answer'] > 5)
		{
			$category .= "21&"; // 6-plus (6+)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(6+)",
			);
		}
		elseif($boilerQuestions[ $question['bedroom'] ]['answer'] <= 1)
		{
			$category .= "30&"; // bed_less_1 (< 1)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(< 1)",
			);
		}
		elseif($boilerQuestions[ $question['bedroom'] ]['answer'] <= 2)
		{
			$category .= "23&"; // bed_less_2 (< 2)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(< 2)",
			);
		}
		elseif($boilerQuestions[ $question['bedroom'] ]['answer'] <= 3)
		{
			$category .= "16&"; // bed_less_3 (< 3)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(< 3)",
			);
		}
		elseif($boilerQuestions[ $question['bedroom'] ]['answer'] <= 4)
		{
			$category .= "17&"; // bed_less_4 (< 4)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(< 4)",
			);
		}
		else //if($boilerQuestions[ $question['bedroom'] ]['answer'] <= 5)
		{
			$category .= "18&"; // bed_less_5 (< 5)
			$debug[] = array(
				'debug:type' 		=> "No. of Bedrooms",
				'debug:category' 	=> "(< 5)",
			);
		}

		$totalBath = (int) $boilerQuestions[ $question['bath_shower']['bath_tubs'] ]['answer'] + (int) $boilerQuestions[ $question['bath_shower']['bath_showers'] ]['answer'];
		if($totalBath <= 2)
		{
			$category .= "13&"; //bath_less_2 (< 2)
			$debug[] = array(
				'debug:type' 		=> "No. of Bathtubs + Showers",
				'debug:category' 	=> "(< 2)",
			);
		}
		elseif($totalBath <= 3)
		{
			$category .= "14&"; //bath_less_3 (< 3)
			$debug[] = array(
				'debug:type' 		=> "No. of Bathtubs + Showers",
				'debug:category' 	=> "(< 3)",
			);
		}
		else
		{
			$category .= "15&"; //bath_less_4 (< 4)
			$debug[] = array(
				'debug:type' 		=> "No. of Bathtubs + Showers",
				'debug:category' 	=> "(< 4)",
			);
		}

		for ($i = 0; $i < count($debug); $i++)
		{
			$debug[$i]['debug:count'] = $i+1;
			$debug[$i]['debug:total_results'] = count($debug);
		}

		$replace = array(
			'wcp_category' 	=> rtrim($category, "&"),
			'wcp_debug' 	=> $debug,
			'show_rewire'	=> $showRewire
		);

		return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, $replace);

	}

	function get_trv_variations()
	{
		$variations = array();
		for ($i=1; $i <= 10; $i++)
		{
			$variations[] = array('number' => $i);
		}
		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $variations);
	}

	function is_trv_selected()
	{
		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);
		if($boilerQuestions === false)
		{
			return ee()->TMPL->no_results();
		}

		if (array_key_exists("109",$boilerQuestions) && $boilerQuestions[109]['answer'] == 'No')
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function set_areas()
	{
		$this->vars = array();
		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			$data = array();
			$data['site_url'] = ee()->input->post('site_url', true);
			$data['entry_id'] = ee()->input->post('location_id', true);
			$data['company_id'] = ee()->input->post('company_id', true);

			$result = ee()->wcp->setAreas($data);
			if($result == 'submitted') {
				ee()->functions->redirect($data['site_url']);
			}
		}

		return $this->generateForm(array("action" => "set_areas"));
	}


	function submit_for_review()
	{
		$this->vars = array();
		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		// $this->vars['parseData']['fields'] = array();

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			$data = array();
			$data['site_url'] = ee()->input->post('site_url', true);
			$data['order_id'] = ee()->input->post('order_id', true);

			$result = ee()->wcp->submitForReview($data);

			$globalVariables = array(
                'global_customer_order_id' => ee()->input->post('order_id', true),
            );

            // $messageBody    = ee()->tp->message_body(215);
            $messageBody    = ee()->wcp->getEmailTemplate("notify_admin_for_review");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

            $subject = 'Installation Job Ready for Review';
			// $to = ee()->config->item('webmaster_email');
            $to = "rhys@warmzilla.co.uk";
            $message = $parsedHtml;
            ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

			ee()->functions->redirect($data['site_url']);

		}


		return $this->generateForm(array("action" => "submit_for_review"));
	}

	function mark_complete()
	{
		$this->vars = array();
		$this->tagdata = ee()->TMPL->tagdata;
		$this->tagparams = ee()->TMPL->tagparams;
		if(! is_array($this->tagparams))
		{
			$this->tagparams = array();
		}

		if(isset($_POST) && is_array($_POST) && count($_POST))
		{
			/*Call Default method load before every form start submission*/
			$this->startFormSubmission();

			$entryID = ee()->input->post('order_id', true);
			$siteURL = ee()->input->post('site_url', true);

			/*to change the status of entry from open to New starts*/
	        $entryData = ee('Model')->get('ChannelEntry')
	                    ->filter('entry_id', $entryID)
	                    ->first();

	        if($entryData)
	        {
	            $entryData->status_id = '29';
	            $entryData->status = 'Completed';
	            $entryData->save();
	        }
	        /*to change the status of entry from open to New ends*/

			ee()->functions->redirect($siteURL);

		}


		return $this->generateForm(array("action" => "mark_complete"));
	}

	function get_status()
	{
		$result = ee()->db->get('statuses');
		$allStatus = $result->result();

		if($result->num_rows == 0)
		{
			return false;
		}

		$status = array();
		foreach ($allStatus as $ask => $asv)
		{
			$status[] = array(
				'status_id' 	=> $asv->status_id,
				'status_name' 	=> $asv->status,
			);
		}

		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $status);
	}

	/*function get_trv_count()
	{
		if (isset($_COOKIE['trv_variations']))
		{

			ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $variations);
		}
		else
		{
			return false;
		}
		print_r($_COOKIE['trv_variations']);
	}*/

	function boiler_postcode()
	{
		return (isset($_COOKIE['boilerPostCode'])) ? strtoupper($_COOKIE['boilerPostCode']) : "";
	}

	function get_action_id()
	{
		$action = ee()->TMPL->fetch_param('action');
		return ee()->wcpModel->getActionID("get_engineer_from_installation_date");
	}

	function get_engineer_from_installation_date()
	{
		$postcode = isset($_GET['postcode']) ? $_GET['postcode'] : $this->boiler_postcode();
		echo ee()->wcp->verifyPostCode($postcode, 1);exit();
	}

	function json_clear_content()
	{
		$tagdata = ee()->TMPL->tagdata;
		$tagdata = htmlentities(htmlspecialchars($tagdata));
		return $tagdata;
	}

	function check_flue_position()
	{

		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);
		if($boilerQuestions === false)
		{
			return ee()->TMPL->no_results();
		}

		$flue_come_out_question = 108;

		$ret = array('wz_flue_position' => "Vertical");
		if($boilerQuestions[ $flue_come_out_question ]['answer'] == "Wall")
		{
			$ret['wz_flue_position'] = "Horizontal";
		}

		return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, $ret);

	}

	function check_plump_kit()
	{

		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);
		if($boilerQuestions === false)
		{
			return ee()->TMPL->no_results();
		}

		$plump_kit = 124;
		$ret = array('wz_plump_kit' => false);
		if(isset($boilerQuestions[$plump_kit]) && strtolower($boilerQuestions[$plump_kit]['answer']) == "no" || strtolower($boilerQuestions[$plump_kit]['answer']) == "unsure")
		{
			$ret['wz_plump_kit'] = true;
		}

		if(isset($boilerQuestions['125']) && strtolower($boilerQuestions['125']['answer']) == "yes")
		{
			$ret['wz_plump_kit'] = true;
		}

		return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, $ret);
	}

	function vendigo_hash()
	{	
		$query = ee()->db->select('value')->from('cartthrob_settings')->where('key', 'Cartthrob_vendigo_settings')->get();
		if($query->num_rows == 0)
		{
			return false;
		}

		$result = $query->row('value');
		$result = unserialize($result);
		if(isset($result['public_key']) && isset($result['secret_key']))
		{
			ee()->load->model('order_model');
			$order = ee()->order_model->get_order(ee()->TMPL->fetch_param('order_id'));

			$secret_key = $result['secret_key'];
			$data = [
				'publicKey' => $result['public_key'],
				'orderId' => "order_" . $order['entry_id'],
				'amount' => $order['order_total'],
				'verticalId' => 3,
				'productType' => isset($_COOKIE['vendigo_type']) ? $_COOKIE['vendigo_type'] : "IBC",
				'apr' => isset($_COOKIE['vendigo_apr']) ? $_COOKIE['vendigo_apr'] : 9.90,
				'term' => isset($_COOKIE['vendigo_months']) ? $_COOKIE['vendigo_months'] : 36,
				'maxTerm' => 120,
				'deposit' => isset($_COOKIE['vendigo_deposit']) ? $_COOKIE['vendigo_deposit'] : 0,
				'defferedPeriod' => 0,

				'successUrl' => '/my-order/thank-you/' . $order['entry_id'],
				'customer' => [
					'title' => 'MR',
					'firstName' => $order['order_shipping_first_name'],
					'lastName' => $order['order_shipping_last_name'],
					'dobDay' => '',
					'dobMonth' => '',
					'dobYear' => '',
					'email' => $order['order_customer_email'],
					'phone' => '',
					'maritalStatus' => '',
					'dependants' => '',
					'earners' => '',
					// 'employment' => [
					// 	'status' => '',
					// 	'companyName' => '',
					// 	'monthInRole' => '',
					// 	'monthlyIncome' => '',
					// ],
					// 'bank' => [
					// 	'holderName' => '',
					// 	'sortCode' => '',
					// 	'accountNumber' => '',
					// ],
					'addresses' => [
						[
							'monthsAtAddress' => '',
							'companyName' => '',
							'flatNumber' => '',
							'buildingName' => '',
							'buildingNumber' => preg_replace('/\D/','', $order['order_shipping_address']),
							'street' => preg_replace('/[0-9,]+/s','', $order['order_shipping_address']),
							'city' => $order['order_shipping_city'],
							'postcode' => $order['order_shipping_zip'],
						]
					]
				]

			];
			// echo "<pre>";
			// print_r($data);
			// exit();
			$data = json_encode($data);
			// 078 7362 3582
			// Ruth Goodwin
			// 601320
			// 43049540
			// $data = '{"publicKey":"AK7p1XpGUXm1lVzxilZgfErrW6ALWct2","orderId":"1304A","amount":2345.67,"verticalId":3,"productType":"IFC","apr":0,"term":12,"maxTerm":36,"deposit":500.23,"defferedPeriod":0,"successUrl":"/thank-you","customer": {"title":"MR","firstName":"John","lastName":"Smith","dobDay":1,"dobMonth":11,"dobYear":1970,"email":"test@test.com","phone":"0123456789","maritalStatus":"SINGLE","dependants":0,"earners":1,"employment": {"status":"EMPLOYED","companyName":"Vendigo","monthInRole":12,"monthlyIncome":45000},"bank": {"holderName":"John Smith","sortCode":"000099","accountNumber":"12345678"},"addresses": [{"residentialStatus":"OWNER WITH MORTGAGE","monthlyRentOrMortgage":2000,"monthsAtAddress":28,"companyName":"Vendigo","flatNumber":"","buildingName":"","buildingNumber":"65","street":"Grosvenor Street","city":"London","postcode":"W1K 3JH"}, {"monthsAtAddress":8,"companyName":"","flatNumber":"","buildingName":"Waterside","buildingNumber":"","street":"Basin Road","city":"Worcester","postcode":"WR5 3DA"}]}}';
			$base64 = base64_encode($data);
			$hash = hash_hmac('sha256', $base64, $secret_key);
			return "{$base64}.{$hash}";
		}
		else
		{
			return false;
		}
	}

	function get()
	{
		$key = ee()->TMPL->fetch_param('key');
		if($key == "") {
			return "";
		}

		return ee()->input->get($key);
	}

	function vendigo_metadata()
	{
		return json_encode([]);
	}

}