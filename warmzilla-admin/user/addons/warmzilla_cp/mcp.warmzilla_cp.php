<?php if (!defined('BASEPATH')) {exit('No direct script access allowed'); }

require PATH_THIRD . 'warmzilla_cp/config.php';

class Warmzilla_cp_mcp {

	public $table_rows;
	public $vars;
	public $errors;

	/* Initialize constructor */
	function __construct() {
		/*Load helpful libraries*/
		ee()->load->library('warmzilla_cp_lib', null, 'WCP');

		/* Needful Helper classes */
		ee()->load->helper(array('form', 'url'));
	}

	function index() {
		/*Set title of the page*/
		ee()->view->cp_page_title = lang('warmzilla_cp');

		/* Generate View file */
		return array(
			'heading' => lang('warmzilla_cp'),
			'body' => ee('View')->make('warmzilla_cp:index')->render(array()),
			'breadcrumb' => array(
				ee('CP/URL', 'addons/settings/warmzilla_cp')->compile() => lang('lable_title_index'),
			),
		);
	}

	function test()
	{

		$entry_id = "312";

		if(isset($_COOKIE['boilerQuestions']) && $_COOKIE['boilerQuestions'] != "")
		{
			$boilerQuestions = json_decode($_COOKIE['boilerQuestions'], true);

			if(is_array($boilerQuestions) && count($boilerQuestions))
			{
				for ($i = 0; $i < count($boilerQuestions); $i++)
				{
					if($boilerQuestions[$i]['selection'] != "")
					{
						$insert = array(
							'entry_id' => $entry_id,
							'row_order' => ($i +1),
							'fluid_field_data_id' => 0,
							'col_id_51' => isset($boilerQuestions[$i]['title']) ? $boilerQuestions[$i]['title'] : "",
							'col_id_52' => isset($boilerQuestions[$i]['answer']) ? $boilerQuestions[$i]['answer'] : "",
							'col_id_53' => isset($boilerQuestions[$i]['incBoilerPrice']) ? $boilerQuestions[$i]['incBoilerPrice'] : "",
							'col_id_54' => isset($boilerQuestions[$i]['incInstallCharge']) ? $boilerQuestions[$i]['incInstallCharge'] : "",
						);

						ee()->db->insert('channel_grid_field_100', $insert);
					}
				}
			}
		}

	}

}