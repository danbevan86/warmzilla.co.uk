<?php
require PATH_THIRD.'warmzilla_cp/config.php';
return array(
	'author'      		=> WARMZILLA_AUTHOR,
	'author_url'  		=> WARMZILLA_AUTHOR_URL,
	'name'        		=> WARMZILLA_NAME,
	'description' 		=> 'WarmZilla Control Panel Custom module to achieve some custom functionality.',
	'version'     		=> WARMZILLA_VER,
	'namespace'   		=> 'ZealousWeb\Addons\WarmzillaCp',
	'settings_exist' 	=> TRUE
);