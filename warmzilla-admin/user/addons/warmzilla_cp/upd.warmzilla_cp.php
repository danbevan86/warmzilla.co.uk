<?php if (!defined('BASEPATH')) {exit('No direct script access allowed'); }

require_once PATH_THIRD . 'warmzilla_cp/config.php';

class Warmzilla_cp_upd {

	public $version = WARMZILLA_VER;
	public $settings = array();
	private $module_name = WARMZILLA_MOD_NAME;
	private $engineerMemberGroup = "6";

	/* Initialize constructor */
	public function __construct() {
		ee()->load->dbforge();
	}

	/**
	 * Install the module
	 * @param $setting       (Setting array optional)
	 * @return boolean TRUE
	 **/
	public function install($settings = array())
	{

		$this->settings = $settings;
		$mod_data = array(
			'module_name' => $this->module_name,
			'module_version' => $this->version,
			/*'settings' => serialize($this->settings),*/
			'has_cp_backend' => "y",
			'has_publish_fields' => 'n',
		);
		ee()->db->insert('modules', $mod_data);


		$fields = array(
			'id' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE,
				'auto_increment'=> TRUE
			),
			'member_id' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE,
			),
			'cal_date' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE
			),
			'status' => array(
                'type'          => 'varchar',
                'constraint'    => '20',
                'null'          => TRUE
            ),
			'order_id' => array(
                'type'          => 'int',
                'constraint'    => '10',
                'unsigned'      => TRUE,
                'null'          => TRUE
            ),
		);
		ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('id', TRUE);
        ee()->dbforge->create_table('wc_engineer_availability');

        $fields = array(
			'id' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE,
				'auto_increment'=> TRUE
			),
			'member_id' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE,
			),
			'boiler_fitting_date' => array(
				'type'          => 'int',
				'constraint'    => '10',
				'unsigned'      => TRUE,
				'null'          => FALSE,
			),
			'order_id' => array(
                'type'          => 'varchar',
                'constraint'    => '30',
                'null'          => TRUE
                ),
			'status' => array(
                'type'          => 'varchar',
                'constraint'    => '20',
                'null'          => TRUE
                ),
		);
		ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('id', TRUE);
        ee()->dbforge->create_table('wc_engineer_work');

		/*Default method*/
		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'find_postcode_availibility',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'availibility_form',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'accept_job',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class'		=> $this->module_name,
			'method'	=> 'set_areas',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'email_reminder',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'email_boiler_service',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'email_reminder_admin',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'email_quote_follow_up',
		);
		ee()->db->insert('actions', $data);
		
		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'submit_for_review',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'change_engineer',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'mark_complete',
		);
		ee()->db->insert('actions', $data);

		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'get_engineer_from_installation_date',
		);
		ee()->db->insert('actions', $data);

		return TRUE;

	}

	/**
	 * Uninstall the module
	 * @return boolean TRUE
	 **/
	public function uninstall() {

		ee()->db->select('module_id');
		$query = ee()->db->get_where('modules', array('module_name' => $this->module_name));

		ee()->db->where('module_id', $query->row('module_id'));
		ee()->db->delete('module_member_groups');

		ee()->db->where('module_name', $this->module_name);
		ee()->db->delete('modules');

		ee()->db->where('class', $this->module_name);
		ee()->db->delete('actions');

		ee()->db->where('class', $this->module_name . '_mcp');
		ee()->db->delete('actions');

		ee()->dbforge->drop_table('wc_engineer_availability');
		ee()->dbforge->drop_table('wc_engineer_work');

		return TRUE;

	}

	/**
	 * Update the module
	 * @param $current       (Current version number)
	 * @return boolean
	 **/
	public function update($current = '') {

		if ($current == $this->version) {
			// No updates
			return FALSE;
		}


		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'get_engineer_from_installation_date',
		);
		ee()->db->insert('actions', $data);

		return true;

	}

}

/* End of file upd.warmzilla_cp.php */
/* Location: /system/expressionengine/third_party/warmzilla_cp/upd.warmzilla_cp.php */
?>