<?php if (!defined('BASEPATH')) {exit('No direct script access allowed'); }



class Warmzilla_cp_model extends CI_Model
{
	/* Initialize constructor */
	function __construct()
	{
		
	}

	function verifyPostCode($postcode, $flag = 0)
	{
		/* get members of given postcode */
		if($flag == 0) {
			ee()->db->limit(1);
		}
		ee()->db->select('cd_99.field_id_99');
		ee()->db->from('channel_titles ct');
		
		ee()->db->join('channel_data_field_99 as cd_99', 'cd_99.entry_id = ct.entry_id');
		ee()->db->join('channel_grid_field_98 as cdg_98', 'cdg_98.entry_id = ct.entry_id');
		
		ee()->db->where('ct.channel_id', "11");
		ee()->db->where('cd_99.field_id_99 != ', "");
		ee()->db->where('cdg_98.col_id_50', $postcode);

		$get = ee()->db->get();
		if($get->num_rows == 0)
		{
			return false;
		}

		$result = $get->result();
		$memberID = array();
		for ($i = 0; $i < count($result); $i++)
		{
			if($result[$i]->field_id_99 != "")
			{
				$tom = array_merge($memberID, explode('|', $result[$i]->field_id_99));
				if (in_array("314", $tom)) {
					$memberID[] = "314";
				} else {
					$memberID = array_merge($memberID, explode('|', $result[$i]->field_id_99));
				}
			}
		}

		$memberID = array_unique($memberID);
		
		/* get available engineers within given postcode */
		ee()->db->distinct();
		ee()->db->select('ae.member_id, cal_date, status, weight');
		ee()->db->from('wc_engineer_availability ae');
		ee()->db->join('members as m', 'm.member_id = ae.member_id');
		ee()->db->where_in('ae.member_id', $memberID);
		ee()->db->where('cal_date >=', date('Ymd',strtotime('+ 2 day', ee()->localize->now)));
		ee()->db->where('m.group_id', 10);
		ee()->db->group_by('cal_date');
		ee()->db->order_by('weight desc, cal_date');
		$get = ee()->db->get();
		if($get->num_rows == 0)
		{
			return false;
		}
		$result = $get->result();
		
		/* get all installation_date */
		/*temp code*/
		ee()->db->select('field_id_101');
		ee()->db->from('channel_data_field_101');
		ee()->db->where('field_id_101 <>', '');
		$getInstallationDate = ee()->db->get();
		if($get->num_rows == 0)
		{
			return false;
		}
		$tempInstallationDate = $getInstallationDate->result();

		$installationDate = array();
		foreach ($tempInstallationDate as $tik => $tiv)
		{
			$installationDate[] = date('Ymd', $tiv->field_id_101);
		}

		$installationDate = array();
		$temp = array();
		$selectedDate = array();

		foreach ($result as $rk => $rv)
		{
			/*remove the date which is already selected by any user*/
			if (! in_array($rv->cal_date, $installationDate))
			{
				$temp[] = array(
					'start' => date('Y-m-d', strtotime($rv->cal_date)),
					'end' => date('Y-m-d', strtotime($rv->cal_date)),
					'overlap' => false,
					'className' => $rv->status .' '.strtotime($rv->cal_date),
					'timestamp' => strtotime($rv->cal_date),
					'member_id' => $rv->member_id,
				);
			}
		}

		if(count($temp) != 0)
		{
			$result = json_encode($temp);
		}
		else
		{
			$result = 0;
		}

		unset($temp);

		return $result;

	}

	function notifyEngineerAdmin($memberID) {
		var_dump($memberID);
	}

	function engineerCalendar()
	{
		$member_id = ee()->TMPL->fetch_param('member_id');
		if($member_id == "")
		{
			$member_id = ee()->session->userdata('member_id');
		}
		ee()->db->select('*');
		ee()->db->from('wc_engineer_availability');
		ee()->db->where('member_id', $member_id);
		$result = ee()->db->get()->result_array();

		$temp = array();

		if (is_array($result) && count($result))
		{
			for ($i=0; $i < count($result); $i++) 
			{ 
				$temp[date('Y-m-d', strtotime($result[$i]['cal_date']))] = array(
					'start' => date('Y-m-d', strtotime($result[$i]['cal_date'])),
					'overlap' => false,
					'rendering' => 'background',
					'className' => $result[$i]['status'],
					'orderID' => $result[$i]['order_id'],
				);
			}			
		}

		$entryData = false;
		ee()->db->select('entry_id');
		ee()->db->from('channel_data_field_257');
		ee()->db->where('field_id_257', $member_id);
		$zipEntry = ee()->db->get();
		if($zipEntry->num_rows)
		{
			$zipEntry = $zipEntry->result_array();
			$entries = array();
			foreach ($zipEntry as $key => $value)
			{
				$entries[] = $value['entry_id'];
			}

			if(count($entries) > 0)
			{
				ee()->db->select('*');
				ee()->db->from('channel_data_field_101');
				ee()->db->where_in('entry_id', $entries);
				$installs = ee()->db->get();

				foreach($installs->result() as $key => $value)
				{
					$date = date('Y-m-d', $value->field_id_101);
					if(isset($temp[$date]))
					{
						$temp[$date] = array(
							'start' 	=> $date,
							'overlap' 	=> false,
							'rendering' => 'background',
							'className' => 'pending',
							'orderID' 	=> $value->entry_id
						);
					}
				}
			}
		}
		
		$temp = array_values($temp);
		$result = json_encode($temp);
		unset($temp);

		return $result;
	}

	/**
	* Get action ID from method
	* @param $method (To find the action ID of perticular method)
	* @return Action ID
	**/
    function getActionID($method)
    {

    	$this->db->limit(1);
    	$this->db->select('action_id');
    	$this->db->from('actions');
    	$this->db->where('method', $method);

    	return $this->db->get()->row("action_id");

	}
	
	function getEngineers($companyID)
	{
		
		ee()->db->select('md_28.m_field_id_28');
		ee()->db->from('member_data_field_28 md_28');
		ee()->db->where('md_28.member_id', $companyID);
		ee()->db->limit(1);

		$companyNames = ee()->db->get();
		$companyName = $companyNames->row('m_field_id_28');

		/* get list of engineers for company */
		ee()->db->distinct();
		ee()->db->select('md_58.m_field_id_58,m.email,m.group_id,md_58.member_id,md_28.m_field_id_28');
		ee()->db->from('members m');

		ee()->db->join('member_data_field_58 as md_58', 'md_58.member_id = m.member_id');
		ee()->db->join('member_data_field_28 as md_28', 'md_28.member_id = m.member_id');

		ee()->db->where('md_28.m_field_id_28', $companyName);

		$finalResult = ee()->db->get();

		if($finalResult->num_rows == 0)
		{
			return false;
		}

		$engineers = array();

		$i = 0;
		foreach ($finalResult->result() as $mk => $mv)
		{
			$engineers[$i]['engineer_id'] 	= $mv->member_id;
			$engineers[$i]['name'] 	= $mv->m_field_id_58;
			$engineers[$i]['email'] 	= $mv->email;
			$engineers[$i]['group'] = $mv->group_id;
			$i++;
		}
		
		return $engineers;
	}

    function newJobOffers($company_id = "")
    {
    	/* get list of postcodes */
		ee()->db->distinct();
		ee()->db->select('cd_99.field_id_99,ct.entry_id,cdg_98.col_id_50');
		ee()->db->from('channel_titles ct');
		
		ee()->db->join('channel_data_field_99 as cd_99', 'cd_99.entry_id = ct.entry_id');
		ee()->db->join('channel_grid_field_98 as cdg_98', 'cdg_98.entry_id = ct.entry_id' ,'left');
		
		ee()->db->where('ct.channel_id', "11");
		ee()->db->where('cd_99.field_id_99 != ', "");

		$resultZipcode = ee()->db->get();

		if ($resultZipcode->num_rows == 0)
		{
			return false;
		}

		$zipcodes = array();
		if ($company_id) {
			$memberID = $company_id;
		} else {
			$memberID = ee()->session->userdata('member_id');
		}
		
		foreach ($resultZipcode->result_array() as $rk => $rv)
		{
			if (in_array($memberID, explode('|', $rv['field_id_99'])))
			{
				$zipcodes[] = $rv['col_id_50'];
			}
		}
		
		if(empty($zipcodes))
		{
			return false;
		}

		$resultEntryID = array();
		/* get entry_id of available engineers in given postcode */		
		ee()->db->select('ct.entry_id, ct.title, cf_51.field_id_51, cf_187.field_id_187');
		ee()->db->from('channel_titles ct');
		ee()->db->join('channel_data_field_51 as cf_51', 'ct.entry_id = cf_51.entry_id');
		ee()->db->join('channel_data_field_187 as cf_187', 'ct.entry_id = cf_187.entry_id');
		// ee()->db->where_in('cf_51.field_id_51', $zipcodes);
		foreach ($zipcodes as $zk => $zv)
		{
			if ($zv == trim($zv) && strpos($zv, ' ') !== false)
			{
				$temp = explode(' ', $zv);
				$zv = $temp[0];
			}

			ee()->db->or_like('cf_51.field_id_51', $zv, 'after');
		}

		$tempResultEntryID = ee()->db->get();
		if($tempResultEntryID->num_rows == 0)
		{
			return false;
		}
		
		foreach ($tempResultEntryID->result_array() as $key => $val)
		{
			// var_dump($val['entry_id']);
			$check = array();
			if($val['field_id_187'] != '') {
				$check = explode('|', $val['field_id_187']);
			}

			if(empty($check) || ! in_array($memberID, $check))
			{
				$resultEntryID[] = $val['entry_id'];
			}
		}

		ee()->db->select('ct.entry_id, ct.title, cf_257.field_id_257');
		ee()->db->from('channel_titles ct');
		ee()->db->join('channel_data_field_257 as cf_257', 'ct.entry_id = cf_257.entry_id');
		ee()->db->where_in('ct.entry_id', $resultEntryID);
		$tempEngJobs = ee()->db->get();

		$selectedJob = array();

		if($tempEngJobs->num_rows() > 0) 
		{
			foreach ($tempEngJobs->result_array() as $row)
			{
				$check1 = array();
				if($row['field_id_257'] != '') {
					$check1 = explode('|', $row['field_id_257']);
				}

				if(in_array($memberID, $check1))
				{
					$selectedJobs[] = $row['entry_id'];
				} 
			}
		}
		if(!isset($selectedJobs)) {
			return false;
		} else {
			return implode('|', $selectedJobs);
		}
	}

    function acceptJob($data)
    {
    	$updateData = array(
    		'status' => 'confirm',
    		'order_id' => $data['order_id'],
    	);

    	ee()->db->where('member_id', $data['company_id']);
		ee()->db->where('cal_date', date('Ymd', $data['cal_date']));
		ee()->db->where('appointments', 'appointments' <= 1 );
    	ee()->db->set($updateData);
    	ee()->db->update('wc_engineer_availability');
		unset($updateData);
		
		if(ee()->db->affected_rows() != 1)
    	{
			ee()->db->select('appointments, order_id, status' );
			ee()->db->from('wc_engineer_availability');
			ee()->db->where('member_id', $data['company_id']);
			ee()->db->where('cal_date', date('Ymd', $data['cal_date']));
			$query = ee()->db->get();
			$ret = $query->row();
			$appointments = $ret->appointments;
			$newAppointments = $appointments - 1;
			$apptCount = $newAppointments;
			$order_id = $ret->order_id;
			if ($order_id == "") {
				$order_id = $data['order_id'];
			} else {
				$order_id = $order_id . "|" . $data['order_id'];
			}

			if ($status == 'confirm') {
				return 0;
			}

			ee()->db->where('member_id', $data['company_id']);
			ee()->db->where('cal_date', date('Ymd', $data['cal_date']));
			ee()->db->set('order_id', $order_id);
			ee()->db->set('appointments', $apptCount);
			ee()->db->update('wc_engineer_availability');
		}

    	$updateData = array(
            'status_id' => '26',
            'status'    => 'Scheduled',
        );

        ee()->db->where('entry_id', $data['order_id']);
        ee()->db->set($updateData);
        ee()->db->update('channel_titles');
        unset($updateData);

        /*insert data into field_129*/
    	$insertData = array();
    	$insertData['entry_id'] = $data['order_id'];
    	$insertData['field_id_129'] = $data['company_id'];
    	ee()->db->insert('channel_data_field_129', $insertData);
    	unset($insertData);

        /*insert data into field_102*/
    	$insertData = array();
    	$insertData['entry_id'] = $data['order_id'];
    	$insertData['field_id_102'] = $data['engineer_id'];
    	ee()->db->insert('channel_data_field_102', $insertData);

    	if(ee()->db->affected_rows() != 1)
    	{
    		return 0;
    	}
    	else
    	{
    		return 'accepted';
    		/*return true;*/
    	}
	}

    function declineJob($data = array())
    {
    	
    	/*to change the status of entry from open to New starts*/
        $entryData = ee('Model')->get('ChannelEntry')
                    ->filter('entry_id', $data['order_id'])
                    ->first();

        if($entryData)
        {
        	$entryData->field_id_129 = '';
            $entryData->status_id = '24';
            $entryData->status = 'New';
            $entryData->save();
        }
        /*to change the status of entry from open to New ends*/

    	$insertData = array();

    	$insertData['entry_id'] = $data['order_id'];
    	$insertData['col_id_92'] = ee()->session->userdata('member_id');
    	$insertData['col_id_66'] = $data['decline_reason'];
    	$insertData['row_order'] = 0;

    	ee()->db->insert('channel_grid_field_114', $insertData);

    	$engineers = $entryData->field_id_187;
    	if($engineers == "")
    	{
    		$entryData->field_id_187 = ee()->session->userdata('member_id');
    	}
    	else
    	{
    		$entryData->field_id_187 = $engineers . "|" . ee()->session->userdata('member_id');
    	}
    	$entryData->save();

    	/* delete engineer from assigned engineer */
    	ee()->db->where(array('field_id_102' => ee()->session->userdata('member_id')));
		ee()->db->where(array('entry_id' => $data['order_id']));
    	ee()->db->delete('channel_data_field_102');

    	/*$temp = ee('Model')->get('ChannelEntry')
				->filter('entry_id', $data['order_id'])->first();
		$temp->save();*/
		/*$temp = ee('Model')->get('ChannelEntry')
				->filter('entry_id', $data['order_id']);
		echo '<pre>';
		print_r($temp);
		exit();*/

    	/* to update engineer status to available */
		$updateData = array(
    		'status' => 'available',
    		'order_id' => NULL,
    	);

		ee()->db->where(array('member_id' => ee()->session->userdata('member_id')));
		ee()->db->where(array('order_id' => $data['order_id']));
    	ee()->db->set($updateData);
    	ee()->db->update('wc_engineer_availability');
		
    	return 'decline';
		/*if(ee()->db->affected_rows() != 1)
    	{
    		return false;
    	}
    	else
    	{
    	}*/
    }

    function isAcceptedJob($data = array())
    {
    	ee()->db->select('status');
    	ee()->db->from('wc_engineer_availability');
    	ee()->db->where('status', 'pending');
    	ee()->db->where('order_id', $data['order_id']);
    	ee()->db->where('cal_date', date('Ymd', $data['installation_date']));
    	ee()->db->where('member_id', ee()->session->userdata('member_id'));
    	$result = ee()->db->get();

    	if ($result->num_rows == 0)
		{
			return false;
		}

		return $result->num_rows;
    }

    function getDeclinedMemberIDs($entry_id = '')
    {
    	if($entry_id == '')
    	{
    		return '';
    	}

    	$entry_id = explode('|', $entry_id);
    	ee()->db->distinct();
    	ee()->db->select('*');
    	ee()->db->from('channel_grid_field_114');
    	ee()->db->where_in('entry_id', $entry_id);
    	$result = ee()->db->get();
    	
    	if($result->num_rows != 0)
    	{
    		$data = array();

	    	foreach ($result->result_array() as $rk => $rv)
	    	{
	    		if($rv['col_id_92'] != '')
	    		{
	    			$data[] = $rv['col_id_92'];
	    		}
	    	}

	    	return 'not '.implode('|', array_unique($data));

    	}
    	else
    	{
    		return '';
    	}

    }

    function reassignEngineer($data = array())
    {
    	
    	$zip = $data['zip'];

    	if ($zip == trim($zip) && strpos($zip, ' ') !== false)
		{
			$temp = explode(' ', $zip);
			$zip = $temp[0];
		}
		unset($temp);

		/*find entry_ids available in respected zip*/
		ee()->db->select('entry_id');
		ee()->db->from('channel_grid_field_98');
		ee()->db->like('col_id_50', $zip, 'after');
		$entryResult = ee()->db->get();

		$entryIDs = array();
		foreach ($entryResult->result() as $ek => $ev)
		{
			$entryIDs[] = $ev->entry_id;
		}
		$entryIDs = array_unique($entryIDs);

		/*find member_ids assigned to respected entry_ids*/
		ee()->db->select('field_id_99');
		ee()->db->from('channel_data_field_99');
		ee()->db->where_in('entry_id', $entryIDs);
		$memberResult = ee()->db->get();
		$memberResult = $memberResult->result();
	
		if ($memberResult[0]->field_id_99 == trim($memberResult[0]->field_id_99) && strpos($memberResult[0]->field_id_99, '|') !== false)
		{
			$temp = explode('|', $memberResult[0]->field_id_99);
			$memberResult = $temp;
		}
		else
		{
			$memberResult = $memberResult[0]->field_id_99;
		}
		unset($temp);

		/*find member details from member_ids*/
    	ee()->db->select('m.member_id, m.username, m.email');
		ee()->db->from('members m');
		ee()->db->where('m.group_id', 7);
		ee()->db->where_in('member_id', $memberResult);
		$finalResult = ee()->db->get();

		if($finalResult->num_rows == 0)
		{
			return false;
		}

		$members = array();

		$i = 0;
		foreach ($finalResult->result() as $mk => $mv)
		{
			$members[$i]['member_id'] 	= $mv->member_id;
			$members[$i]['username'] 	= $mv->username;
			$members[$i]['email'] 		= $mv->email;
			$i++;
		}
		
		return $members;
    }

    function getEngineerInShippingZip($zip = '')
    {

    	if ($zip == trim($zip) && strpos($zip, ' ') !== false)
		{
			$temp = explode(' ', $zip);
			$zip = $temp[0];
		}
		unset($temp);

		ee()->db->distinct();
    	ee()->db->select('cdf_99.field_id_99');
    	ee()->db->from('channel_grid_field_98 cgf_98');
    	ee()->db->join('channel_data_field_99 cdf_99', 'cgf_98.entry_id = cdf_99.entry_id');
    	ee()->db->like('cgf_98.col_id_50', $zip, 'after');

    	$resultMemberIDs = ee()->db->get();

    	$memberID = array();

    	foreach ($resultMemberIDs->result() as $mk => $mv)
    	{
    		if ($mv->field_id_99 == trim($mv->field_id_99) && strpos($mv->field_id_99, '|') !== false)
    		{
    			$temp = explode('|', $mv->field_id_99);
    			foreach ($temp as $k => $v)
    			{
    				$memberID[] = $v;
    			}
    		}
    		else
    		{
    			$memberID[] = $mv->field_id_99;
    		}
    	}


    	ee()->db->select('m.email');
		ee()->db->from('members m');
		ee()->db->where_in('m.member_id', $memberID);
		$resultMemberEmails = ee()->db->get();

		$memberEmails = array();

		foreach ($resultMemberEmails->result() as $rk => $rv)
		{
			$memberEmails[]	= $rv->email;
		}

		return $memberEmails;
	}

    function submitForReview($data)
    {
    	$updateData = array(
            'status_id' => '28',
            'status'    => 'Under Review',
        );

        ee()->db->where('entry_id', $data['order_id']);
        ee()->db->set($updateData);
        ee()->db->update('channel_titles');
        unset($updateData);

        if(ee()->db->affected_rows() != 1)
    	{
    		return 0;
    	}
    	else
    	{
    		return 'submitted';
    	}
	}

	function changeEngineer($data)
	{
		$updateData = array(
            'field_id_102' => $data['engineer_id'],
		);
		
		ee()->db->where('entry_id', $data['order_id']);
        ee()->db->set($updateData);
        ee()->db->update('channel_data_field_102');
        unset($updateData);

        if(ee()->db->affected_rows() != 1)
    	{
    		return 0;
    	}
    	else
    	{
    		return 'submitted';
    	}
	}
	
	function setAreas($data)
	{
		$companyID = $data['company_id'];

		$insertData = array();
    	$insertData['member_id'] = $data['company_id'];
		$insertData['m_field_id_75'] = "1";

		ee()->db->insert('member_data_field_75', $insertData);

		$sql = ee()->db->query("UPDATE af76c7b9_live.exp_channel_data_field_99 SET field_id_99 = REPLACE(field_id_99,'$companyID','')");

		ee()->db->select('*');
		ee()->db->from('channel_data_field_99');
		ee()->db->where_in('entry_id', $data['entry_id']);
		$query = ee()->db->get();

		if($query->num_rows != 0)
		{
			foreach ($query->result_array() as $rk => $rv) {
			
				$engineers = $rv['field_id_99'];

				if($engineers == "")
				{
					$engineers = $data['company_id'];
				}
				else
				{
					$engineers = $engineers . "|" . $data['company_id'];
				}

				ee()->db->where_in('entry_id', $rv['entry_id']);
				ee()->db->set('field_id_99', $engineers);
				ee()->db->update('channel_data_field_99');
			}
		}

		if(ee()->db->affected_rows() != 1)
    	{
    		return 0;
    	}
    	else
    	{
    		return 'submitted';
    	}

	}

}

