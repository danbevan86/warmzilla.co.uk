<?php if (!defined('BASEPATH')) {exit('No direct script access allowed'); }

class Warmzilla_cp_lib
{

	public $fields;
	public $form_errors;
	public $error_prefix;
	public $error_suffix;
	public $config;

	/* Initialize constructor */
	public function __construct()
	{
		/* Needful model classes */
		ee()->load->model('warmzilla_cp_model', 'wcpModel');
		ee()->load->library('wcp_template_parse', null, 'tp');
		$this->config = include PATH_THIRD . 'warmzilla_cp/config.php';
	}

	function getCompanyEmail($company_id)
	{
		ee()->db->limit(1);
		ee()->db->select('email');
		ee()->db->from('members');
		ee()->db->where('member_id', $company_id);

		return ee()->db->get()->row("email");
	}

	function getEmailTemplate($template="")
	{
		if(isset($this->config['email_list'][$template]) && $this->config['email_list'][$template] != "")
		{
			$seg = explode('/', $this->config['email_list'][$template]);
			if(count($seg) != 2)
			{
				return false;
			}

			$template = ee('Model')->get('Template')
			->with('TemplateGroup')
			->filter('site_id', ee()->config->item('site_id'))
			->filter('TemplateGroup.group_name', $seg[0])
			->filter('template_name', $seg[1])->first();

			if($template)
			{
				return $template->template_data;
			}
			else
			{
				return false;
			}
			unset($template);
		}
		else
		{
			return false;
		}
	}


	function verifyPostCode($postcode, $flag = 0)
	{

		if($postcode == "")
		{
			if($flag == 1)
			{
				return json_encode(array());
			}
			else
			{
				return false;
			}
		}

		$postcode = explode(' ', $postcode);

		$ret = ee()->wcpModel->verifyPostCode($postcode[0], $flag);

		if($ret === false)
		{
			if($flag == 1)
			{
				/*return json_encode(array());*/
				return 0;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if($flag == 1)
			{
				return $ret;
			}
			else
			{
				return true;
			}
		}
	}

	function notifyEngineerAdmin($memberID) {
		$globalVariables = array(
			'global_member_id' => $memberID,
		);

		$messageBody    = ee()->wcp->getEmailTemplate("engineer_added");
		$messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		$parsedHtml     = ee()->tp->template_parser($messageBody);
		$subject = 'Engineer information added';
		$to = "rhys@warmzilla.co.uk";
		$this->sendEmail($subject, $to, $parsedHtml['msg_body']);
		return ee()->wcpModel->notifyEngineerAdmin($memberID);
	}

	function getAvailableEngineers()
	{
		return ee()->wcpModel->verifyPostCode();
	}

	function engineerCalendar()
	{
		return ee()->wcpModel->engineerCalendar();
	}

	function getEngineers()
	{
		$companyId = ee()->session->userdata('member_id');
		$output = array();
		$resultData = ee()->wcpModel->getEngineers($companyId);
		if($resultData === false)
		{
			return ee()->TMPL->no_results;
		}

		foreach ($resultData as $rdk => $rdv)
		{
			$output[] = array(
				'engineer_id'		=> $rdv['engineer_id'],
				'name'		=> $rdv['name'],
				'email_address'		=> $rdv['email'],
				'group'		=> $rdv['group']
			);
		}

		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $output);
	}

	function newJobOffers($company_id  = "")
	{
		$zipcodes = ee()->wcpModel->newJobOffers($company_id);

		if($zipcodes === "" || $zipcodes == false)
		{
			return ee()->TMPL->no_results;
		}

		return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, array('wcp_entry_id' => $zipcodes));
	}

	function acceptJob($data = array())
	{
		if($data['type'] == 'accept')
		{
			if(ee()->wcpModel->acceptJob($data) == 'accepted')
			{
				if(isset($_POST['customer_email']) && $_POST['customer_email'] != '' && isset($_POST['engineer_email']) && $_POST['engineer_email'] != '')
				{
					$globalVariables = array(
						'global_order_id' => $data['order_id'],
					);

					/* Email to Customer */
					// $messageBody    = ee()->tp->message_body(195);
					$messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_user");
		            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		            $parsedHtml     = ee()->tp->template_parser($messageBody);
					$subject = 'Your WarmZilla installation date has been confirmed';
					$to = ee()->input->post('customer_email', true);
					$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

					/* Email to Company */;
					$messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_company");
		            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
					$parsedHtml     = ee()->tp->template_parser($messageBody);
					$subject = 'You have successfully accepted a new WarmZilla job!';
					$to = ee()->input->post('company_email', true);
					$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

					/* Email to Engineer*/
					// $messageBody    = ee()->tp->message_body(196);
					$messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_engineer");
		            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		            $parsedHtml     = ee()->tp->template_parser($messageBody);
					$subject = 'You have been assigned a new WarmZilla job!';
					$to = ee()->input->post('engineer_email', true);
					$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

					/* Email to Admin */
					$messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_admin");
		            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
		            $parsedHtml     = ee()->tp->template_parser($messageBody);
					$subject = 'Engineer have successfully accepted a new WarmZilla job!';
					$to = "rhys@warmzilla.co.uk";
					$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

				}
			}
			return ee()->wcpModel->acceptJob($data);
		}

		if($data['type'] == 'decline')
		{
			$ret = ee()->wcpModel->declineJob($data);
			if($ret === false)
			{
				return $ret;
			}

			$globalVariables = array(
				'global_username' => ee()->session->userdata('username'),
				'global_order_id' => $data['order_id'],
			);

			/*email to webmaster_email*/
			// $messageBody    = ee()->tp->message_body(190);
			$messageBody    = ee()->wcp->getEmailTemplate("declined_job_admin_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'An engineer has declined a job offer';
			// $to = ee()->config->item('webmaster_email');
			$to = "rhys@warmzilla.co.uk";

			$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

			/*email to engineer*/
			// $messageBody    = ee()->tp->message_body(197);
			$messageBody    = ee()->wcp->getEmailTemplate("declined_job_engineer_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

			$subject = 'You have declined a Job Offer for WarmZilla';
			$to = ee()->input->post('engineer_email', true);

			$this->sendEmail($subject, $to, $parsedHtml['msg_body']);

			return $ret;
		}
	}

	function isAcceptedJob($data = array())
	{
		return ee()->wcpModel->isAcceptedJob($data);
	}

	function getDeclinedMemberIDs($entry_id = '')
	{
		return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, array('declined_member_id' => ee()->wcpModel->getDeclinedMemberIDs($entry_id)));
		/*return ee()->wcpModel->getDeclinedMemberIDs($entry_id);*/
	}

	function reassignEngineer($data = array())
	{
		$output = array();
		$count = 1;
		$prefix = (ee()->TMPL->fetch_param('prefix') != "") ? ee()->TMPL->fetch_param('prefix') . ":" : "option:";

		$resultData = ee()->wcpModel->reassignEngineer($data);

		foreach ($resultData as $rdk => $rdv)
		{
			$output[] = array(
				$prefix . 'member_id'		=> $rdv['member_id'],
				$prefix . 'username'		=> $rdv['username'],
				$prefix . 'email'			=> $rdv['email'],
				$prefix . 'count'			=> $count++,
				$prefix . 'total_results'	=> count($resultData),
			);
		}

		return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $output);
	}

	function getEngineerInShippingZip($zip = '')
	{
		$resultData = ee()->wcpModel->getEngineerInShippingZip($zip);
		return $resultData;
	}

	function sendEmail($subject, $emails, $message, $bcc = '')
	{

		ee()->load->library('email');
		ee()->email->initialize();

        ee()->email->mailtype = 'html';
        ee()->email->subject($subject);

		ee()->email->from("ask@warmzilla.co.uk", 'Warmzilla');
		ee()->email->bcc($bcc, 200);
		ee()->email->to($emails);

        ee()->email->message($message);

        if(ee()->email->Send())
        {
            return true;
        }
        else
        {
        	/*echo "<pre>" . $emails . "<br>";
        	print_r(ee()->email->print_debugger());
        	exit();*/
            return false;
        }
	}

	function getBoilerQuestionsArray($withKeys = false)
	{

		if(! (isset($_COOKIE['boilerQuestions']) && $_COOKIE['boilerQuestions'] != ""))
		{
			return false;
		}

		$boilerQuestions = json_decode($_COOKIE['boilerQuestions'], true);
		if( ( ! isset($boilerQuestions[count($boilerQuestions)-1]['final'])) || (isset($boilerQuestions[count($boilerQuestions)-1]['final']) && $boilerQuestions[count($boilerQuestions)-1]['final'] != "summary"))
		{
			return false;
		}

		$temp = array();
		$cnt = count($boilerQuestions);

		for ($i = 0; $i < $cnt; $i++)
		{
			if($boilerQuestions[$i]['title'] == "")
			{
				unset($boilerQuestions[$i]);
			}
			else
			{
				if(strpos($boilerQuestions[$i]['answer'], "<sup>"))
				{
					$boilerQuestions[$i]['answer'] = strip_tags($boilerQuestions[$i]['answer']);
					$boilerQuestions[$i]['maximum'] = "yes";
				}

				if(isset($_COOKIE['editQuestions']) && $_COOKIE['editQuestions'] != "")
				{
					$boilerQuestions[$i]['edit_post'] = "yes";
				}
				else
				{
					$boilerQuestions[$i]['edit_post'] = "no";
				}

				$temp[$boilerQuestions[$i]['entry']] = $boilerQuestions[$i];
			}
		}

		if($withKeys)
		{
			return $temp;
		}
		else
		{
			return $boilerQuestions;
		}

	}

	function submitForReview($data)
	{
		return ee()->wcpModel->submitForReview($data);
	}

	function changeEngineer($data)
	{
		return ee()->wcpModel->changeEngineer($data);
	}

	function setAreas($data)
	{
		return ee()->wcpModel->setAreas($data);
	}

	function admin_emails()
	{
		if($_SERVER['HTTP_HOST'] == 'warmzilla.test' || $_SERVER['HTTP_HOST'] == 'staging.warmzilla.co.uk') {
			return "dan.bevan@warmzilla.co.uk";
		} else {
			return "ask@warmzilla.co.uk,dan@warmzilla.co.uk,rhys@warmzilla.co.uk,dan.bevan@warmzilla.co.uk";
		}
	}
}