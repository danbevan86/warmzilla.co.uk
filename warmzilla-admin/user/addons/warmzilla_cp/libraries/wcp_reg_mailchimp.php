<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once PATH_THIRD . 'warmzilla_cp/libraries/MailChimp.php';
use \DrewM\MailChimp\MailChimp;
class Wcp_reg_mailchimp
{

    public $mailChimpKey;
    public $defaultList;
    public $config;
    public function __construct()
    {
        $this->config = include PATH_THIRD . 'warmzilla_cp/config.php';
        $this->mailChimpKey = $this->config['mailchimp_key'];
        $this->defaultList = $this->config['default_mailchimp_list'];
    }

    public function addMemberInList($email, $mergeFields = array(), $list = "")
    {
        $MailChimp = $this->setKeys();
        if($MailChimp === false)
        {
            return false;
        }

        if($list == "")
        {
            $list = $this->defaultList;
        }

        if($list == "")
        {
            return false;
        }

        $result = $MailChimp->post("lists/$list/members", [
            'email_address' => $email,
            'merge_fields'  => $mergeFields,
            'status'        => 'subscribed',
        ]);
        
        return true;
    }

    function setKeys()
    {
        if($this->mailChimpKey == "")
        {
            return false;
        }

        return new MailChimp($this->mailChimpKey);
    }
}