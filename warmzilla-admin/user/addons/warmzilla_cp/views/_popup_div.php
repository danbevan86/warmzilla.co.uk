<div id="sm-modal" class="modal-wrap hidden">
	<div class="modal">
		<div class="col-group snap">
			<div class="col w-16 last">
				<a class="m-close" href="#"></a>
				<div class="box">
					<h1><?= $title;?></h1>
					<div class="alert inline warn sm-alert1"></div>
					<div class="alert inline warn sm-alert2">
						<p>
							<a href="Javascript:void(0);" copy-content="" class="copy_clip">Copy to clipboard</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>