<?php  if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

require PATH_THIRD.'warmzilla_cp/config.php';
Class Warmzilla_cp_ext
{
    public $name            = WARMZILLA_NAME;
    public $version         = WARMZILLA_VER;
    public $description     = '';
    public $settings_exist  = 'n';
    public $docs_url        = WARMZILLA_DOC_URL;
    public $user_base       = '';
    public $settings        = array();
    public $required_by     = array('module');

    function __construct($settings = '')
    {
        /*Define settings*/
        $this->settings = $settings;

        /*Load helpful libraries*/
        ee()->load->library('warmzilla_cp_lib', null, 'wcp');
        ee()->load->library('wcp_template_parse', null, 'tp');

        ee()->load->dbforge();
    }

    function activate_extension()
    {
        //settings
        $this->settings = array();

        //set up the hooks
        $hooks = array(
            //cartthrob hook
            'cartthrob_add_to_cart_start' => 'hook_cartthrob_add_to_cart_start',
            'cartthrob_add_to_cart_end'   => 'hook_cartthrob_add_to_cart_end',
            'before_channel_entry_insert' => 'hook_before_channel_entry_insert',
            'cartthrob_on_authorize'      => 'hook_cartthrob_on_authorize',
            'cartthrob_add_extra_validation_errors'      => 'hook_cartthrob_add_extra_validation_errors',
            'after_channel_entry_save'    => 'job_request_to_engineer',
            'insert_comment_end'          => 'hook_insert_comment_end',
            'charge_post_payment'          => 'hook_charge_post_payment',
        );
        /*echo '<pre>';
        print_r($_POST);
        exit();*/

        foreach ($hooks as $hook => $method)
        {
            $this->registerHooks($hook, $method);
        }
    }

    function disable_extension()
    {
        ee()->db->where('class', __CLASS__);
        ee()->db->delete('extensions');
    }

    function update_extension()
    {
        $hooks = array(
            'charge_post_payment' => 'hook_charge_post_payment',
        );

        foreach ($hooks as $hook => $method)
        {
            $row = ee()->db->select('extension_id')->from('extensions')->where('hook', $hook)->where('class', 'Warmzilla_cp_ext')->get()->num_rows;
            if($row == 0)
            {
                $this->registerHooks($hook, $method);
            }
        }

        return true;
    }

    private function registerHooks($hook, $method)
    {
        //sessions end hook
        $data = array(
            'class'     => __CLASS__,
            'method'    => $method,
            'hook'      => $hook,
            'settings'  => serialize( $this->settings ),
            'priority'  => 10,
            'version'   => $this->version,
            'enabled'   => 'y'
        );
        ee()->db->insert( 'extensions', $data );
    }

    function hook_cartthrob_add_to_cart_start()
    {
        if (! ee()->cartthrob->cart->is_empty())
        {
            ee()->cartthrob->cart->clear()->save();
        }

    }

    function hook_cartthrob_add_to_cart_end()
    {
        if(isset($_POST['item_options']['trv_variations']) && $_POST['item_options']['trv_variations'] != '')
        {
            // setcookie('trv_variations', $_POST['item_options']['trv_variations'], time() + (86400 * 30), "/");
        }
    }

    function hook_cartthrob_add_extra_validation_errors()
    {
        // return "test";
        if(isset($_POST['use_billing_info']))
        {
            if($_POST['use_billing_info'] == "1")
            {
                $zip = "zip";
            }
            else
            {
                $zip = "shipping_zip";
            }
        }

        if(isset($_POST[$zip]))
        {
            if(ee()->wcp->verifyPostCode($_POST[$zip]) !== true)
            {
                return array($zip => 'No Engineers are available on given Postcode.');
            }
        }

        return false;

    }

    function hook_cartthrob_on_authorize()
    {
        $entry_id = ee()->cartthrob->cart->order('entry_id');
        $boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);
        $global_boiler_questions = array();

        /*to change the status of entry from open to New starts*/
        $entryData = ee('Model')->get('ChannelEntry')
                    ->fields('entry_id', 'status', 'status_id' , 'field_id_100')
                    ->filter('entry_id', $entry_id)
                    ->first();
        if($entryData) {
            $entryData->status_id = '24';
            $entryData->status = 'New';
            $entryData->field_id_100 = '';
            $entryData->save();

            if($boilerQuestions === false)
            {
                /*
                    // New fields created in Orders channel and ID of those fields.
                    // Replace IDs when create fields in live site
                    boiler_questions_presaved : 291
                */
                $boilerQuestions = isset($entryData->field_id_291) ? json_decode($entryData->field_id_291, true) : false;
            }
        }

        if($boilerQuestions !== false)
        {
            foreach (ee()->cartthrob->cart->items() as $item)
            {
                $productID = $item->product_id();
                $productEntry = ee('Model')->get('ChannelEntry')
                        ->fields('entry_id', 'field_id_2', 'field_id_14' , 'field_id_139', 'field_id_140', 'field_id_138', 'field_id_141', 'field_id_142')
                        ->filter('entry_id', $productID)
                        ->first();

                $boilerPrices = array(
                    array(
                        'heading'   => "Boiler Price",
                        'data'      => $productEntry->field_id_2
                    )
                );

                foreach ($boilerQuestions as $key => $value)
                {
                    if(isset($value['incboilerprice']) && $value['incboilerprice'] != "" && is_numeric($value['incboilerprice']))
                    {
                        $boilerPrices[] = array(
                            'heading'   => $value['title'] . ' (' . $value['answer'] . ')',
                            'data'      => $value['incboilerprice']);
                    }
                }

                $plumpkitPriceInc   = $productEntry->field_id_139;
                $backboilerPriceInc = $productEntry->field_id_140;
                $backboilerInstallInc = $productEntry->field_id_142;

                if(isset($boilerQuestions['124']) && strtolower($boilerQuestions['124']['answer']) == "no" || strtolower($boilerQuestions['124']['answer']) == "unsure" || isset($boilerQuestions['125']) && strtolower($boilerQuestions['125']['answer']) == "yes" && $plumpkitPriceInc != "" && is_numeric($plumpkitPriceInc))
                {
                    $boilerPrices[] = array(
                        'heading'   => 'Plume Kit Variation',
                        'data'      => $plumpkitPriceInc
                    );
                }

                if(isset($boilerQuestions['104']) && strtolower($boilerQuestions['104']['answer']) == "back boiler" && $backboilerPriceInc != "" && is_numeric($backboilerPriceInc))
                {
                    $boilerPrices[] = array(
                        'heading'   => 'Back boiler - Combi',
                        'data'      => $backboilerPriceInc
                    );
                }

                $itemOptions = $item->item_options();
                if(isset($itemOptions['trv_variations']) && $itemOptions['trv_variations'] != "")
                {

                    $insertData = array(
                        'entry_id'      => $entry_id,
                        'field_id_185'  => $itemOptions['trv_variations'],
                    );
                    ee()->db->insert('channel_data_field_185', $insertData);
                    unset($insertData);

                    $trfModifier = $productEntry->field_id_14;
                    if($trfModifier != "")
                    {
                        $trfModifier = unserialize(base64_decode($trfModifier));
                        if(is_array($trfModifier) && count($trfModifier))
                        {
                            $trfPrice = "";
                            for ($i = 0; $i < count($trfModifier); $i++)
                            {
                                if($trfModifier[$i]['option_value'] == $itemOptions['trv_variations'])
                                {
                                    $boilerPrices[] = array(
                                        'heading'   => 'Thermostatic Radiator Valves (x' . $itemOptions['trv_variations'] . ')',
                                        'data'      => $trfModifier[$i]['price']);
                                }
                            }
                        }
                    }
                }

                for ($i = 0; $i < count($boilerPrices); $i++)
                {
                    $insert = array(
                        'entry_id' => $entry_id,
                        'row_order' => ($i +1),
                        'fluid_field_data_id' => 0,
                        'col_id_90' => $boilerPrices[$i]['heading'],
                        'col_id_91' => $boilerPrices[$i]['data'],
                    );

                    ee()->db->insert('channel_grid_field_186', $insert);
                }

                /*Calclation of Installer fees*/
                $boilerPrices = array();
                if($productEntry->field_id_138 != "" && $productEntry->field_id_138 != 0)
                {
                    $boilerPrices[] = array(
                        'heading'   => "Installation Fee",
                        'data'      => $productEntry->field_id_138
                    );
                }

                foreach ($boilerQuestions as $key => $value)
                {
                    if(isset($value['incinstallcharge']) && $value['incinstallcharge'] != "" && is_numeric($value['incinstallcharge']))
                    {
                        $boilerPrices[] = array(
                            'heading'   => $value['title'] . ' (' . $value['answer'] . ')',
                            'data'      => $value['incinstallcharge']
                        );
                    }
                }

                if(isset($itemOptions['trv_variations']) && $itemOptions['trv_variations'] != "" && is_numeric($itemOptions['trv_variations']))
                {
                    $trv = ($productEntry->field_id_141 == "" || $productEntry->field_id_141 == 0) ? 0 : $productEntry->field_id_141;
                    if($trv > 0)
                    {
                        $boilerPrices[] = array(
                            'heading'   => 'TRV Installation fees (' . $itemOptions['trv_variations'] . ')',
                            'data'      => $itemOptions['trv_variations'] * $trv
                        );
                    }
                }

                if(isset($boilerQuestions['104']) && strtolower($boilerQuestions['104']['answer']) == "back boiler" && $backboilerInstallInc != "" && is_numeric($backboilerInstallInc) && $backboilerInstallInc > 0)
                {
                    $boilerPrices[] = array(
                        'heading'   => 'Back boiler - Combi',
                        'data'      => $backboilerInstallInc
                    );
                }

                if(is_array($boilerPrices) && count($boilerPrices))
                {
                    for ($i = 0; $i < count($boilerPrices); $i++)
                    {
                        $insert = array(
                            'entry_id' => $entry_id,
                            'row_order' => ($i +1),
                            'fluid_field_data_id' => 0,
                            'col_id_86' => $boilerPrices[$i]['heading'],
                            'col_id_87' => $boilerPrices[$i]['data'],
                        );

                        ee()->db->insert('channel_grid_field_143', $insert);
                    }
                }

            }
        }

        if(isset($entry_id) && $boilerQuestions !== false)
        {
            unset($insert);
            $i = 1;
            foreach ($boilerQuestions as $key => $value)
            {
                if($value['selection'] != "")
                {
                    $insert = array(
                        'entry_id' => $entry_id,
                        'row_order' => $i++,
                        'fluid_field_data_id' => 0,
                        'col_id_51' => isset($value['title']) ? $value['title'] : "",
                        'col_id_52' => isset($value['answer']) ? $value['answer'] : "",
                        'col_id_53' => isset($value['incboilerprice']) ? $value['incboilerprice'] : "",
                        'col_id_54' => isset($value['incinstallcharge']) ? $value['incinstallcharge'] : "",
                    );

                    ee()->db->insert('channel_grid_field_100', $insert);

                    $global_boiler_questions[] = array(
                    'g_boiler_questions:question'                           => $insert['col_id_51'],
                    'g_boiler_questions:answer'                             => $insert['col_id_52'],
                    'g_boiler_questions:increment_in_boiler_price'          => $insert['col_id_53'],
                    'g_boiler_questions:increment_in_installation_charge'   => $insert['col_id_54'],
                    );

                }
            }

            if(count($global_boiler_questions) > 0)
            {
                for ($i=0; $i < count($global_boiler_questions); $i++)
                {
                    $global_boiler_questions[$i]['g_boiler_questions:count'] = ($i + 1);
                }
            }

            /*echo '<pre>';
            print_r($insertData);
            exit();

            $entry = ee('Model')->make('ChannelEntry', $entry_id);
            $entry->set($insertData);
            $entry->save();*/

            $globalVariables = array(
                'global_customer_order_id'      => $entry_id,
                'g_boiler_questions:total_rows' => count($global_boiler_questions),
                'g_boiler_questions'            => $global_boiler_questions,
            );

            // $messageBody    = ee()->tp->message_body(192);
            $messageBody    = ee()->wcp->getEmailTemplate("order_placed_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

            $subject = 'WarmZilla has received your order!';
            $to = ee()->cartthrob->cart->order('email_address');
            $bcc = "warmzilla.co.uk+e8eefd39a6@invite.trustpilot.com";
            $message = $parsedHtml;
            ee()->wcp->sendEmail($subject, $to, $message['msg_body'], $bcc);

            // $messageBody    = ee()->wcp->getEmailTemplate("paid_invoice");
            // $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            // $parsedHtml     = ee()->tp->template_parser($messageBody);

            // $subject = 'Customer Invoice';
            // $to = ee()->cartthrob->cart->order('email_address');
            // $message = $parsedHtml;
            // ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
            // ee()->wcp->sendEmail($subject, 'accounts@warmzilla.co.uk', $message['msg_body']);

            $customData = ee()->cartthrob->cart->custom_data();
            if(isset($customData['installation_date']) && $customData['installation_date'] != "" && $customData['engineer_found_in_area'])
            {
                $globalVariables = array(
                    'global_order_id' => $entry_id,
                );

                /* This is the email to the engineer in the area shown in the checkout */
                $messageBody    = ee()->wcp->getEmailTemplate("job_request_to_engineer");
                $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
                $parsedHtml     = ee()->tp->template_parser($messageBody);

                $subject = 'You have a new job offer from WarmZilla!';

                $to = ee()->wcp->getCompanyEmail($customData['engineer_found_in_area']); // gets company email address
                $message = $parsedHtml;
                ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
            }

            $globalVariables = array('global_order_id' => $entry_id);
            $messageBody    = ee()->wcp->getEmailTemplate("boiler_purchase_admin");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);
            $subject        = 'Boiler Purchase #' . $entry_id;
            $to = ee()->wcp->admin_emails();
            $message = $parsedHtml;
            ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
        }


        if(isset($_COOKIE['addUserNewsletter']) && $_COOKIE['addUserNewsletter'] == "yes")
        {
            $customerInfo = ee()->cartthrob->cart->customer_info();
            $customData = ee()->cartthrob->cart->custom_data();

            $email = $customerInfo['email_address'];
            $mergeFields = array(
                "FNAME"     => $customerInfo['first_name'],
                "LNAME"     => $customerInfo['last_name'],
                "EMAIL"     => $email ,
                "PHONE"     => $customerInfo['phone'],
                "INST_DATE" => isset($customData['installation_date']) ? date("d-m-Y H:i:s", $customData['installation_date']) : "",
                "ADDRESS"   => array(
                    "addr1"     => $customerInfo['address'],
                    "addr2"     => $customerInfo['address2'],
                    "city"      => $customerInfo['city'],
                    "state"     => $customerInfo['state'],
                    "zip"       => $customerInfo['zip'],
                    "country"   => $customerInfo['country'],
                    )
                );
            ee()->load->library('wcp_reg_mailchimp', null, 'mailchimp');
            ee()->mailchimp->addMemberInList($email, $mergeFields);
            unset($_COOKIE['addUserNewsletter']);
        }
    }

    function job_request_to_engineer()
    {
        if(isset($_POST['job_request_to_engineer']))
        {
            $globalVariables = array('global_order_id' => ee()->input->post('entry_id', true));

            /*email to all related engineers*/
            // $messageBody    = ee()->tp->message_body(189);
            $messageBody    = ee()->wcp->getEmailTemplate("job_request_to_engineer");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

            $subject = 'You have a new job offer from WarmZilla!';
            $emails = ee()->wcp->getEngineerInShippingZip($_POST['shipping_zip']);
            $message = $parsedHtml;

            if (is_array($emails))
            {
                foreach ($emails as $ek => $ev)
                {
                    ee()->wcp->sendEmail($subject, $ev, $message['msg_body']);
                }
            }
        }

        if(isset($_POST['change_installation_date']))
        {
            $globalVariables = array(
                'global_order_id' => ee()->input->post('entry_id', true),
                'global_installation_date' => ee()->input->post('installation_date', true),
            );

            /*email to website_admin*/
            // $messageBody    = ee()->tp->message_body(199);
            $messageBody    = ee()->wcp->getEmailTemplate("change_installation_date_admin_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

            $subject = 'Customer changed Installation date';
            // $to = ee()->config->item('webmaster_email');
            $to = ee()->wcp->admin_emails();
            ee()->wcp->sendEmail($subject, $to, $parsedHtml['msg_body']);

            /*email to engineer*/

            $to = ee()->input->post('engineer_email', true);
            if($to != "")
            {
                $subject = 'Customer changed Installation date';
                // $messageBody    = ee()->tp->message_body(201);
                $messageBody    = ee()->wcp->getEmailTemplate("change_installation_date_engineer_email");
                $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
                $parsedHtml     = ee()->tp->template_parser($messageBody);
                ee()->wcp->sendEmail($subject, $to, $parsedHtml['msg_body']);
            }

            /*email to customer*/
            // $messageBody    = ee()->tp->message_body(200);
            $messageBody    = ee()->wcp->getEmailTemplate("change_installation_date_customer_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);

            $subject = 'Your WarmZilla Installation Date has been changed';
            $to = ee()->input->post('customer_email', true);

            ee()->wcp->sendEmail($subject, $to, $parsedHtml['msg_body']);
        }

        if(isset($_POST['reassign_engineer']))
        {

            $orderID = ee()->input->post('reassigned_order_id', true);
            ee()->db->where('order_id', $orderID);
            ee()->db->update('wc_engineer_availability', array('status' => 'available', 'order_id' => null));

            if(isset($_POST['installation_date']) && $_POST['installation_date'] != "")
            {

                if($_POST['assigned_engineer_for_this_job']['data'][0] == "")
                {
                    /*to change the status of entry from open to New starts*/
                    ee()->db->where('entry_id', $orderID);
                    ee()->db->update('channel_titles', array('status' => 'New', 'status_id' => '24'));

                    ee()->db->where('entry_id', $orderID);
                    ee()->db->update('channel_data_field_102', array('field_id_102' => ''));

                }
                else
                {
                    ee()->db->where('entry_id', $orderID);
                    ee()->db->update('channel_titles', array('status' => 'Scheduled', 'status_id' => '26'));

                    ee()->db->where('entry_id', $orderID);
                    ee()->db->update('channel_data_field_102', array('field_id_102' => $_POST['assigned_engineer_for_this_job']['data'][0]));

                    $installation_date = strtotime(str_replace("/", "-", $_POST['installation_date']));

                    ee()->db->select('id');
                    ee()->db->from('wc_engineer_availability');
                    ee()->db->where('member_id', $_POST['assigned_engineer_for_this_job']['data'][0]);
                    ee()->db->where('cal_date', date('Ymd', $installation_date));
                    $get = ee()->db->get();

                    if($get->num_rows)
                    {
                        $updateData = array(
                            'status' => 'confirm',
                            'order_id' => $orderID,
                        );
                        ee()->db->where('id', $get->row('id'));
                        ee()->db->update('wc_engineer_availability', $updateData);
                    }
                    else
                    {
                        $insert = array(
                            'member_id' => $_POST['assigned_engineer_for_this_job']['data'][0],
                            'cal_date' => date('Ymd', $installation_date),
                            'status' => 'confirm',
                            'order_id' => $orderID
                            );
                        ee()->db->insert('wc_engineer_availability', $insert);
                    }
                    unset($updateData);
                }

            }

            $emailID = ee()->input->post('reassign_engineer_email', true);
            if($emailID != "")
            {
                $globalVariables = array(
                    'global_reassign_engineer_email' => $emailID,
                    'global_reassigned_order_id'     => $orderID,
                );

                // $messageBody    = ee()->tp->message_body(191);
                $messageBody    = ee()->wcp->getEmailTemplate("reassign_engineer_email");
                $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
                $parsedHtml     = ee()->tp->template_parser($messageBody);

                $subject = 'You have been assigned a new WarmZilla Job';
                $to = $emailID;
                $message = $parsedHtml;
                ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
            }


            $globalVariables = array(
                'global_order_id' => $orderID,
            );

            $entryData = ee('Model')->get('ChannelEntry', $orderID)->fields('field_id_31')->first();
            if($entryData)
            {
                /*email to customer*/
                // $messageBody    = ee()->tp->message_body(195);
                $messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_user");
                $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
                $parsedHtml     = ee()->tp->template_parser($messageBody);

                $subject = 'Your WarmZilla installation date has been confirmed';
                $to = $entryData->field_id_31;

                ee()->wcp->sendEmail($subject, $to, $parsedHtml['msg_body']);
            }
            unset($entryData);

            $messageBody    = ee()->wcp->getEmailTemplate("job_accepted_email_admin");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);
            $subject = 'Engineer have been reassigned a new Job';
            $to = "rhys@warmzilla.co.uk";
            ee()->wcp->sendEmail($subject, $to, $parsedHtml['msg_body']);
        }
    }

    function hook_insert_comment_end()
    {
        $entry_id = ee()->input->post('entry_id', true);
        $email = ee()->input->post('email', true);
        $email_array = array($email);
        $entryData = ee('Model')->get('ChannelEntry', $entry_id)->fields('field_id_102')->first();

        $engineer_email = false;
        if($entryData->field_id_102 != "")
        {
            $member = ee('Model')->get('Member', $entryData->field_id_102)->with('MemberGroup')->first();
            if($member)
            {
                $email_array[] = $member->email;
                $engineer_email = $member->email;
            }
        }
        unset($entryData);
        unset($member);

        $entryData = ee('Model')->get('ChannelEntry', $entry_id)->fields('field_id_129')->first();

        $company_email = false;
        if($entryData->field_id_129 != "")
        {
            $member = ee('Model')->get('Member', $entryData->field_id_129)->with('MemberGroup')->first();
            if($member)
            {
                $email_array[] = $member->email;
                $company_email = $member->email;
            }
        }

        unset($entryData);
        unset($member);

        /*Mail to person who commented*/
        $globalVariables = array(
            'global_order_id' => $entry_id,
            'global_name' => ee()->input->post('name', true),
            'global_comment' => ee()->input->post('comment', true),
        );
        $to = ee()->input->post('email', true);
        $globalVariables['global_group_id'] = ee()->db->get_where('members', array('email'=>$to))->row('group_id');
        // $messageBody    = ee()->tp->message_body(203);
        $messageBody    = ee()->wcp->getEmailTemplate("comment_user_email");
        $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
        $parsedHtml     = ee()->tp->template_parser($messageBody);
        $subject = 'Warmzilla have added a comment to job #' . $entry_id;
        $message = $parsedHtml;
        ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

        // /*email to other admins*/
        // $to = ee()->wcp->admin_emails();
        // $globalVariables['global_group_id'] = "6";
        // // $messageBody    = ee()->tp->message_body(202);
        // $messageBody    = ee()->wcp->getEmailTemplate("comment_admin_email");
        // $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
        // $parsedHtml     = ee()->tp->template_parser($messageBody);
        // $subject = 'There is a new comment on a WarmZilla job';
        // $message = $parsedHtml;
        // ee()->wcp->sendEmail($subject, $to, $message['msg_body']);

        if($engineer_email)
        {
            $to = $engineer_email;
            $globalVariables['global_group_id'] = '7';
            // $messageBody    = ee()->tp->message_body(202);
            $messageBody    = ee()->wcp->getEmailTemplate("comment_admin_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);
            $subject = 'Comment added #' . $entry_id;
            $message = $parsedHtml;
            ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
        }

        if($company_email)
        {
            $to = "rhys@warmzilla.co.uk";
            $globalVariables['global_group_id'] = '10';
            // $messageBody    = ee()->tp->message_body(202);
            $messageBody    = ee()->wcp->getEmailTemplate("comment_admin_email");
            $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
            $parsedHtml     = ee()->tp->template_parser($messageBody);
            $subject = 'Engineer has added a comment to job #' . $entry_id;
            $message = $parsedHtml;
            ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
        }

        ee()->db->distinct();
        ee()->db->select('author_id, name, email');
        ee()->db->from('comments');
        ee()->db->where('entry_id', $entry_id);
        ee()->db->where_not_in('email', $email_array);

        $get = ee()->db->get();
        if($get->num_rows)
        {
            $result = $get->result_array();
            foreach ($result as $key => $value)
            {
                /*email to other admins*/
                $to = "rhys@warmzilla.co.uk";
                $globalVariables['global_group_id'] = ee()->db->get_where('members', array('email'=>$to))->row('group_id');
                // $messageBody    = ee()->tp->message_body(202);
                $messageBody    = ee()->wcp->getEmailTemplate("comment_admin_email");
                $messageBody    = ee()->TMPL->parse_variables_row($messageBody, $globalVariables);
                $parsedHtml     = ee()->tp->template_parser($messageBody);
                $subject = 'Comment added #' . $entry_id;
                $message = $parsedHtml;
                ee()->wcp->sendEmail($subject, $to, $message['msg_body']);
            }
        }
    }

    function hook_charge_post_payment($type, $hook_data)
    {

        if(isset($hook_data['meta:entry_id']) && $hook_data['meta:entry_id'] != "")
        {
            /*
                Vendigo Deposit Paid ID : (290)
            */
            $productEntry = ee('Model')->get('ChannelEntry')
                ->fields('field_id_290')
                ->filter('entry_id', $hook_data['meta:entry_id'])
                ->first();
            if($productEntry)
            {
                $productEntry->status_id = '24';
                $productEntry->status = 'New';
                $productEntry->field_id_290 = $hook_data['hash'];
                $productEntry->save();
            }
        }

    }
}
?>