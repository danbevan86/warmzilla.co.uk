<?php
if (!defined('WARMZILLA_VER'))
{
	define('WARMZILLA_VER', '1.0.2');
	define('WARMZILLA_NAME', 'WarmZilla Control Panel');
	define('WARMZILLA_MOD_NAME', 'Warmzilla_cp');

	define('WARMZILLA_AUTHOR', 'ZealousWeb');
	define('WARMZILLA_AUTHOR_URL', 'http://www.zealousweb.com/');
	define('WARMZILLA_DOC_URL', "http://www.zealousweb.com/expressionengine/");

	define('WARMZILLA_MORE_INFO_URL', 'http://www.zealousweb.com/expressionengine/');
}

$config['warmzilla_version'] 	= WARMZILLA_VER;
$config['warmzilla_name'] 	= WARMZILLA_NAME;
$config['warmzilla_mod_name'] = WARMZILLA_MOD_NAME;
$config['warmzilla_doc_url'] 	= WARMZILLA_DOC_URL;
$config['mailchimp_key'] 	= "939ce7980d1334d56f8158f429e0e04e-us19";
$config['default_mailchimp_list'] 	= "21cba26252";

$config['job_request_to_engineer'] = '_email.group/job_request_to_engineer.html';
$config['email_list'] = array(
	'registration_email_template' 				=> '_email/registration-email-template', // 138
	'forgot_password_email_template' 			=> '_email/forgot-password-email-template', // 139
	'account_validation_notification'			=> '_email/account-validation-notification',
	'become_an_installer' 						=> '_email/become-an-installer', // 183
	'job_request_to_engineer' 					=> '_email/job_request_to_engineer', // 189
	'boiler_purchase_admin' 					=> '_email/boiler-purchase-admin',
	'boiler_finance_admin'						=> '_email/boiler-finance-admin',
	'declined_job_admin_email' 					=> '_email/declined-job-admin-email', // 190
	'reassign_engineer_email' 					=> '_email/reassign-engineer-email', // 191
	'reassign_engineer_email_old'				=> '_email/reassign-engineer-email-old-engineer',
	'reassign_engineer_email_customer'			=> '_email/reassign-engineer-email-customer',
	'reassign_engineer_email_company'			=> '_email/reassign-engineer-email-company',
	'reassign_engineer_email_admin'				=> '_email/reassign-engineer-email-admin',
	'order_placed_email' 						=> '_email/order-placed-email', // 192
	'email_reminder' 							=> '_email/email-reminder', // 193
	'email_reminder_to_admin' 					=> '_email/email-reminder-to-admin', // 194
	'job_accepted_email_user' 					=> '_email/job-accepted-email-user', // 195
	'job_accepted_email_engineer' 				=> '_email/job-accepted-email-engineer', // 196
	'job_accepted_email_company' 				=> '_email/job-accepted-email-company', // 196
	'job_accepted_email_admin' 					=> '_email/job-accepted-email-admin',
	'declined_job_engineer_email' 				=> '_email/declined-job-engineer-email', // 197
	'change_installation_date_admin_email' 		=> '_email/change-installation-date-admin-email', // 199
	'change_installation_date_customer_email' 	=> '_email/change-installation-date-customer-email', // 200
	'change_installation_date_engineer_email' 	=> '_email/change-installation-date-engineer-email', // 201
	'comment_admin_email' 						=> '_email/comment-admin-email', // 202
	'comment_user_email' 						=> '_email/comment-user-email', // 203
	'notify_admin_for_review' 					=> '_email/notify-admin-for-review', // 215
	'forgot_password_email_template_pin' 		=> '_email/forgot-password-email-template-pin', // 360
	'paid_invoice'								=> '_email/paid-invoice',
	'engineer_added'							=> '_email/engineer-added',
	'email_quote_follow_up'						=> '_email/email-quote-follow-up',
	'email_discount'							=> '_email/email-discount',
	'email_discount_follow_up'					=> '_email/email-discount-follow-up',
	'email_boiler_service'						=> '_email/email-boiler-service'
);

return $config;
?>