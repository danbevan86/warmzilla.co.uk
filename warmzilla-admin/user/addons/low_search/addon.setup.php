<?php

/**
 * Low Search config file
 *
 * @package        low_search
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-search
 * @copyright      Copyright (c) 2017, Low
 */

return array(
	'author'         => 'Low',
	'author_url'     => 'http://gotolow.com/',
	'docs_url'       => 'http://gotolow.com/addons/low-search',
	'name'           => 'Low Search',
	'description'    => 'Powerful site search and Find &amp; Replace utility',
	'version'        => '6.1.2',
	'namespace'      => 'Low\Search',
	'settings_exist' => TRUE
);
