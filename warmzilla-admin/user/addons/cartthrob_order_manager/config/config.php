<?php

$config['cartthrob_order_manager'] = [
    'other' => [],
    'fields' => [],
    'location_field' => [],
    'cp_menu' => 'yes',
    'cartthrob_order_manager_cp_menu_label' => 'Ecommerce',
    'show_product_admin' => false,
];
