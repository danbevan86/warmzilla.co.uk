<?=form_open(ee('CP/URL')->make('addons/settings/cartthrob_order_manager/om_sales_dashboard'), 'id="reports_filter"')?>

<?=$reports_filter?>
 	Report <?=form_dropdown('report', $reports, $current_report)?>
	<?=form_submit('', lang('refresh'), 'class="btn submit"')?>
</form>

<?php if ($current_report) : ?>
<div id="reports_view">
	<?=$view?>
</div>
<?php else : ?>
<?=$view?>
<?php endif; ?>

<?=$todays_orders?>

<div>
<?=$reports_date?>
 	<p>
	<?=lang('cartthrob_order_manager_date_start')?> 
	<input type="text" value="" class="datepicker" name="date_start" size="30" style="width:100px"/> 
	<?=lang('cartthrob_order_manager_date_finish')?> 
	<input type="text" value="" class="datepicker" name="date_finish" size="30" style="width:100px"/> 
	<?=form_submit('', lang('cartthrob_order_manager_date_range'), 'class="btn submit"')?>
	</p>
</form>
</div>
<?=$order_totals?>

<?=form_open(ee('CP/URL')->make('addons/settings/cartthrob_order_manager/om_sales_dashboard', array('save' => '1')))?>
	<?=$reports_list?>
	<?=form_submit('', lang('submit'), 'class="btn submit"')?>
<?=form_close()?>
</div>
