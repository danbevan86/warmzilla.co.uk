<?php

include_once PATH_THIRD . 'cartthrob_order_manager/config.php';

return [
    'author' => 'Foster Made',
    'author_url' => 'https://fostermade.co',
    'name' => 'CartThrob Order Manager',
    'description' => 'Order management system for CartThrob',
    'version' => CT_ORDER_MANAGER,
    'namespace' => '\\',
    'settings_exist' => true,
];
