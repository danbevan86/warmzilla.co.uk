<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cartthrob_order_manager_upd
{
    public $module_name;
    public $version;
    public $current;
    public $notification_events = [
        //"order_updated",
        //"order_completed",
        //"order_refunded",
        'tracking_added_to_order',
    ];
    private $mod_actions = [//	'tracker_action',
    ];
    private $mcp_actions = [
        'refund',
        'add_tracking_to_order',
        'delete_order',
        'update_order',
        'resend_email',
        'create_new_report',
        'run_report',
        'remove_report',
    ];
    private $fieldtypes = [];
    private $hooks = [
        ['cp_menu_array', 'cp_menu_array'],
        ['cartthrob_addon_register'],
        //		array('cartthrob_update_cart_end'),
        //		array('template_fetch_template'),
    ];
    private $tables = [
        'cartthrob_order_manager_settings' => [
            'site_id' => [
                'type' => 'int',
                'constraint' => 4,
                'default' => '1',
            ],
            '`key`' => [
                'type' => 'varchar',
                'constraint' => 255,
            ],
            'value' => [
                'type' => 'text',
                'null' => true,
            ],
            'serialized' => [
                'type' => 'int',
                'constraint' => 1,
                'null' => true,
            ],
        ],
        'cartthrob_order_manager_table' => [
            'id' => [
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true,
                'primary_key' => true,
            ],
            'member_id' => [
                'type' => 'int',
                'constraint' => 10,
                'null' => true,
            ],
            'track_event' => [
                'type' => 'int',
                'constraint' => 10,
                'null' => true,
            ],
        ],
        'cartthrob_order_manager_reports' => [
            'id' => [
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true,
                'primary_key' => true,
            ],
            'report_title' => [
                'type' => 'varchar',
                'default' => 'Order Report',
                'constraint' => 255,
            ],
            'type' => [
                'type' => 'varchar',
                'defalt' => 'order',
                'constraint' => 32,
            ],
            'settings' => [
                'type' => 'text',
                'null' => true,
            ],
        ],
        'cartthrob_products' => [
            'id' => [
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true,
                'primary_key' => true,
            ],
            'title' => [
                'type' => 'varchar',
                'constraint' => 255,
            ],
            'url_title' => [
                'type' => 'varchar',
                'constraint' => 65,
            ],
            'status' => [
                'type' => 'varchar',
                'default' => 'open',
                'constraint' => 20,
            ],
            'description' => [
                'type' => 'text',
                'null' => true,
            ],
            'sku' => [
                'type' => 'varchar',
                'constraint' => 65,
            ],
            'featured' => [
                'type' => 'tinyint',
                'default' => 0,
            ],
            'shipping' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'shippable' => [
                'type' => 'tinyint',
                'default' => 0,
            ],
            'weight' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'tax' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'taxable' => [
                'type' => 'tinyint',
                'default' => 0,
            ],
            'taxable' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'price' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'store_cost' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'sale_price' => [
                'type' => 'decimal',
                'constraint' => '10,4',
            ],
            'sale_start' => [
                'type' => 'int',
                'constraint' => 32,
            ],
            'sale_end' => [
                'type' => 'int',
                'constraint' => 32,
            ],
            'images' => [
                'type' => 'text',
                'null' => true,
            ],
            'item_options' => [
                'type' => 'text',
                'null' => true,
            ],
            'option_groups' => [
                'type' => 'text',
                'null' => true,
            ],
            'keywords' => [
                'type' => 'text',
                'null' => true,
            ],
        ],
    ];

    public function __construct()
    {
        $this->module_name = strtolower(str_replace(['_ext', '_mcp', '_upd'], '', __CLASS__));

        include PATH_THIRD . $this->module_name . '/config.php';
        $this->version = $config['version'];

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->add_package_path(PATH_THIRD . $this->module_name . '/');

        $this->params = [
            'module_name' => $this->module_name,
            'current' => $this->current,
            'version' => $this->version,
            'hooks' => $this->hooks,
            'fieldtypes' => $this->fieldtypes,
            'mcp_actions' => $this->mcp_actions,
            'mod_actions' => $this->mod_actions,
            'tables' => $this->tables,
            'notification_events' => $this->notification_events,
        ];

        ee()->load->library('mbr_addon_builder');
        ee()->mbr_addon_builder->initialize($this->params);
    }

    public function install()
    {
        return ee()->mbr_addon_builder->install($has_cp_backend = 'y', $has_publish_fields = 'n');
    }

    public function update($current = '')
    {
        return ee()->mbr_addon_builder->update($current);
    }

    public function uninstall()
    {
        return ee()->mbr_addon_builder->uninstall();
    }
}
