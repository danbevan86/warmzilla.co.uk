<?php

use CartThrob\Event;
use CartThrob\Transactions\TransactionState;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/* @TODO
 * if (ee()->cartthrob->store->config('save_orders') && ee()->cartthrob->store->config('orders_channel'))
 * this module shouldn't even run if orders aren't being saved
 */
/*
@NOTE @TODO

currently this requries the following additional fields to be set up

order_refund_id
order_shipping_note
order_tracking_number

Email Address
Subject
Order Complete
*/

class Cartthrob_order_manager_mcp
{
    public $required_settings = [];
    public $template_errors = [];
    public $templates_installed = [];
    public $extension_enabled = 0;
    public $module_enabled = 0;
    public $limit = '100';
    public $version;
    public $nav = [];
    public $no_nav = [];
    public $default_columns = [
        'row_id',
        'row_order',
        'order_id',
        'entry_id',
        'title',
        'quantity',
        'price',
        'price_plus_tax',
        'weight',
        'shipping',
        'no_tax',
        'no_shipping',
        'extra',
    ];
    public $order_fields = [
        'orders_billing_first_name',
        'orders_billing_last_name',
        'orders_billing_company',
        'orders_billing_address',
        'orders_billing_address2',
        'orders_billing_city',
        'orders_billing_state',
        'orders_billing_zip',
        'orders_country_code',
        'orders_shipping_first_name',
        'orders_shipping_last_name',
        'orders_shipping_company',
        'orders_shipping_address',
        'orders_shipping_address2',
        'orders_shipping_city',
        'orders_shipping_state',
        'orders_shipping_zip',
        'orders_shipping_country_code',
        'orders_customer_email',
        'orders_customer_phone',
        'orders_language_field',
        'orders_full_billing_address',
        'orders_full_shipping_address',
    ];
    public $total_fields = [
        'orders_total',
        'orders_tax',
        'orders_subtotal',
        'orders_shipping',
    ];
    public $params;
    public $cartthrob;
    public $store;
    public $cart;
    public $table = 'cartthrob_order_manager_table';
    private $module_name;
    private $currency_code = null;
    private $prefix = null;
    private $dec_point = null;
    private $remove_keys = ['name', 'submit', 'x', 'y', 'templates', 'XID', 'CSRF_TOKEN'];

    public function __construct()
    {
        $this->module_name = strtolower(str_replace(['_ext', '_mcp', '_upd'], '', __CLASS__));

        ee()->load->add_package_path(PATH_THIRD . $this->module_name . '/');

        include PATH_THIRD . $this->module_name . '/config.php';

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('cartthrob_loader');
        ee()->load->library('get_settings');
        ee()->load->library('number');
        ee()->load->helper('form');

        if (!$this->cartthrobEnabled()) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_cartthrob_must_be_installe'));
            ee()->functions->redirect(ee('CP/URL')->make(''));
        }

        if (!ee()->cartthrob->store->config('save_orders') && !ee()->cartthrob->store->config('orders_channel')) {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_orders_channel_must_be_configured')
            );
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/order_settings'));
        }
    }

    /**
     * @return bool
     */
    private function cartthrobEnabled()
    {
        $query = ee()->db->select('module_name')
            ->where_in('module_name', 'Cartthrob')
            ->get('modules');

        if ($query->result()) {
            $query->free_result();

            return true;
        }

        return false;
    }

    public function delete_products()
    {
        $this->initialize();

        $structure = [];

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [], $structure);
    }

    private function initialize()
    {
        $this->params['module_name'] = $this->module_name;
        $this->params['skip_extension'] = true;
        $this->params['nav'] = [
            'om_sales_dashboard' => ['om_sales_dashboard' => ee()->lang->line('om_sales_dashboard')],

            //// ORDERS
            'view' => ['view' => ee()->lang->line('cartthrob_order_manager_orders_list')],
            'order_report' => ['order_report' => ee()->lang->line('order_report')],
            'edit' => ['edit' => ee()->lang->line('edit')],
            'delete' => ['delete' => ee()->lang->line('delete')],

            //// PRODUCTS
            'view_products' => ['view_products' => ee()->lang->line('view_products')],
            'add_products' => ['add_products' => ee()->lang->line('add_products')],
            'edit_products' => ['edit_products' => ee()->lang->line('edit_products')],
            'delete_products' => ['delete_products' => ee()->lang->line('delete_products')],

            /// REPORTS
            'customer_report' => ['customer_report' => ee()->lang->line('customer_report')],
            'product_report' => ['product_report' => ee()->lang->line('product_report')],
            'run_report' => ['run_report' => ee()->lang->line('run_report')],

            /// UTILITIES
            'system_settings' => ['system_settings' => ee()->lang->line('system_settings')],
            'print_invoice' => ['print_invoice' => ee()->lang->line('print_invoice')],
            'print_packing_slip' => ['print_packing_slip' => ee()->lang->line('print_packing_slip')],
        ];

        if (!ee()->cartthrob->store->config('show_product_admin')) {
            unset(
                $this->params['nav']['delete_products'],
                $this->params['nav']['edit_products'],
                $this->params['nav']['add_products'],
                $this->params['nav']['view_products']
            );
        }

        $this->params['no_form'] = [
            'om_sales_dashboard',
            'edit',
            'delete',
            'view',
            'order_report',
            'run_report',
            'customer_report',
            'print_packing_slip',
            'print_invoice',
            'product_report',
            'add_products',
        ];

        $this->params['no_nav'] = [
            'edit',
            'delete',
            'run_report',
            'print_invoice',
            'print_packing_slip',
            'delete_products',
            'edit_products',
        ];

        ee()->load->library('mbr_addon_builder');
        ee()->mbr_addon_builder->initialize($this->params);
    }

    //// PRODUCTS
    public function add_products()
    {
        $this->initialize();

        ee()->load->library('file_field');
        ee()->load->helper('form');
        ee()->cp->add_js_script(['ui' => 'datepicker']);
        ee()->cp->add_to_head('<link rel="stylesheet" href="' . URL_THIRD_THEMES . '/cartthrob/css/jquery-ui.min.css" type="text/css" media="screen" />');

        ee()->javascript->output("
            if ($('.datepicker').size() > 0) 
            {
                $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
            }
        ");

        ee()->file_field->browser(['publish' => true]);

        $structure = [
            'plugin_type' => 'add_products',
            'plugins' => [
                [
                    'classname' => 'add_products',
                    'title' => 'add_products_title',
                    'overview' => 'add_products_overview',
                    'note' => null,
                    'settings' => [
                        [
                            'name' => 'title',
                            'short_name' => 'title',
                            'default' => null,
                            'type' => 'text',
                        ],
                        [
                            'name' => 'url_title',
                            'short_name' => 'url_title',
                            'default' => null,
                            'type' => 'text',
                        ],
                        [
                            'name' => 'status',
                            'short_name' => 'status',
                            'default' => 'active',
                            'type' => 'select',
                            'options' => [
                                'active' => 'active',
                                'inactive' => 'inactive',
                            ],
                        ],
                        [
                            'name' => 'description',
                            'short_name' => 'description',
                            'default' => null,
                            'type' => 'textarea',
                            'attributes' => [
                                'class' => 'wysiwyg',
                            ],
                        ],
                        [
                            'name' => 'sku',
                            'short_name' => 'sku',
                            'type' => 'text',
                        ],
                        [
                            'name' => 'featured',
                            'short_name' => 'featured',
                            'default' => 'no',
                            'type' => 'select',
                            'options' => [
                                'no' => 'no',
                                'yes' => 'yes',
                            ],
                        ],

                        ///// INVENTORY
                        [
                            'name' => 'inventory',
                            'short_name' => 'inventory',
                            'default' => '',
                            'type' => 'text',
                        ],

                        ////// SHIPPING
                        [
                            'name' => 'shipping',
                            'short_name' => 'shipping',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'shippable',
                            'short_name' => 'shippable',
                            'default' => 'yes',
                            'type' => 'select',
                            'options' => [
                                'yes' => 'yes',
                                'no' => 'no',
                            ],
                        ],
                        [
                            'name' => 'weight',
                            'short_name' => 'weight',
                            'default' => '',
                            'type' => 'text',
                        ],

                        ///// TAX
                        [
                            'name' => 'tax',
                            'short_name' => 'tax',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'taxable',
                            'short_name' => 'taxable',
                            'default' => 'yes',
                            'type' => 'select',
                            'options' => [
                                'yes' => 'yes',
                                'no' => 'no',
                            ],
                        ],

                        ///// PRICING
                        [
                            'name' => 'price_header',
                            'short_name' => 'price_header',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'price',
                            'short_name' => 'price',
                            'default' => '',
                            'type' => 'text',
                        ],
                        [
                            'name' => 'store_cost',
                            'short_name' => 'store_cost',
                            'default' => '',
                            'type' => 'text',
                        ],

                        ///// SALE
                        [
                            'name' => 'sale_pricing',
                            'short_name' => 'sale_pricing',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'sale_price',
                            'short_name' => 'sale_price',
                            'default' => '',
                            'type' => 'text',
                        ],
                        [
                            'name' => 'sale_start',
                            'short_name' => 'sale_start',
                            'default' => '',
                            'type' => 'text',
                            'attributes' => [
                                'class' => 'datepicker',
                            ],
                        ],
                        [
                            'name' => 'sale_end',
                            'short_name' => 'sale_end',
                            'default' => '',
                            'type' => 'text',
                            'attributes' => [
                                'class' => 'datepicker',
                            ],
                        ],

                        /// IMAGES
                        [
                            'name' => 'images_header',
                            'short_name' => 'images_header',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'images',
                            'short_name' => 'images',
                            'type' => 'matrix',
                            'settings' => [
                                /// @TODO replace with an uploader
                                [
                                    'name' => 'userfile',
                                    'short_name' => 'userfile',
                                    'default' => '',
                                    'type' => 'file',
                                ],
                                [
                                    'name' => 'title',
                                    'short_name' => 'title',
                                    'default' => '',
                                    'type' => 'text',
                                ],
                                [
                                    'name' => 'active',
                                    'short_name' => 'active',
                                    'default' => 'yes',
                                    'type' => 'radio',
                                    'options' => [
                                        'yes' => 'yes',
                                        'no' => 'no',
                                    ],
                                ],
                                [
                                    'name' => 'delete',
                                    'short_name' => 'delete',
                                    'type' => 'checkbox',
                                ],
                            ],
                        ],

                        ///// OPTIONS
                        [
                            'name' => 'item_options',
                            'short_name' => 'item_options',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'option_groups',
                            'short_name' => 'option_groups',
                            'type' => 'matrix',
                            'settings' => [
                                [
                                    'name' => 'item_option_name',
                                    'short_name' => 'item_option_name',
                                    'default' => '',
                                    'type' => 'text',
                                ],
                                [
                                    'name' => 'required',
                                    'short_name' => 'required',
                                    'default' => 'yes',
                                    'type' => 'checkbox',
                                ],
                                // @TODO make this use JS to generate new options
                                [
                                    'name' => 'option_data',
                                    'short_name' => 'option_data',
                                    'type' => 'textarea',
                                ],
                            ],
                        ],
                        [
                            'name' => 'meta',
                            'short_name' => 'meta',
                            'type' => 'header',
                        ],
                        [
                            'name' => 'keywords',
                            'short_name' => 'keywords',
                            'default' => '',
                            'type' => 'textarea',
                        ],
                    ],
                ],
            ],
        ];

        $data['html'] = ee()->mbr_addon_builder->view_plugin_settings($structure);

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [
            'data' => $data,
            'add_product_action' => form_open(ee('CP/URL')->make(
                'addons/settings/cartthrob_order_manager/add_product_action',
                ['return' => 'view_products']
            )),
        ]);
    }

    public function add_product_action()
    {
        ee()->load->helper('data_formatting');

        $create_data = [];
        $post_data = ee()->input->post('add_products_settings');

        foreach ($post_data as $key => $value) {
            if (is_array($value)) {
                $value = serialize($value);
            }

            if ($value !== null) {
                $create_data[$key] = $value;
            }
        }

        if (!empty($create_data)) {
            ee()->load->model('generic_model');
            $products = new Generic_model('cartthrob_products');

            $id = $products->create($create_data);
        }

        if (isset($id)) {
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_product_added_successfully'));
        } else {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_product_addition_failed'));
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . ee()->input->get('return', true)));
    }

    public function view_products()
    {
        $this->initialize();

        ee()->load->model('generic_model');

        $cartthrob_products = new Generic_model('cartthrob_products');
        $products = $cartthrob_products->read();
        $data['products'] = [];

        if ($products) {
            foreach ($products as &$prod) {
                $prod = [
                    $prod['title'],
                    $prod['price'],
                ];
            }
        }

        ee()->load->library('table');
        ee()->table->clear();
        ee()->table->set_template([
            'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">',
            'heading_cell_start' => '<th colspan="6">',
        ]);
        ee()->table->set_heading('');

        $data['product_table'] = ee()->table->generate($products);

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, compact('data'));
    }

    public function edit_products()
    {
        $this->initialize();

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [], $structure = []);
    }

    public function form_update()
    {
        if (ee()->input->post('delete_order')) {
            if (!ee()->input->post('id')) {
                ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_no_item_was_selected_for_deletion'));
                ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . ee()->input->get('return', true)));
            } else {
                $this->delete_order();
            }
        }

        return ee()->mbr_addon_builder->form_update($this->table);
    }

    //// ORDER
    public function delete_order()
    {
        ee()->load->model('order_management_model');
        ee()->load->library('api');
        ee()->legacy_api->instantiate('channel_entries');

        if (!ee()->input->post('id')) {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_no_item_was_selected_for_deletion')
            );
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . ee()->input->get('return', true)));
        }

        $entry_ids = (array)ee()->order_management_model->get_purchased_items_by_order(ee()->input->post('id'));

        foreach ($entry_ids as $id) {
            if (!empty($id)) {
                ee()->api_channel_entries->delete_entry($id);
            }
        }

        ee()->api_channel_entries->delete_entry(ee()->input->post('id'));
        ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_order_deleted'));
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . ee()->input->get('return', true)));
    }

    public function plugin_setting($type, $name, $current_value, $options = [], $attributes = [])
    {
        return ee()->mbr_addon_builder->plugin_setting($type, $name, $current_value, $options, $attributes);
    }

    public function system_settings()
    {
        $this->initialize();

        $structure['class'] = 'order_manager';
        $structure['description'] = '';
        $structure['caption'] = '';
        $structure['title'] = 'cartthrob_order_manager_general_settings';
        $structure['settings'] = [
            [
                'name' => 'cartthrob_order_manager_invoice_template',
                'short_name' => 'invoice_template',
                'note' => 'cartthrob_order_manager_invoice_template_note',
                'type' => 'select',
                'attributes' => ['class' => 'templates_blank'],
            ],
            [
                'name' => 'cartthrob_order_manager_packing_slip_template',
                'short_name' => 'packing_slip_template',
                'note' => 'cartthrob_order_manager_packing_slip_template_note',
                'type' => 'select',
                'attributes' => ['class' => 'templates_blank'],
            ],
            [
                'name' => 'custom_templates',
                'short_name' => 'custom_templates',
                'type' => 'matrix',
                'settings' => [
                    [
                        'name' => 'cartthrob_order_manager_custom_template_name',
                        'short_name' => 'custom_template_name',
                        'default' => 'Custom Template',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'cartthrob_order_manager_custom_template',
                        'short_name' => 'custom_template',
                        'type' => 'select',
                        'attributes' => ['class' => 'templates_blank'],
                    ],
                ],
            ],
        ];

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [], $structure);
    }

    public function product_report()
    {
        $this->initialize();

        ee()->cp->add_js_script(['ui' => 'datepicker']);
        ee()->cp->add_to_head('<link rel="stylesheet" href="' . URL_THIRD_THEMES . '/cartthrob/css/jquery-ui.min.css" type="text/css" media="screen" />');

        $date_picker_js = "
            if ($('.datepicker').size() > 0) 
            {
                $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
                $('.datepicker').on('change',function(){
                    var field_name = $(this).attr('name');
                    var value = $(this).val();
                    $(\"input[name='\"+field_name+\"']\").val(value);
                });
            }
        ";
        ee()->javascript->output($date_picker_js);

        ee()->load->model('order_management_model');
        ee()->load->helper('data_formatting');
        ee()->load->library('table');

        $data = [];
        $date_range = ee()->input->get_post('where');
        $where = [];

        if ($date_range) {
            if ($date_range['date_start']) {
                $start_date = explode('-', $date_range['date_start']);
                $start_time = mktime(0, 0, 0, $start_date[1], $start_date[2], $start_date[0]);
                $where[ee()->db->dbprefix . 'cartthrob_order_items.entry_date >='] = $start_time;
            } else {
                $data['date_start'] = null;
            }

            if ($date_range['date_finish']) {
                $end_date = explode('-', $date_range['date_finish']);
                $end_time = mktime(23, 59, 59, $end_date[1], $end_date[2], $end_date[0]);
                $where[ee()->db->dbprefix . 'cartthrob_order_items.entry_date <='] = $end_time;
            } else {
                $data['date_finish'] = null;
            }
        }
        $data['date_start'] = null;
        $data['date_finish'] = null;

        if (ee()->input->get_post('id')) {
            $products = ee()->order_management_model->get_purchased_item(ee()->input->get_post('id'));
        } else {
            $products = ee()->order_management_model->get_purchased_products($where);
        }

        foreach ($products as &$row) {
            $row['options'] = null;

            if ($row['extra'] && $extra = _unserialize($row['extra'], true)) {
                foreach ($extra as $key => $value) {
                    if (!ee()->input->get_post('download')) {
                        $row['options'] .= '<strong>' . $key . '</strong>: ' . $value . '<br>';
                    } else {
                        $row['options'] .= $key . ': ' . $value . '| ';
                    }
                }
            }

            $row['total_sales'] = ee()->number->format($row['total_sales']);
            $row['price'] = ee()->number->format($row['price']);
            $row['price_plus_tax'] = ee()->number->format($row['price_plus_tax']);

            if (!ee()->input->get_post('download')) {
                $href = ee('CP/URL')->make('publish/edit/entry/' . $row['entry_id']);
                $detail_href = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_reload', ['product_id' => $row['entry_id']]);
                $row['entry_id'] = $row['entry_id'] . "<a href='" . $href . "'> (" . lang('cartthrob_order_manager_edit_product') . '&raquo;)</a>';
                $row['title'] = $row['title'] . "<a href='" . $detail_href . "'> (" . lang('cartthrob_order_manager_product_detail_report') . '&raquo;)</a>';
            }

            unset($row['row_id'], $row['row_order'], $row['quantity'], $row['order_id'], $row['weight'], $row['no_tax'], $row['no_shipping'], $row['extra'], $row['entry_date']);
        }

        if (!isset($products[0])) {
            // nothing has been sold. go back.
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_no_products_have_been_sold'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/om_sales_dashboard'));
        }

        $keys = array_keys($products[0]);

        foreach ($keys as &$val) {
            $val = lang('cartthrob_order_manager_' . $val);
        }

        ee()->table->clear();
        ee()->table->set_template(['table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">']);
        ee()->table->set_heading($keys);

        if (ee()->input->get_post('download')) {
            $this->export_csv($products, $keys, ee()->input->post('filename'), $return_data = false, $format = ee()->input->get_post('download'));
        }

        $data['products'] = ee()->table->generate($products);
        $data['hidden_inputs'] = null;

        ee()->load->helper('form');

        if (ee()->input->get_post('id')) {
            foreach ($_POST as $key => $value) {
                $data['hidden_inputs'] .= form_hidden('id', ee()->input->get_post('id'));
            }
        }

        if (isset($_POST['where']['date_start'])) {
            $data['hidden_inputs'] .= form_hidden('where[date_start]', $_POST['where']['date_start']);
        } else {
            $data['hidden_inputs'] .= form_hidden('where[date_start]', null);
        }

        if (isset($_POST['where']['date_finish'])) {
            $data['hidden_inputs'] .= form_hidden('where[date_finish]', $_POST['where']['date_finish']);
        } else {
            $data['hidden_inputs'] .= form_hidden('where[date_finish]', null);
        }

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [
            'data' => $data,
            'run_report' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/product_report', ['return' => __FUNCTION__])),
            'export_csv' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . __FUNCTION__, ['return' => __FUNCTION__])),
        ]);
    }

    public function export_csv(array $data, $headers = [], $filename = null, $return_data = false, $format = 'xls')
    {
        if (!$filename) {
            $filename = 'data';
        }

        $content = null;

        if (is_array($data) && !empty($data)) {
            $rows = [];

            $headers_count = count($headers);
            // data = rows of data key_count == row_count.
            $columns_count = count(array_keys($data[0]));

            foreach ($data as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                $rows[] = $this->format_csv($value, $format);
            }

            if ($headers_count && $columns_count == $headers_count) {
                $content .= $this->format_csv($headers, $format);
            }

            $content .= implode('', $rows);
        }

        if ($content) {
            if ($return_data) {
                return $content;
            } else {
                ee()->load->helper('download');
                force_download($filename . '.' . $format, $content);
            }
        }
    }

    public function format_csv($row, $format = 'xls')
    {
        static $fp = false;

        if ($fp === false) {
            // see http://php.net/manual/en/wrappers.php.php
            $fp = fopen('php://temp', 'r+');
        } else {
            rewind($fp);
        }

        // don't love this
        foreach ($row as $key => $value) {
            $row[$key] = str_replace(["\r", "\n"], ' ', $value);
        }

        // ascii 9 is tab, and ascii 0 is NULL
        // http://www.asciitable.com/
        $delimiter = $format === 'csv' ? ',' : chr(9);
        $enclosure = $format === 'csv' ? '"' : chr(0);

        if (fputcsv($fp, $row, $delimiter, $enclosure) === false) {
            return false;
        }

        rewind($fp);
        $csv = fgets($fp);

        return $csv;
    }

    public function om_sales_dashboard()
    {
        if (ee()->input->get('save')) {
            $reports_settings = ee()->input->post('reports_settings');

            if (is_array($reports_settings) && isset($reports_settings['reports'])) {
                $_POST = ['reports' => $reports_settings['reports']];
            } else {
                $_POST = ['reports' => []];
            }

            $_GET['return'] = 'om_sales_dashboard';

            return $this->quick_save();
        }

        $this->initialize();

        ee()->load->model('order_model');
        ee()->load->helper('form', 'array');

        if (ee()->input->get('entry_id')) {
            ee()->functions->redirect(
                ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => ee()->input->get('entry_id')])
            );
        }

        ee()->load->library('reports');
        ee()->load->library('number');

        if (ee()->input->get_post('report')) {
            ee()->load->library('template_helper');

            ee()->template_helper->reset([
                'base_url' => ee('CP/URL')->make('addons/settings/cartthrob_order_manager/om_sales_dashboard') . AMP . 'report=',
                'template_key' => 'report',
            ]);

            if (is_numeric(ee()->input->get_post('report'))) {
                ee()->load->model('generic_model');

                $reports_model = new Generic_model('cartthrob_order_manager_reports');

                if ($report_info = $reports_model->read(ee()->input->get_post('report'))) {
                    ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_reload', ['id' => $report_info['id']]));
                }
            }
            $data['view'] = ee()->template_helper->cp_render();
        } else { //default view
            if (ee()->input->get('year')) {
                if (ee()->input->get('month')) {
                    if (ee()->input->get('day')) {
                        $name = date('D d', mktime(0, 0, 0, ee()->input->get('month'), ee()->input->get('day'), ee()->input->get('year')));
                        $rows = ee()->reports->get_daily_totals(ee()->input->get('day'), ee()->input->get('month'), ee()->input->get('year'));
                        $overview = lang('narrow_by_order');
                    } else {
                        $name = date('F Y', mktime(0, 0, 0, ee()->input->get('month'), 1, ee()->input->get('year')));
                        $rows = ee()->reports->get_monthly_totals(ee()->input->get('month'), ee()->input->get('year'));
                        $overview = lang('narrow_by_day');
                    }
                } else {
                    $name = ee()->input->get('year');
                    $rows = ee()->reports->get_yearly_totals(ee()->input->get('year'));
                    $overview = lang('narrow_by_month');
                }
            } elseif (ee()->input->get_post('date_start') || ee()->input->get_post('date_finish')) {
                $name = ee()->lang->line('reports_order_totals_in_range');
                $start = null;
                $end = null;

                if (ee()->input->get_post('date_start')) {
                    $start = strtotime(ee()->input->get_post('date_start'));
                }

                if (ee()->input->get_post('date_end')) {
                    $end = strtotime(ee()->input->get_post('date_finish'));
                }

                $rows = ee()->reports->get_all_totals($start, $end);
                $overview = lang('narrow_by_month');
            } else {
                $name = ee()->lang->line('reports_order_totals_to_date');
                $rows = ee()->reports->get_all_totals();
                $overview = lang('narrow_by_month');
            }

            if ($rows) {
                ee()->javascript->output('cartthrobChart(' . json_encode($rows) . ', "' . $name . '");');
            }

            $data['view'] = ee()->load->view('om_reports_home', ['overview' => $overview], true);
        }

        ee()->load->library('table');

        ee()->table->clear();
        ee()->table->set_template(['table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">']);

        ee()->load->model('order_model');
        ee()->load->model('order_management_model');
        ee()->load->library('localize');

        $orders = ee()->order_model->getOrders([
            'year' => ee()->localize->format_date('%Y'),
            'month' => ee()->localize->format_date('%m'),
            'day' => ee()->localize->format_date('%d'),
        ]);

        $todays_order_list = [];

        foreach ($orders as $order) {
            $first_name = null;
            $last_name = null;

            if (ee()->cartthrob->store->config('orders_billing_first_name')) {
                $first_name = element('field_id_' . ee()->cartthrob->store->config('orders_billing_first_name'), $order);
            }

            if (ee()->cartthrob->store->config('orders_billing_last_name')) {
                $last_name = element('field_id_' . ee()->cartthrob->store->config('orders_billing_last_name'), $order);
            }

            $href = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => element('entry_id', $order)]);
            $member = ee('CP/URL')->make('members/profile/settings', ['id' => '']);

            ee()->load->model('order_management_model');

            if (ee()->order_management_model->is_member($order['author_id'])) {
                $member_info = "<a href='" . $member . element('author_id', $order) . "'>(" . element('author_id', $order) . ') ' . $first_name . ' ' . $last_name . ' &raquo;</a><br> ';
            } else {
                $member_info = $first_name . ' ' . $last_name;
            }

            ee()->load->library('localize');

            $date = ee()->localize->format_date('%h%i %a', element('entry_date', $order), $localize = true);

            $todays_order_list[] = [
                "<a href='" . $href . "'>" . element('title', $order) . ' &nbsp;</a>',
                ee()->number->format(element('order_total', $order)),
                element('status', $order),
                $member_info,
                $date,
                '',
            ];
        }

        if (empty($todays_order_list)) {
            $todays_order_list[] = ['none', '', '', '', '', ''];
        }

        $todays_orders_header = [
            lang('cartthrob_order_manager_todays_orders'),
            lang('cartthrob_order_manager_total'),
            lang('cartthrob_order_manager_status'),
            lang('cartthrob_order_manager_customer'),
            lang('cartthrob_order_manager_time'),
            '',
        ];

        array_unshift($todays_order_list, $todays_orders_header);
        $data['todays_orders'] = ee()->table->generate($todays_order_list);
        $order_totals = ee()->order_model->order_totals();
        $average_total = 'Not available';

        if (isset($order_totals['average_total'])) {
            $average_total = ee()->number->format($order_totals['average_total']);
        }

        ee()->load->library('localize');

        $year_totals = ee()->order_model->order_totals(['year' => ee()->localize->format_date('%Y')]);

        $months_orders = ee()->order_model->order_totals([
            'year' => ee()->localize->format_date('%Y'),
            'month' => ee()->localize->format_date('%m'),
        ]);

        $todays_orders = ee()->order_model->order_totals([
            'year' => ee()->localize->format_date('%Y'),
            'month' => ee()->localize->format_date('%m'),
            'day' => ee()->localize->format_date('%d'),
        ]);

        $data['order_totals'] = ee()->table->generate([
            [lang('order_totals'), lang('amount')],
            [lang('today_sales'), ee()->number->format(ee()->reports->get_current_day_total())],
            [lang('month_sales'), ee()->number->format(ee()->reports->get_current_month_total())],
            [lang('year_sales'), ee()->number->format(ee()->reports->get_current_year_total())],
            [lang('cartthrob_order_manager_total_sales'), ee()->number->format($order_totals['total'])],
            [lang('cartthrob_order_manager_average_sale'), $average_total],
            [lang('cartthrob_order_manager_todays_orders'), $todays_orders['orders']],
            [lang('cartthrob_order_manager_months_orders'), $months_orders['orders']],
            [lang('cartthrob_order_manager_years_orders'), $year_totals['orders']],
            [lang('cartthrob_order_manager_total_orders'), $order_totals['orders']],
            [lang('cartthrob_order_manager_total_customers'), ee()->order_management_model->get_customer_count()],
        ]);
        $data['current_report'] = ee()->input->get_post('report');
        $data['reports'] = [];

        $reports = ee()->get_settings->get($this->module_name, 'reports');

        // the put any already created in CT in there
        if (!$reports) {
            $reports = ee()->cartthrob->store->config('reports');
        }

        foreach ($reports as $report) {
            $data['reports']['Custom Reports'][$report['template']] = $report['name'];
        }

        if (!empty($data['reports'])) {
            asort($data['reports']);
            // shoving the default on the front
            array_unshift($data['reports'], lang('order_totals'));
        }

        ee()->load->model('generic_model');

        $reports_model = new Generic_model('cartthrob_order_manager_reports');
        $order_reports = $reports_model->read(null, $order_by = null, $order_direction = 'asc', $field_name = 'type', $string = 'order');

        if ($order_reports) {
            foreach ($order_reports as $rep) {
                $data['reports']['Order Reports'][$rep['id']] = $rep['report_title'];
            }
        }

        $plugin_vars = [
            'cartthrob_mcp' => $this,
            'settings' => [
                'reports_settings' => [
                    'reports' => ee()->get_settings->get($this->module_name, 'reports'),
                ],
            ],
            'plugin_type' => 'reports',
            'plugins' => [
                [
                    'classname' => 'reports',
                    'title' => 'reports_settings_title',
                    'overview' => 'reports_settings_overview',
                    'settings' => [
                        [
                            'name' => 'reports',
                            'short_name' => 'reports',
                            'type' => 'matrix',
                            'settings' => [
                                [
                                    'name' => 'report_name',
                                    'short_name' => 'name',
                                    'type' => 'text',
                                ],
                                [
                                    'name' => 'report_template',
                                    'short_name' => 'template',
                                    'type' => 'select',
                                    'options' => ee()->mbr_addon_builder->get_templates(),
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $data['reports_list'] = ee()->mbr_addon_builder->view_plugin_settings($plugin_vars);
        $data['reports_filter'] = form_open(ee('CP/URL')->make('addons/settings/cartthrob_order_manager/om_sales_dashboard'), 'id="reports_filter"');
        $data['reports_date'] = form_open(ee('CP/URL')->make('addons/settings/cartthrob_order_manager/om_sales_dashboard'), 'id="reports_date"');

        ee()->cp->add_js_script(['ui' => 'datepicker']);
        ee()->cp->add_to_head('<link rel="stylesheet" href="' . URL_THIRD_THEMES . '/cartthrob/css/jquery-ui.min.css" type="text/css" media="screen" />');
        ee()->javascript->output("
            if ($('.datepicker').size() > 0) 
            {
                $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
            }
        ");

        ee()->javascript->output('
            $(document).on("change", "select[name=report]", function(){
                $(this).parents("form").submit();
            });
            $("#reports").show();
        ');

        return ee()->mbr_addon_builder->load_view('om_sales_dashboard', $data);
    }

    public function quick_save()
    {
        return ee()->mbr_addon_builder->quick_save();
    }

    public function customer_report()
    {
        ee()->load->helper('form');
        ee()->load->model('order_management_model');

        $data['customer_count'] = ee()->order_management_model->get_customer_count();

        if (ee()->input->get_post('download')) {
            ee()->load->library('cartthrob_file');

            $filename = ee()->input->get_post('filename');
            ee()->cartthrob_file->output_streaming_file_headers($filename);
            $limit = $this->limit;
            $tmp_data = [];
            $tmp_headers = '';

            for ($i = 0; $i < $data['customer_count']; $i += $limit) {
                $tmp_response = $this->customer_export_datasource($where = [], $order_by = 'entry_date', $sort = 'DESC', $limit, $offset = $i);
                $tmp_data = array_merge($tmp_data, $tmp_response['data']);
                $tmp_headers = $tmp_response['headers'];
            }

            die($this->export_csv($tmp_data, $tmp_headers, $filename, $return_data = false, $format = ee()->input->get_post('download')));
        } else {
            $this->initialize();

            ee()->load->library('table');

            ee()->table->clear();
            ee()->table->set_template(['table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">']);

            $raw_headers = [
                lang('cartthrob_order_manager_customer_name'),
                lang('cartthrob_order_manager_customer_email'),
                lang('cartthrob_order_manager_customer_phone'),
                lang('cartthrob_order_manager_total_order_amount'),
                lang('cartthrob_order_manager_total_order_count'),
                lang('cartthrob_order_manager_total_order_date'),
                lang('cartthrob_order_manager_total_order_date'),
            ];

            $defaults = [
                'sort' => ['entry_date' => 'asc'],
            ];

            $params['limit'] = $this->limit;
            $defaults['offset'] = ee()->input->get_post('rownum', 0);

            ee()->table->set_heading($raw_headers);

            ee()->load->library('pagination');

            $data_table = $this->customer_report_datasource($defaults, $params);

            foreach ($data_table['rows'] as $row) {
                ee()->table->add_row(array_values($row));
            }

            ee()->pagination->initialize([
                // Pass the relevant data to the paginate class
                'base_url' => ee('CP/URL')->make('addons/settings/cartthrob_order_manager/customer_report'),
                'total_rows' => $data_table['pagination']['total_rows'],
                'per_page' => $data_table['pagination']['per_page'],
                'page_query_string' => true,
                'query_string_segment' => 'rownum',
                'full_tag_open' => '<div style="float: none;" class="paginate" title="' . $data_table['pagination']['total_rows'] . ' total entries"><ul>',
                'full_tag_close' => '</ul></div>',
                'first_tag_open' => '<li>',
                'first_tag_close' => '</li>',
                'prev_tag_open' => '<li>',
                'prev_tag_close' => '</li>',
                'cur_tag_open' => '<li><a href="" class="act">',
                'cur_tag_close' => '</a></li>',
                'num_tag_open' => '<li>',
                'num_tag_close' => '</li>',
                'next_tag_open' => '<li>',
                'next_tag_close' => '</li>',
                'last_tag_open' => '<li>',
                'last_tag_close' => '</li>',
                'prev_link' => 'Prev',
                'next_link' => 'Next',
                'first_link' => 'First',
                'last_link' => 'Last',
            ]);

            $data['html'] = ee()->table->generate() . ee()->pagination->create_links();

            return ee()->mbr_addon_builder->load_view(__FUNCTION__, [
                'data' => $data,
                'export_csv' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . __FUNCTION__, ['return' => __FUNCTION__])),
            ]);
        }
    }

    public function customer_export_datasource($where = [], $order_by = 'entry_date', $sort = 'DESC', $limit = null, $offset = null)
    {
        ee()->load->model('cartthrob_field_model');

        $fields = $this->order_fields;
        $default_show = [];
        $headers = [];

        foreach ($fields as $field) {
            if (ee()->cartthrob->store->config($field)) {
                $default_show[] = 'field_id_' . ee()->cartthrob->store->config($field);
                $lang = lang($field);
                $headers['field_id_' . ee()->cartthrob->store->config($field)] = ($lang ? $lang : $field);
            }
        }

        ee()->load->model('order_management_model');

        $rows = ee()->order_management_model->get_customers_reports($where, $order_by, $sort, $limit, $offset);

        $new_rows = [];
        foreach ($rows as $key => &$row) {
            foreach ($row as $k => $r) {
                if (!in_array($k, $default_show)) {
                    unset($row[$k]);
                }
            }

            $new_rows[] = array_merge($headers, $row);
        }

        return [
            'data' => $new_rows,
            'headers' => $headers,
        ];
    }

    public function customer_report_datasource($state, $params = [])
    {
        $offset = $state['offset'];
        $limit = isset($params['limit']) ? $params['limit'] : $this->limit;

        $this->sort = $state['sort'];

        ee()->load->model('order_management_model');

        $sub_params = [];
        foreach ($state['sort'] as $key => $value) {
            $sub_params['order_by'][] = $key;
            $sub_params['sort'][] = $value;
        }

        // let's only show orders that have a status that matches "completed/authorized" orders
        $default_status = ee()->get_settings->get('cartthrob', 'orders_default_status');
        $where = [];

        if ($default_status) {
            $where['status'] = $default_status;
        }

        $rows = ee()->order_management_model->get_customers($where, $sub_params['order_by'], $sub_params['sort'], $limit, $offset);

        foreach ($rows as $key => &$row) {
            $email_address = null;
            $first_name = null;
            $last_name = null;
            $phone = null;

            if (!empty($row['field_id_' . ee()->cartthrob->store->config('orders_customer_email')])) {
                $email_address = $row['field_id_' . ee()->cartthrob->store->config('orders_customer_email')];
            }
            if (!empty($row['field_id_' . ee()->cartthrob->store->config('orders_billing_first_name')])) {
                $first_name = $row['field_id_' . ee()->cartthrob->store->config('orders_billing_first_name')];
            }
            if (!empty($row['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')])) {
                $last_name = $row['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')];
            }
            if (!empty($row['field_id_' . ee()->cartthrob->store->config('orders_customer_phone')])) {
                $phone = $row['field_id_' . ee()->cartthrob->store->config('orders_customer_phone')];
            }

            // first and last name + link to edit information

            if (ee()->order_management_model->is_member($row['author_id'])) {
                $new_row['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')] = '<a href="' . ee('CP/URL')->make('members/profile/settings', ['id' => $row['author_id']]) . '">' . $first_name . ' ' . $last_name . '(' . $row['author_id'] . ')</a>';
            } else {
                $new_row['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')] = $first_name . ' ' . $last_name;
            }

            // email address + link
            $new_row['field_id_' . ee()->cartthrob->store->config('orders_customer_email')] = '<a href="mailto:' . $email_address . '">' . $email_address . '</a>';
            // customer phone
            $new_row['field_id_' . ee()->cartthrob->store->config('orders_customer_phone')] = $phone;
            $new_row['order_total'] = ee()->number->format($row['order_total']);
            $new_row['order_count'] = $row['order_count'];

            ee()->load->library('localize');

            $new_row['order_last'] = ee()->localize->format_date('%m-%d-%Y %h:%i %a', $row['order_last'], $localize = true);

            // link to customer report. can't use post, so we have to use a link
            $new_row['actions'] = '<a href="' . ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_reload',
                    [
                        'return' => __FUNCTION__,
                        'customer_email' => $email_address,
                    ]) . '" class="btn submit">' . lang('cartthrob_order_manager_view_customer_orders') . '</a>';

            $row = $new_row;
        }

        $count = ee()->order_management_model->get_customer_count();

        return [
            'rows' => $rows,
            'pagination' => [
                'per_page' => $limit,
                'total_rows' => $count,
            ],
        ];
    }

    public function order_report()
    {
        if (is_numeric(ee()->input->get_post('report'))) {
            ee()->load->model('generic_model');

            $reports_model = new Generic_model('cartthrob_order_manager_reports');
            $report_info = $reports_model->read(ee()->input->get_post('report'));

            if ($report_info) {
                ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_reload',
                    ['id' => $report_info['id']]));
            }
        }

        $this->initialize();

        ee()->cp->add_js_script(['ui' => 'datepicker']);
        ee()->cp->add_to_head('<link rel="stylesheet" href="' . URL_THIRD_THEMES . '/cartthrob/css/jquery-ui.min.css" type="text/css" media="screen" />');
        ee()->load->helper('form');

        $data = [];
        $data['date_start'] = null; //ee()->localize->decode_date('%Y-%m-%d',ee()->localize->now - 7*24*60*60);
        $data['date_finish'] = null; //ee()->localize->decode_date('%Y-%m-%d',ee()->localize->now );

        ee()->load->model('generic_model');

        $reports_model = new Generic_model('cartthrob_order_manager_reports');
        $order_reports = $reports_model->read(null, $order_by = null, $order_direction = 'asc', $field_name = 'type', $string = 'order');
        $data['reports'] = [];

        if ($order_reports) {
            foreach ($order_reports as $rep) {
                $data['reports'][$rep['id']] = $rep['report_title'];
            }
        }

        // ORDER TOTALS
        $fields = [
            'average_total',
            'discount',
            'orders',
            'shipping',
            'shipping_plus_tax',
            'subtotal',
            'subtotal_plus_tax',
            'tax',
            'total',
        ];

        $default_show = [
            'total',
            'subtotal',
            'tax',
            'shipping',
            'discount',
            'orders',
            'order_items',
        ];

        foreach ($fields as $value) {
            $checked = null;
            if (in_array($value, $default_show)) {
                $checked = ' checked="checked" ';
            }
            $fields_input[] = lang('cartthrob_order_manager_' . $value);
            $fields_input[] = ' <input type="checkbox" value="' . $value . '" ' . $checked . ' name="show_fields[]" />';
        }

        ee()->load->library('table');
        ee()->table->clear();
        ee()->table->set_template([
            'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">',
            'heading_cell_start' => '<th colspan="6">',
        ]);
        ee()->table->set_heading(lang('cartthrob_order_manager_include_total_fields'));

        $new_list = ee()->table->make_columns($fields_input, 6);
        $data['order_totals'] = ee()->table->generate($new_list);

        ///// SEARCH ORDER FIELDS

        $search_fields = [
            'cartthrob_mcp' => $this,
            'plugin_type' => 'search_fields',
            'plugins' => [
                [
                    'classname' => 'search_fields',
                    'title' => 'search_fields_title',
                    'settings' => [
                        [
                            'name' => 'search_fields',
                            'short_name' => 'search_fields',
                            'type' => 'matrix',
                            'settings' => [
                                [
                                    'name' => 'search_field',
                                    'short_name' => 'search_field',
                                    'type' => 'select',
                                    'attributes' => ['class' => 'order_channel_fields'],
                                ],
                                [
                                    'name' => 'search_content',
                                    'short_name' => 'search_content',
                                    'type' => 'text',
                                ],
                                [
                                    'name' => 'search_not_empty',
                                    'short_name' => 'search_not_empty',
                                    'type' => 'checkbox',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $data['search_fields'] = ee()->mbr_addon_builder->view_plugin_settings($search_fields, true);

        ///// MEMBER INPUT FIELDS

        ee()->table->clear();
        ee()->table->set_template([
            'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">',
            'heading_cell_start' => '<th colspan="4">',
        ]);
        ee()->table->set_heading('');

        $member_field_list = [
            'author_id',
            'email_address',
            'first_name',
            'last_name',
            'city',
            'state',
            'country_code',
        ];
        $member_field_inputs = [];

        foreach ($member_field_list as $value) {
            $member_field_inputs[] = lang('cartthrob_order_manager_' . $value);

            switch ($value) {
                case 'state':
                    $class = 'states_blank';
                    $member_field_inputs[] = form_dropdown('where[' . $value . ']', [], null, "class='" . $class . "'");
                    break;
                case 'country_code':
                    $class = 'countries_blank';
                    $member_field_inputs[] = form_dropdown('where[' . $value . ']', [], null, "class='" . $class . "'");
                    break;
                default:
                    $member_field_inputs[] = form_input('where[' . $value . ']', '');
            }
        }

        ee()->table->set_heading(lang('cartthrob_order_manager_search_member_data'));

        $new_list = ee()->table->make_columns($member_field_inputs, 4);
        $data['member_inputs'] = ee()->table->generate($new_list);

        ///// ORDER FIELDS

        ee()->load->model('cartthrob_field_model');

        $channel_id = ee()->cartthrob->store->config('orders_channel');
        $order_channel_fields = ee()->cartthrob_field_model->get_fields_by_channel($channel_id);

        $default_show = [
            'order_total',
            'order_subtotal',
            'order_shipping',
            'order_shipping',
            'order_tax',
            'discount',
            'order_items',
        ];

        $order_fields = [];
        $order_fields['title'] = 'title';
        $order_fields['entry_id'] = 'entry_id';
        $order_fields['status'] = 'status';
        $order_fields['year'] = 'year';
        $order_fields['month'] = 'month';
        $order_fields['day'] = 'day';
        $order_fields['entry_date'] = 'entry_date';

        foreach ($order_channel_fields as $value) {
            if (in_array($value['field_name'], $default_show)) {
                $default_show[] = 'field_id_' . $value['field_id'];
            }
            $order_fields['field_id_' . $value['field_id']] = $value['field_label'];
        }

        asort($order_fields);

        foreach ($order_fields as $key => $value) {
            $checked = null;
            if (in_array($key, $default_show)) {
                $checked = ' checked="checked" ';
            }
            $order_fields_input[] = $value;
            $order_fields_input[] = ' <input type="checkbox" value="' . $value . '" ' . $checked . ' name="order_fields[' . $key . ']" />';
        }

        // GENERATE ORDER FIELDS TABLE
        ee()->table->clear();
        ee()->table->set_template([
            'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">',
            'heading_cell_start' => '<th colspan="6">',
        ]);
        ee()->table->set_heading(lang('cartthrob_order_manager_include_order_fields'));

        $new_list = ee()->table->make_columns($order_fields_input, 6);
        $data['order_fields'] = ee()->table->generate($new_list);

        // JAVASCRIPT
        ee()->cp->add_js_script(['ui' => 'datepicker']);
        ee()->cp->add_to_head('<link rel="stylesheet" href="' . URL_THIRD_THEMES . '/cartthrob/css/jquery-ui.min.css" type="text/css" media="screen" />');
        ee()->javascript->output("
            if ($('.datepicker').size() > 0) {
                $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
            }
        ");

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, [
            'data' => $data,
            'run_report' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_reload', ['return' => __FUNCTION__])),
            'reports_filter' => form_open(ee('CP/URL')->make('addons/settings/cartthrob_order_manager/' . __FUNCTION__), 'id="reports_filter"'),
        ]);
    }

    public function run_reload()
    {
        $return = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/run_report');
        $string = null;
        $new_post = $_POST;

        foreach ($new_post as $key => $value) {
            $new_post[$key] = xss_clean($value);
        }

        foreach ($_GET as $key => $value) {
            if ($key != 'submit' && $key != 'method' && $key != 'module') {
                $new_post[$key] = xss_clean($value);
            }
        }

        if (!empty($new_post)) {
            $string = AMP . 'params=' . urlencode(base64_encode(serialize($new_post)));
        }

        $return = $return . $string;

        ee()->functions->redirect($return);
    }

    public function remove_report()
    {
        $string = null;
        $new_get = $_GET;

        foreach ($new_get as $key => $value) {
            $new_get[$key] = xss_clean($value);
        }

        ee()->load->model('generic_model');

        $reports_model = new Generic_model('cartthrob_order_manager_reports');

        if ($reports_model->read($new_get['reportID'])) {
            $reports_model->delete($new_get['reportID']);
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/order_report'));
        }
    }

    public function run_report()
    {
        ee()->load->helper('array');

        $p_data = [];

        if (!isset($_GET['params']) && isset($_SERVER['QUERY_STRING'])) {
            $get = [];
            parse_str($_SERVER['QUERY_STRING'], $get);

            if (isset($get['params'])) {
                $_GET['params'] = $get['params'];
            }
        }

        if (isset($_GET['params'])) {
            $p_data = @unserialize(base64_decode(urldecode($_GET['params'])));
        } elseif (ee()->input->post('download')) {
            $p_data = array_merge($_GET, $_POST);
        }

        if (isset($p_data['where']) && is_array($p_data['where']) && strtolower(element('status', $p_data['where'])) == 'any') {
            unset($p_data['where']['status']);
            $any_status = 'any';
        }

        ee()->load->model('cartthrob_field_model');
        ee()->load->model('order_management_model');
        ee()->load->model('order_model');

        // 1. ========= AFFECTS PAGE LOAD AND CONTENTS

        ///////////// LOAD EXISTING REPORTS
        if (element('id', $p_data)) {
            ee()->load->model('generic_model');

            $reports_model = new Generic_model('cartthrob_order_manager_reports');
            $report_info = $reports_model->read(element('id', $p_data));

            if ($report_info) {
                ee()->load->helper('data_formatting');
                $settings = _unserialize($report_info['settings']);

                if (isset($settings['save_report'])) {
                    unset($settings['save_report']);
                }

                $p_data = array_merge($p_data, $settings);
            }
        }

        ////////// SAVE REPORTS
        if (element('save_report', $p_data)) {
            $settings = [];
            foreach ($p_data as $key => $value) {
                if (!in_array($key, $this->remove_keys) && !empty($value)) {
                    $settings[$key] = $value;
                }
            }

            ee()->load->library('localize');

            $key = $this->save_report($settings, element('report_title', $p_data, 'Untitled ' . ee()->localize->format_date('%m-%d-%Y %h:%i %a')));

            if ($key) {
                ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_report_saved_successfully'));
                ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/order_report'));
            } else {
                ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_report_not_saved'));
                ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/order_report'));
            }
        }

        // 2. ========= SET UP PARAMETERS FOR DB SEARCH
        $where_array = (array)element('where', $p_data);
        $where = [];

        $where_array = array_filter($where_array);

        //////// FILTER BY CUSTOMER
        if (element('customer_email', $p_data)) {
            $where_array['field_id_' . ee()->cartthrob->store->config('orders_customer_email')] = element('customer_email',
                $p_data);
        }

        foreach ($where_array as $key => $value) {
            switch ($key) {
                case 'date_start':
                    $where['entry_start_date'] = strtotime($value);
                    break;
                case 'date_finish':
                    $where['entry_end_date'] = strtotime($value);
                    break;
                case 'total_maximum':
                    if ($field_id = ee()->cartthrob->store->config('orders_total_field')) {
                        $where['field_id_' . $field_id . ' <'] = ee()->cartthrob->sanitize_number($value, true);
                    }
                    break;
                case 'total_minimum':
                    if ($field_id = ee()->cartthrob->store->config('orders_total_field')) {
                        $where['field_id_' . $field_id . ' >'] = ee()->cartthrob->sanitize_number($value, true);
                    }
                    break;
                default:
                    $where[$key] = $value;
            }
        }

        ////// FILTER BY CONTENT IN SPECIFIC FIELDS
        $search_fields = element('search_fields_settings', $p_data);
        $product_order_ids = [];

        if (!empty($search_fields)) {
            $order_items_field = null;
            $order_items_field_id = ee()->cartthrob->store->config('orders_items_field');

            if ($order_items_field_id) {
                $order_items_field = 'field_id_' . $order_items_field_id;
            }

            foreach ($search_fields['search_fields'] as $key => $value) {
                if (!empty($value['search_content']) && !empty($value['search_field'])) {
                    // this could get to be a really slow search on large DBs
                    if ($order_items_field && $value['search_field'] == $order_items_field) {
                        $order_items_where = [];

                        if (is_numeric($value['search_content'])) {
                            // i happen to know that we'll have to specify the DB prefix here
                            $order_items_where[ee()->db->dbprefix . 'cartthrob_order_items.entry_id'] = $value['search_content'];
                        } else {
                            // i happen to know that we'll have to specify the DB prefix here
                            // this will only get EXACT results. no... "like" search.
                            $order_items_where[ee()->db->dbprefix . 'cartthrob_order_items.title'] = $value['search_content'];
                        }

                        $purchased_products_status = null;

                        if (isset($p_data['where']['status'])) {
                            $purchased_products_status = $p_data['where']['status'];
                        } elseif (isset($any_status)) {
                            $purchased_products_status = 'any';
                        }

                        $product_entries = ee()->order_management_model->get_purchased_products([], $order_by = 'title', $sort = 'ASC', null, null, $order_items_where, $purchased_products_status);

                        if ($product_entries) {
                            foreach ($product_entries as $e) {
                                $ordz = ee()->order_management_model->get_related_orders_by_item(element('entry_id', $e));
                                if ($ordz) {
                                    foreach ($ordz as $k => $v) {
                                        $product_order_ids[] = element('order_id', $v);
                                    }
                                }
                            }
                        }
                    } else {
                        $where[$value['search_field']] = $value['search_content'];
                    }
                } elseif (!empty($value['search_field']) && !empty($value['search_not_empty'])) {
                    $where[$value['search_field']] = 'IS NOT NULL';
                }
            }
        }

        if ($product_order_ids) {
            $product_order_ids = array_unique($product_order_ids);
            $where[ee()->db->dbprefix . 'channel_titles.entry_id'] = $product_order_ids;
        }

        // remove empties
        $where = array_filter($where);

        // 3. ========= SET UP THE FIELDS THAT WILL BE SHOWN
        $show_fields = element('show_fields', $p_data);
        if (!$show_fields) {
            $show_fields = [
                'average_total',
                'discount',
                'orders',
                'shipping',
                'shipping_plus_tax',
                'subtotal',
                'subtotal_plus_tax',
                'tax',
                'total',
            ];
        }

        $order_fields = element('order_fields', $p_data);

        if (!$order_fields) {
            $order_fields = [
                'title' => 'title',
                'status' => 'status',
                'entry_date' => 'entry_date',
                'field_id_' . ee()->cartthrob->store->config('orders_total_field') => lang('cartthrob_order_manager_' . 'total'),
                'field_id_' . ee()->cartthrob->store->config('orders_subtotal_field') => lang('cartthrob_order_manager_' . 'subtotal'),
                'field_id_' . ee()->cartthrob->store->config('orders_tax_field') => lang('cartthrob_order_manager_' . 'tax'),
                'field_id_' . ee()->cartthrob->store->config('orders_shipping_field') => lang('cartthrob_order_manager_' . 'shipping'),
                'field_id_' . ee()->cartthrob->store->config('orders_discount_field') => lang('cartthrob_order_manager_' . 'discount'),
                'field_id_' . ee()->cartthrob->store->config('orders_items_field') => lang('cartthrob_order_manager_' . 'items'),
            ];
        }

        // 4. ========= OUTPUT THE CONTENT
        $header_data = $this->order_data($where, $order_by = 'title', $sort = 'DESC', 1, 0, $order_fields, 'CSV');

        if (empty($header_data['order_data'])) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_report_no_results'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/order_report'));
        }

        /// based on the single line item we grabbed above, we'll generate the CSV and standard headers
        // headers = standard, raw_headers = csv headers.
        $headers = $raw_headers = [];
        $params = [];

        foreach ($header_data['order_headers'] as $field_id => $type) {
            $label = ee()->cartthrob_field_model->get_field_label(str_replace('field_id_', '', $field_id));

            if ($label) {
                $headers[$field_id] = ['header' => $label];
                $params['fields'][$field_id] = $label;
                $raw_headers[] = $label;
            } else {
                if ($field_id == 'title') {
                    $headers[$field_id] = ['header' => lang('order_id')];
                    $params['fields'][$field_id] = lang('order_id');
                    $raw_headers[] = lang('order_id');
                } else {
                    $headers[$field_id] = ['header' => lang($field_id)];
                    $params['fields'][$field_id] = lang($field_id);
                    $raw_headers[] = lang($field_id);
                }
            }
        }

        ////// SEARCH BY PRODUCT ID
        $order_ids = null;

        if (element('product_id', $p_data)) {
            ee()->load->model('order_management_model');

            $order_ids = ee()->order_management_model->get_related_orders_by_item(element('product_id', $p_data));

            if ($order_ids) {
                foreach ($order_ids as $key => &$value) {
                    $value = $value['order_id'];
                }
            }
        }

        ///// EXPORT TO CSV
        if (element('download', $p_data)) {
            ee()->load->library('cartthrob_file');

            $filename = element('filename', $p_data);
            $format = element('download', $p_data, 'xls');

            if (!$filename) {
                $filename = 'untitled';
            }

            if (element('report_title', $p_data)) {
                $filename = element('report_title', $p_data);
            }

            if (strpos($filename, '.' . $format) === false) {
                $filename = $filename . '.' . $format;
            }

            ee()->cartthrob_file->output_streaming_file_headers($filename);

            $limit = $this->limit;

            if ($order_ids) {
                $where['entry_id'] = $order_ids;
                $params['where'] = $where;
            }

            $order_totals = ee()->order_model->order_totals($where);

            for ($i = 0; $i < $order_totals['orders']; $i += $limit) {
                if ($order_ids) {
                    ee()->db->where_in('entry_id', $order_ids);
                }

                $data = $this->order_data($where, $order_by = 'title', $sort = 'DESC', $limit, $offset = $i, $order_fields, 'CSV');
                $send_headers = isset($send_headers) ? [] : $data['order_headers'];

                // must set $return_data to true so that this can stream output as a download in case it's huge.
                echo $this->export_csv($data['order_data'], $send_headers, $filename, $return_data = true, $format);
            }

            // once everythign's done streaming... kill the process
            die();
        } else { ///// LOAD THE PAGE
            $this->initialize();
            ee()->load->library('table');

            ee()->table->clear();

            $defaults = [
                'sort' => ['title' => 'desc'],
            ];

            if ($order_ids) {
                $where['entry_id'] = $order_ids;
                $params['where'] = $where;
            } elseif (element('customer_email', $p_data) || element('product_id', $p_data)) {
                $params['where'] = $where;
            } elseif ($where) {
                $params['where'] = $where;
            }

            $params['limit'] = $this->limit;

            $defaults['offset'] = ee()->input->get_post('rownum', 0);

            ee()->table->set_heading($raw_headers);

            ee()->load->library('pagination');

            $data_table = $this->order_datasource($defaults, $params);

            foreach ($data_table['rows'] as $row) {
                ee()->table->add_row(array_values($row));
            }

            ee()->pagination->initialize([
                // Pass the relevant data to the paginate class
                'base_url' => ee('CP/URL')->make('addons/settings/cartthrob_order_manager/run_report'),
                'total_rows' => $data_table['pagination']['total_rows'],
                'per_page' => $data_table['pagination']['per_page'],
                'page_query_string' => true,
                'query_string_segment' => 'rownum',
                'full_tag_open' => '<div style="float: none;" class="paginate" title="' . $data_table['pagination']['total_rows'] . ' total entries"><ul>',
                'full_tag_close' => '</ul></div>',
                'first_tag_open' => '<li>',
                'first_tag_close' => '</li>',
                'prev_tag_open' => '<li>',
                'prev_tag_close' => '</li>',
                'cur_tag_open' => '<li><a href="" class="act">',
                'cur_tag_close' => '</a></li>',
                'num_tag_open' => '<li>',
                'num_tag_close' => '</li>',
                'next_tag_open' => '<li>',
                'next_tag_close' => '</li>',
                'last_tag_open' => '<li>',
                'last_tag_close' => '</li>',
                'prev_link' => 'Prev',
                'next_link' => 'Next',
                'first_link' => 'First',
                'last_link' => 'Last',
            ]);

            $content = ee()->table->generate() . ee()->pagination->create_links();
            $data['order_table'] = $content;

            // adding the order totals that appear at the top of the screen
            $data = array_merge($data, $this->get_totals($where, $show_fields));

            // adding in any of the get / post variables back into the page in case we want to save the report or generate a CSV using the same parameters
            $data['hidden_inputs'] = null;
            $data['report_title'] = 'Report';

            if (element('report_title', $p_data)) {
                $data['report_title'] = element('report_title', $p_data);
            }

            foreach ($p_data as $key => $value) {
                if (!in_array($key, $this->remove_keys) && !empty($value)) {
                    if ($key == 'search_fields_settings') {
                        foreach ($value as $k => $v) {
                            foreach ($v as $kk => $vv) {
                                foreach ($vv as $kkl => $vvv) {
                                    $data['hidden_inputs'] .= form_hidden($key . '[' . $k . '][' . $kk . '][' . $kkl . ']', $vvv);
                                }
                            }
                        }
                    } else {
                        $data['hidden_inputs'] .= form_hidden($key, $value);
                    }
                }
            }

            foreach ($_GET as $key => $value) {
                if (!in_array($key, $this->remove_keys) && !empty($value)) {
                    $data['hidden_inputs'] .= form_hidden($key, $value);
                }
            }

            // CUSTOMER SPECIFIC. If this is customer report, we'll add a custom header.
            if (element('customer_email', $p_data)) {
                $customer = ee()->order_management_model->get_customers([
                    'field_id_' . ee()->cartthrob->store->config('orders_customer_email') => element('customer_email', $p_data),
                ], $order_by = 'author_id', $sort = 'DESC', $limit = 1, $offset = null);

                $first_name = null;
                $last_name = null;

                if (!empty($customer[0]['field_id_' . ee()->cartthrob->store->config('orders_billing_first_name')])) {
                    $first_name = $customer[0]['field_id_' . ee()->cartthrob->store->config('orders_billing_first_name')];
                }

                if (!empty($customer[0]['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')])) {
                    $last_name = $customer[0]['field_id_' . ee()->cartthrob->store->config('orders_billing_last_name')];
                }

                $data['report_title'] = lang('cartthrob_order_manager_customer_report_title') . ' ' . $first_name . ' ' . $last_name;
            } elseif (element('product_id', $p_data)) {
                $data['report_title'] = lang('cartthrob_order_manager_order_by_product_report_title') . ': (' . element('product_id', $p_data) . ')';
            }

            // adding the filename dynamically based on the report title
            $data['hidden_inputs'] .= form_hidden('filename', $data['report_title']);
            $data['export_csv'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . __FUNCTION__, ['return' => __FUNCTION__]));

            return ee()->mbr_addon_builder->load_view(__FUNCTION__, compact('data'));
        }
    }

    public function save_report($settings, $title)
    {
        ee()->load->model('generic_model');

        $reports = new Generic_model('cartthrob_order_manager_reports');

        $report_data = [
            'settings' => serialize($settings),
            'report_title' => $title,
            'type' => 'order',
        ];

        $key = $reports->create($report_data);

        return $key;
    }

    /**
     * This function gets order data from the order model and formats it to our requirements for a standard output, or csv.
     *
     * @param $where
     * @param string $orderBy
     * @param string $sort
     * @param null $limit
     * @param int $offset
     * @param array $fields
     * @param string $format
     * @return array
     */
    public function order_data($where, $orderBy = 'title', $sort = 'DESC', $limit = null, $offset = 0, $fields = [], $format = 'REPORT')
    {
        ee()->load->model('order_management_model');
        ee()->load->model('cartthrob_field_model');

        $data = [];

        ///// GET THE ORDERS

        foreach ($where as $key => $value) {
            if (!is_array($value) && $value == 'IS NOT NULL') {
                unset($where[$key]);
                ee()->db->where($key . " <> ''", null, false);
                ee()->db->where($key . ' IS NOT NULL', null, false);
            }
        }

        $orders = ee()->order_management_model->getOrders($where, $orderBy, $sort, $limit, $offset);

        if (!$orders) {
            $data['order_data'] = [];
            $data['order_headers'] = [];

            return $data;
        }

        ///// GET THE FIELD TYPES FOR EACH FIELD. SOME OF THEM WE'RE GOING TO HAVE TO PARSE
        $field_types = [];

        foreach ($fields as $field_id => $label) {
            if ($field_id == 'field_id_') {
                unset($fields[$field_id]);
                continue;
            }

            $field = ee()->cartthrob_field_model->get_field_by_id(str_replace('field_id_', '', $field_id));

            if ($field) {
                $field_types[$field_id] = $field['field_type'];
            } else {
                $field_types[$field_id] = 'text';
            }
        }

        // Default columns if none are supplied
        $default_item_columns = array_fill_keys([
            'row_id',
            'row_order',
            'order_id',
            'entry_id',
            'title',
            'quantity',
            'price',
            'price_plus_tax',
            'weight',
            'shipping',
            'no_tax',
            'no_shipping',
            'extra',
        ], '');

        // Cleaning the orders
        $country_code_field = ee()->cartthrob_field_model->get_field_by_name('order_country_code');
        $state_field = ee()->cartthrob_field_model->get_field_by_name('order_billing_state');
        $ip_address_field = ee()->cartthrob_field_model->get_field_by_name('order_ip_address');

        ee()->load->helper('array');
        ee()->load->library('localize');
        ee()->load->model('order_model');

        foreach ($orders as $key => &$ord) {
            $order_id = $ord['entry_id'];
            $title = $ord['title'];
            $ord1 = $ord;
            $ord = array_intersect_key($ord, $field_types);

            foreach ($field_types as $key => $type) {
                $href = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]);

                if ($format == 'REPORT') {
                    // create a link to the entry
                    if ($key == 'entry_id') {
                        $ord['entry_id'] = "<a href='" . $href . "'>" . $order_id . ' &raquo;</a>';
                    }

                    // create a link to the entry
                    if ($key == 'title') {
                        $ord['title'] = "<a href='" . $href . "'>" . $title . ' &raquo;</a>';
                    }
                }

                // format the entry date
                if ($key == 'entry_date') {
                    $ord['entry_date'] = ee()->localize->format_date('%m-%d-%Y %h:%i %a', $ord['entry_date']);
                }

                if (isset($country_code_field['field_id']) && isset($state_field['field_id']) && isset($ip_address_field['field_id'])) {
                    $this->multi_location(
                        element('field_id_' . $country_code_field['field_id'], $ord1),
                        element('field_id_' . $state_field['field_id'], $ord1),
                        element('field_id_' . $ip_address_field['field_id'], $ord1)
                    );
                }

                if (!is_null($this->currency_code)) {
                    ee()->number->set_prefix($this->currency_code);
                }

                if (!is_null($this->prefix)) {
                    ee()->number->set_prefix($this->prefix);
                }

                if (!is_null($this->dec_point)) {
                    ee()->number->set_dec_point($this->dec_point);
                }

                switch ($type) {
                    case 'cartthrob_order_items':
                        $order_items = ee()->order_model->get_order_items([$order_id]);
                        $items = null;

                        if (is_array($order_items)) {
                            foreach ($order_items as $item) {
                                if ($format == 'REPORT') {
                                    // create a link to the item
                                    $items .= "<a href='" . ee('CP/URL')->make('publish/edit/entry/' . $item['entry_id']) . "' >";
                                    $items .= $item['title'];
                                    $items .= '(' . $item['entry_id'] . ')';
                                    $items .= '</a>';
                                    $items .= '<br>';
                                    $items .= $item['quantity'] . ' x ' . ee()->number->format($item['price']) . '(' . ee()->number->format($item['price_plus_tax']) . ')';
                                    $items .= '<br>';
                                    $item_options = array_diff_key($item, $default_item_columns);

                                    if ($item_options) {
                                        foreach ($item_options as $option_key => $option) {
                                            if (is_array($option)) {
                                                foreach ($option as $opt) {
                                                    if ($opt['title']) {
                                                        $items .= $option_key . ': ' . $opt['title'] . '<br>';
                                                    }
                                                }
                                            } else {
                                                $items .= $option_key . ': ' . $option . '<br>';
                                            }
                                        }
                                    }
                                    $items .= '<br><br>';
                                } else {
                                    $items .= $item['entry_id'] . ':' . $item['title'] . ':' . $item['quantity'] . ':' . $item['price'] . '|';
                                }
                            }
                        }
                        $ord[$key] = $items;
                        break;
                    case 'cartthrob_price_simple':
                        if ($format == 'REPORT') {
                            $ord[$key] = ee()->number->format($ord[$key]);
                        }
                        break;
                }
            }
        }

        $single_order = array_keys($orders[0]);

        $entry_keys = [
            'entry_id' => 'entry_id',
            'title' => 'title',
            'status' => 'status',
            'year' => 'year',
            'month' => 'month',
            'day' => 'day',
            'entry_date' => 'entry_date',
        ];

        // because we're getting back all of this stuff in a specific format... we can't just do normal sorting.
        $available_keys = array_intersect(array_values($field_types), $entry_keys);

        $headers = array_merge(array_flip($single_order), $available_keys);
        $headers = array_merge($headers, $fields);

        $data['order_data'] = $orders;
        $data['order_headers'] = $headers;

        return $data;
    }

    public function multi_location($country_code, $state = null, $ip_address = null)
    {
        ee()->load->library('get_settings');

        $loaded = null;
        $settings = ee()->get_settings->settings('cartthrob_multi_location');

        if ($settings) {
            $european_union_array = [
                'AUT',
                'BEL',
                'BGR',
                'CYP',
                'CZE',
                'DNK',
                'EST',
                'FIN',
                'FRA',
                'DEU',
                'GRC',
                'HUN',
                'IRL',
                'ITA',
                'LVA',
                'LTU',
                'LUX',
                'MLT',
                'NLD',
                'POL',
                'PRT',
                'ROU',
                'ROM',
                'SVK',
                'SVN',
                'ESP',
                'SWE',
                'GBR',
            ];

            $europe_array = array_merge([
                'HRV',
                'MKD',
                'ISL',
                'MNE',
                'SRB',
                'TUR',
                'ALB',
                'AND',
                'ARM',
                'AZE',
                'BLR',
                'BIH',
                'GEO',
                'LIE',
                'MDA',
                'MCO',
                'NOR',
                'RUS',
                'SMR',
                'CHE',
                'UKR',
                'VAT',
            ], $european_union_array);

            $us_offshore = ['HI', 'AK'];

            ee()->load->library('locales');
            ee()->load->library('number');

            $country_code = ee()->locales->alpha3_country_code($country_code);

            if ($ip_address && ee()->db->table_exists('ip2nation')) {
                ee()->load->add_package_path(APPPATH . 'modules/ip_to_nation/');
                ee()->load->model('ip_to_nation_data', 'ip_data');

                $country_code = ee()->ip_data->find($ip_address);

                if ($country_code !== false) {
                    if (!isset(ee()->session->cache['ip_to_nation']['countries'])) {
                        if (include(APPPATH . 'config/countries.php')) {
                            /*
                             * The countries.php file above contains the countries variable.
                             *
                             * @var array $countries
                             */
                            ee()->session->cache['ip_to_nation']['countries'] = $countries;
                        }
                    }

                    $country_code = strtoupper($country_code);

                    if ($country_code == 'UK') {
                        $country_code = 'GB';
                    }
                }

                $country_code = ee()->locales->alpha3_country_code($country_code);
            }

            if (isset($settings['other'])) {
                foreach ($settings['other'] as $other) {
                    if ($country_code == $other['country']
                        || $other['country'] == 'global'
                        || ($other['country'] == 'non-continental_us' && in_array($state, $us_offshore))
                        || ($other['country'] == 'europe' && in_array($country_code, $europe_array))
                        || ($other['country'] == 'european_union' && in_array($country_code, $european_union_array))) {
                        if (!empty($other['currency_code'])
                        ) {
                            // going to set the local var, and the config as well. must do this for order totals and other things that use the number lib.
                            // each time number lib gets used, seems the set_ methods don't last till the next use
                            $this->currency_code = $other['currency_code'];
                            ee()->cartthrob->cart->set_config('number_format_defaults_currency_code',
                                $other['currency_code']);
                        }

                        if (!empty($other['prefix'])) {
                            // going to set the local var, and the config as well. must do this for order totals and other things that use the number lib.
                            // each time number lib gets used, seems the set_ methods don't last till the next use
                            $this->prefix = $other['prefix'];
                            ee()->cartthrob->cart->set_config('number_format_defaults_prefix', $other['prefix']);
                        }

                        if (!empty($other['dec_point'])) {
                            switch ($other['dec_point']) {
                                case 'comma':
                                    $dec_point = ',';
                                    break;
                                case 'period':
                                    $dec_point = '.';
                                    break;
                                default:
                                    $dec_point = '.';
                            }

                            // going to set the local var, and the config as well. must do this for order totals and other things that use the number lib.
                            // each time number lib gets used, seems the set_ methods don't last till the next use
                            $this->dec_point = $dec_point;
                            ee()->cartthrob->cart->set_config('number_format_defaults_dec_point', $dec_point);
                        }
                        break;
                    }
                }
            }

            return $loaded = $settings;
        }

        $loaded = false;

        return $loaded;
    }

    public function order_datasource($state, $params = [])
    {
        $offset = $state['offset'];
        $limit = isset($params['limit']) ? $params['limit'] : $this->limit;
        $sub_params = ['order_by' => [], 'sort' => []];

        if (!empty($state['sort'])) {
            foreach ($state['sort'] as $key => $value) {
                $sub_params['order_by'][] = $key;
                $sub_params['sort'][] = $value;
            }
        }

        $where = [];
        $fields = [];

        if (isset($params['where'])) {
            $where = $params['where'];
        }

        if (isset($params['fields'])) {
            $fields = $params['fields'];
        }

        $rows = $this->order_data($where, $sub_params['order_by'], $sub_params['sort'], $limit, $offset, $fields, 'REPORT');

        ee()->load->model('order_model');

        $order_totals = ee()->order_model->order_totals($where);

        return [
            'rows' => $rows['order_data'],
            'pagination' => [
                'per_page' => $limit,
                'total_rows' => $order_totals['orders'],
            ],
        ];
    }

    public function get_totals($where, $totals_fields)
    {
        ee()->load->model('order_model');
        ee()->load->library('table');

        $data = [];
        $show_fields = [];

        // Start creating the table to output this stuff.
        ee()->table->clear();
        ee()->table->set_template([
            'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">',
            'heading_cell_start' => '<th colspan="2">',
        ]);
        ee()->table->set_heading(lang('cartthrob_order_manager_totals_header'));

        $order_totals = ee()->order_model->order_totals($where);

        // only going to display the selected fields for this report.
        foreach ($totals_fields as $value) {
            if (isset($order_totals[$value])) {
                if ($value != 'orders') {
                    if (is_numeric($order_totals[$value])) {
                        $show_fields[] = [
                            lang('cartthrob_order_manager_' . $value),
                            ee()->number->format($order_totals[$value]),
                        ];
                    } else {
                        $show_fields[] = [lang('cartthrob_order_manager_' . $value), $order_totals[$value]];
                    }
                }
            } else {
                $show_fields[] = [
                    lang('cartthrob_order_manager_' . $value),
                    lang('cartthrob_order_manager_not_available'),
                ];
            }
        }

        $data['total_data'] = $order_totals;
        $data['total_table'] = ee()->table->generate($show_fields);

        return $data;
    }

    public function view()
    {
        $data = ['orders' => []];
        $offset = ee()->input->get_post('rownum');

        $this->initialize();

        ee()->db->where('channel_id', ee()->cartthrob->store->config('orders_channel'));

        $total = ee()->db->select('entry_id')
            ->from('channel_data')
            ->count_all_results();

        ee()->load->library('pagination');
        ee()->pagination->initialize(ee()->mbr_addon_builder->pagination_config(__FUNCTION__, $total, $this->limit));

        $pagination = ee()->pagination->create_links();

        if ($offset) {
            ee()->db->offset($offset);
        }

        ee()->db
            ->where('channel_id', ee()->cartthrob->store->config('orders_channel'))
            ->limit($this->limit);

        // @TODO need to pull only data from this site id

        $query = ee()->db->select('entry_id')
            ->from('channel_data')
            ->order_by('entry_id', 'desc')
            ->get();

        ee()->load->model('order_model');

        foreach ($query->result_array() as $row) {
            $order = ee()->order_model->get_order($row['entry_id']);

            $order['order_status_mapped'] = ee()->cartthrob->store->config('orders_default_status');

            if (isset($order['order_status'])) {
                if ($status = ee()->cartthrob->store->config('orders_status_' . $order['order_status'])) {
                    $order['order_status_mapped'] = $status;
                }

                if ($status = ee()->cartthrob->store->config('orders_' . $order['order_status'] . '_status')) {
                    $order['order_status_mapped'] = $status;
                }
            }

            $data['orders'][] = $order;
        }

        if (empty($data['orders'])) {
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_none'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/om_sales_dashboard'));
        }

        ee()->javascript->output("    
            $('.show_quick_view, .hide_quick_view').click(function() {
                $(this).hide();
                
                if ($(this).attr('class') == 'show_quick_view') {
                    $(this).closest('tr').nextAll('.quick_view:first').show();
                    $(this).next('.hide_quick_view').show(); 
                } else {
                    $(this).closest('tr').nextAll('.quick_view:first').hide();
                    $(this).prev('.show_quick_view').show(); 
                }
            });
        ");

        if ($pagination === false) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_none'));
            // @TODO put in a redirect to somewhere
        }

        return ee()->mbr_addon_builder->load_view(
            __FUNCTION__,
            [
                __FUNCTION__ => $data,
                'pagination' => $pagination,
                'form_edit' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/update_order', ['return' => __FUNCTION__])),
                'delete_href' => ee('CP/URL')->make('addons/settings/' . $this->module_name . '/delete', ['id' => '']),
                'href_entry' => ee('CP/URL')->make('publish/edit/entry/id'),
                'edit_href' => ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => '']),
            ]
        );
    }

    public function update_order()
    {
        ee()->load->model('order_model');

        $order_ids = ee()->input->post('id');
        $redirect_id = null;
        $method = ee()->input->get('return', true);

        if (is_array($order_ids)) {
            foreach ($order_ids as $key => $id) {
                $order_update = [];
                foreach ($_POST as $post_key => $post_value) {
                    $post_value = ee()->input->post($post_key);
                    // warning: this will not find any "name" key because that's one of the ignored keys
                    if (!in_array($post_key, $this->remove_keys)) {
                        if (is_array($post_value)) {
                            if (array_key_exists($id, $post_value)) {
                                $order_update[$post_key] = $post_value[$id];
                            }
                        } else {
                            $order_update[$post_key] = $post_value;
                        }
                    }

                    if ($post_key == 'toggle_status' && !empty($post_value)) {
                        $order_update['status'] = (string)$post_value;
                    }

                    if (array_key_exists('status', $order_update) && empty($order_update['status'])) {
                        unset($order_update['status']);
                    }
                }

                // let's get the old status to see if we need to send status update emails.
                ee()->load->model('cartthrob_entries_model');

                $current_entry = ee()->cartthrob_entries_model->entry($id);
                $current_status = $current_entry['status'];

                if (isset($order_update['status']) && ($order_update['status'] !== $current_status)) {
                    // the status has changed. Let's send the email event
                    $current_entry['previous_status'] = $current_status;
                    $current_entry['status'] = $order_update['status'];
                    $this->sendNotifications($event = 'status_change', $current_entry);
                }

                ee()->order_model->update_order($id, $order_update);
            }

            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_orders_updated'));
        } elseif ($order_ids) {
            $order_update = [];

            foreach ($_POST as $post_key => $post_value) {
                // warning: this will not find any "name" key because that's one of the ignored keys
                if (!in_array($post_key, $this->remove_keys)) {
                    $order_update[$post_key] = $post_value;
                }
            }

            $redirect_id = '&id=' . $order_ids;

            ee()->order_model->update_order($id, $order_update);
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_order_updated'));
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . $method) . $redirect_id);
    }

    /**
     * Send a notification email
     *
     * @param string $event
     * @param array $variables
     */
    private function sendNotifications($event, $variables = [])
    {
        ee()->load->library('cartthrob_emails');

        if ($event == Event::TYPE_STATUS_CHANGE) {
            $emails = ee()->cartthrob_emails->get_email_for_event($event, $variables['previous_status'], $variables['status']);
        } else {
            $emails = ee()->cartthrob_emails->get_email_for_event($event);
        }

        if (!empty($emails)) {
            ee()->load->helper('array');

            foreach ($emails as $emailDetails) {
                $emailDetails['variables'] = $variables;

                if ($event == 'completed' && ee()->input->post('email_address')) {
                    // we don't want to send any template not directed to {customer_email}
                    // otherwise we might send emails to customers that should be only for vendors or fulfillment.
                    if (!str_contains(element('to', $emailDetails), '{customer_email}')) {
                        continue;
                    }

                    $emailDetails['to'] = ee()->input->post('email_address');
                } elseif (ee()->input->post('email_address')) {
                    $emailDetails['to'] = ee()->input->post('email_address');
                } elseif (str_contains(element('to', $emailDetails), '{customer_email}')) {
                    $emailDetails['to'] = element('order_customer_email', $variables);
                }

                if (str_contains(element('from_name', $emailDetails), '{customer_name}')) {
                    $emailDetails['from_name'] = element('order_customer_full_name', $variables);
                }

                if (str_contains(element('fromReplyTo', $emailDetails), '{customer_email}')) {
                    $emailDetails['fromReplyTo'] = element('order_customer_email', $variables);
                }

                if (str_contains(element('fromReplyToName', $emailDetails), '{customer_name}')) {
                    $emailDetails['fromReplyToName'] = element('order_customer_full_name', $variables);
                }

                if (ee()->input->post('email_subject')) {
                    $emailDetails['subject'] = ee()->input->post('email_subject');
                }

                ee()->cartthrob_emails->send_email($emailDetails);
            }
        }
    }

    public function print_invoice()
    {
        ee()->load->library('template_helper');

        $vars['entry_id'] = ee()->input->get_post('id');
        $template = ee()->get_settings->get($this->module_name, 'invoice_template');

        die(ee()->template_helper->fetch_and_parse($template, $vars));
    }

    public function print_packing_slip()
    {
        ee()->load->library('template_helper');

        $vars['entry_id'] = ee()->input->get_post('id');
        $template = ee()->get_settings->get($this->module_name, 'packing_slip_template');

        die(ee()->template_helper->fetch_and_parse($template, $vars));
    }

    public function print_custom_template()
    {
        if (ee()->get_settings->get($this->module_name, 'custom_templates')) {
            foreach (ee()->get_settings->get($this->module_name, 'custom_templates') as $key => $template) {
                if ($key == ee()->input->get_post('custom_template_id')) {
                    ee()->load->library('template_helper');
                    $vars['entry_id'] = ee()->input->get_post('id');
                    $template = $template['custom_template'];
                    echo ee()->template_helper->fetch_and_parse($template, $vars);
                }
            }
        }

        // have to exit otherwise EE will do all of its auto-outputtign business
        exit;
    }

    public function email_custom_template()
    {
        $redirect_id = '';
        $method = ee()->input->get('return', true);

        if (ee()->get_settings->get($this->module_name, 'custom_templates')) {
            foreach (ee()->get_settings->get($this->module_name, 'custom_templates') as $key => $template) {
                if ($key == ee()->input->get_post('custom_template_id')) {
                    ee()->load->library('cartthrob_emails');

                    // using email address and such from completed email!!!!!!!!!
                    $emailDetails = ee()->cartthrob_emails->get_email_for_event(Event::TYPE_ORDER_COMPLETED);

                    ee()->load->library('template_helper');
                    $vars['entry_id'] = ee()->input->get_post('id');
                    $order_id = ee()->input->post('id');
                    $redirect_id = null;

                    $variables = $this->loadCartWithOrderById($order_id);

                    $emailDetails['variables'] = $variables;
                    $emailDetails['messageTemplate'] = $template['custom_template'];

                    if (ee()->input->post('email_address')) {
                        $emailDetails['to'] = ee()->input->post('email_address');
                    }
                    if (ee()->input->post('email_subject')) {
                        $emailDetails['subject'] = ee()->input->post('email_subject');
                    }

                    ee()->cartthrob_emails->send_email($emailDetails);

                    $redirect_id = '&id=' . $order_id;
                }
            }
        }

        ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_custom_email_sent'));
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . $method) . $redirect_id);
    }

    /**
     * @param $orderId
     * @return array
     */
    private function loadCartWithOrderById($orderId)
    {
        ee()->load->helper('array');
        ee()->load->library([
            'api/api_cartthrob_payment_gateways',
            'api/api_cartthrob_tax_plugins',
            'cartthrob_payments',
            'form_builder',
            'encrypt',
        ]);
        ee()->load->model('order_model');

        ee()->cartthrob->cart->set_calculation_caching(false);

        $gateway = ee()->input->post('gateway') ?
            xss_clean(ee()->encrypt->decode(ee()->input->post('gateway'))) :
            ee()->cartthrob->store->config('payment_gateway');

        $order = ee()->order_model->get_order($orderId);

        $order = array_merge($order, [
            'entry_id' => element('entry_id', $order),
            'order_id' => element('entry_id', $order),
            'shipping' => element('entry_id', $order),
            'shipping_plus_tax' => element('order_shipping_plus_tax', $order),
            'tax' => element('order_tax', $order),
            'subtotal' => element('order_subtotal', $order),
            'subtotal_plus_tax' => element('order_subtotal_plus_tax', $order),
            'discount' => element('order_discount', $order),
            'total' => element('order_total', $order),
            'authorized' => true,
            'transaction_id' => element('order_transaction_id', $order),
            'credit_card_number' => null,
            'create_user' => false,
            'group_id' => null,
            'create_user_data' => null,
            'payment_gateway' => $gateway,
            'auth' => [
                'authorized' => true,
                'transaction_id' => element('order_transaction_id', $order),
            ],
        ]);

        ee()->cartthrob->cart->set_order($order);
        ee()->cartthrob->cart->save();

        return $order;
    }

    public function add_tracking_to_order()
    {
        $entry_id = ee()->input->post('id');
        $tracking_number = ee()->input->post('order_tracking_number');
        $note = ee()->input->post('order_shipping_note');
        $status = ee()->input->post('status');
        // some servers are emptying the _GET after emailing so capture the method at the beginning for the return
        $method = ee()->input->get('return', true);

        if (is_array($entry_id)) {
            foreach ($entry_id as $key => $value) {
                $variables = $this->loadCartWithOrderById($value);
                $variables['entry_id'] = $value;
                $variables['order_tracking_number'] = $tracking_number[$key];
                $variables['order_shipping_note'] = $note[$key];
                $variables['status'] = $status[$key];

                $this->save_tracking($value, $variables);

                $this->sendNotifications(ucwords($this->module_name) . '_tracking_added_to_order', $variables);
            }
            $redirect_id = null;
        } else {
            $variables = $this->loadCartWithOrderById($entry_id);
            $variables['entry_id'] = $entry_id;
            $variables['order_tracking_number'] = $tracking_number;
            $variables['order_shipping_note'] = $note;
            $variables['status'] = $status;

            $this->save_tracking($entry_id, $variables);

            $redirect_id = '&id=' . $entry_id;
            // when notifications are registered by third party, need to add module name (case sensitive)
            $this->sendNotifications(ucwords($this->module_name) . '_tracking_added_to_order', $variables);
        }

        ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_tracking_added'));
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . $method) . $redirect_id);
    }

    public function save_tracking($order_id, $constants = [])
    {
        ee()->load->model('order_model');

        $order_update = [
            'status' => $constants['status'],
            'order_tracking_number' => $constants['order_tracking_number'],
            'order_shipping_note' => $constants['order_shipping_note'],
        ];

        ee()->order_model->update_order($order_id, $order_update);

        return true;
    }

    // new method added for capturing prior authorized orders

    /**
     * Resend completed order notification email
     */
    public function resend_email()
    {
        $orderId = ee()->input->post('id');
        $method = ee()->input->get('return', true);
        $redirectId = "&id={$orderId}";

        $this->sendNotifications($event = 'completed', $this->loadCartWithOrderById($orderId));

        ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_email_resent'));
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . $method) . $redirectId);
    }

    // new method added for voiding orders that aren't captured

    public function refund()
    {
        $order_id = ee()->input->post('id');

        if (!$order_id) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_refund_failed'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/view'));
        }

        ee()->load->library([
            'form_validation',
            'encrypt',
            'form_builder',
            'api/api_cartthrob_payment_gateways',
            'api/api_cartthrob_tax_plugins',
            'cartthrob_payments',
        ]);
        ee()->load->model('purchased_items_model');
        ee()->load->model('order_model');

        $original_order = ee()->order_model->get_order($order_id);

        $gateway = isset($original_order['order_payment_gateway']) ? $original_order['order_payment_gateway'] : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_problem_loading_gateway') . ' ' . lang($this->module_name . '_refund_failed')
            );
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }
        if (!method_exists(ee()->cartthrob_payments->gateway(), 'refund')) {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_gateway_could_not_process_refund') . ' ' . lang($this->module_name . '_refund_failed')
            );
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        $partial_refund = false;
        $refund = 0;

        if (ee()->input->post('total')) {
            $refund = ee()->cartthrob->sanitize_number(ee()->input->post('total'));
        }

        if (!ee()->input->post('total') && (ee()->input->post('subtotal') || ee()->input->post('shipping') || ee()->input->post('tax'))) {
            $refund = ee()->cartthrob->sanitize_number(ee()->input->post('subtotal')) + ee()->cartthrob->sanitize_number(ee()->input->post('tax')) + ee()->cartthrob->sanitize_number(ee()->input->post('shipping'));
            $partial_refund = true;
        }

        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->refund($original_order['order_transaction_id'], $refund, $original_order['order_last_four']);

        if ($state->isAuthorized()) {
            if ($partial_refund) {
                $order_update = [];

                if (ee()->cartthrob->store->config('orders_total_field')) {
                    $order_update['total'] = $original_order['field_id_' . ee()->cartthrob->store->config('orders_total_field')] - $refund;
                }

                if (ee()->cartthrob->store->config('orders_subtotal_field')) {
                    $order_update['subtotal'] = $original_order['field_id_' . ee()->cartthrob->store->config('orders_subtotal_field')] - ee()->cartthrob->sanitize_number(ee()->input->post('subtotal'));
                }

                if (ee()->cartthrob->store->config('orders_subtotal_plus_tax_field')) {
                    $order_update['subtotal_plus_tax'] = $original_order['field_id_' . ee()->cartthrob->store->config('orders_subtotal_plus_tax_field')] - ee()->cartthrob->sanitize_number(ee()->input->post('subtotal')) - ee()->cartthrob->sanitize_number(ee()->input->post('tax'));
                }

                if (ee()->cartthrob->store->config('orders_tax_field')) {
                    $order_update['tax'] = $original_order['field_id_' . ee()->cartthrob->store->config('orders_tax_field')] - ee()->cartthrob->sanitize_number(ee()->input->post('tax'));
                }

                if (ee()->cartthrob->store->config('orders_shipping_field')) {
                    $order_update['shipping'] = $original_order['field_id_' . ee()->cartthrob->store->config('orders_shipping_field')] - ee()->cartthrob->sanitize_number(ee()->input->post('shipping'));
                }

                if ($order_update) {
                    ee()->order_model->update_order($order_id, $order_update);
                }
            } else {
                foreach (ee()->order_model->get_order_items($order_id) as $item) {
                    $item_options = array_diff_key($item, $this->default_columns);
                    ee()->load->model('product_model');
                    ee()->product_model->increase_inventory($item['entry_id'], $item['quantity'], $item_options);
                }

                ee()->cartthrob_payments->setStatus(Cartthrob_payments::STATUS_REFUNDED, $state, $order_id, $send_email = true);

                $order_update = [
                    'order_refund_id' => $state['transaction_id'],
                ];

                ee()->order_model->update_order($order_id, $order_update);
            }

            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_refund_succeeded'));
        } else {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_refund_failed') . ' - ' . $state->getMessage()
            );
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
    }

    public function authorize_and_charge()
    {
        $order_id = ee()->input->post('id');

        if (!$order_id) {
            // @TODO redirect
        }

        $this->loadCartWithOrderById($order_id);

        ee()->load->library([
            'form_validation',
            'encrypt',
            'form_builder',
            'api/api_cartthrob_payment_gateways',
            'api/api_cartthrob_tax_plugins',
            'cartthrob_payments',
        ]);
        ee()->load->model('purchased_items_model');
        ee()->load->model('order_model');

        $original_order = ee()->order_model->get_order($order_id);
        $gateway = isset($original_order['order_payment_gateway']) ? $original_order['order_payment_gateway'] : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        if (!ee()->api_cartthrob_payment_gateways->set_gateway($gateway)->gateway()) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_problem_loading_gateway'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        if (!method_exists(ee()->cartthrob_payments->gateway(), 'auth_and_charge')) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_gateway_could_not_process'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        // @TODO create this method. need to make gateways that support auth + auth and charge.
        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->setGateway($gateway)->auth_and_charge();

        if ($state->isAuthorized()) {
            ee()->session->set_flashdata(
                $this->module_name . '_system_message',
                lang($this->module_name . '_auth_and_charge_succeeded')
            );
        } else {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_auth_and_charge_failed') . ' - ' . $state->getMessage()
            );
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
    }

    public function capture()
    {
        $order_id = ee()->input->post('id');

        if (!$order_id) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_capture_failed'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/view'));
        }

        ee()->load->library([
            'form_validation',
            'encrypt',
            'form_builder',
            'api/api_cartthrob_payment_gateways',
            'api/api_cartthrob_tax_plugins',
            'cartthrob_payments',
        ]);
        ee()->load->model('purchased_items_model');
        ee()->load->model('order_model');
        ee()->load->model('cartthrob_field_model');

        $original_order = ee()->order_model->get_order($order_id);

        $gateway = isset($original_order['order_payment_gateway']) ? $original_order['order_payment_gateway'] : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_problem_loading_gateway'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        if (!method_exists(ee()->cartthrob_payments->gateway(), 'capture')) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_gateway_could_not_process'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        $transaction_id = $original_order['field_id_' . ee()->cartthrob->store->config('orders_transaction_id')];
        $amount = ee()->input->post('total');
        // need to unset the post variables before sending to the capture method
        unset($_POST);

        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->gateway()->capture($transaction_id, $amount, $order_id);

        if ($state->isAuthorized()) {
            ee()->cartthrob_payments->relaunchCartSnapshot($order_id);
            ee()->cartthrob_payments->checkoutCompleteOffsite($state, $order_id, Cartthrob_payments::COMPLETION_TYPE_STOP);
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_capture_succeeded'));

            $order_items = ee()->order_model->get_order_items($order_id);

            foreach ($order_items as $item) {
                $item_options = array_diff_key($item, $this->default_columns);
                ee()->load->model('product_model');
                ee()->product_model->reduce_inventory($item['entry_id'], $item['quantity'], $item_options);
            }
        } else {
            ee()->session->set_flashdata(
                $this->module_name . '_system_error',
                lang($this->module_name . '_capture_failed') . ' - ' . $state->getMessage()
            );
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
    }

    public function void()
    {
        $order_id = ee()->input->post('id');

        $this->loadCartWithOrderById($order_id);

        ee()->load->library([
            'form_validation',
            'encrypt',
            'form_builder',
            'api/api_cartthrob_payment_gateways',
            'api/api_cartthrob_tax_plugins',
            'cartthrob_payments',
        ]);
        ee()->load->model('purchased_items_model');
        ee()->load->model('order_model');
        ee()->load->model('cartthrob_field_model');

        $original_order = ee()->order_model->get_order($order_id);

        $gateway = isset($original_order['order_payment_gateway']) ? $original_order['order_payment_gateway'] : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_problem_loading_gateway'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        if (!method_exists(ee()->cartthrob_payments->gateway(), 'void')) {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_gateway_could_not_process'));
            ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
        }

        $transaction_id = $original_order['field_id_' . ee()->cartthrob->store->config('orders_transaction_id')];

        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->gateway()->void($transaction_id);

        if ($state->isAuthorized()) {
            ee()->session->set_flashdata($this->module_name . '_system_message', lang($this->module_name . '_void_succeeded'));
            ee()->cartthrob_payments->setStatus(Cartthrob_payments::STATUS_VOIDED, $state, $order_id);
        } else {
            ee()->session->set_flashdata($this->module_name . '_system_error', lang($this->module_name . '_void_failed') . ' - ' . $state->getMessage());
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/edit', ['id' => $order_id]));
    }

    public function edit()
    {
        $this->initialize();

        ee()->load->helper('array');
        ee()->load->model('order_model');
        ee()->load->model('cartthrob_field_model');

        $data = (array)ee()->order_model->get_order(ee()->input->get_post('id'));

        // based on (at least) the country code we're going to try to get the correct currency symbol for this order. only works if multi currency is installed
        if (isset($data['order_country_code'])) {
            $this->multi_location($data['order_country_code'], element('order_billing_state', $data),
                element('order_ip_address', $data));
        }

        if (!array_key_exists('order_tracking_number', $data)) {
            $data['order_tracking_number'] = null;
        }

        if (!array_key_exists('order_shipping_note', $data)) {
            $data['order_shipping_note'] = null;
        }

        if (!array_key_exists('order_transaction_id', $data)) {
            // setting the order_transaction_id based on the configured transaction id field
            $data['order_transaction_id'] = $data['field_id_' . ee()->cartthrob->store->config('orders_transaction_id')];
        }

        $data['order_items'] = ee()->order_model->get_order_items(ee()->input->get_post('id'));

        $keys = array_merge($this->order_fields, $this->total_fields);

        foreach ($keys as $k) {
            if (!is_null($this->currency_code)) {
                ee()->number->set_prefix($this->currency_code);
            }

            if (!is_null($this->prefix)) {
                ee()->number->set_prefix($this->prefix);
            }

            if (!is_null($this->dec_point)) {
                ee()->number->set_dec_point($this->dec_point);
            }

            $data[$k] = null;
            if (ee()->cartthrob->store->config($k . '_field') && array_key_exists('field_id_' . ee()->cartthrob->store->config($k . '_field'), $data)) {
                $data[$k] = $data['field_id_' . ee()->cartthrob->store->config($k . '_field')];

                if (in_array($k, $this->total_fields)) {
                    $data[$k] = ee()->number->format($data[$k]);
                }
            } elseif (ee()->cartthrob->store->config($k) && array_key_exists('field_id_' . ee()->cartthrob->store->config($k), $data)) {
                $data[$k] = $data['field_id_' . ee()->cartthrob->store->config($k)];

                if (in_array($k, $this->total_fields)) {
                    $data[$k] = ee()->number->format($data[$k]);
                }
            }
        }

        ee()->load->model('order_management_model');

        if (!ee()->order_management_model->is_member($data['author_id'])) {
            $data['author_id'] = null;
        }

        // added because one customer's site could't handle $data['view'] = $data;
        $view = $data;
        $data['order_payment_gateway'] = $view['order_payment_gateway'] = isset($data['order_payment_gateway']) ? $data['order_payment_gateway'] : ee()->cartthrob->store->config('payment_gateway');
        $data['view'] = $view;
        $data['form_edit'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/form_update',
            ['return' => 'edit']));
        $data['form_delete'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/delete_order',
            ['return' => 'view']));
        $data['resend_email'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/resend_email',
            ['return' => 'edit']));
        $data['href_entry'] = ee('CP/URL')->make('publish/edit/entry') . '/';
        $data['href_member'] = ee('CP/URL')->make('members/profile/settings', ['id' => '']);
        $data['href_invoice'] = null;
        $data['href_packing_slip'] = null;
        $data['custom_templates'] = [];

        if (ee()->get_settings->get($this->module_name, 'invoice_template')) {
            $data['href_invoice'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/print_invoice',
                ['id' => ee()->input->get_post('id')]);
        }
        if (ee()->get_settings->get($this->module_name, 'packing_slip_template')) {
            $data['href_packing_slip'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/print_packing_slip',
                ['id' => ee()->input->get_post('id')]);
        }
        if (ee()->get_settings->get($this->module_name, 'custom_templates')) {
            foreach (ee()->get_settings->get($this->module_name, 'custom_templates') as $key => $template) {
                $data['custom_templates'][$key] = [
                    'link' => ee('CP/URL')->make('addons/settings/' . $this->module_name . '/print_custom_template',
                        ['id' => ee()->input->get_post('id'), 'custom_template_id' => $key]),
                    'name' => $template['custom_template_name'],
                    'form' => form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/email_custom_template',
                        ['custom_template_id' => $key, 'return' => 'edit'])),
                ];
            }
        }

        // adding in the capture functionality
        $data['captured'] = null;
        $data['voided'] = null;
        $data['form_capture'] = null;
        $data['form_void'] = null;
        $data['refunded'] = null;
        $data['form_refund'] = null;

        if (!empty($data['order_payment_gateway'])) {
            ee()->load->library('cartthrob_payments');
            if (ee()->cartthrob_payments->setGateway($data['order_payment_gateway'])->gateway()) {
                if (ee()->cartthrob_payments->getOrderStatus(ee()->input->get_post('id')) != 'refunded') {
                    if (method_exists(ee()->cartthrob_payments->gateway(), 'refund')) {
                        $data['form_refund'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/refund',
                            ['return' => 'edit']),
                            ['onsubmit' => "return confirm('Are you sure you want to issue this refund?\\nAmount: ' + (($(this).find('#total').length > 0) ? $(this).find('#total').val() : $(this).find('#subtotal').val() + ' + ' + $(this).find('#tax').val() + ' + ' + $(this).find('#shipping').val()));"]);
                    }
                } else {
                    $data['refunded'] = lang('refunded');
                }

                // adding in the capture button
                if (ee()->cartthrob_payments->getOrderStatus(ee()->input->get_post('id')) == 'processing') {
                    if (method_exists(ee()->cartthrob_payments->gateway(), 'capture')) {
                        $data['form_capture'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/capture',
                            ['return' => 'edit']));
                    }
                } else {
                    $data['captured'] = lang('captured');
                }

                // adding in the void button
                if (ee()->cartthrob_payments->getOrderStatus(ee()->input->get_post('id')) == 'processing') {
                    if (method_exists(ee()->cartthrob_payments->gateway(), 'void')) {
                        $data['form_void'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/void',
                            ['return' => 'edit']));
                    }
                } else {
                    $data['captured'] = lang('captured');
                }
            }
        }

        $data['add_tracking'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/add_tracking_to_order', ['return' => 'edit']));

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, $data);
    }

    public function delete()
    {
        $this->initialize();

        ee()->load->model('order_model');

        $data = ee()->order_model->get_order(ee()->input->get_post('id'));
        $data['view'] = $data;
        $data['form_edit'] = form_open(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/form_update', ['return' => 'view']));

        return ee()->mbr_addon_builder->load_view(__FUNCTION__, $data);
    }

    ////////////// NOTIFICATIONS /////////////

    public function index()
    {
        $this->initialize();

        $method = isset($this->params['nav']['view']) ? 'view' : 'add';

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/' . $this->module_name . '/' . $method));
    }
}
