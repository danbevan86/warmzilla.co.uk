<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cartthrob_order_manager
{
    public $return_data;

    public function __construct()
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('cartthrob_loader');
        ee()->load->library('number');
        ee()->load->library('form_builder');
        ee()->load->library('cartthrob_variables');
        ee()->load->library('template_helper');
    }
}
