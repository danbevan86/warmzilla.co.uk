<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cartthrob_order_manager_ext
{
    public $settings = [];
    public $description = 'Generic Description';
    public $docs_url = 'http://cartthrob.com';
    public $name = 'Cartthrob Addon';
    public $settings_exist = 'n';
    public $version;
    public $required_by = ['module'];
    public $testing = false; // either FALSE, or 2 char country code.
    private $module_name;
    private $remove_keys = [
        'name',
        'submit',
        'x',
        'y',
        'templates',
        'XID',
        'CSRF_TOKEN',
    ];

    private $EE;

    /**
     * Constructor
     *
     * @param    mixed    settings array or empty string if none exist
     */
    public function __construct($settings = '')
    {
        $this->module_name = strtolower(str_replace(['_ext', '_mcp', '_upd'], '', __CLASS__));

        ee()->lang->loadfile($this->module_name);

        include PATH_THIRD . $this->module_name . '/config.php';
        $this->name = lang($this->module_name . '_module_name');
        $this->version = $config['version'];
        $this->description = lang($this->module_name . '_description');

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->add_package_path(PATH_THIRD . $this->module_name . '/');
        ee()->load->library('cartthrob_loader');

        $this->params = [
            'module_name' => $this->module_name,
        ];
        ee()->load->library('mbr_addon_builder');
        ee()->mbr_addon_builder->initialize($this->params);

        ee()->load->library('get_settings');
        $this->settings = ee()->get_settings->settings($this->module_name);
    }

    // ----------------------------------------------------------------------

    /**
     * Activate Extension
     *
     * This function enters the extension into the exp_extensions table
     */
    public function activate_extension()
    {
        return ee()->mbr_addon_builder->activate_extension();
    }

    // ----------------------------------------------------------------------

    public function update_extension($current = '')
    {
        return ee()->mbr_addon_builder->update_extension($current);
    }

    public function disable_extension()
    {
        return ee()->mbr_addon_builder->disable_extension();
    }

    public function settings()
    {
        return [];
    }

    public function cp_menu_array($menu)
    {
        if (ee()->extensions->last_call !== false) {
            $menu = ee()->extensions->last_call;
        }

        ee()->load->library('addons');

        $modules = ee()->addons->get_installed();

        if (!isset($modules['cartthrob']['module_version'])) {
            return $menu;
        }

        //if the user has uploaded a new version, but hasn't run the updater yet
        //this hook can cause some pretty bad errors if it tries to access database tables/fields
        //that aren't yet created
        //we're gonna kill this feature if we detect that they need an update
        //so they don't get any errors trying to get to the modules page to do the update
        if (REQ === 'CP') {
            //i'm not worried about the overhead from this since a) we're in the CP and b) Accessories lib calls this on every CP page
            ee()->load->library('addons');

            $modules = ee()->addons->get_installed();

            if (!isset($modules[$this->module_name]['module_version']) || version_compare($this->version,
                    $modules[$this->module_name]['module_version'], '>')) {
                return $menu;
            }
        }

        $channels = [];

        ee()->lang->loadfile($this->module_name, $this->module_name);

        ee()->load->add_package_path(PATH_THIRD . $this->module_name . '/');

        ee()->load->library('get_settings');
        //$label = lang($this->module_name. "_cp_menu_label");
        $label = $this->module_name;
        ee()->lang->language['nav_' . $this->module_name . '_cp_menu_label'] = $label;

        $menu[$label] = [];

        $menu[$label]['overview'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/om_sales_dashboard');
        $menu[$label]['orders'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/view');
        $menu[$label]['products'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/view_products');
        $menu[$label]['order_report'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/order_report');
        $menu[$label]['customer_report'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/customer_report');
        $menu[$label]['product_report'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/product_report');
        $menu[$label]['system_settings'] = ee('CP/URL')->make('addons/settings/' . $this->module_name . '/system_settings');
        $menu[$label][] = '----';

        // if we don't have admin driven products, then hide the menu for this.
        if (!ee()->cartthrob->store->config('show_product_admin')) {
            unset(
                $menu[$label]['products']
            );
        }

        $has_channel = false;

        if (isset($menu['content']['publish']) && is_array($menu['content']['publish'])) {
            //we've got a perfectly good list of channels right here in menu, let's grab it
            foreach ($menu['content']['publish'] as $channel_name => $url) {
                if (preg_match('/channel_id=(\d+)$/', $url, $match)) {
                    $channels[$match[1]] = $channel_name;
                }
            }
        } else {
            if (isset($menu['content']['publish']) && is_string($menu['content']['publish']) && preg_match('/channel_id=(\d+)$/',
                    $menu['content']['publish'], $match)) {
                $channels[$match[1]] = '';
            }
        }

        // need to load CT first, otherwise, the following stuff craps out
        ee()->load->library('cartthrob_loader');

        if (ee()->cartthrob->store->config('product_channels')) {
            if (count(ee()->cartthrob->store->config('product_channels')) > 1) {
                foreach (ee()->cartthrob->store->config('product_channels') as $channel_id) {
                    if (isset($channels[$channel_id])) {
                        $has_channel = true;

                        ee()->lang->language['nav_' . $channels[$channel_id]] = $channels[$channel_id];
                        $menu[$label]['products'][$channels[$channel_id]] = ee('CP/URL')->make('publish/edit',
                            ['filter_by_channel' => $channel_id]);
                    }
                }
            } else {
                $channel_id = current(ee()->cartthrob->store->config('product_channels'));

                if (isset($channels[$channel_id])) {
                    $has_channel = true;
                    $menu[$label]['products'] = ee('CP/URL')->make('publish/edit',
                        ['filter_by_channel' => $channel_id]);
                }
            }
        }

        if (ee()->cartthrob->store->config('save_purchased_items') && ee()->cartthrob->store->config('purchased_items_channel')) {
            if (isset($channels[ee()->cartthrob->store->config('purchased_items_channel')])) {
                $has_channel = true;

                $menu[$label]['purchased_items'] = ee('CP/URL')->make('publish/edit',
                    ['filter_by_channel' => ee()->cartthrob->store->config('purchased_items_channel')]);
            }
        }

        if (ee()->cartthrob->store->config('discount_channel')) {
            if (isset($channels[ee()->cartthrob->store->config('discount_channel')])) {
                $has_channel = true;

                $menu[$label]['discounts'] = ee('CP/URL')->make('publish/edit',
                    ['filter_by_channel' => ee()->cartthrob->store->config('discount_channel')]);
            }
        }

        if (ee()->cartthrob->store->config('coupon_code_channel')) {
            if (isset($channels[ee()->cartthrob->store->config('coupon_code_channel')])) {
                $has_channel = true;

                $menu[$label]['coupon_codes'] = ee('CP/URL')->make('publish/edit',
                    ['filter_by_channel' => ee()->cartthrob->store->config('coupon_code_channel')]);
            }
        }

        $add_settings_menu = true;

        if (ee()->session->userdata('group_id') != 1) {
            if (!ee()->session->userdata('assigned_modules') || !ee()->cp->allowed_group('can_access_addons',
                    'can_access_modules')) {
                $add_settings_menu = false;
            } else {
                $module_id = ee()->db->select('module_id')
                    ->where('module_name', 'Cartthrob_order_manager')
                    ->get('modules')
                    ->row('module_id');

                $assigned_modules = ee()->session->userdata('assigned_modules') ? ee()->session->userdata('assigned_modules') : [];

                if (!$module_id || !array_key_exists($module_id, $assigned_modules)) {
                    $add_settings_menu = false;
                }
            }
        }

        if (empty($menu[$label])) {
            unset($menu[$label]);
        }

        return $menu;
    }

    public function cartthrob_addon_register($valid_addons)
    {
        if (ee()->extensions->last_call !== false) {
            $valid_addons = ee()->extensions->last_call;
        }

        $addon_name = $this->module_name;
        if (strpos($this->module_name, 'cartthrob_') !== false) {
            $addon_name = str_replace('cartthrob_', '', $addon_name);
        }
        $valid_addons[] = $addon_name;

        return $valid_addons;
    }
}

/* End of file ext.price_field_changer_for_cartthrob.php */
/* Location: /system/user/addons/price_field_changer_for_cartthrob/ext.price_field_changer_for_cartthrob.php */
