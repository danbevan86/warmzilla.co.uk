<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once PATH_THIRD . 'smart_members/libraries/MailChimp.php';
use \DrewM\MailChimp\MailChimp;
class Sm_reg_mailchimp
{

    public $mailChimpKey;
    public $defaultList;
    public function __construct()
    {
        $query = ee()->db->select('mailchimp_key, default_mailchimp_list')->get('smart_members_settings');
        $this->mailChimpKey = $query->row('mailchimp_key');
        $this->defaultList = $query->row('default_mailchimp_list');
    }

    public function addMemberInList($email, $mergeFields = array(), $list = "")
    {
        $MailChimp = $this->setKeys();
        if($MailChimp === false)
        {
            return false;
        }

        if($list == "")
        {
            $list = $this->defaultList;
        }

        if($list == "")
        {
            return false;
        }

        $result = $MailChimp->post("lists/$list/members", [
            'email_address' => $email,
            'merge_fields'  => $mergeFields,
            'status'        => 'subscribed',
        ]);

        return true;
    }

    function setKeys()
    {
        if($this->mailChimpKey == "")
        {
            return false;
        }

        return new MailChimp($this->mailChimpKey);
    }
}