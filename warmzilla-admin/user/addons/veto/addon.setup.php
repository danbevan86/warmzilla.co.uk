<?php
return array(
      'author'         => 'James Smith',
      'author_url'     => 'https://cld.agency',
      'name'           => 'Veto',
      'description'    => 'Control panel prettifier',
      'version'        => '1.0.2',
      'namespace'      => 'CLD\Veto',
      'settings_exist' => true,
);
