<?php

$lang = array(
	'veto_logo_path'  => 'Control panel logo URL <span style="display:block;padding-top:8px;color:#777;font-weight:400;font-size:12px;">Only applies when using a base theme included with Veto</span>',
	'veto_options' => 'Options',
	'veto_logo_whiten' => 'Automatically attempt to whiten the logo (uses CSS filters, not currently supported by IE11)',
	'veto_custom_styles' => 'Style overrides<span style="display:block;padding-top:8px;color:#777;font-weight:400;font-size:12px;">Add your own custom CSS inline here, or place a CSS file named "veto-extra-css.css" in your public themes/user/veto/ directory to add to the base theme</span>',
	'veto_custom_js' => 'Extra JavaScript<span style="display:block;padding-top:8px;color:#777;font-weight:400;font-size:12px;">Add your own custom JavaScript inline here, or place a JS file named "veto-extra-js.js" in your public themes/user/veto directory to add to the base theme</span>',
	'veto_theme' => 'Base theme<span style="display:block;padding-top:8px;color:#777;font-weight:400;font-size:12px;">Add your own themes at themes/user/veto/veto-custom-themes/[your-custom-theme-name]/</span>',
);

// EOF