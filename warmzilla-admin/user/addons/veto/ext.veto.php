<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Veto_ext {

	private $package = 'veto';
	private $info;
	public $settings = array();
	public $flavour = 'default';

	public function __construct($settings = array())
	{
		$this->info = ee('App')->get($this->package);
		$this->version = $this->info->getVersion();
		$this->settings = $settings;
	}

	// --------------------------------
	//  Settings
	// --------------------------------

	public function settings()
	{

		// --------------------------------------------
		// AS GOOD A PLACE AS any to inject/update the login
		// styles for if/when an EE upgrade has wiped them out
		// --------------------------------------------

		$this->remove_style_from_login();
		$this->inject_style_into_login();

		// --------------------------------------------
		// SETTINGS
		// --------------------------------------------

		$settings = array();
		// General pattern:
		//
		// $settings[variable_name] => array(type, options, default);
		//
		// variable_name: short name for the setting and the key for the language file variable
		// type:          i - text input, t - textarea, r - radio buttons, c - checkboxes, s - select, ms - multiselect
		// options:       can be string (i, t) or array (r, c, s, ms)
		// default:       array member, array of members, string, nothing


	    // // Creates a text input with a default value of "EllisLab Brand Butter"
	    //$settings['brand']      = array('i', '', "EllisLab Brand Butter");

	    // // Creates a textarea with 20 rows and an empty default value
	    // $settings['description']    = array('t', array('rows' => '20'), '');

	    // // Creates a set of radio buttons, one for "Yes" (y), one for "No" (n) and a default of "Yes"
	    // $settings['tasty']      = array('r', array('y' => "Yes", 'n' => "No"), 'y');

	    // // Creates a set of checkboxes, one for "Lowfat" (l) and one for "Salty" (s), and a
	    // // default of both items being checked
	    // $settings['details']    = array('c', array('l' => "Lowfat", 's' => "Salty"), array('l', 's'));

	    // // Creates a select dropdown with the options "France" (fr), "Germany" (de), and "United States"
	    // // (us), with a default of "United States"
	    // $settings['country']    = array('s', array('fr' => 'France', 'de' => 'Germany', 'us' => 'United States'), 'us');

	    // // Creates a multi-select box with the options "Derek" (dj), "Leslie" (lc), and "Rick" (re) with
	    // // Derek and Rick selected by default
	    // $settings['enjoyed_by'] = array('ms', array('dj' => 'Derek', 'lc' => 'Leslie', 're' => 'Rick'), array('dj', 're'));

		$settings['veto_theme'] = array('s', $this->get_flavours(), 'themes/default');
		$settings['veto_custom_styles'] = array('t', '', '');
		$settings['veto_custom_js'] = array('t', '', '');

		$settings['veto_logo_path'] = array('i', '', '');
		$settings['veto_options'] = array('c', array('y' => 'veto_logo_whiten'), array());

	    return $settings;
	}


	// ------------------------------------------------------

	public function activate_extension()
	{
		$this->_add_hook('cp_css_end', 10);
		$this->_add_hook('cp_js_end', 10);

		$this->inject_style_into_login();
	}

	// ------------------------------------------------------

	public function disable_extension()
	{
		ee()->db->where('class', __CLASS__);
		ee()->db->delete('extensions');

		$this->remove_style_from_login();
	}

	// ------------------------------------------------------

	public function update_extension($current = '')
	{
		if ($current == '' OR (version_compare($current, $this->version) === 0))
		{
			return false; // up to date
		}

		// update table row with current version
		ee()->db->where('class', __CLASS__);
		ee()->db->update('extensions', array('version' => $this->version));

		// update login style. Not sure this is working actually...
		$this->remove_style_from_login();
		$this->inject_style_into_login();
	}

	// ------------------------------------------------------

	public function cp_css_end()
	{
		// play nice with other exts on this hook
		$other_extensions_data = ee()->extensions->last_call !== false ? ee()->extensions->last_call : '';

		$theme = isset($this->settings['veto_theme']) ? $this->settings['veto_theme'] : 'themes/default';
		$path = strpos($theme, 'veto-custom-themes') !== false ? PATH_THEMES . '../user/veto/' : PATH_THIRD . 'veto/';
		$path = $path . $theme . '/css/';

		// filename to fetch
		$stylesheet_filename = 'all.css';

		if (version_compare(APP_VER, '4.0.0', '<'))
		{
			if(file_exists($path . 'ee3.css') && filesize($path . 'ee3.css') != 0)
			{
				$stylesheet_filename = 'ee3.css';
			}
		} else {
			if(file_exists($path . 'ee4.css') && filesize($path . 'ee4.css') != 0)
			{
				$stylesheet_filename = 'ee4.css';
			}
		}

		$out = @file_get_contents( $path . $stylesheet_filename);

		// --------------------------------------------
		// CUSTOM LOGO and FILTER
		// --------------------------------------------

		$filter = '';
		if (isset($this->settings['veto_options'][0]) && $this->settings['veto_options'][0] == 'y')
		{
			$filter = 'filter:brightness(0) invert(1);';
		}

		if ($this->settings['veto_logo_path'])
		{
			$custom_logo_css = "
				.veto-logo-wrap{
					border-bottom:1px solid rgba(0,0,0,0.12);
				}
				.veto-logo{
					height:63px;
					margin:17px;
					background-image:url('" .$this->settings['veto_logo_path']. "');
					background-repeat:no-repeat;
					background-size:contain;
					background-position:0 50%;
					".$filter."
				}
			";

			$out .= $custom_logo_css;
		}

		// --------------------------------------------
		// APPEND CUSTOM STYLES
		// --------------------------------------------

		// Separate file. Goes in the themes folder at: themes/user/veto/
		$custom = @file_get_contents(PATH_THEMES.'../user/veto/veto-extra-css.css');
		if($custom !== false)
		{
			$out .= $custom;
		}

		// ... and/or inline override
		if ($this->settings['veto_custom_styles'])
		{
			$out .= $this->settings['veto_custom_styles'];
		}

		return $other_extensions_data . $out;
	}

	// ------------------------------------------------------

	public function cp_js_end()
	{
		// play nice with other exts on this hook
		$other_extensions_data = ee()->extensions->last_call !== false ? ee()->extensions->last_call : '';

		// figure out the path
		$theme = isset($this->settings['veto_theme']) ? $this->settings['veto_theme'] : 'themes/default';
		$path = strpos($theme, 'veto-custom-themes') !== false ? PATH_THEMES . '../user/veto/' : PATH_THIRD . 'veto/';
		$path = $path . $theme . '/js/';

		// filename to fetch
		$filename = 'all.js';

		if (version_compare(APP_VER, '4.0.0', '<'))
		{
			if(file_exists($path . 'ee3.js') && filesize($path . 'ee3.js') != 0)
			{
				$filename = 'ee3.js';
			}
		} else {
			if(file_exists($path . 'ee4.js') && filesize($path . 'ee4.js') != 0)
			{
				$filename = 'ee4.js';
			}
		}

		$out = @file_get_contents( $path . $filename);

		// --------------------------------------------
		// APPEND CUSTOM SCRIPT
		// --------------------------------------------

		// Separate file. Goes in the themes folder at: themes/user/veto
		$custom = @file_get_contents(PATH_THEMES.'../user/veto/veto-extra-js.js');
		if($custom !== false)
		{
			$out .= $custom;
		}

		// ... and/or inline override
		if ($this->settings['veto_custom_js'])
		{
			$out .= $this->settings['veto_custom_js'];
		}

		return $other_extensions_data . $out;
	}

	// --------------------------------------------------------------------

	private function _add_hook($name, $priority = 10)
	{
		ee()->db->insert('extensions',
			array(
				'class'    => __CLASS__,
				'method'   => $name,
				'hook'     => $name,
				'settings' => serialize($this->settings),
				'priority' => $priority,
				'version'  => $this->version,
				'enabled'  => 'y'
			)
		);
	}

	// --------------------------------------------------------------------

	private function inject_style_into_login()
	{
		// silence any errors in case people have incorrect file permissions set

		$login_file_path = SYSPATH.'ee/EllisLab/ExpressionEngine/View/_templates/login.php';
		$existing = @file_get_contents($login_file_path);

		// which theme and filename?
		$theme = isset($this->settings['veto_theme']) ? $this->settings['veto_theme'] : 'themes/default';
		$path = strpos($theme, 'veto-custom-themes') !== false ? PATH_THEMES . '../user/veto/' : PATH_THIRD . 'veto/';
		$path = $path . $theme . '/css/';

		// filename to fetch
		$filename = 'login-all.css';

		if (version_compare(APP_VER, '4.0.0', '<'))
		{
			if(file_exists($path . 'login-ee3.css') && filesize($path . 'login-ee3.css') != 0)
			{
				$filename = 'login-ee3.css';
			}
		} else {
			if(file_exists($path . 'login-ee4.css') && filesize($path . 'login-ee4.css') != 0)
			{
				$filename = 'login-ee4.css';
			}
		}

		// do the injection...
		if (strpos($existing, 'added by Veto') === false && file_exists($path . $filename) && filesize($path . $filename) != 0)
		{
			$our_stuff = @file_get_contents($path . $filename);
			$new = str_replace('</head>', '<!--added by Veto--><style>'.$our_stuff.'</style><!--end added by Veto--></head>', $existing);
			@file_put_contents($login_file_path, $new);
		}
	}

	private function remove_style_from_login()
	{
		$login_file_path = SYSPATH.'ee/EllisLab/ExpressionEngine/View/_templates/login.php';
		$existing = @file_get_contents($login_file_path);
		if (strpos($existing, 'added by Veto') !== false)
		{
			$regex = '/(<!--added by Veto-->[\s\S]*?<!--end added by Veto-->)/';
			$new = preg_replace($regex, '', $existing);
			@file_put_contents($login_file_path, $new);
		}
	}

	// --------------------------------------------------------------------

	public function get_flavours()
	{
		$flavours = glob(PATH_THIRD . 'veto/themes/*' , GLOB_ONLYDIR);
		$user_flavours = glob(PATH_THEMES . '../user/veto/veto-custom-themes/*' , GLOB_ONLYDIR);
		$flavours = array_merge($flavours, $user_flavours);

		foreach ($flavours as $flavour)
		{
			// we don't want to store the whole path otherwise it won't work cross-environment...
			// so just store veto-custom-themes/[theme-name] or themes/[theme-name]
			$path_prefix = strpos($flavour, 'veto-custom-themes') !== false ? 'veto-custom-themes' : 'themes';
			$flavour_path = $path_prefix . '/' . basename($flavour);

			// Human-readable name
			$flavour_pretty = ucfirst(basename($flavour));
			$replacements = array('-','_');
			$flavour_pretty = str_replace($replacements, ' ', $flavour_pretty);

			$flavours_list[$flavour_path] = $flavour_pretty;
		}

		return $flavours_list;
	}
}