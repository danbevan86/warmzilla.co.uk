
<div id="charge_container" class="ee<?=$ee_ver?>">

	<form method="post" action="<?=$form_post_url?>">
	<input type="hidden" name="XID" value="<?=XID_SECURE_HASH?>"/>
	<input type="hidden" name="edit_coupon" value="yes"/>
	<input type="hidden" name="id" value="<?=$coupon['coupon_id']?>"/>
	<input type="hidden" name="stripe_id" value="<?=$coupon['stripe_id']?>"/>

	<div class="box">
		<h1>Add Coupon</h1>

		<?=charge_get_value($errors, 'general', '<p class="setting-txt bg-warning" style="margin-bottom :0">','</p>')?>

		<div class="settings">
			<table class="data" width="100%">
			<tbody>
				<tr>
					<td width="50%" class="setting-txt">
						<h3><label for="name">Coupon Name</label></h3>
						<em>eg. 'Summer Offer $10 off'</em>
					</td>
					<td>
						<input class="js-code" data-code="code" type="text" name="name" id="name" value="<?=charge_get_value($coupon, 'name')?>"/>
						<?=charge_get_value($errors, 'name', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr>
					<td class="setting-txt">
						<h3><label for="code">Coupon Code</label></h3>
						<em>eg. 'OFF10'</em>
					</td>
					<td>
						<input type="text" name="code" data-raw="<?=charge_get_value($coupon, 'code')?>" id="code" value="<?=charge_get_value($coupon, 'code')?>"/>
						<?=charge_get_value($errors, 'code', '<em class="error">* ','</em>')?>
					</td>
				</tr>
			</tbody>
			</table>

			<h2>Coupon Type</h2>
			<table class="data" width="100%">
			<tbody>
				<tr>
					<td width="50%" class="setting-txt">
						<h3>Coupon Type</h3>
						<em>Percentage off or fixed amount</em>
					</td>
					<td>
						<select name="type" id="type" class="js-enabler js-hide js-select" rel="js-coupon-type">
							<option value="fixed" <?=charge_check_value($coupon, 'type', 'fixed', 'selected="selected"')?>>Fixed Amount</option>
							<option value="percentage"  <?=charge_check_value($coupon, 'type', 'percentage', 'selected="selected"')?>>Percentage Off</option>
						</select>
						<?=charge_get_value($errors, 'type', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr class="js-coupon-type <?=charge_check_value($coupon, 'type', 'fixed', '', 'hidden')?>" rel="fixed">
					<td width="50%" class="setting-txt">
						<h3>Amount Off</h3>
						<em>Amount for the discount. ie. 3.50</em>
					</td>
					<td>
						<label for="amount_off">
							<input style="width:30%" name="amount_off" id="amount_off" type="text" value="<?=charge_get_value($coupon, 'amount_off')?>"/>
						</label>
						<?=charge_get_value($errors, 'amount_off', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr class="last-visible-row js-coupon-type <?=charge_check_value($coupon, 'type', 'fixed', '', 'hidden')?>" rel="fixed">
					<td width="50%" class="setting-txt">
						<h3>Currency</h3>
						<em>Currency for the discount off</em>
					</td>
					<td>
						<label for="currency">
							<select name="currency" id="currency" style="width:30%">
								<?php foreach($supported_currencies as $key => $currency) : ?>
									<option value="<?=$key?>" <?=charge_check_value($coupon, 'currency', $key, 'selected="selected"')?>><?=$currency['cleaned_name']?></option>
								<?php endforeach ?>

							</select>
						</label>
						<?=charge_get_value($errors, 'currency', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr class="js-coupon-type <?=charge_check_value_not($coupon, 'type', 'percentage', 'hidden')?>" rel="percentage">
					<td width="50%" class="setting-txt">
						<h3>Percentage Off</h3>
						<em>Integer value : 1 - 100</em>
					</td>
					<td>
						<label for="percentage_off">
							<input style="width:30%" name="percentage_off" id="percentage_off" type="text" value="<?=charge_get_value($coupon, 'percentage_off')?>"/>
						</label>
						<?=charge_get_value($errors, 'percentage_off', '<em class="error">* ','</em>')?>
					</td>
				</tr>
			</tbody>
			</table>

			<h2>Payment Type</h2>
			<table class="data" width="100%">
			<tbody>
				<tr class="last-visible-row">
					<td width="50%" class="setting-txt">
						<h3>Payment Type</h3>
						<em>What types of payment can this coupon is valid for</em>
					</td>
					<td>
						<select name="payment_type" id="payment_type" class="js-enabler js-hide js-select" rel="js-payment-type">
								<option value="one-off" <?=charge_check_value($coupon, 'payment_type', 'one-off', 'selected="selected"')?>>One-off</option>
								<option value="recurring"  <?=charge_check_value($coupon, 'payment_type', 'recurring', 'selected="selected"')?>>Recurring</option>
						</select>
						<?=charge_get_value($errors, 'payment_type', '<em class="error">* ','</em>')?>
					</td>
				</tr>

				<tr class="special-visible-row js-payment-type <?=charge_check_value($coupon, 'payment_type', 'recurring', '', 'hidden')?>" rel="recurring">
					<td width="50%" class="setting-txt">
						<h3>Max Usages</h3>
						<em>How many times can this be used? Leave blank or zero for no limit</em>
					</td>
					<td>
						<label for="max_redemptions">
							<input style="width:30%" name="max_redemptions" id="max_redemptions" type="text" value="<?=charge_get_value($coupon, 'max_redemptions')?>"/>
						</label>
						<?=charge_get_value($errors, 'max_redemptions', '<em class="error">* ','</em>')?>
					</td>
				</tr>

				<tr class="last-visible-row js-payment-type <?=charge_check_value($coupon, 'payment_type', 'recurring', '', 'hidden')?>" rel="recurring">
					<td width="50%" class="setting-txt">
						<h3>Duration</h3>
						<em>How long does the applied discount stay in effect for?</em>
					</td>
					<td>
						<select name="duration" id="duration" class="js-enabler js-hide js-select" rel="js-duration">
							<option value="once" <?=charge_check_value($coupon, 'duration', 'once', 'selected="selected"')?>>Once</option>
							<option value="forever"  <?=charge_check_value($coupon, 'duration', 'forever', 'selected="selected"')?>>Forever</option>
							<option value="repeating"  <?=charge_check_value($coupon, 'duration', 'repeating', 'selected="selected"')?>>Repeating</option>
						</select>
						<?=charge_get_value($errors, 'duration', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr class="js-duration <?=charge_check_value($coupon, 'duration', 'repeating', '', 'hidden')?>" rel="repeating">
					<td width="50%" class="setting-txt">
						<h3>Duration in Months</h3>
						<em>How many months does the coupon repeat for?</em>
					</td>
					<td>
						<label for="duration_in_months">
							<input style="width:30%" name="duration_in_months" id="duration_in_months" type="text" value="<?=charge_get_value($coupon, 'duration_in_months')?>"/>
						</label>
						<?=charge_get_value($errors, 'duration_in_months', '<em class="error">* ','</em>')?>
					</td>
				</tr>
			</tbody>
			</table>

			<h2>Restrictions</h2>
			<table class="data" width="100%">
			<tbody>
				<tr>
					<td width="50%" class="setting-txt">
						<h3>Start Date</h3>
						<em>Date this coupon is valid.</em>
					</td>
					<td>
						<label for="start_date">
							<input style="width:30%" class="datepicker" name="start_date" id="start_date" type="text" value="<?=charge_get_value($coupon, 'start_date')?>"/>
						</label>
						<?=charge_get_value($errors, 'start_date', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr>
					<td width="50%" class="setting-txt">
						<h3>End Date</h3>
						<em>Date this coupon is no longer valid.</em>
					</td>
					<td>
						<label for="end_date">
							<input style="width:30%" class="datepicker" name="end_date" id="end_date" type="text" value="<?=charge_get_value($coupon, 'end_date')?>"/>
						</label>
						<?=charge_get_value($errors, 'end_date', '<em class="error">* ','</em>')?>
					</td>
				</tr>
				<tr>
					<td width="50%" class="setting-txt">
						<h3>Product</h3>
						<em>Limit this coupon to discount a specific product (only 1 allowed).</em>
					</td>
					<td>
						<label for="entry_id">
							<select name="entry_id" id="entry_id">
							<?php foreach($products as $entry_id => $entry_title) { ?>
								<option value="<?=$entry_id?>" <?=charge_check_value($coupon, 'entry_id', $entry_id, 'selected="selected"')?>><?=$entry_title?></option>
							<?php } ?>
							</select>
						</label>
						<?=charge_get_value($errors, 'entry_id', '<em class="error">* ','</em>')?>
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>

	<input type="submit" class="btn" name="submit" value="Save" />
	</form>
</div>