<div id="charge_container" class="ee<?=$ee_ver?>">
	<div class="box">
		<h1>Coupons</h1>

		<div class="settings">
			<div class="setting-txt">
				<em>Coupons can be used with one-off and recurring payments for fixed amounts or percentage discounts.</em>
			</div>

			<table class="data" width="100%">
			<tbody>
				<?php foreach($coupons as $coupon) : ?>
					<tr>
						<td><?=$coupon['name']?></td>
						<td><?=$coupon['code']?></td>
						<td><a href="<?=$view_single_coupon_uri.$coupon['coupon_id']?>">View / Edit</a></td>
						<td><a class="js-delete" data-type="coupon" href="<?=$delete_coupon_uri.$coupon['coupon_id']?>">Delete</a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
			</table>
		</div>
	</div>

	<p><a href="<?=$add_coupon_uri?>" class="btn">Add a Coupon</a></p>
</div>