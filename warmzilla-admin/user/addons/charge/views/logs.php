<div id="charge_container" class="ee<?=$ee_ver?>">

	<?php if(!$encrypt_key_set) : ?>
		<p class="setting-txt bg-warning"><strong>No Encryption Key Set!</strong> You need to set a value for <code>encryption_key</code> in your site's config</p>
	<?php endif; ?>

	<div class="box">
		<h1>Charge Logs</h1>

		<div class="settings">
<?php if(count($logs) == 0) { ?>
			<div class="setting-txt text-pad">
				Nothing in the logs just yet. Need a hand getting started? <a href="https://eeharbor.com/charge/documentation">Full Documentation is here</a> or <a href="mailto:help@eeharbor.com">Email support and we'll help you out</a>.
			</div>
<?php } else { ?>
			<table class="data-small" width="100%" style="table-layout:fixed;">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="5%">Site ID</th>
					<th width="5%">Mode</th>
					<th width="10%">Time</th>
					<th width="10%">Member</th>
					<th>Event</th>
					<th>Info</th>
				</tr>
			</thead>
			<tbody>
<?php
		foreach($logs as $set) {
			$first = true;
			foreach($set as $row) {
?>
			<tr>
				<td valign="top"><?=$row['log_id']?></td>
<?php if($first) { ?>
				<td valign="top"><?=$row['site_id']?></td>
				<td valign="top"><?=$row['mode']?></td>
				<td valign="top"><?=$row['time_wordy']?></td>
				<td valign="top">
					<?php if($row['member_id'] == '0') : ?>
						<?php
							if(strpos($row['type'], 'webhook')) echo '<i style="color:#0099e5;" class="fa fa-2x fa-cc-stripe"></i>';
							else echo 'Guest';
						?>
					<?php else : ?>
						<?php if(isset($members[$row['member_id']]) && is_array($members[$row['member_id']])) : ?>
							<a href="<?=$members_cp_uri.$row['member_id']?>"><?=$members[$row['member_id']]['screen_name']?></a>
						<?php else : ?>
							<a href="<?=$members_cp_uri.$row['member_id']?>"><?=$row['member_id']?></a>
						<?php endif; ?>
					<?php endif; ?>
				</td>
				<?php $first = false; ?>
<?php } else { ?>
				<td valign="top"></td>
				<td valign="top"></td>
				<td valign="top"></td>
				<td valign="top">&nbsp; &rdsh;</td>
<?php } ?>
				<td valign="top" style="min-width:300px;"><?=$row['log_icon']?> <?=ee()->lang->line($row['type'])?></td>
				<td valign="top" data-logid="<?=$row['log_id']?>" class="extended_data<?php if(empty($row['extended'])) echo ' empty'; ?>"></td>
			</tr>
			<tr id="extended_data_body<?=$row['log_id']?>" style="display:none;">
				<td colspan="7">
					<div class="extended_data_body">
					<?php if(!empty($row['extended'])) { ?>
						<pre><?=print_R($row['extended'],1)?></pre>
					<?php } ?>
					</div>
					<br />
					<a href="#" data-logid="<?=$row['log_id']?>" class="extended_data_toggle extended_data_toggle_bottom extended_data_toggle<?=$row['log_id']?>">Hide Extended</a>
				</td>
			</tr>
<?php
			}
		}
?>
			</tbody>
			</table>

<?php	if($has_pagination) { ?>
			<ul class="pagination">
				<li>Page <?=$current_page?> of <?=$total_pages?></li>
				<li>
					<?php if( $prev_link ) : ?>
						<a href="<?=$prev_link?>" title="Previous page">Previous</a>
					<?php else : ?>
						<b>Previous</b>
					<?php endif; ?>
				</li>
				<li>
					<?php if( $next_link ) : ?>
						<a href="<?=$next_link?>" title="Next page">Next</a>
					<?php else : ?>
						<b>Next</b>
					<?php endif; ?>
				</li>
			</ul>
<?php
		}
	}
?>
		</div>
	</div>
</div>