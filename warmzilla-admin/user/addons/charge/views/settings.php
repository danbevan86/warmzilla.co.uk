<div id="charge_container" class=" ee<?=$ee_ver?>">
	<?php if(!$encrypt_key_set) : ?>
		<p class="setting-txt bg-warning"><strong>No Encryption Key Set!</strong> You need to set a value for <code>encryption_key</code> in your site's config</p>
	<?php endif; ?>

<?php if ($ee_ver > 2) { ?>
    <div class="app-notice-wrap"><?php echo ee('CP/Alert')->getAllInlines(); ?></div>
<?php } ?>

	<form method="post" action="<?=$base_url?>">
	<input type="hidden" name="XID" value="<?=XID_SECURE_HASH?>"/>

	<div class="box">
		<h1><?php echo lang('charge_settings'); ?></h1>
		<div class="settings">
<?php
	foreach($sections as $section_label => $section_data) {
?>
			<h2><?php echo lang($section_label); ?></h2>
			<table id="preferences" class="data" width="100%">
			<tbody>
<?php
		foreach($section_data as $subsection) {
?>
				<tr>
					<td width="50%" valign="top" class="setting-txt">
						<h3><?php echo $subsection['title']; ?></h3>
						<?php if($subsection['desc']) echo '<em>', $subsection['desc'], '</em>'; ?>
					</td>
					<td valign="top">
<?php
			foreach($subsection['fields'] as $field_name => $field_data) {
				$placeholder = '';
				if(isset($field_data['placeholder']) && !empty($field_data['placeholder'])) $placeholder = 'placeholder="'.$field_data['placeholder'].'"';

				$dataTagString = '';

				if(isset($field_data['datatags']) && is_array($field_data['datatags']) && count($field_data['datatags'])) {
					foreach($field_data['datatags'] as $datatag) {
						$dataTagString .= ' data-'.$datatag['tag'].'="'.$datatag['value'].'"';
					}
				}

				switch($field_data['type']) {
					case 'message':
						echo $field_data['text'], "\n";
						break;

					case 'text':
						echo '<input type="text" name="', $field_name, '" id="', $field_name, '"', $placeholder, ' value="', $field_data['value'], '" ', $dataTagString, ' />', "\n";
						break;

					case 'radio':
						foreach($field_data['choices'] as $choice_value => $choice_name) {
							$checked = '';
							if($field_data['value'] == $choice_value) $checked = ' checked="checked"';

							echo '<label class="choice"><input type="radio" name="', $field_name, '" value="', $choice_value, '"', $checked, ' ', $dataTagString, '> ', $choice_name, '</label><br />';
						}
						break;
					case 'inline_radio':
						foreach($field_data['choices'] as $choice_value => $choice_name) {
							$checked = '';
							if($field_data['value'] == $choice_value) $checked = ' checked="checked"';

							echo '<label class="choice"><input type="radio" name="', $field_name, '" value="', $choice_value, '"', $checked, ' ', $dataTagString, '> ', $choice_name, '</label>';
						}
						break;
					case 'select':
						echo '<select name="', $field_name, '" ', $dataTagString, '>', "\n";

						foreach($field_data['choices'] as $choice_value => $choice_name) {
							$selected = '';
							if($field_data['value'] == $choice_value) $selected = ' selected="selected"';

							echo '<option value="', $choice_value, '"', $selected, '> ', $choice_name, '</option>';
						}

						echo '</select>', "\n";
						break;
					case 'button':
						echo '<button class="', (isset($field_data['class']) ? $field_data['class'] : ''), '" type="', (isset($field_data['button_type']) && !empty($field_data['button_type']) ? $field_data['button_type'] : 'button'), '" ', $dataTagString, '>', $field_data['value'], '</button>';
						break;
				}
			}
?>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
			</table>
<?php
	}
?>
		</div>
	</div>

	<input type="submit" class="btn " name="submit" value="<?php echo $save_btn_text; ?>" />
	</form>
</div>
