<div id="charge_container" class="ee<?=$ee_ver?>">
	<div class="box">
		<h1>Actions</h1>

		<div class="settings">
			<div class="setting-txt">
				<em>Actions are triggered after successful purchases. They can update entries, send emails, and add members to subscriptions.</em>
			</div>

			<table class="data" width="100%">
			<tbody>
				<?php foreach($actions as $act) : ?>
					<tr>
						<td><?=$act['name']?></td>
						<td><?=$act['shortname']?></td>
						<td><a href="<?=$view_single_action_uri.$act['action_id']?>">View / Edit</a></td>
						<td><a class="js-delete" data-type="action" href="<?=$delete_action_uri.$act['action_id']?>">Delete</a></td>
					</tr>
				<?php endforeach ?>

			</tbody>
			</table>
		</div>
	</div>

	<p><a href="<?=$add_action_uri?>" class="btn">Add an Action</a></p>
</div>