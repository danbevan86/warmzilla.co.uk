<?php

// This is what we have to do for EE2 support
require_once 'addon.setup.php';

use EEHarbor\Charge\FluxCapacitor\FluxCapacitor;
use EEHarbor\Charge\FluxCapacitor\Base\Upd;

/**
 * Charge Update Class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_upd extends Upd
{

    // --------------------------------------------------------------------
    // PROPERTIES
    // --------------------------------------------------------------------
    private $settings;
    private $create_table_collation = '';

    private $actions = array(
        'act_charge',
        'act_webhook',
        'act_user',
        'act_update_card',
        'act_update_plan',
        'act_validate_coupon'
    );

    private $csrf_exempt = array('act_webhook');


    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor: sets EE instance
     *
     * @access      public
     * @return      null
     */
    public function __construct()
    {
        parent::__construct();

        $this->settings = $this->flux->getSettings();

        // Define the package path
        ee()->load->add_package_path(PATH_THIRD.'charge');

        // Load our helper
        ee()->load->helper('Charge');
        ee()->lang->loadfile('charge');

        // Load base model
        if (!class_exists('Charge_model')) {
            ee()->load->library('Charge_model');
        }
        if (!isset(ee()->charge_stripe)) {
            ee()->charge_model->load_models($this->flux);
        }

        if ($this->flux->ver_gte(4)) {
            $this->create_table_collation = ' DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';
        }
    }

    // --------------------------------------------------------------------

    /**
     * Install the module
     *
     * @access      public
     * @return      bool
     */
    public function install()
    {
        // --------------------------------------
        // Install tables
        // --------------------------------------


        // create settings table
        ee()->db->query("CREATE TABLE IF NOT EXISTS `".ee()->db->dbprefix('charge_settings')."` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `site_id` int(11) NOT NULL DEFAULT '0',
            `charge_stripe_account_mode` char(4) NOT NULL DEFAULT '',
            `charge_stripe_test_credentials_sk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_test_credentials_pk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_live_credentials_sk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_live_credentials_pk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_currency` char(3) NOT NULL DEFAULT '',
            `charge_webhook_key` varchar(32) NOT NULL DEFAULT '',
            `charge_force_ssl` varchar(3) NOT NULL DEFAULT '',
            `charge_metadata_pass` varchar(3) NOT NULL DEFAULT '',
            `charge_email_send_from` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_name` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_reply_to` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_reply_to_name` varchar(50) NOT NULL DEFAULT '',
            `charge_log_level` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            KEY `site_id` (`site_id`))".$this->create_table_collation);

        $default_site_email_settings = ee()->charge_email->get_site_defaults();

        $setting_data = array(
            'site_id' => ee()->config->item('site_id'),
            'charge_stripe_account_mode' => 'test',
            'charge_stripe_currency' => 'usd',
            'charge_force_ssl' => 'no',
            'charge_metadata_pass' => 'yes',
            'charge_email_send_from' => $default_site_email_settings['from'],
            'charge_email_send_name' => $default_site_email_settings['name'],
            'charge_log_level' => 5
        );

        // Find out if the settings exist, if not, insert them.
        ee()->db->where('site_id', ee()->config->item('site_id'));
        $exists = ee()->db->count_all_results('charge_settings');

        // Update or insert the legacy settings into the new table.
        // The table should always exist when installed for the fist time as we inserted defaults and
        // never exist for updates to an older install. This check is just for edge cases.
        if ($exists) {
            ee()->db->where('site_id', ee()->config->item('site_id'));
            ee()->db->update('charge_settings', $setting_data);
        } else {
            ee()->db->insert('charge_settings', $setting_data);
        }

        ee()->charge_log->install();
        ee()->charge_stripe->install();
        ee()->charge_coupon->install();
        ee()->charge_webhook->install();
        ee()->charge_subscription->install();
        ee()->charge_subscription_member->install();
        ee()->charge_action->install();

        ee()->db->query("CREATE TABLE IF NOT EXISTS `".ee()->db->dbprefix('charge_field_data')."` (
            `entry_id` int(11) unsigned NOT NULL,
            `field_name` varchar(55) NOT NULL DEFAULT '',
            `field_data` text NOT NULL,
            PRIMARY KEY (`entry_id`,`field_name`))".$this->create_table_collation);

        $fields = array(
            'member_id'   => array('type' => 'int', 'constraint' => '10', 'unsigned' => true, 'null' => false),
            'customer_id' => array('type' => 'varchar', 'constraint' => '64', 'null' => false),
            'mode'        => array('type' => 'varchar', 'constraint' => '4', 'null' => false),
        );

        ee()->load->dbforge();
        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key(array('member_id', 'customer_id'), true);
        ee()->dbforge->create_table('charge_customers');

        // --------------------------------------
        // Register our actions
        // --------------------------------------

        foreach ($this->actions as $action) {
            $csrf_exempt = 0;
            if (isset($this->csrf_exempt[$action])) {
                $csrf_exempt = 1;
            }

            if (APP_VER >= '2.7') {
                ee()->db->insert('actions', array(
                    'class'  => 'Charge',
                    'method' => $action,
                    'csrf_exempt' => $csrf_exempt
                ));
            } else {
                // No csrf on < 2.7
                ee()->db->insert('actions', array(
                    'class'  => 'Charge',
                    'method' => $action
                ));
            }
        }

        // --------------------------------------
        // Add row to modules table
        // --------------------------------------
        ee()->db->insert('modules', array(
            'module_name'    => 'Charge',
            'module_version' => $this->settings->version,
            'has_cp_backend' => 'y',
            'has_publish_fields' => 'y'
        ));

        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Uninstall the module
     *
     * @return  bool
     */
    public function uninstall()
    {
        // --------------------------------------
        // get module id
        // --------------------------------------

        $query = ee()->db->select('module_id')
                ->from('modules')
               ->where('module_name', 'Charge')
               ->get();

        // --------------------------------------
        // remove references from modules
        // --------------------------------------

        ee()->db->where('module_name', 'Charge');
        ee()->db->delete('modules');

        // --------------------------------------
        // Uninstall tables
        // --------------------------------------

        ee()->charge_log->uninstall();
        ee()->charge_stripe->uninstall();
        ee()->charge_coupon->uninstall();
        ee()->charge_webhook->uninstall();
        ee()->charge_subscription->uninstall();
        ee()->charge_subscription_member->uninstall();
        ee()->charge_action->uninstall();

        ee()->db->query("DROP TABLE IF EXISTS ".ee()->db->dbprefix('charge_settings'));
        ee()->db->query("DROP TABLE IF EXISTS ".ee()->db->dbprefix('charge_field_data'));
        ee()->db->query("DROP TABLE IF EXISTS ".ee()->db->dbprefix('charge_customers'));

        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Update the module
     *
     * @return  bool
     */
    public function update($current = '')
    {
        // Load base model
        if (!class_exists('Charge_model')) {
            ee()->load->library('Charge_model');
        }
        if (!isset(ee()->charge_stripe)) {
            ee()->charge_model->load_models();
        }

        // --------------------------------------
        // Same Version - nothing to do
        // --------------------------------------

        if ($current == '' or version_compare($current, $this->settings->version) === 0) {
            return false;
        }

        if (version_compare($current, '0.5', '<')) {
            $this->_update_from_0_5_0();
        }

        if (version_compare($current, '1.0', '<')) {
            $this->_update_from_1_0_0();
        }

        if (version_compare($current, '1.1.1', '<')) {
            $this->_update_from_1_1_1();
        }

        if (version_compare($current, '1.4.0', '<')) {
            $this->_update_from_1_4_0();
        }

        if (version_compare($current, '1.5.0', '<')) {
            $this->_update_from_1_5_0();
        }

        if (version_compare($current, '1.6.2', '<')) {
            $this->_update_from_1_6_2();
        }

        if (version_compare($current, '1.7.0', '<')) {
            $this->_update_from_1_7_0();
        }

        if (version_compare($current, '1.7.4', '<')) {
            $this->_update_from_1_7_4();
        }

        if (version_compare($current, '1.7.5', '<')) {
            $this->_update_from_1_7_5();
        }

        if (version_compare($current, '1.8.12.b3', '<')) {
            $this->_update_from_1_8_11();
        }


        if (version_compare($current, '1.8.14', '<')) {
            $this->_update_from_1_8_14();
        }

        if (version_compare($current, '1.9.3', '<')) {
            $this->_update_from_1_9_3();
        }

        if (version_compare($current, '1.10.0', '<')) {
            $this->_update_from_1_9_9();
        }

        if (version_compare($current, '1.10.0.b2', '<')) {
            $this->_update_from_1_10_0_b2();
        }

        if (version_compare($current, '1.12', '<')) {
            $this->_update_from_1_10_0();
        }

        if (version_compare($current, '1.13', '<')) {
            $this->_update_from_1_12();
        }

        if (version_compare($current, '1.14', '<')) {
            $this->_update_from_1_13();
        }

        if (version_compare($current, '1.15', '<')) {
            $this->_update_from_1_14();
        }

        if (version_compare($current, '1.16', '<')) {
            $this->_update_from_1_15();
        }

        if (version_compare($current, '2.0.0-a.1', '<')) {
            $this->_update_from_1_16();
        }

        if (version_compare($current, '2.0.0-b.1', '<')) {
            $this->_update_from_2_0_0_a1();
        }

        if (version_compare($current, '2.0.13', '<')) {
            $this->_update_from_2_0_0_b1();
        }

        if (version_compare($current, '3.0.0-b.3', '<')) {
            $this->_update_from_3_0_0_b1();
        }


        // Get the current actions list and compare to the actions list up top.
        $current_actions = ee()->db->where('class', 'Charge')->get('actions')->result_array();

        $actions = $this->actions;

        foreach ($current_actions as $act) {
            if (in_array($act['method'], $actions)) {
                foreach ($actions as $key => $action) {
                    if ($action == $act['method']) {
                        unset($actions[ $key ]);
                    }
                }
            }
        }

        // Now just add the ones that aren't there
        foreach ($actions as $action) {
            $csrf_exempt = 0;
            if (isset($this->csrf_exempt[$action])) {
                $csrf_exempt = 1;
            }

            if (APP_VER >= '2.7') {
                ee()->db->insert('actions', array(
                    'class'  => 'Charge',
                    'method' => $action,
                    'csrf_exempt' => $csrf_exempt
                ));
            } else {
                // No csrf on < 2.7
                ee()->db->insert('actions', array(
                    'class'  => 'Charge',
                    'method' => $action
                ));
            }
        }

        // Just as a safety catch, verify all the actions are correctly csrf_exempt
        $this->_verify_csrf();

        // Returning TRUE updates db version number
        return true;
    }

    private function _verify_csrf()
    {
        if (APP_VER >= '2.7') {
            ee()->db->query("UPDATE exp_actions SET csrf_exempt = 1 WHERE method = 'act_webhook'");
        }
    }

    private function _update_from_3_0_0_b1()
    {
        if (! ee()->db->table_exists('charge_customers')) {
            $fields = array(
                'member_id'   => array('type' => 'int', 'constraint' => '10', 'unsigned' => true, 'null' => false),
                'customer_id' => array('type' => 'varchar', 'constraint' => '64', 'null' => false),
                'mode'        => array('type' => 'varchar', 'constraint' => '4', 'null' => false),
            );

            ee()->load->dbforge();
            ee()->dbforge->add_field($fields);
            ee()->dbforge->add_key(array('member_id', 'customer_id'), true);
            ee()->dbforge->create_table('charge_customers');
        }

        ee()->db->query("ALTER TABLE `exp_charge_log` MODIFY `extended` MEDIUMTEXT NULL default NULL");
        ee()->db->query("ALTER TABLE `exp_charge_stripe` MODIFY `stripe` MEDIUMTEXT NULL default NULL");
    }

    private function _update_from_2_0_0_b1()
    {
        // Make sure the user has the fieldtype setup in their install.
        $hasFieldtype = ee()->db->select('fieldtype_id, version')->get_where('fieldtypes', array('name'=>'charge'), 1)->result_array();
        if (empty($hasFieldtype)) {
            $fieldtypeData['name'] = 'charge';
            $fieldtypeData['version'] = $this->settings->version;
            $fieldtypeData['settings'] = 'YTowOnt9'; // Base64 encoded version of a serialized empty string.
            $fieldtypeData['has_global_settings'] = 'y';
            ee()->db->insert('fieldtypes', $fieldtypeData);
        }
    }

    private function _update_from_2_0_0_a1()
    {
        // Do some fancy footwork to get around MySQL's Strict Mode if we have invalid DATE format data in the column.
        if (ee()->db->field_exists('redeem_by', 'exp_charge_coupon') !== false) {
            ee()->db->query("ALTER TABLE `exp_charge_coupon` MODIFY `redeem_by` VARCHAR(255) NULL default NULL");
            ee()->db->query("UPDATE `exp_charge_coupon` SET redeem_by=NULL WHERE redeem_by='0'");
            ee()->db->query("ALTER TABLE `exp_charge_coupon` CHANGE `redeem_by` `end_date` DATE NULL default NULL");
        }

        // Make sure the various columns we need to add don't already exist.
        if (ee()->db->field_exists('start_date', 'exp_charge_coupon') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_coupon` ADD `start_date` DATE NULL default NULL AFTER `max_redemptions`");
        }

        // Make sure the various columns we need to add don't already exist.
        if (ee()->db->field_exists('member_id', 'exp_charge_coupon') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_coupon` ADD `member_id` int(10) unsigned NOT NULL default 0");
        }

        // Make sure the various columns we need to add don't already exist.
        if (ee()->db->field_exists('auto_apply', 'exp_charge_coupon') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_coupon` ADD `auto_apply` tinyint(1) unsigned NOT NULL default 0");
        }

        // Make sure the various columns we need to add don't already exist.
        if (ee()->db->field_exists('entry_id', 'exp_charge_coupon') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_coupon` ADD `entry_id` int(10) unsigned NOT NULL default 0");
        }
    }

    private function _update_from_1_16()
    {
        // Create the charge settings table.
        ee()->db->query("CREATE TABLE IF NOT EXISTS `".ee()->db->dbprefix('charge_settings')."` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `site_id` int(11) NOT NULL DEFAULT '0',
            `charge_stripe_account_mode` char(4) NOT NULL DEFAULT '',
            `charge_stripe_test_credentials_sk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_test_credentials_pk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_live_credentials_sk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_live_credentials_pk` varchar(50) NOT NULL DEFAULT '',
            `charge_stripe_currency` char(3) NOT NULL DEFAULT '',
            `charge_webhook_key` varchar(32) NOT NULL DEFAULT '',
            `charge_force_ssl` varchar(3) NOT NULL DEFAULT '',
            `charge_metadata_pass` varchar(3) NOT NULL DEFAULT '',
            `charge_email_send_from` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_name` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_reply_to` varchar(50) NOT NULL DEFAULT '',
            `charge_email_send_reply_to_name` varchar(50) NOT NULL DEFAULT '',
            `charge_log_level` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            KEY `site_id` (`site_id`))".$this->create_table_collation);

        // Copy the existing settings from the ee system preferences.
        if (!isset($site_id) || empty($site_id)) {
            $site_id = ee()->config->item('site_id');
        }
        $sql = "SELECT site_system_preferences FROM exp_sites WHERE site_id=".ee()->db->escape_str($site_id);
        $query = ee()->db->query($sql);
        if ($query->num_rows() == 0) {
            return false;
        }

        ee()->load->helper('string');
        $legacySettings = unserialize(base64_decode($query->row('site_system_preferences')));

        $setting_data = array(
            'site_id' => $site_id,
            'charge_stripe_account_mode' => (!empty($legacySettings['charge_stripe_account_mode']) ? $legacySettings['charge_stripe_account_mode'] : 'test'),
            'charge_stripe_test_credentials_sk' => (!empty($legacySettings['charge_stripe_test_credentials_sk']) ? $legacySettings['charge_stripe_test_credentials_sk'] : ''),
            'charge_stripe_test_credentials_pk' => (!empty($legacySettings['charge_stripe_test_credentials_pk']) ? $legacySettings['charge_stripe_test_credentials_pk'] : ''),
            'charge_stripe_live_credentials_sk' => (!empty($legacySettings['charge_stripe_live_credentials_sk']) ? $legacySettings['charge_stripe_live_credentials_sk'] : ''),
            'charge_stripe_live_credentials_pk' => (!empty($legacySettings['charge_stripe_live_credentials_pk']) ? $legacySettings['charge_stripe_live_credentials_pk'] : ''),
            'charge_stripe_currency' => (!empty($legacySettings['charge_stripe_currency']) ? $legacySettings['charge_stripe_currency'] : ''),
            'charge_webhook_key' => (!empty($legacySettings['charge_webhook_key']) ? $legacySettings['charge_webhook_key'] : ''),
            'charge_force_ssl' => (!empty($legacySettings['charge_force_ssl']) ? $legacySettings['charge_force_ssl'] : ''),
            'charge_metadata_pass' => (!empty($legacySettings['charge_metadata_pass']) ? $legacySettings['charge_metadata_pass'] : ''),
            'charge_email_send_from' => (!empty($legacySettings['charge_email_send_from']) ? $legacySettings['charge_email_send_from'] : ''),
            'charge_email_send_name' => (!empty($legacySettings['charge_email_send_name']) ? $legacySettings['charge_email_send_name'] : ''),
            'charge_email_send_reply_to' => (!empty($legacySettings['charge_email_send_reply_to']) ? $legacySettings['charge_email_send_reply_to'] : ''),
            'charge_email_send_reply_to_name' => (!empty($legacySettings['charge_email_send_reply_to_name']) ? $legacySettings['charge_email_send_reply_to_name'] : ''),
            'charge_log_level' => (!empty($legacySettings['charge_log_level']) ? $legacySettings['charge_log_level'] : 5)
        );

        // Find out if the settings exist, if not, insert them.
        ee()->db->where('site_id', ee()->config->item('site_id'));
        $exists = ee()->db->count_all_results('charge_settings');

        // Update or insert the legacy settings into the new table.
        // The table should always exist when installed for the fist time as we inserted defaults and
        // never exist for updates to an older install. This check is just for edge cases.
        if ($exists) {
            ee()->db->where('site_id', ee()->config->item('site_id'));
            ee()->db->update('charge_settings', $setting_data);
        } else {
            ee()->db->insert('charge_settings', $setting_data);
        }

        // Remove the settings from the ee system preferences.
        unset($legacySettings['charge_stripe_account_mode']);
        unset($legacySettings['charge_stripe_test_credentials_sk']);
        unset($legacySettings['charge_stripe_test_credentials_pk']);
        unset($legacySettings['charge_stripe_live_credentials_sk']);
        unset($legacySettings['charge_stripe_live_credentials_pk']);
        unset($legacySettings['charge_stripe_currency']);
        unset($legacySettings['charge_webhook_key']);
        unset($legacySettings['charge_force_ssl']);
        unset($legacySettings['charge_metadata_pass']);
        unset($legacySettings['charge_email_send_from']);
        unset($legacySettings['charge_email_send_name']);
        unset($legacySettings['charge_email_send_reply_to']);
        unset($legacySettings['charge_email_send_reply_to_name']);
        unset($legacySettings['charge_log_level']);

        $prefs = base64_encode(serialize($legacySettings));

        ee()->db->query(ee()->db->update_string('exp_sites', array('site_system_preferences'=>$prefs), array('site_id'=>ee()->db->escape_str($site_id))));
    }

    private function _update_from_1_15()
    {
        if (ee()->db->field_exists('current_period_end', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `current_period_end` int(10) unsigned NOT NULL default 0");
        }
    }

    private function _update_from_1_14()
    {
        if (ee()->db->field_exists('at_period_end', 'exp_charge_subscription_member') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_subscription_member` ADD `at_period_end` tinyint(1) unsigned NOT NULL default 0");
        }
    }

    private function _update_from_1_13()
    {
        if (ee()->db->field_exists('chargecart_unique_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `chargecart_unique_id` varchar(100) NOT NULL DEFAULT ''");
        }
    }

    private function _update_from_1_12()
    {
        ee()->db->query("CREATE TABLE IF NOT EXISTS `".ee()->db->dbprefix('charge_field_data')."` (
            `entry_id` int(11) unsigned NOT NULL,
            `field_name` varchar(55) NOT NULL DEFAULT '',
            `field_data` text NOT NULL,
            PRIMARY KEY (`entry_id`,`field_name`))".$this->create_table_collation);
    }

    private function _update_from_1_10_0()
    {
        if (ee()->db->field_exists('multi_payment_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `multi_payment_id` int(10) unsigned NOT NULL DEFAULT 0");
        }
    }


    private function _update_from_1_10_0_b2()
    {
        if (ee()->db->field_exists('mode', 'exp_charge_log') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_log` ADD `mode` varchar(100) NOT NULL DEFAULT ''");
        }
    }


    private function _update_from_1_9_9()
    {
        if (ee()->db->field_exists('request_key', 'exp_charge_log') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_log` ADD `request_key` varchar(255) NOT NULL DEFAULT ''");
        }

        ee()->db->query("TRUNCATE `exp_charge_log`");
    }

    private function _update_from_1_9_3()
    {
        if (ee()->db->field_exists('connected_entry_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `connected_entry_id` varchar(100) NOT NULL DEFAULT ''");
        }
    }

    private function _update_from_1_8_14()
    {
        if (ee()->db->field_exists('payment_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `payment_id` varchar(100) NOT NULL DEFAULT ''");
        }
    }


    private function _update_from_1_8_11()
    {
        if (ee()->db->field_exists('plan_trial_end', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_trial_end` int(10) unsigned NOT NULL default 0");
        }
    }

    private function _update_from_1_7_5()
    {
        if (APP_VER >= '2.7') {
            ee()->db->query("UPDATE exp_actions SET csrf_exempt = 1 WHERE method = 'act_webhook'");
        }
    }

    private function _update_from_1_7_4()
    {
        if (ee()->db->field_exists('plan_coupon', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_coupon` varchar(100) NOT NULL");
        }
        if (ee()->db->field_exists('plan_coupon_stripe_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_coupon_stripe_id` varchar(100) NOT NULL");
        }
        if (ee()->db->field_exists('plan_discount', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_discount` int(10) unsigned NOT NULL");
        }
        if (ee()->db->field_exists('plan_full_amount', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_full_amount` int(10) unsigned NOT NULL");
        }

        ee()->charge_coupon->install();
    }

    private function _update_from_1_7_0()
    {
        if (ee()->db->field_exists('messages', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `messages` text NOT NULL");
        }
        if (ee()->db->field_exists('card_fingerprint', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `card_fingerprint` varchar(255) NOT NULL");
        }
    }

    // --------------------------------------------------------------------

    private function _update_from_1_6_2()
    {
        if (ee()->db->field_exists('plan_length', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_length` int(10) NOT NULL");
        }
        if (ee()->db->field_exists('plan_length_interval', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_length_interval` varchar(255) NOT NULL");
        }
        if (ee()->db->field_exists('plan_length_expiry', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_length_expiry` int(10) NOT NULL");
        }
    }

    // --------------------------------------------------------------------


    private function _update_from_1_5_0()
    {
        if (ee()->db->field_exists('customer_id', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `customer_id` varchar(255) NOT NULL");
        }

        // We also need to try to extract the customer_id from the stripe blob for all the users
        $charges = ee()->charge_stripe->get_all();
        $customers = array();

        foreach ($charges as $charge) {
            if (isset($charge['customer_id']) and $charge['customer_id'] != '') {
                continue;
            }

            $charge_id = $charge['id'];
            $customer_id = '';

            if ($charge['stripe_object'] == 'customer') {
                $customer_id = $charge['stripe_id'];
            } elseif (isset($charge['stripe_card_customer'])) {
                $customer_id = $charge['stripe_card_customer'];
            }

            if ($customer_id != '') {
                $customers[$charge['id']] = $customer_id;
            }
        }

        foreach ($customers as $cus => $cus_id) {
            ee()->charge_stripe->update($cus, array('customer_id' => $cus_id));
        }
    }





    // --------------------------------------------------------------------

    private function _update_from_1_4_0()
    {
        if (ee()->db->field_exists('charge_id', 'exp_charge_subscription_member') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_subscription_member` ADD `charge_id` int(10) unsigned NOT NULL");
        }
        if (ee()->db->field_exists('plan_trial_days', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `plan_trial_days` int(10) unsigned NOT NULL");
        }
        if (ee()->db->field_exists('state', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `state` varchar(255) NOT NULL");
        }
        if (ee()->db->field_exists('ended_on', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `ended_on` int(10) unsigned NOT NULL");
        }

        // Set all the states to be active too
        ee()->db->query("UPDATE `exp_charge_stripe` SET `state` = \"active\"");

        return true;
    }

    // --------------------------------------------------------------------

    private function _update_from_1_1_1()
    {
        if (ee()->db->field_exists('card_exp_month', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `card_exp_month` varchar(255) NOT NULL");
        }
        if (ee()->db->field_exists('card_exp_year', 'exp_charge_stripe') === false) {
            ee()->db->query("ALTER TABLE `exp_charge_stripe` ADD `card_exp_year` varchar(255) NOT NULL");
        }

        return true;
    }

    // --------------------------------------------------------------------

    private function _update_from_1_0_0()
    {
        ee()->charge_webhook->install();
        ee()->charge_subscription->install();
        ee()->charge_subscription_member->install();
        ee()->charge_action->install();

        return true;
    }

    // --------------------------------------------------------------------

    private function _update_from_0_5_0()
    {
        if (ee()->db->field_exists('mode', 'exp_charge_stripe') === false) {
            $sql = "ALTER TABLE `exp_charge_stripe` ADD `mode` varchar(255) NOT NULL";
        }
    }
} // End class

/* End of file upd.Charge.php */
