<?php

return array(
      'author'          => 'Yuri Salimovskiy',
      'author_url'      => 'http://www.intoeetive.com',
      'name'            => 'Threaded Comments',
      'description'     => 'Enables nested commenting and custom comment fields',
      'version'         => '3.0.3',
      'namespace'       => 'Intoeetive\Threadedcomments',
      'settings_exist'  => TRUE,
);