<?php

$lang = [
    'label'                       => 'Label',
    'handle'                      => 'Handle',
    'is_default'                  => 'Is Default?',
    'blocked_spam_count'          => 'Blocked Spam',
    'no_available_composer_forms' => 'No available composer forms',
];
