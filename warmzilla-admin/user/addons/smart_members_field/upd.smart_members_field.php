<?php if (!defined('BASEPATH')) {exit('No direct script access allowed'); }

/**
* The software is provided "as is", without warranty of any
* kind, express or implied, including but not limited to the
* warranties of merchantability, fitness for a particular
* purpose and noninfringement. in no event shall the authors
* or copyright holders be liable for any claim, damages or
* other liability, whether in an action of contract, tort or
* otherwise, arising from, out of or in connection with the
* software or the use or other dealings in the software.
* -----------------------------------------------------------
* ZealousWeb - Smart Members Field
*
* @package      SmartMembersField
* @author       ZealousWeb
* @copyright    Copyright (c) 2018, ZealousWeb.
* @link         http://zealousweb.com/expressionengine/smart-members-field
* @filesource   ./system/user/addons/smart_members_field/upd.smart_members_field.php
*
*/

require_once PATH_THIRD . 'smart_members_field/config.php';

class Smart_members_field_upd
{
	public $version = SMF_VER;	
	private $module_name = SMF_MOD_NAME;

	/* Initialize constructor */
	public function __construct()
	{
		ee()->load->dbforge();
	}

	/**
	 * Install the module
	 * @param $setting       (Setting array optional)
	 * @return boolean TRUE
	 **/
	public function install($settings = array())
	{
		$mod_data = array(
			'module_name' 			=> $this->module_name,
			'module_version' 		=> $this->version,
			// 'settings' 				=> serialize($settings),
			'has_cp_backend' 		=> "y",
			'has_publish_fields'	=> 'n',
		);
		ee()->db->insert('modules', $mod_data);

		/*Default method*/
		$data = array(
			'class' 	=> $this->module_name,
			'method' 	=> 'smf_default_method',
		);
		ee()->db->insert('actions', $data);

		return TRUE;
	}

	/**
	 * Uninstall the module
	 * @return boolean TRUE
	 **/
	public function uninstall()
	{

		ee()->db->select('module_id');
		$query = ee()->db->get_where('modules', array('module_name' => $this->module_name));

		ee()->db->where('module_name', $this->module_name);
		ee()->db->delete('modules');

		ee()->db->where('class', $this->module_name);
		ee()->db->delete('actions');

		return TRUE;

	}

}
// END CLASS

// EOF
