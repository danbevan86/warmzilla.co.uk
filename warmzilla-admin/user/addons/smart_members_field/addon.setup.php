<?php

require PATH_THIRD . 'smart_members_field/config.php';

return array(
	'author'      		=> SMF_AUTHOR,
	'author_url'  		=> SMF_AUTHOR_URL,
	'name'        		=> SMF_NAME,
	'description' 		=> SMF_DESC,
	'version'     		=> SMF_VER,
	'namespace'   		=> 'ZealousWeb\Addons\SmartMembers',
	'settings_exist'	=> FALSE,
	'docs_url' 			=> SMF_DOC_URL,
);