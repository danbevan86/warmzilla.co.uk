<?php
if (!defined('SMF_VER'))
{
	define('SMF_VER', '1.0.0');
	define('SMF_NAME', 'Smart Members Field');
	define('SMF_DESC', 'Manage the members and member fields in smart way.');
	define('SMF_MOD_NAME', 'Smart_members_field');
	define('SMF_AUTHOR', 'ZealousWeb');
	define('SMF_AUTHOR_URL', 'http://www.zealousweb.com/');
	define('SMF_DOC_URL', "http://www.zealousweb.com/expressionengine/smart-members-field");	
}

$config['smf_version'] 	= SMF_VER;
$config['smf_name'] 	= SMF_NAME;
$config['smf_mod_name'] = SMF_MOD_NAME;
$config['smf_doc_url'] 	= SMF_DOC_URL;

?>