<?php
/**
* The software is provided "as is", without warranty of any
* kind, express or implied, including but not limited to the
* warranties of merchantability, fitness for a particular
* purpose and noninfringement. in no event shall the authors
* or copyright holders be liable for any claim, damages or
* other liability, whether in an action of contract, tort or
* otherwise, arising from, out of or in connection with the
* software or the use or other dealings in the software.
* -----------------------------------------------------------
* ZealousWeb - Smart Members Field
*
* @package      SmartMembersField
* @author       ZealousWeb
* @copyright    Copyright (c) 2018, ZealousWeb.
* @link         http://zealousweb.com/expressionengine/smart-members-field
* @filesource   ./system/expressionengine/third_party/smart_members_field/models/smf_model.php
*
*/

class Smf_model extends CI_Model 
{

	function getGridValues($colID = '', $gridFieldId = '', $entryID = '')
	{
		ee()->db->select($colID);
		ee()->db->from($gridFieldId);
		ee()->db->where('entry_id', $entryID);
		$get = ee()->db->get();
		if($get->num_rows == 0) return false;

		return $get->result();
	}
	
}

// EOF
