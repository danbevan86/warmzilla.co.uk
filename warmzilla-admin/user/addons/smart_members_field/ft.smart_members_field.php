<?php
/**
* The software is provided "as is", without warranty of any
* kind, express or implied, including but not limited to the
* warranties of merchantability, fitness for a particular
* purpose and noninfringement. in no event shall the authors
* or copyright holders be liable for any claim, damages or
* other liability, whether in an action of contract, tort or
* otherwise, arising from, out of or in connection with the
* software or the use or other dealings in the software.
* -----------------------------------------------------------
* ZealousWeb - Smart Members Field
*
* @package      SmartMembersField
* @author       ZealousWeb
* @copyright    Copyright (c) 2018, ZealousWeb.
* @link         http://zealousweb.com/expressionengine/smart-members-field
* @filesource   ./system/expressionengine/third_party/smart_members_field/ft.smart_members_field.php
*
*/

/**
 * Smart Members Field Fieldtype
 */
class Smart_members_field_ft extends EE_Fieldtype 
{	

	public $info = array();
	public $has_array_data = TRUE;
	private $errors;

	/**
	 * Fetch the fieldtype's name and version from its addon.setup.php file.
	 */
	public function __construct()
	{
		$addon = ee('Addon')->get('smart_members_field');
		$this->info = array(
			'name'    => $addon->getName(),
			'version' => $addon->getVersion()
		);

		/* To Load the Needful Language File */
		ee()->lang->loadfile('smart_members_field');

		/*Load helpful libraries*/
		ee()->load->library('smf_lib', null, 'smf');

	}

	/**
	 * Displays the field for channel, grid and fluid
	 *
	 * @param string $name Stored data for the field
	 * @return string Field display
	 */
	public function accepts_content_type($name)
	{
		return ($name == 'channel' || $name == 'grid' ||  $name == 'fluid_field');
	}

	/**
	 * Displays the field in the CP 
	 *
	 * @param array $data Stored data for the field
	 * @return string Field display
	 */
	public function display_field($data)
	{
		return ee()->smf->displayField($data, $this->settings, $this->field_name, $this->content_id, $this->field_id, $this->content_type);
	}

	/**
	 * Save Field
	 *
	 * In our case the actual field entry will be blank, so we'll simply
	 * cache some data for the post_save method.
	 *
	 * @param	field data
	 * @return	column data
	 */
	public function save($data, $model = NULL)
	{		

		if(is_array($data) && ! empty($data) && count($data) > 0)
		{
			if(is_array($data['data']) && ! empty($data['data']) && count($data['data']) > 0)
			{
				$data = implode('|', $data['data']);
			}
			else
			{
				$data = implode('|', $data);	
			}
		}
		return $data;
	}

	/**
	 * Show the tag on the frontend
	 *
	 * @param	column data
	 * @param	tag parameters
	 * @param	tag pair contents
	 * @return	parsed output
	 */
	public function replace_tag($data, $params = '', $tagdata = '')
	{
		return ee()->smf->replaceTag($data, $params, $tagdata, $this->settings, $this->id());
	}

	/**
	 * Displays the settings for the field in CP
	 *
	 * @param array $data Stored data for the field
	 * @return array Field Settings display
	 */
	public function display_settings($data)
	{
		return ee()->smf->displaySettings($data, $this->content_type());
	}

	/**
	 * Saves the settings for the field in CP
	 *
	 * @param array $data containes all settings of smart members field
	 * @return array saves the all information of smart members field
	 */
	public function save_settings($data)
	{
		return ee()->smf->saveSettings($data, $this->content_type());
	}

	/**
	 * Validate the field data in the CP
	 *
	 * @param array $data to validate the field data
	 * @return array retunrs the validated data or validation errors of field
	 */
	function validate($data = array())
	{
		/*$data = isset($data['data']) ? $data['data'] : array();

		foreach ($data as $i => $child_id)
		{
			if ( ! $child_id)
			{
				continue;
			}

			$set[] = $child_id;
		}

		return TRUE;*/
	}

	/**
	 * Validate the settings data in the CP
	 *
	 * @param array $data to validate the settings data
	 * @return array retunrs the validated data or validation errors of settings
	 */
	function validate_settings($data = array())
	{
	 	return ee()->smf->validateSettings($data);
	}
	
}

// EOF