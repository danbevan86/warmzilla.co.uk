<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Smf_custom_validation {
	
	public $validator;
	
	/* Initialize constructor */
	public function __construct()
	{
		$this->validator = ee('Validation')->make();
		$this->callAllValidationMethods();
	}

	/**
	 * call all validation methods
	 *
	 * @return array retunrs the validated data or validation errors 
	 */
	public function callAllValidationMethods()
	{

		/* rule define for unique display name */
		$this->validator->defineRule('validDisplayName', function($key, $value, $parameters)
        {

        	/* to exclude from the Display Names */
        	$excludeDisplayNames = array('screen_name', 'username', 'email', 'member_id');

        	$customMemberFields = ee('Model')->get('MemberField')
			->fields('m_field_id', 'm_field_name')
			->all()
			->getDictionary('m_field_id', 'm_field_name');
			
			if(isset($customMemberFields) && is_array($customMemberFields) && count($customMemberFields) > 0)
			{
				$excludeDisplayNames = array_merge($excludeDisplayNames, $customMemberFields);
			}
			unset($customMemberFields);
			
			/* removes the curly brackets from the Display Name */
        	$wrongNames 	= array_diff($parameters, $excludeDisplayNames);
            if(is_array($wrongNames) && count($wrongNames) > 0)
            {
            	return 'custom_member_fields_desc';
            }

            return true;
            
        });
        
	}

}