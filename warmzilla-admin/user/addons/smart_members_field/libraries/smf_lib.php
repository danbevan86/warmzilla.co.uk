<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Smf_lib
{

	public $fields;
	public $form_errors;
	public $error_prefix;
	public $error_suffix;
	public $smfSettings;

	/* Initialize constructor */
	function __construct() 
	{

		/* Needful Helper classes */
		ee()->load->helper(array('security', 'string', 'form', 'url', 'text'));

		/* Needful model classes */
		ee()->load->model('smf_model', 'smf_model');

		/* Custom validation */
        ee()->load->library('smf_custom_validation', null, 'smfCustomValidation');

        /* Defining error prefix and error suffix */
		$this->error_prefix = '<em class="ee-form-error-message">';
		$this->error_suffix = '</em>';

		if(REQ == 'CP')
		{
			// ee()->cp->add_to_foot("<script src='".URL_THIRD_THEMES."smart_members_field/js/settings.js'></script>");	
			ee()->cp->add_to_foot("<script src='".URL_THIRD_THEMES."smart_members_field/js/script.js'></script>");	
		}

		/*ee()->cp->add_js_script(array(
		  'file' => array('cp/form_group'),
		));*/

	}

	/**
	 * Get all Members Group in the settings for the field in CP
	 *
	 * @return array retunrs all the member groups
	 */
	function getAllMemberGroups()
	{

		return $finalMembersGroup = ee('Model')->get('MemberGroup')
			->fields('group_id', 'group_title')
			->filter('site_id', ee()->config->item('site_id'))
			->order('group_id', 'asc')
			->all()
			->getDictionary('group_id', 'group_title');

	}

	/**
	 * Displays the field in the CP 
	 *
	 * @param array $data contains the data of field in CP
	 * @param string $settings contains settings data for the field
	 * @param string $field_name contains Field Name
	 * @param string $content_id contains the ID of content in CP
	 * @param string $field_id contains the ID of tje field
	 * @return array renders field in CP
	 */
	function displayField($data = array(), $settings = '', $field_name = '', $content_id = '', $field_id = '', $content_type)
	{		

		ee()->lang->loadfile('fieldtypes');

		/* added js to render plugins of jQuery in CP */
		ee()->cp->add_js_script(array(
			'plugin' => array('ui.touch.punch', 'ee_interact.event'),
			'file' => ['fields/relationship/mutable_relationship', 'fields/relationship/relationship', 'fields/relationship/settings'],
			'ui' => 'sortable'
		));
		
		ee()->load->library('template', NULL, 'TMPL');
		$order 		= array();
		$selected 	= array();

		$multiple = $settings['allow_multiple'];
		
		$memberGroups 	= explode('|', $settings['default_member_groups']);
		$limit       	= $settings['limit'];
		$order_field 	= $settings['order_field'];
		$order_dir   	= $settings['order_dir'];

		/* get the Custom Members Field Object */
		$customMemberFieldsOBJ = ee('Model')->get('MemberField')->fields('m_field_name', 'm_field_id');
		$customMemberFields = array();
		foreach ($customMemberFieldsOBJ->all() as $key => $value)
		{
			$customMemberFields[$value->m_field_name] = $value->m_field_id;
		}
		unset($customMemberFieldsOBJ);

		/* get the Members Field Object */
		$choices = array();
		$member = ee('Model')->get('Member');
		
		if(isset($memberGroups) && is_array($memberGroups) && count($memberGroups) > 0)
		{
			$member->filter('group_id', "IN" , $memberGroups);
		}

		$tagdata = $settings['rendered_name_format'];
		$formatArray = $this->removeCurlyBraces($tagdata, "{", "}");

		/* creates the array to render the field in CP */
		$i = 0;
		$choicesData = array();
		foreach ($member->all() as $value)
		{
			$choices[$i]['member_id'] = $value->member_id;
			for ($j=0; $j < count($formatArray); $j++)
			{
				if(isset($customMemberFields[$formatArray[$j]]))
				{
					$temp =  "m_field_id_" . $customMemberFields[$formatArray[$j]];
				}
				else
				{
					$temp = $formatArray[$j];
				}
				$choices[$i][$formatArray[$j]] = $value->$temp;
			}

			$temp = $tagdata;
			$choicesData[] = array(
				'value' => $choices[$i]['member_id'],
				'label' => ee()->TMPL->parse_variables_row($temp, $choices[$i])
			);
			$i++;
		}
		unset($member);
		unset($customMemberFields);

		if(!is_array($data) && !empty($data))
		{
			$data = explode('|', $data);
		}

		/* creates the array to display the selected data in edit page of CP */
		if (is_array($data) && !empty($data))
		{
			foreach ($data as $dk => $dv)
			{
				for ($c=0; $c < count($choicesData); $c++)
				{ 
					if ($dv == $choicesData[$c]['value'])
					{
						$selected[] = [
							'value' => $dv,
							'label' => $choicesData[$c]['label']
						];				
					}
				}
			}
		}

		if ($multiple)
		{
			$select_filters[] = [
				'name' 	=> '_selected',
				'title' => lang('show'),
				'items' => [
					'related' 	=> lang('selected_only'),
					'unrelated' => lang('unselected_only')
				]
			];
		}

		if($multiple == 'n')
		{
			$tempSelected = array();
			$i = 1;
			foreach ($selected as $sk => $sv)
			{
				if($i == 1)
				{
					$tempSelected[] = $sv;
				}
				$i++;
			}
		}

		$ajaxSettings = array();

		$entry_id = ($content_id) ?: ee()->input->get('entry_id');

		/* settings for the rendered data in CP */
		$ajaxSettings['limit']					= $settings['limit'];
		$ajaxSettings['order_field']			= $settings['order_field'];
		$ajaxSettings['order_dir']				= $settings['order_dir'];
		$ajaxSettings['entry_id']				= $entry_id;
		$ajaxSettings['member_groups']			= $memberGroups;
		$ajaxSettings['field_id']				= $field_id;
		$ajaxSettings['selected']				= $selected;
		$ajaxSettings['rendered_name_format']	= $settings['rendered_name_format'];

		/*These settings will be sent to the AJAX endpoint for filtering the field, encrypt them to prevent monkey business*/
		$ajaxSettings = json_encode($ajaxSettings);
		$ajaxSettings = ee('Encrypt')->encode($ajaxSettings, ee()->config->item('session_crypt_key'));
		$field_name = $field_name. "[data]";
	
		return ee('View')->make('smart_members_field:publish')->render([
			'field_name' 		=> $field_name,
			'choices' 			=> $choicesData,
			'selected' 			=> ($multiple == 'y') ? $selected : $tempSelected,
			'multi' 			=> ($multiple == 'y') ? $multiple : FALSE,
			'filter_url' 		=> ee('CP/URL')->make('addons/settings/smart_members_field/ajaxFilter', ['settings' => $ajaxSettings])->compile(),
			'limit' 			=> $settings['limit'] ?: 100,
			'no_results' 		=> ['text' => lang('no_members_found')],
			'no_related' 		=> ['text' => lang('no_members_selected')],
			'select_filters' 	=> $select_filters,
		]);

	}

	/**
	 * Used to filter the members choices in a relationship field
	 *
	 * @return array retunrs filtered members
	 */
	function ajaxFilter()
	{		
	
		$settings = ee('Encrypt')->decode(
			ee('Request')->get('settings'),
			ee()->config->item('session_crypt_key')
		);
		$settings = json_decode($settings, TRUE);
		
		if (empty($settings))
		{
			show_error(lang('unauthorized_access'), 403);
		}

		$settings['search'] = ee('Request')->get('search');
		$settings['_selected'] = ee('Request')->get('_selected');
		$settings['selected'] = ee('Request')->get('selected');

		$related = ($settings['_selected'] != "") ? $settings['_selected'] : NULL;

		if ( ! AJAX_REQUEST OR ! ee()->session->userdata('member_id'))
		{
			show_error(lang('unauthorized_access'), 403);
		}
		
		/* get the all members */
		$choices = array();
		$member = ee('Model')->get('Member');

		if(isset($settings['member_groups']) && is_array($settings['member_groups']) && count($settings['member_groups']) > 0)
		{
			$member->filter('group_id', "IN" , $settings['member_groups']);
		}

		if ($related == 'related')
		{
			$member->filter('member_id', 'IN', $settings['selected']);
		}
		elseif ($related == 'unrelated')
		{
			$member->filter('member_id', 'NOT IN', $settings['selected']);
		}

		/* get the Custom Members Field Object */
		$customMemberFieldsOBJ = ee('Model')->get('MemberField')->fields('m_field_name', 'm_field_id');
		$customMemberFields = array();
		foreach ($customMemberFieldsOBJ->all() as $key => $value)
		{
			$customMemberFields[$value->m_field_name] = $value->m_field_id;
		}
		unset($customMemberFieldsOBJ);

		$tagdata = $settings['rendered_name_format'];
		$formatArray = $this->removeCurlyBraces($tagdata, "{", "}");
		ee()->load->library('template', NULL, 'TMPL');

		/* creates the array to render the field in CP */
		$i = 0;
		$response = array();
		foreach ($member->all() as $value)
		{
			$choices[$i]['member_id'] = $value->member_id;
			for ($j=0; $j < count($formatArray); $j++)
			{
				if(isset($customMemberFields[$formatArray[$j]]))
				{
					$temp =  "m_field_id_" . $customMemberFields[$formatArray[$j]];
				}
				else
				{
					$temp = $formatArray[$j];
				}
				$choices[$i][$formatArray[$j]] = $value->$temp;
			}

			$temp = $tagdata;
			$response[] = array(
				'value' => $choices[$i]['member_id'],
				'label' => ee()->TMPL->parse_variables_row($temp, $choices[$i])
			);
			$i++;
		}
		
		return $response;

	}

	/**
	 * Show the tag on the frontend
	 *
	 * @param	column data
	 * @param	tag parameters
	 * @param	tag pair contents
	 * @return	parsed output
	 */
	function replaceTag($data, $params = '', $tagdata = '', $settings = array(), $id = '')
	{

		$this->smfSettings['data'] = $data;
		$this->smfSettings['params'] = $params;
		$this->smfSettings['tagdata'] = $tagdata;
		$this->smfSettings['settings'] = $settings;
		$this->smfSettings['id'] = $id;

		if(isset($this->smfSettings['params']['prefix']) && $this->smfSettings['params']['prefix'] != "")
		{
			$this->smfSettings['prefix'] = $this->smfSettings['params']['prefix'];
		}
		elseif(isset($this->smfSettings['settings']['grid_field_id']) && $this->smfSettings['settings']['grid_field_id'] != "")
		{

			$temp = ee()->db->where('row_id', $this->smfSettings['settings']['grid_row_id'])->get('channel_grid_field_' . $this->smfSettings['settings']['grid_field_id'])->row('fluid_field_data_id');
			if($temp != "" && $temp != 0)
			{
				$pre = "content";
			}
			else
			{
				$pre = ee('Model')->get('ChannelField', $this->smfSettings['settings']['grid_field_id'])->fields('field_name')->first()->field_name;
			}
			
			$this->smfSettings['prefix'] = $pre . ":" . $this->smfSettings['settings']['col_name'];

		}
		else
		{
			if(isset($this->smfSettings['settings']['fluid_field_data_id']) && $this->smfSettings['settings']['fluid_field_data_id'] != "" && $this->smfSettings['settings']['fluid_field_data_id'] != 0)
			{
				$this->smfSettings['prefix'] = $this->smfSettings['settings']['field_name'];
			}
			else
			{
				$this->smfSettings['prefix'] = ee('Model')->get('ChannelField', $this->smfSettings['id'])->fields('field_name')->first()->field_name;
			}
		}

		if($this->smfSettings['data'] == "")
		{
			return $this->setNoResults();
		}

		/* collection of default member fields */
		$this->smfSettings['membersSavedData'] = array();
		$this->smfSettings['defaultMemberFields'] = array('member_id', 'username', 'email', 'screen_name', 'group_id', 'avatar_filename', 'avatar_width', 'avatar_height', 'photo_filename', 'photo_width', 'photo_height', 'sig_img_filename', 'sig_img_width', 'sig_img_height', 'join_date', 'last_visit', 'total_entries', 'total_comments', 'total_forum_topics', 'total_forum_posts', 'last_entry_date', 'last_comment_date', 'last_forum_post_date', 'last_email_date', 'in_authorlist');

		if(isset($this->smfSettings['data']) && $this->smfSettings['data'] != "")
		{
			$this->smfSettings['membersSavedData'] = explode('|', $this->smfSettings['data']);
		}

		if(isset($this->smfSettings['settings']['default_member_groups']))
		{
			$this->smfSettings['defaultMemberGroups'] = explode('|', $this->smfSettings['settings']['default_member_groups']);
		}

		/* filteration of member object */
		if( !
			(isset($this->smfSettings['defaultMemberGroups']) && 
			is_array($this->smfSettings['defaultMemberGroups']) && 
			count($this->smfSettings['defaultMemberGroups']) > 0 && 
			isset($this->smfSettings['membersSavedData']) && 
			is_array($this->smfSettings['membersSavedData']) && 
			count($this->smfSettings['membersSavedData']) > 0))
		{
			return $this->setNoResults();
		}

		/* fetch all members and custom members fields */
		$memberOBJ = ee('Model')->get('Member');

		$this->smfSettings['customMemberFields'] = ee('Model')->get('MemberField')
		->fields('m_field_id', 'm_field_name')->all()
		->getDictionary('m_field_id', 'm_field_name');

		if( ! (isset($this->smfSettings['customMemberFields']) && is_array($this->smfSettings['customMemberFields']) && count($this->smfSettings['customMemberFields']) > 0) )
		{
			$this->smfSettings['customMemberFields'] = array();
		}

		if(isset($this->smfSettings['params']['group_id']))
		{
			if(strpos($this->smfSettings['params']['group_id'], 'not ') !== false)
			{
				$this->smfSettings['params']['exclude_group_id'] = trim(str_replace("not ", "", $this->smfSettings['params']['group_id']));
			}
			else
			{
				$this->smfSettings['params']['include_group_id'] = trim($this->smfSettings['params']['group_id']);
			}
		}

		if(isset($this->smfSettings['params']['exclude_group_id']))
		{

			$this->smfSettings['params']['exclude_group_id'] = explode("|", $this->smfSettings['params']['exclude_group_id']);
			
			$removeStacks = array_diff($this->smfSettings['params']['exclude_group_id'], $this->smfSettings['defaultMemberGroups']);
			if(count($removeStacks) > 0)
			{
				foreach ($removeStacks as $removeKey => $removeValue)
				{
					unset($this->smfSettings['params']['exclude_group_id'][$removeKey]);
				}
			}
			
			if(is_array($this->smfSettings['params']['exclude_group_id']) && count($this->smfSettings['params']['exclude_group_id']) > 0)
			{
				$memberOBJ->filter('group_id', "NOT IN", $this->smfSettings['params']['exclude_group_id']);
			}

		}

		if(isset($this->smfSettings['params']['include_group_id']))
		{

			$this->smfSettings['params']['include_group_id'] = explode("|", $this->smfSettings['params']['include_group_id']);
			$removeStacks = array_diff($this->smfSettings['params']['include_group_id'], $this->smfSettings['defaultMemberGroups']);
			if(count($removeStacks) > 0)
			{
				foreach ($removeStacks as $removeKey => $removeValue)
				{
					unset($this->smfSettings['params']['include_group_id'][$removeKey]);
				}
			}
			
			if(is_array($this->smfSettings['params']['include_group_id']) && count($this->smfSettings['params']['include_group_id']) > 0)
			{
				$memberOBJ->filter('group_id', "IN", $this->smfSettings['params']['include_group_id']);
			}
		}

		if(isset($this->smfSettings['params']['member_id']))
		{
			if(strpos($this->smfSettings['params']['member_id'], 'not ') !== false)
			{
				$this->smfSettings['params']['exclude_member_id'] = trim(str_replace("not ", "", $this->smfSettings['params']['member_id']));
			}
			else
			{
				$this->smfSettings['params']['include_member_id'] = trim($this->smfSettings['params']['member_id']);
			}
		}

		if(isset($this->smfSettings['params']['exclude_member_id']))
		{

			$this->smfSettings['params']['exclude_member_id'] = explode("|", $this->smfSettings['params']['exclude_member_id']);
			
			$removeStacks = array_diff($this->smfSettings['params']['exclude_member_id'], $this->smfSettings['defaultMemberGroups']);
			if(count($removeStacks) > 0)
			{
				foreach ($removeStacks as $removeKey => $removeValue)
				{
					unset($this->smfSettings['params']['exclude_member_id'][$removeKey]);
				}
			}

			if(is_array($this->smfSettings['params']['exclude_member_id']) && count($this->smfSettings['params']['exclude_member_id']) > 0)
			{
				$memberOBJ->filter('member_id', "NOT IN", $this->smfSettings['params']['exclude_member_id']);
			}

		}

		$includeMember = false;
		if(isset($this->smfSettings['params']['include_member_id']))
		{

			$this->smfSettings['params']['include_member_id'] = explode("|", $this->smfSettings['params']['include_member_id']);
			$removeStacks = array_diff($this->smfSettings['params']['include_member_id'], $this->smfSettings['defaultMemberGroups']);
			if(count($removeStacks) > 0)
			{
				foreach ($removeStacks as $removeKey => $removeValue)
				{
					unset($this->smfSettings['params']['include_member_id'][$removeKey]);
				}
			}

			if(is_array($this->smfSettings['params']['include_member_id']) && count($this->smfSettings['params']['include_member_id']) > 0)
			{
				$includeMember = true;
				$memberOBJ->filter('member_id', "IN", $this->smfSettings['params']['include_member_id']);
			}
			
		}
		
		if($this->smfSettings['tagdata'] == "")
		{
			if($includeMember === true)
			{
				return implode('|', $this->smfSettings['params']['include_member_id']);
			}
			else
			{
				return implode('|', $this->smfSettings['membersSavedData']);
			}
		}

		if(isset($this->smfSettings['customMemberFields']) && is_array($this->smfSettings['customMemberFields']) && count($this->smfSettings['customMemberFields']) > 0)
		{
			$this->smfSettings['allMemberFields'] = array_merge($this->smfSettings['defaultMemberFields'], $this->smfSettings['customMemberFields']);
		}
		else
		{
			$this->smfSettings['allMemberFields'] = $this->smfSettings['defaultMemberFields'];
		}
		
		$this->smfSettings['order_by'] 	= (isset($this->smfSettings['params']['order_by']) && in_array($this->smfSettings['params']['order_by'], $this->smfSettings['allMemberFields'])) ? $this->smfSettings['params']['order_by'] : "member_id";
		$this->smfSettings['sort'] 		= (isset($this->smfSettings['params']['sort']) && in_array(strtolower($this->smfSettings['params']['sort']), array('asc', 'desc'))) ? strtolower($this->smfSettings['params']['sort']) : "desc";
		$memberOBJ->order($this->smfSettings['order_by'], $this->smfSettings['sort']);
		
		if($includeMember == false)
		{
			$memberOBJ->filter('member_id', "IN", $this->smfSettings['membersSavedData']);
		}
		
		if($memberOBJ->count() == 0)
		{
			return $this->setNoResults();
		}

		$count = 0;

		/* creating the array to parse with all member_fields */
		foreach ($memberOBJ->all() as $k)
		{

			/* for default member fields */
			$this->smfSettings['replaceData'][$count][$this->smfSettings['prefix'] . ':count'] 			= ($count + 1);
			$this->smfSettings['replaceData'][$count][$this->smfSettings['prefix'] . ':total_results']  = ($memberOBJ->count());
			$this->smfSettings['replaceData'][$count][$this->smfSettings['prefix'] . ':no_results'] 	= false;
			foreach ($this->smfSettings['defaultMemberFields'] as $dk => $dv)
			{
				$this->smfSettings['replaceData'][$count][$this->smfSettings['prefix'] .':' . $dv] 			= $k->$dv;
			}

			/* for custom member fields */
			if(isset($this->smfSettings['params']['custom_fields']) && $this->smfSettings['params']['custom_fields'] == "yes")
			{
				foreach ($this->smfSettings['customMemberFields'] as $key => $value)
				{
					$tag = "m_field_id_".$key;
					$this->smfSettings['replaceData'][$count][$this->smfSettings['prefix'] . ':' . $value] = $k->$tag;
				}
			}

			$count++;
		}
		
		$return = ee()->TMPL->parse_variables($this->smfSettings['tagdata'], $this->smfSettings['replaceData']);
		unset($memberOBJ);
		unset($this->smfSettings);
		return $return;

	}

	function setNoResults()
	{
		$replacement = '';
		if (strpos($this->smfSettings['tagdata'], 'if ' . $this->smfSettings['prefix'] . ':no_results') !== FALSE
			&& preg_match('/'.LD.'if ' . $this->smfSettings['prefix'] . ':no_results'.RD.'(.*?)'.LD.'\/if'.RD.'/s', $this->smfSettings['tagdata'], $match)) {
			$replacement = $match[1];
		}
	
		return $replacement;
	}

	/**
	 * Displays the settings for the field in CP
	 *
	 * @param array $data Stored data for the field
	 * @param string $content_type contains the Content type of the field
	 * @return array retunrs Field Settings
	 */
	function displaySettings($data = array(), $content_type = '')
	{

		$member_groups = $this->getAllMemberGroups();

		/* To Display Main Settings */
		if(isset($data['default_member_groups']) && is_array($data['default_member_groups']))
		{
			$data['default_member_groups'] = implode('|', $data['default_member_groups']);
		}

		/* to create the fields in Settings page of the Field */
		$settings = array(
			array(
				'title' 	=> 'members_group',
				'desc' 		=> 'member_group_desc',
				'fields' 	=> array(
					'default_member_groups[]' => array(
						'type' 		=> 'select',
						'attrs'		=> 'multiple="multiple" size="5"',
						'choices' 	=> $member_groups,
						'value' 	=> (isset($data['default_member_groups']) ? explode('|', $data['default_member_groups']) : '5'),
						'required' 	=> TRUE,
					)
				)
			),
			array(
				'title' 	=> 'display_name_format',
				'desc' 		=> 'display_name_format_desc',
				'fields' 	=> array(
					'rendered_name_format' => array(
						'type' 		=> 'text',
						'attrs' 	=> 'class="members_data_format"',
						'value' 	=> (isset($data['rendered_name_format']) ? $data['rendered_name_format'] : '{screen_name}'),
						'required' 	=> TRUE,
					)
				)
			),
			array(
				'title' => 'member_limit',
				'desc' 	=> 'member_limit_desc',
				'fields' => array(
					'limit' => array(
						'type' 	=> 'text',
						'value' => (isset($data['limit']) ? $data['limit'] : '100'),
					)
				)
			),
			array(
				'title' => 'order',
				'desc' 	=> 'order_desc',
				'fields' => array(
					'order_field' => array(
						'type' => 'radio',
						'choices' => array(
							'screen_name' 	=> lang('screen_name'),
							'user_name' 	=> lang('user_name'),
							'email' 	 	=> lang('email'),
							'member_id' 	=> lang('member_id'),
						),
						'value' => (isset($data['order_field']) ? $data['order_field'] : 'member_id')
					),
					'order_dir' => array(
						'type' => 'radio',
						'choices' => array(
							'asc' 	=> lang('ft_order_ascending'),
							'desc'	=> lang('ft_order_descending'),
						),
						'value' => (isset($data['order_dir']) ? $data['order_dir'] : 'asc')
					)
				)
			),
			array(
				'title' => 'allow_multi',
				'desc' 	=> 'allow_multi_desc',
				'fields' => array(
					'allow_multiple' => array(
						'type' 	=> 'yes_no',
						'value' => (isset($data['allow_multiple']) ? $data['allow_multiple'] : 'n')
					)
				)
			)
		);

		if ($content_type == 'grid')
		{
			return array('field_options' => $settings);
		}		

		return array('field_options_smart_members_field' => array(
			'label' 	=> 'field_options',
			'group' 	=> 'smart_members_field',
			'settings' 	=> $settings
		));

	}

	/**
	 * Saves the settings for the field in CP
	 *
	 * @param array $data containes all settings of field
	 * @param string $content_type containes content type of the field
	 * @return array saves the all information of field
	 */
	function saveSettings($data = array(), $content_type = '')
	{

		$defaults = array(
			'default_member_groups' => '',
			'rendered_name_format' 	=> '',
			'limit'					=> '',
			'order_field'			=> '',
			'order_dir'				=> '',
			'allow_multiple'		=> '',
		);

		if(is_array($data['default_member_groups']) && !empty($data['default_member_groups']) && count($data['default_member_groups']) > 0)
		{
			$data['default_member_groups'] = implode('|', $data['default_member_groups']);
		}

		$all = array_merge($defaults, $data);

		return array_intersect_key($all, $defaults);
	}

	/**
	 * Validate the field data in the CP
	 *
	 * @param array $data contains field data to validate
	 * @return array retunrs the validated data or validation errors of field
	 */
	function validateSettings($data = array())
	{

		$displayNames 	= $this->removeCurlyBraces($data['rendered_name_format'], '{', '}');
		$rules = array(
			'default_member_groups' => 'required|xss',
			'rendered_name_format' 	=> 'required|xss|validDisplayName['.implode(",", $displayNames).']',
			'order_field'			=> 'required',
			'order_dir'				=> 'required',
		);

		/* check for the validation with defined rules */
		ee()->smfCustomValidation->validator->setRules($rules);
        $result = ee()->smfCustomValidation->validator->validate($data);

		if ($result->isValid())
		{
		  	return TRUE;
		}
		else
		{
			return $result;
		}

	}

	/**
	 * removes the Curly Brackets from the string
	 *
	 * @param array $str array of Display Names having Curly Brackets
	 * @param string $startDelimiter to remove from starting of the string
	 * @param string $endDelimiter to remove from end of the string
	 * @return array $contents retunrs the array of plain Display Names
	 */
	function removeCurlyBraces($str, $startDelimiter, $endDelimiter)
	{
		$contents = array();
		$startDelimiterLength = strlen($startDelimiter);
		$endDelimiterLength = strlen($endDelimiter);
		$startFrom = $contentStart = $contentEnd = 0;

		while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) 
		{
			$contentStart += $startDelimiterLength;
			$contentEnd = strpos($str, $endDelimiter, $contentStart);
			if (false === $contentEnd) 
			{
				break;
			}
			$contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
			$startFrom = $contentEnd + $endDelimiterLength;
		}

		return $contents;
	}

}