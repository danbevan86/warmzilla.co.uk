<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property CI_Controller   $EE
 * @property Cartthrob_cart  $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_price_by_member_group_ft extends Cartthrob_matrix_ft
{
    public $info = [
        'name' => 'CartThrob Price - By Member Group',
        'version' => CARTTHROB_VERSION,
    ];

    public $default_row = [
        'member_group' => '',
        'price' => '',
    ];

    public function __construct()
    {
        parent::__construct();

        unset($this->buttons['add_column']);
    }

    public function pre_process($data)
    {
        $data = parent::pre_process($data);

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->library('cartthrob_loader');

        ee()->load->library('number');

        foreach ($data as &$row) {
            if (isset($row['price']) && $row['price'] !== '') {
                $row['price_plus_tax'] = $row['price'];

                if ($plugin = ee()->cartthrob->store->plugin(ee()->cartthrob->store->config('tax_plugin'))) {
                    $row['price_plus_tax'] = $plugin->get_tax($row['price']) + $row['price'];
                }

                $row['price_numeric'] = $row['price'];
                $row['price_plus_tax_numeric'] = $row['price:plus_tax_numeric'] = $row['price_numeric:plus_tax'] = $row['price_plus_tax'];

                $row['price'] = ee()->number->format($row['price']);
                $row['price_plus_tax'] = $row['price:plus_tax'] = ee()->number->format($row['price_plus_tax']);
            }
        }
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return $data;
    }

    public function display_field_member_group($name, $value, $row, $index, $blank = false)
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        static $member_groups;

        if (is_null($member_groups)) {
            $member_groups[''] = lang('cartthrob_price_by_member_group_global');

            // ee()->load->model('member_model');

            // $query = ee()->member_model->get_member_groups(array(), array(array('group_id !=' => 2), array('group_id !=' => 3), array('group_id !=' => 4)));
            $query = ee('Model')->get('MemberGroup')->filter('group_id', '!=', '2')->orFilter('group_id', '!=',
                '3')->orFilter('group_id', '!=', '4')->all();
            foreach ($query as $row) {
                $member_groups[$row->group_id] = $row->group_title;
            }
        }
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return form_dropdown($name, $member_groups, $value);
    }

    public function replace_tag($data, $params = [], $tagdata = false)
    {
        if (!$tagdata) {
            return $this->replace_price($data, $params, $tagdata);
        }

        return parent::replace_tag($data, $params, $tagdata);
    }

    public function replace_price($data, $params = [], $tagdata = false)
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('number');
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return ee()->number->format($this->cartthrob_price($data));
    }

    public function cartthrob_price($data, $item = null)
    {
        if (!is_array($data)) {
            $serialized = $data;

            if (!isset(ee()->session->cache['cartthrob']['price_by_member_group']['cartthrob_price'][$serialized])) {
                ee()->session->cache['cartthrob']['price_by_member_group']['cartthrob_price'][$serialized] = _unserialize($data,
                    true);
            }

            $data = ee()->session->cache['cartthrob']['price_by_member_group']['cartthrob_price'][$serialized];
        }

        $price = null;

        $default_price = null;

        // loop through the rows and grab the price for current user's member group
        // or grab the default global price if no price is explicitly set for this member group
        foreach ($data as $row) {
            if (is_null($price) && !empty($row['member_group']) && ee()->session->userdata('group_id') == $row['member_group']) {
                $price = $row['price'];
            }

            if (is_null($default_price) && !$row['member_group']) {
                $default_price = $row['price'];
            }
        }

        if (is_null($price) && !is_null($default_price)) {
            $price = $default_price;
        }

        return (is_null($price)) ? 0 : $price;
    }

    public function replace_plus_tax($data, $params = [], $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('number');

        ee()->load->library('cartthrob_loader');
        $data = ee()->cartthrob->sanitize_number($this->cartthrob_price($data));
        if ($plugin = ee()->cartthrob->store->plugin(ee()->cartthrob->store->config('tax_plugin'))) {
            $data = $data + $plugin->get_tax($data);
        }

        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return ee()->number->format($data);
    }

    public function replace_plus_tax_numeric($data, $params = '', $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('number');

        ee()->load->library('cartthrob_loader');
        $data = ee()->cartthrob->sanitize_number($this->cartthrob_price($data));

        if ($plugin = ee()->cartthrob->store->plugin(ee()->cartthrob->store->config('tax_plugin'))) {
            $data = $data + $plugin->get_tax($data);
        }
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return $data;
    }

    public function replace_numeric($data, $params = '', $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        ee()->load->library('number');
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return ee()->cartthrob->sanitize_number($this->cartthrob_price($data));
    }
}
