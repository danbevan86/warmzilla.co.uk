<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * CI Wrapper for CartThrob
 *
 * Loads the settings, the session and then the cart
 *
 * @property Cartthrob_settings_model $cartthrob_settings_model
 * @property Cartthrob_session        $cartthrob_session
 * @property Cart_model               $cart_model
 * @property Customer_model           $customer_model
 */
class Cartthrob_loader
{
    private $setup = [];

    public function __construct($params = [])
    {
        // need to check and see if CartThrob is actually installed first.
        ee()->load->library('addons');
        $modules = ee()->addons->get_installed();

        if (!isset($modules['cartthrob']['module_version'])) {
            return;
        }

        if (!ee()->has('cartthrob')) {
            // if you don't provide a cart in the construct params (like an empty cart array),
            // initialize the session to get the cart
            if (!isset($params['cart'])) {
                // load the settings into CI
                ee()->load->model('cartthrob_settings_model');

                // load the session
                ee()->load->library('cartthrob_session');

                // get the cart id from the session
                $cart_id = ee()->cartthrob_session->cart_id();

                ee()->load->model('cart_model');

                // get the cart data from the db
                $params['cart'] = ee()->cart_model->read_cart($cart_id);

                ee()->load->model('customer_model');

                $existing_customer_info = (isset($params['cart']['customer_info'])) ? $params['cart']['customer_info'] : null;

                $params['cart']['customer_info'] = ee()->customer_model->get_customer_info($existing_customer_info);
            }

            // normally we'd want to instantiate with a config array,
            // but the Cartthrob_core_ee driver overrides the use of the config array and uses the cartthrob_settings_model's config cache
            ee()->set('cartthrob', Cartthrob_core::instance('ee', [
                'cart' => $params['cart'],
            ]));
        }
    }

    // @TODO deprecate
    public function setup(&$object)
    {
        if (!is_object($object)) {
            return;
        }

        if (!in_array($object, $this->setup)) {
            $this->setup[] = $object;
        }

        $object->cartthrob = ee()->cartthrob;
        $object->cart = ee()->cartthrob->cart;
        $object->store = ee()->cartthrob->store;
    }

    // @TODO deprecate
    public function setup_all($which = [])
    {
        if (!is_array($which)) {
            $which = func_get_args();
        }

        foreach ($this->setup as &$object) {
            if (!$which || in_array('core', $which)) {
                $object->cartthrob = ee()->cartthrob;
            }

            if (!$which || in_array('cart', $which)) {
                $object->cart = ee()->cartthrob->cart;
            }

            if (!$which || in_array('store', $which)) {
                $object->store = ee()->cartthrob->store;
            }
        }
    }
}
