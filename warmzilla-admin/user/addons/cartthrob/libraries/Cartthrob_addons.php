<?php

use Illuminate\Support\Arr;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * CartThrob Addons
 */
class Cartthrob_addons
{
    /**
     * @var array container for addon module/plugin instances; class => object
     */
    private $addons = [];

    /**
     * @var array the list of available methods; method => class
     */
    private $methods = [];

    /**
     * @var object the last module/plugin instance checked in method_exists
     */
    private $cached_addon;

    public function __construct()
    {
        $valid_addons = [];

        if (ee()->extensions->active_hook('cartthrob_addon_register')) {
            $valid_addons = ee()->extensions->call('cartthrob_addon_register', $valid_addons);
        }
        foreach ($valid_addons as $short_name) {
            if (strpos($short_name, 'cartthrob_') !== false) {
                $short_name = str_replace('cartthrob_', '', $short_name);
            }
            $paths = [
                PATH_THIRD . 'cartthrob_' . $short_name . '/mod.cartthrob_' . $short_name . '.php',
                PATH_THIRD . 'cartthrob_' . $short_name . '/pi.cartthrob_' . $short_name . '.php',
            ];

            foreach ($paths as $path) {
                if (@file_exists($path)) {
                    require $path;

                    $this->register('Cartthrob_' . $short_name);

                    break;
                }
            }
        }
    }

    /**
     * register a module/plugin so that you can use it's tags with cartthrob
     *
     * @param string|object $class the classname of the module/plugin
     */
    public function register($class)
    {
        if (is_object($class)) {
            $object = $class;

            $class = get_class($object);
        } else {
            $object = new $class();
        }

        $this->addons[$class] = $object;

        foreach (get_class_methods($class) as $method) {
            // "private" or magic method, skip
            if (strncmp($method, '_', 1) === 0) {
                continue;
            }

            $this->methods[$method] = $class;
        }
    }

    /**
     * call a cartthrob addon's method from the cartthrob module
     * pass args via TMPL class
     *
     * @param string $method name of the template tag method
     *
     * @return mixed
     */
    public function call($method)
    {
        if (is_null($this->cached_addon)) {
            if (!$this->method_exists($method)) {
                return;
            }
        }

        $result = $this->cached_addon->$method();

        $this->cached_addon = null;

        return $result;
    }

    public function method_exists($method)
    {
        if (!$class = $this->get_class_from_method($method)) {
            return false;
        }

        if (!$addon = &$this->get_addon_from_class($class)) {
            return false;
        }

        if (!method_exists($addon, $method)) {
            return false;
        }

        $this->cached_addon = &$addon;

        return true;
    }

    private function get_class_from_method($method)
    {
        return Arr::get($this->methods, $method);
    }

    private function get_addon_from_class($class)
    {
        return Arr::get($this->addons, $class);
    }
}
