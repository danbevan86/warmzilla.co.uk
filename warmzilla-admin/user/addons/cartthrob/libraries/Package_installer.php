<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!class_exists(basename(__FILE__, '.php'))) :

    class Package_installer
    {
        private $packages = [];
        private $package_types = [
            'channel',
            'template_group',
        ];
        private $subtypes = [
            'channel' => ['field_group', 'categories', 'channel_data'],
            'template_group' => ['template'],
            'field_group' => ['field'],
            'categories' => ['category'],
            'channel_data' => ['channel_data'],
        ];
        private $errors = [];
        private $installed = [];
        private $template_path;
        private $field_order = 1;

        public function __construct($params = [])
        {
            if (!empty($params['xml'])) {
                $this->load_xml($params['xml']);
            }
        }

        public function clear_packages()
        {
            $this->packages = [];
        }

        public function remove_package($row_id)
        {
            unset($this->packages[$row_id]);
        }

        public function load_xml($xml = false)
        {
            $this->add_package($this->parse_xml($xml));
        }

        public function add_package($package)
        {
            if (!$package) {
                return;
            }

            if (is_array($package)) {
                $this->packages = array_merge($this->packages, $package);
            } else {
                $this->package[] = $package;
            }
        }

        public function packages()
        {
            return $this->packages;
        }

        // --------------------------------
        //  Clean Attributes
        // --------------------------------

        /**
         * Clean up XML attributes before parsing
         *
         * @param obj $xml XML object
         */
        private function clean_data($data)
        {
            $data = str_replace(
                [
                    '\n',
                    '\r',
                ],
                [
                    "\n",
                    "\r",
                ],
                $data
            );

            return $data;
        }

        // --------------------------------
        //  Clean Fields
        // --------------------------------

        /**
         * Remove data from array if the key is not a field in the specified table
         *
         * @param string $table Database table name
         * @param array $data data to be cleaned
         */
        private function clean_fields($table, $data)
        {
            $fields = ee()->db->list_fields($table);

            foreach ($data as $key => $value) {
                if (!in_array($key, $fields)) {
                    unset($data[$key]);
                }
            }

            return $data;
        }

        // --------------------------------
        //  Create Category
        // --------------------------------

        /**
         * Create a category from an XML object
         *
         * @param obj $category XML object
         * @param int $group_id category group id
         */
        private function create_category($category, $group_id)
        {
            if (count($category) > 1) {
                foreach ($category as $cat) {
                    $this->create_category($cat, $group_id);
                }

                return;
            }

            $category_data = $this->get_attributes($category);

            $category_data['site_id'] = ee()->config->item('site_id');

            $category_data['group_id'] = $group_id;

            $original_name = $category_data['category_name'];

            $category_data['category_name'] = $this->rename('categories', $original_name, 'category_name',
                ['group_id' => $group_id]);

            if ($category_data['category_name'] === false) {
                return $this->log_error('category_exists', $original_name);
            }

            $this->insert('categories', $category_data);

            $this->log_install('category', $category_data['category_name']);
        }

        // --------------------------------
        //  Create Category Group
        // --------------------------------

        /**
         * Create a category group from an XML object
         *
         * @param obj $category_group XML object
         * @return int $group_id
         */
        private function create_cat_group($cat_group)
        {
            if (count($cat_group) > 1) {
                foreach ($cat_group as $group) {
                    $this->create_cat_group($group);
                }

                return;
            }

            $cat_group_data = $this->get_attributes($cat_group);

            $cat_group_data['site_id'] = ee()->config->item('site_id');

            $group_id = $this->exists('category_groups', ['group_name' => $cat_group_data['group_name']],
                'group_id');

            if (!$group_id) {
                $group_id = $this->insert('category_groups', $cat_group_data);

                $this->log_install('category_group', $cat_group_data['group_name']);
            }

            if (isset($cat_group->category)) {
                foreach ($cat_group->category as $category) {
                    $this->create_category($category, $group_id);
                }
            }

            return $group_id;
        }

        // --------------------------------
        //  Create Field
        // --------------------------------

        /**
         * Create a custom field from an XML object
         *
         * @param stdClass $field XML object
         * @param int $group_id custom field group id
         * @return string
         */
        private function create_field($field, $group_id)
        {
            ee()->load->dbforge();

            $field_data = $this->get_attributes($field);

            $original_name = $field_data['field_name'];

            $field_data['site_id'] = ee()->config->item('site_id');

            $field_data['field_name'] = $this->rename('channel_fields', $original_name, 'field_name');

            if ($field_data['field_name'] === false) {
                return $this->log_error('field_exists', $original_name);
            }

            $field_group_fields['group_id'] = $group_id;

            $field_data['field_order'] = $this->field_order++;

            $field_id = $this->insert('channel_fields', $field_data);
            $field_group_fields['field_id'] = $field_id;
            $this->insert('channel_field_groups_fields', $field_group_fields);

            if ($field_data['field_type'] == 'date') {
                $fields2 = [
                    'id' => ['type' => 'int', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true],
                    'entry_id' => ['type' => 'int', 'constraint' => '10', 'null' => false],
                    'field_id_' . $field_id => [
                        'type' => 'int',
                        'constraint' => '10',
                        'null' => true,
                        'default' => '0',
                    ],
                    'field_dt_' . $field_id => ['type' => 'varchar', 'constraint' => '50'],
                    'field_ft_' . $field_id => ['type' => 'tinytext', 'null' => true],
                ];
            } elseif ($field_data['field_type'] == 'rel') {
                $fields2 = [
                    'id' => ['type' => 'int', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true],
                    'entry_id' => ['type' => 'int', 'constraint' => '10', 'null' => false],
                    'field_id_' . $field_id => ['type' => 'int', 'constraint' => '10', 'null' => true],
                    'field_ft_' . $field_id => ['type' => 'tinytext', 'null' => true],
                ];
            } else {
                $fields2 = [
                    'id' => ['type' => 'int', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true],
                    'entry_id' => ['type' => 'int', 'constraint' => '10', 'null' => false],
                    'field_id_' . $field_id => ['type' => 'text', 'constraint' => '255', 'null' => true],
                    'field_ft_' . $field_id => ['type' => 'tinytext', 'null' => true],
                ];
            }

            ee()->dbforge->add_key('id', true);

            ee()->dbforge->add_field($fields2);
            ee()->dbforge->create_table('channel_data_field_' . $field_id);
            $sql = 'ALTER TABLE ' . ee()->db->dbprefix . 'channel_data_field_' . $field_id . ' ADD INDEX(`entry_id`)';
            ee()->db->query($sql);
            unset($fields2);

            /*if (@$field_data['field_type'] == 'date' || @$field_data['field_type'] == 'rel')
            {
                ee()->dbforge->add_column('channel_data', array('field_id_'.$field_id => array('type' => 'int(10)')));
            }
            else
            {
                ee()->dbforge->add_column('channel_data', array('field_id_'.$field_id => array('type' => 'text', 'null' => TRUE)));
            }

            ee()->dbforge->add_column('channel_data', array('field_ft_'.$field_id => array('type' => 'tinytext')));

            if (@$field_data['field_type'] == 'date')
            {
                ee()->dbforge->add_column('channel_data', array('field_dt_'.$field_id => array('type' => 'varchar(8)')));
            }*/

            $this->log_install('field', $field_data['field_name']);
        }

        // --------------------------------
        //  Create Field Group
        // --------------------------------

        /**
         * Create a custom field group from an XML object
         *
         * @param obj $field_group XML object
         * @return int $group_id
         */
        private function create_field_group($field_group)
        {
            $field_group_data = $this->get_attributes($field_group);

            $original_name = $field_group_data['group_name'];

            $field_group_data['site_id'] = ee()->config->item('site_id');

            if (false === ($field_group_data['group_name'] = $this->rename('field_groups', $original_name,
                    'group_name'))) {
                return $this->log_error('field_group_exists', $original_name);
            }

            $group_id = $this->insert('field_groups', $field_group_data);

            $this->log_install('field_group', $field_group_data['group_name']);

            if (isset($field_group->field)) {
                $this->field_order = 1;

                foreach ($field_group->field as $field) {
                    $this->create_field($field, $group_id);
                }
            }

            return $group_id;
        }

        private function create_group_fields($field_group, $channel_id)
        {
            $select_fields_by_group = ee()->db->select('field_id')
                ->from('channel_field_groups_fields')
                ->where('group_id', $field_group)
                ->get();
            if ($select_fields_by_group->num_rows() > 0) {
                foreach ($select_fields_by_group->result_array() as $selected_fields) {
                    $channels_channel_data['channel_id'] = $channel_id;
                    $channels_channel_data['field_id'] = $selected_fields['field_id'];
                    $this->insert('channels_channel_fields', $channels_channel_data);
                }
            }
        }

        // --------------------------------
        //  Create Member Group
        // --------------------------------

        /**
         * Create a member group from an XML object
         *
         * @param obj $template_group XML object
         * @return int $group_id
         */
        private function create_member_group($member_group)
        {
            $member_group_data = $this->get_attributes($member_group);

            $member_group_data['site_id'] = ee()->config->item('site_id');

            $group_id = $this->exists('member_groups', ['group_title' => $member_group_data['group_title']],
                'group_id');

            if (!$group_id) {
                $group_id = $this->insert('member_groups', $member_group_data);

                $this->log_install('member_group', $member_group_data['group_title']);
            } else {
                $this->log_error('member_group_exists', $member_group_data['group_title']);
            }
        }

        // --------------------------------
        //  Create Template
        // --------------------------------

        /**
         * Create a template from an XML object
         *
         * @param obj $template XML object
         * @param int $group_id custom field group id
         */
        private function create_template($template, $group_id, $group_data)
        {
            $template_data = $this->get_attributes($template);

            $template_data['site_id'] = ee()->config->item('site_id');

            $template_data['group_id'] = $group_id;

            $ext = '.html';

            if (isset($template_data['template_type'])) {
                ee()->load->library('api');

                ee()->legacy_api->instantiate('template_structure');

                $ext = ee()->api_template_structure->file_extensions($template_data['template_type']);
            }

            $template_file = $this->template_path . $group_data['group_name'] . '.group' . DIRECTORY_SEPARATOR . $template_data['template_name'] . $ext;

            if ($this->template_path && file_exists($template_file)) {
                $template_data['template_data'] = file_get_contents($template_file);
            } else {
                $template_data['template_data'] = trim((string)$template);
            }

            $template_data['edit_date'] = ee()->localize->now;

            if ($this->exists('templates',
                ['group_id' => $group_id, 'template_name' => $template_data['template_name']])) {
                return $this->log_error('template_exists', $template_data['template_name']);
            }

            if ($template_data['template_name'] === false) {
                return $this->log_error('template_exists', $original_name);
            }

            if (!isset($template_data['protect_javascript'])) {
                $template_data['protect_javascript'] = 'n';
            }

            ee()->load->model('template_model');

            $template_data = $this->clean_fields('templates', $template_data);

            $template_group_entity = new \Template_Group_Entity($group_data);

            $template_group_entity->group_id = $group_id;

            $template_entity = new \Template_Entity($template_data);

            $template_entity->set_group($template_group_entity);

            ee()->template_model->save_entity($template_entity);

            $this->log_install('template', $template_data['template_name']);
        }

        // --------------------------------
        //  Create Template Group
        // --------------------------------

        /**
         * Create a template group from an XML object
         *
         * @param obj $template_group XML object
         * @return int $group_id
         */
        private function create_template_group($template_group, $params = [])
        {
            $template_group_data = $this->get_attributes($template_group);

            if ($this->exists('template_groups', ['group_name' => $template_group_data['group_name']])) {
                return $this->log_error('template_group_exists', $template_group_data['group_name']);
            }

            $template_group_data['site_id'] = ee()->config->item('site_id');

            if (@$template_group_data['is_site_default'] == 'y') {
                ee()->db->where('is_site_default', 'y');

                if (ee()->db->count_all_results('template_groups')) {
                }

                $template_group_data['is_site_default'] = 'n';
            }

            $group_id = $this->insert('template_groups', $template_group_data);

            $this->log_install('template_group', $template_group_data['group_name']);

            if (isset($template_group->template)) {
                foreach ($template_group->template as $template) {
                    $this->create_template($template, $group_id, $template_group_data);
                }
            }
        }

        private function get_attributes($node)
        {
            $attr = [];

            foreach ($node->attributes() as $key => $value) {
                $attr[$key] = $this->clean_data($value);
            }

            return $attr;
        }

        // --------------------------------
        //  Create Channel
        // --------------------------------

        /**
         * Create a channel from an XML object
         *
         * @param obj $channel XML object
         * @param bool $add_data If TRUE this will add channel data to be installed
         */
        private function create_channel($channel, $params = [])
        {
            $channel_data = $this->get_attributes($channel);

            $add_data = false;
            if (!empty($params['add_data'])) {
                $add_data = true;
            }

            if ($this->exists('channels', ['channel_name' => $channel_data['channel_name']])) {
                return $this->log_error('channel_exists', $channel_data['channel_name']);
            }

            $cat_group = [];

            foreach (['field_group', 'cat_group'] as $child) {
                if (isset($channel->$child)) {
                    $channel_data[$child] = call_user_func([$this, 'create_' . $child], $channel->$child);
                }
            }

            $channel_data['cat_group'] = (isset($channel_data['cat_group']) && !count($cat_group)) ? $channel_data['cat_group'] : implode('|', $cat_group);
            $channel_data['site_id'] = ee()->config->item('site_id');
            $channel_data['channel_lang'] = ee()->config->item('xml_lang');
            $channel_data['channel_encoding'] = ee()->config->item('charset');

            $channel_id = $this->insert('channels', $channel_data);

            $this->create_group_fields($channel_data['field_group'], $channel_id);

            // insert in the exp_channels_channel_field_groups to match the channel id with the field group
            $channel_field_group['channel_id'] = $channel_id;
            $channel_field_group['group_id'] = $channel_data['field_group'];

            $this->insert('channels_channel_field_groups', $channel_field_group);

            if ($add_data) {
                if (isset($channel->channel_data)) {
                    $this->create_channel_entry($channel->channel_data);
                }
            }
            if (!empty($channel_data['channel_member_groups'])) {
                if (strtolower($channel_data['channel_member_groups']) === 'all') {
                    $query = ee('Model')->get('MemberGroup')->filter('group_id', '>=', '4')->all();

                    foreach ($query as $row) {
                        ee()->db->insert('channel_member_groups',
                            ['group_id' => $row->group_id, 'channel_id' => $channel_id]);
                    }
                    unset($query);
                // $query->free_result();
                } else {
                    foreach (explode('|', $channel_data['channel_member_groups']) as $group_id) {
                        ee()->db->insert('channel_member_groups',
                            ['group_id' => $group_id, 'channel_id' => $channel_id]);
                    }
                }
            }

            $this->log_install('channel', $channel_data['channel_name']);
        }

        // --------------------------------
        //  Create Channel Entry
        // --------------------------------

        /**
         * Create a channel entry from an XML object
         *
         * @param obj $channel_data XML object
         */
        private function create_channel_entry($channel)
        {
            $channel_data = $this->get_attributes($channel);

            if (!$this->exists('channels', ['channel_name' => $channel_data['channel_name']])) {
                return $this->log_error('channel_does_not_exist', $channel_data['channel_name']);
            }

            ee()->load->model('packages_entries_model');
            ee()->load->model('channel_model');

            foreach ($channel->channel_entry as $entry_xml) {
                $entry = [];
                $query = ee()->channel_model->get_channels(null, [],
                    [['channel_name' => [$channel_data['channel_name']]]]);

                $entry['channel_id'] = null;
                if ($query->num_rows()) {
                    foreach ($query->result() as $row) {
                        $entry['channel_id'] = $row->channel_id;
                    }
                }
                if (!$entry['channel_id']) {
                    continue;
                }
                foreach ($this->get_attributes($entry_xml) as $key => $data) {
                    $entry[$key] = $data;
                }
                foreach ($entry_xml->field as $field) {
                    $attr = $this->get_attributes($field);

                    if (isset($attr['data'])) {
                        $entry[$attr['name']] = $attr['data'];
                    }
                }

                ee()->packages_entries_model->create_entry($entry);
            }
        }

        // --------------------------------
        //  Exists
        // --------------------------------

        /**
         * Check to see if a database record exists in the specified table
         * Will return the id if $id_field is specified
         *
         * @param string $table name of table to check
         * @param array $data key=>value pairs of which columns to check for match
         * @param string $id_field name of id column
         * @return bool|int $id_field
         */
        private function exists($table, $data, $id_field = false)
        {
            if (ee()->db->field_exists('site_id', $table)) {
                $data['site_id'] = ee()->config->item('site_id');
            }

            $select = ($id_field) ? $id_field : '*';

            ee()->db->select($select);

            ee()->db->where($data);

            $query = ee()->db->get($table);

            return ($id_field) ? $query->row($id_field) : (bool)$query->num_rows();
        }

        // --------------------------------
        //  Insert
        // --------------------------------

        /**
         * Insert a record into the database
         *
         * @param string $table the database table name
         * @param array $data a keyed array of the data to insert
         * @return int $DB->insert_id
         */
        private function insert($table, $data)
        {
            $data = $this->clean_fields($table, $data);

            ee()->db->insert($table, $data);

            return ee()->db->insert_id();
        }

        // --------------------------------
        //  Load XML
        // --------------------------------

        /**
         * Log an error to be displayed on process
         *
         * @param string $error the error code
         * @param string $data first string of data for error msg
         * @param string $second_data second string of data for error msg
         * @return string $xml
         */
        private function log_error($error)
        {
            $args = func_get_args();

            array_shift($args);

            $this->errors[] = vsprintf(lang('error_' . $error), $args);

            return false;
        }

        public function errors()
        {
            return $this->errors;
        }

        // --------------------------------
        //  Log Install
        // --------------------------------

        /**
         * Logs a successful "Auto-Install" action
         *
         * @param string $type the type (channel, template, etc) installed
         * @param string $name the name of the type installed
         */
        private function log_install($type)
        {
            $args = func_get_args();

            array_shift($args);

            $this->installed[] = vsprintf(lang('installed_' . $type), $args);
        }

        public function installed()
        {
            return $this->installed;
        }

        /**
         * set template path
         *
         * If using flat files w/ templates, you can set the dir where they're stored
         *
         * @return $this
         */
        public function set_template_path($template_path)
        {
            if (is_dir($template_path)) {
                $this->template_path = rtrim($template_path, '/') . '/';
            }

            return $this;
        }

        // --------------------------------
        //  PARSE XML
        // --------------------------------

        /**
         * Parse through and install the submitted XML
         * @param bool $xml
         * @return array|string
         */
        private function parse_xml($xml = false)
        {
            if (!function_exists('simplexml_load_string')) {
                return $this->log_error('no_simplexml');
            }

            if (!$xml) {
                return $this->log_error('blank_xml');
            }

            $xml = (file_exists($xml)) ? simplexml_load_file($xml) : simplexml_load_string($xml);

            if ($xml === false) {
                return $this->log_error('xml_error');
            }

            $packages = [];

            foreach ($this->package_types as $type) {
                if (empty($xml->$type)) {
                    continue;
                }

                foreach ($xml->$type as $package) {
                    $packages[] = $package;
                }
            }

            return $packages;
        }

        // --------------------------------
        //  INSTALL
        // --------------------------------

        /**
         * Parse through and install the submitted XML
         *
         * @param array $params additional parameters to be passed
         */
        public function install($params = [])
        {
            foreach ($this->packages as $package) {
                if (in_array($package->getName(), $this->package_types)) {
                    call_user_func([$this, 'create_' . $package->getName()], $package, $params);
                }
            }
        }

        public function install_templates($params = [])
        {
            foreach ($this->packages as $package) {
                $this->create_template_group($package, $params);
            }
        }

        public function install_channels($params = [])
        {
            foreach ($this->packages as $package) {
                $this->create_channel($package, $params);
            }
        }

        // --------------------------------
        //  RENAME
        // --------------------------------

        /**
         * Checks to see if a record exists for a certain name,
         * and if so, it will append an integer to the end of the
         * name in an attempt to generate a unique name.
         * If the set limit $this->rename_limit is reached it will
         * return FALSE.
         *
         * @param string $table the database table to check
         * @param string $name the name to check
         * @param string $field the name of the name database column
         * @param array $data additional data to check against
         * @return string|bool
         */
        private function rename($table, $name, $field, $data = [], $rename_limit = 25)
        {
            $original_name = $name;

            $count = '';

            do {
                $name = $original_name . $count;

                $count++;

                $exists = $this->exists($table, array_merge([$field => $name], $data));
            } while ($count < $rename_limit && $exists);

            return ($count == $rename_limit && $exists) ? false : $name;
        }
    }

endif;
