<?php

require_once PATH_THIRD . 'cartthrob/vendor/autoload.php';

return [
    'author' => 'Foster Made',
    'author_url' => 'https://fostermade.co',
    'name' => 'CartThrob',
    'description' => 'The most powerful and versatile ecommerce system available for ExpressionEngine.',
    'version' => CARTTHROB_VERSION,
    'namespace' => '\\',
    'settings_exist' => true,
    'fieldtypes' => [
        'cartthrob_discount' => [
            'name' => 'CartThrob Discount Settings',
        ],
        'cartthrob_order_items' => [
            'name' => 'CartThrob Order Items',
        ],
        'cartthrob_package' => [
            'name' => 'CartThrob Package',
        ],
        'cartthrob_price_by_member_group' => [
            'name' => 'CartThrob Price - By Member Group',
        ],
        'cartthrob_price_modifiers' => [
            'name' => 'CartThrob Price Modifiers',
        ],
        'cartthrob_price_modifiers_configurator' => [
            'name' => 'CartThrob Price Modifiers Configurator',
        ],
        'cartthrob_price_quantity_thresholds' => [
            'name' => 'CartThrob Price - Quantity',
        ],
        'cartthrob_price_simple' => [
            'name' => 'CartThrob Price - Simple',
            'compatibility' => 'text',
        ],
    ],
];
