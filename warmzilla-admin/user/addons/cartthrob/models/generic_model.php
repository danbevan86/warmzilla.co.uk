<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Generic_model
 *
 * This model is a generic public CRUD model
 *    Use it like this:
 *    $this->load->model('generic_model');
 *    $my_model= new Generic_model("my_table_name");
 *    all data is passed in through arrays
 *
 * @uses crud_model
 * @uses crud_interface
 **/
class Generic_model extends Crud_model implements Crud_interface
{
    /**
     * $this->table_name
     * this is the name of the database table to act on
     * @var string
     **/
    protected $table_name = '';

    /**
     * constructor
     *
     * @param string $table_name Name of the database table
     **/
    public function __construct($table_name = null)
    {
        //	parent::Crud_model();
        $this->table_name = $table_name;
    }

    public function create($array)
    {
        return $this->_create($array);
    }

    public function read(
        $id = null,
        $order_by = null,
        $order_direction = 'asc',
        $field_name = null,
        $string = null,
        $limit = null,
        $offset = null
    ) {
        return $this->_read($id, $order_by, $order_direction, $field_name, $string, $limit, $offset);
    }

    public function update($id, $array)
    {
        return $this->_update($id, $array);
    }

    public function delete($id)
    {
        return $this->_delete($id);
    }

    public function search($fields_array, $search_terms_array, $limit = null, $offset = null, $like_or = 'like')
    {
        return $this->_search($fields_array, $search_terms_array, $like_or, $limit, $offset);
    }

    /**
     * get_table_name
     *
     **/
    public function get_table_name()
    {
        return $this->_get_table_name();
    }
}
