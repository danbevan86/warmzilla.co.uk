<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cartthrob_price_simple_ft extends EE_Fieldtype
{
    public $info = [
        'name' => 'CartThrob Price - Simple',
        'version' => CARTTHROB_VERSION,
    ];

    public function __construct()
    {
        parent::__construct();

        $package_path_added = in_array(PATH_THIRD . 'cartthrob/', ee()->load->get_package_paths());

        if (!$package_path_added) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        }

        ee()->load->helper('data_formatting');

        if (!$package_path_added) {
            ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');
        }
    }

    public function install()
    {
        return [
            'field_prefix' => '$',
        ];
    }

    public function validate($data)
    {
        ee()->load->library('form_validation');

        if ($data && !ee()->form_validation->numeric($data)) {
            return ee()->lang->line('numeric');
        }

        return true;
    }

    public function pre_process($data)
    {
        if (isset($this->row['channel_id'])) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

            ee()->load->model('cartthrob_settings_model');
            ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

            $product_channel_fields = ee()->config->item('cartthrob:product_channel_fields');

            if (isset($product_channel_fields[$this->row['channel_id']]['global_price'])) {
                $global_price = $product_channel_fields[$this->row['channel_id']]['global_price'];

                if ($global_price !== '') {
                    $data = $product_channel_fields[$this->row['channel_id']]['global_price'];
                }
            }
        }

        return $data;
    }

    public function replace_no_tax($data, $params = '', $tagdata = '')
    {
        return $this->replace_tag($data, $params, $tagdata);
    }

    public function replace_tag($data, $params = '', $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->library('number');

        ee()->number->set_prefix($this->get_prefix());
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return ee()->number->format($data);
    }

    public function get_prefix()
    {
        if (empty($this->settings['field_prefix'])) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

            ee()->load->model('cartthrob_settings_model');
            ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

            return ee()->config->item('cartthrob:number_format_defaults_prefix');
        } else {
            return $this->settings['field_prefix'];
        }
    }

    public function replace_plus_tax($data, $params = '', $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->library('cartthrob_loader');

        ee()->load->library('number');

        if ($plugin = ee()->cartthrob->store->plugin(ee()->cartthrob->store->config('tax_plugin'))) {
            $data = $plugin->get_tax($data) + $data;
        }
        ee()->number->set_prefix($this->get_prefix());
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return ee()->number->format($data);
    }

    public function replace_plus_tax_numeric($data, $params = '', $tagdata = '')
    {
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->library('cartthrob_loader');

        ee()->load->library('number');

        if ($plugin = ee()->cartthrob->store->plugin(ee()->cartthrob->store->config('tax_plugin'))) {
            $data = $plugin->get_tax($data) + $data;
        }
        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return $data;
    }

    public function replace_numeric($data, $params = '', $tagdata = '')
    {
        return $data;
    }

    public function display_field($data)
    {
        $prefix = $this->get_prefix();

        $field_id = $this->settings['field_id'];

        $span = '<span style="position:absolute;padding:5px 0 0 5px;">' . $prefix . '</span>';

        ee()->javascript->output('
				var span = $(\'' . $span . '\').appendTo("body").css({top:-9999});
				var indent = span.width()+4;
				span.remove();
			
			$("#field_id_' . $field_id . '").before(\'' . $span . '\');
			$("#field_id_' . $field_id . '").css({paddingLeft: indent});
			');

        return form_input([
            'name' => $this->field_name,
            'id' => $this->field_name,
            'class' => 'cartthrob_price_simple',
            'value' => $data,
            'maxlength' => $this->settings['field_maxl'],
        ]);
    }

    public function display_settings($data)
    {
        $field_maxl = (empty($data['field_maxl'])) ? 12 : $data['field_maxl'];

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->library('cartthrob_loader');

        $field_prefix = (empty($data['field_prefix'])) ? ee()->cartthrob->store->config('number_format_defaults_prefix') : $data['field_prefix'];

        ee()->load->remove_package_path(PATH_THIRD . 'cartthrob/');

        return [
            'field_options_cartthrob_price_simple' => [
                'label' => 'field_options',
                'group' => 'cartthrob_price_simple',
                'settings' => [
                    [
                        'title' => 'field_max_length',
                        'desc' => '',
                        'fields' => [
                            'field_maxl_cps' => [
                                'type' => 'text',
                                'value' => $field_maxl,
                            ],
                        ],
                    ],
                    [
                        'title' => 'number_format_defaults_prefix',
                        'desc' => 'number_format_defaults_prefix_desc',
                        'fields' => [
                            'field_prefix_cps' => [
                                'type' => 'text',
                                'value' => $field_prefix,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function settings_modify_column($data)
    {
        $fields = parent::settings_modify_column($data);

        $fields['field_id_' . $data['field_id']]['type'] = 'FLOAT';
        $fields['field_id_' . $data['field_id']]['default'] = 0;

        return $fields;
    }

    public function save_settings($data)
    {
        return [
            'field_maxl' => ee()->input->post('field_maxl_cps'),
            'field_prefix' => ee()->input->post('field_prefix_cps'),
            'field_fmt' => 'none',
        ];
    }
}
