<?php

use GuzzleHttp\Psr7\Uri;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property CI_Controller   $EE
 * @property Cartthrob_cart  $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_ext
{
    public $settings = [];
    public $name = 'CartThrob';
    public $version;
    public $description = 'CartThrob Shopping Cart';
    public $settings_exist = 'y';
    public $docs_url = 'http://cartthrob.com/docs';
    public $required_by = ['module'];

    public function __construct($settings = '')
    {
        $this->version = CARTTHROB_VERSION;
    }

    public function settings_form()
    {
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob'));
    }

    public function activate_extension()
    {
        return true;
    }

    /**
     * @param string
     * @return void|bool False if the extension is current
     */
    public function update_extension($current = '')
    {
        if ($current == '' or $current == $this->version) {
            return false;
        }

        ee()->db->update('extensions', ['version' => $this->version], ['class' => __CLASS__]);

        return true;
    }

    /**
     * @param null
     */
    public function disable_extension()
    {
        ee()->db->delete('extensions', ['class' => __CLASS__]);
    }

    public function settings()
    {
    }

    public function safecracker_submit_entry_start(&$channel_form)
    {
        if (!$this->isCartthrobChannel(ee()->input->post('channel_id', true))) {
            return $channel_form;
        }

        return $this->channel_form_submit_entry_start($channel_form);
    }

    /**
     * @param $channel_form
     * @return array
     */
    public function channel_form_submit_entry_start($channel_form)
    {
        if (!$this->isCartthrobChannel(ee()->input->post('channel_id', true))) {
            return $channel_form;
        }

        $data = [
            'entry_id' => ee()->input->post('entry_id'),
            'channel_id' => ee()->input->post('channel_id', true),
            'status' => ee()->input->post('status', true),
        ];

        if (ee()->input->post('entry_id')) {
            ee()->load->model('channel_entries_model');
            $entry = ee()->channel_entries_model->get_entry($data['entry_id'], $data['channel_id']);

            if ($entry->num_rows() > 0) {
                $row = $entry->row();
                $data['revision_post']['status'] = $row->status;
            }
        }

        return $this->publish_form_entry_data($data);
    }

    /**
     * Test if the entries channel is a CartThrob channel
     *
     * @param $channelId
     * @return bool
     */
    private function isCartthrobChannel($channelId)
    {
        ee()->load->model('cartthrob_settings_model');

        $settings = ee()->cartthrob_settings_model->get_settings();
        $cartthrobChannels = [$settings['orders_channel']];

        return in_array($channelId, $cartthrobChannels);
    }

    /**
     * @param array
     * @return array
     */
    public function publish_form_entry_data($data = [])
    {
        if (isset($data['channel_id']) && !$this->isCartthrobChannel($data['channel_id'])) {
            return $data;
        }

        ee()->load->model('cartthrob_settings_model');

        if (ee()->config->item('cartthrob:save_orders') && ee()->config->item('cartthrob:orders_channel')) {
            if (!empty($data['entry_id']) && !empty($data['status'])) {
                if ($data['channel_id'] == ee()->config->item('cartthrob:orders_channel')) {
                    $this->set_status($data['entry_id'], $data['status']);
                }
                if (!empty($data['entry_id']) && !empty($data['revision_post']['status'])) {
                    $this->set_status($data['entry_id'], $data['revision_post']['status']);
                }
            }
        }

        return $data;
    }

    private function set_status($entry_id, $status)
    {
        ee()->session->cache['cartthrob'][$entry_id]['status'] = $status;
    }

    public function safecracker_submit_entry_end(&$channel_form)
    {
        if (!$this->isCartthrobChannel(ee()->input->post('channel_id', true))) {
            return;
        }

        $this->channel_form_submit_entry_end($channel_form);
    }

    public function channel_form_submit_entry_end($channel_form)
    {
        if (!$this->isCartthrobChannel(ee()->input->post('channel_id', true))) {
            return;
        }

        $data = [
            'entry_id' => ee()->input->post('entry_id'),
            'channel_id' => ee()->input->post('channel_id', true),
            'status' => ee()->input->post('status', true),
            'revision_post' => [
                'status' => 'open',
            ],
        ];

        if (ee()->input->post('entry_id')) {
            $data['revision_post']['status'] = $channel_form->entry('status');
        }
    }

    public function before_channel_entry_update($entry, $values, $modified)
    {
        if (!$this->isCartthrobChannel($entry->channel_id)) {
            return;
        }

        $old_status = ee('Model')->get('ChannelEntry')
            ->fields('status')
            ->filter('entry_id', $entry->entry_id)
            ->first();

        if (!is_null($old_status)) {
            $this->set_status($entry->entry_id, $old_status->status);
        }
    }

    public function after_channel_entry_update($entry, $values, $modified)
    {
        if (!$this->isCartthrobChannel($entry->channel_id)) {
            return;
        }

        $status_start = $this->getLastStatus($entry->entry_id, $entry->status);

        $this->send_status_change_emails($status_start, $values);
    }

    /**
     * Get the entry's last status value
     *
     * @param $entryId
     * @param $status
     * @return bool
     */
    private function getLastStatus($entryId, $status)
    {
        $lastStatus = ee()->session->cache['cartthrob'][$entryId]['status'] ?? '';

        if ($status == null || empty($lastStatus)) {
            return false;
        }

        return $lastStatus;
    }

    /**
     * Send CT emails when an entry status changes
     *
     * @param string $status_start the original status
     * @param array $data array of entry data
     * @return array
     */
    private function send_status_change_emails($status_start, $data)
    {
        if (!isset(ee()->cartthrob)) {
            ee()->load->library('cartthrob_loader');
        }

        $data['previous_status'] = $status_start;

        ee()->load->library('cartthrob_emails');

        $emails = ee()->cartthrob_emails->get_email_for_event($event = null, $status_start, $data['status']);

        if (!empty($emails)) {
            foreach ($emails as $emailDetails) {
                if (!empty($emailDetails['to']) && $emailDetails['to'] == '{customer_email}') {
                    if (ee()->cartthrob->store->config('orders_customer_email') && !empty($data['field_id_' . ee()->cartthrob->store->config('orders_customer_email')])) {
                        $emailDetails['to'] = $data['field_id_' . ee()->cartthrob->store->config('orders_customer_email')];
                    }
                }
                if (!empty($emailDetails['from']) && $emailDetails['from'] == '{customer_email}') {
                    if (ee()->cartthrob->store->config('orders_customer_email') && !empty($data['field_id_' . ee()->cartthrob->store->config('orders_customer_email')])) {
                        $emailDetails['from'] = $data['field_id_' . ee()->cartthrob->store->config('orders_customer_email')];
                    }
                }
                if (!empty($emailDetails['from_name']) && $emailDetails['from_name'] == '{customer_name}') {
                    if (ee()->cartthrob->store->config('orders_customer_name') && !empty($data['field_id_' . ee()->cartthrob->store->config('orders_customer_name')])) {
                        $emailDetails['from_name'] = $data['field_id_' . ee()->cartthrob->store->config('orders_customer_name')];
                    }
                }
                ee()->cartthrob_emails->send_email($emailDetails, $data);
            }
        }

        return $data;
    }

    public function entry_submission_end($entry_id, $meta = [], $data = [])
    {
        if (empty($data['revision_post']['status'])) {
            return $data;
        }

        $status_start = $this->getLastStatus($entry_id, $data['revision_post']['status']);

        $this->send_status_change_emails($status_start, $data['revision_post']);
    }

    public function submission_ready($meta = [], $data = [], $autosave)
    {
        ee()->load->model('cartthrob_settings_model');

        if (empty($data['revision_post']['status'])) {
            return $data;
        }

        $status_start = $this->getLastStatus($data['entry_id'], $data['revision_post']['status']);
        $data['revision_post']['previous_status'] = $status_start;
        ee()->load->library('cartthrob_emails');

        $emails = ee()->cartthrob_emails->get_email_for_event($event = null, $status_start,
            $data['revision_post']['status']);
        if (!empty($emails)) {
            foreach ($emails as $emailDetails) {
                if (!empty($emailDetails['to']) && $emailDetails['to'] == '{customer_email}') {
                    if (ee()->config->item('cartthrob:orders_customer_email') && !empty($data['field_id_' . ee()->config->item('cartthrob:orders_customer_email')])) {
                        $emailDetails['to'] = $data['field_id_' . ee()->config->item('cartthrob:orders_customer_email')];
                    }
                }
                if (!empty($emailDetails['from']) && $emailDetails['from'] == '{customer_email}') {
                    if (ee()->config->item('cartthrob:orders_customer_email') && !empty($data['field_id_' . ee()->config->item('cartthrob:orders_customer_email')])) {
                        $emailDetails['from'] = $data['field_id_' . ee()->config->item('cartthrob:orders_customer_email')];
                    }
                }
                if (!empty($emailDetails['from_name']) && $emailDetails['from_name'] == '{customer_name}') {
                    if (ee()->config->item('cartthrob:orders_customer_name') && !empty($data['field_id_' . ee()->config->item('cartthrob:orders_customer_name')])) {
                        $emailDetails['from_name'] = $data['field_id_' . ee()->config->item('cartthrob:orders_customer_name')];
                    }
                }
                ee()->cartthrob_emails->send_email($emailDetails, $data['revision_post']);
            }
        }

        return $data;
    }

    /**
     * Perform additional actions after logout
     *
     * @param null
     */
    public function member_member_logout()
    {
        if (!method_exists(ee()->load, 'get_package_paths') || !in_array(PATH_THIRD . 'cartthrob/', ee()->load->get_package_paths())) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        }

        ee()->load->library('cartthrob_loader');

        if (ee()->cartthrob->store->config('clear_session_on_logout')) {
            ee()->cartthrob_session->destroy();
        } else {
            // @TODO should we update their member_id to 0? or save it for them for later?

            if (ee()->cartthrob->store->config('clear_cart_on_logout') && !ee()->cartthrob->cart->is_empty()) {
                ee()->cartthrob->cart->clear()->save();
            }
        }
    }

    public function member_member_login($userdata)
    {
        if (!method_exists(ee()->load, 'get_package_paths') || !in_array(PATH_THIRD . 'cartthrob/', ee()->load->get_package_paths())) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        }

        ee()->load->library('cartthrob_loader');

        // attach the user's member id to this cart
        if (!empty($userdata->member_id) && ee()->cartthrob_session->session_id()) {
            ee()->cartthrob_session->update(['member_id' => $userdata->member_id]);
        }
    }

    public function cp_menu_array($menu)
    {
        if (ee()->extensions->last_call !== false) {
            $menu = ee()->extensions->last_call;
        }

        // if the user has uploaded a new version, but hasn't run the updater yet
        // this hook can cause some pretty bad errors if it tries to access database tables/fields
        // that aren't yet created
        // we're gonna kill this feature if we detect that they need an update
        // so they don't get any errors trying to get to the modules page to do the update
        if (REQ === 'CP') {
            // i'm not worried about the overhead from this since a) we're in the CP and b) Accessories lib calls this on every CP page
            ee()->load->library('addons');

            $modules = ee()->addons->get_installed();

            if (!isset($modules['cartthrob']['module_version']) || version_compare($this->version,
                    $modules['cartthrob']['module_version'], '>')) {
                return $menu;
            }
        }

        $channels = [];

        if (isset($menu['content']['publish']) && is_array($menu['content']['publish'])) {
            // we've got a perfectly good list of channels right here in menu, let's grab it
            foreach ($menu['content']['publish'] as $channel_name => $url) {
                if (preg_match('/channel_id=(\d+)$/', $url, $match)) {
                    $channels[$match[1]] = $channel_name;
                }
            }
        } else {
            if (isset($menu['content']['publish']) && is_string($menu['content']['publish']) && preg_match('/channel_id=(\d+)$/', $menu['content']['publish'], $match)) {
                $channels[$match[1]] = '';
            }
        }

        ee()->lang->loadfile('cartthrob', 'cartthrob');

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        ee()->load->model('cartthrob_settings_model');

        $label = 'cartthrob';

        if (ee()->config->item('cartthrob:cp_menu_label')) {
            ee()->lang->language['nav_' . ee()->config->item('cartthrob:cp_menu_label')] = ee()->config->item('cartthrob:cp_menu_label');

            $label = ee()->config->item('cartthrob:cp_menu_label');
        }

        $menu[$label] = [];

        $has_channel = false;

        if (ee()->config->item('cartthrob:product_channels')) {
            if (count(ee()->config->item('cartthrob:product_channels')) > 1) {
                foreach (ee()->config->item('cartthrob:product_channels') as $channel_id) {
                    if (isset($channels[$channel_id])) {
                        $has_channel = true;

                        ee()->lang->language['nav_' . $channels[$channel_id]] = $channels[$channel_id];
                        $menu[$label]['products'][$channels[$channel_id]] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . $channel_id;
                    }
                }
            } else {
                $channel_id = current(ee()->config->item('cartthrob:product_channels'));

                if (isset($channels[$channel_id])) {
                    $has_channel = true;

                    $menu[$label]['products'] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . $channel_id;
                }
            }
        }

        if (ee()->config->item('cartthrob:save_orders') && ee()->config->item('cartthrob:orders_channel')) {
            if (isset($channels[ee()->config->item('cartthrob:orders_channel')])) {
                $has_channel = true;

                $menu[$label]['orders'] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . ee()->config->item('cartthrob:orders_channel');
            }
        }

        if (ee()->config->item('cartthrob:save_purchased_items') && ee()->config->item('cartthrob:purchased_items_channel')) {
            if (isset($channels[ee()->config->item('cartthrob:purchased_items_channel')])) {
                $has_channel = true;

                $menu[$label]['purchased_items'] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . ee()->config->item('cartthrob:purchased_items_channel');
            }
        }

        if (ee()->config->item('cartthrob:discount_channel')) {
            if (isset($channels[ee()->config->item('cartthrob:discount_channel')])) {
                $has_channel = true;

                $menu[$label]['discounts'] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . ee()->config->item('cartthrob:discount_channel');
            }
        }

        if (ee()->config->item('cartthrob:coupon_code_channel')) {
            if (isset($channels[ee()->config->item('cartthrob:coupon_code_channel')])) {
                $has_channel = true;

                $menu[$label]['coupon_codes'] = BASE . AMP . 'C=content_edit' . AMP . 'channel_id=' . ee()->config->item('cartthrob:coupon_code_channel');
            }
        }

        $add_settings_menu = true;

        if (ee()->session->userdata('group_id') != 1) {
            if (!ee()->session->userdata('assigned_modules') || !ee()->cp->allowed_group('can_access_addons',
                    'can_access_modules')) {
                $add_settings_menu = false;
            } else {
                $module_id = ee()->db->select('module_id')
                    ->where('module_name', 'Cartthrob')
                    ->get('modules')
                    ->row('module_id');

                $assigned_modules = ee()->session->userdata('assigned_modules') ? ee()->session->userdata('assigned_modules') : [];

                if (!$module_id || !array_key_exists($module_id, $assigned_modules)) {
                    $add_settings_menu = false;
                }
            }
        }

        if ($add_settings_menu === true) {
            require_once PATH_THIRD . 'cartthrob/mcp.cartthrob.php';

            $settings = [];

            foreach (array_keys(Cartthrob_mcp::nav()) as $nav) {
                if (!in_array($nav, Cartthrob_mcp::$no_nav)) {
                    if ($nav == 'modules') {
                        continue;
                    } else {
                        $settings[$nav] = BASE . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=cartthrob' . AMP . 'method=' . $nav;
                    }
                }
            }

            if ($has_channel) {
                $menu[$label][] = '----';

                $menu[$label]['settings'] = $settings;
            } else {
                $menu[$label] = $settings;
            }
        }

        if (ee()->session->userdata('group_id') != 1 && (!ee()->session->userdata('assigned_modules') || !ee()->cp->allowed_group('can_access_addons', 'can_access_modules'))) {
            // do nothing.
        } elseif (ee()->extensions->active_hook('cartthrob_addon_register') === true) {
            if (($addons = ee()->extensions->call('cartthrob_addon_register')) !== false) {
                foreach ($addons as $class) {
                    $class = str_replace('cartthrob_', '', $class);

                    $module_id = ee()->db->select('module_id')
                        ->where('module_name', 'Cartthrob_' . $class)
                        ->get('modules')
                        ->row('module_id');

                    /** @var bool $non_core module TRUE if this is not a ct module */
                    $non_core_module = false;
                    if (!$module_id) {
                        $non_core_module = true;
                        $module_id = ee()->db->select('module_id')
                            ->where('module_name', ucwords($class))
                            ->get('modules')
                            ->row('module_id');
                    }
                    $assigned_modules = ee()->session->userdata('assigned_modules') ? ee()->session->userdata('assigned_modules') : [];

                    if (ee()->session->userdata('group_id') != 1 && (!$module_id || !array_key_exists($module_id,
                                $assigned_modules))) {
                        $add_settings_menu = false;
                    } else {
                        if ($non_core_module) {
                            ee()->load->add_package_path(PATH_THIRD . $class . '/');
                            ee()->lang->loadfile($class);
                            ee()->load->remove_package_path(PATH_THIRD . $class . '/');

                            $addon_menu[$class] = BASE . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=' . $class;
                        } else {
                            ee()->load->add_package_path(PATH_THIRD . 'cartthrob_' . $class . '/');
                            ee()->lang->loadfile('cartthrob_' . $class);
                            ee()->load->remove_package_path(PATH_THIRD . 'cartthrob_' . $class . '/');

                            $addon_menu['cartthrob_' . $class] = BASE . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=' . 'cartthrob_' . $class;
                        }
                    }
                }
            }
        }

        if (!empty($addon_menu)) {
            $menu[$label][] = '----';
            $menu[$label]['addons'] = $addon_menu;
        }

        if (empty($menu[$label])) {
            unset($menu[$label]);
        }

        return $menu;
    }

    public function cp_custom_menu($menu)
    {
        $menu->addItem('CartThrob Pro', ee('CP/URL')->make('/addons/settings/cartthrob/global_settings'));
    }

    public function core_boot()
    {
        ee()->load->library('cartthrob_loader');
        require_once PATH_THIRD . 'cartthrob/mod.cartthrob.php';

        $actionId = (int)ee()->functions->insert_action_ids(ee()->functions->fetch_action_id('Cartthrob', 'consume_async_job'));

        if (
            (int)ee()->input->get('ACT', null) === $actionId ||
            Cartthrob::ASYNC_METHOD_HTTP !== (int)ee()->cartthrob->config('orders_async_method')
        ) {
            return;
        }

        if (!method_exists(ee()->load, 'get_package_paths') || !in_array(PATH_THIRD . 'cartthrob/', ee()->load->get_package_paths())) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');
        }

        $client = HttpClient::create(ee()->config->item('orders_async_http_client_options') ?: []);

        ee()->load->library('paths');
        $url = new Uri(ee()->paths->build_action_url('Cartthrob', 'consume_async_job'));
        if ($workerUrl = ee()->cartthrob->config('orders_async_worker_base_url')) {
            $query = [];
            parse_str(parse_url($url, PHP_URL_QUERY), $query);
            $url = Uri::withQueryValues(new Uri($workerUrl), $query);
        }

        $response = $client->request('GET', $url, [
            'headers' => ['Referer' => $_SERVER['HTTP_REFERER'] ?? ''],
            'on_progress' => function (int $dlNow, int $dlSize, array $info): void {
                if ($info['pretransfer_time'] > 0) {
                    throw new \Exception();
                }
            },
        ]);

        try {
            $response->getHeaders();
        } catch (TransportExceptionInterface $exception) {
            // pass
        }
    }
}
