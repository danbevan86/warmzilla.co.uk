<?php

global $assignToConfig, $systemPath, $debug;

if (!isset($systemPath)) {
    $systemPath = "system";
}

if (realpath($systemPath) !== false) {
    $systemPath = realpath($systemPath) . '/';
}

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('EXT', '.php');
define('BASEPATH', $systemPath . 'ee/legacy/');
define('FCPATH', str_replace(SELF, '', __FILE__));
define('SYSDIR', pathinfo($systemPath, PATHINFO_BASENAME));
define('CI_VERSION', '2.0');
define('DEBUG', isset($debug) ? $debug : 0);
define('SYSPATH', $systemPath);

if (file_exists($systemPath . 'ee/legacy/config/constants.php')) {
    require_once $systemPath . 'ee/legacy/config/constants.php';
} else {
    require_once $systemPath . 'ee/EllisLab/ExpressionEngine/Config/constants.php';
}
require_once $systemPath . 'ee/EllisLab/ExpressionEngine/Boot/boot.common.php';
require_once $systemPath . 'ee/EllisLab/ExpressionEngine/Core/Autoloader.php';
require_once $systemPath . 'ee/legacy/core/Controller.php';

$autoloader = EllisLab\ExpressionEngine\Core\Autoloader::getInstance();

$autoloader->addPrefix(
    'EllisLab',
    $systemPath . 'ee/EllisLab/'
);

$autoloader->addPrefix(
    'Michelf',
    $systemPath . 'ee/legacy/libraries/typography/Markdown/Michelf/'
);

// EL patch

$autoloader->addPrefix(
    'Mexitek',
    $systemPath . 'ee/Mexitek/'
);

$autoloader->register();

class EE3_Bootstrap extends EllisLab\ExpressionEngine\Core\ExpressionEngine
{
    public function boot($assign_to_config = null)
    {
        parent::boot();

        $app = $this->loadApplicationCore();

        if (isset($assign_to_config)) {
            $this->overrideConfig($assign_to_config);
        }

        $this->getLegacyApp()->getFacade()->load->library('core');

        $this->getLegacyApp()->getFacade()->core->bootstrap();

        Controller::_setFacade($this->getLegacyApp()->getFacade());

        new Controller();
    }

    public static function getInstance()
    {
        static $instance;

        if (is_null($instance)) {
            $instance = new static;
        }

        return $instance;
    }
}

function get_instance()
{
    return EE3_Bootstrap::getInstance()->getLegacyApp()->getFacade();
}

function ee($make = null)
{
    if (func_num_args() === 0) {
        return EE3_Bootstrap::getInstance()->getLegacyApp()->getFacade();
    }

    return call_user_func_array([EE3_Bootstrap::getInstance()->getLegacyApp()->getFacade()->di, 'make'],
        func_get_args());
}

EE3_Bootstrap::getInstance()->boot(isset($assignToConfig) ? $assignToConfig : null);
