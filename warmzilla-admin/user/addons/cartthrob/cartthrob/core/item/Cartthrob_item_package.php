<?php

if (!defined('CARTTHROB_PATH')) {
    Cartthrob_core::core_error('No direct script access allowed');
}

class Cartthrob_item_package extends Cartthrob_item
{
    protected $sub_items;

    protected $defaults = [
        'row_id' => null,
        'quantity' => 1,
        'product_id' => null,
        'site_id' => null,
        'price' => 0,
        'weight' => 0,
        'shipping' => 0,
        'title' => '',
        'no_tax' => false,
        'no_shipping' => false,
        'item_options' => [],
        'sub_items' => [],
        'discounts' => [],
    ];

    public function initialize($params = [], $defaults = [])
    {
        parent::initialize($params);

        if (isset($params['sub_items'])) {
            $this->set_sub_items($params['sub_items']);
        }
    }

    protected function set_sub_items($items)
    {
        foreach ($items as $item) {
            $class = (isset($item['class'])) ? $item['class'] : 'default';

            $this->sub_items[$item['row_id']] = Cartthrob_core::create_child($this->core, 'item_' . $class, $item, $this->core->item_defaults);
            $this->sub_items[$item['row_id']]->set_parent_item($this);
        }
    }

    public function sub_item($row_id)
    {
        return (isset($this->sub_items[$row_id])) ? $this->sub_items[$row_id] : false;
    }

    public function price()
    {
        // if the price is set explicitly via the product, then return it
        if (is_numeric($this->product()->price())) {
            return $this->product()->price($this->item_options());
        }

        $price = 0;

        foreach ($this->sub_items() as $row_id => $item) {
            $price += $item->price_subtotal();
        }

        return $price;
    }

    // @TODO add fixed pricing too

    public function product($force_create = true)
    {
        if (!$product = $this->core->store->product($this->product_id)) {
            // create a NULLed product
            if ($force_create) {
                $product = Cartthrob_core::create_child($this->core, 'product');
            }
        }

        return $product;
    }

    public function sub_items()
    {
        return $this->sub_items;
    }

    public function taxed_price()
    {
        if (is_numeric($this->product()->price())) {
            // @TODO if possible make this use item's methods of getting taxes. this may not always work if the item has a specific tax class
            return $this->product()->price($this) * (1 + $this->core->store->tax_rate());
        }

        $price = 0;

        foreach ($this->sub_items() as $item) {
            $price += $item->taxed_price_subtotal();
        }

        return $price;
    }

    public function in_stock()
    {
        return $this->inventory() > 0;
    }

    public function inventory()
    {
        $inventory = false;

        foreach ($this->sub_items() as $row_id => $item) {
            if (!$item->product_id()) {
                continue;
            }

            $_inventory = floor($item->inventory($item->item_options()) / $item->quantity());

            if ($inventory === false || $_inventory < $inventory) {
                $inventory = $_inventory;
            }
        }

        return ($inventory === false) ? parent::inventory() : $inventory;
    }

    public function weight()
    {
        // if the price is set explicitly via the product, then return it
        if (is_numeric($this->product()->weight())) {
            return $this->product()->weight($this);
        }

        $weight = 0;

        foreach ($this->sub_items() as $item) {
            $weight += $item->weight() * $item->quantity();
        }

        return $weight;
    }

    public function data()
    {
        $data = $this->product()->toArray();

        foreach ($this->toArray() as $key => $value) {
            $data[$key] = $value;
        }

        foreach ($this->sub_items() as $row_id => $item) {
            /* @var Cartthrob_item $item */
            $data['sub_items'][$row_id] = $item->toArray();
        }

        return $data;
    }

    /**
     * @param bool $stripDefaults
     * @return array
     */
    public function toArray($stripDefaults = false)
    {
        $data = parent::toArray($stripDefaults);

        foreach ($this->sub_items() as $row_id => $item) {
            /* @var Cartthrob_item $item */
            $data['sub_items'][$row_id] = array_merge($item->toArray(), ['row_id' => $row_id]);
        }

        return $data;
    }

    /**
     * Get the product title
     *
     * @return string
     */
    public function title()
    {
        return $this->product()->title();
    }

    // shortcut to this item's corresponding product object

    /**
     * Get a value from the meta array, or
     * from the product's meta array, or
     * get the whole array by not specifying a key
     *
     * @param string|false $key
     * @return mixed|false
     */
    public function meta($key = false)
    {
        if ($key === false) {
            return array_merge(parent::meta(), $this->product()->meta());
        }

        $meta = parent::meta($key);

        if ($meta === false) {
            return $this->product()->meta($key);
        }

        return $meta;
    }

    /*item_product*/

    public function base_price()
    {
        $item = clone $this;

        $item->clear_item_options();

        return $this->product()->price($item);
    }

    public function shipping()
    {
        if ($this->no_shipping) {
            return 0;
        }

        if ($this->core->hooks->set_hook('item_shipping_start')->run() && $this->core->hooks->end()) {
            $shipping = $this->core->hooks->value();
        } else {
            $shipping = (is_null($this->shipping)) ? $this->product()->shipping() * $this->quantity() : $this->shipping * $this->quantity();

            if ($this->core->hooks->set_hook('item_shipping_end')->run($shipping) && $this->core->hooks->end()) {
                $shipping = $this->core->hooks->value();
            }
        }

        return $this->core->round($shipping);
    }

    /**
     * Update the item's attributes with an array
     *
     * @param array $params
     * @return Cartthrob_item
     */
    public function update($params)
    {
        $sub_items = (isset($params['sub_items'])) ? $params['sub_items'] : [];

        unset($params['sub_items']);

        parent::update($params);

        foreach ($sub_items as $row_id => $item) {
            if (isset($this->sub_items[$row_id])) {
                $this->sub_items[$row_id]->update($item);
            }
        }

        // @TODO do something with sub_items
    }
}
