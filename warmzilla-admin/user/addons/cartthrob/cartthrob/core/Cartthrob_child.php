<?php

use Illuminate\Support\Arr;

if (!defined('CARTTHROB_PATH')) {
    Cartthrob_core::core_error('No direct script access allowed');
}

abstract class Cartthrob_child
{
    /** @var Cartthrob_core */
    protected $core;
    protected $defaults = [];
    protected $errors = [];

    public function errors()
    {
        return $this->errors;
    }

    public function set_error($error)
    {
        $this->errors[] = $error;

        return $this;
    }

    public function clear_errors()
    {
        $this->errors = [];

        return $this;
    }

    public function __call($method, $args)
    {
        if ($this->parent_class()) {
            $_method = $this->parent_class() . '_' . $method;
        } else {
            $_method = Cartthrob_core::get_class($this) . '_' . $method;
        }

        try {
            if (!in_array($_method, get_class_methods($this->core))) {
                throw new Exception('Call to undefined method %s::%s() in %s on line %s');
            } elseif (!is_callable([$this->core, $_method])) {
                throw new Exception('Call to private method %s::%s() in %s on line %s');
            }
        } catch (Exception $e) {
            $backtrace = $e->getTrace();
            $backtrace = $backtrace[1];

            return trigger_error(sprintf($e->getMessage(), $backtrace['class'], $backtrace['function'],
                $backtrace['file'], $backtrace['line']));
        }

        array_push($args, $this);

        return call_user_func_array([$this->core, $_method], $args);
    }

    public function parent_class()
    {
        static $parent_class;

        if (is_null($parent_class)) {
            $parent_class = false;

            $classname = Cartthrob_core::get_class($this);

            $parts = explode('_', $classname);

            if (count($parts) > 1) {
                $parent_class = $parts[0];
            }
        }

        return $parent_class;
    }

    public function defaults($key = false)
    {
        if ($key === false) {
            return $this->defaults;
        }

        return Arr::get($this->defaults, $key, false);
    }

    public function default_keys()
    {
        return array_keys($this->defaults);
    }

    public function is_null()
    {
        foreach ($this->defaults as $key => $value) {
            if ($this->$key !== $value) {
                return false;
            }
        }

        return true;
    }

    public function serialize()
    {
        return serialize($this->toArray());
    }

    public function toArray()
    {
        $data = [];

        foreach ($this->defaults as $key => $value) {
            $data[$key] = $this->$key;
        }

        return $data;
    }

    public function unserialize($data)
    {
        $this->initialize(unserialize($data));
    }

    public function initialize($params = [], $defaults = [])
    {
        $this->set_defaults($defaults);
        $this->prepare_params($params);

        foreach ($this->defaults as $key => $value) {
            $this->$key = Arr::get($params, $key, $value);
        }
    }

    public function set_defaults($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->set_defaults($k, $v);
            }
        } else {
            $this->defaults[$key] = $value;
        }
    }

    public function prepare_params(&$params)
    {
        return $this;
    }

    /**
     * @param Cartthrob_core $core
     */
    public function set_core($core)
    {
        $this->core = $core;
    }

    public function subclass()
    {
        static $subclass;

        if (is_null($subclass)) {
            $subclass = false;

            if ($parent_class = $this->parent_class()) {
                $subclass = substr(Cartthrob_core::get_class($this), strlen($parent_class) + 1);
            }
        }

        return $subclass;
    }
}
