<?php

if (!defined('CARTTHROB_PATH')) {
    Cartthrob_core::core_error('No direct script access allowed');
}

abstract class Cartthrob_price extends Cartthrob_child
{
    public $title = '';
    public $settings = [];
    public $data = [];
    public $classname = '';
    public $type = '';
    public $markup = false;

    public function __construct()
    {
        $this->classname = get_class($this);
        $this->type = preg_replace('/^Cartthrob_/', '', $this->classname);
    }

    public function data($key)
    {
        return (isset($this->data[$key])) ? $this->data[$key] : false;
    }

    public function adjust_price($price)
    {
        return $price;
    }
}
