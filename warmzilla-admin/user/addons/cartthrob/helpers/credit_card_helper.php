<?php

if (!function_exists('card_type')) {
    /**
     * card_type
     *
     * @param string $ccn
     * @return string credit card type, ex. amex, visa, mc, discover
     */
    function card_type($ccn = null)
    {
        $cc = str_replace(' ', '', $ccn);

        $cctype = 'Unknown Card Type';

        $length = strlen($cc);
        if ($length == 15 && substr($length, 0, 1) == '3') {
            $cctype = 'amex';
        } elseif ($length == 16 && substr($length, 0, 1) == '6') {
            $cctype = 'discover';
        } elseif ($length == 16 && substr($length, 0, 1) == '5') {
            $cctype = 'mc';
        } elseif (($length == 16 || $length == 13) && substr($cc, 0, 1) == '4') {
            $cctype = 'visa';
        }

        return $cctype;
    }
}

if (!function_exists('modulus_10_check')) {
    /**
     * modulus 10 check
     *
     * a modulus 10 checker
     * @param string $ccn credit card number
     * @return bool (true if number is good)
     *
     **/
    /* This takes each digit, from right to left and multiplies each second
    digit by two. If the multiple is two-digits long (i.e.: 6 * 2 = 12) the two digits of
    the multiple are then added together for a new number (1 + 2 = 3). You then add up the
    string of numbers, both unaltered and new values and get a total sum. This sum is then
    divided by 10 and the remainder should be zero if it is a valid credit card.
    */
    function modulus_10_check($ccn)
    {
        $cc = str_replace(' ', '', $ccn);
        $char_array = str_split($cc);
        $digit_count = sizeof($char_array);
        $double = [];

        $j = 0;
        for ($i = ($digit_count - 2); $i >= 0; $i -= 2) {
            $double[$j] = $char_array[$i] * 2;
            $j++;
        }
        $size_of_double = sizeof($double);
        $num_for_validation = 0;

        for ($i = 0; $i < $size_of_double; $i++) {
            $double_count = str_split($double[$i]);
            for ($j = 0; $j < sizeof($double_count); $j++) {
                $num_for_validation += $double_count[$j];
            }
            $double_count = '';
        }

        for ($i = ($digit_count - 1); $i >= 0; $i -= 2) {
            $num_for_validation += $char_array[$i];
        }

        if (substr($num_for_validation, -1, 1) == '0') {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('sanitize_credit_card_number')) {
    /**
     * Strips all non-numeric formatting from a string
     *
     * @param string $credit_card_number
     * @return int|string
     */
    function sanitize_credit_card_number($credit_card_number = null)
    {
        if (!$credit_card_number) {
            return '';
        }

        return (string)preg_replace('/[^0-9]/', '', $credit_card_number);
    }//END
}

if (!function_exists('validate_credit_card')) {
    /**
     * validate_credit_card
     *
     * @param string $ccn this is the credit card number to verify
     * @param string $stated_card_type . Matches the stated card type against the actual card type requirements
     * @return array 'valid' (bool), 'card_type' (string), error_code (int)
     *               error_code 1: card type not found.
     *               error_code 2: card type mismatch
     *               error_code 3: invalid card number
     *               error_code 4: incorrect number length for card type
     */
    function validate_credit_card($ccn, $stated_card_type = null)
    {
        // cleaning the number
        $credit_card_number = sanitize_credit_card_number($ccn);

        // setting response data defaults
        $response = [
            'valid' => false,
            'card_type' => null,
            'error_code' => null,
        ];

        // information about credit cards,
        // ordered roughly by popularity to reduce processor usage (common cards are frontloaded)
        $credit_cards = [
            'visa' => [
                'name' => 'Visa',
                'length' => [13, 16],
                'prefix' => [4],
                'mod10' => true,
            ],
            'amex' => [
                'name' => 'AmericanExpress',
                'length' => [15],
                'prefix' => [34, 37],
                'mod10' => true,
            ],
            'discover' => [
                'name' => 'Discover',
                'length' => [16],
                'prefix' => [6011, 622, 64, 65],
                'mod10' => true,
            ],
            'mc' => [
                'name' => 'MasterCard',
                'length' => [16],
                'prefix' => [51, 52, 53, 54, 55],
                'mod10' => true,
            ],
            'diners' => [
                'name' => 'DinersClub',
                'length' => [14, 16],
                'prefix' => [305, 36, 38, 54, 55],
                'mod10' => true,
            ],
            'jcb' => [
                'name' => 'JCB',
                'length' => [16],
                'prefix' => [35],
                'mod10' => true,
            ],
            'laser' => [
                'name' => 'Laser',
                'length' => [16, 17, 18, 19],
                'prefix' => [6304, 6706, 6771, 6709],
                'mod10' => true,
            ],
            'maestro' => [
                'name' => 'Maestro',
                'length' => [12, 13, 14, 15, 16, 18, 19],
                'prefix' => [5018, 5020, 5038, 6304, 6759, 6761],
                'mod10' => true,
            ],
            'solo' => [
                'name' => 'Solo',
                'length' => [16, 18, 19],
                'prefix' => [6334, 6767],
                'mod10' => true,
            ],
            'switch' => [
                'name' => 'Switch',
                'length' => [16, 18, 19],
                'prefix' => [4903, 4905, 4911, 4936, 564182, 633110, 6333, 6759],
                'mod10' => true,
            ],
            'carteblanche' => [
                'name' => 'CarteBlanche',
                'length' => [14],
                'prefix' => [300, 301, 302, 303, 304, 305],
                'mod10' => true,
            ],
            'electron' => [
                'name' => 'VisaElectron',
                'length' => [16],
                'prefix' => [417500, 4917, 4913, 4508, 4844],
                'mod10' => true,
            ],
            'enroute' => [
                'name' => 'enRoute',
                'length' => [15],
                'prefix' => [2014, 2149],
                'mod10' => true,
            ],
        ];
        // finding the type of card by its ccnumber
        foreach ($credit_cards as $key => $card_data) {
            foreach ($card_data['prefix'] as $prefix) {
                if (strpos($credit_card_number, $prefix) === 0) {
                    $response['card_type'] = $key;
                    break 2;
                }
            }
        }
        // checking to see if the type's set now that we've examined the card number
        if (!$response['card_type']) {
            // ERROR card not found
            $response['error_code'] = 1;
        }
        // checking to see if we expect the credit card to be of a certain type
        if ($stated_card_type != null && $stated_card_type != $response['card_type']) {
            // ERROR card type mismatch.
            $response['error_code'] = 2;
        }
        // checking mod10
        if ($credit_cards[$response['card_type']]['mod10']) {
            if (!modulus_10_check($credit_card_number)) {
                // ERROR mod 10 problem.
                $response['error_code'] = 3;
            }
        }
        // checking card length
        $card_length = strlen($credit_card_number);
        $match = false;
        foreach ($credit_cards[$response['card_type']]['length'] as $expected_length) {
            if ($card_length == $expected_length) {
                $match = true;
                break;
            }
        }
        if (!$match) {
            // ERROR card length error
            $response['error_code'] = 4;
        }

        if (empty($response['error_code']) && $response['card_type']) {
            $response['valid'] = true;
        }

        return $response;
    }
}
