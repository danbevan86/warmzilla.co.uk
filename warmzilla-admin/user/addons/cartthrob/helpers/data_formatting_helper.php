<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('set')) {
    /**
     * @return mixed
     */
    function set()
    {
        $args = func_get_args();
        foreach ($args as $arg) {
            if ($arg) {
                return $arg;
            }
        }

        return end($args);
    }
}

if (!function_exists('cartesian')) {
    /**
     * @param $input
     * @return array
     */
    function cartesian($input)
    {
        $result = [];

        while (list($key, $values) = each($input)) {
            // If is empty, skip it.
            if (empty($values)) {
                continue;
            }
            if (empty($result)) {
                if (!is_array($values) && $values !== false && $values !== null && $values != '') {
                    $result[] = [$key => $values];
                } else {
                    foreach ($values as $value) {
                        if ($value !== '' && $value !== null && $value !== false) {
                            $result[] = [$key => $value];
                        } else {
                        }
                    }
                }
            } else {
                $append = [];

                foreach ($result as &$product) {
                    if (is_array($values)) {
                        $product[$key] = array_shift($values);
                        $copy = $product;
                        foreach ($values as $item) {
                            if ($item !== '' && $item !== null && $item !== false) {
                                $copy[$key] = $item;
                                $append[] = $copy;
                            } else {
                            }
                        }

                        array_unshift($values, $product[$key]);
                    }
                }

                if ($append) {
                    $result = array_merge($result, $append);
                }
            }
        }

        return $result;
    }
}

if (!function_exists('cartesian_to_price')) {
    /**
     * @param $input
     * @return array
     */
    function cartesian_to_price($input)
    {
        $prices = [];
        foreach ($input as $key => $value) {
            $prices[$key] = 0;
            foreach ($value as $k => $price) {
                $price = trim($price);
                $price += 0; // cast as number;
                $prices[$key] += trim($price);
            }
        }

        return $prices;
    }
}

if (!function_exists('sanitize_number')) {
    /**
     * Removes all non-numeric, non-decimal formatting from a string
     *
     * @param string $number
     * @param bool $allow_negative
     * @return float|string|int
     */
    function sanitize_number($number = null, $allow_negative = false)
    {
        if (is_int($number) || is_float($number) || ctype_digit($number)) {
            return $number;
        }

        if (!$number) {
            return 0;
        }

        $prefix = ($allow_negative && preg_match('/^-/', $number)) ? '-' : '';
        // @TODO should probably figure out how to check and see if this number was formatted Euro-style with commas replacing decimal points
        $number = preg_replace('/[^0-9\.]/', '', $number);

        // changed so that '' won't be returned
        if (is_numeric($number) || is_int($number) || is_float($number) || ctype_digit($number)) {
            return $prefix . $number;
        } else {
            return 0;
        }
    }
}

if (!function_exists('_array_merge')) {
    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    function _array_merge($a, $b)
    {
        foreach ($b as $key => $value) {
            if (is_array($value) && isset($a[$key])) {
                $a[$key] = @_array_merge($a[$key], $value);
            } else {
                $a[$key] = $value;
            }
        }

        return $a;
    }
}

if (!function_exists('array_key_prefix')) {
    /**
     * @param array $array
     * @param string $prefix
     * @return array
     */
    function array_key_prefix(array $array, $prefix = '')
    {
        $return = [];

        foreach ($array as $key => $value) {
            $return[$prefix . $key] = $value;
        }

        return $return;
    }
}

if (!function_exists('array_value')) {
    /**
     * Get a value nested in a multi-dimensional array
     *
     * @param $array
     * @return mixed
     */
    function array_value($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $args = func_get_args();

        array_shift($args);

        foreach ($args as $key) {
            if (isset($array[$key])) {
                $array = $array[$key];
            } else {
                return false;
            }
        }

        return $array;
    }
}

if (!function_exists('sanitize_credit_card_number')) {
    /**
     * Strips all non-numeric formatting from a string
     *
     * @param string $creditCardNumber
     * @return int|string
     */
    function sanitize_credit_card_number($creditCardNumber = null)
    {
        if (!$creditCardNumber) {
            return false;
        }

        $creditCardNumber = preg_replace('/[^0-9]/', '', $creditCardNumber);

        // SOMETIMES php_int_max is smaller than a CC number (32 bit systems)
        // ideally we want the CC number returned as an integer, but we'll return it as a string if we have to
        if (defined('PHP_INT_MAX') && $creditCardNumber <= PHP_INT_MAX) {
            $creditCardNumber = (int)$creditCardNumber;
        }

        return $creditCardNumber;
    }
}

if (!function_exists('textarea_to_array')) {
    /**
     * Converts a multi-line string to an array
     *
     * @param string $data textarea content
     * @return array
     */
    function textarea_to_array($data)
    {
        return preg_split('/[\r\n]+/', $data);
    }
}

if (!function_exists('param_string_to_array')) {
    /**
     * @param $string
     * @return array
     */
    function param_string_to_array($string)
    {
        $values = [];

        if ($string) {
            foreach (explode('|', $string) as $value) {
                if (strpos($value, ':') !== false) {
                    $value = explode(':', $value);

                    $values[$value[0]] = $value[1];
                } else {
                    $values[$value] = $value;
                }
            }
        }

        return $values;
    }
}

if (!function_exists('get_phone_number_array')) {
    /**
     * Returns an array of phone parts
     *
     * @param string $phone
     * @return string formatted string | array of number parts
     */
    function get_phone_number_array($phone)
    {
        if (!$phone) {
            return null;
        }
        $return = get_formatted_phone($phone);

        $output = '';
        if ($return['international']) {
            $output .= $return['international'] . '-';
        }
        if ($return['area_code']) {
            $output .= $return['area_code'] . '-';
        }
        if ($return['prefix']) {
            $output .= $return['prefix'] . '-';
        }
        if ($return['suffix']) {
            $output .= $return['suffix'];
        }

        return $output;
    }
}

if (!function_exists('get_formatted_phone')) {
    /**
     * @param $phone
     * @return mixed
     */
    function get_formatted_phone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        if (strlen($phone) == 7) {
            $phone = preg_replace('/([0-9]{3})([0-9]{4})/', '$1$2', $phone);
        } elseif (strlen($phone) == 10) {
            $phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1$2$3', $phone);
        }
        $return['international'] = '';
        $return['area_code'] = '';
        $return['prefix'] = '';
        $return['suffix'] = '';

        if (strlen($phone) > 10) {
            $return['international'] = substr($phone, 0, -10);
        }
        if (strlen($phone) >= 10) {
            $return['area_code'] = substr($phone, -10, 3);
        }
        if (strlen($phone) >= 7) {
            $return['prefix'] = substr($phone, -7, 3);
        }
        if (strlen($phone) > 4) {
            $return['suffix'] = substr($phone, -4, 4);
        }

        return $return;
    }
}

if (!function_exists('response_xml_array')) {
    /**
     * @param $xml
     * @return array
     */
    function response_xml_array($xml)
    {
        $xml_array = [];
        $node_chars = '/<(\w+)\s*([^\/>]*)\s*(?:\/>|>(.*)<\/\s*\\1\s*>)/s';

        preg_match_all($node_chars, $xml, $elements);

        foreach ($elements[1] as $key => $value) {
            if ($elements[3][$key]) {
                $xml_array[$elements[1][$key]] = $elements[3][$key];
            }
        }

        return $xml_array;
    }
}

if (!function_exists('xml_to_array')) {
    /**
     * This converts xml to an array. The default will only output
     * one child node at a time. For our purposes this is generally fine,
     * most of the xml returned from gateway processes do not contain
     * multiple child nodes at the same level.
     *
     * @param string $xml
     * @param string $buildType
     * @return array
     */
    function xml_to_array($xml, $buildType = 'basic')
    {
        $values = [];
        $index = [];
        $data = [];
        $count = 0;
        $parser = xml_parser_create();

        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $xml, $values, $index);
        xml_parser_free($parser);

        $name = $values[$count]['tag'];

        $data[$name] = isset($values[$count]['attributes']) ? $values[$count]['attributes'] : '';
        $data[$name] = _build_array($values, $count, $buildType);

        return $data;
    }
}

if (!function_exists('_build_array')) {
    /**
     * recursively builds array out of xml
     * set the build type as "complete" and this will build a complete array
     * even in cases where there are multiple child nodes at the same level.
     * The default will only output one child node at a time. For our purposes
     * this is generally fine, most of the xml returned from gateway processes
     * do not contain multiple child nodes at the same level.
     *
     * @param string $xml_data
     * @param string $count
     * @param string $build_type basic / complete
     * @return array
     */
    function _build_array($xml_data, &$count, $build_type = 'basic')
    {
        $child = [];

        if (isset($xml_data[$count]['value'])) {
            array_push($child, $xml_data[$count]['value']);
        }
        if ($count == 0) {
            $name = @$xml_data[0]['tag'];

            if (!empty($xml_data[0]['attributes'])) {
                foreach ($xml_data[0]['attributes'] as $key => $value) {
                    $child[$key] = $value;
                }
            }
        }

        while ($count++ < count($xml_data)) {
            switch ($xml_data[$count]['type']) {
                case 'cdata':
                    @array_push($child, $xml_data[$count]['value']);
                    break;
                case 'complete':
                    $name = $xml_data[$count]['tag'];
                    if (!empty($name)) {
                        if (isset($xml_data[$count]['value'])) {
                            if ($build_type == 'complete') {
                                $child[$name][]['data'] = $xml_data[$count]['value'];
                            } else {
                                $child[$name]['data'] = $xml_data[$count]['value'];
                            }
                        } else {
                            $child[$name] = '';
                        }
                        if (isset($xml_data[$count]['attributes'])) {
                            foreach ($xml_data[$count]['attributes'] as $key => $value) {
                                $curr = count($child[$name]);
                                if ($build_type == 'complete') {
                                    $child[$name][$curr - 1][$key] = $value;
                                } else {
                                    $child[$name][$key] = $value;
                                }
                            }
                        }
                        if (empty($new_count)) {
                            $new_count = 1;
                        } else {
                            $new_count++;
                        }
                    }
                    break;
                case 'open':
                    $name = $xml_data[$count]['tag'];
                    if (isset($child[$name])) {
                        $size = count($child[$name]);
                    } else {
                        $size = 0;
                    }
                    $child[$name][$size] = _build_array($xml_data, $count);
                    break;
                case 'close':
                    return $child;
                    break;
            }
        }

        return $child;
    }
}

if (!function_exists('url_string_to_array')) {
    /**
     * Converts a urlencoded string into an array.
     *
     * @param string $url_string URLencoded string to split
     * @param string $split_character
     * @return array
     */
    function url_string_to_array($url_string, $split_character = '&')
    {
        parse_str($url_string, $data);

        return $data;
    }
}

if (!function_exists('bool_string')) {
    /**
     * @param $string
     * @param bool $default
     * @return bool
     */
    function bool_string($string, $default = false)
    {
        switch (strtolower($string)) {
            case 'true':
            case 't':
            case 'yes':
            case 'y':
            case 'on':
            case '1':
                return true;
                break;
            case 'false':
            case 'f':
            case 'no':
            case 'n':
            case 'off':
            case '0':
                return false;
                break;
            default:
                return $default;
        }
    }
}

if (!function_exists('create_bool_string')) {
    /**
     * Give us a little more obscurity for our encrypted boolean form values
     *
     * @param bool $bool
     * @return string
     */
    function create_bool_string($bool = false)
    {
        switch (rand(1, 6)) {
            case 1:
                $string = ($bool) ? 'true' : 'false';
                break;
            case 2:
                $string = ($bool) ? 't' : 'f';
                break;
            case 3:
                $string = ($bool) ? 'yes' : 'no';
                break;
            case 4:
                $string = ($bool) ? 'y' : 'n';
                break;
            case 5:
                $string = ($bool) ? 'on' : 'off';
                break;
            case 6:
                $string = ($bool) ? '1' : '0';
                break;
        }

        $output = '';

        foreach (str_split($string) as $char) {
            $output .= (rand(0, 1)) ? $char : strtoupper($char);
        }

        return $output;
    }
}

if (!function_exists('_unserialize')) {
    /**
     * Unserialize data, and always return an array
     *
     * @param mixed $data
     * @param mixed $base64_decode = FALSE
     * @return array
     */
    function _unserialize($data, $base64_decode = false)
    {
        if (is_array($data)) {
            return $data;
        }

        if ($base64_decode) {
            $data = base64_decode($data);
        }

        if (false === ($data = @unserialize($data))) {
            return [];
        }

        return $data;
    }
}

if (!function_exists('split_url_string')) {
    /**
     * Converts a urlencoded string into an array.
     *
     * @param string $url_string URLencoded string to split
     * @param string $split_character
     * @return array
     */
    function split_url_string($url_string, $split_character = '&')
    {
        $array = explode($split_character, $url_string);
        $i = 0;
        while ($i < count($array)) {
            $b = explode('=', $array[$i], 2);

            if (!isset($b[1])) {
                $b[1] = '';
            }
            $no_space_key = rtrim(htmlspecialchars(urldecode($b[0])));
            $new_array[$no_space_key] = htmlspecialchars(urldecode($b[1]));
            $i++;
        }

        return $new_array;
    }
}

if (!function_exists('split_delimited_string')) {
    /**
     * Converts a pipe or comma delimited string into an array.
     *
     * @param string $string string to split
     * @param array|string $split_character the character(s) to split by
     * @return array
     **/
    function split_delimited_string($string, $split_character = [',', '|'], $trim = true)
    {
        if (is_array($split_character)) {
            $regex = '[' . preg_quote(implode($split_character)) . ']';
        } else {
            if (strlen($split_character) > 1) {
                $regex = $split_character;
            } else {
                $regex = '[' . preg_quote($split_character) . ']';
            }
        }

        $regex = ($trim) ? "/\s*{$regex}\s*/" : "/{$regex}/";

        if ($trim) {
            $string = trim($string);
        }

        return preg_split($regex, $string);
    }
}

if (!function_exists('convert_response_xml')) {
    /**
     * This converts xml nodes to array keys. This is really only useful for very small xml
     * responses and does not return attributes, only nodes and node names.
     *
     * @param string $xml xml to be converted to array
     * @return array
     **/
    function convert_response_xml($xml)
    {
        $xml_array = [];
        $node_chars = '/<(\w+)\s*([^\/>]*)\s*(?:\/>|>(.*)<\/\s*\\1\s*>)/s';

        preg_match_all($node_chars, $xml, $elements);

        foreach ($elements[1] as $key => $value) {
            if ($elements[3][$key]) {
                $xml_array[$elements[1][$key]] = $elements[3][$key];
            }
        }

        return $xml_array;
    }
}

if (!function_exists('convert_number_to_string')) {
    /**
     * @param $number
     * @return float|int|string
     */
    function convert_number_to_string($number)
    {
        if (($number < 0) || ($number > 999999999)) {
            return $number;
        }

        $Gn = floor($number / 1000000);  /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);     /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);      /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);       /* Tens (deca) */
        $n = $number % 10;               /* Ones */

        $res = '';

        if ($Gn) {
            $res .= convert_number($Gn) . ' Million';
        }

        if ($kn) {
            $res .= (empty($res) ? '' : ' ') . convert_number($kn) . ' Thousand';
        }

        if ($Hn) {
            $res .= (empty($res) ? '' : ' ') . convert_number($Hn) . ' Hundred';
        }

        $ones = [
            '',
            'One',
            'Two',
            'Three',
            'Four',
            'Five',
            'Six',
            'Seven',
            'Eight',
            'Nine',
            'Ten',
            'Eleven',
            'Twelve',
            'Thirteen',
            'Fourteen',
            'Fifteen',
            'Sixteen',
            'Seventeen',
            'Eightteen',
            'Nineteen',
        ];
        $tens = [
            '',
            '',
            'Twenty',
            'Thirty',
            'Fourty',
            'Fifty',
            'Sixty',
            'Seventy',
            'Eigthy',
            'Ninety',
        ];

        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= ' and ';
            }

            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];

                if ($n) {
                    $res .= '-' . $ones[$n];
                }
            }
        }

        if (empty($res)) {
            $res = 'zero';
        }

        return strtolower($res);
    }
}

if (!function_exists('convert_card_type')) {
    /**
     * @param $card_type_value
     * @param $return_type
     * @return string
     */
    function convert_card_type($card_type_value, $return_type)
    {
        switch (strtolower($card_type_value)) {
            case 'mastercard':
            case 'mc':
            case 'master card':
                if ($return_type == 'title') {
                    return 'Mastercard';
                } elseif ($return_type == 'camel') {
                    return 'MasterCard';
                } elseif ($return_type == 'abbreviate') {
                    return 'MC';
                } elseif ($return_type == 'single') {
                    return 'Mastercard';
                }
                break;
            case 'visa':
                return 'Visa';
                break;
            case 'discover':
                return 'Discover';
                break;
            case 'diners club':
            case 'dc':
            case 'diners':
            case 'dinersclub':
                if ($return_type == 'title') {
                    return 'Diners Club';
                } elseif ($return_type == 'camel') {
                    return 'DinersClub';
                } elseif ($return_type == 'abbreviate') {
                    return 'Diners';
                } elseif ($return_type == 'single') {
                    return 'Dinersclub';
                }
                break;
            case 'american express':
            case 'amex':
            case 'americanexpress':
                if ($return_type == 'title') {
                    return 'American Express';
                } elseif ($return_type == 'camel') {
                    return 'AmericanExpress';
                } elseif ($return_type == 'abbreviate') {
                    return 'amex';
                } elseif ($return_type == 'single') {
                    return 'Americanexpress';
                }
                break;
            case 'switch':
                return 'Switch';
            case 'laser':
                return 'Laser';
            case 'maestro':
                return 'Maestro';
            case 'solo':
                return 'Solo';
            case 'delta':
                return 'Delta';
            default:
                return $card_type_value;
        }

        return $card_type_value;
    }
}

if (!function_exists('arr')) {
    /**
     * Checks an array for a key, returns it if set, or NULL if not set.
     *
     * @param array $array
     * @param string $key
     * @return string|null
     */
    function arr($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        } else {
            return null;
        }
    }
}

if (!function_exists('e')) {
    /**
     * Escape HTML special characters in a string.
     *
     * @param string $value
     * @return string
     */
    function e($value)
    {
        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);
    }
}

if (!function_exists('year2')) {
    /**
     * Return a year in 2-digit format
     *
     * @param $year
     * @return string
     */
    function year2($year)
    {
        if (strlen($year > 2)) {
            return substr($year, -2);
        }

        return str_pad($year, 2, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('year4')) {
    /**
     * Return a year in 4-digit format
     *
     * @param $year
     * @return string
     */
    function year4($year)
    {
        $length = strlen($year);

        switch ($length) {
            case 3:
                return '2' . $year;
            case 2:
                return '20' . $year;
            case 1:
                return '200' . $year;
            case $length > 4:
                return substr($year, -4);
        }

        return $year;
    }
}

if (!function_exists('strip_punctuation')) {
    /**
     * @param $text
     * @return string|string[]|null
     */
    function strip_punctuation($text)
    {
        return preg_replace('/[^a-zA-Z0-9\s\-_]/', ' ', $text);
    }
}
