<?php

use CartThrob\Transactions\TransactionState;
use Illuminate\Support\Arr;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property CI_Controller     $EE
 * @property Cartthrob_core_ee $cartthrob;
 * @property Cartthrob_cart    $cart
 * @property Cartthrob_store   $store
 */
class Cartthrob
{
    /**
     * @var int
     */
    const ASYNC_METHOD_DISABLED = 0;

    /**
     * @var int
     */
    const ASYNC_METHOD_HTTP = 1;

    /**
     * @var int
     */
    const ASYNC_METHOD_CRON = 2;

    public $cartthrob;
    public $store;
    public $cart;

    public function __construct()
    {
        ee()->load->library('cartthrob_loader');

        ee()->cartthrob_loader->setup($this);

        ee()->lang->loadfile('cartthrob');
        ee()->load->helper(['security', 'countries', 'data_formatting', 'credit_card', 'form', 'template']);
        ee()->load->library(['cartthrob_variables', 'template_helper']);
        ee()->load->model('product_model');

        ee()->product_model->load_products(ee()->cartthrob->cart->product_ids());

        ee()->load->helper('debug');
    }

    /**
     * @param $method
     * @param $args
     * @throws Exception
     * @return mixed
     */
    public function __call($method, $args)
    {
        ee()->load->library('cartthrob_addons');

        if (!ee()->cartthrob_addons->method_exists($method)) {
            throw new Exception("Call to undefined method Cartthrob::$method()");
        }

        return ee()->cartthrob_addons->call($method);
    }

    public function convert_simple_commerce_purchases()
    {
        ee()->load->model('order_model');
        ee()->load->model('cartthrob_entries_model');
        ee()->load->model('purchased_items_model');
        ee()->load->library('get_settings');

        $lastId = null;
        $data = ee()->db
            ->where('site_id', ee()->config->item('site_id'))
            ->where('`key`', 'last_simple_commerce_transaction')
            ->limit('1')
            ->get('cartthrob_settings')
            ->result_array();

        if (!empty($data[0])) {
            $z = $data[0]['value'];
            $orig_last_id = $z;
            $lastId = $orig_last_id;
            echo $lastId;
        }

        if ($lastId) {
            ee()->db->where('purchase_id > ' . $lastId);
        }

        $query = ee()->db->where('CHAR_LENGTH(`txn_id`) > 16')
            ->limit(200)
            ->order_by('purchase_id', 'asc')
            ->get('exp_simple_commerce_purchases');

        if ($query->result() && $query->num_rows()) {
            foreach ($query->result_array() as $q) {
                $lastId = $q['purchase_id'];
                $total = $q['item_cost'];

                $orderArray = [
                    'CVV2' => null,
                    'expiration_month' => null,
                    'expiration_year' => null,
                    'items' => [],
                    'card_type' => null,
                    'shipping' => null,
                    'shipping_plus_tax' => null,
                    'tax' => null,
                    'subtotal' => ee()->cartthrob->round($total),
                    'subtotal_plus_tax' => null,
                    'discount' => null,
                    'total' => ee()->cartthrob->round($total),
                    'customer_name' => null,
                    'customer_email' => null,
                    'email_address' => null,
                    'customer_ip_address' => null,
                    'ip_address' => null,
                    'customer_phone' => null,
                    'coupon_codes' => null,
                    'coupon_codes_array' => null,
                    'last_four_digits' => null,
                    'full_billing_address' => null,
                    null,
                    null,
                    'full_shipping_address' => null,
                    null,
                    null,
                    null,
                    null,
                    'billing_first_name' => null,
                    'billing_last_name' => null,
                    'billing_company' => null,
                    'billing_address' => null,
                    'billing_address2' => null,
                    'billing_city' => null,
                    'billing_state' => null,
                    'billing_zip' => null,
                    'billing_country' => null,
                    'billing_country_code' => null,
                    null,
                    'first_name' => null,
                    'last_name' => null,
                    'company' => null,
                    'address' => null,
                    'address2' => null,
                    'city' => null,
                    'state' => null,
                    'zip' => null,
                    'country' => null,
                    'country_code' => null,
                    null,
                    'shipping_first_name' => null,
                    'shipping_last_name' => null,
                    'shipping_company' => null,
                    'shipping_address' => null,
                    'shipping_address2' => null,
                    'shipping_city' => null,
                    'shipping_state' => null,
                    'shipping_zip' => null,
                    'shipping_country' => null,
                    'shipping_country_code' => null,
                    null,
                    'currency_code' => null,
                    'entry_id' => '',
                    'order_id' => '',
                    'total_cart' => ee()->cartthrob->round($total),
                    'auth' => [],
                    'purchased_items' => [],
                    'create_user' => false,
                    'member_id' => $q['member_id'],
                    'group_id' => false,
                    'return' => '',
                    'site_name' => ee()->config->item('site_name'),
                    'custom_data' => [],
                    'subscription' => null,
                    'subscription_options' => [],
                    'payment_gateway' => 'offline_payments',
                    'subscription_id' => null,
                    'entry_date' => $q['purchase_date'],
                    'transaction_id' => $q['txn_id'],
                    'title' => 'Conversion ' . $lastId,
                ];

                $title = 'Item ID ' . $q['item_id'];
                $entryId = null;

                $query2 = ee()->db->where('item_id', $q['item_id'])
                    ->limit(1)
                    ->get('exp_simple_commerce_items');

                if ($query2->result() && $query2->num_rows()) {
                    $d = $query2->row_array();
                    $entryId = $d['entry_id'];

                    if ($entryId) {
                        $entryData = ee()->cartthrob_entries_model->entry($entryId);

                        if (!empty($entryData['title'])) {
                            $title = $entryData['title'];
                        }
                    }
                }

                $orderArray['items'][] = [
                    'entry_id' => $entryId,
                    'title' => $title,
                    'quantity' => 1,
                    'price' => ee()->cartthrob->round($total),
                    'price_plus_tax' => ee()->cartthrob->round($total),
                    'weight' => 0,
                    'shipping' => 0,
                ];

                // other custom fields
                // $order_data['bl_order_price'] = ee()->cartthrob->round($total);
                // $order_data['bl_order_qty'] =  1;
                // $order_data['bl_order_id'] =  $last_id;
                //
                echo '<pre>' . 'aaa';
                print_r($orderArray);
                die;
                $order = ee()->order_model->create_order($orderArray);
                ee()->cartthrob->cart->save();

                $row['order_id'] = $order['entry_id'];
                echo 'new order_id: (' . $lastId . ') ' . $row['order_id'] . '<br>';

                ee()->db->where('site_id', ee()->config->item('site_id'));
                ee()->db->where('`key`', 'last_order_number');
                ee()->db->limit('1');
                $data = ee()->db->get('cartthrob_settings')->result_array();
                echo 'last order ' .
                    var_dump($data);
                echo '<br><br>';

                ee()->order_model->update_order($row['order_id'], [
                    'status' => 'open',
                    'title' => $orderArray['title'],
                    'url_title' => 'conversion_' . $lastId . '_' . uniqid(null, true),
                ]);
                ee()->db->insert('cartthrob_order_items', $row);

                $row['product_id'] = $entryId;
                $row['author_id'] = $orderArray['member_id'];
                $purch_id = ee()->purchased_items_model->create_purchased_item($row, $row['order_id'],
                    ee()->cartthrob->store->config('purchased_items_default_status'));
                ee()->purchased_items_model->update_purchased_item($purch_id,
                    ['author_id' => $orderArray['member_id']]);
            }
            $where = [
                'site_id' => ee()->config->item('site_id'),
                '`key`' => 'last_simple_commerce_transaction',
            ];

            if (isset($orig_last_id)) {
                $temp['serialized'] = 0;
                $temp['value'] = $lastId;

                ee()->db->update('cartthrob_settings', $temp, $where);
            } else {
                $temp['serialized'] = 0;
                $temp['value'] = $lastId;

                ee()->db->insert('cartthrob_settings', array_merge($temp, $where));
            }
            echo $lastId;
            //			ee()->cartthrob->cart->set_meta('simple_id', $last_id);
//			ee()->cartthrob->cart->save();
        }
    }

    public function delete_from_cart_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        if (ee()->extensions->active_hook('cartthrob_delete_from_cart_start') === true) {
            ee()->extensions->call('cartthrob_delete_from_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->load->library('form_builder');

        if (!ee()->form_builder->validate()) {
            return ee()->form_builder->action_complete();
        }

        ee()->cartthrob->save_customer_info();

        if (ee()->input->post('row_id') !== false) {
            ee()->cartthrob->cart->remove_item(ee()->input->post('row_id', true));
        }

        if (ee()->extensions->active_hook('cartthrob_delete_from_cart_end') === true) {
            ee()->extensions->call('cartthrob_delete_from_cart_end');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->form_builder->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    public function field()
    {
        // @TODO make this field figure out the field type, and make that field type handle the output.

        $entry_id = ee()->TMPL->fetch_param('entry_id');
        $field = ee()->TMPL->fetch_param('field');

        ee()->load->model('cartthrob_entries_model');

        $entry = ee()->cartthrob_entries_model->entry(ee()->TMPL->fetch_param('entry_id'));

        ee()->load->helper('array');

        return element(ee()->TMPL->fetch_param('field'), $entry);
    }

    public function download_file_action()
    {
        // @TODO add in debug to output member and group id, and whether the file's protected or not
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        // cartthrob_download_start hook
        if (ee()->extensions->active_hook('cartthrob_download_start') === true) {
            // @TODO work on hook parameters
            // $edata = $EXT->universal_call_extension('cartthrob_download_start');
            ee()->extensions->call('cartthrob_download_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->load->library('form_builder');
        ee()->load->library('cartthrob_file');
        ee()->load->library('curl');
        ee()->load->library('paths');
        ee()->load->helper(['string']);

        ee()->form_builder->set_require_form_hash(false);
        ee()->form_builder->set_require_rules(false);
        ee()->form_builder->set_require_errors(false);

        $path = null;

        if (!ee()->input->get('FP') && !ee()->input->get('FI')) {
            if (!ee()->form_builder->validate()) {
                return ee()->form_builder->action_complete();
            }
        }

        ee()->cartthrob->save_customer_info();

        // Check member id.
        if (ee()->input->get_post('MI') == true) {
            // have to check for get or post due to slightly different encoding types
            if (ee()->input->get('MI')) {
                $member_id = sanitize_number(ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->input->get('MI'))))));
            } else {
                $member_id = sanitize_number(ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('MI'))));
            }
        }

        // Check group id.
        if (ee()->input->get_post('GI')) {
            // have to check for get or post due to slightly different encoding types
            if (ee()->input->get('GI')) {
                $group_id = sanitize_number(ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->input->get('GI'))))));
            } else {
                $group_id = sanitize_number(ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('GI'))));
            }
        }
        // standard file from form, or free_file from download link
        if (ee()->input->get_post('FI')) {
            // have to check for get or post due to slightly different encoding types
            if (ee()->input->get('FI')) {
                $path = ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->input->get('FI')))));
            } else {
                $path = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('FI')));
            }

            if (substr($path, 0, 2) !== 'FI') {
                ee()->form_builder->add_error(ee()->lang->line('download_file_not_authorized'));
            } else {
                $path = substr($path, 2);
            }
        } // protected file from the download link
        elseif (ee()->input->get_post('FP')) {
            if (ee()->input->get('FP')) {
                $path = ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->input->get('FP')))));
            } else {
                $path = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('FP')));
            }

            if (substr($path, 0, 2) !== 'FP') {
                ee()->form_builder->add_error(ee()->lang->line('download_file_not_authorized'));
            } else {
                $path = substr($path, 2);
            }

            if (empty($member_id) && empty($group_id)) {
                ee()->form_builder->add_error(ee()->lang->line('download_file_not_authorized'));
            }
        } else {
            ee()->form_builder->add_error(ee()->lang->line('download_url_not_specified'));
        }

        if (ee()->form_builder->errors()) {
            ee()->form_builder->action_complete();
        }

        // Check member id.
        if (!empty($member_id) && $member_id != ee()->session->userdata('member_id')) {
            ee()->form_builder->add_error(ee()->lang->line('download_file_not_authorized_for_member'));
        }

        // Check group id
        if (!empty($group_id) && $group_id != ee()->session->userdata('group_id')) {
            ee()->form_builder->add_error(ee()->lang->line('download_file_not_authorized_for_group'));
        }

        // cartthrob_download_end hook
        if (ee()->extensions->active_hook('cartthrob_download_end') === true) {
            // @TODO work on hook parameters
            // $edata = $EXT->universal_call_extension('cartthrob_download_end', $path);
            $path = ee()->extensions->call('cartthrob_download_end', $path);
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        if (!ee()->form_builder->errors()) {
            ee()->cartthrob_file->force_download($path, ee()->input->get('debug'));

            if (ee()->cartthrob_file->errors()) {
                ee()->form_builder->add_error(ee()->cartthrob_file->errors());
            }
        }

        ee()->form_builder->action_complete();
    }

    /**
     * Add to Cart Action
     */
    public function add_to_cart_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->load->library('form_builder');
        ee()->load->model(['cartthrob_field_model', 'product_model']);

        // cartthrob_add_to_cart_start hook
        if (ee()->extensions->active_hook('cartthrob_add_to_cart_start') === true) {
            // @TODO work on hook parameters
            // $edata = $EXT->universal_call_extension('cartthrob_add_to_cart_start', $this, $_SESSION['cartthrob']);
            ee()->extensions->call('cartthrob_add_to_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        if (!ee()->form_builder->validate()) {
            ee()->cartthrob_variables->set_global_values();

            ee()->form_builder->set_value([
                'item_options',
                'quantity',
                'title',
            ]);

            return ee()->form_builder->action_complete();
        }

        ee()->cartthrob->save_customer_info();

        $data = [
            'entry_id' => ee()->input->post('entry_id', true),
            'class' => 'product',
        ];

        $item_options = ee()->input->post('item_options', true);

        if ($item_options && is_array($item_options)) {
            $configuration = [];
            $fields_list = [];
            foreach ($item_options as $key => $value) {
                if (strpos($key, 'configuration:') !== false) {
                    list($a, $field, $option_group) = explode(':', $key);
                    $fields_list[] = $field;
                    $configuration[$field][$option_group] = $value;

                    //					unset($item_options[$key]);
                    $data['item_options'][$key] = $value;
                }
            }

            if (!empty($configuration)) {
                $fields_list = array_filter($fields_list);
                foreach ($fields_list as $field_name) {
                    $entry = ee()->product_model->get_product($data['entry_id']);
                    if ($entry) {
                        $sku = ee()->product_model->get_base_variation($data['entry_id'], $field,
                            $configuration[$field_name]);
                        if ($sku) {
                            $item_options[$field_name] = $sku;

                            $inventory = ee()->product_model->check_inventory($data['entry_id'], $field, $sku);

                            if ($inventory !== false && $inventory <= 0) {
                                ee()->form_builder->set_errors([
                                    sprintf(lang('configuration_not_in_stock'), $entry['title']),
                                ])
                                    ->set_success_callback([ee()->cartthrob, 'action_complete'])
                                    ->action_complete();
                            }
                        }
                    }
                }
            }

            // don't grab numeric item_options, those are for sub_items
            foreach ($item_options as $key => $value) {
                if (strpos($key, ':') === false) {
                    $data['item_options'][$key] = $value;
                }
            }
        }

        // normally all of this data would be handled as part of the subs options, but we
        // have to mess around and get a price in the cart anyway, so we'll go ahead and look into it.
        if (ee()->input->post('subscription_plan_id') || ee()->input->post('PI')) {
            // look to see if this was overridden by customer, and then look for the parameter
            $plan_id = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('subscription_plan_id')));
            if (!$plan_id) {
                $plan_id = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('PI')));
            }

            ee()->load->model('subscription_model');
            $plan_data = ee()->subscription_model->get_plan($plan_id);

            if (isset($plan_data['permissions'])) {
                $perms = @unserialize(base64_decode($plan_data['permissions']));

                if (is_array($perms)) {
                    $data['permissions'] = $plan_data['permissions'] = implode('|', $perms);
                } else {
                    $data['permissions'] = $plan_data['permissions'] = $perms;
                }
            }

            if (!empty($plan_data['trial_price']) || $plan_data['trial_price'] === '0') {
                $data['price'] = $plan_data['trial_price'];
            } elseif (!empty($plan_data['price'])) {
                $data['price'] = $plan_data['price'];
            }

            if (isset($plan_data['name'])) {
                $data['title'] = $plan_data['name'];
            }
        }

        if (ee()->input->post('AUP') && ee()->input->post('price') !== false && bool_string(ee('Encrypt')->decode(ee()->input->post('AUP')))) {
            $data['price'] = sanitize_number(ee()->input->post('price', true));
        }

        if (ee()->input->post('PR')) {
            $PR = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('PR')));

            if ($PR == sanitize_number($PR)) {
                $data['price'] = $PR;
            }
        }

        if (ee()->input->post('WGT')) {
            $WGT = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('WGT')));

            if ($WGT == sanitize_number($WGT)) {
                $data['weight'] = $WGT;
            }
        } elseif (ee()->input->post('AUW') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUW'))) && ee()->input->post('weight') !== false) {
            $data['weight'] = ee()->input->post('weight', true);
        }

        if (ee()->input->post('SHP')) {
            $SHP = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('SHP')));

            if ($SHP == sanitize_number($SHP)) {
                $data['shipping'] = $SHP;
            }
        } elseif (ee()->input->post('AUS') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUS'))) && ee()->input->post('shipping') !== false) {
            $data['shipping'] = ee()->input->post('shipping', true);
        }

        if (ee()->input->post('NSH')) {
            $data['no_shipping'] = bool_string(ee('Encrypt')->decode(ee()->input->post('NSH')));
        }

        if (ee()->input->post('NTX')) {
            $data['no_tax'] = bool_string(ee('Encrypt')->decode(ee()->input->post('NTX')));
        }

        $data['product_id'] = $data['entry_id'];

        if (ee()->input->post('quantity')) {
            $data['quantity'] = ee()->input->post('quantity', true);
        }

        if (ee()->input->post('title')) {
            $data['title'] = ee()->input->post('title', true);
        }

        if (!empty($_FILES['userfile'])) {
            ee()->load->library('cartthrob_file');

            $directory = null;
            if (ee()->input->post('UPL')) {
                $directory = ee('Encrypt')->decode(ee()->input->post('UPL'));
            }

            $file_data = ee()->cartthrob_file->upload($directory);

            if (ee()->cartthrob_file->errors()) {
                ee()->form_builder->set_errors(ee()->cartthrob_file->errors())->action_complete();
            } else {
                $data['item_options']['upload'] = $file_data['file_name'];
                $data['item_options']['upload_directory'] = $file_data['file_path'];
            }
        }

        // if it's not on_the_fly, it's a product-based item
        if (!ee()->input->post('OTF') || !bool_string(ee('Encrypt')->decode(ee()->input->post('OTF')))) {
            if (ee()->input->post('title')) {
                $data['title'] = ee()->input->post('title', true);
            }
            $data['site_id'] = 1;
            if ($entry = ee()->product_model->get_product($data['entry_id'])) {
                if (isset($entry['site_id'])) {
                    $data['site_id'] = $entry['site_id'];
                }
                $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($entry['channel_id'], 'cartthrob_package', true);

                if ($field_id && !empty($entry['field_id_' . $field_id])) {
                    // it's a package
                    $data['class'] = 'package';

                    ee()->load->library('api');

                    ee()->legacy_api->instantiate('channel_fields');

                    if (empty(ee()->api_channel_fields->field_types)) {
                        ee()->api_channel_fields->fetch_installed_fieldtypes();
                    }

                    $data['sub_items'] = [];

                    if (ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                        $field_data = ee()->api_channel_fields->apply('pre_process',
                            [$entry['field_id_' . $field_id]]);

                        foreach ($field_data as $row_id => $row) {
                            $item = [
                                'entry_id' => $row['entry_id'],
                                'product_id' => $row['entry_id'],
                                'row_id' => $row_id,
                                'class' => 'product',
                                'site_id' => $data['site_id'],
                                // assuming it has to be from the same site id as the parent based on EE's structure
                            ];

                            $item['item_options'] = (isset($row['option_presets'])) ? $row['option_presets'] : [];

                            $row_item_options = [];

                            if (isset($_POST['item_options'][$row_id])) {
                                $row_item_options = $_POST['item_options'][$row_id];
                            } else {
                                if (isset($_POST['item_options'][$data['entry_id'] . ':' . $row_id . ':'])) {
                                    $row_item_options = $_POST['item_options'][$data['entry_id'] . ':' . $row_id . ':'];
                                } else {
                                    if (isset($_POST['item_options'][':' . $row_id])) {
                                        $row_item_options = $_POST['item_options'][':' . $row_id];
                                    }
                                }
                            }

                            $price_modifiers = ee()->product_model->get_all_price_modifiers($row['entry_id']);

                            foreach ($row_item_options as $key => $value) {
                                // if it's not a price modifier (ie, an on-the-fly item option), add it
                                // if it is a price modifier, check that it's been allowed before adding
                                if (!isset($price_modifiers[$key]) || !empty($row['allow_selection'][$key])) {
                                    $item['item_options'][$key] = ee('Security/XSS')->clean($value);
                                }
                            }

                            $data['sub_items'][$row_id] = $item;
                        }
                    }
                }

                $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($entry['channel_id'], 'cartthrob_subscriptions', true);

                if ($field_id && !empty($entry['field_id_' . $field_id]) && !isset($plan_id)) {
                    // it's a subscription product, let's set the price
                    $item_subscription_options = _unserialize($entry['field_id_' . $field_id], true);
                    if (isset($item_subscription_options['subscription_enabled']) && $item_subscription_options['subscription_enabled'] == true) {
                        if ((isset($item_subscription_options['subscription_trial_occurrences']) && $item_subscription_options['subscription_trial_occurrences'] > 0) && isset($item_subscription_options['subscription_trial_price'])) {
                            $data['price'] = sanitize_number($item_subscription_options['subscription_trial_price']);
                        } else {
                            $data['price'] = sanitize_number($item_subscription_options['subscription_price']);
                        }
                    }
                }
            }
        } elseif (isset($data['class'])) {
            unset($data['class']);
        }

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob');

        // if a class has been assigned to the item.
        if (ee()->input->post('CLS')) {
            $data['class'] = ee('Encrypt')->decode(ee()->input->post('CLS'));
        }
        $original_last_row_id = (ee()->cartthrob->cart->items()) ? ee()->cartthrob->cart->last_row_id() : -1;

        if (!isset($data['quantity']) || (isset($data['quantity']) && $data['quantity'] !== '0' && $data['quantity'] !== 0)) {
            if ($item = ee()->cartthrob->cart->add_item($data)) {
                if (isset($configuration)) {
                    $item->set_meta('configuration', $configuration);
                }

                if ($item->product_id() && $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($item->meta('channel_id'), 'cartthrob_subscriptions', true)) {
                    $item_subscription_options = _unserialize($item->meta('field_id_' . $field_id), true);
                }

                if (isset($plan_id)) {
                    if (isset($plan_data)) {
                        // this crazy thing creates a function to add a prefix to each array key.
                        $item_subscription_options = array_combine(
                            array_map(create_function('$k', 'return "subscription_".$k;'), array_keys($plan_data)), $plan_data
                        );
                    }

                    $item_subscription_options['subscription_enabled'] = true;

                    $item->set_meta('plan_id', $plan_id);
                }

                // set after plan so that plan permissions can be added.
                if (ee()->input->post('PER') && $permissions = ee('Encrypt')->decode(ee()->input->post('PER'))) {
                    $item->set_meta('permissions', $permissions);
                } else {
                    if (isset($item_subscription_options['subscription_permissions'])) {
                        $item->set_meta('permissions', $item_subscription_options['subscription_permissions']);
                    }
                }

                if (ee()->input->post('LIC') && bool_string(ee('Encrypt')->decode(ee()->input->post('LIC')))) {
                    $new_last_row_id = (ee()->cartthrob->cart->items()) ? ee()->cartthrob->cart->last_row_id() : -1;

                    for ($i = $original_last_row_id; $i <= $new_last_row_id; $i++) {
                        if ($i < 0 || !$_item = ee()->cartthrob->cart->item($i)) {
                            continue;
                        }

                        if ($data['class'] === 'package') {
                            foreach ($_item->sub_items() as $sub_item) {
                                $sub_item->set_meta('license_number', true);
                            }
                        } else {
                            $_item->set_meta('license_number', true);
                        }
                    }
                }

                $sub = $this->clean_sub_data((isset($item_subscription_options) ? $item_subscription_options : null));
                if ($sub !== false) {
                    // adding subscription meta. even if there's no new info, we still want the subscription meta set
                    $item->set_meta('subscription_options', $sub);
                    $item->set_meta('subscription', true);

                    if (!$item->title() && isset($sub['name'])) {
                        $item->set_title($sub['name']);
                    }
                }

                if (ee()->input->post('EXP')) {
                    $EXP = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('EXP')));

                    if ($EXP == sanitize_number($EXP)) {
                        $new_last_row_id = (ee()->cartthrob->cart->items()) ? ee()->cartthrob->cart->last_row_id() : -1;

                        for ($i = $original_last_row_id; $i <= $new_last_row_id; $i++) {
                            if ($i < 0 || !$_item = ee()->cartthrob->cart->item($i)) {
                                continue;
                            }

                            if ($data['class'] === 'package') {
                                foreach ($_item->sub_items() as $sub_item) {
                                    $sub_item->set_meta('expires', $EXP);
                                }
                            } else {
                                $_item->set_meta('expires', $EXP);
                            }
                        }
                    }
                }

                if ($inventory_reduce = ee()->input->post('inventory_reduce', true)) {
                    $item->set_meta('inventory_reduce', $inventory_reduce);
                }
            }

            // tired of adding item options, only to strip them out later and conver them to meta.
            $meta = ee()->input->post('meta', true);

            if (ee()->input->post('MET')) {
                $meta = @unserialize(base64_decode(ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('MET')))));

                if ($meta && is_array($meta)) {
                    // don't grab numeric item_options, those are for sub_items
                    foreach ($meta as $key => $value) {
                        if (strpos($key, ':') === false) {
                            // don't want to override any existing meta that has been set already
                            if (!$item->meta($key)) {
                                $item->set_meta($key, $value);
                            }
                        }
                    }
                }
            }

            // cartthrob_add_to_cart_end hook
            if (ee()->extensions->active_hook('cartthrob_add_to_cart_end') === true) {
                // @TODO work on hook parameters
                // $edata = $EXT->universal_call_extension('cartthrob_add_to_cart_end', $this, $_SESSION['cartthrob'], $row_id);
                ee()->extensions->call('cartthrob_add_to_cart_end', $item);
                if (ee()->extensions->end_script === true) {
                    return;
                }
            }
        }

        // if they're using inline stuff we wanna clear the added item upon error
        if (ee()->input->post('error_handling') === 'inline' && $item) {
            ee()->form_builder->set_error_callback([ee()->cartthrob->cart, 'remove_item', $item->row_id()]);
        }

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    public function clean_sub_data($item_subscription_options = [], $update = false)
    {
        // if item and SUB OR subscription (to account for select boxes)
        // OR if item AND corresponding product has subscription fieldtype and is enabled
        if (!empty($item_subscription_options['subscription_enabled']) || ((ee()->input->post('SUB') && bool_string(ee('Encrypt')->decode(ee()->input->post('SUB')))) || (ee()->input->post('subscription') && bool_string(ee()->input->post('subscription'))))) {
            // these are all of the subscription options
            // @TODO make a decision about these? do we need allow_user_subscription_trial_price or allow_user="trial_price|start_date"
            $subscription = [];

            ee()->load->model('subscription_model');

            // iterating through those options. if they're in post, we'll add them to the "subscription_options" meta
            foreach (ee()->subscription_model->option_keys() as $encoded_key => $key) {
                $option = null;

                if ($update) {
                    if (array_key_exists($key, $item_subscription_options)) {
                        $option = $item_subscription_options[$key];
                    }
                }
                // a couple of these things can be plain text
                if (ee()->input->post('subscription_' . $key)) {
                    switch ($key) {
                        case 'name':
                            $option = ee()->input->post('subscription_' . $key);
                            break;
                        case 'description':
                            $option = ee()->input->post('subscription_' . $key);
                            break;
                    }
                }

                if (!$option && ee()->input->post($encoded_key)) {
                    $option = ee('Encrypt')->decode(ee()->input->post($encoded_key));
                } else {
                    if (!$option && ee()->input->post($key)) {
                        $option = ee('Encrypt')->decode(ee()->input->post($key));
                    } else {
                        if (!$option && isset($item_subscription_options['subscription_' . $key])) {
                            switch ($key) {
                                case 'end_date':
                                case 'start_date':
                                    if (!$item_subscription_options['subscription_' . $key]) {
                                        $option = '';
                                    } else {
                                        if (!is_numeric($item_subscription_options['subscription_' . $key])) {
                                            $option = strtotime($item_subscription_options['subscription_' . $key]);
                                        } else {
                                            $option = $item_subscription_options['subscription_' . $key];
                                        }
                                    }
                                    break;
                                default:
                                    if (!$update) {
                                        $option = $item_subscription_options['subscription_' . $key];
                                    }
                            }
                        }
                    }
                }
                if (!is_null($option)) {
                    if (in_array($encoded_key, ee()->subscription_model->encoded_bools())) {
                        if ($update) {
                            if ($key != 'allow_modification') {
                                $option = bool_string($option);
                            }
                        } else {
                            $option = bool_string($option);
                        }
                    }

                    if (strncmp($key, 'subscription_', 13) === 0) {
                        $key = substr($key, 13);
                    }

                    $subscription[$key] = $option;
                }
            }

            if (empty($subscription['price']) && ee()->input->post('AUP') && ee()->input->post('price') !== false && bool_string(ee('Encrypt')->decode(ee()->input->post('AUP')))) {
                $subscription['price'] = sanitize_number(ee()->input->post('price', true));
            }
        }

        if (isset($subscription)) {
            return $subscription;
        }

        return false;
    }

    /**
     * update_cart_form
     *
     * handles submissions from the update_cart_form
     * redirects on completion
     */
    public function update_cart_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        if (ee()->extensions->active_hook('cartthrob_update_cart_start') === true) {
            ee()->extensions->call('cartthrob_update_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->load->library('form_builder');

        if (!ee()->form_builder->validate()) {
            ee()->cartthrob_variables->set_global_values();

            ee()->form_builder->set_value([
                'clear_cart',
            ]);

            return ee()->form_builder->action_complete();
        }

        ee()->cartthrob->save_customer_info();

        if (ee()->input->post('clear_cart')) {
            ee()->cartthrob->cart->clear();
        } else {
            foreach (ee()->cartthrob->cart->items() as $row_id => $item) {
                if (element($row_id, element('delete', $_POST))) {
                    $_POST['quantity'][$row_id] = 0;
                }

                $data = [];

                foreach ($_POST as $key => $value) {
                    if ($item->sub_items()) {
                        foreach ($item->sub_items() as $sub_item) {
                            if (isset($value[$row_id . ':' . $sub_item->row_id()]) && in_array($key,
                                    $sub_item->default_keys())) {
                                $_value = $value[$row_id . ':' . $sub_item->row_id()];

                                ee()->load->library('api');

                                ee()->legacy_api->instantiate('channel_fields');

                                if (empty(ee()->api_channel_fields->field_types)) {
                                    ee()->api_channel_fields->fetch_installed_fieldtypes();
                                }

                                if ($key === 'item_options' && ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                                    ee()->load->model('cartthrob_field_model');
                                    $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($item->meta('channel_id'),
                                        'cartthrob_package', true);

                                    $field_data = ee()->api_channel_fields->apply('pre_process',
                                        [$item->meta('field_id_' . $field_id)]);

                                    ee()->load->add_package_path(PATH_THIRD . 'cartthrob');

                                    foreach ($field_data as $row) {
                                        if (isset($row['allow_selection'])) {
                                            foreach ($row['allow_selection'] as $zkey => $allowed) {
                                                if (!$allowed && isset($_value[$zkey])) {
                                                    unset($_value[$zkey]);
                                                }
                                            }
                                        }
                                    }
                                }
                                $sub_item->update([$key => ee('Security/XSS')->clean($_value)]);
                            }
                        }
                    }

                    if (isset($value[$row_id]) && in_array($key, $item->default_keys())) {
                        $item_options = [];

                        if ($key == 'item_options') {
                            $configuration = [];
                            $configuration_meta = null;
                            $set_configuration_data = [];

                            $fields_list = [];
                            ee()->load->helper('array');
                            $arr = $value[$row_id];

                            foreach ($arr as $k => $v) {
                                if (strpos($k, 'configuration:') !== false) {
                                    list($a, $field, $option_group) = explode(':', $k);
                                    $fields_list[] = $field;
                                    $configuration[$field][$option_group] = $v;
                                    unset($value[$row_id][$k]);
                                }
                            }

                            if (!empty($configuration)) {
                                $fields_list = array_filter($fields_list);
                                ee()->load->model('product_model');
                                foreach ($fields_list as $field_name) {
                                    $entry = ee()->product_model->get_product($item->product_id());
                                    if ($entry) {
                                        $sku = ee()->product_model->get_base_variation($item->product_id(), $field,
                                            $configuration[$field_name]);

                                        if ($sku) {
                                            $item_options[$field_name] = $sku;

                                            $qu = (isset($_POST['quantity'][$row_id]) ? $_POST['quantity'][$row_id] : 1);
                                            $inventory = ee()->product_model->check_inventory($item->product_id(), $qu,
                                                [$field_name => $sku]);
                                            if ($inventory !== false && $inventory <= 0) {
                                                $title = ee()->input->post('title');
                                                if (!$title) {
                                                    $title = lang('item_title_placeholder');
                                                }
                                                ee()->form_builder->set_errors([
                                                    sprintf(lang('configuration_not_in_stock'), $title),
                                                ])
                                                    ->set_success_callback([ee()->cartthrob, 'action_complete'])
                                                    ->action_complete();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $data[$key] = ee('Security/XSS')->clean($value[$row_id]);
                        if (isset($item_options) && !empty($item_options) && is_array($data[$key])) {
                            $data[$key] = array_merge($data[$key], $item_options);

                            $configuration_meta = $item->meta('configuration');
                            if ($configuration_meta && isset($configuration)) {
                                $set_configuration_data = [];
                                foreach ($configuration_meta as $b => $c) {
                                    if (array_key_exists($b, $configuration)) {
                                        $set_configuration_data[$b] = array_merge($c, $configuration[$b]);
                                    }
                                }
                            }
                            if (isset($set_configuration_data) && is_array($set_configuration_data)) {
                                $item->set_meta('configuration', $set_configuration_data);
                            }
                        }
                    }

                    if (isset($value[$row_id]) && $key === 'subscription') {
                        $item->set_meta('subscription', bool_string($value[$row_id]));
                    }
                }

                $sub = $this->clean_sub_data((array)$item->meta('subscription_options'), $update = true);
                if ($sub !== false) {
                    // adding subscription meta. even if there's no new info, we still want the subscription meta set
                    $item->set_meta('subscription_options', $sub);
                }

                if ($data) {
                    $item->update($data);
                }
            }
        }

        if (ee()->extensions->active_hook('cartthrob_update_cart_end') === true) {
            ee()->extensions->call('cartthrob_update_cart_end');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        if (trim(ee()->input->post('coupon_code', true))) {
            ee()->cartthrob->cart->add_coupon_code(trim(ee()->input->post('coupon_code', true)));
        }

        ee()->cartthrob->cart->check_inventory();

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    public function add_coupon_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->load->library('form_builder');

        if (!ee()->form_builder->validate()) {
            ee()->cartthrob_variables->set_global_values();

            ee()->form_builder->set_value('coupon_code');

            return ee()->form_builder->action_complete();
        }

        ee()->cartthrob->save_customer_info();

        ee()->cartthrob->cart->add_coupon_code(trim(ee()->input->post('coupon_code', true)));

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    public function cart_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->cartthrob->save_customer_info();
    }

    public function checkout_action()
    {
        /* 1. How did that happen? **/
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        /* 2. Give hooks a chance to bail out **/
        if (ee()->extensions->active_hook('cartthrob_checkout_action_start') === true) {
            ee()->extensions->call('cartthrob_checkout_action_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        /** 3. Process checkout options */
        $checkoutOptions = $this->marshalCheckoutOptions();

        /* 4. Maybe save user info */
        $this->processCustomerInfo($checkoutOptions);

        /* 5. Load libraries */
        ee()->load->library('languages');
        ee()->languages->set_language(ee()->input->post('language', true));

        ee()->load->library('cartthrob_payments');
        ee()->cartthrob_payments->setGateway($checkoutOptions['gateway']);
        ee()->cartthrob_payments->setGatewayMethod($checkoutOptions['gateway_method']);

        ee()->load->library('form_validation');
        ee()->load->library('form_builder');

        /* 6. start the form */
        ee()->cartthrob_variables->set_global_values();

        ee()->form_builder->set_value(['coupon_code']);
        ee()->form_builder
            ->set_show_errors(true)
            ->set_captcha(ee()->session->userdata('member_id') == 0 && ee()->cartthrob->store->config('checkout_form_captcha'))
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->set_error_callback([ee()->cartthrob, 'action_complete']);

        if (!$checkoutOptions['create_user'] && !ee()->session->userdata('member_id') && ee()->cartthrob->store->config('logged_in')) {
            return ee()->form_builder
                ->add_error(ee()->lang->line('must_be_logged_in'))
                ->action_complete();
        }

        ee()->load->library('languages');

        ee()->languages->set_language(ee()->input->post('language', true));

        /** 7. Validate required fields */
        $not_required = explode('|', ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('NRQ') ?: '')));
        $required = array_diff(ee()->cartthrob_payments->requiredGatewayFields(), $not_required);

        if (!ee()->form_builder->set_required($required)->validate()) {
            return ee()->form_builder->action_complete();
        }

        /* 8. Bail out if user required but no user */
        if (ee()->cartthrob->store->config('logged_in') && !($checkoutOptions['create_user'] || ee()->session->userdata('member_id'))) {
            return ee()->form_builder
                ->add_error(ee()->lang->line('must_be_logged_in'))
                ->action_complete();
        }

        /* 9. Add coupon if present */
        if ($coupon_code = trim(ee()->input->post('coupon_code', true))) {
            ee()->cartthrob->cart->add_coupon_code($coupon_code);
        }

        /* 10. Subscription stuff  (maybe) */
        $this->processSubscriptionOptions($checkoutOptions);

        /* 11. Create user (maybe) */
        $this->createUser($checkoutOptions);

        /** Everything up to this point needs to happen regardless of async setting */
        $async = ((int)$this->cartthrob->config('orders_async_method') !== static::ASYNC_METHOD_DISABLED)
            && false === element('order_id', $checkoutOptions)
            && false === element('update_order_id', $checkoutOptions)
            && false === element('subscription_id', $checkoutOptions)
            && false === element('is_subscription_rebill', $checkoutOptions)
            && false === element('update_subscription_id', $checkoutOptions)
        ;

        $method = $async ? 'asyncCheckoutStart' : 'checkoutStart';
        $state = ee()->cartthrob_payments->$method($checkoutOptions);

        if ($state === false || !$state instanceof TransactionState) {
            return ee()->form_builder
                ->add_error(ee()->cartthrob_payments->errors())
                ->action_complete();
        }

        $method = $async ? 'asyncCheckoutComplete' : 'checkoutComplete';

        ee()->cartthrob_payments->$method($state);
    }

    public function asyncCheckoutAction()
    {
        return true;
    }

    public function multi_add_to_cart_action()
    {
        // NOTE: multi add to cart does not work with configured items
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->load->library('form_builder');

        // cartthrob_multi_add_to_cart_start hook
        if (ee()->extensions->active_hook('cartthrob_multi_add_to_cart_start') === true) {
            ee()->extensions->call('cartthrob_multi_add_to_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        if (!ee()->form_builder->validate()) {
            return ee()->form_builder->action_complete();
        }

        ee()->cartthrob->save_customer_info();

        $entry_ids = ee()->input->post('entry_id', true);
        $items = [];
        if (is_array($entry_ids)) {
            $on_the_fly = (ee()->input->post('OTF') && bool_string(ee('Encrypt')->decode(ee()->input->post('OTF'))));

            $json = (ee()->input->post('JSN') && bool_string(ee('Encrypt')->decode(ee()->input->post('JSN'))));

            $allow_user_price = (ee()->input->post('AUP') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUP'))));

            $allow_user_shipping = (ee()->input->post('AUS') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUS'))));

            $allow_user_weight = (ee()->input->post('AUW') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUW'))));

            $class = null;
            // if a class has been assigned to the item.
            if (ee()->input->post('CLS')) {
                $class = ee('Encrypt')->decode(ee()->input->post('CLS'));
            }

            foreach ($entry_ids as $row_count => $entry_id) {
                $quantity = ee('Security/XSS')->clean(array_value($_POST, 'quantity', $row_count));

                if (!is_numeric($quantity) || $quantity <= 0) {
                    continue;
                }

                $data = [
                    'entry_id' => ee('Security/XSS')->clean(array_value($_POST, 'entry_id', $row_count)),
                    'quantity' => $quantity,
                ];
                // thanks to Dion40 for catching an error related to no_shipping, no_tax
                if (ee()->input->post('NSH')) {
                    $data['no_shipping'] = bool_string(ee('Encrypt')->decode(ee()->input->post('NSH')));
                }

                if (ee()->input->post('NTX')) {
                    $data['no_tax'] = bool_string(ee('Encrypt')->decode(ee()->input->post('NTX')));
                }
                if (($allow_user_price || $on_the_fly) && ($value = array_value($_POST, 'price', $row_count)) !== false) {
                    $data['price'] = ee('Security/XSS')->clean($value);
                }

                if (($allow_user_weight || $on_the_fly) && ($value = array_value($_POST, 'weight', $row_count)) !== false) {
                    $data['weight'] = ee('Security/XSS')->clean($value);
                }

                if (($allow_user_shipping || $on_the_fly) && ($value = array_value($_POST, 'shipping', $row_count)) !== false) {
                    $data['shipping'] = ee('Security/XSS')->clean($value);
                }

                if ($value = array_value($_POST, 'title', $row_count)) {
                    $data['title'] = ee('Security/XSS')->clean($value);
                }

                if (!$on_the_fly) {
                    $data['class'] = 'product';
                }

                if ($class) {
                    $data['class'] = $class;
                }

                $data['site_id'] = 1;

                ee()->load->model('product_model');
                ee()->load->model('cartthrob_field_model');

                $item_options = [];
                if ($value = array_value($_POST, 'item_options', $row_count)) {
                    $item_options = ee('Security/XSS')->clean($value);
                }
                // don't grab numeric item_options, those are for sub_items
                foreach ($item_options as $key => $value) {
                    if (strpos($key, ':') === false) {
                        $data['item_options'][$key] = $value;
                    }
                }

                if ($entry = ee()->product_model->get_product($data['entry_id'])) {
                    if (isset($entry['site_id'])) {
                        $data['site_id'] = $entry['site_id'];
                    }

                    $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($entry['channel_id'], 'cartthrob_package', true);

                    if ($field_id && !empty($entry['field_id_' . $field_id])) {
                        // it's a package
                        $data['class'] = 'package';

                        ee()->load->library('api');

                        ee()->legacy_api->instantiate('channel_fields');

                        if (empty(ee()->api_channel_fields->field_types)) {
                            ee()->api_channel_fields->fetch_installed_fieldtypes();
                        }

                        $data['sub_items'] = [];

                        if (ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                            $field_data = ee()->api_channel_fields->apply('pre_process', [$entry['field_id_' . $field_id]]);

                            foreach ($field_data as $row_id => $row) {
                                $item = [
                                    'entry_id' => $row['entry_id'],
                                    'product_id' => $row['entry_id'],
                                    'row_id' => $row_id,
                                    'class' => 'product',
                                    'site_id' => $data['site_id'],
                                    // assuming it has to be from the same site id as the parent based on EE's structure
                                ];

                                $item['item_options'] = (isset($row['option_presets'])) ? $row['option_presets'] : [];

                                $row_item_options = [];

                                if (isset($_POST['item_options'][$row_count])) {
                                    $row_item_options = $_POST['item_options'][$row_count];
                                } else {
                                    if (isset($_POST['item_options'][$data['entry_id'] . ':' . $row_id . ':' . $row_count])) {
                                        $row_item_options = $_POST['item_options'][$data['entry_id'] . ':' . $row_id . ':' . $row_count];
                                    } else {
                                        if (isset($_POST['item_options'][':' . $row_id . ':' . $row_count])) {
                                            $row_item_options = $_POST['item_options'][':' . $row_id . ':' . $row_count];
                                        }
                                    }
                                }

                                $price_modifiers = ee()->product_model->get_all_price_modifiers($row['entry_id']);

                                foreach ($row_item_options as $key => $value) {
                                    // if it's not a price modifier (ie, an on-the-fly item option), add it
                                    // if it is a price modifier, check that it's been allowed before adding
                                    if (!isset($price_modifiers[$key]) || !empty($row['allow_selection'][$key])) {
                                        $item['item_options'][$key] = ee('Security/XSS')->clean($value);
                                    }
                                }

                                $data['sub_items'][$row_id] = $item;
                            }
                        }
                    }
                }

                $data['product_id'] = $data['entry_id'];

                $item = ee()->cartthrob->cart->add_item($data);

                if (ee()->input->post('PER')) {
                    if ($permissions = ee('Encrypt')->decode(ee()->input->post('PER'))) {
                        $item->set_meta('permissions', $permissions);
                    }
                }

                if ($item && $value = array_value($_POST, 'license_number', $row_count)) {
                    $item->set_meta('license_number', true);
                }
            }

            $items[$entry_id] = $item;
        }

        ee()->cartthrob->cart->check_inventory();

        // cartthrob_multi_add_to_cart_end hook
        if (ee()->extensions->active_hook('cartthrob_multi_add_to_cart_end') === true) {
            ee()->extensions->call('cartthrob_multi_add_to_cart_end', $entry_ids, $items);
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    public function save_customer_info_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->load->library('form_builder');

        if (ee()->extensions->active_hook('cartthrob_save_customer_info_start') === true) {
            ee()->extensions->call('cartthrob_save_customer_info_start');
        }

        if (ee()->form_builder->validate()) {
            ee()->cartthrob->save_customer_info();
        } else {
            ee()->cartthrob_variables->set_global_values();
        }

        if (ee()->extensions->active_hook('cartthrob_save_customer_info_end') === true) {
            ee()->extensions->call('cartthrob_save_customer_info_end');
        }

        ee()->form_builder->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    /**
     * payment_return_action
     *
     * handles information from PayPal's IPN, offsite gateways, or other payment notification systems.
     * @param string $gateway the payment gateway class/file that should called
     * @param string $method the method in the gateway class that should handle the transaction
     */
    public function payment_return_action($gateway = null, $method = null)
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        $gateway = ee('Security/XSS')->clean(ee('Encrypt')->decode(str_replace(' ', '+',
            base64_decode(ee()->input->get_post('gateway')))));
        if (!$gateway) {
            $gateway = ee('Security/XSS')->clean(ee('Encrypt')->decode(str_replace(' ', '+',
                base64_decode(ee()->input->get_post('G')))));
        }

        // When offsite payments are returned, they're expected to have a method
        // set to handle processing the payments.
        if (ee()->input->get_post('method')) {
            $method = ee('Security/XSS')->clean(ee('Encrypt')->decode(str_replace(' ', '+',
                base64_decode(ee()->input->get_post('method')))));
        } elseif (ee()->input->get_post('M')) {
            $method = ee('Security/XSS')->clean(ee('Encrypt')->decode(str_replace(' ', '+',
                base64_decode(ee()->input->get_post('M')))));
        }

        ee()->load->library('cartthrob_payments');

        $state = new TransactionState();

        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            $state->setFailed(ee()->lang->line('invalid_payment_gateway'));
        } elseif ($method && method_exists(ee()->cartthrob_payments->gateway(), $method)) {
            $data = ee('Security/XSS')->clean($_POST);

            $data['gateway'] = $gateway;
            $data['method'] = $method;
            $data['orderId'] = ee('Security/XSS')->clean(
                ee('Encrypt')->decode(
                    str_replace(' ', '+', base64_decode(ee()->input->get_post('orderId')))
                )
            );

            // handling get variables.
            if ($_SERVER['QUERY_STRING']) {
                // the following was added to convert the query string manually into an array
                // because something like &company=abercrombie&fitch&name=joe+jones was causing the return
                // data to get hosed.
                $_SERVER['QUERY_STRING'] = preg_replace('/&(?=[^=]*&)/', '%26', $_SERVER['QUERY_STRING']);

                $get = [];
                parse_str($_SERVER['QUERY_STRING'], $get);

                foreach ($get as $key => $value) {
                    if (!isset($data[$key])) {
                        $data[$key] = ee('Security/XSS')->clean($value);
                    }
                }
            }

            foreach ($data as $key => $item) {
                ee()->cartthrob->log($key . ' - ' . $item);
            }

            $state = ee()->cartthrob_payments->gateway()->$method($data);
        } else {
            $state->setFailed(ee()->lang->line('gateway_function_does_not_exist'));
        }

        ee()->cartthrob_payments->checkoutComplete($state);
    }

    // @TODO add subscription updater
    // @TODO make this function read the gateway out of the database based on the provided entry id

    public function update_recurrent_billing_form()
    {
        if (ee()->session->userdata('member_id') == 0) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('api/api_cartthrob_payment_gateways');

        if (ee()->TMPL->fetch_param('gateway')) {
            ee()->api_cartthrob_payment_gateways->setGateway(ee()->TMPL->fetch_param('gateway'));
        }

        $data = ee()->cartthrob_variables->global_variables(true);

        $data['recurrent_billing_fields'] = ee()->api_cartthrob_payment_gateways->gateway_fields(false,
            'recurrent_billing_update');
        $data['gateway_fields'] = ee()->api_cartthrob_payment_gateways->gateway_fields();

        ee()->load->library('form_builder');

        ee()->form_builder->initialize([
            'classname' => 'Cartthrob',
            'method' => 'update_recurrent_billing_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            'form_data' => [
                'action',
                'secure_return',
                'return',
                'language',
            ],
            'encoded_form_data' => [
                'required' => 'REQ',
                'gateway' => 'gateway',
                'subscription_name' => 'SUN',
                'subscription_start_date' => 'SSD',
                'subscription_end_date' => 'SED',
                'subscription_interval_units' => 'SIU',
                'sub_id' => 'SD',
                'subscription_type' => 'SUT',
            ],
            'encoded_numbers' => [
                'subscription_total_occurrences' => 'SO',
                'subscription_trial_price' => 'ST',
                'subscription_trial_occurrences' => 'SP',
                'subscription_interval_length' => 'SI',
                'order_id' => 'OI',
            ],
            'encoded_bools' => [
                'allow_user_price' => 'AUP',
                // 'show_errors' => array('ERR', TRUE),
                'json' => 'JSN',
                // 'subscription_allow_modification'		=> 'SM',
            ],
        ]);

        return ee()->form_builder->form();
    }

    public function update_recurrent_billing_action()
    {
        // currently we allow the customer information stored on file with the recurrent bill to be changed.
        // the actual details of the original order are not changed however.
        // over time we need feedback about what needs to be added / changed in the original order
        // or purchased items when someone decides to update their subscription
        // not all systems allow the sub itself to be updated, but they all allow customer information
        // like credit card numbers to be changed. For our purposes, we're currently only using this
        // as a card data update.

        // @TODO catch the sub id, and order id.

        $total = 0;

        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->cartthrob->save_customer_info();

        ee()->load->library('form_validation');
        ee()->load->library('form_builder');

        ee()->form_builder->set_show_errors(true)
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->set_error_callback([ee()->cartthrob, 'action_complete']);

        if (!ee()->cartthrob->store->config('save_orders')) {
            ee()->form_builder->action_complete();
        }

        ee()->load->library('languages');

        ee()->languages->set_language(ee()->input->post('language', true));

        $not_required = [];
        $required = [];

        if (ee()->input->post('REQ')) {
            $required_string = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('REQ')));

            if (preg_match('/^not (.*)/', $required_string, $matches)) {
                $not_required = explode('|', $matches[1]);
                $required_string = '';
            }

            if ($required_string) {
                $required = explode('|', $required_string);
            }

            unset($required_string);
        }

        $creditCardNumber = sanitize_credit_card_number(ee()->input->post('credit_card_number', true));
        $gateway = ee()->input->post('gateway')
            ? ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('gateway')))
            : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        ee()->load->library('cartthrob_payments');

        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            ee()->form_builder
                ->add_error(ee()->lang->line('invalid_payment_gateway'))
                ->action_complete();
        }

        if (ee()->input->post('PR')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('PR')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $total -= $subtotal;
                $subtotal = $data;
                $total += $subtotal;
            }
        } elseif (ee()->input->post('AUP') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUP')))) {
            $total = sanitize_number(ee()->input->post('price', true));
        }

        if (ee()->input->post('SD')) {
            $subId = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('SD')));
        }

        foreach ($not_required as $key) {
            unset($required[array_search($key, $required)]);
        }

        if (!ee()->form_builder->set_required($required)->validate()) {
            ee()->form_builder->action_complete();
        }

        ee()->load->model('order_model');

        ee()->cartthrob->cart->set_order(ee()->order_model->order_data_array());
        ee()->cartthrob_payments->setTotal($total);
        ee()->cartthrob->cart->save();

        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->updateRecurrentBilling($subId, $creditCardNumber);

        // since we use the authorized variables as tag conditionals in submitted_order_info,
        // we won't throw any errors from here on out
        ee()->form_builder->set_show_errors(false);

        if ($state->isAuthorized()) {
            ee()->form_builder->set_return(ee()->cartthrob->cart->order('authorized_redirect'));
        } else {
            ee()->form_builder
                ->set_return(ee()->cartthrob->cart->order('failed_redirect'))
                ->add_error($state->getMessage());
        }

        ee()->cartthrob->cart->save();

        ee()->form_builder->action_complete();
    }

    // @TODO make this function read the gateway out of the database based on the provided entry id

    public function delete_recurrent_billing_form()
    {
        if (ee()->session->userdata('member_id') == 0) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('api/api_cartthrob_payment_gateways');

        if (ee()->TMPL->fetch_param('gateway')) {
            ee()->api_cartthrob_payment_gateways->setGateway(ee()->TMPL->fetch_param('gateway'));
        }

        $data = ee()->cartthrob_variables->global_variables(true);

        $data['gateway_fields'] = ee()->api_cartthrob_payment_gateways->gateway_fields(false,
            'recurrent_billing_delete');

        ee()->load->library('form_builder');

        ee()->form_builder->initialize([
            'classname' => 'Cartthrob',
            'method' => 'delete_recurrent_billing_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            'form_data' => [
                'action',
                'secure_return',
                'return',
                'language',
            ],
            'encoded_form_data' => [
                'required' => 'REQ',
                'sub_id' => 'SD',
                'gateway' => 'gateway',
            ],
            'encoded_numbers' => [
                'order_id' => 'OI',
            ],
            'encoded_bools' => [
                'allow_user_price' => 'AUP',
                // 'show_errors' => array('ERR', TRUE),
                'json' => 'JSN',
            ],
        ]);

        return ee()->form_builder->form();
    }

    public function delete_recurrent_billing_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        ee()->cartthrob->save_customer_info();

        ee()->load->library('form_validation');
        ee()->load->library('form_builder');

        ee()->form_builder->set_show_errors(true)
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->set_error_callback([ee()->cartthrob, 'action_complete']);

        if (!ee()->cartthrob->store->config('save_orders')) {
            ee()->form_builder->action_complete();
        }

        ee()->load->library('languages');

        ee()->languages->set_language(ee()->input->post('language', true));

        $not_required = [];
        $required = [];

        if (ee()->input->post('REQ')) {
            $required_string = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('REQ')));

            if (preg_match('/^not (.*)/', $required_string, $matches)) {
                $not_required = explode('|', $matches[1]);
                $required_string = '';
            }

            if ($required_string) {
                $required = explode('|', $required_string);
            }

            unset($required_string);
        }

        $gateway = (ee()->input->post('gateway')) ? ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('gateway'))) : ee()->cartthrob->store->config('payment_gateway');

        // Load the payment processing plugin that's stored in the extension's settings.
        ee()->load->library('cartthrob_payments');

        if (!ee()->cartthrob_payments->setGateway($gateway)->gateway()) {
            ee()->form_builder
                ->add_error(ee()->lang->line('invalid_payment_gateway'))
                ->action_complete();
        }

        $authorized_redirect = ee()->input->post('authorized_redirect', true);
        $failed_redirect = ee()->input->post('failed_redirect', true);
        $declined_redirect = ee()->input->post('declined_redirect', true);

        if (ee()->input->post('OI')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('OI')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $order_id = $data;
            }
        }

        if (ee()->input->post('SD')) {
            $subId = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('SD')));
        }

        foreach ($not_required as $key) {
            unset($required[array_search($key, $required)]);
        }

        if (!ee()->form_builder->set_required($required)->validate()) {
            ee()->form_builder->action_complete();
        }

        ee()->load->model('order_model');

        $order_data = ee()->order_model->order_data_array();

        ee()->cartthrob->cart->set_order($order_data);

        ee()->cartthrob->cart->save();

        /** @var TransactionState $state */
        $state = ee()->cartthrob_payments->deleteRecurrentBilling($subId);

        ee()->cartthrob->cart->update_order($state->toArray());

        ee()->session->set_flashdata($state->toArray());

        // since we use the authorized variables as tag conditionals in submitted_order_info,
        // we won't throw any errors from here on out
        ee()->form_builder->set_show_errors(false);

        if ($state->isAuthorized()) {
            ee()->form_builder->set_return(ee()->cartthrob->cart->order('authorized_redirect'));
        } else {
            ee()->form_builder
                ->set_return(ee()->cartthrob->cart->order('failed_redirect'))
                ->add_error($state->getMessage());
        }

        ee()->cartthrob->cart->save();

        ee()->form_builder->action_complete();
    }

    /**
     * Prints a coupon code form.
     *
     * @param string $TMPL ->fetch_param('action')
     * @param string $TMPL ->fetch_param('id')
     * @param string $TMPL ->fetch_param('class')
     * @param string $TMPL ->fetch_param('name')
     * @param string $TMPL ->fetch_param('onsubmit')
     * @return string
     */
    public function add_coupon_form()
    {
        if (!ee()->session->userdata('member_id')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('form_builder');

        $data = ee()->cartthrob_variables->global_variables(true);

        $data['allowed'] = 1;

        if (ee()->cartthrob->store->config('global_coupon_limit') && count(ee()->cartthrob->cart->coupon_codes()) >= ee()->cartthrob->store->config('global_coupon_limit')) {
            $data['allowed'] = 0;
        }

        ee()->form_builder->initialize([
            'classname' => 'Cartthrob',
            'method' => 'add_coupon_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            'form_data' => [
                'action',
                'secure_return',
                'return',
                'language',
            ],
            'encoded_form_data' => [],
            'encoded_numbers' => [],
            'encoded_bools' => [
                // 'show_errors' => array('ERR', TRUE),
                'json' => 'JSN',
            ],
        ]);

        return ee()->form_builder->form();
    }

    public function add_to_cart()
    {
        // cartthrob_add_to_cart_start hook
        if (ee()->extensions->active_hook('cartthrob_add_to_cart_start') === true) {
            // @TODO work on hook parameters
            // $edata = $EXT->universal_call_extension('cartthrob_add_to_cart_start', $this, $_SESSION['cartthrob']);
            ee()->extensions->call('cartthrob_add_to_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        $data = [
            'entry_id' => ee()->TMPL->fetch_param('entry_id'),
            'quantity' => (ee()->TMPL->fetch_param('quantity') !== false) ? ee()->TMPL->fetch_param('quantity') : 1,
            'class' => 'product',
        ];

        foreach (ee()->TMPL->tagparams as $key => $value) {
            if (preg_match('/^item_options?:(.*)$/', $key, $match)) {
                if (!isset($data['item_options'])) {
                    $data['item_options'] = [];
                }

                $data['item_options'][$match[1]] = $value;
            }
        }

        if (bool_string(ee()->TMPL->fetch_param('shipping_exempt'))) {
            $data['no_shipping'] = true;
        }
        if (bool_string(ee()->TMPL->fetch_param('no_shipping'))) {
            $data['no_shipping'] = true;
        }

        if (bool_string(ee()->TMPL->fetch_param('tax_exempt'))) {
            $data['no_tax'] = true;
        }
        if (bool_string(ee()->TMPL->fetch_param('no_tax'))) {
            $data['no_tax'] = true;
        }

        $data['product_id'] = $data['entry_id'];

        if (!$data['entry_id']) {
            ee()->cartthrob->set_error(lang('add_to_cart_no_entry_id'));
        }

        if (!ee()->cartthrob->errors()) {
            $entry = ee()->product_model->get_product($data['entry_id']);

            // it's a package
            if ($entry && $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($entry['channel_id'], 'cartthrob_package', true)) {
                $data['class'] = 'package';

                ee()->load->library('api');

                ee()->legacy_api->instantiate('channel_fields');

                if (empty(ee()->api_channel_fields->field_types)) {
                    ee()->api_channel_fields->fetch_installed_fieldtypes();
                }

                $data['sub_items'] = [];

                if (ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                    $field_data = ee()->api_channel_fields->apply('pre_process', [$entry['field_id_' . $field_id]]);

                    foreach ($field_data as $row_id => $row) {
                        $item = [
                            'entry_id' => $row['entry_id'],
                            'product_id' => $row['entry_id'],
                            'row_id' => $row_id,
                            'class' => 'product',
                        ];

                        $item['item_options'] = (isset($row['option_presets'])) ? $row['option_presets'] : [];

                        $row_item_options = [];

                        if (isset($_POST['item_options'][$row_id])) {
                            $row_item_options = $_POST['item_options'][$row_id];
                        } elseif (isset($_POST['item_options'][':' . $row_id])) {
                            $row_item_options = $_POST['item_options'][':' . $row_id];
                        }

                        $price_modifiers = ee()->product_model->get_all_price_modifiers($row['entry_id']);

                        foreach ($row_item_options as $key => $value) {
                            // if it's not a price modifier (ie, an on-the-fly item option), add it
                            // if it is a price modifier, check that it's been allowed before adding
                            if (!isset($price_modifiers[$key]) || !empty($row['allow_selection'][$key])) {
                                $item['item_options'][$key] = ee('Security/XSS')->clean($value);
                            }
                        }

                        $data['sub_items'][$row_id] = $item;
                    }
                }
            } elseif ($entry) {
                // it's a product... don't need to do anything extra
                // but we need to check for it... else the class gets killed and we dont' want that.
            } else {
                // it's a dynamic product. kill the class
                if (isset($data['class'])) {
                    unset($data['class']);
                }
            }

            $item = ee()->cartthrob->cart->add_item($data);

            if ($item && ee()->TMPL->fetch_param('permissions')) {
                $item->set_meta('permissions', ee()->TMPL->fetch_param('permissions'));
            }

            if ($item && bool_string(ee()->TMPL->fetch_param('license_number'))) {
                $item->set_meta('license_number', true);
            }

            // cartthrob_add_to_cart_end hook
            if (ee()->extensions->active_hook('cartthrob_add_to_cart_end') === true) {
                // @TODO work on hook parameters
                // $edata = $EXT->universal_call_extension('cartthrob_add_to_cart_end', $this, $_SESSION['cartthrob'], $row_id);
                ee()->extensions->call('cartthrob_add_to_cart_end', $item);
                if (ee()->extensions->end_script === true) {
                    return;
                }
            }
        }

        $show_errors = bool_string(ee()->TMPL->fetch_param('show_errors'), true);

        ee()->session->set_flashdata([
            'success' => !(bool)ee()->cartthrob->errors(),
            'errors' => ee()->cartthrob->errors(),
            'csrf_token' => ee()->functions->add_form_security_hash('{csrf_token}'),
        ]);

        if ($show_errors && ee()->cartthrob->errors() && !AJAX_REQUEST) {
            return show_error(ee()->cartthrob->errors());
        }

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    // --------------------------------
    //  Add to Cart Form
    // --------------------------------

    /**
     * add_to_cart_form
     *
     * This tag creates a form for adding one or more products to the cart object
     *
     * @return string Tagdata output
     */
    public function add_to_cart_form()
    {
        if (!ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('form_builder');

        ee()->load->model('subscription_model');

        ee()->form_builder->initialize([
            'form_data' => [
                'entry_id',
                'quantity',
                'secure_return',
                'title',
                'language',
                'return',
            ],
            'encoded_form_data' => array_merge(
                ee()->subscription_model->encoded_form_data(),
                [
                    'shipping' => 'SHP',
                    'weight' => 'WGT',
                    'permissions' => 'PER',
                    'upload_directory' => 'UPL',
                    'class' => 'CLS',
                ]
            ),
            'encoded_numbers' => array_merge(
                ee()->subscription_model->encoded_numbers(),
                [
                    'price' => 'PR',
                    'expiration_date' => 'EXP',
                ]
            ),
            'encoded_bools' => array_merge(
                [
                    'allow_user_price' => 'AUP',
                    'allow_user_weight' => 'AUW',
                    'allow_user_shipping' => 'AUS',
                    'on_the_fly' => 'OTF',
                    'show_errors' => ['ERR', true],
                    'license_number' => 'LIC',
                ]
            ),
            'array_form_data' => [
                'item_options',
            ],
            'encoded_array_form_data' => [
                'meta' => 'MET',
            ],
            'classname' => 'Cartthrob',
            'method' => 'add_to_cart_action',
            'params' => ee()->TMPL->tagparams,
        ]);

        // can't just shove these in the encoded bools, or they will always be FALSE by default unless set.
        // since the field type overrides them, we don't even want them set here unless explicitly set.
        foreach (ee()->subscription_model->encoded_bools() as $key => $value) {
            if (ee()->TMPL->fetch_param($key)) {
                ee()->form_builder->set_encoded_bools($key, $value)->set_params(ee()->TMPL->tagparams);
            }
        }

        if (bool_string(ee()->TMPL->fetch_param('no_tax'))) {
            ee()->form_builder->set_encoded_bools('no_tax', 'NTX')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('tax_exempt'))) {
            ee()->form_builder->set_encoded_bools('tax_exempt', 'NTX')->set_params(ee()->TMPL->tagparams);
        }

        if (bool_string(ee()->TMPL->fetch_param('no_shipping'))) {
            ee()->form_builder->set_encoded_bools('no_shipping', 'NSH')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('shipping_exempt'))) {
            ee()->form_builder->set_encoded_bools('shipping_exempt', 'NSH')->set_params(ee()->TMPL->tagparams);
        }

        $data = array_merge(
            ee()->cartthrob_variables->item_option_vars(ee()->TMPL->fetch_param('entry_id')),
            ee()->cartthrob_variables->global_variables(true)
        );

        ee()->cartthrob_variables->add_encoded_option_vars($data);

        foreach (ee()->TMPL->var_single as $var) {
            if (preg_match('/^inventory:reduce(.+)$/', $var, $match)) {
                $data[$match[0]] = '';

                $var_params = ee('Variables/Parser')->parseTagParameters($match[1]);

                if (!empty($var_params['entry_id'])) {
                    if (empty($var_params['quantity'])) {
                        $var_params['quantity'] = 1;
                    } else {
                        $var_params['quantity'] = sanitize_number($var_params['quantity']);
                    }

                    ee()->form_builder->set_hidden('inventory_reduce[' . $var_params['entry_id'] . ']',
                        $var_params['quantity']);
                }
            }
        }

        ee()->load->library('languages');

        ee()->languages->set_language(ee()->TMPL->fetch_param('language'));

        ee()->form_builder->set_content(ee()->template_helper->parse_variables_row($data));

        return ee()->form_builder->form();
    }

    public function add_coupon_code()
    {
        ee()->cartthrob->cart->add_coupon_code(ee()->TMPL->fetch_param('coupon_code'));

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function purchased_entry_ids()
    {
        $data = [];

        ee()->load->model('purchased_items_model');

        $purchased = ee()->purchased_items_model->purchased_entry_ids();

        foreach ($purchased as $entry_id) {
            $data[] = ['entry_id' => $entry_id];
        }

        return ee()->template_helper->parse_variables($data);
    }

    /**
     * most_purchased
     *
     * Tag pair will print out the entry IDs of items purchased in descending order.
     * @param $TMPL limit
     * @return string
     **/
    public function most_purchased()
    {
        $data = [];

        ee()->load->model('order_management_model');
        $sort = ee()->TMPL->fetch_param('sort') ? ee()->TMPL->fetch_param('sort') : 'DESC';
        $limit = ee()->TMPL->fetch_param('limit');

        $purchased = ee()->order_management_model->get_purchased_products([], 'total_sales', $sort, $limit);

        foreach ($purchased as $row) {
            $data[] = [
                'entry_id' => $row['entry_id'],
            ];
        }

        return ee()->template_helper->parse_variables($data);
    }

    /**
     * also_purchased
     *
     * Tag pair will replace {entry_id} with entry id of related purchased items.
     * @param $TMPL entry_id
     * @param $TMPL limit
     * @return string
     **/
    public function also_purchased()
    {
        $data = [];

        if ($parent_id = ee()->TMPL->fetch_param('entry_id')) {
            ee()->load->model(['purchased_items_model', 'cartthrob_entries_model']);

            $purchased = ee()->purchased_items_model->also_purchased($parent_id, ee()->TMPL->fetch_param('limit'));

            foreach ($purchased as $entry_id => $count) {
                if ($row = ee()->cartthrob_entries_model->entry_vars($entry_id)) {
                    $data[] = $row;
                }
            }
        }

        return ee()->template_helper->parse_variables($data);
    }

    /**
     * arithmetic
     *
     * This function does arithmetic calculations
     *
     * @param string TEMPLATE PARAM operator + / - etc
     * @return string
     */
    public function arithmetic()
    {
        ee()->load->library(['math', 'number']);

        if (ee()->TMPL->fetch_param('expression') !== false) {
            if (bool_string(ee()->TMPL->fetch_param('debug'))) {
                return ee()->TMPL->fetch_param('expression');
            }

            $evaluation = (ee()->TMPL->fetch_param('expression')) ? ee()->math->evaluate(ee()->TMPL->fetch_param('expression')) : 0;
        } else {
            $evaluation = ee()->math->arithmetic(ee()->TMPL->fetch_param('num1'), ee()->TMPL->fetch_param('num2'),
                ee()->TMPL->fetch_param('operator'));
        }

        if ($evaluation === false && bool_string(ee()->TMPL->fetch_param('show_errors'), true)) {
            return ee()->math->last_error;
        }

        return ee()->number->format($evaluation);
    }

    // --------------------------------
    //  Cart Empty Redirect
    // --------------------------------

    /**
     * Redirects if cart is empty.
     * Place on your view cart page.
     */
    public function cart_empty_redirect()
    {
        if (ee()->cartthrob->cart->is_empty()) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
        }
    }

    public function cart_form()
    {
        ee()->load->library(['number', 'form_builder']);

        $data = ee()->cartthrob_variables->global_variables(true);

        $data['items'] = [];

        foreach (ee()->cartthrob->cart->items() as $row_id => $item) {
            $data['items'][$row_id] = $item->data();
            $data['items'][$row_id]['entry_id'] = $item->product_id();

            $row['item_price:numeric'] =
            $row['price:numeric'] =
            $row['item_price_numeric'] =
            $row['price_numeric'] =
                $item->price();

            $row['item_price_plus_tax:numeric'] =
            $row['price_numeric:plus_tax'] =
            $row['price_plus_tax:numeric'] =
            $row['item_price_plus_tax_numeric'] =
            $row['price_plus_tax_numeric'] =
                $item->taxed_price();

            $row['item_price'] =
            $row['price'] =
                ee()->number->format($item->price());

            $row['item_price_plus_tax'] =
            $row['price:plus_tax'] =
            $row['item_price:plus_tax'] =
            $row['price_plus_tax'] =
                ee()->number->format($item->taxed_price());

            foreach (ee()->cartthrob_variables->item_option_vars($item->product_id(), $row_id) as $key => $value) {
                $data['items'][$row_id][$key] = $value;
            }
        }

        ee()->load->library('data_filter');

        $order_by = (ee()->TMPL->fetch_param('order_by')) ? ee()->TMPL->fetch_param('order_by') : ee()->TMPL->fetch_param('orderby');

        ee()->data_filter->sort($data['items'], $order_by, ee()->TMPL->fetch_param('sort'));
        ee()->data_filter->limit($data['items'], ee()->TMPL->fetch_param('limit'), ee()->TMPL->fetch_param('offset'));

        ee()->form_builder->initialize([
            'form_data' => [
                'action',
                'secure_return',
                'return',
                'language',
            ],
            'encoded_form_data' => [],
            'encoded_numbers' => [],
            'encoded_bools' => [],
            'classname' => 'Cartthrob',
            'method' => 'cart_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
        ]);

        return ee()->form_builder->form();
    }

    /**
     * cart_entry_ids
     *
     * returns a pipe delimited list of entry ids
     *
     * @return string
     */
    public function cart_entry_ids()
    {
        return implode('|', ee()->cartthrob->cart->product_ids());
    }

    /**
     * cart_info
     *
     * Template tag that outputs generic cart info & conditionals related to totals and shipping
     *
     * @return string
     */
    public function cart_info()
    {
        ee()->TMPL->tagdata = ee()->functions->prep_conditionals(ee()->TMPL->tagdata,
            ee()->cartthrob->cart->info(false));

        return ee()->template_helper->parse_variables_row(ee()->cartthrob_variables->global_variables());
    }

    /**
     * Print out cart contents
     *
     * @return string
     */
    public function cart_items_info()
    {
        ee()->load->library(['number', 'typography']);
        ee()->load->helper('array');

        $data = [];
        $global_vars = ee()->cartthrob_variables->global_variables();
        $entry_ids = (ee()->TMPL->fetch_param('entry_id')) ? explode('|', ee()->TMPL->fetch_param('entry_id')) : false;
        $row_ids = (ee()->TMPL->fetch_param('row_id') !== false) ? explode('|', ee()->TMPL->fetch_param('row_id')) : false;
        $plan_ids = (ee()->TMPL->fetch_param('plan_id') !== false) ? explode('|', ee()->TMPL->fetch_param('plan_id')) : false;

        ee()->load->library('api');
        ee()->legacy_api->instantiate('channel_fields');
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob');
        ee()->load->model(['product_model', 'cartthrob_field_model', 'subscription_model']);

        $categories = (strpos(ee()->TMPL->tagdata, '{categories') !== false) ? ee()->product_model->get_categories() : false;

        if ($categories) {
            ee()->cartthrob_entries_model->load_categories_by_entry_id(ee()->cartthrob->cart->product_ids());
        }

        if (preg_match_all('#{packages?(.*?)}(.*?){/packages?}#s', ee()->TMPL->tagdata, $matches)) {
            $package_tagdata = [];

            foreach ($matches[0] as $i => $full_match) {
                $package_tagdata[substr($full_match, 1, -1)] = $matches[2][$i];
            }
        }

        foreach (ee()->cartthrob->cart->items() as $row_id => $item) {
            if (($entry_ids && !in_array($item->product_id(), $entry_ids))
                || ($row_ids && !in_array($row_id, $row_ids))
                || ($plan_ids && !in_array($item->meta('plan_id'), $plan_ids))
            ) {
                continue;
            }

            $row = ee()->cartthrob_variables->item_vars($item, $global_vars);

            if (isset($package_tagdata)) {
                foreach ($package_tagdata as $full_match => $_package_tagdata) {
                    $row[$full_match] = '';

                    foreach (ee()->cartthrob_variables->sub_item_vars($item, $global_vars,
                        $_package_tagdata) as $sub_row) {
                        $row[$full_match] .= ee()->TMPL->parse_variables($_package_tagdata, [$sub_row]);
                    }
                }
            }

            $row['is_subscription'] = ($item->meta('subscription')) ? 1 : 0;

            $keys = ee()->subscription_model->option_keys();

            foreach ($keys as $v) {
                $row['subscription_' . $v] = $item->meta('subscription_options') ? element($v, $item->meta('subscription_options')) : null;
            }

            $row['is_package'] = ($item->sub_items()) ? 1 : 0;
            $row['item_options'] = ($item->item_options()) ? count($item->item_options()) : 0;

            $data[] = $row;
        }

        // alternate for nested tag
        if (preg_match('/' . LD . 'if no_items' . RD . '(.*?)' . LD . '\/if' . RD . '/s', ee()->TMPL->tagdata, $match)) {
            ee()->TMPL->tagdata = str_replace($match[0], '', ee()->TMPL->tagdata);
            ee()->TMPL->no_results = $match[1];
        }

        if (!$data) {
            return ee()->TMPL->no_results();
        }

        ee()->load->library('data_filter');

        $order_by = (ee()->TMPL->fetch_param('order_by')) ? ee()->TMPL->fetch_param('order_by') : ee()->TMPL->fetch_param('orderby');

        ee()->data_filter->sort($data, $order_by, ee()->TMPL->fetch_param('sort'));
        ee()->data_filter->limit($data, ee()->TMPL->fetch_param('limit'), ee()->TMPL->fetch_param('offset'));

        ee()->template_helper->apply_search_filters($data);

        if (!$data) {
            return ee()->TMPL->no_results();
        }

        $count = 1;
        $total_results = count($data);

        foreach ($data as &$row) {
            $row['cart_count'] = $count;
            $row['cart_total_results'] = $total_results;
            $row['first_row'] = ($count === 1) ? true : false;
            $row['last_row'] = ($count === $total_results) ? true : false;

            $count++;
        }

        $return_data = ee()->template_helper->parse_variables($data);

        // if there are unparsed file paths in the return data, parse {filedir_n}
        $return_data = ee()->template_helper->parse_files($return_data);

        return $return_data;
    }

    /**
     * Returns total discount amount for cart
     *
     * @return mixed
     */
    public function cart_discount()
    {
        $value = ee()->cartthrob->cart->discount();

        switch (tag_param(2)) {
            case 'numeric':
                return $value;

            case 'minus_tax':
                /*
                 * We are ADDING the tax amount, because the discount will INCREASE due to the offset of the reduced tax
                 * applied to everything else based on this discount. Technically the discount is a negative amount... flip
                 * your brain... we're representing the total negative amount applied to the cart.
                 */
                $value = ee()->cartthrob->cart->discount() + ee()->cartthrob->cart->discount_tax();

                if (tag_param_equals(3, 'numeric')) {
                    return $value;
                }
                break;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns discount percentage of total
     *
     * @return float
     */
    public function cart_discount_percent_of_total()
    {
        return ee()->cartthrob->cart->discount() / ee()->cartthrob->cart->total() * 100;
    }

    /**
     * Returns discount percentage of subtotal
     *
     * @return float
     */
    public function cart_discount_percent_of_subtotal()
    {
        return ee()->cartthrob->cart->discount() / ee()->cartthrob->cart->subtotal() * 100;
    }

    /**
     * Returns subtotal price of all items in cart
     *
     * @return mixed
     */
    public function cart_subtotal()
    {
        $value = ee()->cartthrob->cart->subtotal();

        switch (tag_param(2)) {
            case 'numeric':
                return $value;

            case 'plus_tax':
                $value = ee()->cartthrob->cart->subtotal_with_tax();

                if (tag_param_equals(3, 'numeric')) {
                    return $value;
                }
                break;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns subtotal price of all items in cart plus tax
     *
     * @return string
     */
    public function cart_subtotal_plus_tax()
    {
        $value = ee()->cartthrob->cart->subtotal_with_tax();

        if (tag_param_equals(2, 'numeric')) {
            return $value;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns subtotal price of all items in cart minus discount
     *
     * @return mixed
     */
    public function cart_subtotal_minus_discount()
    {
        $value = ee()->cartthrob->cart->subtotal() - ee()->cartthrob->cart->discount();

        switch (tag_param(2)) {
            case 'numeric':
                return $value;

            case 'plus_tax':
                $value = ee()->cartthrob->cart->subtotal_with_tax() - ee()->cartthrob->cart->discount();

                if (tag_param_equals(3, 'numeric')) {
                    return $value;
                }
                break;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns subtotal price of all items in cart plus shipping
     *
     * @return mixed
     */
    public function cart_subtotal_plus_shipping()
    {
        $value = ee()->cartthrob->cart->subtotal() + ee()->cartthrob->cart->shipping();

        if (tag_param_equals(2, 'numeric')) {
            return $value;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns total shipping price for cart
     *
     * @return mixed
     */
    public function cart_shipping()
    {
        $value = ee()->cartthrob->cart->shipping();

        switch (tag_param(2)) {
            case 'numeric':
                return $value;

            case 'plus_tax':
                $value = ee()->cartthrob->cart->shipping_plus_tax();

                if (tag_param_equals(3, 'numeric')) {
                    return $value;
                }
                break;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns total shipping price for cart plus tax
     *
     * @return mixed
     */
    public function cart_shipping_plus_tax()
    {
        $value = ee()->cartthrob->cart->shipping_plus_tax();

        if (tag_param_equals(2, 'numeric')) {
            return $value;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns total tax amount for cart
     *
     * @return string
     */
    public function cart_tax()
    {
        $value = ee()->cartthrob->cart->tax();

        if (tag_param_equals(2, 'numeric')) {
            return $value;
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                $value,
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Return the tax rate
     *
     * @return mixed
     */
    public function cart_tax_rate()
    {
        return ee()->cartthrob->store->tax_rate();
    }

    /**
     * Returns total price of all items in cart
     * The formula is subtotal + tax + shipping - discount
     *
     * @return string
     */
    public function cart_total()
    {
        if (tag_param_equals(2, 'numeric')) {
            return ee()->cartthrob->cart->total();
        }

        return sprintf('%s%s',
            ee()->TMPL->fetch_param('prefix', '$'),
            number_format(
                ee()->cartthrob->cart->total(),
                $decimals = (int)ee()->TMPL->fetch_param('decimals', 2),
                $decimalPoint = ee()->TMPL->fetch_param('dec_point', '.'),
                $thousandSeparator = ee()->TMPL->fetch_param('thousands_sep', ',')
            )
        );
    }

    /**
     * Returns the total weight of all items in the cart
     *
     * @return mixed
     */
    public function cart_weight()
    {
        return ee()->cartthrob->cart->weight();
    }

    public function change_quantity()
    {
        if ($item = ee()->cartthrob->cart->item(ee()->TMPL->fetch_param('row_id'))) {
            $item->set_quantity(ee()->TMPL->fetch_param('quantity'));
        }

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function check_cc_number_errors()
    {
        $data = [
            'errors' => '',
            'valid' => true,
        ];

        if (!ee()->TMPL->fetch_param('credit_card_number')) {
            $data['errors'] = ee()->lang->line('validate_cc_number_missing'); // return lang missing number.
        }

        $response = validate_credit_card(ee()->TMPL->fetch_param('credit_card_number'),
            ee()->TMPL->fetch_param('card_type'));

        if (!$response['valid']) {
            $data['errors'] = $response['error_code'];

            $data['valid'] = false;

            switch ($response['error_code']) {
                case '1':
                    $data['errors'] = ee()->lang->line('validate_cc_card_type_unknown');
                    break;
                case '2':
                    $data['errors'] = ee()->lang->line('validate_cc_card_type_mismatch');
                    break;
                case '3':
                    $data['errors'] = ee()->lang->line('validate_cc_invalid_card_number');
                    break;
                case '4':
                    $data['errors'] = ee()->lang->line('validate_cc_incorrect_card_length');
                    break;
                default:
                    $data['errors'] = ee()->lang->line('validate_cc_card_type_unknown');
            }
        }

        return ee()->template_helper->parse_variables_row($data);
    }

    /**
     * Empties the cart
     */
    public function clear_cart()
    {
        ee()->cartthrob->cart->clear()
            ->clear_coupon_codes()
            ->clear_shipping_info()
            ->clear_totals();

        if (bool_string(ee()->TMPL->fetch_param('clear_customer_info'))) {
            ee()->cartthrob->cart->clear_customer_info()
                ->clear_custom_data();
        }

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function clear_coupon_codes()
    {
        ee()->cartthrob->cart->clear_coupon_codes()->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function convert_country_code()
    {
        ee()->load->library('locales');

        $country_code = ee()->TMPL->fetch_param('country_code');
        $code = ee()->locales->alpha3_country_code($country_code);

        $countries = ee()->locales->all_countries();

        return (isset($countries[$code])) ? $countries[$code] : $country_code;
    }

    public function countries()
    {
        ee()->load->library('locales');

        $data = [];

        foreach (ee()->locales->countries(bool_string(ee()->TMPL->fetch_param('alpha2'))) as $abbrev => $country) {
            $data[] = [
                'country_code' => $abbrev,
                'countries:country_code' => $abbrev,
                'country' => $country,
                'countries:country' => $country,
            ];
        }

        return ee()->template_helper->parse_variables($data);
    }

    public function country_select()
    {
        ee()->load->library('locales');
        ee()->load->helper('form');

        $name = (ee()->TMPL->fetch_param('name')) ? ee()->TMPL->fetch_param('name') : 'country';

        $countries = ee()->locales->countries(
            bool_string(ee()->TMPL->fetch_param('alpha2')),
            bool_string(ee()->TMPL->fetch_param('country_codes'), true)
        );

        if (bool_string(ee()->TMPL->fetch_param('add_blank'))) {
            $blank = ['' => '---'];
            $countries = $blank + $countries;
        }

        $attrs = [];

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) !== ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }

        $selected = (ee()->TMPL->fetch_param('selected')) ? ee()->TMPL->fetch_param('selected') : ee()->TMPL->fetch_param('default');

        return form_dropdown(
            $name,
            $countries,
            $selected,
            $extra
        );
    }

    public function coupon_count()
    {
        return count(ee()->cartthrob->cart->coupon_codes());
    }

    public function coupon_info()
    {
        ee()->load->library('number');

        if (!$coupon_codes = ee()->cartthrob->cart->coupon_codes()) {
            return ee()->TMPL->no_results();
        }

        ee()->load->model('coupon_code_model');

        foreach ($coupon_codes as $coupon_code) {
            $row = array_key_prefix(ee()->coupon_code_model->get_coupon_code_data($coupon_code), 'coupon_');
            $row['coupon_code'] = $coupon_code;

            $entry_id = $row['coupon_metadata']['entry_id'];

            $discount_price = ee()->cartthrob->cart->discount(true, $entry_id, $coupon_code);

            $row['discount_amount'] = $row['coupon_amount'] = $row['voucher_amount'] = ee()->number->format($discount_price);

            unset($row['coupon_metadata']);

            $variables[] = array_merge(ee()->cartthrob_entries_model->entry_vars($entry_id), $row);
        }

        return ee()->template_helper->parse_variables($variables);
    }

    public function discount_info()
    {
        ee()->load->model('discount_model');
        ee()->load->library('number');

        if (!$discounts = ee()->discount_model->get_valid_discounts()) {
            return ee()->TMPL->no_results();
        }

        foreach ($discounts as $discount) {
            $row = [];

            foreach ($discount as $key => $value) {
                if (strpos($key, 'discount_') !== 0) {
                    $key = 'discount_' . $key;
                }

                $row[$key] = $value;
            }

            $discount_price = ee()->cartthrob->cart->discount(true, $discount['entry_id']);
            $row['discount_amount'] = ee()->number->format($discount_price);

            $row = array_merge(ee()->cartthrob_entries_model->entry_vars($row['discount_entry_id']), $row);

            $variables[] = $row;
        }

        return ee()->template_helper->parse_variables($variables);
    }

    public function customer_info()
    {
        return ee()->template_helper->parse_variables_row(ee()->cartthrob_variables->global_variables());
    }

    /**
     * debug_info
     * Outputs all data related to CartThrob
     *
     * @return string
     */
    public function debug_info()
    {
        if (!ee()->cartthrob->store->config('show_debug')) {
            return;
        } elseif (ee()->cartthrob->store->config('show_debug') == 'super_admins') {
            if (ee()->session->userdata('group_id') !== '1') {
                return;
            }
        }

        $debug['session'] = ee()->cartthrob_session->toArray();
        $debug = array_merge($debug, ee()->cartthrob->cart->toArray());

        uksort($debug, 'strnatcasecmp');

        if (bool_string(ee()->TMPL->fetch_param('console'))) {
            ee()->load->library('javascript');

            return '<script type="text/javascript">(function(data) { if (typeof(window.console) == "undefined") return; window.console.log(data) })(' . json_encode($debug) . ')</script>';
        }

        $output = '<fieldset id="ct_debug_info" style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#ffbc9f ">';
        $output .= '<legend style="color:#000;">&nbsp;&nbsp;' . ee()->lang->line('cartthrob_profiler_data') . '  </legend>';

        $output .= $this->format_debug($debug);

        $output .= '</table>';
        $output .= '</fieldset>';

        return $output;
    }

    // --------------------------------
    //  Debug Info
    // --------------------------------

    /**
     * format_debug
     * Formats debug arrays into tables
     *
     * @return string
     */
    private function format_debug($data, $parent_key = null)
    {
        $output = '';
        if (is_array($data)) {
            uksort($data, 'strnatcasecmp');
            $output = "<table style='width:100%;'>";
            foreach ($data as $key => $value) {
                $content = '';
                $output_key = $key;
                if (is_numeric($key)) {
                    $output_key = 'Row ID: ' . $key;
                }
                if (is_array($value)) {
                    $content .= $this->format_debug($value, $key);
                } else {
                    if ($key == 'inventory' && $value == PHP_INT_MAX) {
                        $value = 'unlimited';
                    }
                    if ($key == 'price') {
                        if ($value == '' && $parent_key !== null) {
                            $item = ee()->cartthrob->cart->item($parent_key);

                            if ($item) {
                                ee()->load->model('cartthrob_field_model');
                                $field_id = ee()->cartthrob->store->config('product_channel_fields',
                                    $item->meta('channel_id'), $key);

                                $field_name = 'channel entry';
                                if (ee()->cartthrob->store->config('product_channel_fields', $item->meta('channel_id'),
                                    'global_price')) {
                                    $field_name = 'globally set';
                                } elseif ($field_id) {
                                    $field_name = ee()->cartthrob_field_model->get_field_name($field_id) . ' field';
                                }

                                $value = $item->price() . ' (uses ' . $field_name . ' price)';
                            }
                        } else {
                            $value = $value . ' (uses customer price)';
                        }
                    }
                    if ($key == 'entry_id' && empty($value)) {
                        $value = '(dynamic item)';
                    }
                    $content .= htmlspecialchars($value);
                }
                $output .= "<tr><td style='padding:5px; vertical-align: top;color:#900;background-color:#ddd;'>" . $output_key . "&nbsp;&nbsp;</td><td style='padding:5px; color:#000;background-color:#ddd;'>" . $content . "</td></tr>\n";
            }
            $output .= '</table>';
        } else {
            $output = htmlspecialchars($data);
        }

        return $output;
    }

    /**
     * decrypt
     *
     * Encrypts and returns a string.
     * @param string $TMPL ->fetch_param('string') the data to be decrypted
     * @param string $TMPL ->fetch_param('key') the key used to encrypt the data
     * @return string decrypted string
     **/
    public function decrypt()
    {
        return ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->TMPL->fetch_param('string'))),
            ee()->TMPL->fetch_param('key')));
    }

    public function delete_from_cart()
    {
        if (ee()->extensions->active_hook('cartthrob_delete_from_cart_start') === true) {
            ee()->extensions->call('cartthrob_delete_from_cart_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        if (ee()->TMPL->fetch_param('row_id') !== false) {
            ee()->cartthrob->cart->remove_item(ee()->TMPL->fetch_param('row_id'));
        } else {
            if (ee()->TMPL->fetch_param('entry_id')) {
                $data = ['entry_id' => ee('Security/XSS')->clean(ee()->TMPL->fetch_param('entry_id'))];

                foreach (ee()->TMPL->tagparams as $key => $value) {
                    if (preg_match('/^item_options?:(.*)$/', $key, $match)) {
                        $data['item_options'][$match[1]] = $value;
                    }
                }

                if (ee()->input->post('item_options') && is_array(ee()->input->post('item_options'))) {
                    $data['item_options'] = (isset($data['item_options'])) ? array_merge($data['item_options'],
                        ee()->input->post('item_options', true)) : ee()->input->post('item_options', true);
                }

                if ($item = ee()->cartthrob->cart->find_item($data)) {
                    $item->remove();
                }
            }
        }

        if (ee()->extensions->active_hook('cartthrob_delete_from_cart_end') === true) {
            ee()->extensions->call('cartthrob_delete_from_cart_end');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function delete_from_cart_form()
    {
        if (!ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('form_builder');

        $data = ee()->cartthrob_variables->global_variables(true);

        ee()->form_builder->initialize([
            'form_data' => [
                'secure_return',
                'row_id',
                'return',
            ],
            'classname' => 'Cartthrob',
            'method' => 'delete_from_cart_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            // 'secure_action' => bool_string()
        ]);

        return ee()->form_builder->form();
    }

    /**
     * download_file
     *
     * This uses curl for URLs, or fopen for paths to download files.
     *
     * @param string $TMPL ->fetch_param('file')
     * @param string $TMPL ->fetch_param('return')
     * @param
     **/
    public function download_file()
    {
        ee()->load->library('paths');

        if (ee()->TMPL->fetch_param('field') && ee()->TMPL->fetch_param('entry_id')) {
            ee()->load->model(['cartthrob_field_model', 'cartthrob_entries_model', 'tools_model']);

            $entry = ee()->cartthrob_entries_model->entry(ee()->TMPL->fetch_param('entry_id'));

            ee()->load->helper('array');

            if ($path = element(ee()->TMPL->fetch_param('field'), $entry)) {
                ee()->load->library('paths');

                $path = ee()->paths->parse_file_server_paths($path);

                ee()->TMPL->tagparams['file'] = $path;
            }
        }

        if (ee()->TMPL->fetch_param('member_id') !== false) {
            if (!ee()->TMPL->fetch_param('member_id')) {
                return show_error(ee()->lang->line('download_file_not_authorized'));
            }

            if (bool_string(ee()->TMPL->fetch_param('encrypted'))) {
                if (ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_encode(rawurldecode(ee()->TMPL->fetch_param('member_id'))))) != ee()->session->userdata('member_id')) {
                    return show_error(ee()->lang->line('download_file_not_authorized'));
                }
            } else {
                if (ee()->TMPL->fetch_param('member_id') != ee()->session->userdata['member_id']) {
                    return show_error(ee()->lang->line('download_file_not_authorized'));
                }
            }
        }
        if (!ee()->TMPL->fetch_param('file')) {
            return show_error(ee()->lang->line('download_url_not_specified'));
        } else {
            $post_url = ee()->TMPL->fetch_param('file');
        }

        if (bool_string(ee()->TMPL->fetch_param('encrypted'))) {
            $post_url = ee('Security/XSS')->clean(ee('Encrypt')->decode(base64_decode(rawurldecode($post_url))));
        }

        ee()->load->library('cartthrob_file');

        ee()->cartthrob_file->force_download($post_url);

        if (ee()->cartthrob_file->errors()) {
            return show_error(ee()->cartthrob_file->errors());
        }
    }

    public function download_file_form()
    {
        if (ee()->TMPL->fetch_param('member_id')) {
            if (in_array(ee()->TMPL->fetch_param('member_id'),
                ['CURRENT_USER', '{logged_in_member_id}', '{member_id}'])) {
                ee()->TMPL->tagparams['member_id'] = ee()->session->userdata('member_id');
            } else {
                ee()->TMPL->tagparams['member_id'] = sanitize_number(ee()->TMPL->fetch_param('member_id'));
            }
        }

        if (ee()->TMPL->fetch_param('group_id')) {
            if (in_array(ee()->TMPL->fetch_param('group_id'), ['{logged_in_group_id}', '{group_id}'])) {
                ee()->TMPL->tagparams['group_id'] = ee()->session->userdata('group_id');
            } else {
                ee()->TMPL->tagparams['group_id'] = sanitize_number(ee()->TMPL->fetch_param('group_id'));
            }
        }

        if (ee()->TMPL->fetch_param('field') && ee()->TMPL->fetch_param('entry_id')) {
            ee()->load->model(['cartthrob_field_model', 'cartthrob_entries_model', 'tools_model']);

            $entry = ee()->cartthrob_entries_model->entry(ee()->TMPL->fetch_param('entry_id'));

            ee()->load->helper('array');
            // @NOTE if the developer has assigned an entry id and a field, but there's nothing IN the field,  then the path doesn't get set, and no debug information is output, because path, below would be set to NULL
            if ($path = element(ee()->TMPL->fetch_param('field'), $entry)) {
                ee()->load->library('paths');

                $path = ee()->paths->parse_file_server_paths($path);

                ee()->TMPL->tagparams['file'] = $path;
            }
        }

        if (bool_string(ee()->TMPL->fetch_param('debug')) && ee()->TMPL->fetch_param('file')) {
            ee()->load->library('cartthrob_file');
            ee()->TMPL->tagdata .= ee()->cartthrob_file->file_debug(ee()->TMPL->fetch_param('file'));
        }

        ee()->load->library('form_builder');

        $data = ee()->cartthrob_variables->global_variables(true);

        if (in_array(ee()->TMPL->fetch_param('member_id'),
            ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
            ee()->TMPL->tagparams['member_id'] = ee()->session->userdata('member_id');
        }

        if (in_array(ee()->TMPL->fetch_param('group_id'), ['{group_id}', '{logged_in_group_id}'])) {
            ee()->TMPL->tagparams['group_id'] = ee()->session->userdata('group_id');
        }

        if (ee()->TMPL->fetch_param('free_file')) {
            ee()->TMPL->tagparams['free_file'] = 'FI' . ee()->TMPL->fetch_param('free_file');
        } else {
            if (ee()->TMPL->fetch_param('file') && (!ee()->TMPL->fetch_param('member_id') && !ee()->TMPL->fetch_param('group_id'))) {
                ee()->TMPL->tagparams['free_file'] = 'FI' . ee()->TMPL->fetch_param('file');
            } elseif (ee()->TMPL->fetch_param('file')) {
                ee()->TMPL->tagparams['file'] = 'FP' . ee()->TMPL->fetch_param('file');
            }
        }

        ee()->form_builder->initialize([
            'form_data' => [
                'secure_return',
                'language',
            ],
            'encoded_form_data' => [
                'file' => 'FP',
                'free_file' => 'FI',
            ],
            'encoded_numbers' => [
                'member_id' => 'MI',
                'group_id' => 'GI',
            ],
            'classname' => 'Cartthrob',
            'method' => 'download_file_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
        ]);

        return ee()->form_builder->form();
    }

    public function duplicate_item()
    {
        ee()->cartthrob->cart->duplicate_item(ee()->TMPL->fetch_param('row_id'));

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    /**
     * encrypt
     *
     * Encrypts and returns a string.
     * @param string $string | $TMPL->fetch_param('string') the data to be encrypted
     * @param string $key | $TMPL->fetch_param('key') the text string key that will be used to encrypt the data
     * @return string encrypted string
     **/
    public function encrypt()
    {
        return rawurlencode(base64_encode(ee('Encrypt')->encode(ee()->TMPL->fetch_param('string'),
            ee()->TMPL->fetch_param('key'))));
    }

    public function https_redirect()
    {
        ee()->load->helper('https');

        force_https(ee()->TMPL->fetch_param('domain'), (ee()->config->item('send_headers') === 'y'));

        if (bool_string(ee()->TMPL->fetch_param('secure_site_url'))) {
            ee()->config->config['site_url'] = str_replace('http://', 'https://', ee()->config->item('site_url'));
        }

        return ee()->TMPL->tagdata;
    }

    /**
     * @return string credit card type, ex. Amex, Visa, Mc, Discover
     */
    public function get_card_type()
    {
        return card_type(ee()->TMPL->fetch_param('credit_card_number'));
    }

    public function get_cartthrob_logo()
    {
        ee()->load->helper(['html', 'url']);

        return anchor(
            'http://cartthrob.com',
            img([
                'src' => 'http://cartthrob.com/images/powered_by_logos/powered_by_cartthrob.png',
                'alt' => ee()->lang->line('powered_by_title'),
            ]),
            [
                'title' => ee()->lang->line('powered_by_title'),
                'onclick' => "javascript:window.open('http://cartthrob.com','cartthrob');return false;",
            ]
        );
    }

    /**
     * Returns string of entry_id's separated by | for use in weblog:entries
     *
     * @param $IN ->GBL('price_min')
     * @param $IN ->GBL('price_max')
     * @return string
     */
    public function get_items_in_range()
    {
        $price_min = (ee()->TMPL->fetch_param('price_min') !== false) ? ee('Security/XSS')->clean(ee()->TMPL->fetch_param('price_min')) : ee()->input->get_post('price_min',
            true);

        $price_max = (ee()->TMPL->fetch_param('price_max') !== false) ? ee('Security/XSS')->clean(ee()->TMPL->fetch_param('price_max')) : ee()->input->get_post('price_max',
            true);

        if (!is_numeric($price_min)) {
            $price_min = '';
        }
        if (!is_numeric($price_max)) {
            $price_max = '';
        }

        if ($price_min == '' && $price_max == '') {
            return '';
        }

        ee()->load->model('product_model');

        $entry_ids = ee()->product_model->get_products_in_price_range($price_min, $price_max);

        if (count($entry_ids)) {
            return implode('|', $entry_ids);
        } else {
            return null;
        }
    }

    /**
     * Returns the options from the selected shipping plugin
     *
     * @return string
     */
    public function get_shipping_options()
    {
        ee()->load->library('api/api_cartthrob_shipping_plugins');

        if (ee()->TMPL->fetch_param('shipping_plugin')) {
            ee()->api_cartthrob_shipping_plugins->set_plugin(ee()->TMPL->fetch_param('shipping_plugin'));
        }

        $options = ee()->api_cartthrob_shipping_plugins->shipping_options();
        $tagData = trim(ee()->TMPL->tagdata);

        if (!$options && !$tagData) {
            if (ee()->cartthrob->cart->custom_data('shipping_error')) {
                $option['price'] = '';
                $option['option_value'] = '';
                $option['option_name'] = '';
                $option['checked'] = '';
                $option['selected'] = '';
                $option['count'] = 0;
                $option['first_row'] = false;
                $option['last_row'] = false;
                $option['total_results'] = 0;

                $options['error_message'] = ee()->cartthrob->cart->custom_data('shipping_error');

                return ee()->template_helper->parse_variables_row($options);
            }
        }

        $selected = (ee()->cartthrob->cart->shipping_info('shipping_option')) ?
            ee()->cartthrob->cart->shipping_info('shipping_option') :
            ee()->api_cartthrob_shipping_plugins->default_shipping_option();

        if (!$tagData) {
            if (!$options) {
                return null;
            }

            $attrs = [];

            if (ee()->TMPL->fetch_param('id')) {
                $attrs['id'] = ee()->TMPL->fetch_param('id');
            }

            if (ee()->TMPL->fetch_param('class')) {
                $attrs['class'] = ee()->TMPL->fetch_param('class');
            }

            if (ee()->TMPL->fetch_param('onchange')) {
                $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
            }

            $extra = '';

            if ($attrs) {
                $extra .= _attributes_to_string($attrs);
            }

            if (ee()->TMPL->fetch_param('extra')) {
                if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                    $extra .= ' ';
                }

                $extra .= ee()->TMPL->fetch_param('extra');
            }

            $selectOptions = [];

            foreach ($options as $row) {
                if (bool_string(ee()->TMPL->fetch_param('hide_price'))) {
                    $selectOptions[$row['rate_short_name']] = $row['rate_title'];
                } else {
                    $selectOptions[$row['rate_short_name']] = $row['rate_title'] . ' - ' . $row['price'];
                }
            }

            if (!empty($selectOptions)) {
                return form_dropdown(
                    'shipping_option',
                    $selectOptions,
                    $selected,
                    $extra
                );
            }

            return null;
        }

        ee()->load->library('number');

        $newOptions = [];

        foreach ($options as $key => $option) {
            if (empty($option['rate_short_name']) || empty($option['rate_title'])) {
                continue;
            }

            if (isset($count)) {
                $count++;
            } else {
                $count = 1;
            }

            $option['price'] = ee()->number->format($option['price']);
            $option['option_value'] = $option['rate_short_name'];
            $option['option_name'] = $option['rate_title'];
            $option['checked'] = ($option['rate_short_name'] == $selected) ? ' checked="checked"' : '';
            $option['selected'] = ($option['rate_short_name'] == $selected) ? ' selected="selected"' : '';
            $option['count'] = $count;
            $option['first_row'] = ($count === 1) ? true : false;
            $option['last_row'] = ($count === count($options)) ? true : false;
            $option['total_results'] = count($options);
            $option['error_message'] = null;

            if (ee()->cartthrob->cart->custom_data('shipping_error')) {
                $option['error_message'] = ee()->cartthrob->cart->custom_data('shipping_error');
            }

            $newOptions[] = $option;
        }

        return ee()->template_helper->parse_variables($newOptions);
    }

    // does not show content if shipping rates require update

    public function has_shippable_items()
    {
        foreach (ee()->cartthrob->cart->items() as $row_id => $item) {
            $product = ($item->product_id()) ? ee()->product_model->get_product($item->product_id()) : false;

            if ($product) {
                $data = ee()->cartthrob_entries_model->entry_vars($product);

                if ($data && isset($data['product_shippable']) && $data['product_shippable'] == 'Yes') {
                    return true;
                }
            }
        }

        return false;
    }

    public function in_array()
    {
        $needle = ee()->TMPL->fetch_param('needle');

        $haystack = (ee()->TMPL->fetch_param('haystack')) ? explode('|', ee()->TMPL->fetch_param('haystack')) : [];

        return (in_array($needle, $haystack)) ? '1' : 0;
    }

    // @TODO this needs some serious work, it relies on a field specifically called product_shippable and also assumes it's value is "Yes"

    /**
     * Returns a conditional whether item has been purchased
     *
     * @param string $TMPL ->fetch_param('entry_id')
     * @return string (int)
     */
    public function is_purchased_item()
    {
        // @TODO add in the ability to pull up items with a particular status
        // or recognize only completed itms.

        $entry_id = ee()->TMPL->fetch_param('entry_id');

        ee()->load->model('purchased_items_model');

        $data['is_purchased_item'] = ee()->purchased_items_model->has_purchased(ee()->TMPL->fetch_param('entry_id'));

        // single tag
        if (!ee()->TMPL->tagdata) {
            return (int)$data['is_purchased_item'];
        }

        return ee()->template_helper->parse_variables_row($data);
    }

    public function is_in_cart()
    {
        $data['is_in_cart'] = (int)(ee()->TMPL->fetch_param('entry_id') && ee()->cartthrob->cart->find_item(['entry_id' => ee()->TMPL->fetch_param('entry_id')]));

        // single tag
        if (!ee()->TMPL->tagdata) {
            return $data['is_in_cart'];
        }

        $data['item_in_cart'] = $data['is_in_cart'];

        return ee()->template_helper->parse_variables_row($data);
    }

    /**
     * For use in a conditional, returns whether or not customer_info has been saved
     *
     * @return string
     */
    public function is_saved()
    {
        foreach (ee()->cartthrob->cart->customer_info() as $key => $value) {
            if (!empty($value)) {
                return '1';
            }
        }

        return 0;
    }

    public function item_options()
    {
        ee()->load->helper('inflector');
        ee()->load->helper('array');

        $entry_id = ee()->TMPL->fetch_param('entry_id');

        $row_id = ee()->TMPL->fetch_param('row_id');

        if (!$entry_id && $row_id === false) {
            return ee()->TMPL->no_results();
        }

        $item = false;
        $parent_id = false;
        $item_row_id = false;
        $option_value = false;
        $selected = false;
        if (strpos($row_id, 'configurator:') !== false) {
            $item = ee()->cartthrob->cart->item($row_id);
        } elseif (strpos($row_id, ':') !== false) {
            list($parent_id, $item_row_id) = explode(':', $row_id);
            if ($parent_item = ee()->cartthrob->cart->item($parent_id)) {
                $item = $parent_item->sub_item($item_row_id);
            }
        } else {
            $item = ee()->cartthrob->cart->item($row_id);
        }
        if ($item && $item->product_id()) {
            $entry_id = $item->product_id();
        }

        $price_modifiers = ee()->product_model->get_all_price_modifiers($entry_id);
        if ($row_id === false) {
            if (ee()->cartthrob->cart->meta('all_item_options')) {
                $all_keys = ee()->cartthrob->cart->meta('all_item_options');
                foreach ($price_modifiers as $key => $value) {
                    if (in_array($key, $all_keys)) {
                        unset($price_modifiers[$key]);
                    }
                }
            }
        }
        // this will be an array of option field name => bool is dynamic
        $item_options = [];

        foreach (array_keys($price_modifiers) as $key) {
            // not dyanmic
            $item_options[$key] = false;
        }

        if ($item) {
            $conf = $item->meta('configuration');

            if (is_array($item->item_options())) {
                foreach (array_keys($item->item_options()) as $key) {
                    if (!isset($item_options[$key])) {
                        // dynamic
                        $item_options[$key] = true;
                    }

                    if ($conf && is_array($conf)) {
                        foreach ($conf as $k => $v) {
                            if (array_key_exists($k, $item_options)) {
                                unset($item_options[$k]);
                                continue;
                            }
                        }
                    }
                }
            }
        }
        $return_data = '';

        // if I leave {selected} in there, assign_variables output is wrong
        ee()->TMPL->tagdata = str_replace('{selected}', '8bdb34edd2d86eff7aa60be77e3002f5', ee()->TMPL->tagdata);
        $variables = ee('Variables/Parser')->extractVariables(ee()->TMPL->tagdata);
        ee()->TMPL->var_single = $variables['var_single'];
        ee()->TMPL->var_pair = $variables['var_pair'];
        ee()->TMPL->tagdata = str_replace('8bdb34edd2d86eff7aa60be77e3002f5', '{selected}', ee()->TMPL->tagdata);

        $tagdata = ee()->TMPL->tagdata;

        // only use one field instead of all fields
        $fields = ee()->TMPL->fetch_param('field') ? explode('|', ee()->TMPL->fetch_param('field')) : false;

        $count = 0;
        foreach ($item_options as $field_name => $dynamic) {
            if ($fields && !in_array($field_name, $fields)) {
                continue;
            }
            ++$count;
            ee()->TMPL->tagdata = $tagdata;

            // for early parsing
            ee()->TMPL->tagdata = ee()->TMPL->swap_var_single('option_field', $field_name, ee()->TMPL->tagdata);
            // add this line for dynamic options
            $option_value = ($item) ? $item->item_options($field_name) : '';
            if ($item && $item->is_sub_item() && $entry = ee()->cartthrob_entries_model->entry($item->parent_item()->product_id())) {
                // already in the cart
                $item_row_id = $item->row_id();
                $option_value = ($item) ? $item->item_options($field_name) : '';
            } elseif ($parent_id && $entry = ee()->cartthrob_entries_model->entry($parent_id)) {
                // just getting the entry
            }

            $vars = [];
            $vars['allow_selection'] = 1;

            if ($item_row_id !== false) {
                if ($field_id = ee()->cartthrob_field_model->channel_has_fieldtype($entry['channel_id'],
                    'cartthrob_package', true)) {
                    ee()->load->library('api');

                    ee()->legacy_api->instantiate('channel_fields');

                    if (empty(ee()->api_channel_fields->field_types)) {
                        ee()->api_channel_fields->fetch_installed_fieldtypes();
                    }

                    if (ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                        if (!isset(ee()->session->cache['cartthrob']['cartthrob_package'][$entry['entry_id']][$field_id])) {
                            ee()->session->cache['cartthrob']['cartthrob_package'][$entry['entry_id']][$field_id] = ee()->api_channel_fields->apply('pre_process',
                                [$entry['field_id_' . $field_id]]);
                        }

                        $field_data = ee()->session->cache['cartthrob']['cartthrob_package'][$entry['entry_id']][$field_id];

                        if (isset($field_data[$item_row_id]) && empty($field_data[$item_row_id]['allow_selection'][$field_name])) {
                            $vars['allow_selection'] = 0;
                        }

                        if (!$item) {
                            if (isset($field_data[$item_row_id]) && isset($field_data[$item_row_id]['option_presets'][$field_name])) {
                                if (!in_array($field_data[$item_row_id]['option_presets'][$field_name],
                                    [null, ''])) {
                                    $option_value = $field_data[$item_row_id]['option_presets'][$field_name];
                                    $selected = $option_value;
                                }
                            }
                        }
                    }
                }
            }

            $vars = array_merge(ee()->cartthrob_variables->item_option_vars($entry_id, $row_id, $field_name, $selected),
                $vars);

            $vars['option_field'] = $field_name;
            $vars['option_label'] = $vars['item_options:option_label'] = ee()->cartthrob_field_model->get_field_label(ee()->cartthrob_field_model->get_field_id($field_name));
            $vars['field_type'] = ee()->cartthrob_field_model->get_field_type(ee()->cartthrob_field_model->get_field_id($field_name));
            $vars['configuration_label'] = null;
            if ($vars['field_type'] == 'cartthrob_price_modifiers_configurator' && strpos($vars['option_field'],
                    ':') === false) {
                $vars['configuration_label'] = $vars['option_label'];
            }

            $vars['item_options_total_results'] = count($item_options);
            $vars['item_options_count'] = $count;
            $vars['dynamic'] = $dynamic;
            $vars['option_value'] = $option_value;
            $vars['options_exist'] = (isset($price_modifiers[$field_name]) && count($price_modifiers[$field_name]) > 0) ? (int)(count($price_modifiers[$field_name])) : false;
            if (empty($vars['option_label'])) {
                $labels = ee()->cartthrob->cart->meta('item_option_labels');

                if (isset($labels[$vars['option_field']])) {
                    $vars['option_label'] = $vars['item_options:option_label'] = $labels[$vars['option_field']];
                } else {
                    $vars['option_label'] = $vars['item_options:option_label'] = humanize($field_name);
                }
            }

            $return_data .= ee()->template_helper->parse_variables_row($vars);
        }

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob');

        return $return_data;
    }

    public function member_downloads()
    {
        if (!ee()->session->userdata('member_id')) {
            return ee()->TMPL->no_results();
        }

        ee()->load->model('cartthrob_entries_model');

        return ee()->cartthrob_entries_model->channel_entries([
            'dynamic' => 'no',
            'author_id' => ee()->session->userdata('member_id'),
            'channel_id' => ee()->cartthrob->store->config('purchased_items_channel'),
        ]);
    }

    public function multi_add_to_cart_form()
    {
        if (!ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('languages');

        ee()->languages->set_language(ee()->TMPL->fetch_param('language'));

        $TMPL = [
            'tagdata' => ee()->TMPL->tagdata,
            'var_single' => ee()->TMPL->var_single,
            'var_pair' => ee()->TMPL->var_pair,
            'tagparams' => ee()->TMPL->tagparams,
        ];

        foreach ($TMPL as $key => $value) {
            ee()->TMPL->{$key} = $value;
        }

        ee()->load->library('form_builder');

        $data = array_merge(
            ee()->cartthrob_variables->item_option_vars(),
            ee()->cartthrob_variables->global_variables(true)
        );

        ee()->form_builder->initialize([
            'classname' => 'Cartthrob',
            'method' => 'multi_add_to_cart_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            'form_data' => [
                'secure_return',
                'language',
                'return',
            ],
            'encoded_form_data' => [
                'shipping' => 'SHP',
                'weight' => 'WGT',
                'permissions' => 'PER',
                'upload_directory' => 'UPL',
                'class' => 'CLS',
            ],
            'encoded_bools' => [
                'allow_user_price' => 'AUP',
                'allow_user_shipping' => 'AUS',
                'allow_user_weight' => 'AUW',
                // 'show_errors' => array('ERR', TRUE),
                'on_the_fly' => 'OTF',
                'json' => 'JSN',
                'tax_exempt' => 'TXE',
                'shipping_exempt' => 'SHX',
            ],
        ]);

        if (bool_string(ee()->TMPL->fetch_param('no_tax'))) {
            ee()->form_builder->set_encoded_bools('no_tax', 'NTX')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('tax_exempt'))) {
            ee()->form_builder->set_encoded_bools('tax_exempt', 'NTX')->set_params(ee()->TMPL->tagparams);
        }

        if (bool_string(ee()->TMPL->fetch_param('no_shipping'))) {
            ee()->form_builder->set_encoded_bools('no_shipping', 'NSH')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('shipping_exempt'))) {
            ee()->form_builder->set_encoded_bools('shipping_exempt', 'NSH')->set_params(ee()->TMPL->tagparams);
        }

        return ee()->form_builder->form();
    }

    public function new_cart()
    {
        ee()->cartthrob->cart->initialize()->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function order_items()
    {
        $order_ids = (ee()->TMPL->fetch_param('order_id')) ? explode('|', ee()->TMPL->fetch_param('order_id')) : false;
        $entry_ids = (ee()->TMPL->fetch_param('entry_id')) ? explode('|', ee()->TMPL->fetch_param('entry_id')) : false;
        $member_ids = (ee()->TMPL->fetch_param('member_id')) ? explode('|',
            str_replace(['CURRENT_USER', '{logged_in_member_id}', '{member_id}'],
                ee()->session->userdata('member_id'), ee()->TMPL->fetch_param('member_id'))) : false;

        ee()->load->model(['order_model', 'product_model']);

        ee()->load->library('number');

        $data = ee()->order_model->get_order_items($order_ids, $entry_ids, $member_ids);

        if (!$data) {
            return ee()->TMPL->no_results();
        }

        ee()->load->library('api');

        ee()->legacy_api->instantiate('channel_fields');

        ee()->api_channel_fields->include_handler('cartthrob_order_items');

        ee()->load->model('cartthrob_entries_model');

        if (!ee()->api_channel_fields->setup_handler('cartthrob_order_items')) {
            return '';
        }

        if (ee()->TMPL->fetch_param('variable_prefix')) {
            ee()->api_channel_fields->field_types['cartthrob_order_items']->variable_prefix = ee()->TMPL->fetch_param('variable_prefix');
        }

        ee()->api_channel_fields->apply('pre_process', [$data]);

        $return_data = ee()->api_channel_fields->apply('replace_tag', [$data, ee()->TMPL->tagparams, ee()->TMPL->tagdata]);

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob');

        return $return_data;
    }

    public function order_totals()
    {
        ee()->load->library('number');

        $data = [
            'total' => 0,
            'subtotal' => 0,
            'tax' => 0,
            'shipping' => 0,
            'discount' => 0,
            'count' => 0,
        ];

        if (ee()->cartthrob->store->config('orders_channel')) {
            ee()->load->model('cartthrob_entries_model');

            if ($query = ee()->cartthrob_entries_model->channel_entries(['channel_id' => ee()->cartthrob->store->config('orders_channel')],
                true)) {
                $data['count'] = $query->num_rows();

                foreach ($query->result_array() as $row) {
                    if (ee()->cartthrob->store->config('orders_total_field') && isset($row['field_id_' . ee()->cartthrob->store->config('orders_total_field')])) {
                        $data['total'] += sanitize_number($row['field_id_' . ee()->cartthrob->store->config('orders_total_field')]);
                    }

                    if (ee()->cartthrob->store->config('orders_subtotal_field') && isset($row['field_id_' . ee()->cartthrob->store->config('orders_subtotal_field')])) {
                        $data['subtotal'] += sanitize_number($row['field_id_' . ee()->cartthrob->store->config('orders_subtotal_field')]);
                    }

                    if (ee()->cartthrob->store->config('orders_tax_field') && isset($row['field_id_' . ee()->cartthrob->store->config('orders_tax_field')])) {
                        $data['tax'] += sanitize_number($row['field_id_' . ee()->cartthrob->store->config('orders_tax_field')]);
                    }

                    if (ee()->cartthrob->store->config('orders_shipping_field') && isset($row['field_id_' . ee()->cartthrob->store->config('orders_shipping_field')])) {
                        $data['shipping'] += sanitize_number($row['field_id_' . ee()->cartthrob->store->config('orders_shipping_field')]);
                    }

                    if (ee()->cartthrob->store->config('orders_discount_field') && isset($row['field_id_' . ee()->cartthrob->store->config('orders_discount_field')])) {
                        $data['discount'] += sanitize_number($row['field_id_' . ee()->cartthrob->store->config('orders_discount_field')]);
                    }
                }
            }
        }

        foreach ($data as $key => $value) {
            if ($key === 'count') {
                continue;
            }

            $data[$key] = ee()->number->format($value);
        }

        if (!ee()->TMPL->tagdata) {
            return $data['total'];
        }

        return ee()->template_helper->parse_variables_row($data);
    }

    public function package()
    {
        if (ee()->TMPL->fetch_param('row_id', '') !== '') {
            $item = ee()->cartthrob->cart->item(ee()->TMPL->fetch_param('row_id'));
        }

        $data = [];

        if (empty($item)) {
            if (ee()->TMPL->fetch_param('entry_id', '') !== '') {
                $product = ee()->product_model->get_product(ee()->TMPL->fetch_param('entry_id'));

                ee()->load->library('api');

                ee()->legacy_api->instantiate('channel_fields');

                if ($product && ee()->api_channel_fields->setup_handler('cartthrob_package')) {
                    if (ee()->TMPL->fetch_param('variable_prefix')) {
                        ee()->api_channel_fields->field_types['cartthrob_package']->variable_prefix = ee()->TMPL->fetch_param('variable_prefix');
                    }

                    $field_id = ee()->cartthrob_field_model->channel_has_fieldtype($product['channel_id'],
                        'cartthrob_package', true);

                    if ($field_id && isset($product['field_id_' . $field_id])) {
                        $data = ee()->api_channel_fields->apply('pre_process',
                            [$product['field_id_' . $field_id]]);

                        return ee()->api_channel_fields->apply('replace_tag',
                            [$data, ee()->TMPL->tagparams, ee()->TMPL->tagdata]);
                    }
                }
            }
        } else {
            if ($item->sub_items()) {
                $data = ee()->cartthrob_variables->sub_item_vars($item);
            }
        }

        if (count($data) === 0) {
            return ee()->TMPL->no_results();
        }
        ee()->load->add_package_path(PATH_THIRD . 'cartthrob');

        return ee()->template_helper->parse_variables($data);
    }

    public function save_customer_info()
    {
        ee()->load->library('form_builder');

        $_POST = array_merge($_POST, ee()->TMPL->tagparams);

        $customer_fields = array_keys(ee()->cartthrob->cart->customer_info());

        $required = ee()->TMPL->fetch_param('required');

        $save_shipping = bool_string(ee()->TMPL->fetch_param('save_shipping'), true);

        if ($required == 'all') {
            $required = $customer_fields;

            if ($save_shipping) {
                $required[] = 'shipping_option';
            }
        } elseif (preg_match('/^not\s/', $required)) {
            $not_required = explode('|', substr($required, 4));

            $required = $customer_fields;

            if ($save_shipping) {
                $required[] = 'shipping_option';
            }

            foreach ($required as $key => $value) {
                if (in_array($value, $not_required)) {
                    unset($required[$key]);
                }
            }
        } elseif ($required) {
            $required = explode('|', $required);
        }

        if (!$required) {
            $required = [];
        }

        if (ee()->form_builder
            ->set_require_rules(false)
            ->set_require_errors(false)
            ->set_require_form_hash(false)
            ->set_required($required)->validate($required)) {
            ee()->cartthrob->save_customer_info();
        }

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function save_customer_info_form()
    {
        if (ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('form_builder');

        $variables = ee()->cartthrob_variables->global_variables(true);

        ee()->form_builder->initialize([
            'form_data' => [
                'return',
                'secure_return',
                'derive_country_code',
                'error_handling',
            ],
            'encoded_form_data' => [],
            'classname' => 'Cartthrob',
            'method' => 'save_customer_info_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($variables),
        ]);

        return ee()->form_builder->form();
    }

    // @TODO test

    /**
     * Saves chosen shipping option to SESSION
     *
     * @return string
     */
    public function save_shipping_option()
    {
        $shipping_option = set(ee()->TMPL->fetch_param('shipping_option'), ee()->input->post('shipping_option', true));

        ee()->cartthrob->cart->set_shipping_info('shipping_option', $shipping_option);

        ee()->cartthrob->cart->save();

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function gateway_select()
    {
        ee()->load->helper('form');

        $attrs = [];

        if (ee()->TMPL->fetch_param('encrypt') && bool_string(ee()->TMPL->fetch_param('encrypt')) == false) {
            $encrypt = false;
        } else {
            $encrypt = true;
        }

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }

        $selectable_gateways = ee()->cartthrob->store->config('available_gateways');

        $name = (ee()->TMPL->fetch_param('name') ? ee()->TMPL->fetch_param('name') : 'gateway');
        $selected = (ee()->TMPL->fetch_param('selected') ? ee()->TMPL->fetch_param('selected') : ee()->cartthrob->store->config('payment_gateway'));

        // get the gateways that the user wants to output
        if (ee()->TMPL->fetch_param('gateways')) {
            foreach (explode('|', ee()->TMPL->fetch_param('gateways')) as $my_gateways) {
                $final_g['Cartthrob_' . $my_gateways] = '1';
            }
            // Making it so that it's possible to add the default gateway in this parameter without it having been selected as a choosable gateway.
            // if its the default then it's choosable in my book.
            if (isset($final_g[ee()->cartthrob->store->config('payment_gateway')]) && !isset($selectable_gateways[ee()->cartthrob->store->config('payment_gateway')])) {
                $selectable_gateways[ee()->cartthrob->store->config('payment_gateway')] = 1;
            }
            $selectable_gateways = array_intersect_key($final_g, $selectable_gateways);
        }
        // if the users selected gateways is not an option, then we'll use the default
        if (!isset($selectable_gateways[$selected]) && is_array($selectable_gateways)) {
            if (isset($selectable_gateways['Cartthrob_' . $selected])) {
                $selected = 'Cartthrob_' . $selected;
            } elseif (isset($selectable_gateways['Cartthrob_' . ee('Encrypt')->decode($selected)])) {
                $selected = 'Cartthrob_' . ee('Encrypt')->decode($selected);
            } // make sure this isn't an encoded value.
            elseif (!isset($selectable_gateways[ee('Encrypt')->decode($selected)])) {
                $selected = ee()->cartthrob->store->config('payment_gateway');
                $selectable_gateways = array_merge([ee()->cartthrob->store->config('payment_gateway') => '1'],
                    (array)$selectable_gateways);
            } else {
                $selected = ee('Encrypt')->decode($selected);
            }
        }

        ee()->load->library('api');
        ee()->load->library('api/api_cartthrob_payment_gateways');

        if ($this->cart_has_subscription()) {
            $subscription_gateways = [];

            foreach (ee()->api_cartthrob_payment_gateways->subscription_gateways() as $plugin_data) {
                $subscription_gateways[] = $plugin_data['classname'];
            }

            $selectable_gateways = array_intersect_key($selectable_gateways, array_flip($subscription_gateways));
        }

        // if none have been selected, OR if you're not allowed to select, then the default is shown
        if (!ee()->cartthrob->store->config('allow_gateway_selection') || count($selectable_gateways) == 0) {
            $selectable_gateways = [ee()->cartthrob->store->config('payment_gateway') => '1'];
            $selected = ee()->cartthrob->store->config('payment_gateway');
        }

        $gateways = ee()->api_cartthrob_payment_gateways->gateways();

        $data = [];
        foreach ($gateways as $plugin_data) {
            if (isset($selectable_gateways[$plugin_data['classname']])) {
                ee()->lang->loadfile(strtolower($plugin_data['classname']), 'cartthrob', false);

                if (isset($plugin_data['title'])) {
                    $title = ee()->lang->line($plugin_data['title']);
                } else {
                    $title = $plugin_data['classname'];
                }
                if ($encrypt) {
                    // have to create a variable here, because it'll be used in a spot
                    // where it needs to match. each time we encode, the values change.
                    $encoded = ee('Encrypt')->encode($plugin_data['classname']);
                    $data[$encoded] = $title;

                    if ($plugin_data['classname'] == $selected) {
                        $selected = $encoded;
                    }
                } else {
                    $data[$plugin_data['classname']] = $title;
                }
            }
        }

        asort($data);

        if (bool_string(ee()->TMPL->fetch_param('add_blank'))) {
            $data = array_merge(['' => '---'], $data);
        }

        return form_dropdown(
            $name,
            $data,
            $selected,
            $extra
        );
    }

    public function cart_has_subscription()
    {
        foreach (ee()->cartthrob->cart->items() as $item) {
            if ($item->meta('subscription')) {
                return '1';
            }
        }

        return 0;
    }

    /**
     * gateway_fields_url
     *
     * outputs an action URL so that you can post requests for gateway fields to a URL instead of a template
     * this will use the change_gateway_fields action to get selected gateway fiedls with an CSRF_TOKEN hash
     *
     * @return string action url
     */
    public function gateway_fields_url()
    {
        return ee()->functions->fetch_site_index(0,
                0) . QUERY_MARKER . 'ACT=' . ee()->functions->insert_action_ids(ee()->functions->fetch_action_id('Cartthrob',
                'change_gateway_fields_action'));
    }

    /**
     * change_gateway_fields_action
     *
     * gets gateway fields of selected gateway
     */
    public function change_gateway_fields_action()
    {
        if (!AJAX_REQUEST) {
            exit;
        }

        $data = ee()->cartthrob_variables->global_variables(true);

        $html = ee()->template_helper->parse_template($this->selected_gateway_fields(), $data);

        ee()->session->set_flashdata([
            'success' => !(bool)ee()->cartthrob->errors(),
            'errors' => ee()->cartthrob->errors(),
            'gateway_fields' => $html,
            'csrf_token' => ee()->functions->add_form_security_hash('{csrf_token}'),
        ]);
    }

    /**
     * selected_gateway_fields
     *
     * returns data from the 'html' field of the currently selected gateway
     *
     * @param bool $gateway
     * @return string
     */
    public function selected_gateway_fields()
    {
        $selectable_gateways = ee()->cartthrob->store->config('available_gateways');

        if (ee()->input->post('gateway')) {
            $selected = ee()->input->post('gateway');
        } else {
            $selected = (ee()->TMPL->fetch_param('gateway') ? ee()->TMPL->fetch_param('gateway') : ee()->cartthrob->store->config('payment_gateway'));
        }

        if (!isset($selectable_gateways[$selected])) {
            if (isset($selectable_gateways['Cartthrob_' . $selected])) {
                $selected = 'Cartthrob_' . $selected;
            } elseif (isset($selectable_gateways['Cartthrob_' . ee('Encrypt')->decode($selected)])) {
                $selected = 'Cartthrob_' . ee('Encrypt')->decode($selected);
            } // make sure this isn't an encoded value.
            elseif (!isset($selectable_gateways[ee('Encrypt')->decode($selected)])) {
                $selected = ee()->cartthrob->store->config('payment_gateway');
                $selectable_gateways = array_merge([ee()->cartthrob->store->config('payment_gateway') => '1'],
                    $selectable_gateways);
            } else {
                $selected = ee('Encrypt')->decode($selected);
            }
        }

        // if none have been selected, OR if you're not allowed to select, then the default is shown
        if (!ee()->cartthrob->store->config('allow_gateway_selection') || count($selectable_gateways) == 0) {
            $selectable_gateways = [ee()->cartthrob->store->config('payment_gateway') => '1'];
            $selected = ee()->cartthrob->store->config('payment_gateway');
        }

        ee()->load->library('api');
        ee()->load->library('api/api_cartthrob_payment_gateways');

        ee()->api_cartthrob_payment_gateways->set_gateway($selected);

        if (ee()->api_cartthrob_payment_gateways->template()) {
            $return_data = '{embed="' . ee()->api_cartthrob_payment_gateways->template() . '"}';
        } else {
            $return_data = ee()->api_cartthrob_payment_gateways->gateway_fields();
        }

        ee()->api_cartthrob_payment_gateways->reset_gateway();

        return $return_data;
    }

    public function set_config()
    {
        ee()->load->helper('array');

        $data = array_merge(ee()->cartthrob->cart->customer_info(),
            array_key_prefix(ee()->cartthrob->cart->customer_info(), 'customer_'), ee()->cartthrob->cart->info(),
            ee()->TMPL->segment_vars, ee()->config->_global_vars);

        ee()->TMPL->tagdata = ee()->functions->prep_conditionals(ee()->TMPL->tagdata, $data);

        ee()->TMPL->tagdata = ee()->TMPL->advanced_conditionals(ee()->TMPL->tagdata);

        $hash = md5(ee()->TMPL->tagdata);

        if (ee()->cartthrob->cart->meta('set_config_hash') === $hash) {
            // maybe we shouldn't reset it? leaving it for now @TODO
            ee()->cartthrob->cart->set_meta('set_config_hash', false)->save();

            return '';
        }

        ee()->cartthrob->cart->set_meta('set_config_hash', $hash);

        $vars = ee('Variables/Parser')->extractVariables(ee()->TMPL->tagdata);

        foreach ($vars['var_single'] as $var_single) {
            $params = ee('Variables/Parser')->parseTagParameters($var_single);

            $method = (preg_match('/^set_(config_)?([^\s]+)\s*.*$/', $var_single,
                $match)) ? 'set_config_' . $match[2] : false;

            if ($method && method_exists(ee()->cartthrob, $method)) {
                ee()->cartthrob->$method($params);
            } else {
                if (isset($params['value'])) {
                    ee()->cartthrob->cart->set_config($match[2], $params['value']);
                }
            }

            if ($method) {
                ee()->TMPL->tagdata = ee()->TMPL->swap_var_single($var_single, '', ee()->TMPL->tagdata);
            }
        }

        ee()->cartthrob->cart->save();

        ee()->functions->redirect(ee()->functions->create_url(ee()->uri->uri_string()));

        return ee()->TMPL->tagdata;
    }

    /**
     * get_live_rates_form
     * Outputs a quote request form
     *
     * @param $TMPL ->shipping_plugin
     * @return string
     **/
    public function get_live_rates_form()
    {
        ee()->load->library('form_builder');

        $data = ee()->cartthrob_variables->global_variables(true);

        $data['shipping_fields'] = $this->selected_shipping_fields();

        ee()->form_builder->initialize([
            'classname' => 'Cartthrob',
            'method' => 'update_live_rates_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
            'form_data' => [
                'return',
                'secure_return',
                'derive_country_code',
                'shipping_plugin',
                'shipping_option',
                'activate_plugin',
            ],
            'encoded_form_data' => [],
        ]);

        return ee()->form_builder->form();
    }

    /**
     * selected_shipping_fields
     *
     * returns data from the 'html' field of the currently selected shipping plugin
     *
     * @param bool $plugin
     * @return string
     */
    public function selected_shipping_fields()
    {
        ee()->load->library('api');

        ee()->load->library('api/api_cartthrob_shipping_plugins');

        return ee()->api_cartthrob_shipping_plugins->set_plugin(ee()->TMPL->fetch_param('shipping_plugin'))->html();
    }

    /**
     * update_live_rates_action
     * Gets a quoted shipping value from the default shipping method, and applies that value as the shipping value
     *
     * @param ee()->TMPL->shipping_plugin
     * @param ee()->TMPL->validate (checks required fields)
     * @return string
     **/
    public function update_live_rates_action()
    {
        // save_shipping (if set in post...will automatically save the cheapest option)

        if (!ee()->input->get_post('ACT')) {
            return;
        }

        if (ee()->extensions->active_hook('cartthrob_update_live_rates_start') === true) {
            ee()->extensions->call('cartthrob_update_live_rates_start');
            if (ee()->extensions->end_script === true) {
                return;
            }
        }
        ee()->cartthrob->save_customer_info();
        ee()->cartthrob->cart->save();

        ee()->load->library('form_validation');
        ee()->load->library('form_builder');
        ee()->load->library('api/api_cartthrob_shipping_plugins');
        ee()->load->library('languages');

        if (ee()->cartthrob->cart->count() <= 0) {
            return ee()->form_builder
                ->add_error(ee()->lang->line('empty_cart'))
                ->action_complete();
        }

        if (ee()->cartthrob->cart->shippable_subtotal() <= 0) {
            ee()->form_builder
                ->set_errors(ee()->cartthrob->errors())
                ->set_success_callback([ee()->cartthrob, 'action_complete'])
                ->action_complete();
        }

        ee()->languages->set_language(ee()->input->post('language', true));

        $not_required = [];

        $required = [];

        if (ee()->input->post('REQ')) {
            $required_string = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('REQ')));

            if (preg_match('/^not (.*)/', $required_string, $matches)) {
                $not_required = explode('|', $matches[1]);
                $required_string = '';
            }

            if ($required_string) {
                $required = explode('|', $required_string);
            }

            unset($required_string);
        }

        if (ee()->input->post('shipping_plugin')) {
            $selected_plugin = ee('Security/XSS')->clean(ee()->input->post('shipping_plugin'));
            ee()->api_cartthrob_shipping_plugins->set_plugin($selected_plugin);
            if (bool_string(ee('Security/XSS')->clean(ee()->input->post('activate_plugin')), true)) {
                ee()->cartthrob->cart->set_config('shipping_plugin', $selected_plugin);
            }
        }

        $shipping_name = ee()->api_cartthrob_shipping_plugins->title();

        $required = array_unique(array_merge($required, ee()->api_cartthrob_shipping_plugins->required_fields()));
        foreach ($not_required as $key) {
            unset($required[array_search($key, $required)]);
        }
        if (!ee()->form_builder->set_required($required)->validate()) {
            return ee()->form_builder->action_complete();
        }

        $product_id = ee()->input->post('shipping_option') ? ee()->input->post('shipping_option') : 'ALL';

        $shippingInfo = [
            'error_message' => null,
            'option_value' => [],
            'option_name' => [],
            'price' => [],
        ];

        $shippingInfo = array_merge($shippingInfo, ee()->api_cartthrob_shipping_plugins->get_live_rates($product_id));

        ee()->load->library('cartthrob_shipping_plugins');

        // OUTPUTS ERROR IN STANDARD EE WAY
        if (!$shippingInfo || (empty($shippingInfo['error_message']) && empty($shippingInfo['option_value']))) {
            return ee()->form_builder
                ->add_error(ee()->lang->line('no_shipping_returned'))
                ->action_complete();
        }
        if (!empty($shippingInfo['error_message'])) {
            return ee()->form_builder
                ->add_error($shippingInfo['error_message'])
                ->action_complete();
        } else {
            // SAVE THE CHEAPEST OPTION AS SELECTED
            if (bool_string(ee()->input->post('save_shipping'), true)) {
                if (!in_array($this->selected_shipping_option(), $shippingInfo['option_value'])) {
                    $lowest_amount_key = array_pop(array_keys($shippingInfo['price'], min($shippingInfo['price'])));
                    if (!empty($shippingInfo['option_value'][$lowest_amount_key])) {
                        ee()->cartthrob->cart->set_shipping($shippingInfo['price'][$lowest_amount_key]);
                        ee()->cartthrob->cart->set_shipping_info('shipping_option',
                            $shippingInfo['option_value'][$lowest_amount_key]);
                        ee()->cartthrob->cart->save();
                    }
                }
            }
        }

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete();
    }

    /**
     * selected_shipping_option
     *
     * outputs the description of the shipping item selected in the backend
     *
     * @return string
     */
    public function selected_shipping_option()
    {
        ee()->load->library('api');

        ee()->load->library('api/api_cartthrob_shipping_plugins');

        return (ee()->cartthrob->cart->shipping_info('shipping_option')) ? ee()->cartthrob->cart->shipping_info('shipping_option') : ee()->api_cartthrob_shipping_plugins->default_shipping_option();
    }

    /**
     * states
     *
     * swaps abbrev, and state from list in templates
     * @param $TMPL country_code 3 character country code (Default USA)
     * @return string
     */
    public function states()
    {
        ee()->load->library('locales');

        $data = [];
        $country_code = (ee()->TMPL->fetch_param('country_code')) ? ee()->TMPL->fetch_param('country_code') : false;

        foreach (ee()->locales->states($country_code) as $abbrev => $state) {
            $data[] = compact('abbrev', 'state');
        }

        return ee()->template_helper->parse_variables($data);
    }

    /**
     * @return mixed
     */
    public function state_select()
    {
        ee()->load->library('locales');
        ee()->load->helper('form');

        $name = (ee()->TMPL->fetch_param('name')) ? ee()->TMPL->fetch_param('name') : 'state';
        $selected = (ee()->TMPL->fetch_param('selected')) ? ee()->TMPL->fetch_param('selected') : ee()->TMPL->fetch_param('default');
        $abbrev_label = bool_string(ee()->TMPL->fetch_param('abbrev_label'));
        $abbrev_value = bool_string(ee()->TMPL->fetch_param('abbrev_value'), true);

        $states = ee()->locales->states(ee()->TMPL->fetch_param('country_code'));

        if (bool_string(ee()->TMPL->fetch_param('add_blank'))) {
            $blank = ['' => '---'];
            $states = $blank + $states;
        }

        $states_converted = [];
        foreach ($states as $abbrev => $state) {
            $value = ($abbrev_value) ? $abbrev : $state;
            $states_converted[$value] = ($abbrev_label) ? $abbrev : $state;
        }

        $attrs = [];

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }
        ee()->load->helper('form');

        return form_dropdown(
            $name,
            $states_converted,
            ee()->TMPL->fetch_param('selected'),
            $extra
        );
    }

    public function subscription_select()
    {
        ee()->load->helper('form');
        ee()->load->model('subscription_model');

        $name = (ee()->TMPL->fetch_param('name')) ? ee()->TMPL->fetch_param('name') : 'subscription_interval_units';

        if ($name == 'subscription') {
            $name = 'SUB';
        }
        $selected = (ee()->TMPL->fetch_param('selected')) ? ee()->TMPL->fetch_param('selected') : ee()->TMPL->fetch_param('default');
        $temp_name = str_replace('subscription_', '', $name);

        if (!$selected && $item = ee()->cartthrob->cart->item(ee()->TMPL->fetch_param('row_id'))) {
            ee()->load->helper('array');
            $opt = element($temp_name, $item->meta('subscription_options'));
            if ($item->meta('subscription_options') && $opt !== false) {
                $selected = $opt;
            }
        }

        $options = (ee()->TMPL->fetch_param('options') ? ee()->TMPL->fetch_param('options') : 'days|weeks|months|years');

        // need to get the encoded key SUB
        $keys = ee()->subscription_model->option_keys();
        $encoded_key = array_search($temp_name, $keys);
        if ($encoded_key) {
            $name = $encoded_key;
        }
        $options = explode('|', $options);
        if ($name == 'PI') {
            if (in_array('days', $options)) {
                // either no options were passed in or someone screwed up. Clear the options if it has days in the option array
                $options = null;
            }
            $temp_options = [];
            $sub_params['order_by'][] = 'name';
            $sub_params['sort'][] = 'asc';
            $all_plans = ee()->subscription_model->get_plans($sub_params, 100);

            if (is_array($all_plans) && count($all_plans)) {
                if (!empty($options)) {
                    foreach ($all_plans as $p) {
                        if (in_array($p['id'], $options)) {
                            $temp_options[] = $p['id'] . ':' . $p['name'];
                        }
                    }
                } else {
                    foreach ($all_plans as $p) {
                        $temp_options[] = $p['id'] . ':' . $p['name'];
                    }
                }

                $options = $temp_options;
            }
        }

        $revised_options = [];
        foreach ($options as $option_value => $option_name) {
            // format: options="12:1 year|24:2 years|36:3 years"
            if (strpos($option_name, ':') !== false) {
                list($option_value, $option_name) = explode(':', $option_name);
            } else {
                $option_value = $option_name;
            }

            $encoded_value = ee('Encrypt')->encode(trim($option_value));

            if ($option_value == $selected) { // have to do this because an unencoded value won't = the encoded version of same
                $selected = $encoded_value;
            }
            $revised_options[$encoded_value] = $option_name;
        }
        if (bool_string(ee()->TMPL->fetch_param('add_blank'))) {
            $blank = ['' => '---'];
            $revised_options = array_merge($blank, $revised_options);
        }

        $attrs = [];

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }

        return form_dropdown(
            $name,
            $revised_options,
            $selected,
            $extra
        );
    }

    public function submitted_order_info()
    {
        $data = ee()->cartthrob->cart->order();

        ee()->load->model(['cartthrob_entries_model', 'order_model']);

        if (!$data) {
            return ee()->template_helper->parse_variables();
        }

        foreach ($data as $i => $row) {
            // what's happening here:
            // not all of the data from cart->order() is suitable to be passed to parse_variables
            // particularly arrays of data that don't contain arrays
            // remove them.
            if (is_array($row) && count($row) > 0 && !is_array(current($row))) {
                if ($i === 'custom_data') {
                    foreach ($row as $key => $value) {
                        $data['custom_data:' . $key] = $value;
                    }
                }

                unset($data[$i]);
            }
        }

        $data = array_merge($data, array_key_prefix($data, 'cart_'));

        if (!empty($data['order_id'])) {
            if ($order = ee()->order_model->get_order($data['order_id'])) {
                $status = ee()->order_model->get_order_status($data['order_id']);

                switch ($status) {
                    case 'authorized':
                    case 'completed':
                        $data['authorized'] = true;
                        break;
                    case 'declined':
                        $data['declined'] = true;
                        break;
                    case 'failed':
                    case 'refunded':
                    case 'expired':
                    case 'reversed':
                    case 'canceled':
                    case 'voided':
                        $data['failed'] = true;
                        break;
                    default:
                        $data['processing'] = true;
                }

                $data['transaction_id'] = Arr::get($data, 'auth.transaction_id', ee()->order_model->get_order_transaction_id($data['order_id']));
                $data['error_message'] = Arr::get($data, 'auth.error_message', ee()->order_model->get_order_error_message($data['order_id']));
                $data = array_merge(ee()->cartthrob_entries_model->entry_vars($order), $data);
            }
        }

        // this needs to remain just before variable parsing so that any scripts above are not affected by removing data keys
        foreach ($data as $i => $row) {
            // what's happening here:
            // not all of the data from cart->order() is suitable to be passed to parse_variables
            // particularly arrays of data that don't contain arrays
            // remove them.
            if (is_array($row) && count($row) > 0 && !is_array(current($row))) {
                if ($i === 'custom_data') {
                    foreach ($row as $key => $value) {
                        $data['custom_data:' . $key] = $value;
                    }
                }

                unset($data[$i]);
            }
        }

        return ee()->template_helper->parse_variables_row($data);
    }

    /**
     * Returns total number of ALL items (including indexes) in cart
     * If you have 4 of product A, and 5 of product B, this would return 9.
     * To get total individual items, use total unique items
     *
     * @return string
     */
    public function total_items_count()
    {
        return ee()->cartthrob->cart->count_all();
    }

    public function unique_items_count()
    {
        return ee()->cartthrob->cart->count();
    }

    // --------------------------------
    //  Total Items Count
    // --------------------------------

    /**
     * update_cart_form
     *
     * outputs a form for updating data in the cart
     *
     * @param ee()->TMPL->id
     * @param ee()->TMPL->name
     * @param ee()->TMPL->onsubmit
     * @param ee()->TMPL->show_errors
     * @param ee()->TMPL->json
     * @param ee()->TMPL->redirect
     * @param ee()->TMPL->return
     * @param ee()->TMPL->class
     * @return string
     */
    public function update_cart_form()
    {
        if (!ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        ee()->load->library('form_builder');

        $variables = ee()->cartthrob_variables->global_variables(true);

        foreach (ee()->TMPL->var_single as $key) {
            if (!isset($variables[$key]) && strpos($key, 'custom_data:') === 0) {
                $variables[$key] = '';
            }
        }

        ee()->load->model('subscription_model');

        ee()->form_builder->initialize([
            'form_data' => [
                'secure_return',
                'return',
            ],
            'encoded_form_data' => ee()->subscription_model->encoded_form_data(),
            'encoded_numbers' => ee()->subscription_model->encoded_numbers(),
            'encoded_bools' => ee()->subscription_model->encoded_bools(),
            'classname' => 'Cartthrob',
            'method' => 'update_cart_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($variables),
        ]);

        return ee()->form_builder->form();
    }

    /**
     * Updates an item's quantity and item_options
     *
     * @param string ee()->TMPL->fetch_param('entry_id')
     * @return string
     */
    public function update_item()
    {
        foreach (ee()->TMPL->tagparams as $key => $value) {
            if (preg_match('/^item_options?:(.*)$/', $key, $match)) {
                unset(ee()->TMPL->tagparams[$key]);

                ee()->TMPL->tagparams['item_options'][$match[1]] = $value;
            }
        }

        if ($item = ee()->cartthrob->cart->item(ee()->TMPL->fetch_param('row_id'))) {
            $item->update(ee()->TMPL->tagparams);

            ee()->cartthrob->cart->save();
        }

        ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('return'));
    }

    public function update_item_form()
    {
        if (ee()->session->userdata('member_id') && ee()->TMPL->fetch_param('logged_out_redirect')) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        $item = ee()->cartthrob->cart->item(ee()->TMPL->fetch_param('row_id'));

        $entry_id = ($item && $item->product_id()) ? $item->product_id() : ee()->TMPL->fetch_param('entry_id');

        ee()->load->library('form_builder');

        $data = array_merge(
            ee()->cartthrob_variables->global_variables(true),
            ee()->cartthrob_variables->item_option_vars($entry_id, ee()->TMPL->fetch_param('row_id'))
        );

        ee()->form_builder->initialize([
            'form_data' => [
                'secure_return',
                'entry_id',
                'row_id',
                'quantity',
                'title',
                'language',
                'return',
                'delete',
                'delete_all',
            ],
            'encoded_form_data' => array_merge(
                [
                    'shipping' => 'SHP',
                    'weight' => 'WGT',
                    'permissions' => 'PER',
                ],
                ee()->subscription_model->encoded_form_data()
            ),
            'encoded_numbers' => array_merge(
                [
                    'price' => 'PR',
                    'expiration_date' => 'EXP',
                ],
                ee()->subscription_model->encoded_numbers()
            ),
            'encoded_bools' => array_merge(
                [
                    'allow_user_price' => 'AUP',
                    'allow_user_weight' => 'AUW',
                    'allow_user_shipping' => 'AUS',
                    'on_the_fly' => 'OTF',
                    'license_number' => 'LIC',
                ],
                ee()->subscription_model->encoded_bools()
            ),
            'array_form_data' => [
                'item_option',
            ],
            'classname' => 'Cartthrob',
            'method' => 'update_item_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
        ]);

        if (bool_string(ee()->TMPL->fetch_param('no_tax'))) {
            ee()->form_builder->set_encoded_bools('no_tax', 'NTX')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('tax_exempt'))) {
            ee()->form_builder->set_encoded_bools('tax_exempt', 'NTX')->set_params(ee()->TMPL->tagparams);
        }

        if (bool_string(ee()->TMPL->fetch_param('no_shipping'))) {
            ee()->form_builder->set_encoded_bools('no_shipping', 'NSH')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('shipping_exempt'))) {
            ee()->form_builder->set_encoded_bools('shipping_exempt', 'NSH')->set_params(ee()->TMPL->tagparams);
        }

        return ee()->form_builder->form();
    }

    public function update_item_action()
    {
        if (!ee()->input->get_post('ACT')) {
            return;
        }

        $row_id = ee()->input->post('row_id') ? ee()->input->post('row_id') : 0;
        $post = ee()->security->ee('Security/XSS')->clean($_POST);

        $item = ee()->cartthrob->cart->item($row_id);
        if ($item) {
            if (element($row_id, element('delete', $post))) {
                ee()->cartthrob->cart->remove_item($row_id);
            } else {
                foreach ($post as $key => $value) {
                    if (in_array($key, $item->default_keys())) {
                        $data[$key] = $value;
                    }
                }

                if (!empty($data)) {
                    $item->update($data);
                }
            }
        }
        if (ee()->input->post('delete_all')) {
            ee()->cartthrob->cart->clear();
        }
        ee()->cartthrob->cart->check_inventory();

        ee()->load->library('form_builder');

        ee()->form_builder->set_errors(ee()->cartthrob->errors())
            ->set_success_callback([ee()->cartthrob, 'action_complete'])
            ->action_complete(true);
    }

    public function update_subscription_form()
    {
        ee()->load->library('form_builder');
        $id = ee()->TMPL->fetch_param('sub_id');
        $subs = null;

        if (!$id) {
            ee()->cartthrob->set_error(lang('you_must_supply_a_subscription_id'));
        } else {
            ee()->load->model('subscription_model');

            $subs = ee()->subscription_model->get_subscription($id);
        }

        if (!$subs) {
            ee()->cartthrob->set_error(lang('no_subscription_by_that_id'));
        }

        if (ee()->form_builder->errors()) {
            return ee()->form_builder->action_complete();
        }

        return $this->checkout_form();
    }

    public function checkout_form()
    {
        if (ee()->session->userdata('member_id') == 0) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('logged_out_redirect'));
        }

        if (ee()->cartthrob->cart->is_empty()) {
            ee()->template_helper->tag_redirect(ee()->TMPL->fetch_param('cart_empty_redirect'));
        }

        if (bool_string(ee()->TMPL->fetch_param('live_rates'))) {
            return $this->require_shipping_update();
        }

        if (!ee()->TMPL->fetch_param('id')) {
            ee()->TMPL->tagparams['id'] = 'checkout_form';
        }

        ee()->load->library('api/api_cartthrob_payment_gateways');

        if (!ee()->cartthrob->store->config('allow_gateway_selection')) {
            unset(ee()->TMPL->tagparams['gateway']);
        } elseif (ee()->TMPL->fetch_param('gateway')) {
            ee()->api_cartthrob_payment_gateways->set_gateway(ee()->TMPL->fetch_param('gateway'));
        }

        if (strpos(ee()->TMPL->tagdata, '{gateway_fields}') !== false) {
            ee()->TMPL->tagdata = str_replace('{gateway_fields}', ee()->api_cartthrob_payment_gateways->gateway_fields(), ee()->TMPL->tagdata);
        }

        if (isset(ee()->TMPL->tagparams['required']) && strncmp(ee()->TMPL->tagparams['required'], 'not ', 4) === 0) {
            ee()->TMPL->tagparams['not_required'] = substr(ee()->TMPL->tagparams['required'], 4);

            unset(ee()->TMPL->tagparams['required']);
        }

        ee()->load->library('form_builder');
        ee()->load->model('subscription_model');

        ee()->cartthrob_variables->add_encoded_option_vars($data);

        ee()->form_builder->initialize([
            'captcha' => (bool)(!ee()->session->userdata('member_id') && ee()->cartthrob->store->config('checkout_form_captcha')),
            'form_data' => [
                'action',
                'secure_return',
                'return',
                'language',
                'authorized_redirect',
                'failed_redirect',
                'declined_redirect',
                'processing_redirect',
                'create_user',
                'member_id',
                'order_id',
            ],
            'encoded_form_data' => array_merge(
                [
                    'required' => 'REQ',
                    'file' => 'FI',
                    'not_required' => 'NRQ',
                    'gateway' => 'gateway',
                    'permissions' => 'PER',
                ],
                ee()->subscription_model->encoded_form_data()
            ),
            'encoded_numbers' => array_merge(
                [
                    'price' => 'PR',
                    'shipping' => 'SHP',
                    'tax' => 'TX',
                    'group_id' => 'GI',
                    'expiration_date' => 'EXP',
                    'vault_id' => 'vault_id',
                ],
                ee()->subscription_model->encoded_numbers()
            ),
            'encoded_bools' => array_merge(
                [
                    'allow_user_price' => 'AUP',
                    'allow_user_shipping' => 'AUS',
                    'on_the_fly' => 'OTF',
                    'license_number' => 'LIC',
                    'force_vault' => 'VLT',
                    'force_processing' => 'FPR',
                ],
                ee()->subscription_model->encoded_bools()
            ),
            'classname' => 'Cartthrob',
            'method' => 'checkout_action',
            'params' => ee()->TMPL->tagparams,
            'action' => ee()->cartthrob->store->config('payment_system_url'),
        ]);

        // setting the subscription id. if a subscription id is set the contents of the cart are removed, and only the subscription itself is updated.
        if (ee()->TMPL->fetch_param('sub_id')) {
            ee()->form_builder->set_hidden('sub_id', ee()->TMPL->fetch_param('sub_id'));
        }

        if (ee()->TMPL->fetch_param('gateway_method')) {
            ee()->form_builder->set_hidden('gateway_method', ee()->TMPL->fetch_param('gateway_method'));
        }

        if (bool_string(ee()->TMPL->fetch_param('no_tax'))) {
            ee()->form_builder->set_encoded_bools('no_tax', 'NTX')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('tax_exempt'))) {
            ee()->form_builder->set_encoded_bools('tax_exempt', 'NTX')->set_params(ee()->TMPL->tagparams);
        }

        if (bool_string(ee()->TMPL->fetch_param('no_shipping'))) {
            ee()->form_builder->set_encoded_bools('no_shipping', 'NSH')->set_params(ee()->TMPL->tagparams);
        } elseif (bool_string(ee()->TMPL->fetch_param('shipping_exempt'))) {
            ee()->form_builder->set_encoded_bools('shipping_exempt', 'NSH')->set_params(ee()->TMPL->tagparams);
        }

        // do this after initialize so captch vars are set
        $variables = ee()->cartthrob_variables->global_variables(true);

        ee()->form_builder->set_content(ee()->template_helper->parse_variables_row($variables));

        if (ee()->TMPL->fetch_param('order_id') || ee()->TMPL->fetch_param('member_id')) {
            ee()->form_builder->set_hidden('save_member_data', 0);
        }

        return ee()->form_builder->form() . ee()->api_cartthrob_payment_gateways->gateway('form_extra');
    }

    // aliases the checkout form. sub_id MUST be supplied, and must be valid.

    public function require_shipping_update()
    {
        // @TODO language
        ee()->load->library('api/api_cartthrob_shipping_plugins');
        ee()->load->library('template_helper');
        ee()->cartthrob->cart->shipping();

        if (ee()->TMPL->fetch_param('shipping_plugin')) {
            ee()->api_cartthrob_shipping_plugins->set_plugin(ee()->TMPL->fetch_param('shipping_plugin'));
        }
        if (!ee()->api_cartthrob_shipping_plugins->shipping_options()) {
            $error = ee()->cartthrob->cart->custom_data('shipping_error');
        } else {
            $error = null;
        }

        if (ee()->cartthrob->cart->custom_data('shipping_requires_update') != null || $error != null) {
            if ($error) {
                $content = "<span class='error_message'>Shipping Error: " . ee()->cartthrob->cart->custom_data('shipping_error') . '</span>';
            } else {
                $content = '';
            }
            // @TODO... this would be great if it was configurable
            $content .= '
				{exp:cartthrob:customer_info}
					{exp:cartthrob:update_cart_form return="" id="shipping_update_required"}
						<div>
							<h2>' . ee()->lang->line('shipping_update_required') . '</h2>

							<fieldset class="shipping" id="shipping">
								<legend>Shipping</legend>

								<label for="shipping_address">Shipping Address
								<input type="text" value="{customer_shipping_address}" name="shipping_address" id="shipping_address" />
								</label>

								<label for="shipping_address2">Shipping Address (apartment/suite number)
								<input type="text" value="{customer_shipping_address2}" name="shipping_address2" id="shipping_address2" />
								</label>

								<label for="shipping_city">Shipping City
								<input type="text" value="{customer_shipping_city}" name="shipping_city" id="shipping_city" />
								</label>

								<label for="shipping_state">Shipping State
								{exp:cartthrob:state_select  id="shipping_state" name="shipping_state" selected="{customer_shipping_state}" add_blank="yes" }
								</label>

								<label for="shipping_zip">Shipping Zip/Postal Code
								<input type="text" value="{customer_shipping_zip}" name="shipping_zip" id="shipping_zip" />
								</label>

								<label for="shipping_country">Shipping Country
								    {exp:cartthrob:country_select name="shipping_country_code" id="shipping_country" selected="{customer_shipping_country_code}"}
								</label>

								<label for="shipping_country">Shipping Option
									<select name="shipping_option">
									    {exp:cartthrob:get_shipping_options shipping_plugin="' . ee()->TMPL->fetch_param('shipping_plugin') . '"}
									        <option value="{rate_short_name}" {selected}>{rate_title} - {price}</option>
									    {/exp:cartthrob:get_shipping_options}
									</select>
								</label>

							</fieldset>

							<input type="submit" value="Submit" name="submit"/>
						</div>
					{/exp:cartthrob:update_cart_form}
				{/exp:cartthrob:customer_info}
				';

            return ee()->TMPL->tagdata = $content;
        }

        return null;
    }

    public function update_subscription_details()
    {
        $id = ee()->TMPL->fetch_param('sub_id');

        ee()->load->model('subscription_model');

        // this should NOT be a db search parameter of: sub_id, which is assigned by the payment provider.
        $params = ['id' => $id];

        if (ee()->session->userdata('group_id') != 1) {
            $params['member_id'] = ee()->session->userdata('member_id');
        }

        // @TODO for now only the member can update his own sub
        $subs = ee()->subscription_model->get_subscriptions($params);

        if (!$subs) {
            return ee()->TMPL->no_results();
        }

        $sub = array_shift($subs);

        ee()->load->library('form_builder');

        $data = array_merge(
            $sub,
            ee()->cartthrob_variables->global_variables(true)
        );

        ee()->cartthrob_variables->add_encoded_option_vars($data);

        $encoded_form_data = [];
        $form_data = ['sub_id'];

        foreach (ee()->subscription_model->columns as $key => $default) {
            if (in_array($key, ['name', 'description'])) {
                $form_data[] = 'subscription_' . $key;
            } else {
                if (ee()->TMPL->fetch_param('subscription_' . $key)) {
                    $encoded_form_data['subscription_' . $key] = 'subscription_' . $key;
                }
            }
        }

        ee()->form_builder->initialize([
            'form_data' => $form_data,
            'encoded_form_data' => $encoded_form_data,
            'classname' => 'Cartthrob',
            'method' => 'update_subscription_action',
            'params' => ee()->TMPL->tagparams,
            'content' => ee()->template_helper->parse_variables_row($data),
        ]);

        ee()->form_builder->set_hidden('sub_id', ee()->TMPL->fetch_param('sub_id', ee()->TMPL->fetch_param('sub_id')));

        return ee()->form_builder->form();
    }

    public function update_subscription_action()
    {
        ee()->load->library(['form_builder', 'encrypt']);

        ee()->load->model('subscription_model');

        $data = ['id' => ee()->input->post('sub_id')];

        ee()->cartthrob->save_customer_info();

        if (!$data['id']) {
            ee()->form_builder
                ->add_error(lang('no_subscription_id'))
                ->action_complete();
        }

        foreach (ee()->subscription_model->columns as $key => $default) {
            $value = null;

            $encoded_bools = ee()->subscription_model->encoded_bools();
            $encoded_form_data = ee()->subscription_model->encoded_form_data();
            $encoded_numbers = ee()->subscription_model->encoded_numbers();
            if (($value = ee()->input->post('subscription_' . $key)) !== false) {
                $data[$key] = in_array($key, ['name', 'description']) ? $value : ee('Encrypt')->decode($value);
            } elseif (isset($encoded_form_data['subscription_' . $key]) && ($value = ee()->input->post($encoded_form_data['subscription_' . $key])) !== false) {
                $data[$key] = ee('Encrypt')->decode($value);
            } elseif (isset($encoded_bools['subscription_' . $key]) && ($value = ee()->input->post($encoded_bools['subscription_' . $key])) !== false) {
                $data[$key] = ee('Encrypt')->decode($value);
            } elseif (isset($encoded_numbers['subscription_' . $key]) && ($value = ee()->input->post($encoded_numbers['subscription_' . $key])) !== false) {
                $data[$key] = ee('Encrypt')->decode($value);
            }
        }
        if (!ee()->subscription_model->validate($data)) {
            return ee()->form_builder
                ->add_error(ee()->subscription_model->errors)
                ->action_complete();
        }

        // check to see if the plan is changing
        $current_subscription = ee()->subscription_model->get_subscription($data['id']);
        if (isset($data['plan_id']) && ($current_subscription['plan_id'] != $data['plan_id'])) {
            $permissions_id = [];

            // update permissions when a plan changes
            ee()->load->model('permissions_model');
            // get the current permissions for this id
            $current_permissions_data = (array)ee()->permissions_model->get(['sub_id' => $data['id']], null, 0,
                true);
            // get the permissions for the new plan
            $new_plan = ee()->subscription_model->get_plan($data['plan_id']);
            $new_plan_permissions = unserialize(base64_decode($new_plan['permissions']));

            // let's go ahead and delete the permissions that we no longer want
            if (!empty($current_permissions_data)) {
                foreach ($current_permissions_data as $current_perm) {
                    if ($new_plan_permissions) {
                        if (($key = array_search($current_perm['permission'], $new_plan_permissions)) !== false) {
                            // it was in the array, so I'm removing it from the new permissions array
                            // when I'm done deleting unwanted permissions, I'll use what's left in the new permissions data to create new permissions
                            unset($new_plan_permissions[$key]);
                            // push the id to the permissions_id array
                            $permissions_id[] = $current_perm['id'];
                        } else {
                            // we don't want this permission anymore, delete it
                            ee()->permissions_model->delete($current_perm['id']);
                        }
                    } else {
                        // apparently we don't want any permissions anymore, deleting them all
                        ee()->permissions_model->delete($current_perm['id']);
                    }
                }
            }

            // var_dump($current_subscription);
            if (!empty($new_plan_permissions)) {
                foreach ($new_plan_permissions as $perm) {
                    $perm_id = ee()->permissions_model->update([
                        'member_id' => $current_subscription['member_id'],
                        'permission' => $perm,
                        'item_id' => (isset($current_subscription['entry_id'])) ? $current_subscription['entry_id'] : 000,
                        'order_id' => $current_subscription['order_id'],
                        'sub_id' => $data['id'],
                    ]);

                    // the permission ids need to get updated in the subscription table
                    $permissions_id[] = $perm_id;
                }
            }

            // going to add the permission ids into the subscription
            // should have the serialized item from the current subscription, unserialize it, add the permissions_id, then reserialize it
            if (isset($current_subscription['serialized_item'])) {
                $unserialized_item = unserialize(base64_decode($current_subscription['serialized_item']));
                $unserialized_item['permissions_id'] = $permissions_id;
                $data['serialized_item'] = base64_encode(serialize($unserialized_item));
            }
        }

        ee()->subscription_model->update($data);

        // cartthrob_add_to_cart_start hook
        if (ee()->extensions->active_hook('cartthrob_update_subscription_details') === true) {
            // $edata = $EXT->universal_call_extension('cartthrob_update_subscription_details', $data);
            ee()->extensions->call('cartthrob_update_subscription_details', $data);
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        ee()->form_builder->set_success_callback([ee()->cartthrob->cart, 'save'])
            ->action_complete();
    }

    /**
     * @param $number bool
     * @param string $TMPL ->fetch_param('price')
     * @param string $TMPL ->fetch_param('currency_code')
     * @param string $TMPL ->fetch_param('new_currency_code')
     * @param string $TMPL ->fetch_param('decimals')
     * @param string $TMPL ->fetch_param('dec_point')
     * @param string $TMPL ->fetch_param('thousands_sep')
     * @param string $TMPL ->fetch_param('prefix')
     * @param string $TMPL ->fetch_param('new_prefix')
     * @return string
     **/
    public function view_converted_currency()
    {
        ee()->load->library('number');
        ee()->load->library('curl');

        // Check to see if this value is being passed in or not.
        $number = ee()->TMPL->fetch_param('price');

        if ($number === false) {
            return '';
        }

        // clean the number
        $number = sanitize_number($number);

        // -------------------------------------------
        // 'cartthrob_view_converted_currency' hook.
        //
        if (ee()->extensions->active_hook('cartthrob_view_converted_currency') === true) {
            return ee()->extensions->call('cartthrob_view_converted_currency', $number);
        }

        // set defaults
        $currency = (ee()->TMPL->fetch_param('currency_code') !== false) ? ee()->TMPL->fetch_param('currency_code') : ee()->cartthrob->store->config('number_format_default_currency_code');
        $new_currency = (ee()->TMPL->fetch_param('new_currency_code') !== false) ? ee()->TMPL->fetch_param('new_currency_code') : ee()->cartthrob->store->config('number_format_default_currency_code');

        $currency = strtolower($currency);
        $new_currency = strtolower($new_currency);

        $new_prefix = bool_string(ee()->TMPL->fetch_param('use_prefix'));

        $prefix = '';

        if ($new_prefix) {
            switch ($new_currency) {
                case 'eur':
                    $prefix = '&#8364;';
                    break;
                case 'usd':
                    $prefix = '$';
                    break;
                case 'gbp':
                    $prefix = '&#163;';
                    break;
                case 'aud':
                    $prefix = '$';
                    break;
                case 'brl':
                    $prefix = 'R$';
                    break;
                case 'nzd':
                    $prefix = '$';
                    break;
                case 'cad':
                    $prefix = '$';
                    break;
                case 'chf':
                    $prefix = 'CHF';
                    break;
                case 'cny':
                    $prefix = '&#165;';
                    break;
                case 'dkk':
                    $prefix = 'kr';
                    break;
                case 'hkd':
                    $prefix = '$';
                    break;
                case 'inr':
                    $prefix = '&#8360;';
                    break;
                case 'jpy':
                    $prefix = '&#165;';
                    break;
                case 'krw':
                    $prefix = '&#8361;';
                    break;
                case 'mxn':
                    $prefix = '$';
                    break;
                case 'myr':
                    $prefix = 'RM';
                    break;
                case 'nok':
                    $prefix = 'kr';
                    break;
                case 'sek':
                    $prefix = 'kr';
                    break;
                case 'sgd':
                    $prefix = '$';
                    break;
                case 'thb':
                    $prefix = '&#3647;';
                    break;
                case 'zar':
                    $prefix = 'R';
                    break;
                case 'bgn':
                    $prefix = '&#1083;&#1074;';
                    break;
                case 'czk':
                    $prefix = '&#75;&#269;';
                    break;
                case 'eek':
                    $prefix = 'kr';
                    break;
                case 'huf':
                    $prefix = 'Ft';
                    break;
                case 'ltl':
                    $prefix = 'Lt';
                    break;
                case 'lvl':
                    $prefix = '&#8364;';
                    break;
                case 'pln':
                    $prefix = 'z&#322;';
                    break;
                case 'ron':
                    $prefix = 'kr';
                    break;
                case 'hrk':
                    $prefix = 'kn';
                    break;
                case 'rub':
                    $prefix = '&#1088;&#1091;&#1073;';
                    break;
                case 'try':
                    $prefix = 'TL';
                    break;
                case 'php':
                    $prefix = 'Php';
                    break;
                case 'cop':
                    $prefix = '$';
                    break;
                case 'ars':
                    $prefix = '$';
                    break;
                default:
                    $prefix = '$';
            }
        }

        ee()->number->set_prefix($prefix);

        // ee()->load->library('services_json');
        ee()->load->library('curl');

        $api_key = (ee()->TMPL->fetch_param('api_key')) ? '?key=' . ee()->TMPL->fetch_param('api_key') : '';

        if ($json = ee()->curl->simple_get('http://xurrency.com/api/' . $currency . '/' . $new_currency . '/' . $number . $api_key)) {
            $obj = json_decode($json);

            if (is_object($obj)
                && isset($obj->{'result'})
                && isset($obj->{'status'})
                && $obj->{'status'} == 'ok'
                && isset($obj->{'result'}->{'value'})
            ) {
                return ee()->number->format($obj->{'result'}->{'value'});
            }
        }

        return ee()->number->format($number);
    }

    public function view_download_link()
    {
        $link = ee()->TMPL->fetch_param('template');

        if (!ee()->TMPL->fetch_param('file')) {
            return show_error(ee()->lang->line('download_url_not_specified'));
        } else {
            $link .= rawurlencode(base64_encode(ee('Encrypt')->encode(ee()->TMPL->fetch_param('file'))));
        }

        if ($member_id = ee()->TMPL->fetch_param('member_id')) {
            if (in_array($member_id, ['{logged_in_member_id}', '{member_id}', 'CURRENT_USER'])) {
                $member_id = ee()->session->userdata('member_id');
            }

            $link .= '/' . rawurlencode(base64_encode(ee('Encrypt')->encode($member_id)));
        }

        return $link;
    }

    public function get_download_link()
    {
        $file = null;
        $path = null;

        if (ee()->TMPL->fetch_param('field') && ee()->TMPL->fetch_param('entry_id')) {
            ee()->load->model(['cartthrob_field_model', 'cartthrob_entries_model', 'tools_model']);

            $entry = ee()->cartthrob_entries_model->entry(ee()->TMPL->fetch_param('entry_id'));

            ee()->load->helper('array');

            // @NOTE if the developer has assigned an entry id and a field, but there's nothing IN the field,  then the path doesn't get set, and no debug information is output, because path, below would be set to NULL
            if ($path = element(ee()->TMPL->fetch_param('field'), $entry)) {
                ee()->load->library('paths');

                $path = ee()->paths->parse_file_server_paths($path);

                ee()->TMPL->tagparams['file'] = $path;
                ee()->TMPL->tagparams['free_file'] = $path;
            }
        }

        if (bool_string(ee()->TMPL->fetch_param('debug')) && ee()->TMPL->fetch_param('file')) {
            ee()->load->library('cartthrob_file');

            return ee()->cartthrob_file->file_debug(ee()->TMPL->fetch_param('file'));
        }

        foreach (ee()->TMPL->tagparams as $key => $value) {
            if ($value !== '' || $value !== false) {
                switch ($key) {
                    case 'member_id':
                        if (in_array($value, ['{logged_in_member_id}', '{member_id}', 'CURRENT_USER'])) {
                            $value = ee()->session->userdata('member_id');
                        }
                        $member_id = rawurlencode(base64_encode(ee('Encrypt')->encode(sanitize_number($value))));
                        if (isset(ee()->TMPL->tagparams['free_file'])) {
                            unset(ee()->TMPL->tagparams['free_file']);
                        }
                        break;
                    case 'group_id':
                        if (in_array($value, ['{logged_in_group_id}', '{group_id}'])) {
                            $value = ee()->session->userdata('group_id');
                        }
                        $group_id = rawurlencode(base64_encode(ee('Encrypt')->encode(sanitize_number($value))));
                        if (isset(ee()->TMPL->tagparams['free_file'])) {
                            unset(ee()->TMPL->tagparams['free_file']);
                        }
                        break;
                    case 'language':
                        $language = $value;
                        break;
                    case 'free_file':
                        $file = '&FI=' . rawurlencode(base64_encode(ee('Encrypt')->encode('FI' . $value)));
                        break;
                    case 'file':
                        $file = '&FP=' . rawurlencode(base64_encode(ee('Encrypt')->encode('FP' . $value)));
                        break;
                }
            }
        }

        if (bool_string(ee()->TMPL->fetch_param('debug'))) {
            ee()->load->library('cartthrob_file');
            ee()->cartthrob_file->file_debug($file);
        }

        $downloadUrl = ee()->functions->fetch_site_index(0, 0) . QUERY_MARKER . 'ACT=' . ee()->functions->insert_action_ids(ee()->functions->fetch_action_id('Cartthrob', 'download_file_action')) . $file;

        if (isset($member_id)) {
            $downloadUrl .= '&MI=' . $member_id;
        }
        if (isset($group_id)) {
            $downloadUrl .= '&GI=' . $group_id;
        }
        if (isset($language)) {
            $downloadUrl .= '&L=' . $language;
        }

        return $downloadUrl;
    }

    /**
     * Formats a number
     *
     * @param int ee()->TMPL->fetch_param('number')
     * @param int ee()->TMPL->fetch_param('decimals')
     * @param string ee()->TMPL->fetch_param('dec_point')
     * @param string ee()->TMPL->fetch_param('thousands_sep')
     * @param string ee()->TMPL->fetch_param('prefix')
     * @return string
     **/
    public function view_formatted_number()
    {
        ee()->load->library('number');

        return ee()->number->format(ee()->TMPL->fetch_param('number'));
    }

    public function view_country_name()
    {
        ee()->load->library('locales');

        $countries = ee()->locales->all_countries();

        return (ee()->TMPL->fetch_param('country_code') && isset($countries[ee()->TMPL->fetch_param('country_code')])) ? $countries[ee()->TMPL->fetch_param('country_code')] : '';
    }

    public function view_decrypted_string()
    {
        if (!ee()->TMPL->fetch_param('string')) {
            return '';
        }

        return ee('Encrypt')->decode(base64_decode(rawurldecode(ee()->TMPL->fetch_param('string'))),
            ee()->TMPL->fetch_param('key'));
    }

    public function view_encrypted_string()
    {
        if (!ee()->TMPL->fetch_param('string')) {
            return '';
        }

        return rawurlencode(base64_encode(ee('Encrypt')->encode(ee()->TMPL->fetch_param('string'),
            ee()->TMPL->fetch_param('key'))));
    }

    /**
     * format_phone
     *
     * returns an array of phone parts
     * @param string $phone
     * @return string formatted string | array of number parts
     */
    public function view_formatted_phone_number()
    {
        if (!ee()->TMPL->fetch_param('number')) {
            return '';
        }

        $return = get_formatted_phone(ee()->TMPL->fetch_param('number'));

        $output = '';

        if ($return['international']) {
            $output .= $return['international'] . '-';
        }

        if ($return['area_code']) {
            $output .= $return['area_code'] . '-';
        }

        if ($return['prefix']) {
            $output .= $return['prefix'] . '-';
        }

        if ($return['suffix']) {
            $output .= $return['suffix'];
        }

        return $output;
    }

    /**
     * view_setting
     *
     * returns selected settings from the backend.
     *
     * @return string
     **/
    public function view_setting()
    {
        foreach (ee()->TMPL->tagparams as $key => $value) {
            switch ($key) {
                case !$key:
                case !bool_string($value):
                    break;
                case 'prefix':
                case 'number_prefix':
                    return ee()->cartthrob->store->config('number_format_defaults_prefix');
                case 'country':
                    return ee()->cartthrob->store->config('default_location', 'country_code');
                case 'country_code':
                case 'state':
                case 'region':
                case 'zip':
                    return ee()->cartthrob->store->config('default_location', $key);
                case 'member_id':
                    return ee()->cartthrob->store->config('default_member_id');
                case 'thousands_sep':
                case 'thousands_separator':
                    return ee()->cartthrob->store->config('number_format_defaults_thousands_sep');
                case 'prefix_position':
                    return ee()->cartthrob->store->config('number_format_defaults_prefix_position');
                case 'decimal':
                case 'decimal_point':
                    return ee()->cartthrob->store->config('number_format_defaults_dec_point');
                case 'decimal_precision':
                    return ee()->cartthrob->store->config('number_format_defaults_decimals');
                case 'currency_code':
                    return ee()->cartthrob->store->config('number_format_defaults_currency_code');
                case 'shipping_option':
                case 'selected_shipping_option':
                    return ee()->cartthrob->cart->shipping_info('shipping_option');
                default:
                    return ee()->cartthrob->store->config($key);
            }
        }

        return '';
    }

    public function vaults()
    {
        ee()->load->model('vault_model');

        $variables = [];

        $params = [];

        if (ee()->TMPL->fetch_param('id')) {
            $params['id'] = (strstr(ee()->TMPL->fetch_param('id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('id')) : ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('order_id')) {
            $params['order_id'] = (strstr(ee()->TMPL->fetch_param('order_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('order_id')) : ee()->TMPL->fetch_param('order_id');
        }

        if (ee()->TMPL->fetch_param('member_id')) {
            if (in_array(ee()->TMPL->fetch_param('member_id'),
                ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
                $params['member_id'] = ee()->session->userdata('member_id');
            } else {
                $params['member_id'] = (strstr(ee()->TMPL->fetch_param('member_id'), '|') !== false) ? explode('|',
                    ee()->TMPL->fetch_param('member_id')) : ee()->TMPL->fetch_param('member_id');
            }
        }

        // default to current member's vaults if no other params are specified
        if (!$params) {
            $params = ['member_id' => ee()->session->userdata('member_id')];
        }

        // @TODO add pagination

        $params['limit'] = (ee()->TMPL->fetch_param('limit')) ? ee()->TMPL->fetch_param('limit') : 100;

        $variables = ee()->vault_model->get_vaults($params);

        if (!$variables) {
            return ee()->TMPL->no_results();
        }

        return ee()->template_helper->parse_variables($variables);
    }

    public function subscriptions()
    {
        ee()->load->model('subscription_model');
        ee()->load->library('number');

        $variables = [];

        $params = [];

        if (ee()->TMPL->fetch_param('id')) {
            $params['id'] = (strstr(ee()->TMPL->fetch_param('id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('id')) : ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('order_id')) {
            $params['order_id'] = (strstr(ee()->TMPL->fetch_param('order_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('order_id')) : ee()->TMPL->fetch_param('order_id');
        }

        if (ee()->TMPL->fetch_param('member_id')) {
            if (in_array(ee()->TMPL->fetch_param('member_id'),
                ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
                $params['member_id'] = ee()->session->userdata('member_id');
            } else {
                $params['member_id'] = (strstr(ee()->TMPL->fetch_param('member_id'), '|') !== false) ? explode('|',
                    ee()->TMPL->fetch_param('member_id')) : ee()->TMPL->fetch_param('member_id');
            }
        }

        // default to current member's vaults if no other params are specified
        if (!$params && ee()->session->userdata('group_id') != 1) {
            $params = ['member_id' => ee()->session->userdata('member_id')];
        }

        // @TODO add pagination

        $params['limit'] = (ee()->TMPL->fetch_param('limit')) ? ee()->TMPL->fetch_param('limit') : 100;

        if (!ee()->TMPL->fetch_param('status')) {
            ee()->db->where('status !=', 'closed');
        } else {
            $params['status'] = ee()->TMPL->fetch_param('status');
        }

        if (is_array(ee()->TMPL->tagparams)) {
            $params = array_merge(ee()->TMPL->tagparams, $params);
        }
        $data = ee()->subscription_model->get_subscriptions($params);

        if (empty($params['member_id']) && ee()->session->userdata('group_id') != 1) {
            return ee()->TMPL->no_results();
        }

        ee()->load->model('order_model');
        foreach ($data as $k => &$row) {
            $item = _unserialize($row['serialized_item'], true);

            if (!empty($row['plan_id'])) {
                $plan = ee()->subscription_model->get_plan($row['plan_id']);
                if (!$plan) {
                    // plan doesn't exist anymore
                    unset($data[$k]);
                    continue;
                } else {
                    if (isset($plan['id'])) {
                        unset($plan['id']);
                    }
                    if (array_key_exists('used_total_occurrences', $plan)) {
                        unset($plan['used_total_occurrences']);
                    }
                    if (array_key_exists('used_trial_occurrences', $plan)) {
                        unset($plan['used_trial_occurrences']);
                    }
                    if (isset($plan['status'])) {
                        unset($plan['status']);
                    }

                    if (empty($row['name'])) {
                        if (!empty($plan['name'])) {
                            $row['name'] = $plan['name'];
                        }
                    }

                    $row = array_merge($row, $plan);
                }
            }
            $row['entry_id'] = isset($item['product_id']) ? $item['product_id'] : '';

            if (isset($row['permissions'])) {
                $perms = @unserialize(base64_decode($row['permissions']));
                if (is_array($perms)) {
                    $row['permissions'] = implode('|', $perms);
                } else {
                    $row['permissions'] = $perms;
                }
            }

            if ($row['start_date'] === '0') {
                $row['start_date'] = null;
            }
            if ($row['end_date'] === '0') {
                $row['end_date'] = null;
            }
            if ($row['last_bill_date'] === '0') {
                $row['last_bill_date'] = null;
            }

            $last_billing = ($row['last_bill_date']) ? $row['last_bill_date'] : $row['start_date'];
            $last_billing = ($last_billing) ? $last_billing : strtotime('now');

            $date_string = @date('Y-m-d',
                    $last_billing) . '+  ' . $row['interval_length'] . '  ' . $row['interval_units'];

            $row['next_billing_date'] = strtotime($date_string);

            if (array_key_exists('price', $row)) {
                $row['price_numeric'] = $row['price'];
                $row['price'] = ee()->number->format($row['price']);
                // $row['last_bill_date'] = NULL;
            }

            unset($row['serialized_item']);
            // @TODO need to delete rows if there's no order to back it up'
        }

        ee()->load->library('data_filter');

        $order_by = (ee()->TMPL->fetch_param('order_by')) ? ee()->TMPL->fetch_param('order_by') : ee()->TMPL->fetch_param('orderby');

        ee()->data_filter->sort($data, $order_by, ee()->TMPL->fetch_param('sort'));
        ee()->data_filter->limit($data, ee()->TMPL->fetch_param('limit'), ee()->TMPL->fetch_param('offset'));

        ee()->template_helper->apply_search_filters($data);

        if (!$data) {
            return ee()->TMPL->no_results();
        }

        return ee()->template_helper->parse_variables($data);
    }

    public function has_subscription_permission()
    {
        if (!ee()->session->userdata('member_id')) {
            return ee()->TMPL->no_results();
        }

        $params = [];

        if (in_array(ee()->TMPL->fetch_param('member_id'),
            ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
            $params['member_id'] = ee()->session->userdata('member_id');
        } else {
            $params['member_id'] = (strstr(ee()->TMPL->fetch_param('member_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('member_id')) : ee()->TMPL->fetch_param('member_id');
        }

        if (empty($params['member_id'])) {
            $params['member_id'] = ee()->session->userdata('member_id');
        }
        ee()->load->model('subscription_model');
        ee()->db->where('status', 'open');
        ee()->db->or_where('end_date >', ee()->localize->now);

        $data = null;

        $data = ee()->subscription_model->get_subscriptions($params);

        if (!$data) {
            return ee()->TMPL->no_results();
        } else {
            foreach ($data as $key => $value) {
                $params['sub_id'][] = $value['id'];
            }

            ee()->load->model('permissions_model');

            if (ee()->TMPL->fetch_param('permissions')) {
                $permissions = explode('|', ee()->TMPL->fetch_param('permissions'));
                $params['permission'] = $permissions;
                $query = ee()->permissions_model->get($params, 1);
            } else {
                $query = ee()->permissions_model->get($params, 1);
            }

            if (!empty($query)) {
                // single tag
                if (!ee()->TMPL->tagdata) {
                    return 1;
                }

                return ee()->TMPL->tagdata;
            } else {
                return ee()->TMPL->no_results();
            }
        }

        return ee()->TMPL->no_results();
    }

    public function has_permission()
    {
        if (!ee()->session->userdata('member_id')) {
            return ee()->TMPL->no_results();
        }

        $params = [];

        if (in_array(ee()->TMPL->fetch_param('member_id'),
            ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
            $params['member_id'] = ee()->session->userdata('member_id');
        } else {
            $params['member_id'] = (strstr(ee()->TMPL->fetch_param('member_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('member_id')) : ee()->TMPL->fetch_param('member_id');
        }

        if (empty($params['member_id'])) {
            $params['member_id'] = ee()->session->userdata('member_id');
        }

        // checking to see if there's a sub id. if the sub is inactive, the permission is irrelevant.
        if (ee()->TMPL->fetch_param('sub_id')) {
            $params['sub_id'] = (strstr(ee()->TMPL->fetch_param('sub_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('sub_id')) : ee()->TMPL->fetch_param('sub_id');

            ee()->load->model('subscription_model');
            ee()->db->where('status', 'open');
            $data = null;
            if (!is_array($params['sub_id']) && strtolower($params['sub_id']) == 'any') {
                unset($params['sub_id']);
                $data = ee()->subscription_model->get_subscriptions($params);
                unset($params['member_id']);
            } else {
                $data = ee()->subscription_model->get_subscriptions($params);
            }
            if (!$data) {
                return ee()->TMPL->no_results();
            } else {
                foreach ($data as $key => $value) {
                    $params['sub_id'][] = $value['id'];
                }

                ee()->load->model('permissions_model');

                if (ee()->TMPL->fetch_param('permissions')) {
                    $permissions = explode('|', ee()->TMPL->fetch_param('permissions'));
                    $params['permission'] = $permissions;
                    $query = ee()->permissions_model->get($params, 1);
                } else {
                    $query = ee()->permissions_model->get($params, 1);
                }

                if (!empty($query)) {
                    // single tag
                    if (!ee()->TMPL->tagdata) {
                        return 1;
                    }

                    return ee()->TMPL->tagdata;
                } else {
                    return ee()->TMPL->no_results();
                }
            }
        }

        if (ee()->TMPL->fetch_param('permissions')) {
            $permissions = explode('|', ee()->TMPL->fetch_param('permissions'));

            foreach ($permissions as $key => $value) {
                $params['permission'] = $value;
                ee()->load->model('permissions_model');

                $query = ee()->permissions_model->get($params, 1);

                if (!empty($query)) {
                    // single tag
                    if (!ee()->TMPL->tagdata) {
                        return 1;
                    }

                    return ee()->TMPL->tagdata;
                }
            }
        }

        return ee()->TMPL->no_results();
    }

    public function permissions()
    {
        ee()->load->model('permissions_model');

        $variables = [];

        $params = [];

        if (ee()->TMPL->fetch_param('id')) {
            $params['id'] = (strstr(ee()->TMPL->fetch_param('id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('id')) : ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('item_id')) {
            $params['item_id'] = (strstr(ee()->TMPL->fetch_param('item_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('item_id')) : ee()->TMPL->fetch_param('item_id');
        }

        if (ee()->TMPL->fetch_param('order_id')) {
            $params['order_id'] = (strstr(ee()->TMPL->fetch_param('order_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('order_id')) : ee()->TMPL->fetch_param('order_id');
        }

        if (ee()->TMPL->fetch_param('member_id')) {
            if (in_array(ee()->TMPL->fetch_param('member_id'),
                ['CURRENT_USER', '{member_id}', '{logged_in_member_id}'])) {
                $params['member_id'] = ee()->session->userdata('member_id');
            } else {
                $params['member_id'] = (strstr(ee()->TMPL->fetch_param('member_id'), '|') !== false) ? explode('|',
                    ee()->TMPL->fetch_param('member_id')) : ee()->TMPL->fetch_param('member_id');
            }
        }

        if (ee()->TMPL->fetch_param('sub_id')) {
            $sub_param = (strstr(ee()->TMPL->fetch_param('sub_id'), '|') !== false) ? explode('|',
                ee()->TMPL->fetch_param('sub_id')) : ee()->TMPL->fetch_param('sub_id');

            if (!is_array($sub_param)) {
                $subs[] = $sub_param;
            } else {
                $subs = $sub_param;
            }

            $use = [];
            // look through the subs model to make sure this subscription is open.
            // if it's not open, then don't return anything
            ee()->load->model('subscription_model');
            foreach ($subs as $id) {
                $s = ee()->subscription_model->get_subscription($id);
                if (isset($s['status']) && $s['status'] == 'open') {
                    $use[] = $id;
                }
            }
            if (empty($use)) {
                return ee()->TMPL->no_results();
            }
            $params['sub_id'] = $use;
        }

        // default to current member's permissions if no other params are specified
        if (!$params) {
            $params = ['member_id' => ee()->session->userdata('member_id')];
        }

        $params['limit'] = (ee()->TMPL->fetch_param('limit')) ? ee()->TMPL->fetch_param('limit') : 100;

        $variables = ee()->permissions_model->get($params);

        if (empty($variables)) {
            return ee()->TMPL->no_results();
        }

        return ee()->template_helper->parse_variables($variables);
    }

    public function years()
    {
        $years = (is_numeric(ee()->TMPL->fetch_param('years'))) ? ee()->TMPL->fetch_param('years') : 5;

        $start_year = (is_numeric(ee()->TMPL->fetch_param('start_year'))) ? ee()->TMPL->fetch_param('start_year') : date('Y');

        $final_year = $start_year + $years;

        $data = [];

        for ($year = $start_year; $year < $final_year; $year++) {
            $data[] = ['year' => $year];
        }

        return ee()->template_helper->parse_variables($data);
    }

    public function month_select()
    {
        $selected = null;

        $attrs = [];
        ee()->load->helper('form');
        $data = [
            '01' => ee()->lang->line('january'),
            '02' => ee()->lang->line('february'),
            '03' => ee()->lang->line('march'),
            '04' => ee()->lang->line('april'),
            '05' => ee()->lang->line('may'),
            '06' => ee()->lang->line('june'),
            '07' => ee()->lang->line('july'),
            '08' => ee()->lang->line('august'),
            '09' => ee()->lang->line('september'),
            '10' => ee()->lang->line('october'),
            '11' => ee()->lang->line('november'),
            '12' => ee()->lang->line('december'),
        ];

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }

        $name = (ee()->TMPL->fetch_param('name') ? ee()->TMPL->fetch_param('name') : 'expiration_month');

        if (ee()->TMPL->fetch_param('selected')) {
            $selected = ee()->TMPL->fetch_param('selected');
        }

        if (!$selected || !array_key_exists($selected, $data)) {
            $selected = @date('m');
        }

        return form_dropdown(
            $name,
            $data,
            $selected,
            $extra
        );
    }

    public function years_select()
    {
        return $this->year_select();
    }

    public function year_select()
    {
        $selected = null;

        ee()->load->helper('form');

        $years = (is_numeric(ee()->TMPL->fetch_param('years'))) ? ee()->TMPL->fetch_param('years') : 5;

        $start_year = (is_numeric(ee()->TMPL->fetch_param('start_year'))) ? ee()->TMPL->fetch_param('start_year') : date('Y');

        $final_year = $start_year + $years;

        $data = [];

        for ($year = $start_year; $year < $final_year; $year++) {
            $data[$year] = $year;
        }

        $attrs = [];

        if (ee()->TMPL->fetch_param('id')) {
            $attrs['id'] = ee()->TMPL->fetch_param('id');
        }

        if (ee()->TMPL->fetch_param('class')) {
            $attrs['class'] = ee()->TMPL->fetch_param('class');
        }

        if (ee()->TMPL->fetch_param('onchange')) {
            $attrs['onchange'] = ee()->TMPL->fetch_param('onchange');
        }

        $extra = '';

        if ($attrs) {
            $extra .= _attributes_to_string($attrs);
        }

        if (ee()->TMPL->fetch_param('extra')) {
            if (substr(ee()->TMPL->fetch_param('extra'), 0, 1) != ' ') {
                $extra .= ' ';
            }

            $extra .= ee()->TMPL->fetch_param('extra');
        }

        $name = (ee()->TMPL->fetch_param('name') ? ee()->TMPL->fetch_param('name') : 'expiration_year');

        if (ee()->TMPL->fetch_param('selected')) {
            $selected = ee()->TMPL->fetch_param('selected');
        }

        if (!$selected) {
            $selected = @date('Y');
        }

        return form_dropdown(
            $name,
            $data,
            $selected,
            $extra
        );
    }

    public function consume_async_job()
    {
        ignore_user_abort(true);

        $limit = (int)(ee()->input->get('limit', true) ?: 1);

        ee()->load->model('async_job_model');

        foreach (ee()->async_job_model->fetch($limit) as $job) {
            try {
                $order_id = $job['payload']['order']['order_id'];

                ee()->load->model('order_model');
                ee()->order_model->update_order_items($order_id, $job['payload']['order']['items']);
                ee()->order_model->update_order($order_id, $job['payload']['order']);
                $order = ee()->order_model->get_order_from_entry($order_id);

                ee()->load->model('Cart_model');
                ee()->cartthrob->cart->set_order($order);
                ee()->cartthrob->cart->save();

                ee()->load->library('cartthrob_payments');
                ee()->cartthrob_payments->checkoutComplete($job['state'], null, null, true);

                ee()->async_job_model->delete($job['id']);
            } catch (\Exception $e) {
                ee()->async_job_model->update($job, $e->getMessage());
            }
        }
    }

    protected function processCustomerInfo(array $checkoutOptions)
    {
        // if you're logged in as an admin, and you're creating a user, we don't want to save the info now, or your admin info will be overwritten
        if (true === $checkoutOptions['create_user']) {
            if (!in_array(ee()->session->userdata('group_id'), ee()->config->item('cartthrob:admin_checkout_groups'))) {
                // Save the current customer info for use after checkout
                // needed for return trip after offsite processing
                ee()->cartthrob->save_customer_info();
            } elseif (ee()->cartthrob->cart->customer_info('email_address') == ee()->input->post('email_address')) {
                // admin checkout with create user turned on... but checking out with their own account
                ee()->cartthrob->save_customer_info();
            }
        } elseif (ee()->input->post('member_id', true) && in_array(ee()->session->userdata('group_id'), ee()->config->item('cartthrob:admin_checkout_groups'))) {
            // Save the current customer info for use after checkout
            // needed for return trip after offsite processing
            ee()->cartthrob->cart->set_meta('checkout_as_member', ee()->input->post('member_id', true));
        } elseif (!ee()->input->post('order_id')) {
            // we also don't want to save data if you're tring to update an order id at this point.
            ee()->cartthrob->save_customer_info();
        }
    }

    /**
     * @return array
     */
    protected function marshalCheckoutOptions()
    {
        $checkoutOptions = [];

        $checkoutOptions['create_user'] = bool_string(ee()->input->post('create_user'));

        if (ee()->input->post('order_id')) {
            $checkoutOptions['update_order_id'] = ee()->input->post('order_id');
        }

        if ((ee()->cartthrob->store->config('allow_gateway_selection') && ee()->input->post('gateway'))) {
            $checkoutOptions['gateway'] = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('gateway')));
        } else {
            $checkoutOptions['gateway'] = ee()->cartthrob->store->config('payment_gateway');
        }

        if (empty($checkoutOptions['gateway'])) {
            $checkoutOptions['gateway'] = ee()->cartthrob->store->config('payment_gateway');
        }

        $checkoutOptions['gateway_method'] = ee()->input->post('gateway_method');
        $checkoutOptions['credit_card_number'] = sanitize_credit_card_number(ee()->input->post('credit_card_number', true));

        if (ee()->input->post('EXP')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('EXP')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $checkoutOptions['expiration_date'] = $data;
            }
        }

        $checkoutOptions['tax'] = ee()->cartthrob->cart->tax();
        $checkoutOptions['shipping'] = ee()->cartthrob->cart->shipping();
        // discount MUST be calculated before shipping to set shipping free, etc.
        $checkoutOptions['discount'] = ee()->cartthrob->cart->discount();
        $checkoutOptions['shipping'] = ee()->cartthrob->cart->shipping();
        $checkoutOptions['shipping_plus_tax'] = ee()->cartthrob->cart->shipping_plus_tax();
        $checkoutOptions['subtotal'] = ee()->cartthrob->cart->subtotal();
        $checkoutOptions['subtotal_plus_tax'] = ee()->cartthrob->cart->subtotal_with_tax();
        $checkoutOptions['total'] = ee()->cartthrob->cart->total();

        if (ee()->input->post('TX')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('TX')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $checkoutOptions['total'] -= $checkoutOptions['tax'];
                $checkoutOptions['tax'] = $data;
                $checkoutOptions['total'] += $checkoutOptions['tax'];
                unset($checkoutOptions['subtotal_plus_tax']);
                unset($checkoutOptions['shipping_plus_tax']);
            }
        }

        if (ee()->input->post('SHP')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('SHP')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $checkoutOptions['total'] -= $checkoutOptions['shipping'];
                $checkoutOptions['shipping'] = $data;
                $checkoutOptions['total'] += $checkoutOptions['shipping'];
                unset($checkoutOptions['shipping_plus_tax']);
            }
        } elseif (ee()->input->post('AUS') && ee()->input->post('shipping') !== false && bool_string(ee('Encrypt')->decode(ee()->input->post('AUS')))) {
            $data = ee('Security/XSS')->clean(ee()->input->post('shipping'));

            $checkoutOptions['total'] -= $checkoutOptions['shipping'];
            $checkoutOptions['shipping'] = $data;
            $checkoutOptions['total'] += $checkoutOptions['shipping'];
            unset($checkoutOptions['shipping_plus_tax']);
        }

        $checkoutOptions['group_id'] = 5;

        if (ee()->input->post('GI')) {
            $checkoutOptions['group_id'] = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('GI')));

            if ($checkoutOptions['group_id'] < 5) {
                $checkoutOptions['group_id'] = 5;
            }
        }

        if (ee()->input->post('PR')) {
            $data = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('PR')));

            if ($data == sanitize_number($data)) { // ignore a non-numeric input
                $checkoutOptions['total'] -= $checkoutOptions['subtotal'];
                $checkoutOptions['subtotal'] = $data;
                $checkoutOptions['total'] += $checkoutOptions['subtotal'];
                unset($checkoutOptions['subtotal_plus_tax']);
            }
        } elseif (ee()->input->post('AUP') && bool_string(ee('Encrypt')->decode(ee()->input->post('AUP')))) {
            $checkoutOptions['total'] = sanitize_number(ee()->input->post('price', true));
        }

        $checkoutOptions['subscription'] = (bool)((ee()->input->post('SUB') && bool_string(ee('Encrypt')->decode(ee()->input->post('SUB')))) || ee()->input->post('sub_id'));
        $checkoutOptions['subscription_options'] = [];

        $checkoutOptions['force_vault'] = bool_string(ee('Encrypt')->decode(ee()->input->post('VLT')));
        $checkoutOptions['force_processing'] = bool_string(ee('Encrypt')->decode(ee()->input->post('FPR')));

        if (isset($_POST['member_id']) && in_array(ee()->session->userdata('group_id'), ee()->config->item('cartthrob:admin_checkout_groups'))) {
            $checkoutOptions['member_id'] = ee()->session->cache['cartthrob']['member_id'] = ee()->input->post('member_id');
        }

        if (ee()->input->post('vault_id')) {
            $vault_id = ee('Security/XSS')->clean(ee('Encrypt')->decode(ee()->input->post('vault_id')));

            if ($vault_id == sanitize_number($vault_id)) { // ignore a non-numeric input
                ee()->load->model('vault_model');

                if ($vault = ee()->vault_model->get_vault($vault_id)) {
                    $checkoutOptions['vault'] = $vault;
                }
            }
        }

        // if the sub_id is passed in, we're deleting the cart contents, and only updating the sub
        if (ee()->input->post('sub_id')) {
            $checkoutOptions['update_subscription_id'] = ee()->input->post('sub_id');
        }

        return $checkoutOptions;
    }

    protected function processSubscriptionOptions(array &$checkoutOptions)
    {
        if (false === $checkoutOptions['subscription']) {
            return;
        }

        // these are all of the subscription options
        ee()->load->model('subscription_model');

        // iterating through those options. if they're in post, we'll add them to the "subscription_options" meta
        foreach (ee()->subscription_model->option_keys() as $encoded_key => $key) {
            $option = null;

            if (ee()->input->post($encoded_key)) {
                $option = ee('Encrypt')->decode(ee()->input->post($encoded_key));
            } else {
                if (ee()->input->post('subscription_' . $key)) {
                    if ($key == 'name' || $key == 'description') {
                        $option = ee()->input->post('subscription_' . $key);
                    } else {
                        $option = ee('Encrypt')->decode(ee()->input->post('subscription_' . $key));
                    }
                } else {
                    if (ee()->input->post($key)) {
                        $option = ee()->input->post($key);
                    }
                }
            }

            if (!is_null($option)) {
                if (in_array($encoded_key, ee()->subscription_model->encoded_bools())) {
                    $option = bool_string($option);
                }

                if (strncmp($key, 'subscription_', 13) === 0) {
                    $key = substr($key, 13);
                }

                $checkoutOptions['subscription_options'][$key] = $option;
            }
        }
    }

    protected function createUser(array &$checkoutOptions)
    {
        if (false === $checkoutOptions['create_user']) {
            return;
        }

        $checkoutOptions['create_username'] = ee()->input->post('username');
        $checkoutOptions['create_email'] = ee()->input->post('email_address') ? ee()->input->post('email_address') : ee()->cartthrob->cart->customer_info('email_address');
        $checkoutOptions['create_screen_name'] = ee()->input->post('screen_name', true);
        $checkoutOptions['create_password'] = ee()->input->post('password');
        $checkoutOptions['create_group_id'] = $checkoutOptions['group_id'];
        $checkoutOptions['create_password_confirm'] = ee()->input->post('password_confirm');
        $checkoutOptions['create_language'] = ee()->cartthrob->cart->customer_info('language');
    }
}
