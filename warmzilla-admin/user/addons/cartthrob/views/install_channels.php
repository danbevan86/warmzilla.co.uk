<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<caption>Manual Update</caption>
		<tbody>
			<tr class="odd">
				<td>
					<label style="height:100%;">Run manual update</label>
				</td>
				<td style="width:50%;">
					<a class="submit" href="<?php echo ee('CP/URL')->make('addons/settings/cartthrob/update'); ?>">Run</a>
				</td>
			</tr>
		</tbody>	
	</table>
	
<?php if (count($templates_installed)) : ?>
<div class="box">
	<h1><?=lang('installed')?></h1>
	<div class="txt-wrap"><div class="alert inline success">
	<ul class="arrow-list">
		<?php foreach ($templates_installed as $installed) : ?>
			<li><?=$installed?></li>
		<?php endforeach; ?>
	</ul>
	</div></div>
</div>
<?php endif; ?>
<?php if (count($template_errors)) : ?>
<div class="box">
	<h1><?=lang('errors')?></h1>
	<div class="txt-wrap"><div class="alert inline issue">
	<ul class="arrow-list">
		<?php foreach ($template_errors as $error) : ?>
			<li>
				<?=$error?>
			</li>
		<?php endforeach; ?>
	</ul>
	</div></div>
</div>
<?php endif; ?>


 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<caption><?=lang('install_channels_header')?></caption>
		<thead class="visualEscapism">
			<tr>
				<th><?=lang('preference')?></th><th><?=lang('setting')?></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($install_channels)) : ?>
				<tr class="<?=alternator('odd', 'even')?>">
					<td>
						<label style="height:100%;"><?=lang('section')?></label>
					</td>
					<td style='width:50%;'>
						<ul class="without-bullets">
                            <?php foreach ($install_channels as $index => $name): ?>
                                <li>
                                    <label class="radio">
                                        <input type="checkbox" checked="checked" name="channels[]" class="templates" value="<?=$index?>" />
                                        <?=$name?>
                                    </label>
                                </li>
                            <?php endforeach; ?>
						</ul>
					</td>
				</tr>
 			<?php endif; ?>
			<?php if (count($install_template_groups)) : ?>
			<?php $checked = array('cart', 'cart_examples', 'cart_multi_page_checkout', 'cart_includes'); ?>
				<tr class="<?=alternator('odd', 'even')?>">
					<td>
						<label style="height:100%;"><?=lang('templates')?></label>
					</td>
					<td style='width:50%;'>
						<ul class="without-bullets">
						<?php foreach ($install_template_groups as $index => $name): ?>
						<li>
							<label class="radio">
								<input type="checkbox" <?php if (in_array($name, $checked)) : ?>checked="checked" <?php endif; ?>name="templates[]" class="templates" value="<?=$index?>" />
								<?=$name?>
							</label>
						</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>	
			<?php endif; ?>
			<?php if (count($install_member_groups)) : ?>
			
 			<tr class="<?=alternator('odd', 'even')?>">
				<td>
					<label style="height:100%;"><?=lang('section')?></label>
				</td>
				<td style='width:50%;'>
					<ul class="without-bullets">
					<?php foreach ($install_member_groups as $index => $name): ?>
						<li>
							<label class="radio">
								<input type="checkbox" checked="checked" name="templates[]" class="templates" value="<?=$index?>" />
								<?=$name?>
							</label>
						</li>
					<?php endforeach; ?>
					</ul>
				</td>
			</tr>
			<?php endif; ?>
		</tbody>	
	</table>

<?php if ($themes) : ?>	
	<p><input type="submit" name="submit" value="Install Templates &amp; Channels" class="submit"></p>

	</div>
</form>

	<?=str_repeat(BR, 2);?>
	<?=form_open(ee('CP/URL')->make('addons/settings/cartthrob/install_theme'))?>

	<div>
		
<?php if (count($themes_installed)) : ?>
<div class="box">
	<h1><?=lang('installed')?></h1>
	<div class="txt-wrap"><div class="alert inline success">
	<ul class="arrow-list">
		<?php foreach ($themes_installed as $installed) : ?>
			<li><?=$installed?></li>
		<?php endforeach; ?>
	</ul>
	</div></div>
</div>
<?php endif; ?>
<?php if (count($theme_errors)) : ?>
<div class="box">
	<h1><?=lang('errors')?></h1>
	<div class="txt-wrap"><div class="alert inline success">
	<ul class="arrow-list">
		<?php foreach ($theme_errors as $error) : ?>
			<li>
				<?=$error?>
			</li>
		<?php endforeach; ?>
	</ul>
	</div></div>
</div>
<?php endif; ?>
		
 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<caption>Install A Theme</caption>
		<tbody>
			<tr class="odd">
				<td>
					<label style="height:100%;">Select a theme to install:</label>
				</td>
				<td style="width:50%;">
					<?=form_dropdown('theme', $themes)?>
				</td>
			</tr>
		</tbody>	
	</table>
<?php endif; ?>

<p><input type="submit" name="submit" value="Install Templates &amp; Channels" class="submit"></p>

</div>
</form>
