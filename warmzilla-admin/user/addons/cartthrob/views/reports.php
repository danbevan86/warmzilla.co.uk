<?=form_open(ee('CP/URL')->make('addons/settings/cartthrob/reports'), 'id="reports_filter"')?>
	Report <?=form_dropdown('report', $reports, $current_report)?>
	<?=form_submit('', lang('refresh'), 'class="submit"')?>
<?=form_close()?>
<?php if ($current_report) : ?>
<div id="reports_view">
	<?=$view?>
</div>
<?php else : ?>
<?=$view?>
<?php endif; ?>

<?=$order_totals?>

<?=form_open(ee('CP/URL')->make('addons/settings/cartthrob/reports', array('save' => '1')))?>
	<?=$reports_list?>
	<?=form_submit('', lang('submit'), 'class="submit"')?>
<?=form_close()?>
