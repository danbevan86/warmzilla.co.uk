<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th colspan="2">
				<strong><?=lang('beta_features')?></strong><br />
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				<p><?=lang('beta_disclaimer')?></p>
			</td>
		</tr>
	</tbody>
</table>

<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th colspan="2">
				<strong>Multi-Site Manager</strong><br />
			</th>
		</tr>
	</thead>
	<tbody>
		<tr class="<?php echo alternator('even', 'odd');?>">
			<td>
				<label><?=lang('msm_show_all')?></label>
				<div class="subtext"><?=lang('msm_show_all_description')?></div>
			</td>
			<td style='width:50%;'>
				<div class="setting-field">
					<label class="choice mr<?php if ($settings['msm_show_all']) : ?> chosen<?php endif; ?>">
						<input class='radio' type='radio' name='msm_show_all' value='1' <?php if ($settings['msm_show_all']) : ?>checked='checked'<?php endif; ?> />
						<?=lang('yes')?>
					</label>
					<label class="choice mr<?php if ( ! $settings['msm_show_all']) : ?> chosen<?php endif; ?>">
						<input class='radio' type='radio' name='msm_show_all' value='0' <?php if ( ! $settings['msm_show_all']) : ?>checked='checked'<?php endif; ?> />
						<?=lang('no')?>
					</label>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th colspan="2">
				<strong>Orders</strong><br />
			</th>
		</tr>
	</thead>
	<tbody>
		<tr class="<?php echo alternator('even', 'odd');?>">
			<?php
				$setting = (int) ($settings['orders_async_method'] ?? 0);
				$url = htmlentities(ee()->paths->build_action_url('Cartthrob', 'consume_async_job', ['limit' => 5]), ENT_QUOTES, 'UTF-8', false);
			?>
			<td>
				<label><?=lang('orders_async_method')?></label>
				<div class="subtext"><?php printf(lang('orders_async_method_description'), $url); ?></div>
			</td>
			<td style='width:50%;'>
				<?=form_dropdown('orders_async_method', array(lang('disabled'), lang('orders_async_method_http'), lang('orders_async_method_cron')), $setting)?>
			</td>
		</tr>
		<tr class="<?php echo alternator('even', 'odd');?>">
			<?php $setting = $settings['orders_async_worker_base_url'] ?? ''; ?>
			<td>
				<label><?=lang('orders_async_worker_base_url')?></label>
				<div class="subtext"><?=lang('orders_async_worker_base_url_description')?></div>
			</td>
			<td style='width:50%;'>
				<input  dir='ltr' type='text' name='orders_async_worker_base_url' id='orders_async_worker_base_url' value='<?=e($setting)?>' size='90' maxlength='254' />
			</td>
		</tr>
	</tbody>
</table>
