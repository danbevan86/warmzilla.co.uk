<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<caption><?=lang('number_format_defaults_header')?></caption>
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('number_format_defaults_heading')?></strong>
					<br />
					<?=lang('number_format_defaults_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_decimals')?></label>
 				</td>
				<td style='width:50%;'>
					<input  dir='ltr' type='text' name='number_format_defaults_decimals' id='number_format_defaults_decimals' value='<?=e($settings['number_format_defaults_decimals'])?>' size='90' maxlength='100' />
				</td>
			</tr>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_dec_point')?></label>
 				</td>
				<td style='width:50%;'>
					<input  dir='ltr' type='text' name='number_format_defaults_dec_point' id='number_format_defaults_dec_point' value='<?=e($settings['number_format_defaults_dec_point'])?>' size='90' maxlength='100' />
				</td>
			</tr>			
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_thousands_sep')?></label>
 				</td>
				<td style='width:50%;'>
					<input  dir='ltr' type='text' name='number_format_defaults_thousands_sep' id='number_format_defaults_thousands_sep' value='<?=e($settings['number_format_defaults_thousands_sep'])?>' size='90' maxlength='100' />
				</td>
			</tr>			
 			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_prefix')?></label>
 				</td>
				<td style='width:50%;'>
					<?=form_input(array('name' => 'number_format_defaults_prefix', 'id' => 'number_format_defaults_prefix', 'value' => $settings['number_format_defaults_prefix'], 'size' => '90', 'maxlength' => '100', 'dir' => 'ltr'))?>
				</td>
			</tr>
 			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_prefix_position')?></label>
 				</td>
				<td style='width:50%;'>
					<label class="block">
					<input class='radio' type='radio' name='number_format_defaults_prefix_position' value='BEFORE' <?php if (! $settings['number_format_defaults_prefix_position'] || $settings['number_format_defaults_prefix_position'] =="BEFORE") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('before')?>
					</label>
					<label class="block">
				<input class='radio' type='radio' name='number_format_defaults_prefix_position' value='AFTER' <?php if ($settings['number_format_defaults_prefix_position'] == "AFTER") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('after')?>
					</label>
			</tr>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('number_format_defaults_currency_code')?></label>
 				</td>
				<td style='width:50%;'>
					<input  dir='ltr' type='text' name='number_format_defaults_currency_code' id='number_format_defaults_currency_code' value='<?=e($settings['number_format_defaults_currency_code'])?>' size='90' maxlength='100' />
				</td>
			</tr>
 			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<label><?=lang('round_to')?></label>
					<div class="subtext"><?=lang('rounding_description')?></div>
 				</td>
				<td style='width:50%;'>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='standard' <?php if (! $settings['rounding_default'] || $settings['rounding_default'] =="standard") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('rounding_standard')?>

					</label>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='round_up' <?php if (! $settings['rounding_default'] || $settings['rounding_default'] =="round_up") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('round_up')?>

					</label>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='round_down' <?php if (! $settings['rounding_default'] || $settings['rounding_default'] =="round_down") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('round_down')?>

					</label>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='round_up_extra_precision' <?php if (! $settings['rounding_default'] || $settings['rounding_default'] =="round_up_extra_precision") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('round_up_extra_precision')?>
					
					</label>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='swedish' <?php if ($settings['rounding_default'] == "swedish") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('rounding_swedish')?>

					</label>
					<label class="block">
					<input class='radio' type='radio' name='rounding_default' value='new_zealand' <?php if ($settings['rounding_default'] == "new_zealand") : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('rounding_new_zealand')?>
					</label>
				</td>
			</tr>
		</tbody>
	</table>
