<?php
class Cartthrob_divido extends Cartthrob_payment_gateway
{

	public $title = 'divido_title';
 	public $overview = 'divido_overview';
	public $language_file = TRUE;
	public $divido = "";

	public $settings = array(
		array(
			'name' =>  'divido_public_key',
			'short_name' => 'public_key',
			'type' => 'text',
			'default' => '',
		),
		array(
			'name' =>  'divido_secret_key',
			'short_name' => 'secret_key',
			'type' => 'text',
			'default' => '',
		),
		array(
			'name' => 'divido_test_mode',
			'short_name' => 'test_mode',
			'type' => 'radio',
			'default' => 'test',
			'options' => array(
				'test' => 'test',
				'live' => 'live'
			)
		)
	);

	public $required_fields = array();

	public $fields = array(
		'first_name',
		'middle_name',
		'last_name',
		'date_of_birth',
		'address',
		'address2',
		'city',
		'state',
		'zip',
		'phone',
		'email_address',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_address',
		'shipping_address2',
		'shipping_city',
		'shipping_state',
		'shipping_zip'
 	);

 	public $hidden = array();

	public $card_types = NULL;

	public function initialize()
	{
		$apiKey = $this->plugin_settings('public_key');
		if($apiKey != "")
		{
			require_once("libraries/divido-api-php/lib/Divido.php");
			Divido::setMerchant($apiKey);
		}
	}

	public function charge($credit_card_number)
	{

		$productInformation = json_decode(base64_decode($_COOKIE['productInformation']));
		$item = $this->order('items');

		$item = $item[0];
		$productID = "product_" . $item['entry_id'];
		$country = $this->order('shipping_country_code');
		if($country == "GBR")
		{
			$country = "GB";
		}

		$metaData = array(
			"country" 	=> $country,
			"finance" 	=> $productInformation->$productID->finance,
			"deposit" 	=> $productInformation->$productID->depositAmount,
			"amount"  	=> $this->order('total'),
			/*"reference"=>$_POST['reference'],*/
			"customer" => array(
				/*"title" 		=> $this->order('title'),*/
				"first_name" 	=> $this->order('shipping_first_name'),
				"middle_name"	=> $this->order('middle_name'),
				"last_name"	 	=> $this->order('shipping_last_name'),
				"date_of_birth"	=> $this->order('date_of_birth'),
				"email" 		=> $this->order('email_address'),
				"mobileNumber" 	=> $this->order('phone'),
				"phoneNumber" 	=> $this->order('phone'),
				"postcode" 		=> $this->order('shipping_zip'),
				"country" 		=> $country,
				"address" 		=> array(
					"text" 			=> $this->order('shipping_address'),
					"street"		=> $this->order('shipping_address2'),
					"postcode"		=> $this->order('shipping_zip'),
					"flat"			=> "",
					"town"			=> $this->order('shipping_city'),
				),
			),

			"metadata" => array(
				"orderNumber"		=> $this->order('order_id'),
				'invoiceNumber' 	=> $this->order('invoice_number'),
				"checksum"			=> md5($this->order('order_id') . ":" . ee()->config->item('encryption_key')),
				'orderTitle' 		=> $this->order('title'),
				'memberID' 			=> $this->order('member_id'),
				'customerIP' 		=> $this->order('ip_address'),
				'billingName' 		=> trim($this->order('first_name') . ' ' . $this->order('last_name')),
				'billingCompany' 	=> $this->order('company'),
				'billingAddress1' 	=> $this->order('address'),
				'billingAddress2' 	=> $this->order('address2'),
				'billingCity' 		=> $this->order('city'),
				'billingPhone' 		=> $this->order('phone'),
				'email' 			=> $this->order('email_address'),
				'total' 			=> $this->order('total'),
				'subTotal' 			=> $this->order('subtotal') - $this->order('discount'),
				'tax1' 				=> $this->order('tax'),
				'trackingCode'		=> "CompareBoilerQuotes",
			),

			"products" => array(
				array(
					"type" 		=> "product",
					"text" 		=> $item['title'],
					"quantity" 	=> 1,
					"value" 	=> $this->order('total'),
				)
			),

			"response_url" => $this->response_script(ucfirst(get_class($this)), array("webhook", $this->order('order_id'))),
			'checkout_url' => $this->response_script(ucfirst(get_class($this)), array("checkout", $this->order('order_id'))),
			"redirect_url" => $this->response_script(ucfirst(get_class($this)), array("callback", $this->order('order_id'))),
		);

		$response = Divido_CreditRequest::create($metaData);
		if ($response->status == 'ok') {
			$this->completePaymentOffsite($response->url, $metaData);
		} else {
			echo "<pre> somewher : ";
			print_r($response->__toArray());
			exit();
		}

	}

	public function extload($post)
	{

		if($post['ct_option'] == "")
		{
			echo $this->lang('error_ext_load');exit();
		}
		else
		{
			require_once $_SERVER["DOCUMENT_ROOT"] . "/warmzilla-admin/ee/Mexitek/PHPColors/Color.php";
			$this->relaunch_cart(NULL, $post['ct_option']);
		}

		if (isset($_SERVER['HTTP_RAW_POST_DATA']) && $_SERVER['HTTP_RAW_POST_DATA'])
		{
			$dividoData = file_get_contents($_SERVER['HTTP_RAW_POST_DATA']);
		}
		else
		{
			$dividoData = file_get_contents('php://input');
		}

		$auth = array(
			'authorized' 		=> FALSE,
			'declined' 			=> FALSE,
			'processing'    	=> FALSE,
			'canceled' 			=> FALSE,
			'expired' 			=> FALSE,
			'failed' 			=> FALSE,
			'error_message' 	=> NULL,
			'transaction_id'	=> NULL,
			'refunded' 			=> FALSE,
		);

		if($post['ct_action'] == 'callback')
		{
			$auth['authorized'] = TRUE;
			$auth['error_message'] = $this->lang('divido_payment_ready');
			unset($auth['transaction_id']);
		}
		else
		{
			if($dividoData != "")
			{

				$dividoData = json_decode($dividoData, true);

				switch ($dividoData['status'])
				{

					case "PROPOSAL":
						$auth['processing'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_processing_proposal');
						break;

					case "ACCEPTED":
						$auth['processing'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_processing_accepted');
						break;

					case "DEPOSIT-PAID":
						$auth['processing'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_deposit_paid');
						break;

					case "SIGNED":
						$auth['processing'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_signed');
						break;

					case "READY":
						$auth['authorized'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_ready');
						break;

					case "AWAITING-ACTIVATION":
						$auth['authorized'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_waiting_activation');
						break;

					case "ACTIVATED":
						$auth['authorized'] = TRUE;
						$auth['complete'] 	= TRUE;
						$auth['completed'] 	= TRUE;
						$auth['error_message'] = $this->lang('divido_payment_activated');
						break;

					case "AWAITING-CANCELLATION":
						$auth['canceled'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_waiting_cancellation');
						break;

					case "CANCELED":
						$auth['canceled'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_canceled');
						break;

					case "CANCELLED":
						$auth['canceled'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_canceled');
						break;

					case "REFUNDED":
						$auth['refunded'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_refunded');
						break;

					case "EXPIRED":
						$auth['expired'] = TRUE;
						$auth['error_message'] = $this->lang('divido_payment_expired');
						break;

					default:
						$auth['failed'] = TRUE;
						$auth['error_message'] = "Unknown status return : " . $dividoData['status'];
						break;
				}

				$auth['transaction_id'] = $dividoData['application'];

				$insertData = array(
					'entry_id' => $this->order('order_id'),
					'field_id_56' => $auth['transaction_id'],
				);

				ee()->db->insert('channel_data_field_56', $insertData);
				unset($insertData);
			}
		}

		if(is_array($dividoData))
		{
			$dividoData = "coded : " . json_encode($dividoData);
		}

		$dataToLog = "==================================================\n";
		$dataToLog .= date('l jS \of F Y h:i:s A') . "\n";
		$dataToLog .= "My Json product data : " . json_encode($post);
		$dataToLog .= "\n";
		$dataToLog .= "My Divido data : " . $dividoData;
		$dataToLog .= "\n";
		$dataToLog .= "Auth : " . json_encode($auth);
		$dataToLog .= "\n";
		$dataToLog .= "==================================================\n";
		$this->addLog($dataToLog);

		if($post['ct_action'] == 'callback')
		{
			$this->gateway_order_update($auth, $this->order('order_id'), $this->order('return'));
		}
		else
		{
			$this->gateway_order_update($auth, $this->order('order_id'));
		}
		exit;

	}

	function addLog($data)
	{
		file_put_contents(__DIR__ . '/logs.txt', $data.PHP_EOL , FILE_APPEND | LOCK_EX);
	}

	public function refund($transaction_id, $amount, $card_num, $order_id=NULL)
	{

		echo "transaction_id: " . $transaction_id . "<br />";
		echo "amount: " . $amount . "<br />";
		echo "card_num: " . $card_num . "<br />";
		echo "order_id: " . $order_id . "<br />";

		$response = Divido_Refund::refund(array(
			'application' => $transaction_id,
			'amount' 	=> $amount,

			'merchant' 	=> $this->plugin_settings('public_key'),
		), $this->plugin_settings('public_key'));

		if ($response->status == 'ok')
		{
			return array(
				'authorized' => TRUE,
				'failed' => FALSE,
				'declined' => FALSE,
				'error_message' => NULL,
				'transaction_id' => $response->result->id,
			);
		}
		else
		{
			return array(
				'authorized' => FALSE,
				'failed' => TRUE,
				'declined' => FALSE,
				'error_message' => "Can't refund amount."
			);
		}

		exit();
	}

}

?>