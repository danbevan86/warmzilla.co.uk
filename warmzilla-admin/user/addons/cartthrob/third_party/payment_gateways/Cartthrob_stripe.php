<?php

use CartThrob\CartThrobException;
use CartThrob\Transactions\TransactionState;
use Omnipay\Omnipay;
use Omnipay\Stripe\Message\AbstractRequest;
use Omnipay\Stripe\Message\Response;
use Omnipay\Stripe\PaymentIntentsGateway as OmnipayGateway;

class Cartthrob_stripe extends Cartthrob_payment_gateway
{
    /**
     * @var string
     */
    const STRIPE_VERSION = '2019-02-11';

    /**
     * @var string
     */
    const STATUS_REQUIRES_ACTION = 'requires_action';

    /**
     * @var string
     */
    const STATUS_REQUIRES_CONFIRMATION = 'requires_confirmation';

    /**
     * @var string
     */
    const STATUS_REQUIRES_CAPTURE = 'requires_capture';

    /**
     * @var string
     */
    const STATUS_SUCCEEDED = 'succeeded';

    /**
     * @var string
     */
    const DEFAULT_ERROR_MESSAGE = 'stripe_unknown_error';

    /**
     * @var string
     */
    public $title = 'stripe_title';

    /**
     * @var string
     */
    public $overview = 'stripe_overview';

    /**
     * @var array
     */
    public $settings = [
        [
            'name' => 'mode',
            'short_name' => 'mode',
            'type' => 'select',
            'default' => 'test',
            'options' => [
                'test' => 'stripe_mode_test',
                'live' => 'stripe_mode_live',
            ],
        ],
        [
            'name' => 'stripe_private_key',
            'short_name' => 'api_key_test_secret',
            'type' => 'text',
        ],
        [
            'name' => 'stripe_api_key',
            'short_name' => 'api_key_test_publishable',
            'type' => 'text',
        ],
        [
            'name' => 'stripe_live_key_secret',
            'short_name' => 'api_key_live_secret',
            'type' => 'text',
        ],
        [
            'name' => 'stripe_live_key',
            'short_name' => 'api_key_live_publishable',
            'type' => 'text',
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'first_name',
        'last_name',
        'address',
        'address2',
        'city',
        'state',
        'zip',
        'phone',
        'email_address',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_address',
        'shipping_address2',
        'shipping_city',
        'shipping_state',
        'shipping_zip',
    ];

    /**
     * @var array
     */
    public $nameless_fields = [];

    /**
     * @var string
     */
    public $embedded_fields = <<<EOS
<fieldset class="credit_card_info" id="credit_card_info">
    <legend>Credit Card Info</legend>
    <div class="control-group" style="margin:auto;width:80%">
        <!-- placeholder for Elements -->
        <div id="card-element"></div>
    </div>
</fieldset>
EOS;

    /**
     * @var OmnipayGateway
     */
    protected $omnipayGateway;

    public function initialize()
    {
        ee()->load->library('paths');

        $key = ($this->plugin_settings('mode') === 'live') ? $this->plugin_settings('api_key_live_publishable') : $this->plugin_settings('api_key_test_publishable');
        $apiKey = ($this->plugin_settings('mode') === 'live') ? $this->plugin_settings('api_key_live_secret') : $this->plugin_settings('api_key_test_secret');

        $this->omnipayGateway = Omnipay::create('Stripe\\PaymentIntents');
        $this->omnipayGateway->initialize(['apiKey' => $apiKey]);

        $this->form_extra = '<script src="https://js.stripe.com/v3/"></script>';
        $this->form_extra .= sprintf('<script src="%s"></script>', $this->themeFolderUrl('user/cartthrob/scripts/ender.min.js'));
        $this->form_extra .= sprintf('<script src="%s"></script>', $this->themeFolderUrl('user/cartthrob/scripts/cartthrob-tokenizer.js'));
        $this->form_extra .= sprintf('<script>var stripe = Stripe("%s");</script>', $key);
        $this->form_extra .= <<<EOS
<script type="text/javascript">
    var elements = stripe.elements();
    var cardElement = elements.create("card");
    cardElement.mount("#card-element");

    var cardButton = document.getElementById("complete_checkout");

    CartthrobTokenizer.init();

    cardButton.addEventListener("click", function(ev) {
        if($("input[name=gateway]:checked").data('gateway') == "Stripe")
        {
            ev.preventDefault();
            var cardholderName = [document.getElementById("card_first_name").value, document.getElementById("card_last_name").value].join(' ');
            stripe.createPaymentMethod("card", cardElement, {
                billing_details: {name: cardholderName}
            }).then(function(result) {
                if (result.error) {
                    CartthrobTokenizer.errorHandler(result.error.message);
                    CartthrobTokenizer.submissionState = false;
                } else {
                    CartthrobTokenizer.addHidden("payment-method-id", result.paymentMethod.id).submitHandler();
                    $('.overlay').show();
                }
            });
        }
        else
        {
            CartthrobTokenizer.submissionState = true;
            return true;
        }
    });
    cardElement.addEventListener('change', function(event) {
        if($("input[name=gateway]:checked").data('gateway') == "Stripe")
        {
            if (event.error) {
                CartthrobTokenizer.errorHandler(event.error.message);
            } else {
                CartthrobTokenizer.errorHandler("");
            }
        }
    });
</script>
EOS;
    }

    /**
     * @param $ignored
     * @return TransactionState
     */
    public function charge($ignored)
    {
        return $this->do_charge([
            'paymentMethod' => ee()->input->post('payment-method-id'),
            'confirm' => true,
            'returnUrl' => $this->generateReturnUrl(),
        ]);
    }

    public function refund($transactionId, $amount, $lastFour)
    {
        $state = new TransactionState();

        $paymentIntentReference = $this->createRequest('fetchPaymentIntent', ['paymentIntentReference' => $transactionId])->send();

        if ($charge = $paymentIntentReference->getData()['charges']['data'][0] ?? null) {
            $params['transactionReference'] = $charge['id'];

            if ($amount) {
                $params['amount'] = $amount;
            }

            $charge = (object)$this->createRequest('refund', $params)->send()->getData();

            if (empty($charge->failure_code) && ($charge->status === 'paid' || $charge->status === 'succeeded')) {
                return $state->setAuthorized()->setTransactionId($charge->id);
            }
        }

        return $state->setFailed(ee()->lang->line('stripe_refund_could_not_be_completed'));
    }

    /**
     * @param $params
     * @return TransactionState
     */
    protected function do_charge($params)
    {
        if (!isset($params['amount'])) {
            $params['amount'] = $this->total();
        }

        if (!isset($params['currency'])) {
            $currency = strtolower(($this->order('currency_code') ? $this->order('currency_code') : 'GBP'));

            $params['currency'] = $currency;
        }

        if (!isset($params['description'])) {
            $params['description'] = $this->order('title') . ' (' . $this->orderId() . ')';
        }

        // Stripe allows for metadata to be attached to the charge object.
        $params['metadata'] = $this->prepare_metadata($_POST);

        try {
            $response = $this->createRequest('authorize', $params)->send();

            $paymentIntentReference = $response->getPaymentIntentReference();

            return $this->checkPaymentIntent($paymentIntentReference, $response->getData());
        } catch (Exception $e) {
            return $this->fail($e->getMessage());
        }
    }

    /**
     * @param array $data
     * @return TransactionState
     */
    public function completeCheckout(array $data)
    {
        try {
            return $this->confirm($data['payment_intent']);
        } catch (CartThrobException $e) {
            return $this->fail($e->getMessage());
        }
    }

    protected function prepare_metadata($data)
    {
        // Stripe's requirements for metadata are:
        // 1. Up to 20 keys
        // 2. Key names up to 40 characters
        // 3. Values up to 500 characters
        // See: https://stripe.com/docs/api#metadata
        $metadata = [];
        $maxKeys = 20;
        // Look for keys in the format meta:XYZ
        $keyCount = 0;
        foreach ($data as $k => $v) {
            if ($keyCount == $maxKeys) {
                break;
            }
            if (substr($k, 0, 5) == 'meta:' && strlen($k) > 5) {
                $key = substr($k, 5, 40);
                $value = substr($v, 0, 500);
                $metadata[$key] = $value;
                $keyCount++;
            }
        }

        return $metadata;
    }

    /**
     * @param string $paymentIntent
     * @param array $data
     * @throws Exception
     * @return TransactionState
     */
    protected function checkPaymentIntent($paymentIntent = "", array $data)
    {
        if (isset($data['error'])) {
            return $this->fail($data['error']['message']);
        }

        if (self::STATUS_REQUIRES_CAPTURE === $data['status']) {
            return $this->capture($paymentIntent);
        }

        if (self::STATUS_REQUIRES_ACTION === $data['status']) {
            // This will redirect if necessary, so we only need to check
            // if the response is successful.
            return $this->confirm($paymentIntent);
        }

        return $this->fail();
    }

    /**
     * @param string $paymentIntent
     * @return void|TransactionState
     */
    protected function confirm(string $paymentIntent)
    {
        /** @var Response $response */
        $response = $this->createRequest('confirm', [
            'paymentIntentReference' => $paymentIntent,
            'returnUrl' => $this->generateReturnUrl(),
        ])->send();

        if ($response->isRedirect()) {
            $response->redirect();

            // The above was not automatically finishing the request?
            return;
        }

        if (!$response->isSuccessful()) {
            throw new CartThrobException(ee()->lang->line('stripe_sca_failed'));
        }

        return $this->checkPaymentIntent($paymentIntent, $response->getData());
    }

    /**
     * @param string $paymentIntent
     * @return TransactionState
     */
    protected function capture(string $paymentIntent)
    {
        $response = $this->createRequest('capture', [
            'paymentIntentReference' => $paymentIntent,
        ])->send()->getData();

        if (self::STATUS_SUCCEEDED === $response['status']) {
            return $this->authorize($response['id']);
        }

        return $this->fail();
    }

    protected function generateReturnUrl()
    {
        ee()->load->library('paths');

        $enc = ee('Encrypt');

        return ee()->paths->build_action_url(
            'Cartthrob',
            'payment_return_action',
            [
                'method' => base64_encode($enc->encode('completeCheckout')),
                'gateway' => base64_encode($enc->encode(__CLASS__)),
                'orderId' => base64_encode($enc->encode($this->orderId())),
            ]
        );
    }

    /**
     * @param $ignored
     * @return Cartthrob_token
     */
    public function createToken($ignored)
    {
        $token = new Cartthrob_token();

        // if there's no token it means that the end user doesn't have javascript enabled
        if (false === ($card_token = ee()->input->post('stripeToken'))) {
            return $token->set_error_message(ee()->lang->line('stripe_javascript_required'));
        }

        try {
            $params = [
                'source' => $card_token,
                'email' => $this->order('email_address'),
                'description' => $this->customerId(),
            ];

            $customer = (object)$this->createRequest('createCustomer', $params)->send()->getData();

            if (!empty($customer->id)) {
                return $token->set_token($customer->id);
            }

            return $token->set_error_message(ee()->lang->line('stripe_unknown_error'));
        } catch (Exception $e) {
            return $token->set_error_message($e->getMessage());
        }
    }

    /**
     * @param $token
     * @return TransactionState
     */
    public function chargeToken($token)
    {
        return $this->do_charge(['customerReference' => $token]);
    }

    /**
     * @param string $method
     * @param array $params
     * @return AbstractRequest
     */
    protected function createRequest(string $method, array $params)
    {
        /** @var AbstractRequest $request */
        $request = $this->omnipayGateway->$method($params);
        $request->setStripeVersion(self::STRIPE_VERSION);

        return $request;
    }
}
