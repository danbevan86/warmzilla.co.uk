<?php

use CartThrob\Transactions\TransactionState;

class Cartthrob_vendigo extends Cartthrob_payment_gateway
{

	public $title = 'vendigo_title';
 	public $overview = 'vendigo_overview';
	public $language_file = TRUE;

	public $settings = array(
		array(
			'name' =>  'vendigo_public_key',
			'short_name' => 'public_key',
			'type' => 'text',
			'default' => '',
		),
		array(
			'name' =>  'vendigo_secret_key',
			'short_name' => 'secret_key',
			'type' => 'text',
			'default' => '',
		),
		array(
			'name' => 'vendigo_test_mode',
			'short_name' => 'test_mode',
			'type' => 'radio',
			'default' => 'test',
			'options' => array(
				'test' => 'test',
				'live' => 'live'
			)
		)
	);

	public $required_fields = array();

	public $fields = array(
		'first_name',
		'middle_name',
		'last_name',
		'date_of_birth',
		'address',
		'address2',
		'city',
		'state',
		'zip',
		'phone',
		'email_address',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_address',
		'shipping_address2',
		'shipping_city',
		'shipping_state',
		'shipping_zip'
 	);

 	public $hidden = array();

	public $card_types = NULL;

	public function initialize()
	{

	}

	public function charge($credit_card_number)
	{

		ee()->load->add_package_path(PATH_THIRD.'warmzilla_cp/');
		ee()->load->library('warmzilla_cp_lib', null, 'wcp');
		$boilerQuestions = ee()->wcp->getBoilerQuestionsArray(true);

		/*
			// New fields created in Orders channel and ID of those fields.
			// Replace IDs when create fields in live site
			vendigo_term : 288
			vendigo_deposit : 289
			boiler_questions_presaved : 291
		*/
		$entry = ee('Model')->get('ChannelEntry', $this->order('order_id'))->first();
		$entry->field_id_288 = isset($_COOKIE['vendigo_months']) ? $_COOKIE['vendigo_months'] : 36;
		$entry->field_id_289 = isset($_COOKIE['vendigo_deposit']) ? $_COOKIE['vendigo_deposit'] : 0;
		$entry->field_id_291 = json_encode($boilerQuestions);

		$entry->save();
		$this->completePaymentOffsite(rtrim(ee()->config->item('base_url'), '/') . "/my-order/vendigo-finance-form/" . $this->order('order_id'));
	}

	public function extload($post)
	{

		if(isset($post['applicationId']))
		{
			$vendigoData = $post;
		}
		elseif (isset($_SERVER['HTTP_RAW_POST_DATA']) && $_SERVER['HTTP_RAW_POST_DATA'])
		{
			$vendigoData = file_get_contents($_SERVER['HTTP_RAW_POST_DATA']);
			$vendigoData = json_decode($vendigoData, true);
		}
		else
		{
			$vendigoData = file_get_contents('php://input');
			$vendigoData = json_decode($vendigoData, true);
		}

		// $vendigoData = '{"applicationId":6827,"orderId":"order_2290","status":"APPROVED","eSignUrl":"https:\/\/trial.bonafidee.com\/signsolo.aspx?t=32714B21-77C0-4F1F-8381-47712BD63288","refNumber":"VTP4UENM","refNumberLender":"81319378","referredReason":"","satisfactionNoteUrl":null}';
		// $vendigoData = json_decode($vendigoData, true);

		if(! isset($vendigoData['orderId']))
		{
			$this->addLog("No Order Found!", $vendigoData, $post);
			exit();
		}

		$entryId = str_replace('order_', '', $vendigoData['orderId']);
		$entryId = str_replace("'", "", $entryId);

		$this->relaunchCart(null, $entryId);

		$state = new TransactionState();

		/*
			setAuthorized
			setPending
			setFailed
			setProcessing
			setCanceled
			setExpired
			setDeclined
		*/

		switch ($vendigoData['status'])
		{

			case "QUOTE":
				$state->setProcessing(ee()->lang->line('vendigo_payment_quote'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "STARTED":
				$state->setProcessing(ee()->lang->line('vendigo_payment_started'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "PENDING":
				$state->setProcessing(ee()->lang->line('vendigo_payment_pending'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "APPROVED":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_approved'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "CONDITIONALLY APPROVED":
				$state->setProcessing(ee()->lang->line('vendigo_payment_conditionally_approved'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "REFERRED":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_referred'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "DECLINED":
				$state->setDeclined(ee()->lang->line('vendigo_payment_declined'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "CANCELLED":
				$state->setCanceled(ee()->lang->line('vendigo_payment_cancelled'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "ACTIVE":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_active'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "SATISFACTION NOTE":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_satisfaction_note'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "ISSUE RAISED":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_issue_raised'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "TO BE PAID":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_to_be_paid'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "PAID":
				$state->setAuthorized(ee()->lang->line('vendigo_payment_paid'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "CONTRACTS PENDING":
				$state->setPending(ee()->lang->line('vendigo_payment_contracts_pending'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "NO MATCHING LENDER":
				$state->setFailed(ee()->lang->line('vendigo_payment_no_matching_lender'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "ERROR":
				$state->setFailed(ee()->lang->line('vendigo_payment_error'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "EXPIRED":
				$state->setExpired(ee()->lang->line('vendigo_payment_expired'))->setTransactionId($vendigoData['refNumber']);
				break;

			case "ADDRESS MISMATCH":
				$state->setFailed(ee()->lang->line('vendigo_payment_address_mismatch'))->setTransactionId($vendigoData['refNumber']);
				break;
		}

		// $this->checkoutCompleteOffsite($state, $entryId, Cartthrob_payments::COMPLETION_TYPE_STOP);
		$this->checkoutComplete($state, null, null, true);
		// echo "string";exit();
		/*
			// New fields created in Orders channel and ID of those fields.
			// Replace IDs when create fields in live site

			vendigo_application_id : 281
			vendigo_e_sign_url : 282
			vendigo_reference_number : 283
			vendigo_refrence_number_lender : 284
			vendigo_referred_reason : 285
			vendigo_satisfaction_note_url : 286
			vendigo_status : 287
			vendigo_term : 288
			vendigo_deposit : 289
		*/

		$entry = ee('Model')->get('ChannelEntry', $entryId)->first();
		$entry->field_id_281 = isset($vendigoData['applicationId']) ? $vendigoData['applicationId'] : "";
		$entry->field_id_280 = isset($vendigoData['eSignUrl']) ? $vendigoData['eSignUrl'] : "";
		$entry->field_id_283 = isset($vendigoData['refNumber']) ? $vendigoData['applicationId'] : "";
		$entry->field_id_284 = isset($vendigoData['refNumberLender']) ? $vendigoData['refNumberLender'] : "";
		$entry->field_id_285 = isset($vendigoData['referredReason']) ? $vendigoData['referredReason'] : "";
		$entry->field_id_286 = isset($vendigoData['satisfactionNoteUrl']) ? $vendigoData['satisfactionNoteUrl'] : "";
		$entry->field_id_287 = isset($vendigoData['status']) ? $vendigoData['status'] : "";

		if(in_array($vendigoData['status'], ["CANCELLED"]))
		{
			$entry->status = "Canceled";
		}
		elseif(in_array($vendigoData['status'], ["PENDING", "CONTRACTS PENDING"]))
		{
			$entry->status = "PENDING";
		}
		elseif(in_array($vendigoData['status'], ["ADDRESS MISMATCH", "NO MATCHING LENDER", "ERROR"]))
		{
			$entry->status = "Failed";
		}
		elseif(in_array($vendigoData['status'], ["DECLINED"]))
		{
			$entry->status = "Declined";
		}
		elseif(in_array($vendigoData['status'], ["EXPIRED"]))
		{
			$entry->status = "Expired";
		}

		$entry->save();

		$this->addLog("Order Webhook processed!", $vendigoData, $post);
		// $this->checkoutCompleteOffsite($state, $entryId, Cartthrob_payments::COMPLETION_TYPE_STOP);
		// exit();

	}

	function addLog($message, $data = [], $post = [])
	{
		$dataToLog = "==================================================\n";
		$dataToLog .= "Message : " . $message . "\n";
		$dataToLog .= date('l jS \of F Y h:i:s A') . "\n";
		$dataToLog .= "post : " . json_encode($post);
		$dataToLog .= "\n";
		$dataToLog .= "Vendigo data : " . json_encode($data);
		$dataToLog .= "\n";
		$dataToLog .= "==================================================\n";

		file_put_contents(__DIR__ . '/logs.txt', $dataToLog.PHP_EOL , FILE_APPEND | LOCK_EX);
	}

	public function refund($transaction_id, $amount, $card_num, $order_id=NULL)
	{

		echo "transaction_id: " . $transaction_id . "<br />";
		echo "amount: " . $amount . "<br />";
		echo "card_num: " . $card_num . "<br />";
		echo "order_id: " . $order_id . "<br />";

		$response = Divido_Refund::refund(array(
			'application' => $transaction_id,
			'amount' 	=> $amount,

			'merchant' 	=> $this->plugin_settings('public_key'),
		), $this->plugin_settings('public_key'));

		if ($response->status == 'ok')
		{
			return array(
				'authorized' => TRUE,
				'failed' => FALSE,
				'declined' => FALSE,
				'error_message' => NULL,
				'transaction_id' => $response->result->id,
			);
		}
		else
		{
			return array(
				'authorized' => FALSE,
				'failed' => TRUE,
				'declined' => FALSE,
				'error_message' => "Can't refund amount."
			);
		}

		exit();
	}

}

?>