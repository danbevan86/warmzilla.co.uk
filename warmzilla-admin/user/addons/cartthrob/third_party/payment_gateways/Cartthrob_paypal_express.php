<?php

use CartThrob\Transactions\TransactionState;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Item;
use Omnipay\Common\ItemBag;
use Omnipay\Omnipay;
use Omnipay\PayPal\ExpressGateway as OmnipayGateway;
use Omnipay\PayPal\Message\ExpressAuthorizeResponse;
use Omnipay\PayPal\Message\Response;

class Cartthrob_paypal_express extends Cartthrob_payment_gateway
{
    /**
     * @var array
     */
    const LOCALE_MAP = [
        'DE' => [
            'AUT' => 'AT',
            'DEU' => 'DE',
        ],
        'EN' => [
            'AUS' => 'AU',
            'CAN' => 'CA',
            'GBR' => 'GB',
            'USA' => 'US',
        ],
        'ES' => [
            'ES' => 'ES',
        ],
        'NL' => [
            'BEL' => 'BE',
            'NED' => 'NL',
        ],
        'IT' => [
            'IT' => 'IT',
        ],
        'FR' => [
            'FR' => 'FR',
        ],
        'ZH' => [
            'CN' => 'CN',
        ],
        'PL' => [
            'PL' => 'PL',
        ],
    ];

    /**
     * @var array
     */
    const SHIPPING_SETTING_MAP = [
        'addressOverride' => [
            'paypal_shipping',
            'static_shipping',
        ],
        'noShipping' => [
            'hide_shipping',
        ],
    ];

    public $title = 'paypal_express_title';
    public $overview = 'paypal_express_overview';
    public $required_fields = [];
    public $paypal_server;
    public $API_UserName;
    public $API_Password;
    public $API_Signature;
    public $paypal_offsite;
    private $API_Subject;
    private $endpoint;
    private $application_id;

    public $settings = [
        [
            'name' => 'paypal_express_api_username',
            'short_name' => 'api_username',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'paypal_express_api_password',
            'short_name' => 'api_password',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'paypal_express_signature',
            'short_name' => 'api_signature',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'paypal_express_sandbox_api_username',
            'short_name' => 'test_username',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'paypal_express_sandbox_api_password',
            'short_name' => 'test_password',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'paypal_express_sandbox_signature',
            'short_name' => 'test_signature',
            'type' => 'text',
            'default' => '',
        ],
        [
            'name' => 'mode',
            'short_name' => 'mode',
            'type' => 'radio',
            'default' => 'test',
            'options' => [
                'test' => 'sandbox',
                'live' => 'live',
            ],
        ],
        [
            'name' => 'paypal_express_allow_note',
            'note' => 'paypal_express_allow_note_note',
            'short_name' => 'allow_note',
            'type' => 'radio',
            'default' => 'no',
            'options' => [
                'no' => 'no',
                'yes' => 'yes',
            ],
        ],
        [
            'name' => 'paypal_express_show_item_id',
            'short_name' => 'show_item_id',
            'type' => 'radio',
            'default' => 'yes',
            'options' => [
                'no' => 'no',
                'yes' => 'yes',
            ],
        ],
        [
            'name' => 'paypal_express_show_item_options',
            'short_name' => 'show_item_options',
            'type' => 'radio',
            'default' => 'no',
            'options' => [
                'no' => 'no',
                'yes' => 'yes',
            ],
        ],
        [
            'name' => 'paypal_express_customization_settings_header',
            'short_name' => 'customization_settings_header',
            'type' => 'header',
        ],
        [
            'name' => 'paypal_express_header_image_url',
            'short_name' => 'header_image_url',
            'default' => '',
            'type' => 'text',
        ],
        [
            'name' => 'paypal_express_header_border_color_hex_value',
            'short_name' => 'header_border_color',
            'default' => '',
            'type' => 'text',
        ],
        [
            'name' => 'paypal_express_header_background_color_hex_value',
            'short_name' => 'header_background_color',
            'default' => '',
            'type' => 'text',
        ],
        [
            'name' => 'paypal_express_advanced_settings_header',
            'short_name' => 'advanced_settings_header',
            'type' => 'header',
        ],
        [
            'name' => 'paypal_account_preferences',
            'short_name' => 'solutiontype',
            'type' => 'radio',
            'default' => 'Mark',
            'options' => [
                'Sole' => 'paypal_sole',
                'Mark' => 'paypal_mark',
            ],
        ],
        [
            'name' => 'paypal_display_billing_page',
            'short_name' => 'display_billing_page',
            'type' => 'radio',
            'default' => 'Login',
            'note' => 'paypal_display_billing_page_note',
            'options' => [
                'Login' => 'paypal_show_login',
                'Billing' => 'paypal_show_credit',
            ],
        ],
        [
            'name' => 'paypal_express_shipping_settings',
            'short_name' => 'shipping_settings',
            'type' => 'select',
            'note' => 'paypal_express_no_shipping_note',
            'default' => 'hide_shipping',
            'options' => [
                'hide_shipping' => 'paypal_express_hide_shipping_address',
                'editable_shipping' => 'paypal_express_editable_shipping',
                'static_shipping' => 'paypal_express_static_shipping',
                'paypal_shipping' => 'paypal_express_paypal_shipping',
            ],
        ],
        [
            'name' => 'paypal_express_payment_action',
            'short_name' => 'payment_action',
            'type' => 'radio',
            'default' => 'Sale',
            'options' => [
                'Sale' => 'sale',
                'Authorization' => 'authorization',
            ],
        ],
    ];

    public $fields = [
        'first_name',
        'last_name',
        'address',
        'address2',
        'city',
        'state',
        'zip',
        'company',
        'country_code',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_address',
        'shipping_address2',
        'shipping_city',
        'shipping_state',
        'shipping_zip',
        'shipping_country_code',
        'phone',
        'email_address',
    ];

    /**
     * @var OmnipayGateway
     */
    public $omnipayGateway;

    public function initialize()
    {
        $mode = ('test' === $this->plugin_settings('mode')) ? 'test' : 'api';
        $this->omnipayGateway = Omnipay::create('PayPal_Express');
        $this->omnipayGateway->initialize([
            'username' => $this->plugin_settings("{$mode}_username"),
            'password' => $this->plugin_settings("{$mode}_password"),
            'signature' => $this->plugin_settings("{$mode}_signature"),
            'subject' => $this->plugin_settings("{$mode}_subject"),
            'testMode' => ('test' === $mode),
        ]);
    }

    /**
     * @param string $creditCardNumber
     * @return TransactionState|void
     */
    public function charge($creditCardNumber)
    {
        $state = new TransactionState();
        $data = $this->assemble_post_array();

        try {
            /** @var ExpressAuthorizeResponse $response */
            $response = $this->omnipayGateway->purchase($data)->send();

            if ($response->isRedirect()) {
                $this->completePaymentOffsite($response->getRedirectUrl());
            } else {
                if (null !== $response->getMessage()) {
                    $state->setFailed($response->getMessage() . ' ' . $response->getData()['L_ERRORCODE0']);
                }

                return $state;
            }
        } catch (\Exception $e) {
            return $state->setFailed(ee()->lang->line('paypal_express_did_not_respond'));
        }
    }

    public function assemble_post_array($method = 'SetExpressCheckout', $token = null, $payer_id = null)
    {
        // added to handle situations when a person has an alternate total and goes to PayPal.
        if ($method == 'DoExpressCheckoutPayment' && $this->order('pp_alt_total')) {
            $total = $this->order('pp_alt_total');
        } else {
            if ($this->total()) {
                $total = $this->total();
            } else {
                $total = $this->order('total');
            }
        }

        $card = new CreditCard([
            'name' => substr(($this->order('shipping_first_name') ? $this->order('shipping_first_name') . ' ' . $this->order('shipping_last_name') : $this->order('first_name') . ' ' . $this->order('last_name')), 0, 31),
            'shippingAddress1' => substr(($this->order('shipping_address') ? $this->order('shipping_address') : $this->order('address')), 0, 99),
            'shippingAddress2' => substr(($this->order('shipping_address2') ? $this->order('shipping_address2') : $this->order('address2')), 0, 99),
            'shippingCity' => substr(($this->order('shipping_city') ? $this->order('shipping_city') : $this->order('city')), 0, 40),
            'shippingPostcode' => ($this->order('shipping_zip') ? $this->order('shipping_zip') : $this->order('zip')),
            'shippingCountry' => alpha2_country_code(($this->order('shipping_country_code') ? $this->order('shipping_country_code') : $this->order('country_code'))),
            'shippingState' => ($this->order('shipping_state') ? strtoupper($this->order('shipping_state')) : strtoupper($this->order('state'))),
            'email' => $this->order('email_address'),
            'phone' => $this->order('phone'),
        ]);

        $post_array = [
            'amount' => round($total, 2),
            'itemAmount' => round($this->order('subtotal'), 2),
            'taxAmount' => round($this->order('tax'), 2),
            'shippingAmount' => round($this->order('shipping'), 2),
            'card' => $card,
            'returnUrl' => $this->getNotifyUrl(ucfirst(get_class($this)), 'confirm_payment'),
            'cancelUrl' => ee()->config->item('base_url') . ee()->uri->uri_string(),
            'currency' => ($this->order('currency_code') ? $this->order('currency_code') : 'USD'),
            'allowNote' => (int)filter_var($this->plugin_settings('allow_note'), FILTER_VALIDATE_BOOLEAN),
            'allowedPaymentMethod' => 'InstantPaymentOnly',
        ];

        if ($code = self::LOCALE_MAP[$this->order('language')][$this->order('country_code')] ?? null) {
            $post_array['localeCode'] = $code;
        } elseif ($map = self::LOCALE_MAP[$this->order('language')] ?? null) {
            $post_array['localeCode'] = end($map);
        } else {
            $post_array['localeCode'] = 'US';
        }

        $shippingSetting = $this->plugin_settings('shipping_settings');

        $post_array['addressOverride'] = (int)isset(self::SHIPPING_SETTING_MAP['addressOverride'][$shippingSetting]);
        $post_array['noShipping'] = (int)isset(self::SHIPPING_SETTING_MAP['noShipping'][$shippingSetting]);

        if (in_array($shippingSetting, ['hide_shipping', 'paypal_shipping'])) {
            $card->setShippingAddress1(null);
            $card->setShippingAddress2(null);
            $card->setShippingCity(null);
            $card->setShippingPostcode(null);
            $card->setShippingCountry(null);
            $card->setShippingState(null);
        }

        if ($this->plugin_settings('header_image_url')) {
            $post_array['headerImageUrl'] = $this->plugin_settings('header_image_url');
        }

        // making it so you can checkout with a CC
        if ($this->plugin_settings('solutiontype') == 'Sole') {
            $post_array['solutionType'] = 'Sole';
            $post_array['landingPage'] = ($this->plugin_settings('display_billing_page') ? $this->plugin_settings('display_billing_page') : 'Login');
        }

        $post_array['items'] = new ItemBag(array_map(function (array $item) {
            $params = [
                'name' => $item['title'],
                'quantity' => $item['quantity'],
                'price' => $item['price'],
            ];

            if ($this->plugin_settings('show_item_id') == 'yes') {
                $params['code'] = $item['entry_id'];
            }

            return new Item($params);
        }, $this->order('items')));

        if ($this->order('discount') > 0) {
            $post_array['itemAmount'] = round(($this->order('subtotal') - $this->order('discount')), 2);

            $params = [
                'name' => ee()->lang->line('discount'),
                'quantity' => 1,
                'price' => -round($this->order('discount'), 2),
            ];

            if ($this->plugin_settings('show_item_id') == 'yes') {
                $params['code'] = '000';
            }

            $post_array['items']->add(new Item($params));
        }

        // if the price is manually set, we want to kill the item totals and other values, because paypal does not like it when the item totals and the checkout total does not match.
        if (!empty($_POST['PR']) || !empty($_POST['price']) || $this->order('pp_alt_total')) {
            if ($this->total()) {
                ee()->cartthrob->cart->update_order(['pp_alt_total' => $this->total()]);
            }

            $post_array['items']->replace([]);
            unset($post_array['shippingAmount']);
            unset($post_array['taxAmount']);
            unset($post_array['itemAmount']);
        }

        if ($token) {
            $post_array['token'] = $token;
        }

        if ($payer_id) {
            $post_array['payerId'] = $payer_id;
            $post_array['ipAddress'] = $_SERVER['SERVER_NAME'];
        }

        return $post_array;
    }

    // @TODO need to add methods for handling refunds like PayPal standard.

    public function confirm_payment($post)
    {
        $state = new TransactionState();

        try {
            /** @var Response $response */
            $response = $this->omnipayGateway->fetchCheckout(['token' => $post['token']])->send();
            if ($response->isSuccessful()) {
                $post_array = $this->assemble_post_array('DoExpressCheckoutPayment', $post['token'], $response->getData()['PAYERID']);
                /** @var Response $response */
                $response = $this->omnipayGateway->completePurchase($post_array)->send();

                if ($response->isSuccessful()) {
                    $state->setAuthorized()->setTransactionId($response->getTransactionReference());
                } else {
                    if (null !== $response->getMessage()) {
                        $state->setFailed($response->getMessage() . ' ' . $response->getData()['L_ERRORCODE0']);
                    }
                }
            } else {
                if (null !== $response->getMessage()) {
                    $state->setFailed($response->getMessage() . ' ' . $response->getData()['L_ERRORCODE0']);
                }
            }
        } catch (\Exception $e) {
            $state->setFailed(ee()->lang->line('paypal_express_did_not_respond'));
        }

        $this->checkoutCompleteOffsite($state, $this->order('entry_id'));
        exit();
    }
}
