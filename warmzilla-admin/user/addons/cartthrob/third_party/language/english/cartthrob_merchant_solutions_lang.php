<?php

$lang = [
    // PAYPAL EXPRESS ERRORS & CONTENT

    'merchant_solutions_api_title' => 'MERCHANT e-SOLUTIONS',
    'merchant_solutions_api_overview' => 'MERCHANT e-SOLUTIONS payment method',
    'merchant_solutions_api_password' => 'Password',
    'merchant_solutions_public_id' => 'Public ID',
    'merchant_solutions_key' => 'Merchant Key',
    'sandbox_merchant_solutions_customer_id' => 'Customer ID',
    'merchant_solutions_payment_method' => 'Payment Method',
    'test_mode' => 'Test Mode',
    'test_response' => 'Test Response',
    'merchant_solutions_alias_form_header' => 'Form Header',
    'merchant_solutions_alias_form_footer' => 'Form Footer',
    'merchant_solutions_sandbox_mode' => 'Sandbox Mode?',
    'merchant_solutions_transparent_card_name' => 'Card Name',
    'merchant_solutions_transparent_card_number' => 'Card Number',
    'merchant_solutions_transparent_expiration' => 'Expiration Date',
    'merchant_solutions_transparent_cardcvn' => 'CardCVN',
];
