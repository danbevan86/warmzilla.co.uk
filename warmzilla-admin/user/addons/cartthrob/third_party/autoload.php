<?php

use Illuminate\Support\Str;

$dirs = ['discount_plugins', 'payment_gateways', 'price_plugins', 'shipping_plugins', 'tax_plugins'];

foreach ($dirs as $dir) {
    autoloadThirdPartyPlugins(__DIR__ . '/' . $dir);
}

// ================================================================================
// ================================================================================

function autoloadThirdPartyPlugins($dir)
{
    $files = scandir($dir);

    // Filter to only PHP files
    foreach ($files as $key => $file) {
        if (!Str::endsWith($file, '.php')) {
            unset($files[$key]);
        }
    }

    if (count($files) <= 0) {
        return;
    }

    foreach ($files as $file) {
        $fullFilePath = $dir . '/' . $file;

        if (is_dir($fullFilePath)) {
            autoloadThirdPartyPlugins($fullFilePath);
        } elseif (Str::endsWith($fullFilePath, '.php')) {
            require_once $fullFilePath;
        }
    }
}
