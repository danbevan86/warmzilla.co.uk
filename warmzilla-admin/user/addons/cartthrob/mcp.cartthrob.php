<?php

use Illuminate\Support\Collection;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property CI_Controller   $EE
 * @property Cartthrob_cart  $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_mcp
{
    public static $nav = [
        'global_settings' => [
            'general_settings' => 'nav_general_settings',
            'number_format_defaults' => 'nav_number_format_defaults',
            'default_location' => 'nav_default_location',
            'locales' => 'nav_locales',
            'set_license_number' => 'nav_set_license_number',
        ],
        'product_settings' => [
            'product_channels' => 'nav_product_channels',
            'product_options' => 'nav_product_options',
        ],
        'order_settings' => [
            'order_channel_configuration' => 'nav_order_channel_configuration',
            'purchased_items' => 'nav_purchased_items',
        ],
        'shipping' => [
            'shipping' => 'nav_shipping',
        ],
        'taxes' => [
            'tax' => 'nav_tax',
        ],
        'coupons_discounts' => [
            'coupon_options' => 'nav_coupon_options',
            'discount_options' => 'nav_discount_options',
        ],
        'notifications' => [
            'notifications' => 'notifications',
        ],
        'members' => [
            'member_configuration' => 'nav_member_configuration',
        ],
        'payment_gateways' => [
            'payment_gateways' => 'nav_payment_gateways',
            'payment_security' => 'nav_payment_security',
        ],
        'reports' => [
            'reports' => 'reports',
        ],
        'installation' => [
            'install_channels' => 'nav_install_channels',
            'template_variables' => 'nav_template_variables',
        ],
        'import_export' => [
            'import_settings' => 'nav_import_settings',
            'export_settings' => 'nav_export_settings',
        ],
        'beta' => [
            'beta' => 'nav_beta',
        ],
        'add_tax' => [
            'add_tax' => '',
        ],
        'edit_tax' => [
            'edit_tax' => '',
        ],
        'delete_tax' => [
            'delete_tax' => '',
        ],
        'update_skus_action' => [
            'update_skus_action' => '',
        ],
    ];
    public static $subnav = [];
    public static $no_nav = [
        'edit_tax',
        'add_tax',
        'delete_tax',
        'update_skus_action',
    ];
    public $required_settings = [];
    public $template_errors = [];
    public $templates_installed = [];
    public $extension_enabled = 0;
    public $module_enabled = 0;
    public $version;
    public $no_form = [
        'support',
        'import_export',
        'reports',
        'add_tax',
        'taxes',
        'edit_tax',
        'delete_tax',
        'subscriptions_list',
        'subscriptions_vaults',
        'subscriptions_permissions',
        'update_skus_action',
    ];
    private $module_name;
    private $initialized = false;
    private $remove_keys = [
        'name',
        'submit',
        'x',
        'y',
        'templates',
        'XID',
        'CSRF_TOKEN',
    ];

    public function __construct()
    {
        $this->module_name = strtolower(str_replace(['_ext', '_mcp', '_upd'], '', __CLASS__));

        ee()->load->helper(['debug', 'array']);
    }

    public function update()
    {
        require_once PATH_THIRD . 'cartthrob/upd.cartthrob.php';

        $upd = new Cartthrob_upd();

        $upd->sync();

        $this->index();
    }

    public function index()
    {
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/global_settings'));
    }

    public function global_settings()
    {
        return $this->load_view(__FUNCTION__);
    }

    private function load_view($current_nav, $more = [], $structure = [])
    {
        ee()->load->library('cartthrob_payments');

        if (!ee()->config->item('encryption_key')) {
            ee()->cp->cp_page_title = ee()->lang->line('cartthrob_module_name') . ' - ' . ee()->lang->line('encryption_key');

            return ee()->load->view('encryption_key', [], true);
        }

        ee()->load->library('addons');

        $modules = ee()->addons->get_installed();

        if (!isset($modules['cartthrob']['module_version']) || version_compare($this->version(), $modules['cartthrob']['module_version'], '>')) {
            ee()->cp->cp_page_title = ee()->lang->line('cartthrob_module_name') . ' - ' . ee()->lang->line('update_required');

            return ee()->load->view('update_required', [], true);
        }

        if ($this->module_enabled('subscriptions')) {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob_subscriptions/');
        }

        $this->initialize();

        // check if we need to use the old or the new installer.xml
        if (version_compare(APP_VER, '2.6', '<')) {
            ee()->load->library('package_installer', ['xml' => PATH_THIRD . 'cartthrob/installer/installer_legacy.xml']);
        } else {
            ee()->load->library('package_installer', ['xml' => PATH_THIRD . 'cartthrob/installer/installer.xml']);
        }

        ee()->cp->cp_page_title = ee()->lang->line('cartthrob_module_name') . ' - ' . ee()->lang->line('nav_' . $current_nav);

        // if it's not set to TRUE, and there's an uninintialized setting (since the default settings haven't been loaded) we're screwed
        $settings = $this->get_saved_settings(true);

        if ($this->module_enabled('subscriptions')) {
            if (!isset($settings['purchased_items_sub_id_field'])) {
                $settings['purchased_items_sub_id_field'] = null;
            }
        } elseif (isset($settings['purchased_items_sub_id_field'])) {
            unset($settings['purchased_items_sub_id_field']);
        }

        $site_id = null;
        if (ee()->config->item('cartthrob:msm_show_all')) {
            $site_id = 'all';
        }

        $channels = ee()->channel_model->get_channels($site_id)->result_array();
        $fields = [];
        $statuses = [];
        $view_paths = [];
        $status_titles = [];
        $channel_titles = [];
        $settings_views = [];

        foreach ($channels as $channel) {
            $channel_titles[$channel['channel_id']] = $channel['channel_title'];

            // $fields[$channel['channel_id']] = ee()->field_model->get_fields($channel['field_group'])->result_array();
            // only want to capture a subset of data, because we're using this for JSON and we were getting too much data previously
            $channel_fields = ee()->field_model->get_fields()->result_array();

            foreach ($channel_fields as $key => &$data) {
                // This is 5.2 only... sigh this will eventually replace the 3 lines below.
                // $fields[$channel['channel_id']][$key] = array_intersect_key($data, array_fill_keys(array('field_id', 'site_id', 'group_id', 'field_name', 'field_type', 'field_label'), TRUE));
                $array_fill_keys = ['field_id', 'site_id', 'group_id', 'field_name', 'field_type', 'field_label'];
                $combined = array_combine($array_fill_keys, array_fill(0, count($array_fill_keys), true));

                $fields[$channel['channel_id']][$key] = array_intersect_key($data, $combined);
            }

            $statuses[$channel['channel_id']] = ee()->cartthrob_settings_model->get_status_channels($channel['channel_id']);
        }

        foreach ($statuses as $status) {
            foreach ($status as $item) {
                $status_titles[$item['status']] = $item['status'];
            }
        }

        if (!empty($settings['product_channels'])) {
            foreach ($settings['product_channels'] as $i => $channel_id) {
                if (!isset($channel_titles[$channel_id])) {
                    unset($settings['product_channels'][$i]);
                }
            }
        }

        if (!empty($settings['product_channel_fields'])) {
            foreach ($settings['product_channel_fields'] as $channel_id => $values) {
                if (!isset($channel_titles[$channel_id])) {
                    unset($settings['product_channel_fields'][$channel_id]);
                }
            }
        }

        $nav = self::nav();
        $no_nav = self::$no_nav;

        // -------------------------------------------
        // 'cartthrob_add_settings_nav' hook.
        //
        if (ee()->extensions->active_hook('cartthrob_add_settings_nav') === true) {
            if ($addl_nav = ee()->extensions->call('cartthrob_add_settings_nav', $nav)) {
                $nav = array_merge($nav, $addl_nav);
            }
        }

        // -------------------------------------------
        // 'cartthrob_add_settings_views' hook.
        //
        if (ee()->extensions->active_hook('cartthrob_add_settings_views') === true) {
            $settings_views = ee()->extensions->call('cartthrob_add_settings_views', $settings_views);
        }

        if (is_array($settings_views) && count($settings_views)) {
            foreach ($settings_views as $key => $value) {
                if (is_array($value)) {
                    if (isset($value['path'])) {
                        $view_paths[$key] = $value['path'];
                    }

                    if (isset($value['title'])) {
                        $nav['more_settings'][$key] = $value['title'];
                    }
                } else {
                    $nav['more_settings'][$key] = $value;
                }
            }
        }

        $sections = [];

        foreach ($nav as $top_nav => $_nav) {
            if ($top_nav != $current_nav) {
                continue;
            }

            foreach ($_nav as $url_title => $section) {
                if (!preg_match('/^http/', $url_title)) {
                    $sections[] = $url_title;
                }
            }
        }

        $member_fields = ['' => '----'];

        if (ee()->cartthrob->store->config('use_profile_edit') && isset(ee()->extensions->extensions['channel_form_submit_entry_start'][10]['Profile_ext'])) {
            ee()->load->add_package_path(PATH_THIRD . 'profile/');

            ee()->load->model('profile_model');

            ee()->load->remove_package_path(PATH_THIRD . 'profile/');

            if ($profile_edit_channel_id = ee()->profile_model->settings('channel_id')) {
                // profile might be on a different MSM site, therefore it might not already be in the $fields array
                // let's double check
                if (isset($fields[$profile_edit_channel_id])) {
                    $profile_fields = $fields[$profile_edit_channel_id];
                } else {
                    $site_id = ee()->profile_model->site_id();

                    if ($site_id != ee()->config->item('site_id')) {
                        ee()->cartthrob_field_model->load_fields($site_id);
                    }

                    $profile_fields = ee()->cartthrob_field_model->get_fields_by_channel($profile_edit_channel_id);
                }

                foreach ($profile_fields as $field) {
                    $member_fields[$field['field_id']] = $field['field_label'];
                }
            }
        } else {
            $m_fields = ee('Model')->get('MemberField')
                ->fields('m_field_name', 'm_field_id', 'm_field_label')
                ->all();
            foreach ($m_fields as $row) {
                $member_fields[$row->m_field_id] = $row->m_field_label;
            }
        }

        $member_groups = [];
        $query = ee('Model')->get('MemberGroup')->filter('group_id', '>=', '5')->all();

        foreach ($query as $row) {
            $member_groups[$row->group_id] = $row->group_title;
        }

        unset($query);

        if (!version_compare(APP_VER, '2.2', '<')) {
            foreach ($view_paths as $path) {
                ee()->load->add_package_path($path);
            }
        }

        ee()->load->library('paths');

        $data = [
            'structure' => $structure,
            'nav' => $nav,
            'subnav' => $this->has_subnav($current_nav),
            'current_nav' => $current_nav,
            'sections' => $sections,
            'channels' => $channels,
            'channel_titles' => $channel_titles,
            'fields' => $fields,
            'statuses' => $statuses,
            'status_titles' => $status_titles,
            'templates' => $this->get_templates(),
            'payment_gateways' => $this->get_payment_gateways(),
            'shipping_plugins' => $this->shippingPlugins(),
            'tax_plugins' => $this->get_tax_plugins(),
            'install_channels' => [],
            'install_template_groups' => [],
            'install_member_groups' => [],
            'view_paths' => $view_paths,
            'cartthrob_mcp' => $this,
            'input' => ee()->input,
            'load' => ee()->load,
            'session' => ee()->session,
            'form_open' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/quick_save', ['return' => ee()->uri->segment(5)])),
            'extension_enabled' => $this->extension_enabled,
            'module_enabled' => $this->module_enabled,
            'settings' => $settings,
            'orders_status' => $settings['orders_status'],
            'states_and_countries' => array_merge(
                ['global' => 'Global', '' => '---'],
                ee()->locales->states(),
                ['0' => '---'],
                ee()->locales->all_countries()
            ),
            'states' => ee()->locales->states(),
            'countries' => ee()->locales->all_countries(),
            'no_form' => (in_array($current_nav, $this->no_form)),
            'no_nav' => $no_nav,
            'member_fields' => $member_fields,
            'member_groups' => $member_groups,
            'customer_data_fields' => [
                'first_name',
                'last_name',
                'address',
                'address2',
                'city',
                'state',
                'zip',
                'country',
                'country_code',
                'company',
                'phone',
                'email_address',
                'use_billing_info',
                'shipping_first_name',
                'shipping_last_name',
                'shipping_address',
                'shipping_address2',
                'shipping_city',
                'shipping_state',
                'shipping_zip',
                'shipping_country',
                'shipping_country_code',
                'shipping_company',
                'language',
                'shipping_option',
                'region',
            ],
        ];

        foreach (ee()->package_installer->packages() as $index => $template) {
            switch ($template->getName()) {
                case 'channel':
                    $data['install_channels'][$index] = $template->attributes()->channel_title;
                    break;
                case 'template_group':
                    $data['install_template_groups'][$index] = $template->attributes()->group_name;
                    break;
                case 'member_group':
                    $data['install_member_groups'][$index] = $template->attributes()->group_name;
                    break;
            }
        }

        if (!empty($structure)) {
            $data['html'] = ee()->load->view('settings_template', $data, true);
        }

        $data = array_merge($data, $more);
        $self = $data;
        $data['data'] = $self;

        unset($self);

        ee()->cp->add_js_script('ui', 'accordion');

        if (version_compare(APP_VER, '2.2', '<')) {
            ee()->cp->add_to_head('<link href="' . URL_THIRD_THEMES . 'cartthrob/css/cartthrob.css" rel="stylesheet" type="text/css" />');
            ee()->cp->add_to_foot(ee()->load->view('settings_form_head', $data, true));

            $output = ee()->load->view('settings_form', $data, true);
        } else {
            ee()->cp->add_to_head('<link href="' . URL_THIRD_THEMES . 'cartthrob/css/cartthrob.css" rel="stylesheet" type="text/css" />');
            ee()->cp->add_to_foot(ee()->load->view('settings_form_head', $data, true));

            $output = ee()->load->view('settings_form', $data, true);

            foreach ($view_paths as $path) {
                ee()->load->remove_package_path($path);
            }
        }

        return $output;
    }

    public function version()
    {
        if (is_null($this->version)) {
            $this->version = CARTTHROB_VERSION;
        }

        return $this->version;
    }

    private function initialize()
    {
        if ($this->initialized) {
            return;
        }

        $this->initialized = true;

        ee()->load->model('cartthrob_settings_model');
        ee()->load->model(['field_model', 'channel_model', 'product_model']);

        ee()->load->library('locales');
        ee()->load->library('languages');
        ee()->load->library('cartthrob_payments');
        ee()->load->library('mbr_addon_builder');

        ee()->load->helper(['security', 'countries', 'data_formatting', 'form', 'file', 'string', 'inflector']);

        ee()->lang->loadfile('cartthrob', 'cartthrob');
        ee()->lang->loadfile('cartthrob_errors', 'cartthrob');

        $this->module_enabled = (bool)ee()->db->where('module_name', 'Cartthrob')->count_all_results('modules');
        $this->extension_enabled = (bool)ee()->db->where([
            'class' => 'Cartthrob_ext',
            'enabled' => 'y',
        ])->count_all_results('extensions');

        ee()->mbr_addon_builder->initialize(['module_name' => $this->module_name]);
    }

    public function get_saved_settings($get_all_settings = false)
    {
        $settings = [];

        foreach (ee()->db->where('site_id',
            ee()->config->item('site_id'))->get('cartthrob_settings')->result() as $row) {
            if ($row->serialized) {
                $settings[$row->key] = @unserialize($row->value);
            } else {
                $settings[$row->key] = $row->value;
            }
        }

        if ($get_all_settings) {
            $settings = array_merge($this->get_settings(), $settings);
        }

        return $settings;
    }

    /**
     * Loads cart, and gets default settings, then gets saved settings
     *
     * @param null
     * @return array $settings
     */
    public function get_settings()
    {
        $this->initialize();

        return ee()->cartthrob_settings_model->get_settings();
    }

    public static function nav()
    {
        static $built = false;

        if ($built === true) {
            return self::$nav;
        }

        $built = true;

        if (self::module_enabled('subscriptions')) {
            self::$nav['subscriptions_settings'] = ['subscriptions_settings' => ''];
        }

        $modules = [
            'cartthrob_order_manager' => 'om_sales_dashboard',
            'ct_admin' => 'order_admin',
            'cartthrob_item_options' => 'global_item_options',
        ];

        foreach ($modules as $key => $module) {
            if (self::module_enabled($key, null)) {
                self::$nav['modules'][$key] = [$key => $modules[$key]];
            }
        }

        return self::$nav;
    }

    /**
     * module_enabled
     *
     * use this to check if one or more modules are installed. If any one of the modules are not installed, false will be returned.
     *
     * @param string|array $modules list of modules to check
     * @param string $prefix . If not set, Cartthrob_ will be used.
     * @return bool
     */
    public static function module_enabled($modules, $prefix = 'Cartthrob_')
    {
        $classes = [];
        if (!is_array($modules)) {
            $query = ee()->db->select('module_name')
                ->where_in('module_name', $prefix . $modules)
                ->get('modules');

            if ($query->result()) {
                $query->free_result();

                return true;
            }
        } else {
            foreach ($modules as $module) {
                $query = ee()->db->select('module_name')
                    ->where_in('module_name', $prefix . $module)
                    ->get('modules');

                if (!$query->result()) {
                    return false;
                }
                $query->free_result();
            }

            return true;
        }

        return false;
    }

    public function has_subnav($which)
    {
        foreach (self::$subnav as $subnav) {
            if (in_array($which, $subnav)) {
                return $subnav;
            }
        }

        return false;
    }

    /**
     * Loads payment gateway files
     *
     * @param null
     * @return array $gateways Array containing settings and information about the gateway
     */
    public function get_payment_gateways()
    {
        $this->initialize();

        ee()->load->helper('file');
        ee()->load->library('api/api_cartthrob_payment_gateways');

        $templates = ['' => ee()->lang->line('gateways_default_template')];

        ee()->load->model('template_model');

        $query = ee()->template_model->get_templates();

        foreach ($query->result_array() as $row) {
            $templates[$row['group_name'] . '/' . $row['template_name']] = $row['group_name'] . '/' . $row['template_name'];
        }

        $gateways = ee()->api_cartthrob_payment_gateways->gateways();

        foreach ($gateways as &$plugin_data) {
            ee()->lang->loadfile(strtolower($plugin_data['classname']), 'cartthrob', false);

            foreach (['title', 'overview'] as $key) {
                if (isset($plugin_data[$key])) {
                    $plugin_data[$key] = ee()->lang->line($plugin_data[$key]);
                }
            }

            $plugin_data['html'] = ee()->api_cartthrob_payment_gateways->set_gateway($plugin_data['classname'])->gateway_fields(true);

            if (isset($plugin_data['settings']) && is_array($plugin_data['settings'])) {
                foreach ($plugin_data['settings'] as $key => $setting) {
                    $plugin_data['settings'][$key]['name'] = ee()->lang->line($setting['name']);
                }

                $plugin_data['settings'][] = [
                    'name' => ee()->lang->line('template_settings_name'),
                    'note' => ee()->lang->line('template_settings_note'),
                    'type' => 'select',
                    'short_name' => 'gateway_fields_template',
                    'options' => $templates,
                ];
            }
        }

        ee()->load->library('data_filter');

        ee()->data_filter->sort($gateways, 'title');

        return $gateways;
    }

    /**
     * @return array
     */
    public function shippingPlugins()
    {
        return (new Collection($this->get_plugins('shipping')))
            ->map(function ($plugin) {
                $plugin['fulltext_title'] = lang($plugin['title']);

                return $plugin;
            })
            ->sortBy('fulltext_title')
            ->toArray();
    }

    /**
     * Loads shipping plugin files
     *
     * @param null
     * @return array $plugins Array containing settings and information about the plugin
     */
    public function get_plugins($type)
    {
        $this->initialize();

        ee()->load->helper(['file', 'data_formatting']);

        $plugins = [];

        $paths[] = CARTTHROB_PATH . 'plugins/' . $type . '/';

        if (ee()->config->item('cartthrob_third_party_path')) {
            $paths[] = rtrim(ee()->config->item('cartthrob_third_party_path'), '/') . '/' . $type . '_plugins/';
        } else {
            $paths[] = PATH_THIRD . 'cartthrob/third_party/' . $type . '_plugins/';
        }

        foreach ($paths as $path) {
            if (!is_dir($path)) {
                continue;
            }

            foreach (get_filenames($path, true) as $file) {
                if (!preg_match('/^Cartthrob_/', basename($file, '.php'))) {
                    continue;
                }

                $class = basename($file, '.php');
                $language = set(ee()->session->userdata('language'), ee()->input->cookie('language'), ee()->config->item('deft_lang'), 'english');

                if (file_exists(PATH_THIRD . 'cartthrob/language/' . $language . '/' . strtolower($class) . '_lang.php')) {
                    ee()->lang->loadfile(strtolower($class), 'cartthrob', false);
                } else {
                    if (file_exists($path . '../language/' . $language . '/' . strtolower($class) . '_lang.php')) {
                        ee()->lang->load(strtolower($class), $language, false, true, $path . '../', false);
                    }
                }

                $plugin_info = get_class_vars($class);

                $plugin_info['classname'] = $class;

                $settings = $this->get_settings();

                if (isset($plugin_info['settings']) && is_array($plugin_info['settings'])) {
                    foreach ($plugin_info['settings'] as $key => $setting) {
                        // retrieve the current set value of the field
                        $current_value = (isset($settings[$class . '_settings'][$setting['short_name']])) ? $settings[$class . '_settings'][$setting['short_name']] : false;
                        // set the value to the default value if there is no set value and the default value is defined
                        $current_value = ($current_value === false && isset($setting['default'])) ? $setting['default'] : $current_value;

                        if ($setting['type'] == 'matrix') {
                            if (!is_array($current_value) || !count($current_value)) {
                                $current_values = [[]];

                                foreach ($setting['settings'] as $matrix_setting) {
                                    $current_values[0][$matrix_setting['short_name']] = isset($matrix_setting['default']) ? $matrix_setting['default'] : '';
                                }
                            } else {
                                $current_values = $current_value;
                            }
                        }
                    }
                }

                $plugins[] = $plugin_info;
            }
        }

        return $plugins;
    }

    // @TODO

    public function get_tax_plugins()
    {
        return $this->get_plugins('tax');
    }

    public function get_templates()
    {
        static $templates;

        if (is_null($templates)) {
            $templates = [];

            ee()->load->model('template_model');

            $query = ee()->template_model->get_templates();

            foreach ($query->result() as $row) {
                $templates[$row->group_name . '/' . $row->template_name] = $row->group_name . '/' . $row->template_name;
            }
        }

        return $templates;
    }

    public function order_admin()
    {
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/ct_admin'));
    }

    public function global_item_options()
    {
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob_item_options'));
    }

    public function product_settings()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function order_settings()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function shipping()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function coupons_discounts()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function email_notifications()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function subscriptions_settings()
    {
        ee()->load->library('cartthrob_payments');

        if (!$this->module_enabled('subscriptions')) {
            return show_error('subscriptions_not_enabled'); // @TODO LANG
        }

        ee()->load->library('api/api_cartthrob_payment_gateways');

        $vars = [];

        [];

        ee()->load->library('paths');

        $vars['crontabulous_url'] = htmlentities(ee()->paths->build_action_url('Cartthrob_mcp',
            'crontabulous_get_pending_subscriptions'), ENT_QUOTES, 'UTF-8', false);

        $vars['gateways'] = ee()->api_cartthrob_payment_gateways->subscription_gateways();

        ee()->cp->add_js_script([
            'ui' => ['core', 'widget', 'progressbar'],
        ]);

        return $this->load_view(__FUNCTION__, $vars, []);
    }

    /**
     * "Edit"-style list of subscriptions
     *
     * @return string the view
     */
    public function subscriptions_list()
    {
        return $this->load_view(__FUNCTION__);
    }

    // --------------------------------
    //  Plugin Settings
    // --------------------------------

    /**
     * "Edit"-style list of vaults
     *
     * @return string the view
     */
    public function subscriptions_vaults()
    {
        return $this->load_view(__FUNCTION__);
    }

    /**
     * "Edit"-style list of permissions
     *
     * @return string the view
     */
    public function subscriptions_permissions()
    {
        return $this->load_view(__FUNCTION__);
    }

    public function notifications()
    {
        ee()->load->model(['field_model', 'channel_model', 'product_model']);

        $channels = ee()->channel_model->get_channels()->result_array();

        $external_app_events = [];
        foreach (ee()->db->get('cartthrob_notification_events')->result() as $row) {
            $external_app_events[$row->application . '_' . $row->notification_event] = lang($row->application) . ': ' . lang($row->notification_event);
        }
        if (!empty($external_app_events)) {
            $email_events = [
                'payment_triggers' => [
                    'completed' => 'ct_completed',
                    'declined' => 'ct_declined',
                    'failed' => 'ct_failed',
                    'offsite' => 'ct_offsite',
                    'processing' => 'ct_processing',
                    'refunded' => 'ct_refunded',
                    'expired' => 'ct_expired',
                    'canceled' => 'ct_canceled',
                    'pending' => 'ct_pending',
                ],
                // why is status change set to blank?
                'other_events' => [
                    'low_stock' => 'ct_low_stock',
                    '' => 'status_change',
                ],
                'application_events' => $external_app_events,
            ];
        } else {
            $email_events = [
                'payment_triggers' => [
                    'completed' => 'ct_completed',
                    'declined' => 'ct_declined',
                    'failed' => 'ct_failed',
                    'offsite' => 'ct_offsite',
                    'processing' => 'ct_processing',
                    'refunded' => 'ct_refunded',
                    'expired' => 'ct_expired',
                    'canceled' => 'ct_canceled',
                    'pending' => 'ct_pending',
                ],
                // why is status change set to blank?
                'other_events' => [
                    'low_stock' => 'ct_low_stock',
                    '' => 'status_change',
                ],
            ];
        }
        $structure['class'] = 'notifications';
        $structure['stacked'] = true;
        $structure['description'] = '';
        $structure['caption'] = '';
        $structure['title'] = 'notifications';

        $structure['settings'] = [
            [
                'name' => 'log_email',
                'note' => 'log_email_note',
                'short_name' => 'log_email',
                'default' => 'no',
                'type' => 'select',
                'options' => [
                    'no' => 'no',
                    'log_only' => 'log_only',
                    'log_and_send' => 'log_and_send',
                ],
            ],
            [
                'name' => 'notifications',
                'short_name' => 'notifications',
                'type' => 'matrix',
                'settings' => [
                    [
                        'name' => 'email_subject',
                        'short_name' => 'email_subject',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'email_from_name',
                        'short_name' => 'email_from_name',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'email_from',
                        'short_name' => 'email_from',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'email_reply_to_name',
                        'note' => 'email_reply_to_note',
                        'short_name' => 'email_reply_to_name',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'email_reply_to',
                        'short_name' => 'email_reply_to',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'email_to',
                        'short_name' => 'email_to',
                        'type' => 'text',
                        'default' => '{customer_email}',
                    ],
                    [
                        'name' => 'email_template',
                        'short_name' => 'email_template',
                        'type' => 'select',
                        'attributes' => [
                            'class' => 'templates',
                        ],
                    ],
                    [
                        'name' => 'cartthrob_initiated_event',
                        'short_name' => 'email_event',
                        'type' => 'select',
                        'default' => '',
                        'options' => $email_events,
                    ],
                    [
                        'name' => 'starting_status',
                        'short_name' => 'status_start',
                        'type' => 'select',
                        'default' => '---',
                        'attributes' => [
                            'class' => 'statuses_blank',
                        ],
                    ],
                    [
                        'name' => 'ending_status',
                        'short_name' => 'status_end',
                        'type' => 'select',
                        'default' => '---',
                        'attributes' => [
                            'class' => 'statuses_blank',
                        ],
                    ],
                    [
                        'name' => 'email_type',
                        'short_name' => 'email_type',
                        'type' => 'select',
                        'default' => 'html',
                        'options' => [
                            'html' => 'send_html_email',
                            'text' => 'send_text_email',
                        ],
                    ],
                ],
            ],
        ];

        return $this->load_view(__FUNCTION__, [], $structure);
    }

    public function payment_gateways()
    {
        ee()->load->library('javascript');

        $extload_url = ee()->config->slash_item('theme_folder_url') . 'user/cartthrob/lib/extload.php?cp_test=1';

        ee()->javascript->output('
			$.ajax({
				url: ' . json_encode($extload_url) . ',
				dataType: "text",
				success: function(data) {
					$(".check-extload").show();
					if (data !== "Success") {
						$(".check-extload .alert.issue").show();
						$(".check-extload .request-info").text(data);
					} else {
						$(".check-extload .alert.success").show();
					}
				},
				error: function(xhr) {
					$(".check-extload").show();
					$(".check-extload .alert.issue").show();
					$(".check-extload .request-info").text(xhr.status + " " + xhr.statusText);
				},
			});

			// prevent these events from bubbling up, causing all checkboxes to get checked
			$("table.mainTable").find(".checkboxes").on("click", function(e) {
				e.stopPropagation();
			}).find("input[type=checkbox]").on("change", function(e) {
				e.stopPropagation();
			});
		');

        return $this->load_view(__FUNCTION__, ['extload_url' => $extload_url]);
    }

    // --------------------------------
    //  Save Settings
    // --------------------------------

    public function members()
    {
        $profile_edit_active = false;
        if (isset(ee()->extensions->extensions['channel_form_submit_entry_start'][10]['Profile_ext'])) {
            $profile_edit_active = true;
        }

        return $this->load_view(
            __FUNCTION__,
            [
                'profile_edit_active' => $profile_edit_active,
            ]
        );
    }

    public function import_export()
    {
        return $this->load_view(
            __FUNCTION__,
            ['form_open' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/import_settings'), ['enctype' => 'multipart/form-data'])]
        );
    }

    public function beta()
    {
        return $this->load_view(
            __FUNCTION__,
            ['form_edit' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/form_update_beta'))]
        );
    }

    public function installation()
    {
        return $this->load_view(
            __FUNCTION__,
            [
                'form_open' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/install_templates')),
                'template_errors' => (ee()->session->flashdata('template_errors')) ? ee()->session->flashdata('template_errors') : [],
                'templates_installed' => (ee()->session->flashdata('templates_installed')) ? ee()->session->flashdata('templates_installed') : [],
                'theme_errors' => (ee()->session->flashdata('theme_errors')) ? ee()->session->flashdata('theme_errors') : [],
                'themes_installed' => (ee()->session->flashdata('themes_installed')) ? ee()->session->flashdata('themes_installed') : [],
                'themes' => $this->get_themes(),
            ]
        );
    }

    private function get_themes()
    {
        if (ee()->config->item('cartthrob_third_party_path')) {
            $theme_base_path = ee()->config->slash_item('cartthrob_third_party_path') . 'installer/';
        } else {
            $theme_base_path = PATH_THIRD . 'cartthrob/third_party/installer/';
        }

        ee()->load->helper('directory');

        $themes = [];

        if ($map = directory_map($theme_base_path, 1)) {
            foreach ($map as $theme) {
                $theme_path = $theme_base_path . $theme;

                if (@is_file($theme_path) || !@is_dir($theme_path) || !@file_exists($theme_path . '/installer.xml') || !@is_dir($theme_path . '/templates')) {
                    continue;
                }

                $themes[$theme] = $theme;
            }
        }

        return $themes;
    }

    public function configurator_ajax()
    {
        if (!AJAX_REQUEST) {
            $debug = false; // change this as needed. should always be off in production
        } else {
            $debug = false; // don't change this value.
        }

        if ($debug) {
            if (!empty($_POST)) {
                @session_start();
                $_SESSION['post_data'] = $_POST;
            }
        }
        $html = null;
        if (!AJAX_REQUEST) {
            if ($debug) {
                @session_start();
                if (empty($_SESSION['post_data'])) {
                    // test data
                    $_POST = [
                        'field_id_81' => [
                                0 => [
                                        'all_values' => '',
                                        'option_value' => '1515',
                                        'option_name' => '111',
                                        'price' => '111',
                                        'option_group' => 'size',
                                        'options' => [
                                                'option' => [
                                                        2 => 'small',
                                                        290428430 => 'medium',
                                                        1034489027 => 'large',
                                                    ],
                                                'price' => [
                                                        2 => '10',
                                                        290428430 => '20',
                                                        1034489027 => '30',
                                                    ],
                                                'option_template' => '',
                                                'price_template' => '',
                                            ],
                                    ],
                                1 => [
                                        'option_group' => 'color',
                                        'options' => [
                                                'option' => [
                                                        2 => 'red',
                                                        4824268948 => 'blue',
                                                    ],
                                                'price' => [
                                                        2 => '',
                                                        4824268948 => '',
                                                    ],
                                                'option_template' => '',
                                                'price_template' => '',
                                            ],
                                    ],
                            ],
                        'opt_field_name' => '81',
                        'CSRF_TOKEN' => '{csrf_token}',
                    ];
                } else {
                    $_POST = $_SESSION['post_data'];
                }
            }
        }

        ee()->load->helper(['data_formatting_helper', 'html', 'array', 'form', 'array']);

        $field_id = element('opt_field_name', $_POST);
        $field_id_name = 'field_id_' . $field_id;
        $p_opt = element($field_id_name, $_POST, []);
        $html = null;
        $options = [];
        $prices = [];
        $saved_all_values = [];
        $saved_option_value = [];
        $saved_option_label = [];
        $saved_price = [];
        $saved_inventory = [];

        foreach ($p_opt as $key => $value) {
            if (element('options', $value)) {
                $o = element('option', $value['options']);
                if ($o) {
                    foreach ($o as $k => $v) {
                        $price = element($k, $value['options']['price']);
                        if (!$price) {
                            $price = 0;
                        }
                        $options[element('option_group', $value)][] = $v;
                        $prices[element('option_group', $value)][] = $price;
                    }
                }
            }

            if (array_key_exists('option_value', $value)) {
                $price = element('price', $value);
                if (!$price) {
                    $price = 0;
                }
                $saved_all_values[] = element('all_values', $value);
                $saved_option_value[] = element('option_value', $value);
                $saved_option_label[] = element('option_name', $value);
                $saved_price[] = $price;
                $saved_inventory[] = element('inventory', $value);
            }
        }
        $final_options = [];
        $final_prices = [];
        $final_options = cartesian($options);
        $final_prices = cartesian($prices);
        $prices = cartesian_to_price($final_prices);

        $all_values = [];
        $option_value = [];
        $option_label = [];
        $price = [];
        $inventory = [];

        foreach ($final_options as $k => $v) {
            $cost = element($k, $prices);
            if (!$cost) {
                $cost = 0;
            }
            $all_values[$k] = base64_encode(serialize($v));
            $option_value[$k] = ''; // implode("-",$v); // sku
            $option_label[$k] = ''; // ucwords(str_replace("_" , " ", implode(", ",$v))); // name
            $price[$k] = $cost;
            $inventory[$k] = '';
        }
        if (count($saved_all_values) && !empty($saved_all_values) && is_array($saved_all_values)) {
            $copy = $final_options;

            foreach ($saved_all_values as $key => $value) {
                $opt = @unserialize(base64_decode($value));

                if (is_array($copy) && is_array($opt)) {
                    foreach ($copy as $k => $v) {
                        $temp_arr = array_intersect_assoc($opt, $v);

                        if (count($temp_arr) == count($opt)) {
                            $all_values[$k] = base64_encode(serialize($v));
                            $option_value[$k] = element($key, $saved_option_value);
                            $option_label[$k] = element($key, $saved_option_label);
                            // $price[$k] = element($key, $saved_price);
                            $inventory[$k] = element($key, $saved_inventory);
                            unset($copy[$k]);
                        }
                    }
                }
            }
        }

        $data = [
            'all_values' => $all_values,
            'option_value' => $option_value,
            'option_label' => $option_label,
            'price' => $price,
            'inventory' => $inventory,
            'options' => $final_options,
            'field_id' => $field_id,
            'field_id_name' => $field_id_name,
            'show_inventory' => element('show_inventory', $_POST, 0),
        ];

        $html = null;
        $html .= ee()->load->view('configurator', $data, true);

        if (!$html) {
            $html = 'could not be loaded';
        }

        @ob_start();
        var_dump($data);
        $output = @ob_get_clean();

        if ($debug) {
            $html .= $output;
            echo '<pre>';
            var_export($output);
            echo '</pre>';
            exit;
        } else {
            ee()->output->send_ajax_response([
                'success' => $html,
                'CSRF_TOKEN' => ee()->functions->add_form_security_hash('{csrf_token}'),
            ]);
        }
    }

    public function reports()
    {
        if (ee()->input->get('save')) {
            $reports_settings = ee()->input->post('reports_settings');

            if (is_array($reports_settings) && isset($reports_settings['reports'])) {
                $_POST = ['reports' => $reports_settings['reports']];
            } else {
                $_POST = ['reports' => []];
            }

            $_GET['return'] = 'reports';

            return $this->quick_save();
        }

        $this->initialize();

        if (ee()->input->get('entry_id')) {
            ee()->functions->redirect(ee('CP/URL')->make('publish/edit/entry/' . ee()->input->get('entry_id')));
        }

        ee()->load->library('reports');

        ee()->load->library('number');

        if (ee()->input->get_post('report')) {
            ee()->load->library('template_helper');

            ee()->template_helper->reset([
                'base_url' => ee('CP/URL')->make('addons/settings/cartthrob/reports', ['report' => '']),
                'template_key' => 'report',
            ]);

            $data['view'] = ee()->template_helper->cp_render();
        } // default view
        else {
            if (ee()->input->get('year')) {
                if (ee()->input->get('month')) {
                    if (ee()->input->get('day')) {
                        $name = date('D d', mktime(0, 0, 0, ee()->input->get('month'), ee()->input->get('day'),
                            ee()->input->get('year')));

                        $rows = ee()->reports->get_daily_totals(ee()->input->get('day'), ee()->input->get('month'),
                            ee()->input->get('year'));

                        $overview = lang('narrow_by_order');
                    } else {
                        $name = date('F Y', mktime(0, 0, 0, ee()->input->get('month'), 1, ee()->input->get('year')));

                        $rows = ee()->reports->get_monthly_totals(ee()->input->get('month'), ee()->input->get('year'));

                        $overview = lang('narrow_by_day');
                    }
                } else {
                    $name = ee()->input->get('year');

                    $rows = ee()->reports->get_yearly_totals(ee()->input->get('year'));

                    $overview = lang('narrow_by_month');
                }
            } else {
                $name = ee()->lang->line('reports_order_totals_to_date');

                $rows = ee()->reports->get_all_totals();

                $overview = lang('narrow_by_month');
            }

            if ($rows) {
                ee()->javascript->output('cartthrobChart(' . json_encode($rows) . ', "' . $name . '");');
            }

            $data['view'] = ee()->load->view('reports_home', ['overview' => $overview], true);
        }

        ee()->load->library('table');

        ee()->table->clear();

        ee()->table->set_template(['table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="mainTable padTable">']);

        $data['order_totals'] = ee()->table->generate([
            [lang('order_totals'), lang('amount')],
            [lang('today_sales'), ee()->number->format(ee()->reports->get_current_day_total())],
            [lang('month_sales'), ee()->number->format(ee()->reports->get_current_month_total())],
            [lang('year_sales'), ee()->number->format(ee()->reports->get_current_year_total())],
        ]);

        $data['current_report'] = ee()->input->get_post('report');

        $data['reports'] = [
            '' => lang('order_totals'),
        ];

        if (ee()->config->item('cartthrob:reports')) {
            foreach (ee()->config->item('cartthrob:reports') as $report) {
                $data['reports'][$report['template']] = $report['name'];
            }
        }

        $plugin_vars = [
            'cartthrob_mcp' => $this,
            'settings' => [
                'reports_settings' => [
                    'reports' => ee()->config->item('cartthrob:reports'),
                ],
            ],
            'plugin_type' => 'reports',
            'plugins' => [
                [
                    'classname' => 'reports',
                    'title' => 'reports_settings_title',
                    'overview' => 'reports_settings_overview',
                    'settings' => [
                        [
                            'name' => 'reports',
                            'short_name' => 'reports',
                            'type' => 'matrix',
                            'settings' => [
                                [
                                    'name' => 'report_name',
                                    'short_name' => 'name',
                                    'type' => 'text',
                                ],
                                [
                                    'name' => 'report_template',
                                    'short_name' => 'template',
                                    'type' => 'select',
                                    'options' => $this->get_templates(),
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $data['reports_list'] = ee()->load->view('plugin_settings', $plugin_vars, true);

        ee()->load->library('javascript');

        ee()->javascript->output('
		$(document).on("change", "select[name=report]", function(){
			$(this).parents("form").submit();
		});
		$("#reports").show();
		');

        return $this->load_view(__FUNCTION__, $data);
    }

    public function quick_save($set_success_message = true)
    {
        $this->initialize();

        $settings = $this->get_saved_settings($get_all_settings = false);

        $data = [];

        // if they change fingerprint method we have to wipe their sessions
        if (isset($_POST['session_fingerprint_method']) && isset($settings['session_fingerprint_method']) && $_POST['session_fingerprint_method'] != $settings['session_fingerprint_method']) {
            ee()->db->truncate('cartthrob_sessions');
        }

        foreach (array_keys($_POST) as $key) {
            if (!in_array($key, $this->remove_keys) && !preg_match('/^(Cartthrob_.*?_settings|product_weblogs|product_weblog_fields|default_location|tax_settings)_.*/', $key)) {
                $data[$key] = ee()->input->post($key);
            }
        }

        foreach ($data as $key => $value) {
            $where = [
                'site_id' => ee()->config->item('site_id'),
                '`key`' => $key,
            ];

            // custom key actions
            switch ($key) {
                case 'cp_menu':

                    $is_installed = (bool)ee()->db->where('class', 'Cartthrob_ext')->where('hook',
                        'cp_menu_array')->count_all_results('extensions');

                    if ($value) {
                        if (!$is_installed) {
                            ee()->db->insert('extensions', [
                                'class' => 'Cartthrob_ext',
                                'method' => 'cp_menu_array',
                                'hook' => 'cp_menu_array',
                                'settings' => '',
                                'priority' => 10,
                                'version' => $this->version(),
                                'enabled' => 'y',
                            ]);
                        }
                    } else {
                        if ($is_installed) {
                            ee()->db->where('class', 'Cartthrob_ext')->where('hook',
                                'cp_menu_array')->delete('extensions');
                        }
                    }

                    break;
            }

            if (is_array($value)) {
                $row['serialized'] = 1;
                $row['value'] = serialize($value);
            } else {
                $row['serialized'] = 0;
                $row['value'] = $value;
            }

            if (isset($settings[$key]) && ee()->db->count_all('cartthrob_settings') > 0) {
                if ($value !== $settings[$key]) {
                    ee()->db->update('cartthrob_settings', $row, $where);
                }
            } else {
                ee()->db->insert('cartthrob_settings', array_merge($row, $where));
            }
        }

        if ($set_success_message) {
            ee()->session->set_flashdata('message_success',
                sprintf('%s %s %s', lang('cartthrob_module_name'), lang('nav_' . ee()->input->get('return')),
                    lang('settings_saved')));
        }

        $return = (ee()->input->get('return')) ? '/' . ee()->input->get('return', true) : '';

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob' . $return));
    }

    // --------------------------------
    //  Validate Settings
    // --------------------------------

    /**
     * Creates setting controls
     *
     * @param string $type text|textarea|radio The type of control that is being output
     * @param string $name input name of the control option
     * @param string $current_value the current value stored for this input option
     * @param array|bool $options array of options that will be output (for radio, else ignored)
     * @return string the control's HTML
     */
    public function plugin_setting($type, $name, $current_value, $options = [], $attributes = [])
    {
        $output = '';

        if (!is_array($options)) {
            $options = [];
        } else {
            $new_options = [];
            foreach ($options as $key => $value) {
                // optgropus
                if (is_array($value)) {
                    $key = lang($key);
                    foreach ($value as $sub => $item) {
                        $new_options[$key][$sub] = lang($item);
                    }
                } else {
                    $new_options[$key] = lang($value);
                }
            }
            $options = $new_options;
        }

        if (!is_array($attributes)) {
            $attributes = [];
        }

        switch ($type) {
            case 'add_to_head':
                $output = null;

                if (strpos($current_value, '<script') !== false) {
                    ee()->cp->add_to_foot($current_value);
                } else {
                    ee()->cp->add_to_head($current_value);
                }
                break;
            case 'add_to_foot':
                $output = null;
                ee()->cp->add_to_foot($current_value);
                break;
            case 'note':
                $output = $current_value;
                break;
            case 'select':
                if (empty($options)) {
                    $attributes['value'] = $current_value;
                }
                $output = form_dropdown($name, $options, $current_value, _attributes_to_string($attributes));
                break;
            case 'multiselect':
                $output = form_multiselect($name . '[]', $options, $current_value, _attributes_to_string($attributes));
                break;
            case 'checkbox':
                if ($attributes) {
                    if (!empty($options['extra'])) {
                        $options['extra'] .= ' ' . _attributes_to_string($attributes);
                    } else {
                        $options['extra'] = _attributes_to_string($attributes);
                    }
                }
                $output = form_checkbox($name, 1, !empty($current_value),
                        isset($options['extra']) ? $options['extra'] : '')
                    . '&nbsp;' . form_label((!empty($options['label']) ? $options['label'] : ee()->lang->line('yes')),
                        $name);
                break;
            case 'text':
                $attributes['name'] = $name;
                $attributes['value'] = $current_value;
                $output = form_input($attributes);
                break;
            case 'textarea':
                $attributes['name'] = $name;
                $attributes['value'] = $current_value;
                $output = form_textarea($attributes);
                break;
            case 'radio':
                if (empty($options)) {
                    $output .= form_label(form_radio($name, 1,
                            (bool)$current_value) . '&nbsp;' . ee()->lang->line('yes'), $name,
                        ['class' => 'radio']);
                    $output .= form_label(form_radio($name, 0,
                            (bool)!$current_value) . '&nbsp;' . ee()->lang->line('no'), $name,
                        ['class' => 'radio']);
                } else {
                    // if is index array
                    if (array_values($options) === $options) {
                        foreach ($options as $option) {
                            $output .= form_label(form_radio($name, $option,
                                    ($current_value === $option)) . '&nbsp;' . $option, $name,
                                ['class' => 'radio']);
                        }
                    } // if associative array
                    else {
                        foreach ($options as $option => $option_name) {
                            $output .= form_label(form_radio($name, $option,
                                    ($current_value === $option)) . '&nbsp;' . lang($option_name), $name,
                                ['class' => 'radio']);
                        }
                    }
                }
                break;
            default:
        }

        return $output;
    }

    // --------------------------------
    //  Export Settings
    // --------------------------------

    public function email_test()
    {
        if (!AJAX_REQUEST) {
            exit;
        }

        if (REQ !== 'CP') {
            exit;
        }

        ee()->load->library('cartthrob_emails');

        $event = ee()->input->post('email_event');
        if (!$event) {
            $emails = ee()->cartthrob_emails->get_email_for_event($event = null, 'open', 'closed');
        } else {
            $emails = ee()->cartthrob_emails->get_email_for_event($event);
        }

        if (!empty($emails)) {
            $test_panel = [
                'inventory' => 5,
                'billing_address' => 'Test Avenue',
                'billing_address2' => 'Apt 1',
                'billing_city' => 'Testville',
                'billing_company' => 'Testco',
                'billing_country' => 'United States',
                'billing_country_code' => 'USA',
                'billing_first_name' => 'Testy',
                'billing_last_name' => 'Testerson',
                'billing_state' => 'MO',
                'billing_zip' => '63303',
                'customer_email' => 'test@yoursite.com',
                'customer_name' => 'Test Testerson',
                'customer_phone' => '555-555-5555',
                'discount' => '0.00',
                'entry_id' => '111',
                'group_id' => '1',
                'member_id' => '1',
                'order_id' => '111',
                'shipping' => '10',
                'shipping_plus_tax' => '10.80',
                'subtotal' => '110.00',
                'subtotal_plus_tax' => '123.45',
                'tax' => '13.45',
                'title' => '111',
                'total' => '123.45',
                'total_cart' => '123.45',
                'transaction_id' => '12345678',
            ];

            foreach ($emails as $emailDetails) {
                ee()->cartthrob_emails->send_email($emailDetails, $test_panel);
            }
        }

        // forces json output
        ee()->output->send_ajax_response(['CSRF_TOKEN' => ee()->functions->add_form_security_hash('{csrf_token}')]);
    }

    // --------------------------------
    //  GET Settings
    // --------------------------------

    public function save_price_modifier_presets_action()
    {
        if (!AJAX_REQUEST) {
            exit;
        }

        if (REQ !== 'CP' && !ee()->security->secure_forms_check(ee()->input->post('csrf_token'))) {
            exit;
        }

        ee()->db->from('cartthrob_settings')
            ->where('`key`', 'price_modifier_presets')
            ->where('site_id', ee()->config->item('site_id'));

        $presets = (ee()->input->post('price_modifier_presets')) ? ee()->input->post('price_modifier_presets', true) : [];
        $value = [];

        foreach ($presets as $preset) {
            if (!is_array($preset['values'])) {
                continue;
            }

            $value[$preset['name']] = $preset['values'];
        }

        $data = [
            'value' => serialize($value),
            'serialized' => 1,
        ];

        if (ee()->db->count_all_results() == 0) {
            $data['site_id'] = ee()->config->item('site_id');
            $data['`key`'] = 'price_modifier_presets';

            ee()->db->insert('cartthrob_settings', $data);
        } else {
            ee()->db->update(
                'cartthrob_settings',
                $data,
                [
                    'site_id' => ee()->config->item('site_id'),
                    '`key`' => 'price_modifier_presets',
                ]
            );
        }

        // forces json output
        ee()->output->send_ajax_response(['CSRF_TOKEN' => ee()->functions->add_form_security_hash('{csrf_token}')]);
    }

    // gets saved settings, overrides cached settings.

    /**
     * Validates, cleans, saves data, reports errors if fields were not filled in, saves and updates CartThrob settings in the database
     *
     * @param null
     */
    public function install_templates()
    {
        $this->initialize();

        if (version_compare(APP_VER, '2.2', '<')) {
            $orig_view_path = ee()->load->_ci_view_path;

            ee()->load->_ci_view_path = PATH_THIRD . 'cartthrob/views/';

            ee()->load->library('package_installer', ['xml' => PATH_THIRD . 'cartthrob/installer/installer.xml']);

            ee()->load->_ci_view_path = $orig_view_path;
        } else {
            ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

            ee()->load->library('package_installer', ['xml' => PATH_THIRD . 'cartthrob/installer/installer.xml']);
        }

        if (is_array($templates_to_install = ee()->input->post('templates')) || is_array($channels_to_install = ee()->input->post('channels'))) {
            ee()->mbr_addon_builder->install_templates(ee()->input->post('templates'));

            // package data won't work since it uses product IDs in the serialized string... and whaddya know that stuff's all wrong.
            // related item data won't work either.
            ee()->mbr_addon_builder->install_channels(ee()->input->post('channels'), ee()->input->post('channel_data'));

            $_POST = [];

            $settings = $this->get_settings();

            $_POST['product_channels'] = element('product_channels', $settings);
            $_POST['product_channel_fields'] = element('product_channel_fields', $settings);

            $query = ee()->channel_model->get_channels(null, [], [
                [
                    'channel_name' => [
                        'products',
                        'store_packages',
                        'orders',
                        'purchased_items',
                        'coupon_codes',
                        'discounts',
                    ],
                ],
            ]);

            foreach ($query->result() as $channel) {
                $query = ee()->field_model->get_fields();

                if ($channel->channel_name == 'products') {
                    if (is_array($_POST['product_channels'])) {
                        $_POST['product_channels'][] = $channel->channel_id;
                    } else {
                        $_POST['product_channels'] = [$channel->channel_id];
                    }

                    $_POST['product_channels'] = array_unique($_POST['product_channels']);

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'product_price':
                                $_POST['product_channel_fields'][$channel->channel_id]['price'] = $field->field_id;
                                break;
                            case 'product_shipping':
                                $_POST['product_channel_fields'][$channel->channel_id]['shipping'] = $field->field_id;
                                break;
                            case 'product_weight':
                                $_POST['product_channel_fields'][$channel->channel_id]['weight'] = $field->field_id;
                                break;
                            case 'product_inventory':
                                $_POST['product_channel_fields'][$channel->channel_id]['inventory'] = $field->field_id;
                                break;
                            case 'product_size':
                            case 'product_options_other':
                            case 'product_color':
                                if (isset($_POST['product_channel_fields'][$channel->channel_id]['price_modifiers'])) {
                                    $_POST['product_channel_fields'][$channel->channel_id]['price_modifiers'][] = $field->field_id;
                                } else {
                                    $_POST['product_channel_fields'][$channel->channel_id]['price_modifiers'] = [$field->field_id];
                                }
                                break;
                        }
                    }
                }

                if ($channel->channel_name === 'store_packages') {
                    if (is_array($_POST['product_channels'])) {
                        $_POST['product_channels'][] = $channel->channel_id;
                    } else {
                        $_POST['product_channels'] = [$channel->channel_id];
                    }

                    $_POST['product_channels'] = array_unique($_POST['product_channels']);

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'packages_price':
                                $_POST['product_channel_fields'][$channel->channel_id]['price'] = $field->field_id;
                                break;
                        }
                    }
                }

                if ($channel->channel_name == 'orders') {
                    $_POST['save_orders'] = 1;

                    $_POST['orders_channel'] = $channel->channel_id;

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'order_items':
                                $_POST['orders_items_field'] = $field->field_id;
                                break;
                            case 'order_subtotal':
                                $_POST['orders_subtotal_field'] = $field->field_id;
                                break;
                            case 'order_ip_address':
                                $_POST['orders_customer_ip_address'] = $field->field_id;
                                break;
                            case 'order_payment_gateway':
                                $_POST['orders_payment_gateway'] = $field->field_id;
                                break;
                            case 'order_full_billing_address':
                                $_POST['orders_full_billing_address'] = $field->field_id;
                                break;
                            case 'order_billing_company':
                                $_POST['orders_billing_company'] = $field->field_id;
                                break;
                            case 'order_billing_country':
                                $_POST['orders_billing_country'] = $field->field_id;
                                break;
                            case 'order_country_code':
                                $_POST['orders_country_code'] = $field->field_id;
                                break;
                            case 'order_full_shipping_address':
                                $_POST['orders_full_shipping_address'] = $field->field_id;
                                break;
                            case 'order_shipping_company':
                                $_POST['orders_shipping_company'] = $field->field_id;
                                break;
                            case 'order_shipping_country':
                                $_POST['orders_shipping_country'] = $field->field_id;
                                break;
                            case 'order_shipping_country_code':
                                $_POST['orders_shipping_country_code'] = $field->field_id;
                                break;
                            case 'order_customer_full_name':
                                $_POST['orders_customer_name'] = $field->field_id;
                                break;
                            case 'order_discount':
                                $_POST['orders_discount_field'] = $field->field_id;
                                break;
                            case 'order_subtotal_plus_tax':
                                $_POST['orders_subtotal_plus_tax_field'] = $field->field_id;
                                break;
                            case 'order_tax':
                                $_POST['orders_tax_field'] = $field->field_id;
                                break;
                            case 'order_shipping':
                                $_POST['orders_shipping_field'] = $field->field_id;
                                break;
                            case 'order_shipping_plus_tax':
                                $_POST['orders_shipping_plus_tax_field'] = $field->field_id;
                                break;
                            case 'order_total':
                                $_POST['orders_total_field'] = $field->field_id;
                                break;
                            case 'order_transaction_id':
                                $_POST['orders_transaction_id'] = $field->field_id;
                                break;
                            case 'order_last_four':
                                $_POST['orders_last_four_digits'] = $field->field_id;
                                break;
                            case 'order_coupons':
                                $_POST['orders_coupon_codes'] = $field->field_id;
                                break;
                            case 'order_customer_email':
                                $_POST['orders_customer_email'] = $field->field_id;
                                break;
                            case 'order_customer_phone':
                                $_POST['orders_customer_phone'] = $field->field_id;
                                break;
                            case 'order_billing_first_name':
                                $_POST['orders_billing_first_name'] = $field->field_id;
                                break;
                            case 'order_billing_last_name':
                                $_POST['orders_billing_last_name'] = $field->field_id;
                                break;
                            case 'order_billing_address':
                                $_POST['orders_billing_address'] = $field->field_id;
                                break;
                            case 'order_billing_address2':
                                $_POST['orders_billing_address2'] = $field->field_id;
                                break;
                            case 'order_billing_city':
                                $_POST['orders_billing_city'] = $field->field_id;
                                break;
                            case 'order_billing_state':
                                $_POST['orders_billing_state'] = $field->field_id;
                                break;
                            case 'order_billing_zip':
                                $_POST['orders_billing_zip'] = $field->field_id;
                                break;
                            case 'order_shipping_first_name':
                                $_POST['orders_shipping_first_name'] = $field->field_id;
                                break;
                            case 'order_shipping_last_name':
                                $_POST['orders_shipping_last_name'] = $field->field_id;
                                break;
                            case 'order_shipping_address':
                                $_POST['orders_shipping_address'] = $field->field_id;
                                break;
                            case 'order_shipping_address2':
                                $_POST['orders_shipping_address2'] = $field->field_id;
                                break;
                            case 'order_shipping_city':
                                $_POST['orders_shipping_city'] = $field->field_id;
                                break;
                            case 'order_shipping_state':
                                $_POST['orders_shipping_state'] = $field->field_id;
                                break;
                            case 'order_shipping_zip':
                                $_POST['orders_shipping_zip'] = $field->field_id;
                                break;
                            case 'order_shipping_option':
                                $_POST['orders_shipping_option'] = $field->field_id;
                                break;
                            case 'order_error_message':
                                $_POST['orders_error_message_field'] = $field->field_id;
                                break;
                            case 'order_site_id':
                                $_POST['orders_site_id'] = $field->field_id;
                                break;
                            case 'order_subscription_id':
                                $_POST['orders_subscription_id'] = $field->field_id;
                                break;
                            case 'order_vault_id':
                                $_POST['orders_vault_id'] = $field->field_id;
                                break;
                        }
                    }
                }

                if ($channel->channel_name == 'purchased_items') {
                    $_POST['save_purchased_items'] = 1;

                    $_POST['purchased_items_channel'] = $channel->channel_id;

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'purchased_id':
                                $_POST['purchased_items_id_field'] = $field->field_id;
                                break;
                            case 'purchased_quantity':
                                $_POST['purchased_items_quantity_field'] = $field->field_id;
                                break;
                            case 'purchased_price':
                                $_POST['purchased_items_price_field'] = $field->field_id;
                                break;
                            case 'purchased_order_id':
                                $_POST['purchased_items_order_id_field'] = $field->field_id;
                                break;
                            case 'purchased_items_package_id':
                                $_POST['purchased_items_package_id_field'] = $field->field_id;
                                // no break
                            case 'purchased_items_license_number':
                                $_POST['purchased_items_license_number_field'] = $field->field_id;
                                break;
                        }
                    }
                }

                if ($channel->channel_name == 'coupon_codes') {
                    $_POST['coupon_code_field'] = 'title';

                    $_POST['coupon_code_channel'] = $channel->channel_id;

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'coupon_code_type':
                                $_POST['coupon_code_type'] = $field->field_id;
                                break;
                        }
                    }
                }

                if ($channel->channel_name == 'discounts') {
                    $_POST['discount_channel'] = $channel->channel_id;

                    foreach ($query->result() as $field) {
                        switch ($field->field_name) {
                            case 'discount_type':
                                $_POST['discount_type'] = $field->field_id;
                                break;
                        }
                    }
                }
            }

            $_GET['return'] = 'installation';

            $this->quick_save(false);
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob'));
    }

    // --------------------------------
    //  Get Payment Gateways
    // --------------------------------

    public function install_theme()
    {
        $themes = $this->get_themes();

        $theme = ee()->input->post('theme');

        if (!in_array($theme, $themes)) {
            show_error(lang('invalid_theme'));
        }

        ee()->load->add_package_path(PATH_THIRD . 'cartthrob/');

        if (ee()->config->item('cartthrob_third_party_path')) {
            $theme_path = ee()->config->slash_item('cartthrob_third_party_path') . 'installer/' . $theme . '/';
        } else {
            $theme_path = PATH_THIRD . 'cartthrob/third_party/installer/' . $theme . '/';
        }

        ee()->load->library('package_installer', ['xml' => $theme_path . 'installer.xml']);

        ee()->package_installer->set_template_path($theme_path . 'templates/')->install();

        ee()->session->set_flashdata('theme_errors', ee()->package_installer->errors());

        ee()->session->set_flashdata('themes_installed', ee()->package_installer->installed());

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/installation'));
    }

    public function save_template_variables()
    {
        ee()->session->set_flashdata('template_variables_updated', ee()->package_installer->installed());
        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/installation'));
    }

    public function import_settings()
    {
        $this->initialize();

        if (isset($_FILES['settings']) && $_FILES['settings']['error'] == 0) {
            ee()->load->helper('file');

            if ($new_settings = read_file($_FILES['settings']['tmp_name'])) {
                $_POST = _unserialize($new_settings);
            }

            $_GET['return'] = 'import_export';

            $this->quick_save();
        }

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/import_export'));
    }

    // --------------------------------
    //  Get Shipping Plugins
    // --------------------------------

    public function set_encryption_key()
    {
        ee()->config->_update_config(['encryption_key' => ee()->input->post('encryption_key', true)]);

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob'));
    }

    /**
     * Checks to see if any fields are missing. If the fields are missing, The "missing" array is returned, and 'valid' boolean is false.
     *
     * @param null
     * @return array
     */
    public function validate_settings()
    {
        $valid = true;

        $missing = [];

        foreach ($this->required_settings as $required) {
            if (!ee()->input->post($required)) {
                $missing[] = $required;

                $valid = false;
            }
        }

        return ['valid' => $valid, 'missing' => $missing];
    }

    /// BEGIN  TAXES ****************************************

    /**
     * Generates & downloads a file called "cartthrob_settings.txt" that contains current settings for CartThrob
     * Useful for backup and transfer.
     *
     * @param null
     */
    public function export_settings()
    {
        $this->initialize();

        ee()->load->helper('download');

        force_download('cartthrob_settings.txt', serialize($this->get_settings()));
    }

    public function taxes()
    {
        // @TODO tax model
        ee()->load->model('tax_model');
        $limit = '50';
        /////////// pagination //////////////////////////////
        if (!$offset = ee()->input->get_post('rownum')) {
            $offset = 0;
        }
        ee()->load->library('pagination');

        $total = ee()->db->count_all('cartthrob_tax');

        if ($total == 0) {
            //	ee()->session->set_flashdata('message_failure', sprintf('%s %s %s', lang('cartthrob_module_name'), lang('nav_'.ee()->input->get('return')), lang('taxes_none')));
        }
        ee()->pagination->initialize($this->pagination_config('taxes', $total, $limit));

        $pagination = ee()->pagination->create_links();
        /////////// end pagination //////////////////////////////

        $data = ee()->tax_model->read(null, $limit, $offset);

        return $this->load_view(
            __FUNCTION__,
            [
                'taxes' => $data,
                'pagination' => $pagination,
                'form_open' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/quick_save',
                    ['return' => 'taxes'])),
                'add_href' => ee('CP/URL')->make('addons/settings/cartthrob/add_tax'),
            ]
        );
    }

    private function pagination_config($method, $total_rows, $per_page = 50)
    {
        $config['base_url'] = ee('CP/URL')->make('addons/settings/cartthrob/' . $method);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'rownum';
        $config['full_tag_open'] = '<p id="paginationLinks">';
        $config['full_tag_close'] = '</p>';
        $config['prev_link'] = '<img src="' . ee()->cp->cp_theme_url . 'images/pagination_prev_button.gif" width="13" height="13" alt="<" />';
        $config['next_link'] = '<img src="' . ee()->cp->cp_theme_url . 'images/pagination_next_button.gif" width="13" height="13" alt=">" />';
        $config['first_link'] = '<img src="' . ee()->cp->cp_theme_url . 'images/pagination_first_button.gif" width="13" height="13" alt="< <" />';
        $config['last_link'] = '<img src="' . ee()->cp->cp_theme_url . 'images/pagination_last_button.gif" width="13" height="13" alt="> >" />';

        return $config;
    }

    public function add_tax()
    {
        return $this->load_view(
            __FUNCTION__,
            [
                'form_edit' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/form_update_tax', ['return' => 'taxes'])),
            ]
        );
    }

    public function edit_tax()
    {
        ee()->load->model('tax_model');

        $data = ee()->tax_model->read(ee()->input->get('id'));

        return $this->load_view(
            __FUNCTION__,
            [
                'tax' => $data,
                'form_edit' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/form_update_tax', ['return' => 'taxes'])),
            ]
        );
    }

    public function delete_tax()
    {
        ee()->load->model('tax_model');

        $data = ee()->tax_model->read(ee()->input->get('id'));

        return $this->load_view(
            __FUNCTION__,
            [
                'tax' => $data,
                'form_edit' => form_open(ee('CP/URL')->make('addons/settings/cartthrob/form_update_tax', ['return' => 'taxes'])),
            ]
        );
    }

    /// END TAXES ****************************************

    /// START BETA ****************************************

    public function form_update_beta()
    {
        $this->initialize();
    }

    /// END BETA ****************************************

    public function form_update_tax()
    {
        // @TODO add tax
        $this->initialize();
        ee()->load->model('tax_model');
        $data = [];

        foreach (array_keys($_POST) as $key) {
            if (
                !in_array($key, $this->remove_keys) &&
                !preg_match('/^(Cartthrob_.*?_settings|product_weblogs|product_weblog_fields|default_location)_.*/', $key)
            ) {
                $data[$key] = ee()->input->post($key, true);
            }
        }

        if (!ee()->input->post('id')) {
            $data['id'] = ee()->input->post('add_id');
            ee()->tax_model->create($data);
        } elseif (ee()->input->post('delete_tax')) {
            ee()->tax_model->delete(ee()->input->post('id'));
        } else {
            ee()->tax_model->update($data, ee()->input->post('id'));
        }

        ee()->session->set_flashdata(
            'message_success',
            sprintf('%s %s %s', lang('cartthrob_module_name'), lang('nav_' . ee()->input->get('return')), lang('settings_saved'))
        );

        ee()->functions->redirect(ee('CP/URL')->make('addons/settings/cartthrob/' . ee()->input->get('return', true)));
    }

    /**
     * package filter
     *
     * used in package fieldtype to process entry filter ajax request
     */
    public function package_filter()
    {
        if (!AJAX_REQUEST) {
            show_error(ee()->lang->line('unauthorized_access'));
        }

        ee()->load->library('cartthrob_loader');

        $channels = ee()->config->item('cartthrob:product_channels');

        ee()->load->model('search_model');

        if (ee()->input->get_post('channel_id') && ee()->input->get_post('channel_id') != 'null') {
            $channels = ee()->input->get_post('channel_id');
        }

        $keywords = ee()->input->get_post('keywords');

        ee()->load->model('cartthrob_entries_model');

        // typed in an entry_id
        if (is_numeric($keywords)) {
            $entries = [];

            if ($entry = ee()->cartthrob_entries_model->entry($keywords)) {
                $entries[] = $entry;
            }
        } else {
            ee()->load->helper('text');

            $search = [
                'channel_id' => '',
                'cat_id' => (ee()->input->get_post('cat_id') != 'all') ? ee()->input->get_post('cat_id') : '',
                'status' => (ee()->input->get_post('status') != 'all') ? ee()->input->get_post('status') : '',
                'date_range' => ee()->input->get_post('date_range'),
                'author_id' => ee()->input->get_post('author_id'),
                'search_in' => (ee()->input->get_post('search_in')) ? ee()->input->get_post('search_in') : 'title',
                'exact_match' => ee()->input->get_post('exact_match'),
                'keywords' => $keywords,
                'search_keywords' => (ee()->config->item('auto_convert_high_ascii') === 'y') ? ascii_to_entities($keywords) : $keywords,
                '_hook_wheres' => [
                    ee()->db->dbprefix('channel_titles') . '.channel_id' => $channels,
                ],
                // 'perpage' => ee()->input->get_post('perpage'),
                // 'rownum' => ee()->input->get_post('rownum'),
            ];

            // $data = ee()->search_model->build_main_query($search, array('title' => 'asc'));
            $query = ee()->db->select('entry_id')
                ->distinct()
                ->from('exp_channel_titles')
                ->where('site_id', ee()->config->item('site_id'))
                ->where_in('channel_id', $channels)
                ->get();

            ee()->load->library('data_filter');

            $entry_ids = ee()->data_filter->key_values($query->result_array(), 'entry_id');

            $entries = ee()->cartthrob_entries_model->entries($entry_ids);
        }

        ee()->load->model(['product_model', 'cartthrob_field_model']);

        foreach ($entries as &$entry) {
            $entry['price_modifiers'] = ee()->product_model->get_all_price_modifiers($entry['entry_id']);

            foreach ($entry['price_modifiers'] as $price_modifier => $options) {
                $entry['price_modifiers'][$price_modifier]['label'] = ee()->cartthrob_field_model->get_field_label(ee()->cartthrob_field_model->get_field_id($price_modifier));
            }
        }

        ee()->output->send_ajax_response([
            'entries' => $entries,
            'id' => ee()->input->get_post('filter_id'),
        ]);
    }

    public function crontabulous_get_pending_subscriptions()
    {
        ee()->load->library('cartthrob_loader', ['cart' => []]);

        ee()->load->library(['crontabulous_responder', 'paths']);

        ee()->crontabulous_responder->set_private_key(ee()->cartthrob->store->config('crontabulous_api_key'));

        if (ee()->crontabulous_responder->validate_request()) {
            ee()->load->model('subscription_model');

            $query = ee()->subscription_model->get_pending_subscriptions();

            foreach ($query->result() as $row) {
                // add the process subscription url for this pending subscription to the queue
                ee()->crontabulous_responder->enqueue(
                    htmlentities(ee()->paths->build_action_url(
                        'Cartthrob_mcp',
                        'crontabulous_process_subscription',
                        [
                            'id' => $row->id,
                        ]
                    ), ENT_QUOTES, 'UTF-8', false)
                );
            }
        }

        ee()->crontabulous_responder->send_response();
    }

    public function crontabulous_process_subscription()
    {
        ee()->load->library('cartthrob_loader', ['cart' => []]);

        ee()->load->library(['crontabulous_responder', 'paths']);

        ee()->crontabulous_responder->set_private_key(ee()->cartthrob->store->config('crontabulous_api_key'));

        if (ee()->crontabulous_responder->validate_request()) {
            $this->process_subscription(ee()->input->get('id'));
        }

        ee()->crontabulous_responder->send_response();
    }

    public function process_subscription($subscription_id)
    {
        if (!is_numeric($subscription_id)) {
            exit;
        }

        // load cartthrob core
        ee()->load->library('cartthrob_loader', ['cart' => []]);

        ee()->load->library('cartthrob_payments');

        ee()->load->model(['vault_model', 'subscription_model', 'order_model', 'customer_model']);

        return ee()->cartthrob_payments->apply('subscriptions', 'process_subscription', $subscription_id);
    }

    /**
     * This method is accessed in many ways
     *
     * a) cron_subscriptions.sh
     * b) cron_subsciptions.pl
     * c) extload.php
     * d) url
     *
     * @return Type Description
     */
    public function process_subscriptions($get_ids = false)
    {
        // load cartthrob core
        ee()->load->library('cartthrob_loader', ['cart' => []]);

        ee()->load->library('cartthrob_payments');

        ee()->load->model(['vault_model', 'subscription_model', 'order_model', 'customer_model']);

        $subscriptions = [];

        // get subscriptions that are due for billing and/or expired
        $query = ee()->subscription_model->get_pending_subscriptions();

        $subscriptions = $query->result_array();

        $query->free_result();

        // return a space delimited list of subscription ids which the shell script will use to process subs individually
        if ($get_ids) {
            $subscription_ids = [];

            foreach ($subscriptions as $subscription) {
                $subscription_ids[] = $subscription['id'];
            }

            exit(implode(' ', $subscription_ids));
        }

        // extload
        // loop through subscriptions and process each one via php cli and passthru
        if (@php_sapi_name() === 'cli') {
            if (empty($_SERVER['argv'])) {
                exit;
            }

            $args = $_SERVER['argv'];

            // add the php command
            array_unshift($args, 'php');

            // remove the current cron command
            array_pop($args);

            // add the process_subscription cron command
            array_push($args, 'process_subscription');

            foreach ($subscriptions as $subscription) {
                // add the id to the command
                array_push($args, $subscription['id']);

                passthru(implode(' ', $args));

                array_pop($args);
            }

            exit;
        }

        // this is the url interface, process ONE subscription
        if ($subscription = array_shift($subscriptions)) {
            $this->process_subscription($subscription['id']);
        }
    }

    /**
     * Used by the manual processing in the CP
     */
    public function ajax_process_subscription()
    {
        $subscription_id = ee()->input->get_post('subscription_id');

        // exit($subscription_id);
        $this->process_subscription($subscription_id);

        exit;
    }

    public function ajax_get_pending_subscriptions()
    {
        if (!ee()->input->is_ajax_request()) {
            return show_404();
        }

        ee()->load->model('subscription_model');

        $query = ee()->subscription_model->get_pending_subscriptions();

        $data = [
            'count' => $query->num_rows(),
            'subscriptions' => [],
        ];

        foreach ($query->result() as $row) {
            $data['subscriptions'][] = $row->id;
        }

        $query->free_result();

        ee()->output->send_ajax_response($data);

        exit;
    }

    /**
     * get_news
     *
     * @return string
     **/
    public function get_news()
    {
        $this->initialize();

        ee()->load->library('curl');
        ee()->load->library('simple_cache');
        ee()->load->helper('data_formatting');

        $return_data['version_update'] = null;
        $return_data['news'] = null;

        $cache = ee()->simple_cache->get('cartthrob/version');

        if (!$cache) {
            $data = ee()->curl->simple_get('http://cartthrob.com/site/versions/cartthrob_2');

            if (!$data) {
                return $return_data;
            }

            $cache = ee()->simple_cache->set('cartthrob/version', $data);
        }

        if (empty($cache)) {
            return $return_data;
        }

        parse_str($cache, $content);

        if (isset($content['version']) && $content['version'] > $this->version()) {
            $return_data['version_update'] = "<a href='http://cartthrob.com/cart/purchased_items/'>CartThrob has been updated to version " . $content['version'] . '</a>';
        } else {
            $return_data['version_update'] = ee()->lang->line('there_are_no_updates');
        }

        if (!empty($content['news'])) {
            $return_data['news'] = stripslashes(urldecode($content['news']));
        }

        return $return_data;
    }

    public function garbage_collection()
    {
        header('X-Robots-Tag: noindex');

        ee()->db->where('expires <', @time())->delete('cartthrob_sessions');

        ee()->db->query('DELETE `' . ee()->db->dbprefix('cartthrob_cart') . '`
				  FROM `' . ee()->db->dbprefix('cartthrob_cart') . '`
				  LEFT OUTER JOIN `' . ee()->db->dbprefix('cartthrob_sessions') . '`
				  ON `' . ee()->db->dbprefix('cartthrob_cart') . '`.`id` = `' . ee()->db->dbprefix('cartthrob_sessions') . '`.`cart_id`
				  WHERE `' . ee()->db->dbprefix('cartthrob_sessions') . '`.`cart_id` IS NULL');
    }

    protected function html($content, $tag = 'p', $attributes = '')
    {
        if (is_array($attributes)) {
            $attributes = _parse_attributes($attributes);
        }

        return '<' . $tag . $attributes . '>' . $content . '</' . $tag . '>';
    }

    private function json_response($data)
    {
        ee()->load->library('javascript');

        if (ee()->config->item('send_headers') == 'y') {
            ee()->load->library('user_agent', null, 'user_agent');

            // many browsers do not consistently like this content type
            // array('Firefox', 'Mozilla', 'Netscape', 'Camino', 'Firebird')
            if (0 && is_array($msg) && in_array(ee()->user_agent->browser(), ['Safari', 'Chrome'])) {
                @header('Content-Type: application/json');
            } else {
                @header('Content-Type: text/html; charset=UTF-8');
            }
        }

        die(json_encode($data));
    }
}
