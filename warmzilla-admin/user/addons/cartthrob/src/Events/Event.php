<?php

namespace CartThrob;

class Event
{
    const TYPE_LOW_STOCK = 'low_stock';
    const TYPE_STATUS_CHANGE = 'status_change';
    const TYPE_ORDER_PROCESSING = 'processing';
    const TYPE_ORDER_COMPLETED = 'completed';
    const TYPE_ORDER_DECLINED = 'declined';
    const TYPE_ORDER_FAILED = 'failed';
}
