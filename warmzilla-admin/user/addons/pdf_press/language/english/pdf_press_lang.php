<?php

$lang = array(

/* ----------------------------------------
/*  Required for MODULES page
/* ----------------------------------------*/

"pdf_press_module_name"			=> 	"PDF Press",

"pdf_press_module_description" 	=>	"Saves a template as a PDF, uses the dompdf library (https://github.com/dompdf/dompdf)",
//-----------------------------------------//

"requirement"					=> 	"Requirement",

"required"						=> 	"Required",

"present"						=> 	"Current",

"config_name"					=> 	"Configuration",

"value"							=> 	"Value",

"desc"							=> 	"Description",

"setting_override"				=> 	"You can override these settings by modifying 'dompdf_config.custom.inc.php' in the /third_party/pdf_press folder.",

"preview"						=> "PDF Preview",

"index"							=> "Configuration",

"settings"						=> "Setting Presets",

"preview_desc"					=> "Enter a template path (ex. 'site/test') to test how your template will look as a PDF.",

"error_check_markup"			=> "PDF Press: There was an error in the DOMPDF Library, usually this means your HTML/CSS is not compliant in some way, please check your HTML/CSS markup using the W3C Validator (<a href='http://validator.w3.org' target='_blank'>http://validator.w3.org</a>).",

"dompdf_error"					=> "Error Message: ",

"error_curl_fopen"				=> "Both allow_url_fopen and CURL are disabled on the server. Please contact your server administrator to enabled either CURL or set 'allow_url_fopen = true' in php.ini.",

"encrypt_description"			=> "You can set encryption settings & permissions for your PDFs below",

"key"							=> "Preset Short Name",

"data"							=> "Preset Setting Data",

"attachment"					=> "Render Type",

"size"							=> "Size",

"orientation"					=> "Orientation",

"filename"						=> "Filename",

"encrypt"						=> "Encryption",

"userpass"						=> "User password",

"ownerpass"						=> "Owner password",

"can_print"						=> "Print Enabled?",

"can_modify"					=> "Editable?",

"can_copy"						=> "Can Copy?",

"can_add"						=> "Can Add to PDF?",

"cache"							=> "Cache Enabled",

"cache_enabled"  				=> "Enable PDF caching?",

"cache_path"					=> "Custom Cache Path",

"cache_path_description"		=> "You can set the custom cache path, otherwise the default EE path will be used.",

"cache_ttl"						=> "Cache timeout in seconds (default 3600)",

"no_setting_found"				=> "PDF Press: No setting presets found for the provided 'key' parameter.",

"setting_form_error"			=> "PDF Press<br/>Could not save the setting presets. There are errors in the form.",

"fonts"							=> "Fonts / Unicode",

"font_install_help"				=> "In order to use Unicode, you must upload a Unicode font before you set the font-family in the CSS. You can also upload other fonts to use in your PDF document.",

"setting_delete_success"		=> "Setting deleted!",

"cp_screen_name"                => "PDF Press",

"no_settings"                   => "No Settings",

"create_settings"               => "New Setting Preset",

"dompdf_temp_dir"               => "DOMPDF_TEMP_DIR",

"dompdf_temp_dir_desc"          => "Temporary folder",

"dompdf_font_dir"               => "Font directory",

"dompdf_font_dir_desc"          => "",

"dompdf_fontcache_dir"          => "Font cache directory",

"dompdf_fontcache_dir_desc"     => "",

"dompdf_logoutput_file"         => "Dompdf log file",

"dompdf_logoutput_file_desc"    => "",

"dompdf_enable_fontsubsetting"  => "Enable Font Subsetting",

"dompdf_enable_fontsubsetting_desc" => "Enable font subsetting, will make smaller documents when using Unicode fonts",

"dompdf_pdf_backend"            => "PDF Backend",

"dompdf_pdf_backend_desc"       => "Valid settings are 'PDFLib', 'CPDF', 'GD', and 'auto'. 'auto' will look for PDFLib and use it if found, or if not it will fall back on CPDF. 'GD' renders PDFs to graphic files. If you're unsure leave the setting at 'auto' or 'CPDF'",

"dompdf_pdflib_license"         => "PDFLib License Key",

"dompdf_pdflib_license_desc"    => "If you've bought the separate commercial <a href='https://www.pdflib.com/' target='_blank'>pdflib PDF backend</a> and selected 'pdflib' above enter the pdflib license key here. <strong>NOTE: THIS IS *NOT* THE LICENSE KEY FOR PDF PRESS. LEAVE IT BLANK IF YOU'RE UNSURE.</strong>",

"dompdf_default_media_type"     => "Default CSS Media Type",

"dompdf_default_media_type_desc" => "DOMPDF will use the CSS marked with a certain media type in the style tag. Default is \"pdf\" so it will look for any CSS w/ &lt;style media=\"pdf\"&gt;. You could also use (print, screen, etc ...)",

"dompdf_default_paper_size"     => "Default Paper Size",

"dompdf_default_paper_size_desc" => "Default paper size (A4, letter, ...). North America standard is \"letter\"; other countries generally \"a4\"",

"dompdf_default_paper_orientation" => "Default Paper Orientation",

"dompdf_default_paper_orientation_desc" => "Portrait or Landscape",

"dompdf_default_font"           => "Default Font",

"dompdf_default_font_desc"      => "Default font, used if the specified font in the CSS stylesheet was not found",

"dompdf_dpi"                    => "Image DPI",

"dompdf_dpi_desc"               => "The default DPI setting for images & fonts. Inline images can be overriden using width & height. Background images will only use this parameter. For the purposes of DOMPDF, pixels per inch (PPI) = dots per inch (DPI).",

"dompdf_enable_php"             => "Enable PHP",

"dompdf_enable_php_desc"        => "If this setting is set to true then DOMPDF will automatically evaluate embedded PHP contained within &lt;script type=\"text/php\"&gt; ... &lt;/script&gt; tags. <br/><br/> <strong>This setting may increase the risk of system exploit.</strong> Do not change this settings without understanding the consequences. Additional documentation is available on the dompdf wiki at: <a href='https://github.com/dompdf/dompdf/wiki' target='_blank'>https://github.com/dompdf/dompdf/wiki</a>",

"dompdf_enable_javascript"      => "Enable Javascript",

"dompdf_enable_javascript_desc" => "If this setting is set to true then DOMPDF will automatically insert JavaScript code contained within &lt;script type=\"text/javascript\"&gt; ... &lt;/script&gt; tags.",

"dompdf_enable_remote"          => "Enable Remote CSS/Images",

"dompdf_enable_remote_desc"     => "If this setting is set to true, DOMPDF will access remote sites for images and CSS files as required. This is enabled by default because ExpressionEngine uses absolute file paths in images/css, so this setting is neccessary for most cases. <br/><br/> <strong>Be very careful enabling this setting w/ PHP enabled. This setting may increase the risk of system exploit.</strong> Do not change this settings without understanding the consequences. Additional documentation is available on the dompdf wiki at: <a href='https://github.com/dompdf/dompdf/wiki' target='_blank'>https://github.com/dompdf/dompdf/wiki</a>",

"dompdf_enable_html5parser"     => "Enable HTML5 Parser",

"dompdf_enable_html5parser_desc" => "Enable the HTML5 parser (recommended)",

"debugpng"                      => "Debug PNG",

"debugpng_desc"                 => "Debug PNG images",

"debugcss"                      => "Debug CSS",

"debugcss_desc"                 => "Debug CSS",

"debugkeeptemp"                 => "Keep Temp Files",

"debugkeeptemp_desc"            => "Keep temporary image files",

"debug_layout"                  => "Debug Layout",

"debug_layout_desc"             => "Debug layout",

"debug_layout_lines"            => "Debug Layout Lines",

"debug_layout_lines_desc"       => "Debug text lines layout",

"debug_layout_blocks"           => "Debug Layout Blocks",

"debug_layout_blocks_desc"      => "Debug block elements layout",

"debug_layout_inline"           => "Debug Layout Inline",

"debug_layout_inline_desc"      => "Debug inline elements layout",

"debug_layout_paddingbox"       => "Debug Padding-box",

"debug_layout_paddingbox_desc"  => "Debug padding boxes layout",

"dompdf_base_path"              => "Base Path",

"dompdf_base_path_desc"         => "If you use relative paths for images & css, the base path tells DOMPDF where the files are on the server. Default is the web/document root.",

"btn_save_options"              => "Save",

"edit_options"                  => "Configuration",

"debug_options"                 => "Debug Options",
/* END */
''=>''
);

