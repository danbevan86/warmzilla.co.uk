<?php
if(!session_id())
	session_start();
/*
=====================================================
PDF Press
-----------------------------------------------------
 http://www.anecka.com/pdf_press
-----------------------------------------------------
 Copyright (c) 2013 Patrick Pohler
=====================================================
 This software is based upon and derived from
 ExpressionEngine software protected under
 copyright dated 2004 - 2012. Please see
 http://expressionengine.com/docs/license.html
=====================================================
 File: mcp.pdf_press.php
-----------------------------------------------------
 Dependencies: dompdf/
-----------------------------------------------------
 Purpose: Allows an EE template to be saved as a PDF
=====================================================
*/

use EllisLab\ExpressionEngine\Library\CP\Table;

require PATH_THIRD."pdf_press/config.php";

class Pdf_press_mcp {

	var $site_id = 1;
	var $base_url;
	var $perpage = 25;
	var $sidebar;var $home_nav;var $settings_nav;var $fonts_nav; var $preview_nav;

	function __construct() {
		// Make a local reference to the ExpressionEngine super object
		$this->site_id = ee()->config->item('site_id');
		$this->base_url = ee('CP/URL', 'addons/settings/pdf_press');
		ee()->view->cp_page_title = ee()->lang->line('pdf_press_module_name');

		$this->sidebar = ee('CP/Sidebar')->make();

        $this->home_nav = $this->sidebar->addHeader(lang('configuration'), ee('CP/URL', 'addons/settings/pdf_press'));

        $this->preview_nav = $this->sidebar->addHeader(lang('preview'), ee('CP/URL', 'addons/settings/pdf_press/preview'));

		$this->settings_nav = $this->sidebar->addHeader(lang('settings'), ee('CP/URL', 'addons/settings/pdf_press/settings'))
			->withButton(lang('new'), ee('CP/URL', 'addons/settings/pdf_press/new_setting'));

	}

	function index() {
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');

        ee()->cp->add_js_script(array(
            'file' => array('cp/form_group'),
        ));

        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            $options_data = [
                'dompdf_pdf_backend'                => ee()->input->post('dompdf_pdf_backend'),
                'dompdf_default_media_type'         => ee()->input->post('dompdf_default_media_type'),
                'dompdf_default_paper_size'         => ee()->input->post('dompdf_default_paper_size'),
                'dompdf_default_paper_orientation'  => ee()->input->post('dompdf_default_paper_orientation'),
                'dompdf_default_font'               => ee()->input->post('dompdf_default_font'),
                'dompdf_dpi'                        => ee()->input->post('dompdf_dpi'),
                'dompdf_pdflib_license'             => ee()->input->post('dompdf_pdflib_license'),
                'dompdf_enable_fontsubsetting'      => (ee()->input->post('dompdf_enable_fontsubsetting') == 'y' ? true: false),
                'dompdf_enable_html5parser'         => (ee()->input->post('dompdf_enable_html5parser') == 'y' ? true: false),
                'dompdf_enable_php'                 => (ee()->input->post('dompdf_enable_php') == 'y' ? true: false),
                'dompdf_enable_javascript'          => (ee()->input->post('dompdf_enable_javascript') == 'y' ? true: false),
                'dompdf_enable_remote'              => (ee()->input->post('dompdf_enable_remote') == 'y' ? true: false),
                'debugpng'                          => (ee()->input->post('debugpng') == 'y' ? true: false),
                'debugcss'                          => (ee()->input->post('debugcss') == 'y' ? true: false),
                'debugkeeptemp'                     => (ee()->input->post('debugkeeptemp') == 'y' ? true: false),
                'debug_layout'                      => (ee()->input->post('debug_layout') == 'y' ? true: false),
                'debug_layout_lines'                => (ee()->input->post('debug_layout_lines') == 'y' ? true: false),
                'debug_layout_blocks'               => (ee()->input->post('debug_layout_blocks') == 'y' ? true: false),
                'debug_layout_inline'               => (ee()->input->post('debug_layout_inline') == 'y' ? true: false),
                'debug_layout_paddingbox'            => (ee()->input->post('debug_layout_paddingbox') == 'y' ? true: false),
                'dompdf_base_path'                  => ee()->input->post('dompdf_base_path'),
            ];

            $options = [
              'data'    => json_encode($options_data),
            ];

            ee()->db->update('pdf_press_options', $options, ['site_id' => $this->site_id]);
        }

        $query = ee()->db->get_where('pdf_press_options', array('site_id' => $this->site_id));
        $row = $query->row();

        $vars = $this->_edit_options_form($row);
        return ee('View')->make('pdf_press:index')->render($vars);

	}

	function preview() {
		$_SESSION["authenticated"] = true;
		ee()->load->helper('form');
		ee()->load->library('table');
		ee()->load->library('javascript');

		$action_id = ee()->cp->fetch_action_id('Pdf_press', 'create_pdf');

		$add_query = "?";
		if(ee()->config->item('force_query_string') == 'y') $add_query = "";

		$path = ee()->input->post('path', '');

		$attachment = 0;
		$size = ee()->input->post('size','letter');
		$orientation = ee()->input->post('orientation', 'portrait');

		$url = ee()->functions->create_url("").$add_query."ACT=$action_id&attachment=$attachment&path=$path&size=$size&orientation=$orientation";

		$vars = [];
		$vars['url_path'] = $path;
		$vars['size'] = $size;
		$vars['orientation'] = $orientation;

		$vars['dom_path'] = $url;
		$vars['paper_sizes'] = array_keys($this->_get_paper_sizes());

        $query = ee()->db->get_where('pdf_press_options', array('site_id' => $this->site_id));
        $row = $query->row();
        $options = json_decode($row->data, true);
        $vars['default_paper_size'] = $options['dompdf_default_paper_size'];

		//var_dump($vars);
		return array(
			'body' => ee()->load->view('preview', $vars, TRUE),
			'breadcrumb' => array(
				ee('CP/URL', 'addons/settings/pdf_press')->compile() => lang('pdf_press_module_name')
			),
			'heading' => lang('preview')
		);
	}

	function settings() {
		ee()->cp->add_js_script('file', 'cp/sort_helper');
		ee()->cp->add_js_script('plugin', 'ee_table_reorder');
		ee()->cp->load_package_js('pdf_press');

		$query = $this->_get_settings($this->site_id, $this->perpage, 0);

		$sizes = array_keys($this->_get_paper_sizes());
		$settings = array();

		foreach($query->result() as $row) {
			$setting_data = json_decode($row->data, true);

			$setting_label = ($setting_data['attachment'] == 1 ? lang('Download') : lang('Browser')).", "
				.$setting_data['orientation'].", ".$setting_data['size'].", "
				.lang('encrypt')."? ".($setting_data['encrypt'] == 1 ? lang('Yes') : lang('No'))." "
				.$setting_data['filename'];

			$settings[] = array(
				'id'			=> $row->id,
				'key'			=> $row->key,
				'data'			=> $setting_label,
				array('toolbar_items' => array(
					'edit' => array(
			        'href' => ee('CP/URL', 'addons/settings/pdf_press/edit_setting/'.$row->id),
			        'title' => lang('edit')
			      ),
				)),
			);
		}

		$table = ee('CP/Table');
		$table->setColumns(
			array(
		    	'id',
		    	'key',
		    	'data',
				'manage' => array(
      				'type'  => Table::COL_TOOLBAR
    			)
		  	)
		);

		$table->setData($settings);
		$table->setNoResultsText('no_settings', 'create_settings', ee('CP/URL', 'addons/settings/pdf_press/new_setting'));

		$vars['table'] = $table->viewData(ee('CP/URL', 'addons/settings/pdf_press/settings'));

		return array(
			'body' => ee()->load->view('settings', $vars, TRUE),
			'breadcrumb' => array(
				ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
			),
			'heading' => lang('settings'),
			'sidebar' => $this->sidebar
		);

	}

	function edit_setting($id) {
		ee()->load->helper('form');
		ee()->load->library('table');
		ee()->load->library('javascript');

		$vars['form_hidden'] = null;
		$vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting');//BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=pdf_press'.AMP.'method=save_setting';

		$query = ee()->db->get_where('pdf_press_configs', array('id' => $id));
		$row = $query->row();

		$vars['data'] = $this->_edit_setting_form($row);
		$vars['preset_page_title'] = lang('Edit Preset: '.$row->key);

		return ee()->load->view('setting-form', $vars, TRUE);
	}

	function delete_setting($id) {
		ee()->db->delete('pdf_press_configs', array("id" => $id));

		ee()->session->set_flashdata('message_success',lang('setting_delete_success'));

		ee()->functions->redirect(ee('CP/URL', 'addons/settings/pdf_press/settings'));
	}

	function new_setting() {
		ee()->load->helper('form');
		ee()->load->library('table');
		ee()->load->library('javascript');

		$vars = array();
		$vars['form_hidden'] = null;
		$vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting'); //BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=pdf_press'.AMP.'method=save_setting';
		$vars['data'] = $this->_new_setting_form();
		$vars['preset_page_title'] = lang('New Preset:');

		return array(
			'body' => ee()->load->view('setting-form', $vars, TRUE),
			'breadcrumb' => array(
				ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
			),
			'heading' => lang('pdf_press_module_name'),
				'sidebar' => $this->sidebar
		);
	}

	function save_setting() {

		//validation?
		ee()->load->helper(array('form', 'url'));
		ee()->load->library('form_validation');
		ee()->form_validation->set_rules('key', lang('key'), 'required');

		$config_data = array(
			'key'			=> ee()->input->post('key'),
			'attachment'	=> ee()->input->post('attachment'),
			'orientation'	=> ee()->input->post('orientation'),
			'size'			=> ee()->input->post('size'),
			'filename'		=> ee()->input->post('filename'),
			'cache_enabled' => ee()->input->post('cache_enabled'),
			'cache_ttl'		=> ee()->input->post('cache_ttl'),
			'encrypt'		=> ee()->input->post('encrypt'),
			'userpass'		=> ee()->input->post('userpass'),
			'ownerpass'		=> ee()->input->post('ownerpass'),
			'can_print'		=> ee()->input->post('can_print'),
			'can_modify'	=> ee()->input->post('can_modify'),
			'can_copy'		=> ee()->input->post('can_copy'),
			'can_add'		=> ee()->input->post('can_add'),
		);

		$id = ee()->input->post('id');

		$data = array(
			'site_id' => $this->site_id,
			'key'	=>  ee()->input->post('key'),
			'data'	=> json_encode($config_data)
		);

		if (ee()->form_validation->run() == FALSE)
		{
			ee()->load->library('table');
			ee()->load->library('javascript');

			$vars['form_hidden'] = null;
			$vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting'); //BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=pdf_press'.AMP.'method=save_setting';

			$row = (object) array('id' => $id, 'key' => ee()->input->post('key'), 'data' => json_encode($config_data));
			$vars['data'] = $this->_edit_setting_form($row);
			$vars['preset_page_title'] = lang('Edit Preset: '.$row->key);

			ee()->session->set_flashdata('message_failure',lang('setting_form_error'));

			return ee()->load->view('setting-form', $vars, TRUE);
		}
		else
		{

			if($id == "") {
				ee()->db->insert('pdf_press_configs', $data);
			}
			else {
				ee()->db->where('id', $id);
				ee()->db->update('pdf_press_configs', $data);
			}

			ee()->functions->redirect(ee('CP/URL', 'addons/settings/pdf_press/settings'));
		}
	}

	function _new_setting_form() {
		$paper_sizes = array_keys($this->_get_paper_sizes());
		$sizes = array();

		foreach($paper_sizes as $index => $size) {
			$sizes[$size] = $size;
		}

        $query = ee()->db->get_where('pdf_press_options', array('site_id' => $this->site_id));
        $row = $query->row();
        $options = json_decode($row->data, true);
        $default_paper_size = $options['dompdf_default_paper_size'];

		$data = array(
			'key'			=> form_hidden('id', '').form_input('key', ''),
			'attachment'	=> "<span style='padding-left:10px;'>".form_radio('attachment', '1', TRUE)." ".lang('Download')."</span><span style='padding-left:10px;'>".form_radio('attachment', '0', FALSE)." ".lang('Browser')."</span>",//form_input('attachment', ''),
			'size'			=> form_dropdown('size', $sizes, $default_paper_size), //form_input('member_email', $query->row('member_email')),
			'orientation'	=> "<span style='padding-left:10px;'>".form_radio('orientation', 'portrait', TRUE)." ".lang('Portrait')."</span><span style='padding-left:10px;'>".form_radio('orientation', 'landscape', FALSE)." ".lang('Landscape')."</span>",//form_input('orientation', ''),
			'filename'		=> form_input('filename', ''),
			'cache_enabled' => "<span style='padding-left:10px;'>".form_radio('cache_enabled', '1', FALSE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('cache_enabled', '0', TRUE)." ".lang('No')."</span>",//form_checkbox('encrypt','yes'),
			'cache_ttl'		=> form_input('cache_ttl', ''),
			''				=> lang('encrypt_description'),
			'encrypt'		=> "<span style='padding-left:10px;'>".form_radio('encrypt', '1', FALSE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('encrypt', '0', TRUE)." ".lang('No')."</span>",//form_checkbox('encrypt','yes'),
			'userpass'		=> form_password('userpass', ''),
			'ownerpass'		=> form_password('ownerpass', ''),
			'can_print'		=> "<span style='padding-left:10px;'>".form_radio('can_print', '1', TRUE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_print', '0', FALSE)." ".lang('No')."</span>", //form_checkbox('can_print', 'yes'),
			'can_modify'	=> "<span style='padding-left:10px;'>".form_radio('can_modify', '1', TRUE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_modify', '0', FALSE)." ".lang('No')."</span>", //form_checkbox('can_modify', 'yes'),
			'can_copy'		=> "<span style='padding-left:10px;'>".form_radio('can_copy', '1', TRUE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_copy', '0', FALSE)." ".lang('No')."</span>", //form_checkbox('can_copy', 'yes'),
			'can_add'		=> "<span style='padding-left:10px;'>".form_radio('can_add', '1', TRUE)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_add', '0', FALSE)." ".lang('No')."</span>", //form_checkbox('can_add', 'yes'),
		);

		return $data;
	}

	function _edit_setting_form($row) {
		$paper_sizes = array_keys($this->_get_paper_sizes());
		$sizes = array();

		$setting_data = json_decode($row->data, true);

		foreach($paper_sizes as $index => $size) {
			$sizes[$size] = $size;
		}

		$data = array(
			'key'			=> form_hidden('id', $row->id).form_input('key', $row->key)."<br/><p><strong style='color:red'>".form_error('key')."</strong></p>",
			'attachment'	=> "<span style='padding-left:10px;'>".form_radio('attachment', '1', ($setting_data['attachment'] == 1 ? TRUE : FALSE) )." ".lang('Download')."</span><span style='padding-left:10px;'>".form_radio('attachment', '0', ($setting_data['attachment'] == 0 ? TRUE : FALSE))." ".lang('Browser')."</span>",
			'size'			=> form_dropdown('size', $sizes, $setting_data['size'] ),
			'orientation'	=> "<span style='padding-left:10px;'>".form_radio('orientation', 'portrait', ($setting_data['orientation'] == 'portrait' ? TRUE : FALSE) )." ".lang('Portrait')."</span><span style='padding-left:10px;'>".form_radio('orientation', 'landscape', ($setting_data['orientation'] == 'landscape' ? TRUE : FALSE) )." ".lang('Landscape')."</span>",
			'filename'		=> form_input('filename', $setting_data['filename']),
			'cache_enabled' => "<span style='padding-left:10px;'>".form_radio('cache_enabled', '1', ($setting_data['cache_enabled'] == 1 ? TRUE : FALSE))." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('cache_enabled', '0', ($setting_data['cache_enabled'] == 0 ? TRUE : FALSE))." ".lang('No')."</span>",//form_checkbox('encrypt','yes'),
			'cache_ttl'		=> form_input('cache_ttl', $setting_data['cache_ttl']),
			''				=> lang('encrypt_description'),
			'encrypt'		=> "<span style='padding-left:10px;'>".form_radio('encrypt', '1', ($setting_data['encrypt'] == 1 ? TRUE : FALSE) )." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('encrypt', '0', ($setting_data['encrypt'] == 0 ? TRUE : FALSE) )." ".lang('No')."</span>",
			'userpass'		=> form_password('userpass', $setting_data['userpass']),
			'ownerpass'		=> form_password('ownerpass', $setting_data['ownerpass']),
			'can_print'		=> "<span style='padding-left:10px;'>".form_radio('can_print', '1', ($setting_data['can_print'] == 1 ? TRUE : FALSE) )." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_print', '0', ($setting_data['can_print'] == 0 ? TRUE : FALSE) )." ".lang('No')."</span>",
			'can_modify'	=> "<span style='padding-left:10px;'>".form_radio('can_modify', '1', ($setting_data['can_modify'] == 1 ? TRUE : FALSE) )." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_modify', '0',($setting_data['can_modify'] == 0 ? TRUE : FALSE) )." ".lang('No')."</span>",
			'can_copy'		=> "<span style='padding-left:10px;'>".form_radio('can_copy', '1', ($setting_data['can_copy'] == 1 ? TRUE : FALSE) )." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_copy', '0', ($setting_data['can_copy'] == 0 ? TRUE : FALSE) )." ".lang('No')."</span>",
			'can_add'		=> "<span style='padding-left:10px;'>".form_radio('can_add', '1', ($setting_data['can_add'] == 1 ? TRUE : FALSE) )." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_add', '0', ($setting_data['can_add'] == 0 ? TRUE : FALSE) )." ".lang('No')."</span>",
		);

		return $data;
	}

	function _edit_options_form($row) {
        $option_data = json_decode($row->data, true);

        $vars['sections'] = [
            [
                [
                    'title'     => 'dompdf_pdf_backend',
                    'desc'      => 'dompdf_pdf_backend_desc',
                    'fields'    => [
                        'dompdf_pdf_backend' => [
                            'type' => 'select',
                            'choices' => [
                                'auto' => 'auto',
                                'CPDF' => 'CPDF',
                                'GD'    => 'GD',
                                'PDFLib' => 'PDFLib',
                            ],
                            'value' => $option_data['dompdf_pdf_backend']
                        ]
                    ]
                ],
                [
                    'title'     => 'dompdf_pdflib_license',
                    'desc'      => 'dompdf_pdflib_license_desc',
                    'fields'    => [
                        'dompdf_pdflib_license' => ['type' => 'text', 'value' => $option_data['dompdf_pdflib_license'] ]
                    ]
                ],
                [
                    'title'     => 'dompdf_default_media_type',
                    'desc'      => 'dompdf_default_media_type_desc',
                    'fields'    => [
                        'dompdf_default_media_type' => ['type' => 'text', 'value' => $option_data['dompdf_default_media_type']]
                    ]
                ],
                [
                    'title'     => 'dompdf_default_paper_size',
                    'desc'      => 'dompdf_default_paper_size_desc',
                    'fields'    => [
                        'dompdf_default_paper_size' => [
                            'type' => 'select',
                            'choices' => $this->_return_paper_size_choices(),
                            'value'   => $option_data['dompdf_default_paper_size']
                        ]
                    ]
                ],
                [
                    'title'     => 'dompdf_default_paper_orientation',
                    'desc'      => 'dompdf_default_paper_orientation_desc',
                    'fields'    => [
                        'dompdf_default_paper_orientation' => [
                            'type' => 'inline_radio',
                            'choices' => [
                                'portrait' => 'Portrait',
                                'landscape' => 'Landscape'
                            ],
                            'value' => $option_data['dompdf_default_paper_orientation']
                        ]
                    ]
                ],
                [
                    'title'     => 'dompdf_base_path',
                    'desc'      => 'dompdf_base_path_desc',
                    'fields'    => [
                        'dompdf_base_path'  => ['type'=>'text', 'value' => $option_data['dompdf_base_path']]
                    ]
                ],
                [
                    'title'     => 'dompdf_enable_fontsubsetting',
                    'desc'      => 'dompdf_enable_fontsubsetting_desc',
                    'fields'    => [
                        'dompdf_enable_fontsubsetting' => ['type' => 'yes_no', 'value' => $option_data['dompdf_enable_fontsubsetting']]
                    ]
                ],
                [
                    'title'     => 'dompdf_enable_html5parser',
                    'desc'      => 'dompdf_enable_html5parser_desc',
                    'fields'    => [
                        'dompdf_enable_html5parser' => ['type' => 'yes_no', 'value' => $option_data['dompdf_enable_html5parser']]
                    ]
                ],
                [
                    'title'     => 'dompdf_enable_php',
                    'desc'      => 'dompdf_enable_php_desc',
                    'fields'    => [
                        'dompdf_enable_php' => ['type' => 'yes_no', 'value' => $option_data['dompdf_enable_php']]
                    ]
                ],
                [
                    'title'     => 'dompdf_enable_javascript',
                    'desc'      => 'dompdf_enable_javascript_desc',
                    'fields'    => [
                        'dompdf_enable_javascript' => ['type' => 'yes_no', 'value' => $option_data['dompdf_enable_javascript']]
                    ]
                ],
                [
                    'title'     => 'dompdf_enable_remote',
                    'desc'      => 'dompdf_enable_remote_desc',
                    'fields'    => [
                        'dompdf_enable_remote' => ['type' => 'yes_no', 'value' => $option_data['dompdf_enable_remote']]
                    ]
                ],
                [
                    'title'     => 'dompdf_default_font',
                    'desc'      => 'dompdf_default_font_desc',
                    'fields'    => [
                        'dompdf_default_font' => ['type' => 'text', 'value' => $option_data['dompdf_default_font']]
                    ]
                ],
                [
                    'title'     => 'dompdf_dpi',
                    'desc'      => 'dompdf_dpi_desc',
                    'fields'    => [
                        'dompdf_dpi' => ['type' => 'text', 'value' => $option_data['dompdf_dpi']]
                    ]
                ],
            ],
            'debug_options' => [
                [
                    'title'     => 'debug_layout',
                    'desc'      => 'debug_layout_desc',
                    'fields'    => [
                        'debug_layout' => ['type' => 'yes_no', 'value' => $option_data['debug_layout']]
                    ]
                ],
                [
                    'title'     => 'debug_layout_blocks',
                    'desc'      => 'debug_layout_blocks_desc',
                    'fields'    => [
                        'debug_layout_blocks' => ['type' => 'yes_no', 'value' => $option_data['debug_layout_blocks']]
                    ]
                ],
                [
                    'title'     => 'debug_layout_inline',
                    'desc'      => 'debug_layout_inline_desc',
                    'fields'    => [
                        'debug_layout_inline' => ['type' => 'yes_no', 'value' => $option_data['debug_layout_inline']]
                    ]
                ],
                [
                    'title'     => 'debug_layout_lines',
                    'desc'      => 'debug_layout_lines_desc',
                    'fields'    => [
                        'debug_layout_lines' => ['type' => 'yes_no', 'value' => $option_data['debug_layout_lines']]
                    ]
                ],
                [
                    'title'     => 'debug_layout_paddingbox',
                    'desc'      => 'debug_layout_paddingbox_desc',
                    'fields'    => [
                        'debug_layout_paddingbox' => ['type' => 'yes_no', 'value' => $option_data['debug_layout_paddingbox']]
                    ]
                ],
                [
                    'title'     => 'debugcss',
                    'desc'      => 'debugcss_desc',
                    'fields'    => [
                        'debugcss' => ['type' => 'yes_no', 'value' => $option_data['debugcss']]
                    ]
                ],
                [
                    'title'     => 'debugkeeptemp',
                    'desc'      => 'debugkeeptemp_desc',
                    'fields'    => [
                        'debugkeeptemp' => ['type' => 'yes_no', 'value' => $option_data['debugkeeptemp']]
                    ]
                ],
                [
                    'title'     => 'debugpng',
                    'desc'      => 'debugpng_desc',
                    'fields'    => [
                        'debugpng' => ['type' => 'yes_no', 'value' => $option_data['debugpng']]
                    ]
                ],
            ]
        ];

        $vars += array(
            'base_url' => ee('CP/URL', 'addons/settings/pdf_press'),
            'cp_page_title' => lang('edit_options'),
            'save_btn_text' => 'btn_save_options',
            'save_btn_text_working' => 'btn_saving'
        );

        return $vars;
    }

	function _get_paper_sizes() {
		$PAPER_SIZES = array(
		    "4a0" => array(0,0,4767.87,6740.79),
		    "2a0" => array(0,0,3370.39,4767.87),
		    "a0" => array(0,0,2383.94,3370.39),
		    "a1" => array(0,0,1683.78,2383.94),
		    "a2" => array(0,0,1190.55,1683.78),
		    "a3" => array(0,0,841.89,1190.55),
		    "a4" => array(0,0,595.28,841.89),
		    "a5" => array(0,0,419.53,595.28),
		    "a6" => array(0,0,297.64,419.53),
		    "a7" => array(0,0,209.76,297.64),
		    "a8" => array(0,0,147.40,209.76),
		    "a9" => array(0,0,104.88,147.40),
		    "a10" => array(0,0,73.70,104.88),
		    "b0" => array(0,0,2834.65,4008.19),
		    "b1" => array(0,0,2004.09,2834.65),
		    "b2" => array(0,0,1417.32,2004.09),
		    "b3" => array(0,0,1000.63,1417.32),
		    "b4" => array(0,0,708.66,1000.63),
		    "b5" => array(0,0,498.90,708.66),
		    "b6" => array(0,0,354.33,498.90),
		    "b7" => array(0,0,249.45,354.33),
		    "b8" => array(0,0,175.75,249.45),
		    "b9" => array(0,0,124.72,175.75),
		    "b10" => array(0,0,87.87,124.72),
		    "c0" => array(0,0,2599.37,3676.54),
		    "c1" => array(0,0,1836.85,2599.37),
		    "c2" => array(0,0,1298.27,1836.85),
		    "c3" => array(0,0,918.43,1298.27),
		    "c4" => array(0,0,649.13,918.43),
		    "c5" => array(0,0,459.21,649.13),
		    "c6" => array(0,0,323.15,459.21),
		    "c7" => array(0,0,229.61,323.15),
		    "c8" => array(0,0,161.57,229.61),
		    "c9" => array(0,0,113.39,161.57),
		    "c10" => array(0,0,79.37,113.39),
		    "ra0" => array(0,0,2437.80,3458.27),
		    "ra1" => array(0,0,1729.13,2437.80),
		    "ra2" => array(0,0,1218.90,1729.13),
		    "ra3" => array(0,0,864.57,1218.90),
		    "ra4" => array(0,0,609.45,864.57),
		    "sra0" => array(0,0,2551.18,3628.35),
		    "sra1" => array(0,0,1814.17,2551.18),
		    "sra2" => array(0,0,1275.59,1814.17),
		    "sra3" => array(0,0,907.09,1275.59),
		    "sra4" => array(0,0,637.80,907.09),
		    "letter" => array(0,0,612.00,792.00),
		    "legal" => array(0,0,612.00,1008.00),
		    "ledger" => array(0,0,1224.00, 792.00),
		    "tabloid" => array(0,0,792.00, 1224.00),
		    "executive" => array(0,0,521.86,756.00),
		    "folio" => array(0,0,612.00,936.00),
		    "commercial #10 envelope" => array(0,0,684,297),
		    "catalog #10 1/2 envelope" => array(0,0,648,864),
		    "8.5x11" => array(0,0,612.00,792.00),
		    "8.5x14" => array(0,0,612.00,1008.0),
		    "11x17"  => array(0,0,792.00, 1224.00),
            "dl" => array(0,0,283.47,595.28),
		  );
		return $PAPER_SIZES;
	}

	function _return_paper_size_choices() {
	    return [
            "4a0" => "4a0",
            "2a0" => "2a0",
            "a0" => "a0",
            "a1" => "a1",
            "a2" => "a2",
            "a3" => "a3",
            "a4" => "a4",
            "a5" => "a5",
            "a6" => "a6",
            "a7" => "a7",
            "a8" => "a8",
            "a9" => "a9",
            "a10" => "a10",
            "b0" => "b0",
            "b1" => "b1",
            "b2" => "b2",
            "b3" => "b3",
            "b4" => "b4",
            "b5" => "b5",
            "b6" => "b6",
            "b7" => "b7",
            "b8" => "b8",
            "b9" => "b9",
            "b10" => "b10",
            "c0" => "c0",
            "c1" => "c1",
            "c2" => "c2",
            "c3" => "c3",
            "c4" => "c4",
            "c5" => "c5",
            "c6" => "c6",
            "c7" => "c7",
            "c8" => "c8",
            "c9" => "c9",
            "c10" => "c10",
            "ra0" => "ra0",
            "ra1" => "ra1",
            "ra2" => "ra2",
            "ra3" => "ra3",
            "ra4" => "ra4",
            "sra0" => "sra0",
            "sra1" => "sra1",
            "sra2" => "sra2",
            "sra3" => "sra3",
            "sra4" => "sra4",
            "letter" => "letter",
            "legal" => "legal",
            "ledger" => "ledger",
            "tabloid" => "tabloid",
            "executive" => "executive",
            "folio" => "folio",
            "commercial #10 envelope" => "commercial #10 envelope",
            "catalog #10 1/2 envelope" => "catalog #10 1/2 envelope",
            "8.5x11" => "8.5x11",
            "8.5x14" => "8.5x14",
            "11x17" => "11x17",
            "dl" => "dl",
        ];
    }

	function _get_settings($site_id, $perpage, $rownum) {
		ee()->db->select('id, site_id, key, data');

		if($perpage > 0)
			$query = ee()->db->get_where('pdf_press_configs', array('site_id' => $site_id), $perpage, $rownum);
		else
			$query = ee()->db->get_where('pdf_press_configs', array('site_id' => $site_id));

		return $query;
	}

	function pagination_config($method, $total_rows) {
		//pass data to paginate class
		$config = array();
		$config['base_url'] = ee('CP/URL', "addons/settings/pdf_press/$method");//BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=pdf_press'.AMP.'method='.$method;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $this->perpage;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'rownum';
		$config['full_tag_open'] = '<p id="paginationLinks">';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '<img src="'.ee()->cp->cp_theme_url.'images/pagination_prev_button.gif" width="13" height="13" alt="<" />';
		$config['next_link'] = '<img src="'.ee()->cp->cp_theme_url.'images/pagination_next_button.gif" width="13" height="13" alt=">" />';
		$config['first_link'] = '<img src="'.ee()->cp->cp_theme_url.'images/pagination_first_button.gif" width="13" height="13" alt="< <" />';
		$config['last_link'] = '<img src="'.ee()->cp->cp_theme_url.'images/pagination_last_button.gif" width="13" height="13" alt="> >" />';

		return $config;
	}
}

