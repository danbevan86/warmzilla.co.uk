<?php
/*
=====================================================
PDF Press
-----------------------------------------------------
 http://www.anecka.com/pdf_press
-----------------------------------------------------
 Copyright (c) 2013 Patrick Pohler
=====================================================
 This software is based upon and derived from
 ExpressionEngine software protected under
 copyright dated 2004 - 2012. Please see
 http://expressionengine.com/docs/license.html
=====================================================
 File: mod.pdf_press.php
-----------------------------------------------------
 Dependencies: dompdf/
-----------------------------------------------------
 Purpose: Allows an EE template to be saved as a PDF
=====================================================
*/

require 'vendor/autoload.php';
use Dompdf\Dompdf;

if (! defined('BASEPATH')) exit('No direct script access allowed');

require PATH_THIRD."pdf_press/config.php";

class Pdf_press {
	var $site_id = 1;
	var $cache_ttl = 3600;
	var $cache_path = "";
	var $default_paper_size = "";

	function __construct() {
		$this->site_id = ee()->config->item('site_id');
		ee()->lang->loadfile('pdf_press');

        $query = ee()->db->get_where('pdf_press_options', array('site_id' => $this->site_id));
        $row = $query->row();
        $opts = json_decode($row->data, true);

        $this->default_paper_size = $opts['dompdf_default_paper_size'];

		//Make sure we have the cache directory
		$this->_check_cache_dir();
	}

	public function save_to_pdf_form() {
		/* this method is deprecated, please use save_to_pdf or parse_pdf instead */
		ee()->output->show_user_error('general', $errors);
	}

	public function save_to_pdf() {
		$path = ee()->TMPL->fetch_param('path', ee()->uri->uri_string());
		$attachment = ee()->TMPL->fetch_param('attachment', '1');
		$compress = ee()->TMPL->fetch_param('compress', '1');
		$size = ee()->TMPL->fetch_param('size', $this->default_paper_size);
		$orientation = ee()->TMPL->fetch_param('orientation', 'portrait');
		$filename = ee()->TMPL->fetch_param('filename', '');
		$key = ee()->TMPL->fetch_param('key', '');

		$action_id = ee()->functions->fetch_action_id('Pdf_press', 'create_pdf');

		$add_query = "?";
		if(ee()->config->item('force_query_string') == 'y') $add_query = "";

		return ee()->functions->create_url("").$add_query."ACT=$action_id&path=".urlencode($path)."&size=".urlencode($size)."&orientation=$orientation&key=$key&attachment=$attachment&compress=$compress&filename=".urlencode($filename);
	}

	public function parse_pdf() {
		$settings = array(
			'attachment' 	=> ee()->TMPL->fetch_param('attachment', '1'),
			'compress'		=> ee()->TMPL->fetch_param('compress', '1'),
			'orientation' 	=> ee()->TMPL->fetch_param('orientation', 'landscape'),
			'size'			=> ee()->TMPL->fetch_param('size', $this->default_paper_size),
			'filename'		=> ee()->TMPL->fetch_param('filename', ''),
			'cache_enabled' => false,
			'cache_ttl' 	=> $this->cache_ttl,
			'encrypt'		=> false,
			'userpass'		=> '',
			'ownerpass'		=> '',
			'can_print'		=> true,
			'can_modify'	=> true,
			'can_copy'		=> true,
			'can_add'		=> true,
		);

		//get the key
		$key = ee()->TMPL->fetch_param('key', '');

		try {

			if($key != "") {
				//lookup the key & pull settings, if key not found then throw user error
				$data_settings = $this->_lookup_settings($key);
				//array merge the key with the user parameter overrides
				foreach($data_settings as $field => $value) {
					if($value != null && $value != "")
						$settings[$field] = $value;
				}
			}

			$html = $this->_render(ee()->TMPL->tagdata);

			$this->_generate_pdf($html, $settings, FALSE);
			exit;

		} catch (Exception $e) {
			$check_markup = ee()->lang->line('error_check_markup');
			$dompdf_error =  ee()->lang->line('dompdf_error');

			$errors = array($check_markup,
					$dompdf_error.$e->getMessage());
			ee()->output->show_user_error('general', $errors);
		}
	}

	public function create_pdf() {

		$settings = array(
			'attachment' 	=> ee()->input->get_post('attachment'),
			'compress'		=> ee()->input->get_post('compress'),
			'orientation' 	=> ee()->input->get_post('orientation'),
			'size'			=> urldecode(ee()->input->get_post('size')),
			'filename'		=> urldecode(ee()->input->get_post('filename')),
			'cache_enabled' => false,
			'cache_ttl' 	=> $this->cache_ttl,
			'encrypt'		=> false,
			'userpass'		=> '',
			'ownerpass'		=> '',
			'can_print'		=> true,
			'can_modify'	=> true,
			'can_copy'		=> true,
			'can_add'		=> true,
		);

		//get the key
		$key = ee()->input->get_post('key', '');
		$path = urldecode(ee()->input->get_post('path'));
		$filename = $settings['filename'];

		if($filename == "") {
			$filename = str_replace("/", "_", $path).".pdf";
			$settings['filename'] = $filename;
		}

		$full_url = ee()->functions->create_url($path);

		$html = $this->get_url_contents($full_url);

		if($key != "") {
			//lookup the key & pull settings, if key not found then throw user error
			$data_settings = $this->_lookup_settings($key);
			//array merge the key with the user parameter overrides
			foreach($data_settings as $field => $value) {
				if($value != null && $value != "")
					$settings[$field] = $value;
			}
		}

		try {
			$this->_generate_pdf($html, $settings, FALSE, $full_url);
			exit;

		} catch (Exception $e) {
			$check_markup = ee()->lang->line('error_check_markup');
			$dompdf_error =  ee()->lang->line('dompdf_error');

			$errors = array($check_markup,
					$dompdf_error.$e->getMessage());
			ee()->output->show_user_error('general', $errors);
		}
	}

	public function create_pdf_ext($path, $pdf_settings = array(), $key = "") {
		$full_url = ee()->functions->create_url($path);
		$html = $this->get_url_contents($full_url);

		if($key != "") {
			$data_settings = $this->_lookup_settings($key);
			//array merge the key with the user parameter overrides
			foreach($data_settings as $field => $value) {
				if($value != null && $value != "")
				$pdf_settings[$field] = $value;
			}
		}

		return $this->_generate_pdf($html, $pdf_settings, TRUE);
	}

	public function physical_path() {
		$to_path = ee()->TMPL->fetch_param('to', '');
		return realpath($to_path);
	}

	private function _lookup_settings($key) {
		ee()->db->select('id, key, data');
		ee()->db->where("key", $key);
		ee()->db->where('site_id', $this->site_id);

		$query = ee()->db->get("pdf_press_configs");

		if($query->num_rows() > 0) {
			$row = $query->row();
			$setting_data = json_decode($row->data, true);
			$settings = array(
				'attachment' 	=> $setting_data['attachment'],
				'orientation' 	=> $setting_data['orientation'],
				'size'			=> $setting_data['size'],
				'filename'		=> $setting_data['filename'],
				'cache_enabled' => $setting_data['cache_enabled'],
				'cache_ttl' 	=> $setting_data['cache_ttl'],
				'encrypt'		=> $setting_data['encrypt'],
				'userpass'		=> $setting_data['userpass'],
				'ownerpass'		=> $setting_data['ownerpass'],
				'can_print'		=> $setting_data['can_print'],
				'can_modify'	=> $setting_data['can_modify'],
				'can_copy'		=> $setting_data['can_copy'],
				'can_add'		=> $setting_data['can_add'],
			);

			return $settings;
		} else {

			$errors = array(lang('no_setting_found'), "missing setting preset: '$key'");
			ee()->output->show_user_error('general', $errors);

			//throw new Exception(lang('no_setting_found'));
		}
	}

	private function _generate_pdf($html, $settings, $return_output = FALSE, $path = '') {
		$cache_key = $this->get_cache_key_from_uri($path);
		$md5_key = md5($cache_key);
		$pdf_path = ee()->cache->get($md5_key);
		$filename = $settings['filename'];

		if($settings['cache_enabled']){
			if($pdf_path == '' && !file_exists($pdf_path)){
				$dompdf = $this->_init_new_dompdf($html, $settings);
				$pdf = $dompdf->output();

				if($return_output)
					return $pdf;

				$pdf_path = $this->get_cache_file_path($md5_key);
				if($settings['cache_ttl'] != '' && $settings['cache_ttl'] > 0)
					ee()->cache->save($md5_key, $pdf_path, intval($settings['cache_ttl']));
				else
					ee()->cache->save($md5_key, $pdf_path);

				file_put_contents($pdf_path, $pdf);
			}

			$this->_stream_pdf_from_filesystem($pdf_path, $filename, $settings['attachment']);
		} else {
			$dompdf = $this->_init_new_dompdf($html, $settings);

			if($return_output)
				return $dompdf->output();

			$options = array();

			if($settings['attachment'] != '') {
				$options['Attachment'] = $settings['attachment'];
			}

			if($settings['compress'] != '') {
				$options['compress'] = $settings['compress'];
			}

			if(sizeof($options) > 0) {
				$dompdf->stream($settings['filename'], $options);
			} else {
				$dompdf->stream($settings['filename']);	
			}
		}
	}

	private function _init_new_dompdf($html, $settings){
        $query = ee()->db->get_where('pdf_press_options', array('site_id' => $this->site_id));
        $row = $query->row();
        $opts = json_decode($row->data, true);

        $options = new \Dompdf\Options();
        $options->set('defaultMediaType', $opts['dompdf_default_media_type']);
        $options->set('defaultPaperSize', $opts['dompdf_default_paper_size']);
        $options->set('defaultPaperOrientation', $opts['dompdf_default_paper_orientation']);
        $options->set('defaultFont', $opts['dompdf_default_font']);
        $options->set('dpi', $opts['dompdf_dpi']);
        $options->set('isPhpEnabled', $opts['dompdf_enable_php']);
        $options->set('isRemoteEnabled', $opts['dompdf_enable_remote']);
        $options->set('isJavascriptEnabled', $opts['dompdf_enable_javascript']);
        $options->set('isHtml5ParserEnabled', $opts['dompdf_enable_html5parser']);
        $options->set('isFontSubsettingEnabled', $opts['dompdf_enable_fontsubsetting']);
        $options->set('debugPng', $opts['debugpng']);
        $options->set('debugCss', $opts['debugcss']);
        $options->set('debugLayout', $opts['debug_layout']);
        $options->set('debugLayoutLines', $opts['debug_layout_lines']);
        $options->set('debugLayoutBlocks', $opts['debug_layout_blocks']);
        $options->set('debugLayoutInline', $opts['debug_layout_inline']);
        $options->set('debugLayoutPaddingBox', $opts['debug_layout_paddingbox']);
        $options->set('pdflibLicense', $opts['dompdf_pdflib_license']);

        //set fonts
		$options->set('fontDir', PATH_THIRD."/".PDF_PRESS_PACKAGE."/fonts");
        $options->set('fontCache',  PATH_THIRD."/".PDF_PRESS_PACKAGE."/fonts");

		$dompdf = new Dompdf($options);
		if(isset($opts['dompdf_base_path']) && $opts['dompdf_base_path'] != '') {
            $dompdf->setBasePath($opts['dompdf_base_path']);
		}

		$dompdf->loadHtml($html);
		$dompdf->setPaper($settings['size'], $settings['orientation']);
		$dompdf->render();

		if($settings['encrypt']) {
			$permissions = array();

			if($settings['can_print'])
				$permissions[] = 'print';

			if($settings['can_modify'])
				$permissions[] = 'modify';

			if($settings['can_copy'])
				$permissions[] = 'copy';

			if($settings['can_add'])
				$permissions[] = 'add';

			$dompdf->getCanvas()->get_cpdf()->setEncryption($settings['userpass'], $settings['ownerpass'], $permissions);
		}

		return $dompdf;
	}

	private function _check_cache_dir(){
		$path = SYSPATH.'user/cache/'.ee()->config->item('site_short_name').'/'.PDF_PRESS_PACKAGE;
		if(!is_dir($path)){
			mkdir($path, 0755, true);
		}
		$this->cache_path = $path;
	}

	private function _stream_pdf_from_filesystem($path, $name, $attachment){
		if(strpos($name, ".pdf") === FALSE){
			$name .= ".pdf";
		}

		if($attachment){ //download
			header('Content-Type: application/pdf');
			header("Content-Transfer-Encoding: Binary");
			header("Content-disposition: attachment; filename=".$name);
			header('Accept-Ranges: bytes');
		} else { //open in browser
			header('Content-type: application/pdf');
			header("Content-Disposition: inline; filename=".$name);
			header('Content-Transfer-Encoding: Binary');
			header('Accept-Ranges: bytes');
		}

		ob_clean();
		flush();

		readfile($path);
	}

	private function get_cache_key_from_uri($url = ''){
		$key = '/'.PDF_PRESS_PACKAGE;

		if($url == ''){
			$key .= '/'.ee()->uri->uri_string();
		} else {
			$pos = strpos($url, "index.php");
			if($pos){
				$key .= substr($url, $pos + strlen("index.php"), strlen($url)-1);
			}
		}

		return $key;
	}

	private function get_cache_file_path($filename){
		return $this->cache_path.'/'.$filename.'.pdf';
	}

	private function get_url_contents($url) {
		$fopen_enabled = ini_get('allow_url_fopen');
		/* gets the data from a URL */
		if($fopen_enabled) {
			return file_get_contents($url);
		} else if($this->curl_installed()) {
			//if fopen isn't enabled, then go get the html using curl
			$ch = curl_init();
			$timeout = 5;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		} else {
			ee()->output->show_user_error('general', ee()->lang->line('error_curl_fopen'));
		}

	}

	//cool function from Veno Designs to render the template in its own context
	//http://venodesigns.net/tag/expressionengine/
	private function _render($text, $opts = array()) {
        /* Create a new EE Template Instance */
        //ee()->TMPL = new EE_Template();

        /* Run through the initial parsing phase, set output type */
        ee()->TMPL->parse($text, FALSE);
		ee()->TMPL->final_template = ee()->TMPL->parse_globals(ee()->TMPL->final_template);
        ee()->output->out_type = ee()->TMPL->template_type;

        /* Return source. If we were given opts to do template replacement, parse them in */
        if(count($opts) > 0) {
            ee()->output->set_output(
                ee()->TMPL->parse_variables(
                    ee()->TMPL->final_template, array($opts)
                )
            );
        } else {
            ee()->output->set_output(ee()->TMPL->final_template);
        }
		return ee()->output->final_output;
    }

	private function curl_installed(){
	    return function_exists('curl_version');
	}
}

?>
