<?php

/*
=====================================================
PDF Press
-----------------------------------------------------
 http://www.anecka.com/pdf_press
-----------------------------------------------------
 Copyright (c) 2013 Patrick Pohler
=====================================================
 This software is based upon and derived from
 ExpressionEngine software protected under
 copyright dated 2004 - 2012. Please see
 http://expressionengine.com/docs/license.html
=====================================================
 File: upd.pdf_press.php
-----------------------------------------------------
 Dependencies: dompdf/
-----------------------------------------------------
 Purpose: Allows an EE template to be saved as a PDF
=====================================================
*/

require PATH_THIRD."pdf_press/config.php";

class Pdf_press_upd {

    var $site_id = 1;
    var $version = PDF_PRESS_VERSION;

    function __construct() {
        // Make a local reference to the ExpressionEngine super object
        $this->site_id = ee()->config->item('site_id');
    }

	function install() {
		ee()->load->dbforge();

		$data = array(
			'module_name' => PDF_PRESS_NAME,
			'module_version' => PDF_PRESS_VERSION,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);

		ee()->db->insert('modules', $data);

		$this->install_actions();

		$this->install_configs();

		$this->install_options();

		return true;
	}

	function uninstall() {
		ee()->load->dbforge();
		ee()->db->select('module_id');

		$query = ee()->db->get_where('modules', array('module_name' => PDF_PRESS_NAME));

	    ee()->db->where('module_id', $query->row('module_id'));
	    ee()->db->delete('modules');

	    ee()->db->where('class', PDF_PRESS_NAME);
	    ee()->db->delete('actions');

		ee()->dbforge->drop_table('pdf_press_configs');

        ee()->dbforge->drop_table('pdf_press_options');

		return TRUE;
	}

	function install_actions() {
		$actions = array(
			array(
				'class'		=> PDF_PRESS_NAME,
				'method'	=> 'create_pdf'
			),
		);

		ee()->db->insert_batch('actions', $actions);
	}

	function install_configs() {
		$fields = array(
			'id'		=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'auto_increment' => TRUE),
			'site_id'	=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE),
			'key'		=> array('type' => 'varchar', 'constraint' => '500'),
			'data' 		=> array('type' => 'text', 'null' => true)
		);

		ee()->dbforge->add_field($fields);
		ee()->dbforge->add_key('id', TRUE);

		ee()->dbforge->create_table('pdf_press_configs');
	}


	function update($current = '')
	{
		ee()->load->dbforge();

		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}

		if( version_compare($current, '1.5', '<') ) {
			$this->install_configs();
			return TRUE;
		}

		if(version_compare($current, '3.5.0', '<')) {
			$field = array(
                'site_id'	=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE),
            );

            ee()->dbforge->add_column('pdf_press_configs', $field);

			return TRUE;
		}

        if(version_compare($current, '4.0.0', '<')) {
            $this->install_options();

            return TRUE;
        }

	    return TRUE;
	}

	public function install_options() {
		$fields = array(
			'id'		=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'auto_increment' => TRUE),
			'site_id'	=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE),
			'data' 		=> array('type' => 'text', 'null' => true)
		);

		ee()->dbforge->add_field($fields);
		ee()->dbforge->add_key('id', TRUE);

		ee()->dbforge->create_table('pdf_press_options');

		$default_options = [
			'dompdf_pdf_backend' 			=> 'auto',
			'dompdf_default_media_type' 	=> 'pdf',
			'dompdf_default_paper_size' 	=> 'a4',
            'dompdf_default_paper_orientation'	=> 'portrait',
			'dompdf_default_font'			=> 'serif',
			'dompdf_dpi'					=> 96,
            'dompdf_pdflib_license'			=> '',
            'dompdf_base_path'              => $_SERVER['DOCUMENT_ROOT'],
			'dompdf_enable_fontsubsetting'	=> true,
			'dompdf_enable_html5parser'		=> true,
			'dompdf_enable_php'				=> false,
			'dompdf_enable_javascript'		=> false,
			'dompdf_enable_remote'			=> true,
			'debugpng'						=> false,
			'debugcss'						=> false,
			'debugkeeptemp'					=> false,
			'debug_layout'					=> false,
			'debug_layout_lines'			=> false,
			'debug_layout_blocks'			=> false,
			'debug_layout_inline'			=> false,
			'debug_layout_paddingbox'		=> false,
		];


		$options = [
			'site_id'	=> $this->site_id,
			'data'		=> json_encode($default_options)
		];

		ee()->db->insert('pdf_press_options', $options);
	}
}
