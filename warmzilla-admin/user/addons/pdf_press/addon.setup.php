<?php

return array(
      'author'          => 'Anecka',
      'author_url'      => 'http://anecka.com',
      'name'            => 'PDF Press',
      'description'     => 'Saves a template as a PDF, uses the dompdf library (https://github.com/dompdf/dompdf)',
      'version'         => '4.0.1',
      'namespace'       => 'Anecka\PDFPress',
      'settings_exist'  => TRUE,
      'docs_url'        => 'http://www.anecka.com/docs/PDF%20Press%20User%20Guide.html',
);
