#PDF Press for ExpressionEngine&reg; User Guide
***
*Author:* Patrick Pohler

*Version:* 4.0.1

*URL:* <https://gum.co/pdfpress>

*Docs:* [PDF Press User Guide](http://www.patpohler.com/wp-content/uploads/2017/10/PDF-Press-User-Guide.pdf), also under the docs folder in the zip file.
***
Thank you for your purchase! If you have any support questions about the module, please use our [offical support repo on GitHub](https://github.com/patpohler/pdf_press_issues/issues). 

If you have any questions about billing or your license, you can email us at [contact@anecka.com](mailto:contact@anecka.com)

***
##Table of Contents
1. Prerequisites
2. Installation & Upgrade
3. Module Tags
4. Formatting the PDF File
5. Setting Presets and Security
6. Caching 
7. Configuring PDF Press
8. PDF Preview
9. Unicode and Custom Fonts
10. Calling PDF Press from other add-ons
11. Tips and Troubleshooting
12. Support
13. Changelog 
 
***

##1. Prerequisites

Make sure your system meets the following requirements:

- PHP 5.3+
- DOMDocument extension
- MBString extension
- GD
- php-font-lib
- php-svg-lib
- ExpressionEngine 3.0 or later
- Opcache and IMagick or GMagick (**recommended**)
- **allow_url_fopen enabled in php.ini is recommended for images & CSS but not required (see Tips & Troubleshooting 'Images or CSS stylesheet files not loading')**

***
##2. Installation & Upgrade

### Installation

1. Copy the `ee3/system/user/addons/pdf_press` folder to `system/user/addons/`

2. Install the module by going to the Add-On Manager, look for PDF Press and click `Install`

3. That's it! Go to the PDF Press module’s Control Panel (`Add-Ons > Modules > PDF Press > Configuration`) and verify that your environment is configured to generate PDFs.

### Upgrade

To upgrade PDF Press from a previous version, overwrite the files as specified in the installation instructions. Go to Add-On Manager and click the Update link.

***
##3. Module Tags

PDF Press has two tags, each will generate a PDF from an ExpressionEngine template.

###Tag `{exp:pdf_press:save_to_pdf}`

The `save_to_pdf` tag generates a link to download the template as a PDF, it contains the following parameters:

- **path** (optional): accepts an ExpressionEngine template path in the form `template_group/template` (ex. site/pdf), if no path is given then the current page will be used.

- **filename** (optional): sets the filename for the downloaded PDF

- **size** (optional): sets the paper size for the PDF (ex. `letter`, `legal`, `a5`, etc). You can view all of the available sizes by checking under the `Size` dropdown of the PDF Preview tab

- **orientation** (optional): sets the orientation of the page, either `portrait` **(default)** or `landscape`

- **attachment** (optional): if left black or set to `1`, the PDF will be downloaded, if the attachment parameter is set to `0` the PDF will be opened in the browser

- **key** : this is the setting preset shortcode, see **5. Settings Presets** for more information

####Example:

	<a href="{exp:pdf_press:save_to_pdf path='site/projects' attachment='0' filename='projects.pdf'}">Download Project Case Studies</a>

This will create a link to open a PDF titled "projects.pdf" in the browser using the `site/projects` template

***

###Tag `{exp:pdf_press:parse_pdf}`

Sometimes you'll want to create a PDF with a member only or logged in page. For this you want to use `parse_pdf`

	{exp:pdf_press:parse_pdf}
		<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<title>Parse Test</title>
		</head>
		<body>
			{if logged_in}
				{exp:channel:entries channel="Projects"}
					<p><strong>{title}</strong></p>
				{/exp:channel:entries}
			{if:else}
				<p>Not logged in</p>
			{/if}
		</body>
		</html>
	{/exp:pdf_press:parse_pdf}

As you can guess, this tag, instead of creating a link to download/open a PDF, will create a PDF using whatever HTML & EE tags that are inside the `parse_pdf` tag-pair. It has all of the same parameters as `save_to_pdf` (except for `path`)

**Note: Remember to put the parse_pdf tag before and after the HTML tag (and DOCTYPE) otherwise PDF Press won't render your complete page)**

***
##4. Formatting the PDF File

Formatting the PDF file is as simple as creating a special CSS file and referencing it in the `<head>` tag in your template. Use the `media` attribute and set it to **"pdf"**. This will cause the PDF Press to use the stylesheet (and it will be ingnored in your browser)

	<link href="{site_url}css/pdf.css" rel="stylesheet" media="pdf">

If you want to hide your main stylesheet from PDF Press (which is very useful, CSS frameworks such as Bootstrap or Foundation can drastically slow down PDF Press), just set the `media` attribute to **"screen."**

	<link href="{site_url}css/site_only.css" rel="stylesheet" media="screen">
	<link href="{site_url}css/pdf.css" rel="stylesheet" media="pdf">

***
##5. Setting Presets and Security

Presets are a great way to save time, they allow you to set all of the options passed as parameters in the `save_to_pdf` & `parse_pdf` tags in the Control Panel. You can then re-use those settings in your template by just passing the short code into the "key" parameter.

	{exp:save_to_pdf key="product_settings"}

Presets also let you securely set a variety of security settings for your PDFs. You can enable encryption, set a owner password, user password and then control if users can save, print, or modify PDFs.

You can access the Setting Presets by going to `Add-Ons > Modules > PDF Press > Setting Presets`

***
##6. Caching PDF files

You can control if generated PDF files are cached using the "Cache" and "Cache Duration" settings in Setting Presets. PDF Press look at the `path=` parameter to build a unqiue hash and hold onto the generated file for set period of time (the default is 3600 seconds or one hour).

***
##7. Configuring PDF Press

There are a lot of settings you can configure in PDF Press. You can tweak PDF Press to your needs by going to `Add-ons > Modules > PDF Press > Configuration`
***
##8. PDF Preview

A helpful tool you can use to develop your PDF templates is the PDF Preview tab. You can find it under `Add-Ons > Modules > PDF Press > PDF Preview`

Specify the size, orientation and an ExpressionEngine template path (ex. `site/test`) and click **Submit**. The PDF (or other output if there’s an error) will be displayed below in an iframe. 
***
##9. Unicode and Custom Fonts

PDF Press uses the DOMPDF library, which supports a wide variety of unicode character sets. 

Previous versions of PDF Press required you to manually load fonts. Now all you need is to use @font-face in your CSS. DOMPDF will find and download the font you specify.

Unicode is automatically enabled. DOMPDF comes with a default unicode font, DejaVu to help get you started. 

	{exp:pdf_press:parse_pdf}
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<title>Unicode Test</title>
		</head>
		<body>
			<p style="font-family:'DejaVu Sans';">Лорем ипсум долор сит амет, цум но децоре долорес дефинитионем. Елитр легимус адмодум еа при, оффендит волуптатум хис ин, еи еос лорем суммо.</p>
		</body>
	</html>
	{/exp:pdf_press:parse_pdf}

Also make sure you’ve set the “Content-Type” to “utf-8” in your html template’s `<head>` tag.

##10. Calling PDF Press from other add-ons

You can now call PDF Press from other add-ons or modules to create PDFs by calling the `pdf_press_generate_pdf` extension hook. Here are the parameters for the hook:

- **path** (required): path to the ExpressionEngine template for the PDF layout
- **settings** (array, required): settings for how the pdf is generated, matches the parameters described in Section 3: Module Tags of this document
- **key** (optional): this is an option parameter for the setting preset

Here's an example of how you'd use the hook to create a pdf and save it to a file.

####Example:

    if ($this->EE->extensions->active_hook('pdf_press_generate_pdf') === TRUE)
    {
        $path = "pdf/".$entry_id; /* example of passing a channel entry to the pdf template */
        $settings = array(
            'orientation' 	=> 'portrait',
            'size'			=> 'letter',
            'compress'		=> '1',
            'encrypt'		=> false,
        );

        $pdf_output = $this->EE->extensions->call('pdf_press_generate_pdf', $path, $settings);

        $pdf_filename = APPPATH.'cache/entry_'.$entry->form_entry_id.'.pdf';
        file_put_contents($pdf_filename, $pdf_output);
    }

***
##11. Tips and Troubleshooting

### PHP Errors
The DOMPDF library is **VERY STRICT** when it parses the HTML & CSS of your template. The HTML5 parser is enabled by default, which isn't as strict but you still might get errors.  If you are having trouble getting your PDF to generate and you're getting PDF errors, run your HTML & CSS through a validator, such as the W3C Validation Service (<http://validator.w3.org>). **We recommend starting with very basic HTML template & CSS first, then add only the styles you need for your PDF.**

### Fonts aren't appearing 

DOMPDF downloads fonts to the /system/user/addons/pdf_press/fonts directory. Make sure that directory exists and is writable on the server. If it's empty, then DOMPDF can't find or write to the directory.

### Adding new paper sizes

You can add paper sizes to PDF Press by editing the `$PAPER_SIZES` array in the following files:

	third_party\pdf_press\dompdf\include\cpdf_adapter.cls.php, line 37
	third_party\pdf_press\mcp.pdf_press.php, line 536

Adding new paper size is as simple as adding a new row to the array (sizes are in pixels).

	$PAPER_SIZES = array(
		...
		 "dl" => array(0,0,283.47,595.28),
		...
	);

### PDF Preview

You can troubleshoot your template using the PDF Preview tab in the Control Panel. Just go to `Add-Ons > Modules > PDF Press > PDF Preview`

### Images or CSS stylesheet files not loading

If you're getting "image not found" errors or external stylesheet files not loading in your PDF document, first check to see if `allow_url_fopen` is enabled. You can do a quick check if you go to the **Configuration** page in the PDF Press control panel, look for the **DOMPDF_ENABLE_REMOTE** line and verify that it's set to "true" and the status is "allow_url_fopen_enabled."

If "allow_url_fopen" is disabled you can enabled it in your server's php.ini file. However if you can't enable "allow_url_fopen" because your hosting provider has it disabled or you can't access the php.ini you'll have to modify your PDF template to use physical file paths for images & CSS.

PDF Press comes with a tag `{exp:pdf_press:physical_path}` which will output the physical path to the public folder of your website. Using this tag you should be able to tell PDF Press where your images are. Here's an example.

Say your template has the following image :
	<img src="{site_url}uploads/my_folder/myimage.png">

Instead of using `{site_url}` you'll want to replace it with our physical path tag:

	<img src="{exp:pdf_press:physical_path to='uploads/my_folder/myimage.png'}">

If you leave out the **to=** parameter, the tag will just return the physical path to the website's public folder.

### A Blank White Screen
If you're using an `.htaccess` file to strip out **index.php** from the url, make sure your rewrite settings in `.htaccess` are correct and don't strip out the `?ACT=` parameter. We recommend using the following settings.

```
	RewriteEngine On
	RewriteBase /

	# Remove the trailing slash to paths without an extension
	RewriteCond %{REQUEST_METHOD} !=POST
	RewriteCond %{REQUEST_URI} /$
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.+)/$ $1 [L,R=301]

	# Removes index.php from ExpressionEngine URLs
	RewriteCond %{QUERY_STRING} !^(ACT=.*)$ [NC]
	RewriteCond %{REQUEST_URI} !(\.[a-zA-Z0-9]{1,5})$
	RewriteCond $1 !\.(gif|jpe?g|png)$ [NC]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$ /index.php/$1 [L]
```

If your .htaccess file has rewrite conditions like the following, then PDF Press will not work.

```
	# Removes index.php from ExpressionEngine URLs
	RewriteCond %{THE_REQUEST} ^GET.*index\.php [NC]
	RewriteCond %{REQUEST_URI} !/systemname/.* [NC]
	RewriteRule (.*?)index\.php/*(.*) /$1$2 [R=301,NE,L]
```

***
##11. Support
If you have any questions or concerns visit our [offical support repo on GitHub](https://github.com/patpohler/pdf_press_issues/issues)

***
##13. Changelog

See changelog.md in the zip file for the release notes/changelog
