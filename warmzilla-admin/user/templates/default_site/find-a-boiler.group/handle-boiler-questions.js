boilerQuestions = [];
$answerLabel = true;
var _debug = false;

$(document).ready(function() {
    getBoilerQuestions();

    $(document).on('click', '.execute-questions', function(event) {
        event.preventDefault();
        /* Act on the event */

        setProcessPage("start-now-screen", "ask-for-postcode", 0);
        if(_debug){console.log('progressbar update: 1');}
        updateProgressBar();

        $('.start-now-screen').slideUp();
        $('.ask-for-postcode').slideDown();
        /*$('.start-questions').slideDown();*/

    });

    $(document).on('click', '.edit-question', function(event) {
        event.preventDefault();
        setCookie('editQuestions', $(this).data('edit') + "|" + window.location.href, 1);
        window.location.href = "/find-a-boiler";
    });

    $(document).on('click', '.answer-box', function(event) {
        event.preventDefault();
        /* Act on the event */
        if(_debug){console.log("Answer box clicked.");}

        if($answerLabel === true)
        {
            if(_debug){console.log("Answer box click Accepted.");}
            $answerLabel = false;
        }
        else
        {
            if(_debug){console.log("Answer box click rejected.");}
            return false;
        }
        $(this).parents('.que-list:first').find('.answer-box').addClass('pointer-none')

        /*if(boilerQuestions[boilerQuestions.length-1] !== undefined)
        {
            if(boilerQuestions[boilerQuestions.length-1].selection === $(this).data('selection'))
            {
                if(_debug){console.log("Answer box click rejected. [Due to double click on answer box");}
                return false;
            }
        }*/
        currentSection = {
            selection           : $(this).data('selection'),
            next                : $(this).data('next'),
            final               : $(this).data('final'),
            progressBar         : $(this).parents('.que-sec:first').data('step'),
            entry               : $(this).parents('.que-sec:first').data('entry'),
            title               : $(this).parents('.que-sec:first').data('title'),
            answer              : $(this).data('answer'),
            incboilerprice      : $(this).data('incboilerprice'),
            incinstallcharge    : $(this).data('incinstallcharge'),

        };
        $pval = "";
        entryData = {};

        if(getCookie('editQuestions') != "")
        {
            entryData = getCookie('editQuestions').split("|");
            entry_id = entryData[0];
            for(i=0; i < boilerQuestions.length; i++)
            {
                if(entry_id == boilerQuestions[i].entry)
                {
                    if(_debug){console.log("Fount PVAL");}
                    $pval = i;
                    break;
                }
            }
        }
        else
        {
            if(_debug){console.log("No prev PVAL found");}
            boilerQuestions.push(currentSection);
        }

        setCookie('editQuestions', "false", -1);
        saveBoilerQuestions();

        $label = true;
        if($pval != "")
        {
            $label = false;
            boilerQuestions[$pval] = currentSection;
            saveBoilerQuestions();
            if(boilerQuestions[$pval+1] === undefined)
            {
                $label = true;
            }
            else if(boilerQuestions[$pval+1].entry == currentSection.next)
            {
                if(_debug){console.log("Edit single INDEPENDENT question is done. Redirecting now.");}
                if(entryData[1] !== undefined)
                {
                    window.location.href = entryData[1];
                }
                else
                {
                    window.location.href = "/find-a-boiler/quote-summary";
                }
            }
            else
            {
                if(_debug){console.log("After PVAL, It is next question mode (dependent)");}
                $label = true
                boilerQuestions.length = $pval + 1;
                saveBoilerQuestions();
            }
        }

        if($label == true)
        {
            if(currentSection.next != "")
            {
                if(_debug){console.log("In dependent mode, This will now render all other questions");}
                $(this).parents('.que-sec:first').slideUp();
                $('.question_' + currentSection.next).slideDown();
            }
            else
            {
                setFinalStep();
            }

            if(_debug){console.log('progressbar update: 2');}
            updateProgressBar();
        }
        $answerLabel = true;
    });

    $(document).on('click', '.back-btn', function(event) {
        event.preventDefault();
        /* Act on the event */
        lastQuery = boilerQuestions[boilerQuestions.length - 1];
        
        if(lastQuery.entry != "")
        {
            entry_id = lastQuery.entry;
            boilerQuestions.length = boilerQuestions.length - 1;
            saveBoilerQuestions();

            $('.que-sec:visible').slideUp();
            $('.question_' + entry_id).slideDown();
            $('.question_' + entry_id).find(".answer-box").removeClass('pointer-none');
        }
        else
        {
            _process = lastQuery.processFrom;
            boilerQuestions.length = boilerQuestions.length - 1;
            saveBoilerQuestions();

            $('.que-sec:visible').slideUp();
            $('.' + _process).slideDown();
        }
        if(_debug){console.log('progressbar update: 3');}
        updateProgressBar();

    });

    $(document).on('click', '.find-postcode', function(event) {
        event.preventDefault();
        /* Act on the event */
        if(! $(".poscode-form").valid()) {
            return false;
        }

        $('.ajax-loader').show();
        $('.find-postcode').attr('disabled', 'disabled');
        var _savedPostCode = $('.poscode-form').find('input[name="postcode"]').val().split(" ")[0];

        $.ajax({
            url: '/find-a-boiler/ajax-check-valid-poscode/' + _savedPostCode,
        })
        .success(function(data, status, xhr) {
            $('.ajax-loader').hide();
            $('.find-postcode').removeAttr('disabled');
            $('.poscode-form').find('input[name="postcode"]').val("")
            if(data == "1")
            {
                setCookie("boilerPostCode", _savedPostCode, 5);
                startQuestions();
            }
            else
            {
                setCookie("engineersArea", "no", 5);
                setCookie("boilerPostCode", _savedPostCode, 5);
                startQuestions();
                // showContactForm();
            }
        })
        .fail(function(a, b, c) {
            $('.ajax-loader').hide();
            $('.find-postcode').removeAttr('disabled');
            showContactForm();
        })
    });

    $(document).on('click', '.question_110 .answer-box', function(event) {
        var boilerQuestions = $.parseJSON(getCookie('boilerQuestions'));
        var backBoiler = boilerQuestions[2].answer;
        var airingCupboard = boilerQuestions[8].answer;
        if(backBoiler == "Back Boiler" || backBoiler == "Standard" ) {
            if(airingCupboard == "Middle of House") {
                $('[data-answer="Roof"]').click();
                $('[data-answer="Sloped"]').click();
                $('[data-answer="Top Two Thirds"]').click();
                $('.question_123').css({opacity: 0});
            } else if(airingCupboard == "Outside Wall") {
                $('[data-answer="Wall"]').click();
                $('[data-selection="88"]').click();
                $('[data-selection="86"]').click();
                $('[data-selection="82"]').click();
                $('[data-selection="84"]').click();
                $('.question_127').css({opacity: 0});
                $('.question_126').css({opacity: 0});
                $('.question_125').css({opacity: 0});
            }
        }
    });

    $(document).on('click', '.accept-restore', function(event) {
        event.preventDefault();
        boilerQuestions = [];
        saveBoilerQuestions();
        window.location.href = "/find-a-boiler";
    });
    
    $(document).on('click', '.accept-restore', function(event) {
        $('#confirm-modal').modal('toggle');
    });

    $('label.required').each(function(index, el) {
        if($(this).next('input').length)
        {
            $(this).next('input').addClass('required validate[required]');
        }
        /*$(this).parents('form:first').addClass('validation-form');*/
    });

    $(document).on('submit', '.validation-form', function(event) {
        $(this).find('ul.errors').remove();
        $(this).find('div.error-inner').remove();
        if(! $(this).valid())
        {
            return false;
        }
    });
});

function setProcessPage(from, to, progress)
{
	if(typeof(progress) === "undefined"){
    	progress = 0;
    }
    currentSection = {
        selection   : "",
        next        : "",
        final       : "",
        entry       : "",
        title       : "",
        progressBar : progress,
        processFrom : from,
        processTo   : to,
        answer      : "",
        incboilerprice : 0,
        incinstallcharge: 0
    };

    boilerQuestions.push(currentSection);
    saveBoilerQuestions();
}

function updateProgressBar($pval)
{
	if(typeof($pval) === "undefined"){
    	$pval = "";
    }
    if(boilerQuestions.length > 0)
    {
        if($pval != "")
        {
            _current = boilerQuestions[$pval];
        }
        else
        {
            _current = boilerQuestions[boilerQuestions.length - 1];
        }

        if(_current.progressBar == 0 || _current.progressBar == "")
        {
            $('.progress-bar-wrapper').hide();
        }
        else if(_current.final == "give_us_a_call")
        {
            $('.progress-bar-wrapper').hide();
        }
        else 
        {
            if($('.progress-bar-wrapper:visible').length == 0)
            {
                $('.progress-bar-wrapper').show();
            }
            _block = $('.progress-bar-wrapper').find(".wz-steps-wizard").children('.pbar-' + _current.progressBar);
            _block.removeClass('finished').addClass('active');
            _block.prevAll().addClass('finished').removeClass('active')
            _block.nextAll().removeClass('active finished');
            $('.progress-bar-wrapper').find(".wz-progress").removeClass().addClass('wz-progress active-' + _current.progressBar);
        }
    }
    else
    {
        $('.progress-bar-wrapper').hide();
    }

    if(_debug){console.log("On the right place at last");}
}

function performFreshBoilerQuestionary()
{
    boilerQuestions = [];
    saveBoilerQuestions();

    $('.start-now-screen').show();
    $('.boiler-question').hide();
    updateProgressBar()
}

function startQuestions()
{
    $('.que-sec:visible').slideUp();
    $('.start-questions').slideDown();
    if(boilerQuestions[boilerQuestions.length - 1] !== undefined)
    {
        boilerQuestions[boilerQuestions.length - 1].progressBar = 1;
    }
    updateProgressBar();
}

function showContactForm()
{
    setProcessPage("ask-for-postcode", "postcode-not-found", 0);
    $('.que-sec:visible').slideUp();
    $('.postcode-not-found').slideDown();
    updateProgressBar()
}

function retriveFromLastQuestion()
{
    $pval = "";
    entry_id = "";
    if(getCookie('editQuestions') != "")
    {
        entryData = getCookie('editQuestions').split("|");
        entry_id = entryData[0];

        for(i=0; i < boilerQuestions.length; i++)
        {
            if(entry_id == boilerQuestions[i].entry)
            {
                $pval = i;
                break;
            }
        }

        if($pval == "")
        {
            entry_id = "";
        }
    }
    
    if(entry_id == "")
    {
        entry_id = boilerQuestions[boilerQuestions.length - 1].next;
    }

    if(entry_id != "")
    {
        $('.find-a-boiler-sec').hide();
        $('.start-now-screen').hide();
        $('.question_' + entry_id).show();
    }
    else
    {
        setFinalStep();
    }
    if(_debug){console.log('progressbar update: 4');}
    updateProgressBar($pval);
}


function saveBoilerQuestions()
{
    var temp = JSON.stringify(boilerQuestions)
    setCookie("boilerQuestions", temp, 5);
    if(_debug){console.log("Boiler Questions saved successfully.");}
}

function getBoilerQuestions()
{
    temp = getCookie("boilerQuestions");
    if(temp == "" || temp.length == 0)
    {
        if(_debug){console.log('No questions found. Setting it to 1st question.');}
        performFreshBoilerQuestionary();
        return false;
    }
    else
    {
        boilerQuestions = JSON.parse(temp);
        if(boilerQuestions.length == 0)
        {
            if(_debug){console.log('Questions array found blank. Setting it to 1st question.');}
            performFreshBoilerQuestionary();
            return false;
        }
        else
        {
            if(_debug){console.log('Find boiler questions.');}
            retriveFromLastQuestion();
        }
    }
}

function setFinalStep()
{
    temp = boilerQuestions[boilerQuestions.length - 1];

    if(temp.next != "")
    {
        $(this).parents('.que-sec:visible').slideUp();
        $('.question_' + temp.next).slideDown();
    }
    else
    {
        if(temp.final != "")
        {
            switch(temp.final) {
                case "summary":
                    handleSummaryPage();
                    break;

                case "give_us_a_call":
                    $('.que-sec:visible').slideUp();
                    $('.give-a-call').show();
                    $('.give-us-call').slideDown();
                    break;

                default:
                    alert("You are in the wrong window. Are you sure you did create entry properly?");
            }
        }
        else
        {
            $('.que-sec:visible').hide();
            $("." + temp.processTo).show();
        }
    }
}

function handleSummaryPage()
{
    myURL = window.location.href;
    myURL = myURL.replace("http://", "");
    myURL = myURL.replace("https://", "");
    var _url = myURL.split("/");
    if( _url[1] == "my-order" && _url[2] == "finalise-order" )
    {
        boilerQuestions[boilerQuestions.length - 1].progressBar = 6;
        updateProgressBar();
    }
    else if( _url[1] == "my-order" && _url[2] == "select-install-date" )
    {
        boilerQuestions[boilerQuestions.length - 1].progressBar = 7;
        updateProgressBar();
    }
    else if( (_url[1] == "find-a-boiler") && (_url[2] == "" || _url[2] == undefined) )
    {
        boilerQuestions[boilerQuestions.length - 1].progressBar = 5;
        saveBoilerQuestions();
        window.location.href = "/find-a-boiler/quote-summary";
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}