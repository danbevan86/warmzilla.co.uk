myVendigo = $('.vendigo_calculator');
depositSlider = myVendigo.find('input[name="finance-deposit"]');
loanAmount = $('#loan-amount');
maxDeposit = parseInt(loanAmount.text().replace(/£|,/gi, '') / 2);
depositSlider.attr('max', maxDeposit);
numYears = 3;
url = window.location.pathname;

if(url == "/find-a-boiler/quote-summary") {
} else {
    if($.cookie("vendigo_deposit")) {
        myVendigo.find('#deposit-amount').html("£" + $.cookie("vendigo_deposit"));
        myVendigo.find('input[name="finance-deposit"]').val($.cookie("vendigo_deposit"));
    } else {
        myVendigo.find('#deposit-amount').html("£0.00");
    }

    if($.cookie("vendigo_apr")) {
        $('.vendigo_calculator.interest-0').find('select[name="finance-plan"]').val($.cookie("vendigo_apr"));
        if($.cookie("vendigo_apr") == "0.0") {
            $('.vendigo_calculator.interest-0').find('select[name="finance-plan"] option:eq(0)').attr('selected', true);
            $('.vendigo_calculator.interest-0').find('select[name="finance-term"]').val(2);
            $('.vendigo_calculator.interest-0').find('.finance-12').hide();
            $('.vendigo_calculator.interest-0').find('.finance-0').show();
        }
    }

    if($.cookie("vendigo_months")) {
        switch($.cookie("vendigo_months")) {
            case '24':
                $('.vendigo_calculator.interest-0').find('select[name="finance-term"]').val(2);
                numYears = 2;
                break;
            case '36':
                myVendigo.find('select[name="finance-term"]').val(3);
                numYears = 3;
                break;
            case '48':
                myVendigo.find('select[name="finance-term"]').val(4);
                numYears = 4;
                break;
            case '60':
                myVendigo.find('select[name="finance-term"]').val(5);
                numYears = 5;
                break;
            case '84':
                myVendigo.find('select[name="finance-term"]').val(7);
                numYears = 7;
                break;
            case '120':
                myVendigo.find('select[name="finance-term"]').val(10);
                numYears = 10;
                break;        
        }
    }

    if($.cookie("vendigo_payments")) {
        myVendigo.find('#monthly-payment').html($.cookie("monthlyPayment"));
        $('.vendigo-monthly-payment').html($.cookie("monthlyPayment"));
    } else {
        myVendigo.find('#monthly-payment').html("£0.00");
        $('.vendigo-monthly-payment').html("£0.00");
    }

    if($.cookie("interest")) {
        myVendigo.find('#total-interest').html($.cookie("interest"));
    } else {
        myVendigo.find('#total-interest').html("£0.00");
    }
}


if(numYears) {
} else {
    numYears = 3;
}

$('.vendigo_calculator').each(function(index) {
    calculateResults($(this));
})

function changePlan(plan) {
    let val = $(plan).children("option:selected").val();
    if(val == 9.90) {
        $(plan).parents('.c-vendigo-calc').find(".fin-plan option.finance-12").show();
        $(plan).parents('.c-vendigo-calc').find(".fin-plan option.finance-0").hide();
        $(plan).parents('.c-vendigo-calc').find('select[name="finance-term"]').val(3);
        numYears = $(plan).parents('.c-vendigo-calc').find('select[name="finance-term"]').val();
        vendigo_type = "IBC";
    } else {
        $(".fin-plan option.finance-12").hide();
        $(".fin-plan option.finance-0").show();
        $(plan).parents('.c-vendigo-calc').find('select[name="finance-term"]').val(2);
        numYears = 2;
        vendigo_type = "IFC";
        $.cookie("vendigo_months", 24, {expires: 120, path: '/'});
    }
    calculateResults($(plan).parents('.vendigo_calculator'), $(plan).parents('.c-vendigo-calc').find('select[name="finance-term"] option:selected').val());
}

function changeTerm(term) {
    if($('.vendigo_calculator').length) {
        numYears = $(term).children("option:selected").val();
        calculateResults($(term).parents('.vendigo_calculator'), numYears);
    } else {
        calculateResults($('.vendigo_calculator'), $('select[name="finance-term"] option:selected').val());
    }
}

function calculateResults(product, term) {
    maxDeposit = parseInt($(product).find('#loan-amount').text().replace(/£|,/gi, '') / 2);
    depositSlider.attr('max', maxDeposit);
    let amount = "";
    let deposit = product.find('input[name="finance-deposit"]').val();
    if(typeof discountApplied !== 'undefined') {
        $(product).find('#loan-amount').text(discountApplied);
    }
    amount = parseFloat($(product).find('#loan-amount').text().replace(/£|,/gi, ''), 2);
    
    if(deposit) {
        amount = amount - deposit;
    }

    let years = "";
    if(term) {
        years = term;
    } else {
        years = numYears;
    }
    var interest;
    if(years == 2) {
        interest = 0.0;
    } else {
        interest = parseFloat($('#finance-plan option:selected').val()).toFixed(2);
    }
    const calculatedInterest = interest / 100 / 12;
    const calculatedPayments = years * 12;
    let monthly;

    if(calculatedInterest == 0.00) {
        monthly = amount / calculatedPayments
    } else {
        const x = Math.pow(1 + calculatedInterest, calculatedPayments);
        monthly = (amount * x * calculatedInterest) / (x - 1);
    }
    
    $(product).find('#monthly-payment').text('£' + monthly.toFixed(2));
    $('.payable_amount').find('.vendigo-monthly-payment').text('£' + monthly.toFixed(2));
    $(product).find('.vendigo-monthly-payment').text('£' + monthly.toFixed(2));
    var loan_amount = monthly * calculatedPayments;
    if(loan_amount < 1000) {
        $('.less-1k').show();
    } else {
        $('.less-1k').hide();
    }
    $(product).find('#total-price').text("£" + loan_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    $(product).find('#total-interest').text("£" + (monthly * calculatedPayments - amount).toFixed(2));
    $.cookie("vendigo_months", calculatedPayments, {expires: 120, path: '/'});
    if(deposit > 0) {
        var vendigoDeposit = deposit;
    } else {
        var vendigoDeposit = parseFloat($('#deposit-amount').text().replace("£", ""));
    }
    $.cookie("vendigo_deposit", vendigoDeposit, {expires: 120, path: '/'});
    var monthlyPayment = $(product).find('#monthly-payment').text();
    $.cookie("vendigo_payments", monthlyPayment, {expires: 120, path: '/'});
    $(product).parents('.item').find('.divido-widget-launcher a').html(monthlyPayment + '<span class="duration">per month   </span>');
    var vendigoApr = $(product).find('#finance-plan').children("option:selected").val();
    var vendigoType = '';
    if(years == '2') {
        vendigoType = 'IFC';
    } else {
        vendigoType = 'IBC';
    }
    $.cookie("vendigo_apr", vendigoApr, {expires: 120, path: '/'});
    $.cookie("vendigo_type", vendigoType, {expires: 120, path: '/'});
}

function changeDeposit(slider) {
    var val = slider.value;
    var price = $(slider).parents('div.c-vendigo-calc').find('#original-price').val();
    var loan_amount = parseInt(price.replace(/£|,/gi, ''));
    loan_amount = loan_amount - val;
    $(slider).parents('div.c-vendigo-calc').find('#deposit-amount').text('£' + parseFloat(val).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    calculateResults($(slider).parents('.vendigo_calculator'));
}

var $secret_key = "w2pyVeCNtP84-1aLRsZWZNa2XLRtoRjFL0X5ZvbKFseRXMzEUfFCsYHn_PT6eVHJ";
var $total_price = $('.divido_calculator').data("divido-amount");
var $firstName = $("input[name='first_name']").val();
var $lastName = $("input[name='last_name']").val();
var $email = $("input[name='email']").val();
var $phone = $("input[name='phone']").val();
var $buildingNumber = $('input[name="shipping_address"]').val().replace(/\D/g,'');
var $street = $('input[name="shipping_address"]').val().replace(/[0-9,]+/s,'');
var $city = $("input[name='shipping_city']").val();
var $postcode = $("input[name='shipping_zip']").val();
var $apr = $("select[name='finance-plan']").val();
if($apr == 9.90) {
    $vendigo_type = "IBC";
} else {
    $vendigo_type = "IFC";
}

if (typeof $.cookie("vendigo_months") !== 'undefined') {
    $term = parseFloat($.cookie("vendigo_months"));
} else {
    $term = 36;
}

if (typeof $.cookie("vendigo_deposit") !== 'undefined') {
    $deposit = parseFloat($.cookie("vendigo_deposit"));
} else {
    $deposit = 0;
}

$data = {
    "publicKey":"AK7p1XpGUXm1lVzxilZgfErrW6ALWct2",
    "orderId":"1304A",
    "amount": $total_price,
    "verticalId":3,
    "productType":$vendigo_type,
    "apr":$apr,
    "term":$term,
    "maxTerm":120,
    "deposit":$deposit,
    "defferedPeriod":0,
    "successUrl":"/thank-you",
    "customer": {
        "firstName":$firstName,
        "lastName":$lastName,
        "email":$email,
        "phone":$phone,
        "addresses": [{
            "buildingNumber":$buildingNumber,
            "street":$street,
            "city":$city,
            "postcode":$postcode
        }]
    }
}

/*var vendigoWidget = new VendigoWidget({
    onApplicationCreated: function(application) {
        console.log('application created', application);
    },
    onApplicationUpdated: function(application) {
        console.log('application updated', application);
    },
    onExit: function(application) {
        console.log('widget closed', application);
    },
});

$('#vendigo').click(function() {
    debugger;
    $.ajax({
        type: 'POST',
        url: '{site_url}' + '?ACT=120',
        data: {
            $data: JSON.stringify($data),
            $secret_key: $secret_key
        },
        success: function(result){
            console.log('success');
            console.log(result);
            vendigoWidget.open(result, $data);
        },
        error: function(msg){
            console.log('fail');
        }
    });
});*/
