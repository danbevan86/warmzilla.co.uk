finalizeProcess = "";
member_id = "";
_navigator = navigator.userAgent.toLowerCase();
$(document).ready(function() {
    /* Finalize Process */
    initFinalizeProcess();

    if($('#installation-date').val() != "")
    {
        var jsDate = new Date($('#installation-date').val());
        $('.step-1 h3 sup').html(nth(jsDate.getDate()));

        var member_id = getMemberIdFromInstallationDate();
        if(member_id == "") {
            setEngDate();
        } else {
            setEngDate(member_id);
        }
    }

    var maxMonths = 2;
    var curDate = new Date();
    allowedMonths = [];
    for (var i = 0; i < maxMonths; i++)
    {
        allowedMonths[i] = (curDate.getMonth() + 1) + "-" + curDate.getFullYear();
        curDate.setMonth(curDate.getMonth() + 1);
    }

    $('#calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        firstDay: 1,
        showNonCurrentDates : false,
        fixedWeekCount: false,
        dayRender: function(date, cell){
            $(cell).addClass('available');
            var myDate = new Date();
            var daysToHide = "";
            var hours = myDate.getHours();
            var day = myDate.getDay();
            if(day == 5) { // Friday 
                if(hours >= 18) { // 6pm or after
                    daysToHide = 3; // block off next 3 days
                } else {
                    daystoHide = 1; // block off next day
                }
            } else if(day == 6) { // Saturday
            	daysToHide = 2; // block off next 2 days
            } else { // any other day
                daysToHide = 1; // block off next day
            }
            
            myDate.setDate(myDate.getDate() + daysToHide);
            if(date < myDate) {
                $(cell).removeClass('available').addClass('disabled disable unavailable');
            }
            var xmasStart = new Date(2020, 11, 19);
            var xmasEnd = new Date(2021, 0, 6);
            var xmasSat = new Date(2020, 11, 12);
            if (date >= xmasStart && date < xmasEnd) {
                $(cell).removeClass('available').addClass('disabled disable unavailable');
            }

            if (date == xmasSat) {
                $(cell).removeClass('available').addClass('disabled disable unavailable');
            }

            $('td[data-date="2021-01-06"]').removeClass('available').addClass('disabled disable unavailable');
            $('td[data-date="2021-01-07"]').removeClass('available').addClass('disabled disable unavailable');
            $('td[data-date="2021-01-09"]').removeClass('available').addClass('disabled disable unavailable');
            $('td[data-date="2021-01-16"]').removeClass('available').addClass('disabled disable unavailable');
            $('td[data-date="2021-01-18"]').removeClass('available').addClass('disabled disable unavailable');
            $('td[data-date="2021-01-19"]').removeClass('available').addClass('disabled disable unavailable');
            $('.fc-bg .fc-today').removeClass('available').addClass('disabled disable unavailable');
        },
        events: 0,
        eventAfterAllRender: function( view ) {
            for (var i = 0; i < _myCal.length; i++)
            {
                if(_myCal[i].className.split(" ")[0] == "available")
                {
                    $('td[data-date="' + _myCal[i].start + '"]').removeClass('disabled disable unavailable').addClass('available');
					$('td.fc-event-container').find('.' + _myCal[i].className.split(" ")[1]).attr('data-date', _myCal[i].start);
                }
            }

            $('td.fc-event-container a').each(function(index, el) {
                var allClass = $(this).attr('class').split(' ');
                for (var i = 0; i < allClass.length; i++)
                {
                    if(allClass[i] == "available" && allClass[i+1] != undefined)
                    {
                        var dataDate = $(this).attr('data-date');
                        $('td[data-date="' + dataDate + '"]').attr('data-select', allClass[6]);
                        $(this).attr('data-select', allClass[6]);
                        $(this).attr('data-select', allClass[6]);
                        $(this).removeClass('unavailable disabled disable').addClass('select-date');
                    }
                }
            });

            var myCurMonth = (view.start._d.getMonth() + 1) + "-" + view.start._d.getFullYear();
            for (var i = 0; i < allowedMonths.length; i++)
            {
                if(allowedMonths[i] == myCurMonth)
                {
                    if(allowedMonths[i - 1] == undefined) {
                        $('.fc-left').hide();
                    } else {
                        $('.fc-left').show();
                    }

                    if(allowedMonths[i + 1] == undefined) {
                        $('.fc-right').hide();
                    } else {
                        $('.fc-right').show();
                    }
                }
            }

            setTimeout(function() {
                if($('#installation-date').val() != "")
                {
                    if(typeof($('#installation-date').attr('data-date')) !== "undefined")
                    {
                        var selectDate = $('td[data-date="' + $('#installation-date').data('date') + '"]').data('select');
                        $('td[data-select="' + selectDate + '"]').click();
                        $('input[name="custom_data[installation_date]"]').val(selectDate);

                        var member_id = getMemberIdFromInstallationDate();
                        if(member_id == "")
                        {
                            setFinalizeProcess("step-1");
                        }
                        // $('input[name="custom_data[assigned_engineer_for_this_job]"]').val(member_id);
                        $('#installation-date').removeAttr('data-date');
                    }
                    else
                    {
                        $('td[data-select="' + $('#installation-date').val() + '"]').click();
                    }
                }
            }, 50);

            var tempDate = new Date(view.start._d.getFullYear(), view.start._d.getMonth() + 1, 0);
            var lastDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1 < 10 ? '0' + tempDate.getMonth() + 1 : tempDate.getMonth() + 1) + '-' + (tempDate.getDate() < 10 ? '0' + tempDate.getDate() : tempDate.getDate());

            var dates1 = lastDate.split("-");
            var newDate = dates1[1]+"/"+dates1[2]+"/"+dates1[0];
            var lastDate = new Date(newDate).getTime()/1000;

            $('td.available').removeClass('unavailable');
            $('.fc-bg td.fc-sun').addClass('unavailable');
        },
        dayClick: function(date) {
            if($(this).hasClass('selected-date'))
            {
                $('td').removeClass('selected-date');
                $('#installation-date').val("");
                $('.installation_date_btn').addClass('disabled').attr('disabled', 'disabled');
                $('.installation_date_btn').text('Select Your Date');
                $('.select-date-info').show();
                $('.selected-date-info').hide();
            }
            else
            {
                if($(this).hasClass('unavailable')) {
                } else {
                    $('td').removeClass('selected-date');
                    $('.fc-content-skeleton .fc-today span, .fc-content-skeleton .fc-future span').css('color','#727272');
                    if($(this).data('select')) {
                        selected = $(this).data('select');
                        $('td[data-select="' + selected + '"]').addClass('selected-date');
                        var selectedDate = $('.fc-bg .selected-date').attr('data-date');
                        $('.fc-content-skeleton .fc-day-top[data-date="' + selectedDate + '"] span').css('color','white');
                        var jsDate = parseInt(selected) * 1000;
                    } else {
                        selected = $(this).attr('data-date');
                        $('td[data-date="' + selected + '"]').addClass('selected-date');
                        var selectedDate = $('.fc-bg .selected-date').attr('data-date');
                        $('.fc-content-skeleton .fc-day-top[data-date="' + selectedDate + '"] span').css('color','white');
                        var jsDate = selected;
                    }

                    $('.installation_date_btn').text('Confirm Your Date');
                    $('.select-date-info').hide();
                    $('.selected-date-info').show();

                    jsDate = new Date(jsDate);

                    const monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                    ];

                    var inDate = jsDate.getDate() + "<sup>" + nth(jsDate.getDate()) + "</sup> " + monthNames[jsDate.getMonth()];
                    $('.step-1').find('.after').find('h3').children('span').html(inDate)
                    $('input[name="custom_data[installation_date]"]').val(selected);

                    var member_id = getMemberIdFromInstallationDate();
                    if(member_id == "")
                    {
                        setFinalizeProcess("step-1");
                        $('.installation_date_btn').removeClass('disabled').removeAttr('disabled');
                    }
                    else
                    {
                        setFinalizeProcess("step-1");
                        $('.installation_date_btn').removeClass('disabled').removeAttr('disabled');
                    }
                }
            }
        }
    });

    $('#my-next-button').click(function() {
        $('#calendar').fullCalendar('next');
    });

    $(document).on('click', '.step-edit', function(event) {
        event.preventDefault();

        step = $(this).parents('.step:first');
        step.addClass('is-show').find('.step-body').slideDown();
        // step.removeClass('is-completed').addClass('is-show').find('.step-body').slideDown();
        step.siblings('.step').removeClass('is-show').find('.step-body').slideUp();

    });

    $(document).on('click', '.installation_date_btn', function(event) {
        event.preventDefault();
        if($("#installation-date").val() != "")
        {

            var member_id = getMemberIdFromInstallationDate($("#installation-date").val());
            if(member_id == "")
            {
                member_id = 0;
            }

            var cart_form_options = {
                success: completeStep1,
                dataType: 'json'
            };
            var form = $('form#save_data');
            $('.ajax-loader').show();
            $(form).ajaxForm(cart_form_options);
            $(form).submit();
        }
    });

    $(document).on('click', '.step-2-submit', function(event) {
        event.preventDefault();

        var nextStep = "step-3";
        if($('.' + nextStep).length == 0 || ($('.' + nextStep).length && $('.' + nextStep).hasClass('logged-in')))
        {
            nextStep = "step-4";
        }
        setFinalizeProcess(nextStep);
        switchCompleted("step-2", nextStep);

    });

    $(document).on('click', '.open-forgot-pass', function(event) {
        event.preventDefault();
        $('.member-forms').hide();
        $('.member-forgot-password').find('.main-form').show();
        $('.member-forgot-password').find('.forgot-success').hide();
        $('.member-forgot-password').addClass('is-show').show().find('.step-body').show();
    });

    $(document).on('click', '.open-reset-pass', function(event) {
        event.preventDefault();
        $('.member-forms').hide();
        $('.member-reset-password').find('.main-form').show();
        $('.member-reset-password').find('.forgot-success').hide();
        $('.member-reset-password').addClass('is-show').show().find('.step-body').show();
    });

    $(document).on('click', '.open-register-member', function(event) {
        event.preventDefault();
        $('.member-forms').hide();
        $('.member-register').addClass('is-show').show().find('.step-body').show();
    });

    $(document).on('click', '.open-login', function(event) {
        event.preventDefault();
        $('.member-forms').hide();
        $('.member-login').addClass('is-show').show().find('.step-body').show();
    });

    $(document).on('submit', 'form#forgot-password, form#reset-password', function(event) {
        event.preventDefault();
        /* Act on the event */
        _this = $(this);
        $('.ajax-loader').show();
        _this.find('ul.errors').html("").hide();
        if( ! _this.valid() )
        {
            $('.ajax-loader').hide();
            _this.find('input.error-inner:first').focus();
            return false;
        }

        $.ajax({
            url: _this.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: _this.serialize(),
        })
        .done(function(data, status, xhr) {
            if(typeof(data.CSRF_TOKEN) != "undefined")
            {
                $('input[name="csrf_token"]').val(data.CSRF_TOKEN);
            }

            if(data.status == "error")
            {
                if(data.error.length == 0)
                {
                    alert("There is some issue in logging you with site. Please refresh and try again.")
                }

                var generalErrors  = "";
                $.each(data.error, function(index, el) {
                    index = index.replace('error:', "");
                    if(_this.find('input[name="' + index + '"]').length)
                    {
                        _this.find('input[name="' + index + '"]').after('<div for="' + index + '" generated="true" class="error-inner">' + el + '</div>');
                    }
                    else
                    {
                        generalErrors += "<li>" + index + " : " + el  + "</li>";
                    }
                });
                if(generalErrors != "")
                {
                    if( ! _this.find('ul.errors').length )
                    {
                        _this.find('.form-group:last').after('<ul class="errors pt-4"></ul>');
                    }
                    _this.find('ul.errors').html(generalErrors).show();
                }
            }
            else
            {
                _this.find('.main-form').hide();
                _this.find('.forgot-success').show();
            }
            $('.ajax-loader').hide();
        })
        .fail(function() {
            $('.ajax-loader').hide();
            alert("There is some issue in logging you with site. Please refresh and try again.")
        });

    });

    $(document).on('submit', 'form#ajax_login_from, form#ajax_registration_from', function(event) {
        event.preventDefault();
        /* Act on the event */
        _this = $(this);
        $('.ajax-loader').show();
        _this.find('ul.errors').html("").hide();
        if( ! _this.valid() )
        {
            $('.ajax-loader').hide();
            _this.find('input.error-inner:first').focus();
            return false;
        }

        $.ajax({
            url: _this.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: _this.serialize(),
        })
        .done(function(data, status, xhr) {
            // <div for="email" generated="true" class="error-inner">This field is required.</div>
            if(typeof(data.CSRF_TOKEN) != "undefined")
            {
                $('input[name="csrf_token"]').val(data.CSRF_TOKEN);
            }

            if(data.status == "error")
            {
                if(data.error.length == 0)
                {
                    alert("There is some issue in logging you with site. Please refresh and try again.")
                }

                var generalErrors  = "";
                $.each(data.error, function(index, el) {
                    index = index.replace('error:', "");
                    if(_this.find('input[name="' + index + '"]').length)
                    {
                        _this.find('input[name="' + index + '"]').after('<div for="' + index + '" generated="true" class="error-inner">' + el + '</div>');
                    }
                    else
                    {
                        generalErrors += "<li>" + index + " : " + el  + "</li>";
                    }
                });
                if(generalErrors != "")
                {
                    if( ! _this.find('ul.errors').length )
                    {
                        _this.find('.form-group:last').after('<ul class="errors pt-4"></ul>');
                    }
                    _this.find('ul.errors').html(generalErrors).show();
                }
                $('.ajax-loader').hide();
            }
            else
            {
                $('.header-wrapper').load('/includes/.header', function(){

                });

                if(_this.attr('id') == "ajax_login_from")
                {
                    // $('.step-3').addClass('logged-in');
                    // setFinalizeProcess("step-4");
                    // switchCompleted("step-3", "step-4");
                    $('.your-details-form-wrapper').load('/my-order/.get-basic-fields', function(){
                        $('.ajax-loader').hide();
                        $('.step-3').hide();
                        $('.step-3.final').show().addClass('is-show').find('.step-body').show();
                    });
                }
                else
                {

                    if($('input[name="signup_reminder_boiler_service_register"]').prop('checked'))
                    {
                        setCookie('addUserNewsletter', 'yes', 1);
                        $('input[name="signup_reminder_boiler_service"]').prop('checked', true);
                    }
                    else
                    {
                        setCookie('addUserNewsletter', 'yes', -1);
                        $('input[name="signup_reminder_boiler_service"]').prop('checked', false);
                    }

                    $('.step-3.final').find('input[name="first_name"]').val($('.step-3.member-register').find('input[name="first_name"]').val());
                    $('.step-3.final').find('input[name="last_name"]').val($('.step-3.member-register').find('input[name="last_name"]').val());
                    $('.step-3.final').find('input[name="phone"]').val($('.step-3.member-register').find('input[name="phone"]').val());
                    $('.step-3.final').find('input[name="email"]').val($('.step-3.member-register').find('input[name="username"]').val());
                    $('.ajax-loader').hide();
                    $('form#update_cart_form_basic').find('.save-basic-info').click();
                }

            }
        })
        .fail(function() {
            $('.ajax-loader').hide();
            alert("There is some issue in logging you with site. Please refresh and try again.")
        });
    });

    $(document).on('change', 'input[name="use_billing_info"]', function(event) {
        event.preventDefault();
        /* Act on the event */
        if($('input[name="use_billing_info"]:checked').val() == 0)
        {
            $('input[name="address"]').val('');
            $('input[name="address2"]').val('');
            $('input[name="city"]').val('');
            $('input[name="state"]').val('');
            $('input[name="zip"]').val('');

            $('.billing-address-wrapper').show();
        }
        else
        {
            $('input[name="address"]').val($('input[name="shipping_address"]').val())
            $('input[name="address2"]').val($('input[name="shipping_address2"]').val())
            $('input[name="city"]').val($('input[name="shipping_city"]').val())
            $('input[name="state"]').val($('input[name="shipping_state"]').val())
            $('input[name="zip"]').val($('input[name="shipping_zip"]').val())

            $('.billing-address-wrapper').hide();
        }
    });

    $(document).on('click', '.trigger-manual-in-address', function(event) {
        event.preventDefault();
        showInstallationAddress();
        var _cookiePostCode = getCookie('boilerPostCode');
        $('.validate_zip').val(_cookiePostCode.toUpperCase());
    });

    $(document).on('click', '.trigger-manual-bill-address', function(event) {
        event.preventDefault();
        $('.billing-address-search').hide();
        $('.billing-address-data').show();
        setCookie('manualBillAdd', "yes", 1);
        $('.error-inner').hide();
    });

    _currentAjax = null;
    _validate_zip = false;
    _shipping_zip = "shipping_zip_validate";
    $(document).on('keyup', 'input.validate_zip', function(event) {

        event.preventDefault();
        /* Act on the event */
        _zip_this = $(this);
        var _cookiePostCode = getCookie('boilerPostCode');
        $('.address_submit').addClass('disabled').attr('disabled', 'disabled');
        if(_zip_this.val() == "")
        {
            _zip_this.parent().children('.icon-image').hide();
            _zip_this.parent().find('div.error-inner').remove();
        }
        else
        {
            if($(this).parents('.icon-wrapper').hasClass(_shipping_zip))
            {

                var _savedPostCode = _zip_this.val().split(" ")[0].toUpperCase();
                if(_cookiePostCode != "")
                {
                    _cookiePostCode = _cookiePostCode.split(" ")[0].toUpperCase();
                }

                _zip_this.parent().children('.icon-image').hide();
                _zip_this.parent().find('.loader').show();
                // if(_savedPostCode != _cookiePostCode)
                // {
                //     _zip_this.parent().children('.icon-image').hide();
                //     _zip_this.parent().find('.wrong').show();
                //     _zip_this.parent().find('div.error-inner').remove();
                //     _zip_this.after('<div class="error-inner">Postcode must start with "' + _cookiePostCode.toUpperCase() + '".</div>');
                //     _validate_zip = false;
                //     return false;
                // }


                _currentAjax = $.ajax({
                    url: '/find-a-boiler/ajax-check-valid-poscode/' + _savedPostCode,
                    beforeSend : function()    {
                        if(_currentAjax != null) {
                            _currentAjax.abort();
                            _zip_this.parent().find('.loader').show();
                        }
                    }
                })
                .success(function(data, status, xhr) {
                    _zip_this.parent().children('.icon-image').hide();
                    // if(data == "1")
                    // {
                        _zip_this.parent().find('.right').show();
                        _zip_this.parent().find('div.error-inner').remove();
                        _validate_zip = true;
                        $('.address_submit').removeClass('disabled').removeAttr('disabled');
                    // }
                    // else
                    // {
                    //     _zip_this.parent().find('.wrong').show();
                    //     _zip_this.parent().find('div.error-inner').remove();
                    //     _zip_this.after('<div class="error-inner">No Engineers are available on given Postcode.</div>');
                    //     _validate_zip = true;
                    // }
                })
                .fail(function(a, b, c) {
                    // _zip_this.parent().find('.wrong').show();
                    _zip_this.parent().children('.icon-image').hide();
                    _zip_this.parent().find('div.error-inner').remove();
                });
            }
            else
            {
                _zip_this.parent().children('.icon-image').hide();
                _zip_this.parent().find('div.error-inner').remove();
            }
        }

    });
    $('#in_postcode').keyup();

    $(document).on('click', '.address_submit', function(event) {
        event.preventDefault();
        _this = $(this).parents('form:first');

        if($('input[name="use_billing_info"]:checked').val() == "1")
        {
            $('input[name="address"]').val($('input[name="shipping_address"]').val())
            $('input[name="address2"]').val($('input[name="shipping_address2"]').val())
            $('input[name="city"]').val($('input[name="shipping_city"]').val())
            $('input[name="state"]').val($('input[name="shipping_state"]').val())
            $('input[name="zip"]').val($('input[name="shipping_zip"]').val())
        }

        if($('.billing-address-data:visible').length == 0 && $('input[name="use_billing_info"]:checked').val() == 0)
        {
            $('#autocomplete_billing_address').addClass('required validate[required]');
        }
        else
        {
            $('#autocomplete_billing_address').removeClass('required validate[required]');
        }

        if(! _this.valid() )
        {
            $('#ajax-loader').hide();
            _this.find('input.error-inner:first').focus();
            return false;
        }
        if(_this.find('div.error-inner:visible').length)
        {
            $('#ajax-loader').hide();
            return false;
        }

        var cart_form_options = {
            success: completeStep4,
            dataType: 'json'
        };
        var form = $('form#update_cart_form');
        $('.ajax-loader').show();
        $(form).ajaxForm(cart_form_options);
        $(form).submit();

    });

    $(document).on('click', '.save-basic-info', function(event) {

        if(! $('form#update_cart_form_basic').valid())
        {
            return false;
        }

        var cart_form_options = {
            success: completeStep3,
            dataType: 'json'
        };
        var form = $('form#update_cart_form_basic');
        var firstName = form.find('input[name="first_name"]').val();
        var lastName = form.find('input[name="last_name"]').val();
        $('#card_first_name').val(firstName);
        $('#card_last_name').val(lastName);
        $('.ajax-loader').show();
        $(form).ajaxForm(cart_form_options);
        $(form).submit();
    });

    $(document).on('keyup', 'input[name="shipping_address"], input[name="shipping_address2"], input[name="shipping_city"], input[name="shipping_state"], input[name="shipping_zip"]', function(event) {
        event.preventDefault();
        /* Act on the event */
        if($('input[name="use_billing_info"]:checked').val() == "1")
        {
            var name = $(this).attr('name').replace("shipping_", '');
            $('input[name="' + name + '"]').val($(this).val());
        }
    });

    $('form#checkout_form').validate({
        errorPlacement: function(error, element) {
            if(element.attr("name") == "privacy_policy")
            {
                error.appendTo(element.parent('.custom-checkbox'));
            }
            else
            {
                error.insertAfter(element);
            }
        }
    });

    $(document).on('change', 'input[name=gateway]', function() {
        $('input[name=gateway]').next('label').removeClass('checked');
        $('input[name=gateway]:checked').next('label').addClass('checked');

        if($("input[name=gateway]:checked").data('gateway') == "Divido")
        {
            $('.divido-widget-launcher').trigger('click')
            $('input[name="deposit"]').removeAttr('disabled');

            $('#credit_card_number').removeClass('required validate[required] creditcard');
            $('#expiration_month').removeClass('required validate[required]');
            $('#expiration_year').removeClass('required validate[required]');
            $('#CVV2').removeClass('required validate[required]');
        }
        else if($("input[name=gateway]:checked").data('gateway') == "Vendigo")
        {
            $('input[name="deposit"]').attr('disabled', 'disabled');

            $('#credit_card_number').removeClass('required validate[required] creditcard');
            $('#expiration_month').removeClass('required validate[required]');
            $('#expiration_year').removeClass('required validate[required]');
            $('#CVV2').removeClass('required validate[required]');
        }
        else
        {
            $('input[name="deposit"]').attr('disabled', 'disabled');
            $('#credit_card_number').addClass('required validate[required] creditcard');
            $('#expiration_month').addClass('required validate[required]');
            $('#expiration_year').addClass('required validate[required]');
            $('#CVV2').addClass('required validate[required]');
        }
    });
    $('input[name=gateway]').trigger('change');

    $(document).on('click', '.search-for-installation-address', function(event) {
        event.preventDefault();
        $('.installation-address-data').hide().children('.form-group:first').children('.inner-group:first').find('input').val("");
        $('.installation-address-search').show().find('input').val('');
        setCookie('manualInAdd', "no", -1);
    });

    $(document).on('click', '.search-for-billing-address', function(event) {
        event.preventDefault();
        $('.billing-address-data').hide().children('.form-group:first').children('.inner-group:first').find('input').val("");
        $('.billing-address-search').show().find('input').val('');
        setCookie('manualBillAdd', "no", -1);
    });

    $('.step-5').find('input').change(function(event) {
        var stage=true;
        $('.step-5').find('input:visible').each(function(index, el) {
            if($(this).attr('type') == "checkbox" && $(this).hasClass('required'))
            {
                if($('.step-5').find('input[name="' + $(this).attr('name') + '"]:checked').length == 0)
                {
                    stage = false;
                }
            }
            else if($(this).val() == "" && $(this).hasClass('required'))
            {
                stage = false;
            }
            else if($('.step-5').find('.less-1k').is(':visible')) {
                stage = false;
            }
        });

        if(stage == true)
        {
            $('#complete_checkout').removeAttr('disabled').removeClass('disabled');
        }
        else
        {
            $('#complete_checkout').attr('disabled', 'disabled').addClass('disabled');
        }
    });
});

$(document).on('click', '#complete_checkout', function(event) {
    if($("input[name=gateway]:checked").data('gateway') !== "Stripe")
    {
        CartthrobTokenizer.submissionState = true;
        $('.overlay').show();
    }
});

$(window).load(function() {
    setTimeout(function() {
        $('.unavailable').removeClass('hideit');
    }, 700);
});

function completeStep3(data, statusText, xhr, $form)
{
    $('.ajax-loader').hide();
    if (data.success)
    {

        if($('input[name="signup_reminder_boiler_service"]').prop('checked'))
        {
            setCookie('addUserNewsletter', 'yes', 1);
        }
        else
        {
            setCookie('addUserNewsletter', 'yes', -1);
        }

        $('input[name="csrf_token"]').val(data.CSRF_TOKEN);
        $('.step-3.final').find('.after').find('h3').children('span').html($('form#update_cart_form_basic').find('input[name="first_name"]').val() + " " + $('form#update_cart_form_basic').find('input[name="last_name"]').val());
        setFinalizeProcess("step-4");
        switchCompleted("step-3", "step-4", render = "animate");
        $('.step-3').hide();
        $('.step-3.final').show().addClass('is-completed');
    }
}
function completeStep4(data, statusText, xhr, $form)
{
    $('.ajax-loader').hide();
    if (data.success)
    {
        $('input[name="csrf_token"]').val(data.CSRF_TOKEN);
        $('.step-4').find('.after').find('h3').children('span').html($('input[name="shipping_zip"]').val());
        setFinalizeProcess("step-5");
        switchCompleted("step-4", "step-5", render = "animate");
    }
}

function switchCompleted(step1, step2, render)
{
    if(typeof(render) === "undefined")
    {
        render = "animate";
    }

    if(render == "direct")
    {
        $('.' + step1 + ":visible").removeClass('is-show').addClass('is-completed').find('.step-body').hide();
        $('.' + step2 + ":visible").removeClass('is-completed').addClass('is-show').find('.step-body').show();
    }
    else
    {
        $('.' + step1).removeClass('is-show').addClass('is-completed').find('.step-body').slideUp();
        $('.' + step2 + ":visible").removeClass('is-completed').addClass('is-show').find('.step-body').slideDown();
    }
}

function initFinalizeProcess()
{
    finalizeProcess = getCookie('finalizeProcess');
    
    if($('#installation-date').val() == "") {
        finalizeProcess = "step-1";
    } else {
        if(finalizeProcess == "" || (finalizeProcess != "" && finalizeProcess != "step-1" && finalizeProcess != "step-5" && $('#installation-date').val() == ""))
        {
            finalizeProcess = "step-1";
        }
    }

    // if(finalizeProcess != "step-1")
    // {
    //     debugger;
    //     var member_id = getMemberIdFromInstallationDate();
    //     if(member_id == "")
    //     {
    //         finalizeProcess = "step-1";
    //     }
    // }

    if(finalizeProcess == "step-3" && $('.'+finalizeProcess).length == 0)
    {
        finalizeProcess = "step-4";
    }

    /*if(finalizeProcess == "step-4" || finalizeProcess == "step-5" || finalizeProcess == "step-6" || finalizeProcess == "step-7")
    {
        if($('.step-3').length)
        {
            finalizeProcess = "step-3";
        }
    }*/

    flag = true;
    $('.checkout-step').children('.step').each(function(index, el) {

        if($(this).hasClass(finalizeProcess))
        {
            if(finalizeProcess != "step-2");
            {
                $(this).addClass('is-show').children('.step-body').slideDown();
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this).offset().top
                }, 2000);
            }
            flag = false;
        }
        else
        {
            if(flag)
            {
                $(this).addClass('is-completed').children('.step-body').hide()
            }
            else
            {
                $(this).removeClass('is-completed is-show').children('.step-body').hide()
            }
        }
    });

    var manualInAdd = getCookie('manualInAdd');
    if(manualInAdd == "yes")
    {
        $('.installation-address-search').hide();
        $('.installation-address-data').show();
    }

    var manualBillAdd = getCookie('manualBillAdd');
    if(manualBillAdd == "yes")
    {
        $('.billing-address-search').hide();
        $('.billing-address-data').show();
    }
}

function setFinalizeProcess(progress)
{

    finalizeProcess = progress;
    setCookie("finalizeProcess", finalizeProcess, 1);
}
function completeStep1(data, statusText, xhr, $form)
{
    if (data.success)
    {
        $('input[name="csrf_token"]').val(data.CSRF_TOKEN);
        setEngDate();
    }
}

function getMemberIdFromInstallationDate(instDate)
{
    member_id = "";
    for(i=0; i<_myCal.length; i++)
    {
        if(_myCal[i].timestamp == $('#installation-date').val())
        {
            member_id = _myCal[i].member_id;
        }
    }
    return member_id;
}

function setEngDate(temp)
{
    if(typeof(temp) === "undefined")
    {
        temp = "";
    }

    if(temp != "")
    {
        member_id = temp;
    }

    $('.ajax-loader').hide();
    if(temp == "")
    {
        setFinalizeProcess("step-2");
        switchCompleted("step-1", "step-2");
    }
    else if(finalizeProcess == "step-2")
    {
        switchCompleted("step-1", "step-2", 'direct');
    }

    // $.ajax({
    //     url: 'https://www.warmzilla.co.uk/json/engineer-info/' + member_id,
    //     dataType: 'json'
    // })
    // .done(function(data, status, xhr)
    // {
    //     $('.eng_name').html(data.name);
    //     var firstName = data.name.split(" ")[0];
    //     $('.step-2').find('.after').find('h3').children('span').html(data.name);
    //     $('.eng_bio').html("Hi, I'm " + firstName + " and I'll be fitting your new boiler. I'll arrive between 8:00am - 11:00am on the day of installation. I will be bringing your boiler and all the materials needed to complete the job with me. Looking forward to meeting you :)");
    //     $('.eng_avatar').attr('src', data.avatar);
    //     $('input[name="custom_data[engineer_found_in_area]"]').val(member_id);

    //     if(data.fav_station == "" || data.experience == "")
    //     {
    //         $('.station-and-exp').hide();
    //     }
    //     else
    //     {
    //         $('.station-and-exp').show();
    //         if(data.fav_station == "")
    //         {
    //             $('.eng_fav_radio_wrapper').hide();
    //         }
    //         else
    //         {
    //             $('.eng_fav_radio_wrapper').show();
    //             $('.eng_fav_radio').html(data.fav_station);
    //         }
    //         if(data.experience == "")
    //         {
    //             $('.eng_experience_wrapper').hide();
    //         }
    //         else
    //         {
    //             $('.eng_experience_wrapper').show();
    //             $('.eng_experience').html(data.experience);
    //         }
    //     }

        // if(data.name == "")
        // {
        //     $('.eng_first_name').each(function(index, el) {
        //         $(this).html($(this).attr('alternate'))
        //     });
        // }
        // else
        // {
        //     $('.eng_first_name').html(firstName);
        // }

}

function nth(d) {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}

var placeSearch, autocomplete;
var searchedAddress = {
    street_number: '',
    route: '',
    locality: '',
    sublocality_level_1: '',
    sublocality_level_2: '',
    sublocality_level_3: '',
    administrative_area_level_1: '',
    administrative_area_level_2: '',
    country: '',
    postal_code: ''
};
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    sublocality_level_1: 'long_name',
    sublocality_level_2: 'long_name',
    sublocality_level_3: 'long_name',
    administrative_area_level_1: 'short_name',
    administrative_area_level_2: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    autocomplete_billing_address = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete_billing_address')), {});
    autocomplete_installation_address = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete_installation_address')), {});
    //{types: ['geocode']}
    // autocomplete.addListener('place_changed', fillInAddress);
    google.maps.event.addListener(autocomplete_billing_address, 'place_changed', fillBillAddress);
    google.maps.event.addListener(autocomplete_installation_address, 'place_changed', fillInAddress);
}

function fillInAddress()
{
    fillAddress(autocomplete_installation_address, "in");
    if($('input[name="use_billing_info"]:checked').val() == "1")
    {
        fillAddress(autocomplete_installation_address, "bill");
    }
    showInstallationAddress();
}

function fillBillAddress()
{
    fillAddress(autocomplete_billing_address, "bill");
    $('.trigger-manual-bill-address').trigger('click');
}
function fillAddress(autocomplete, type) {
    if(typeof(type) === "undefined")
    {
        type = "in";
    }
    var place = autocomplete.getPlace();

    for (var component in searchedAddress) {
        // document.getElementById(component).value = '';
        // document.getElementById(component).disabled = false;
        searchedAddress[component] = "";
    }

    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            searchedAddress[addressType] = place.address_components[i][componentForm[addressType]];
            // document.getElementById(addressType).value = val;
        }
    }
    var addressLine1 = "";
    var addressLine2 = "";
    var city = "";
    var county = "";
    var postcode = "";
    if(searchedAddress.street_number != "" && searchedAddress.street_number != "undefined")
    {
        if(addressLine1 != "") { addressLine1 += ", "; }
        addressLine1 += searchedAddress.street_number;
    }

    if(searchedAddress.route != "" && searchedAddress.route != "undefined")
    {
        if(addressLine1 != "") { addressLine1 += ", "; }
        addressLine1 += searchedAddress.route;
    }

    if(searchedAddress.sublocality_level_3 != "" && searchedAddress.sublocality_level_3 != "undefined")
    {
        if(addressLine2 != "") { addressLine2 += ", "; }
        addressLine2 += searchedAddress.sublocality_level_3;
    }
    if(searchedAddress.sublocality_level_2 != "" && searchedAddress.sublocality_level_2 != "undefined")
    {
        if(addressLine2 != "") { addressLine2 += ", "; }
        addressLine2 += searchedAddress.sublocality_level_2;
    }
    if(searchedAddress.sublocality_level_1 != "" && searchedAddress.sublocality_level_1 != "undefined")
    {
        if(addressLine2 != "") { addressLine2 += ", "; }
        addressLine2 += searchedAddress.sublocality_level_1;
    }

    if(searchedAddress.locality != "" && searchedAddress.locality != "undefined")
    {
        city = searchedAddress.locality;
    }

    if(searchedAddress.administrative_area_level_2 != "" && searchedAddress.administrative_area_level_2 != "undefined")
    {
        county = searchedAddress.administrative_area_level_2;
    }

    if(searchedAddress.postal_code != "" && searchedAddress.postal_code != "undefined")
    {
        postcode = searchedAddress.postal_code;
    }

    $('#' + type + '_add_line_1').val(addressLine1);
    $('#' + type + '_add_line_2').val(addressLine2);
    $('#' + type + '_city').val(city);
    $('#' + type + '_county').val(county);
    $('#' + type + '_postcode').val(postcode);

}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function showInstallationAddress()
{
    $('.installation-address-search').hide();
    $('.installation-address-data').show();
    $('#in_postcode').keyup();
    setCookie('manualInAdd', "yes", 1);
}