$(function() {
    var tabs = $("#tabs").tabs();

    if($(".error-inner").length > 0) {
        selectedTab = parseInt($('.error-inner').parents('.page').attr('id').split("-")[1]) - 1;
        tabs("option", "active", selectedTab);
    }

    $(".next").click(function(e){
        e.preventDefault;
        var valid = true;
        var i = 0;
        $(this).closest(".page").find(".requ").addClass("required");
        var $inputs = $(this).closest(".page").find(".required");
        $inputs.each(function() {
            if (!validator.element(this) && valid) {
                valid = false;
                $("html,body").animate({scrollTop: $(".error-inner:visible").parent().offset().top - 20}, 300);
            }
        });

        if (valid) {
            n = this.hash.split("-");
            n = parseInt(n[1]);
            n = n - 1;
            $(".first-name").html($("input[name='first_name']").val());
            $("#tabs").tabs("option", "active", n);
            $("html,body").animate({scrollTop: $("body").offset().top}, 300);
            if(n == 4) {
                $('#tab-5').find('.required').removeClass('error-inner');
                $('#tab-5').find('.error-inner').hide();
            }
        } else {
            return false;
        }
    });

    $(".prev").click(function(){
        n = this.hash.split("-");
        n = parseInt(n[1]);
        n = n - 1;
        $(".first-name").html($("input[name='first_name']").val());
        $("#tabs").tabs("option", "active", n);
    });

    $("#tabs .submit").click(function(){
        var valid = true;
        $(this).closest(".page").find(".requ").addClass("required");
        var $inputs = $(this).closest(".page").find(".required");
        $inputs.each(function() {
            if (!validator.element(this) && valid) {
                valid = false;
            }
        });

        if (valid) {
            $(this).parents("form").submit();
            $(".overlay").show();
        }
    })
});
