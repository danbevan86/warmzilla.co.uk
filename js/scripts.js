var $ = jQuery.noConflict();

$(function() {

    /* Responsive Jquery Navigation */
    $(".hamburger").click(function() {
        $("#mobilenav").toggleClass("is-open");
        $("html,body").toggleClass("scroll-hidden");
    });

    $("#mobilenav .nav-backdrop").click(function() {
        $("#mobilenav").removeClass("is-open");
        $("html,body").removeClass("scroll-hidden");
    });

    $("body").on("keyup",function(evt) {
        if (evt.keyCode == 27) {
            $(".hamburger.is-active").removeClass("is-active");
            $("#mobilenav.is-show").removeClass("is-show");
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    var clickable = $('#menu').attr('link-clickable');
    $('#mobilenav li:has(ul)').addClass('has-sub');
    $('#mobilenav .has-sub>a').after('<em class="caret">');
    if(clickable == 'true'){
        $('#mobilenav .has-sub>a').addClass('trigger-caret').attr('href','javascript:;');
    }else{
        $('#mobilenav .has-sub>.caret').addClass('trigger-caret');
    }

    $(".nav-item").hover(function() {
        $(this).find(".submenu").toggleClass("open");
    });

    /* menu open and close on single click */
    $(document).on('click','#mobilenav .has-sub>.trigger-caret',function(event) {
        var element = $(this).parent('li');
        if (element.hasClass('is-open')) {
            element.removeClass('is-open');
            element.find('li').removeClass('is-open');
            element.find('ul').slideUp(200);
        }
        else {
            element.addClass('is-open');
            element.children('ul').slideDown(200);
            element.siblings('li').children('ul').slideUp(200);
            element.siblings('li').removeClass('is-open');
            element.siblings('li').find('li').removeClass('is-open');
            element.siblings('li').find('ul').slideUp(200);
        }
    });  

    if(!$('.warminstaller').length) {
        // Parse the URL
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&amp;]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        // Give the URL parameters variable names
        var source = getParameterByName('utm_source');
        var medium = getParameterByName('utm_medium');
        var campaign = getParameterByName('utm_campaign');

        // Set the cookies
        if($.cookie('utm_source') == null || $.cookie('utm_source') == "") {
            $.cookie('utm_source', source, {path: '/', expires: 120});
        }
        if($.cookie('utm_medium') == null || $.cookie('utm_medium') == "") {
            $.cookie('utm_medium', medium, {path: '/', expires: 120});
        }
        if($.cookie('utm_campaign') == null || $.cookie('utm_campaign') == "") {
            $.cookie('utm_campaign', campaign, {path: '/', expires: 120});
        }
    }

    /*changed by dev from url to _url to solve an error*/
    $(".video-model").on('hidden.bs.modal', function (e) {
        var _url = $(".video-model iframe").attr("src");
        _url = _url.replace(/&autoplay=1/ig, "");
        _url = _url.replace(/\?autoplay=1/ig, "");
        $(".video-model iframe").attr("src", _url);
    });

    /* slider with popup */    
    $.fancybox.defaults.thumbs.autoStart = true;
    var _user_selector = '.gallery-slider.user .slick-slide:not(.slick-cloned)';
    var _engineer_selector = '.gallery-slider.engineer .slick-slide:not(.slick-cloned)';
    var _gas_selector = '.gallery-slider.gas .slick-slide:not(.slick-cloned)';
    var _electrical_selector = '.gallery-slider.electrical .slick-slide:not(.slick-cloned)';
    var _benchmark_selector = '.gallery-slider.benchmark .slick-slide:not(.slick-cloned)';

    $('.gallery-slider').fancybox({
        selector : _user_selector,
        backFocus : false,
        animationEffect : "fade"
    });

    $('.gallery-slider').fancybox({
        selector : _engineer_selector,
        backFocus : false,
        animationEffect : "fade"
    });

    $('.gallery-slider').fancybox({
        selector : _gas_selector,
        backFocus : false,
        animationEffect : "fade"
    });

    $('.gallery-slider').fancybox({
        selector : _electrical_selector,
        backFocus : false,
        animationEffect : "fade"
    });
    $('.gallery-slider').fancybox({
        selector : _benchmark_selector,
        backFocus : false,
        animationEffect : "fade"
    });

    $(document).on('click','.gallery-slider .slick-cloned', function(e) {
        $(selector)
            .eq(($(this).attr("data-slick-index") || 0) % $(selector).length)
            .trigger("click.fb-start", { $trigger: $(this) });
        return false;
    });

    if($('.gallery-slider').length){
        $('.gallery-slider').slick({
            infinite: true,            
            slidesToShow : 4,
            slidesToScroll: 1,              
            prevArrow:'<span class="slick-prev"><i class="fal fa-angle-left"></i></span>',
            nextArrow:'<span class="slick-next"><i class="fal fa-angle-right"></i></span>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow   : 3,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 482,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
            ]

        });
    }

    $('.que-sec .que-list .item .info').click(function(){
        $(this).parents('.item').toggleClass('is-show-info'); 
    });

    /* faq-accordian */
    $('.faqs-list .card:first:not(.product-description):not(.general)').addClass('is-open');
    $('.faqs-list .card:first:not(.product-description):not(.general) .collapse').slideDown();
    $('.faqs-list .card .card-header').click(function() {        
        if ($(this).parents('.card').hasClass('is-open')) {
            $(this).parents('.card').removeClass('is-open');
            $(this).parent().find(".collapse").stop(true, false).slideUp();
        } else {            
            $('.faqs-list .card').removeClass('is-open');
            $(this).parents('.card').addClass('is-open');
            $(".faqs-list .card .collapse").slideUp();
            $(this).parent().find(".collapse").stop(true, false).slideDown();
        }
    }); 

    if($('#pay_accordion').length){
        $('#pay_accordion').collapse({
            toggle: false
        });
    }

    $(".success-message .close-btn").click(function(){
        $(this).parents(".success-message").remove(); 
    });

    $(".declined-message .close-btn").click(function(){
        $(this).parents(".declined-message").remove(); 
    });

    /* custome radio button */
    $('.finalize-job-engineer input[type="radio"]').each(function(){
        $(this).parent('label').addClass('custom-radio');
        $(this).after('<label class="custom-control-label"></label>');
        $(this).addClass('custom-control-input');
    });

    /* new checkout step */
    $(".step.is-show .step-body").slideDown();
    $('.step.is-completed .step-header').click(function(){
        if ($(this).parents('.step').hasClass('is-show')) {
            $(this).parents('.step').removeClass('is-show');
            $(this).parent().find('.step-body').stop(true, false).slideUp();
        } else {
            $('.step').removeClass('is-show');
            $(this).parents('.step').addClass('is-show');
            $(".step .step-body").slideUp();
            $(this).parent().find(".step-body").stop(true, false).slideDown();
        }
    })
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}