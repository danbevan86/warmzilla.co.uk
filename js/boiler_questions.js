boilerQuestions = [];
$(document).ready(function() {
    getBoilerQuestions();

    $(document).on('click', '.execute-questions', function(event) {
        event.preventDefault();
        /* Act on the event */

        setProcessPage("start-now-screen", "ask-for-postcode", 1);
        console.log('progressbar update: 1');
        updateProgressBar();

        $('.start-now-screen').slideUp();
        $('.ask-for-postcode').slideDown();
        /*$('.start-questions').slideDown();*/

    });

    $(document).on('click', '.answer-box', function(event) {
        event.preventDefault();
        /* Act on the event */
        console.log("Answer box clicked.");

        currentSection = {
            selection   : $(this).data('selection'),
            next        : $(this).data('next'),
            final       : $(this).data('final'),
            progressBar : $(this).parents('.que-sec:first').data('step'),
            entry       : $(this).parents('.que-sec:first').data('entry'),
            title       : $(this).parents('.que-sec:first').data('title'),
        };

        boilerQuestions.push(currentSection);
        saveBoilerQuestions();
        
        if(currentSection.next != "")
        {
            $(this).parents('.que-sec:first').slideUp();
            $('.question_' + currentSection.next).slideDown();
        }
        else
        {
            setFinalStep();
        }

        console.log('progressbar update: 2');
        updateProgressBar();
    });

    $(document).on('click', '.back-btn', function(event) {
        event.preventDefault();
        /* Act on the event */
        lastQuery = boilerQuestions[boilerQuestions.length - 1];
        
        if(lastQuery.entry != "")
        {
            entry_id = lastQuery.entry;
            boilerQuestions.length = boilerQuestions.length - 1;
            saveBoilerQuestions();

            $('.que-sec:visible').slideUp();
            $('.question_' + entry_id).slideDown();
        }
        else
        {
            _process = lastQuery.processFrom;
            boilerQuestions.length = boilerQuestions.length - 1;
            saveBoilerQuestions();

            $('.que-sec:visible').slideUp();
            $('.' + _process).slideDown();
        }
        console.log('progressbar update: 3');
        updateProgressBar();

    });

    $(document).on('click', '.find-postcode', function(event) {
        event.preventDefault();
        /* Act on the event */
        if(! $(".poscode-form").valid()) {
            return false;
        }

        $('.ajax-loader').show();
        $('.find-postcode').attr('disabled', 'disabled');


        $.ajax({
            url: '/find-a-boiler/ajax-check-valid-poscode/' + $('.poscode-form').find('input[name="postcode"]').val().split(" ")[0],
        })
        .success(function(data, status, xhr) {
            $('.ajax-loader').hide();
            $('.find-postcode').removeAttr('disabled');
            $('.poscode-form').find('input[name="postcode"]').val("")
            if(data == "1")
            {
                startQuestions();
            }
            else
            {
                showContactForm();
            }
        })
        .fail(function(a, b, c) {
            $('.ajax-loader').hide();
            $('.find-postcode').removeAttr('disabled');
            showContactForm();
        })
    });

    $('label.required').each(function(index, el) {
        if($(this).next('input').length)
        {
            $(this).next('input').addClass('required validate[required]');
        }
        /*$(this).parents('form:first').addClass('validation-form');*/
    });

    $(document).on('submit', '.validation-form', function(event) {
        $(this).find('ul.errors').remove();
        $(this).find('div.error-inner').remove();
        if(! $(this).valid())
        {
            return false;
        }
    });
});

function setProcessPage(from, to, progress = 0)
{
    currentSection = {
        selection   : "",
        next        : "",
        final       : "",
        entry       : "",
        title       : "",
        progressBar : progress,
        processFrom : from,
        processTo   : to,
    };

    boilerQuestions.push(currentSection);
    saveBoilerQuestions();
}

function updateProgressBar()
{
    if(boilerQuestions.length > 0)
    {
        _current = boilerQuestions[boilerQuestions.length - 1];
        if(_current.progressBar == 0 || _current.progressBar == "")
        {
            $('.progress-bar-wrapper').hide();
        }
        else if(_current.final == "give_us_a_call")
        {
            $('.progress-bar-wrapper').hide();
        }
        else 
        {
            if($('.progress-bar-wrapper:visible').length == 0)
            {
                $('.progress-bar-wrapper').show();
            }
            _block = $('.progress-bar-wrapper').find(".wz-steps-wizard").children('.pbar-' + _current.progressBar);
            _block.removeClass('finished').addClass('active');
            _block.prevAll().addClass('finished').removeClass('active')
            _block.nextAll().removeClass('active finished');
            $('.progress-bar-wrapper').find(".wz-progress").removeClass().addClass('wz-progress active-' + _current.progressBar);
        }
    }
    else
    {
        $('.progress-bar-wrapper').hide();
    }

    /*if($(".boiler-question:visible").length > 0)
    {
        if($('.progress-bar-wrapper:visible').length == 0)
        {
            $('.progress-bar-wrapper').show();
        }

        progressBar = $(".boiler-question:visible").data('step');
        console.log("Herre: " + progressBar)
        _block = $('.progress-bar-wrapper').find(".wz-steps-wizard").children('.pbar-' + progressBar);
        _block.removeClass('finished').addClass('active');
        _block.prevAll().addClass('finished').removeClass('active')
        _block.nextAll().removeClass('active finished');
        $('.progress-bar-wrapper').find(".wz-progress").removeClass().addClass('wz-progress active-' + progressBar);
    }
    else
    {
        $('.progress-bar-wrapper').hide();
    }*/

    console.log("On the right place at last");
}

function performFreshBoilerQuestionary()
{
    boilerQuestions = [];
    saveBoilerQuestions();

    $('.start-now-screen').show();
    $('.boiler-question').hide();
    updateProgressBar()
}

function startQuestions()
{
    $('.que-sec:visible').slideUp();
    $('.progress-bar-wrapper').show();
    $('.start-questions').slideDown();
    updateProgressBar()
}

function showContactForm()
{
    setProcessPage("ask-for-postcode", "postcode-not-found");
    $('.que-sec:visible').slideUp();
    $('.postcode-not-found').slideDown();
    updateProgressBar()
}

function retriveFromLastQuestion()
{
    entry_id = boilerQuestions[boilerQuestions.length - 1].next;
    if(entry_id != "")
    {
        $('.find-a-boiler-sec').hide();
        $('.start-now-screen').hide();
        $('.question_' + entry_id).show();
    }
    else
    {
        setFinalStep();
    }
    console.log('progressbar update: 4');
    updateProgressBar();
}


function saveBoilerQuestions()
{
    var temp = JSON.stringify(boilerQuestions)
    setCookie("boilerQuestions", temp, 5);
    console.log("Boiler Questions saved successfully.");
}

function getBoilerQuestions()
{
    temp = getCookie("boilerQuestions");
    if(temp == "" || temp.length == 0)
    {
        console.log('No questions found. Setting it to 1st question.');
        performFreshBoilerQuestionary();
        return false;
    }
    else
    {
        boilerQuestions = JSON.parse(temp);
        if(boilerQuestions.length == 0)
        {
            console.log('Questions array found blank. Setting it to 1st question.');
            performFreshBoilerQuestionary();
            return false;
        }
        else
        {
            console.log('Find boiler questions.');
            retriveFromLastQuestion();
        }
    }
}

function setFinalStep()
{
    temp = boilerQuestions[boilerQuestions.length - 1];

    if(temp.next != "")
    {
        $(this).parents('.que-sec:visible').slideUp();
        $('.question_' + temp.next).slideDown();
    }
    else
    {
        if(temp.final != "")
        {
            switch(temp.final) {
                case "summary":
                    handleSummaryPage();
                    break;

                case "give_us_a_call":
                    $('.que-sec:visible').slideUp();
                    $('.give-a-call').show();
                    $('.give-us-call').slideDown();
                    break;

                default:
                    alert("You are in the wrong window. Are you sure you did create entry properly?");
            }
        }
        else
        {
            $('.que-sec:visible').hide();
            $("." + temp.processTo).show();
        }
    }
}

function handleSummaryPage()
{
    window.location.href = "/find-a-boiler/quote-summary";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}