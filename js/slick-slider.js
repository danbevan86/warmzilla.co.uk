$(function() {

    var $slickSlider = $('.slider-for');

    $slickSlider.each(function() {
        if ($(this).is(".c-trustbox-gallery")) {
                $(this).slick({
                    infinite: true,            
                    slidesToShow : 5,
                    slidesToScroll: 1,              
                    autoplay: true,
                    autoplaySpeed: 3000,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 992,
                            settings: {
                                centerMode: true,
                                centerPadding: '220px',
                                slidesToShow   : 1,
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true,
                                centerPadding: '150px',
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true,
                                centerPadding: '40px',
                            }
                        },
                    ]
                });
        };

        if ($(this).is(".c-logo-strip__logos")) {
            $(this).slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                arrows: false
            }); 
        }

        if ($(this).is(".brand-slider")) {
            $(this).slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            centerMode: true,
                            centerPadding: '180px',
                            slidesToShow   : 1,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '140px',
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '100px',
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '25px',
                        }
                    },
                ]
            }); 
        }

        if($('.c-calendar-slider').length) {
            $('.c-calendar-slider').slick({
                infinite: true,        
                prevArrow:'<span class="slick-prev"><i class="fal fa-angle-left"></i></span>',
                nextArrow:'<span class="slick-next"><i class="fal fa-angle-right"></i></span>'
            });
        }

        if($('.calendar-title-slider').length){
            $('.calendar-title-slider').slick({
                infinite: true,        
                asNavFor: '.calendar-date',
                arrows: false,
                fade: true,
                responsive: [
                    {
                        breakpoint: 641,
                        settings: {
                            arrows: true,
                            prevArrow:'<span class="slick-prev"><i class="fal fa-angle-left"></i></span>',
                            nextArrow:'<span class="slick-next"><i class="fal fa-angle-right"></i></span>'
                        }
                    }
                ]
            });
        }

        if($('.calendar-date').length){
            $('.calendar-date').slick({
                infinite: true,        
                fade: true,
                asNavFor: '.calendar-title-slider',
                prevArrow:'<span class="slick-prev"><i class="fal fa-angle-left"></i></span>',
                nextArrow:'<span class="slick-next"><i class="fal fa-angle-right"></i></span>',
                responsive: [
                    {
                        breakpoint: 641,
                        settings: {
                            arrows: false,
                        }
                    }
                ]
            });
        }
    });
});