var Divido = (function() {
    var Divido = function(element, options) {
        this.finances = [{
            "id": "50747d2b-a290-4368-88dd-e00a36f284c6",
            "country": "GB",
            "text": "36 months interest bearing (9.90%)",
            "interest_rate": 9.9,
            "min_amount": 0,
            "min_deposit": 10,
            "max_deposit": 50,
            "setup_fee": 0,
            "instalment_fee": 0,
            "agreement_duration":36,
            "deferral_period": 0,
            "calculation_family": "d9401b9e8d7dbff9040fddf2b54f1d5b",
            "lender_code": "0.0"
        }, {
            "id": "5c2a0067-bdb0-4c5b-a12b-0c7bc3594053",
            "country": "GB",
            "text": "60 months interest bearing (9.90%)",
            "interest_rate": 9.9,
            "min_amount": 0,
            "min_deposit": 10,
            "max_deposit": 50,
            "setup_fee": 0,
            "instalment_fee": 0,
            "agreement_duration": 60,
            "deferral_period": 0,
            "calculation_family": "ca326d0cb730607e594b13f2fa06ec6d",
            "lender_code": "0.0"
        }];
        this.settings = {};

        window.divido_finances = this.finances;

        this.type = null;

        // Initialize empty, to use just as calculator
        if (element == undefined) {
            return true;
        }

        if (element.jquery) {
            element = element[0];
        }
        this.element = element;

        this.data = {
            amount: 0,
            finance: {},
            minDeposit: 1,
            maxDeposit: 50,
            depositPercentage: 0,
            depositAmount: 0,
            totalPayable: 0,
            creditAmount: 0,
            monthlyInstalment: 0,
            agreementDuration: 0,
            interestRate: 0,
            financeCost: 0,
            amountMin: 250,
            amountMax: 10000,
            language: 'en',
        };

        this.regional = {
            "GB": {
                header: "Split your payments",
                choosePlan: "Choose your plan",
                chooseDeposit: "Choose your deposit",
                monthlyPayments: "<span data-divido-agreement-duration></span> monthly payments of <span data-divido-monthly-instalment></span>",
                term: "Term",
                months: "months",
                monthlyInstalments: "Monthly instalments",
                deposit: "Deposit",
                costOfCredit: "Cost of credit",
                totalPayable: "Total payable",
                totalInterest: false,
                totalInterestAPR: "Total interest APR",
                setupFee: false,
                administrationFee: false,
                fieldOrder: [0, 1, 2, 3, 4, 5],
                currency: {
                    decimalPlaces: 2,
                    prefix: "&pound;",
                    suffix: ""
                }
            },
            "SE": {
                header: "Delbetala",
                choosePlan: "V&auml;lj finansalternativ",
                chooseDeposit: "V&auml;lj deposition",
                monthlyPayments: "M&aring;nadskostnad: <span data-divido-monthly-instalment></span>",
                term: "Avtalsl&auml;ngd",
                months: "m&aring;n",
                monthlyInstalments: false,
                deposit: "Deposition",
                costOfCredit: "Kreditkostnad",
                totalPayable: "Totalt att betala",
                totalInterest: false,
                totalInterestAPR: "Effektiv r&auml;nta",
                setupFee: "Uppl&auml;gg. avgift",
                administrationFee: "Adm. avgift",
                fieldOrder: [0, 2, 5, 3, 4],
                currency: {
                    decimalPlaces: 0,
                    prefix: "",
                    suffix: "kr"
                }
            },
            "DE": {
                header: "Bezahlen Sie in Raten",
                choosePlan: "W&auml;hlen Sie Ihren Zahlungsplan",
                chooseDeposit: "Legen Sie Ihre Anzahlung fest",
                monthlyPayments: "<span data-divido-agreement-duration></span> monatliche Zahlungen &aacute; <span data-divido-monthly-instalment></span>",
                term: "Dauer",
                months: "Monate",
                monthlyInstalments: "monatliche Raten",
                deposit: "Anzahlung",
                costOfCredit: "Finanzierungskosten",
                totalPayable: "insgesamt zu Bezahlen",
                totalInterest: "Gesamtsumme der Zinsen",
                totalInterestAPR: "Zinssatz",
                setupFee: "Installationsgeb&uauml;hr",
                administrationFee: "Administrationsgebühr",
                fieldOrder: [0, 2, 5, 3, 4],
                currency: {
                    decimalPlaces: 2,
                    prefix: "&euro;",
                    suffix: ""
                }
            },
            "FI": {
                header: "Maksu osissa",
                choosePlan: "Valitse suunnitelma",
                chooseDeposit: "Valitse takuu",
                monthlyPayments: "<span data-divido-agreement-duration></span> kuukausier&auml;n suuruus on <span data-divido-monthly-instalment></span>",
                term: "Laineaika",
                months: "Kuukaudet",
                monthlyInstalments: "Kuukausier&auml;t",
                deposit: "K&auml;siraha",
                costOfCredit: "Kokonaiskustannus luotolle",
                totalPayable: "Yhteens&auml; maksettava",
                totalInterest: "Kokonaiskorko",
                totalInterestAPR: "Todellinen vuosikorko",
                setupFee: "J&auml;rjestelypalkkio",
                administrationFee: "Laskutuspalkkio",
                fieldOrder: [0, 1, 2, 3, 4, 5],
                currency: {
                    decimalPlaces: 2,
                    prefix: "&euro;",
                    suffix: ""
                }
            },
            "NO": {
                header: "Delbetal",
                choosePlan: "Velg betalingsalternativ",
                chooseDeposit: "Velg innskudd",
                monthlyPayments: "<span data-divido-agreement-duration></span> m&aring;nedlige betalinger p&aring; <span data-divido-monthly-instalment></span>",
                term: "Periode",
                months: "m&aring;neder",
                monthlyInstalments: "M&aring;nedlige avdrag",
                deposit: "Innskudd",
                costOfCredit: "Kredittkostnad",
                totalPayable: "Totalt &aring; betale",
                totalInterest: "Totalrente",
                totalInterestAPR: "Totalrente",
                setupFee: "Etableringsgebyr",
                administrationFee: "Administrasjonsgebyr",
                fieldOrder: [0, 1, 2, 3, 4, 5],
                currency: {
                    decimalPlaces: 0,
                    prefix: "",
                    suffix: "kr"
                }
            },
            /*
				 "": {
				 header: "",
				 choosePlan: "",
				 chooseDeposit: "",
				 monthlyPayments: "",
				 term: "",
				 months: "",
				 monthlyInstalments: "",
				 deposit: "",
				 costOfCredit: "",
				 totalPayable: "",
				 totalInterest: "",
				 totalInterestAPR: "",
				 setupFee: "",
				 administrationFee: "",
				 fieldOrder: [0, 1, 2, 3, 4, 5],
				 currency: {
				 prefix: "",
				 suffix: ""
				 }
				 },
				 */
        };

        this.fields = [function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-term">' + ctx.getText('term') + '</span></dt><dd class="divido-widget-data"><span data-divido-agreement-duration></span> <span class="lang-months">' + ctx.getText('months') + '</span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-monthlyInstalments">' + ctx.getText('monthlyInstalments') + '</span></dt><dd class="divido-widget-data"><span data-divido-monthly-instalment></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-deposit">' + ctx.getText('deposit') + '</span></dt><dd class="divido-widget-data"><span data-divido-deposit></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-costOfCredit">' + ctx.getText('costOfCredit') + '</span></dt><dd class="divido-widget-data"><span data-divido-finance-cost-rounded></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-totalPayable">' + ctx.getText('totalPayable') + '</span></dt><dd class="divido-widget-data"><span data-divido-total-payable-rounded></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-totalInterestAPR">' + ctx.getText('totalInterestAPR') + '</span></dt><dd class="divido-widget-data"><span data-divido-interest-rate></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-setupFee">' + ctx.getText('setupFee') + '</span></dt><dd class="divido-widget-data"><span data-divido-setup-fee></span></dd>')
        }
        , function(ctx) {
            return ('<dt class="divido-widget-term"><span class="lang-administrationFee">' + ctx.getText('administrationFee') + '</span></dt><dd class="divido-widget-data"><span data-divido-administration-fee></span></dd>')
        }
        , ];

        this.initCalc(options);

        this.render();
    }

    Divido.prototype.calculators = {};

    Divido.prototype.calculators["default"] = function(finance, amount, depositPercentage) {
        var interestRate = finance.interest_rate;
        var agreementDuration = finance.agreement_duration;
        var deferralPeriod = finance.deferral_period;

        var depositAmount = parseFloat((depositPercentage / 100) * amount).toFixed(2);
        var creditAmount = parseFloat(amount - depositAmount).toFixed(2);
        var monthlyInstalment = 0;
        var costOfCredit = 0;

        if (deferralPeriod > 0) {

            var totalDuration = agreementDuration + deferralPeriod;
            var iRate = parseFloat(interestRate * 0.01) + 1;
            var monthlyRate = Math.pow(iRate, (1 / 12)) - 1;
            var nominalAnnualRate = monthlyRate * 12;
            var postInitBalance;
            var monthlyPayment = 0;

            var table = [];
            table.push({
                month: 0,
                starting_balance: 0,
                interest_accrued: 0,
                monthly_payment: 0,
                end_balance: parseFloat(creditAmount)
            });

            for (var i = 0; i < totalDuration; i++) {
                var lastRow = table[i];

                var month = lastRow.month + 1;

                var startingBalance = lastRow.end_balance;
                startingBalance = parseFloat(startingBalance.toFixed(2));

                var interestAccrued = startingBalance * monthlyRate;
                interestAccrued = parseFloat(interestAccrued.toFixed(2));

                if (month == deferralPeriod + 1) {
                    postInitBalance = lastRow.end_balance;
                    if (monthlyRate > 0) {
                        monthlyPayment = (postInitBalance * monthlyRate) / (1 - Math.pow((1 + monthlyRate), -1 * agreementDuration));
                    } else {
                        monthlyPayment = postInitBalance / agreementDuration;
                    }

                    postInitBalance = parseFloat(postInitBalance.toFixed(2));
                    monthlyPayment = parseFloat(monthlyPayment.toFixed(2));
                }

                var endBalance = startingBalance + interestAccrued - monthlyPayment;
                endBalance = parseFloat(endBalance.toFixed(2));
                if (endBalance < 1) {
                    endBalance = 0;
                }

                var row = {
                    month: month,
                    starting_balance: startingBalance.toFixed(2),
                    interest_accrued: interestAccrued.toFixed(2),
                    monthly_payment: monthlyPayment,
                    end_balance: endBalance
                };

                table.push(row);
            }

            monthlyInstalment = monthlyPayment;
            for (var i = 0; i < table.length; i++) {
                costOfCredit += table[i].interest_accrued;
            }

        } else if (interestRate > 0) {
            var compound = parseFloat((1 / 12).toFixed(8));
            var yrRate = parseFloat(((parseFloat(interestRate) * 1.0) / 100).toFixed(8));
            var rdefine = parseFloat((Math.pow((1.0 + yrRate), compound) - 1.0).toFixed(8));
            monthlyInstalment = parseFloat(((creditAmount * rdefine * (Math.pow((1.0 + rdefine), agreementDuration))) / ((Math.pow((1.0 + rdefine), agreementDuration)) - 1.0))).toFixed(2);

        } else {
            monthlyInstalment = parseFloat(creditAmount / agreementDuration).toFixed(2);
        }

        var interestRate = parseFloat(interestRate).toFixed(1);
        var financeCost = parseFloat((monthlyInstalment * agreementDuration) - creditAmount).toFixed(2);
        var totalPayable = parseFloat((monthlyInstalment * agreementDuration) + parseFloat(depositAmount)).toFixed(2);

        if (financeCost < 0.5) {
            financeCost = 0;
        }

        var calculation = {
            parent: this,
            finance: finance,
            depositAmount: parseFloat(depositAmount),
            creditAmount: parseFloat(creditAmount),
            interestRate: parseFloat(interestRate),
            monthlyInstalment: parseFloat(monthlyInstalment),
            interestRate: parseFloat(interestRate),
            financeCost: parseFloat(financeCost),
            totalPayable: parseFloat(totalPayable),
            agreementDuration: parseInt(agreementDuration)
        };

        return calculation;

    }
    ;

    Divido.prototype.setLanguage = function() {
        var finance = this.getFinance();
        var language = finance.country || "GB";
        if (typeof this.regional[language] == 'object') {
            this.data.language = language;
        }
    }

    Divido.prototype.formatCurrency = function(money) {
        var currency = this.regional[this.data.language].currency;

        if (currency.decimalPlaces != undefined && typeof money === 'number') {
            money = money.toFixed(currency.decimalPlaces);
        }

        return currency.prefix + money + currency.suffix;
    }

    Divido.prototype.getText = function(name) {
        return this.regional[this.data.language][name] !== undefined ? this.regional[this.data.language][name] : this.regional["GB"][name];
    }

    Divido.prototype.attach = function() {
        switch (document.readyState) {
        case "loading":
            document.addEventListener('DOMContentLoaded', this.startup);
            break;

        case "interactive":
        case "complete":
            this.startup();
            break;
        }
    }
    ;

    Divido.prototype.startup = function() {
        var calculators = document.querySelectorAll('[data-divido-calculator], [data-divido-widget], [data-divido-preview]');
        Array.prototype.forEach.call(calculators, function(calculator, i) {
            var calc = new Divido(calculator);
        });
    }

    Divido.prototype.initCalc = function(options) {
        var el = this.element;

        this.type = 'fixed';
        if (el.getAttribute('data-divido-widget') != null) {
            this.type = 'popup';
        } else if (el.getAttribute('data-divido-calculator') != null) {
            this.type = 'fixed';
        } else if (el.getAttribute('data-divido-preview') != null) {
            this.type = 'preview';
        }

        this.settings.apiKey = null;
        var script = document.querySelector("script[data-divido-public-key]");
        if (script && script.getAttribute('data-divido-public-key')) {
            this.settings.apiKey = script.getAttribute('data-divido-public-key');
        }

        var product = el.getAttribute('data-divido-product');
        var quantity = el.getAttribute('data-divido-quantity');
        var amount = el.getAttribute('data-divido-amount');
        var amountMin = el.getAttribute('data-divido-amount-min');
        var amountMax = el.getAttribute('data-divido-amount-max');
        var depositPercentage = el.getAttribute('data-divido-deposit');
        var defaultFinance = el.getAttribute('data-divido-plan');
        var plans = el.getAttribute('data-divido-plans');
        var applyAttr = el.getAttribute('data-divido-apply');
        var applyLabel = el.getAttribute('data-divido-apply-label');
        var footnote = el.getAttribute('data-divido-footnote');
        var prefix = el.getAttribute('data-divido-prefix');
        var suffix = el.getAttribute('data-divido-suffix');
        var styles = el.getAttribute('data-divido-styles');
        var showLogo = !(el.getAttribute('data-divido-logo') === 'false');

        if (typeof options == 'object') {
            if ('product'in options) {
                product = options.product;
            }

            if ('quantity'in options) {
                quantity = options.quantity;
            }

            if ('amount'in options) {
                amount = options.amount;
            }

            if ('amountMin'in options) {
                amountMin = options.amountMin;
            }

            if ('amountMax'in options) {
                amountMax = options.amountMax;
            }

            if ('deposit'in options) {
                depositPercentage = options.deposit;
            }

            if ('plan'in options) {
                defaultFinance = options.plan;
            }

            if ('plans'in options) {
                plans = options.plans;
            }

            if ('apply'in options) {
                applyAttr = options.apply;
            }

            if ('applyLabel'in options) {
                applyLabel = options.applyLabel;
            }

            if ('footnote'in options) {
                applyDesc = options.applyDesc;
            }

            if ('prefix'in options) {
                prefix = options.prefix;
            }

            if ('suffix'in options) {
                suffix = options.suffix;
            }

            if ('styles'in options) {
                styles = options.styles;
            }

            if ('logo'in options) {
                showLogo = options.logo;
            }
        }

        if (plans && typeof plans == "string") {
            plans = plans.trim().replace(' ', '').split(',')
        }

        amount = parseFloat(amount.toString().replace(/(\d+),(?=[\d,]*\.\d{2}\b)/g, "$1"));

        if (!applyLabel) {
            applyLabel = 'Apply now';
        }

        if (!footnote) {
            footnote = '';
        }

        if (!prefix) {
            prefix = 'or';
        }

        if (!suffix) {
            suffix = '';
        }

        this.initWidgetStyles(styles);
        this.initPrevStyles(styles);

        this.settings.product = product;
        this.settings.quantity = quantity;
        this.settings.defaultFinance = defaultFinance;
        this.settings.applyAttr = applyAttr;
        this.settings.applyLabel = applyLabel;
        this.settings.footnote = footnote;
        this.settings.prefix = prefix;
        this.settings.suffix = suffix;
        this.settings.showLogo = showLogo;
        this.settings.showApply = applyAttr && applyAttr != 'false' && applyAttr != '0' && product;

        if (amountMin) {
            this.data.amountMin = amountMin;
        }

        if (amountMax) {
            this.data.amountMax = amountMax;
        }

        if (plans) {
            this.setShow(plans);
        }

        this.setAmount(amount);
        this.setFinance(this.finances[0].id);
        if (!isNaN(parseFloat(depositPercentage)) && isFinite(depositPercentage)) {
            this.setDepositPercentage(depositPercentage);
        }
    }
    ;

    Divido.prototype.render = function() {
        switch (this.type) {
        case "popup":
            this.renderPopup();
            break;
        case "fixed":
            this.renderFixed();
            break;
        case "preview":
            this.renderPreview();
            break;
        }
    }
    ;

    Divido.prototype.setFinance = function(financeId, callback) {
        for (var i = 0; i < this.finances.length; i++) {
            if (this.finances[i].id == financeId) {
                this.data.finance = this.finances[i];
            }
        }
        if (typeof this.data.finance == 'undefined' && this.finances.length > 0) {
            this.data.finance = this.finances[0];
        }
        if (typeof this.data.finance == 'object') {
            this.data.interestRate = this.data.finance.interest_rate;
            this.data.agreementDuration = this.data.finance.agreement_duration;
            this.data.deferral_period = this.data.finance.deferral_period;
            this.data.minDeposit = this.data.finance.min_deposit;
            this.data.maxDeposit = this.data.finance.max_deposit;
            this.data.minAmount = this.data.finance.min_amount;

            var depositPercentage = (!(this.data.depositPercentage >= this.data.minDeposit && this.data.depositPercentage <= this.data.maxDeposit)) ? this.data.minDeposit : this.data.depositPercentage;
            this.setDepositPercentage(depositPercentage);
            this.setLanguage();
            this.renderLanguage();
        }
        if (typeof callback === "function") {
            return callback();
        }
    }
    ;

    Divido.prototype.setShow = function(finances) {

        var newFinances = [];

        if (finances.length > 0) {

            var _finances = {};

            for (var i = 0; i < this.finances.length; i++) {
                _finances[this.finances[i].id] = this.finances[i];
            }

            var finance = this.finance;

            for (var i = 0; i < finances.length; i++) {
                var finance = finances[i];

                if (typeof _finances[finance] == 'object') {
                    newFinances.push(_finances[finance]);
                }
            }
        }

        this.finances = newFinances;
    }
    ;

    Divido.prototype.doCalculate = function(finance, amount, depositPercentage, callback) {
        var calculator;

        if (finance.calculation_family != undefined && this.calculators.hasOwnProperty(finance.calculation_family)) {
            calculator = this.calculators[finance.calculation_family];
        } else {
            calculator = this.calculators["default"];
        }

        var calculation = calculator(finance, amount, depositPercentage, callback);

        if (typeof callback === "function") {
            return callback(calculation);
        } else {
            return calculation;
        }
    }

    Divido.prototype.calculate = function(element, callback) {
    	console.log(element)
        if (!this.data.finance) {
            return false;
        }

        var plan = this.data.finance;

        if (element && element.getAttribute('data-divido-plan')) {
            var planId = element.getAttribute('data-divido-plan');
            Array.prototype.forEach.call(this.finances, function(finance, i) {
                if (finance.id == planId) {
                    plan = finance;
                }
            });

        }

        var self = this;
        this.doCalculate(plan, this.getAmount(), this.getDepositPercentage(), function(result) {
            self.data.finance = plan;
            self.data.agreementDuration = result.agreementDuration;
            self.data.monthlyInstalment = result.monthlyInstalment;
            self.data.financeCost = result.financeCost;
            self.data.creditAmount = result.creditAmount;
            self.data.totalPayable = result.totalPayable;
            self.data.interestRate = result.interestRate;

            if (typeof callback === "function") {
                return callback();
            }
        }
        .bind(this));
    }
    ;

    Divido.prototype.getMinDeposit = function() {
        return this.data.minDeposit;
    }
    ;

    Divido.prototype.getMaxDeposit = function() {
        var maxDeposit = Math.floor(((this.data.amount - this.data.finance.min_amount) / this.data.amount) * 100);

        if (maxDeposit > this.data.finance.min_deposit && maxDeposit < this.data.finance.max_deposit) {
            this.data.maxDeposit = maxDeposit;
        }

        return this.data.maxDeposit;
    }
    ;

    Divido.prototype.getTotalPayable = function() {
        return this.data.totalPayable;
    }
    ;

    Divido.prototype.getCreditAmount = function() {
        return this.data.creditAmount;
    }
    ;

    Divido.prototype.getMonthlyInstalment = function() {
        return this.data.monthlyInstalment;
    }
    ;

    Divido.prototype.getAgreementDuration = function() {
        return this.data.agreementDuration;
    }
    ;

    Divido.prototype.getInterestRate = function() {
        return this.data.interestRate;
    }
    ;

    Divido.prototype.getFinanceCost = function() {
        return this.data.financeCost;
    }
    ;

    Divido.prototype.getFinances = function() {
        return this.finances;
    }
    ;

    Divido.prototype.setAmount = function(amount) {
        amount = parseFloat(amount.toString().replace(/(\d+),(?=[\d,]*\.\d{2}\b)/g, "$1"));
        this.data.amount = amount;
    }
    ;

    Divido.prototype.getAmount = function() {
        return this.data.amount;
    }
    ;

    Divido.prototype.getAmountMin = function() {
        return this.data.amountMin;
    }
    ;

    Divido.prototype.getAmountMax = function() {
        return this.data.amountMax;
    }
    ;

    Divido.prototype.getFinance = function() {
        return this.data.finance;
    }
    ;

    Divido.prototype.setDepositPercentage = function(depositPercentage) {
        this.data.depositPercentage = depositPercentage;

        if (depositPercentage >= 0) {
            this.data.depositAmount = (depositPercentage / 100) * this.getAmount();
        }
    }
    ;

    Divido.prototype.getDepositPercentage = function() {
        return this.data.depositPercentage;
    }
    ;

    Divido.prototype.setDepositAmount = function(depositAmount) {
        this.data.depositAmount = depositAmount;

        if (this.data.depositAmount > 0) {
            this.data.depositPercentage = (this.data.depositAmount / this.data.amount) * 100;
        }
    }
    ;

    Divido.prototype.getLowestMonthlyInstalment = function() {

        var monthlyInstalment = this.getAmount();
        var lowestFinance = this.finances[0];

        for (var i = 0; i < this.finances.length; i++) {
            var finance = this.finances[i];
            this.doCalculate(finance, this.getAmount(), finance.min_deposit, function(result) {
                if (result.monthlyInstalment > 0 && result.monthlyInstalment < monthlyInstalment) {
                    monthlyInstalment = result.monthlyInstalment;
                    lowestFinance = result.finance;
                }
            });
        }
        if (typeof lowestFinance == 'object') {
            this.setFinance(lowestFinance.id);
        } else {
            this.setFinance(this.finances[i]);
        }
        this.setDepositPercentage(lowestFinance.min_deposit);

        this.calculate();

        return monthlyInstalment;
    }
    ;

    Divido.prototype.getDepositAmount = function() {
        return this.data.depositAmount;
    }
    ;

    Divido.prototype.renderLanguage = function() {
        this.renderFields();

        for (var key in this.regional[this.data.language]) {
            var els = document.getElementsByClassName("lang-" + key);
            if (els.length > 0) {
                var string = this.regional[this.data.language][key];
                for (var i = els.length - 1; i >= 0; i--) {
                    els[i].innerHTML = string;
                }
            }
        }
    }

    Divido.prototype.renderFields = function(returnMarkup) {
        var fieldOrder = this.regional[this.data.language].fieldOrder;
        var fields = '';

        for (var i = 0; i < fieldOrder.length; i++) {
            var markup = this.fields[fieldOrder[i]](this);
            fields += markup;
        }

        if (returnMarkup) {
            return fields;
        } else {
            var el = document.getElementsByClassName('divido-widget-info');
            if (el.length > 0)
                el[0].innerHTML = fields;
        }
    }

    Divido.prototype.launchPopup = function(caller, direction) {

        var html = '<span class="divido-widget-close">&#x2715;</span><div class="divido-widget-heading lang-header">' + this.getText('header') + '</div>' + '<span class="divido-widget-control" data-divido-choose-finance data-divido-label="' + this.getText('choosePlan') + '" data-divido-form="finance"></span>' + '<div class="divido-widget-control" data-divido-choose-deposit data-divido-label="' + this.getText('chooseDeposit') + '" data-divido-form="deposit"></div>' + '<div class="divido-widget-summary lang-monthlyPayments">' + this.getText('monthlyPayments') + '</div>' + '<dl class="divido-info divido-widget-info">' + this.renderFields(true) + '</dl>';

        var wrapper = document.createElement("div");
        wrapper.className = "divido-widget-wrapper";
        wrapper.innerHTML = html;

        if (this.settings.showApply) {
            var applyNowLabelText = document.createTextNode(this.settings.applyLabel);

            var _applyNow = document.createElement('a');
            _applyNow.setAttribute('href', '#');
            _applyNow.className += ' applyButton divido-widget-apply-link';
            _applyNow.appendChild(applyNowLabelText);

            var self = this;
            var applyHandler = function(e) {
                var invoker = e.target;
                var className = 'applyButton';

                if (!new RegExp('(^| )' + className + '( |$)','gi').test(invoker.className)) {
                    return;
                }

                e.preventDefault();

                window.open('http://apply.divido.com/?pk=' + self.settings.apiKey + '&f=' + self.getFinance().id + '&a=' + encodeURIComponent(self.getAmount()) + '&d=' + self.getDepositPercentage() + '&p=' + encodeURIComponent(self.settings.product), '_blank');
            };

            document.body.addEventListener('click', applyHandler, false);
            wrapper.appendChild(_applyNow);
        }

        var footnoteText = document.createTextNode(this.settings.footnote);
        if (footnoteText) {
            var footnote = document.createElement('div');
            footnote.className = 'divido-widget-footnote';
            footnote.appendChild(footnoteText);
            wrapper.appendChild(footnote);
        }

        function closeHandler(e) {
            var closeClick = e.target.className.match(/divido-widget-close/);
            var outSideClick = wrapper !== e.target && !wrapper.contains(e.target);

            if (closeClick || outSideClick) {
                document.removeEventListener('mouseup', closeHandler);
                wrapper.parentNode.removeChild(wrapper);
            }
        }
        ;
        document.addEventListener('mouseup', closeHandler);

        this.renderCalculator(wrapper);

        var container = document.createElement("div");
        container.className = "divido-widget-container";
        container.setAttribute('data-divido-calculator', '');
        container.setAttribute('data-divido-amount', this.getAmount());
        container.appendChild(wrapper);

        var left = caller.getBoundingClientRect().left;
        var sX = (window.scrollX || window.pageXOffset);
        left += sX;
        left = (left < 10) ? 8 : left;

        var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var rightBoundary = left + 360;
        if (rightBoundary > width) {
            left += (width - rightBoundary);
        }

        var top = caller.getBoundingClientRect().top;
        var sY = (window.scrollY || window.pageYOffset);
        top += sY;

        container.style.left = left + 'px';
        container.style.top = top + 'px';

        document.body.appendChild(container);

        if (direction === "up") {
            container.style.top = top - container.clientHeight + 'px';
        }
    }
    ;

    Divido.prototype.renderPreview = function() {
        if (this.getFinances().length <= 0) {
            return false;
        }

        var preview = '<div class="divido-prev-container">' + '<div class="divido-prev-content">' + '<img src="//cdn.divido.com/images/logo-white-40x12.png" class="divido-prev-logo">' + '<span class="divido-prev-header">Divido preview calculator</span>' + '<div class="divido-prev-control" data-divido-choose-amount data-divido-label="Choose your amount" data-divido-form="amount"></div>' + '<div class="divido-prev-control" data-divido-choose-deposit data-divido-label="Choose your deposit" data-divido-form="deposit"></div>' + '<div class="divido-prev-info">Financed amount: <span>&pou</span><span data-divido-financed-amount></span></div>';

        Array.prototype.forEach.call(this.getFinances(), function(plan, i) {
            preview += '<dl class="divido-prev-plan divido-info" data-divido-plan="' + plan.id + '">' + '<dd class="divido-prev-data"><span class="divido-prev-plan-name" data-divido-finance></span></dd>' + '<dt class="divido-prev-term">Term</dt>' + '<dd class="divido-prev-data"><span data-divido-agreement-duration></span> months</dd>' + '<dt class="divido-prev-term">Monthly instalment</dt>' + '<dd class="divido-prev-data"><span data-divido-monthly-instalment></span></dd>' + '<dt class="divido-prev-term">Deposit</dt>' + '<dd class="divido-prev-data"><span data-divido-deposit></span></dd>' + '<dt class="divido-prev-term">Cost of credit</dt>' + '<dd class="divido-prev-data"><span data-divido-finance-cost-rounded></span></dd>' + '<dt class="divido-prev-term">Total payable</dt>' + '<dd class="divido-prev-data"><span data-divido-total-payable-rounded></span></dd>' + '<dt class="divido-prev-term">Total interest APR</dt>' + '<dd class="divido-prev-data"><span data-divido-interest-rate></span></dd>' + '</dl>';
        }
        .bind(this));

        preview += '</div></div>';
        this.element.innerHTML = preview;
        this.renderCalculator(this.element);
    }
    ;

    Divido.prototype.renderPopup = function() {
        debugger;
        this.element.innerHTML = "";

        if (this.getFinances().length > 0) {

            if (this.settings.defaultFinance) {
                this.setFinance(this.settings.defaultFinance);

                var amount = this.getAmount();
                var depositPercentage = this.getDepositPercentage();
                var finance = this.getFinance();
                var obj = this.doCalculate(finance, amount, depositPercentage);

                var monthlyInstalment = obj.monthlyInstalment;
            } else {
                var monthlyInstalment = this.getLowestMonthlyInstalment();
            }

            var clickHandler = this.popupClickHandler.bind(this);

            var widgetPrefix = document.createTextNode(this.settings.prefix + ' ');
            var widgetSuffix = document.createTextNode(' ' + this.settings.suffix);

            var widgetAnchorText = this.formatCurrency(monthlyInstalment) + ' per month';
            var widgetAnchor = document.createElement('a');
            widgetAnchor.setAttribute('href', '#');
            widgetAnchor.innerHTML = widgetAnchorText;
            widgetAnchor.addEventListener('click', clickHandler);
            var widget = document.createElement('span');
            widget.className = 'divido-widget-launcher';
            widget.appendChild(widgetPrefix);
            widget.appendChild(widgetAnchor);
            widget.appendChild(widgetSuffix);

            this.element.appendChild(widget);

            if (this.settings.showLogo) {
                var logoImg = document.createElement('img');
                logoImg.className = 'divido-widget-logo';
                logoImg.setAttribute('src', '//cdn.divido.com/images/logo-white-40x12.png');
                logoImg.setAttribute('alt', 'Divido');

                var logo = document.createElement('a');
                logo.className = 'divido-widget-logo-link';
                logo.appendChild(logoImg);
                logo.addEventListener('click', clickHandler);

                this.element.appendChild(logo);
            }
        }
    }
    ;

    Divido.prototype.renderFixed = function() {

        if (this.settings.defaultFinance) {
            this.setFinance(this.settings.defaultFinance);
        }

        if (this.getFinances().length > 0) {
            if (this.settings.defaultPlan) {
                this.setFinance(this.settings.defaultPlan);
                obj = divido.doCalculate(this.getFinance(), this.getAmount(), this.getDepositPercentage());
                var monthlyInstalment = obj.monthlyInstalment;
            } else {
                var monthlyInstalment = this.getLowestMonthlyInstalment();
            }

            this.renderCalculator(this.element);
        }
    }
    ;

    Divido.prototype.updateLabels = function() {
    }

    Divido.prototype.popupClickHandler = function(event) {
        var space = window.innerHeight - event.target.offsetTop;
        var direction = space < 300 ? "up" : "down";
        event.preventDefault();
        this.launchPopup(event.target, direction);
    }
    ;

    Divido.prototype.recalculate = function(calculator, infoElements) {
        var self = this;

        var globalAgreementDuration = calculator.querySelector('[data-divido-agreement-duration]');
        if (globalAgreementDuration) {
            globalAgreementDuration.innerHTML = self.getAgreementDuration().toFixed(2).replace('.00', "");
        }

        var globalMonthlyInstalment = calculator.querySelector('[data-divido-monthly-instalment]');
        if (globalMonthlyInstalment) {
            globalMonthlyInstalment.innerHTML = self.formatCurrency(self.getMonthlyInstalment().toFixed(2).replace('.00', ""));
        }

        Array.prototype.forEach.call(infoElements, function(element, i) {
            self.calculate(element, function() {
                var finance, agreementDuration, monthlyInstalment, monthlyInstalmentRounded, deposit, depositRounded, depositPercentage, creditAmount, creditAmountRounded, financeCost, financeCostRounded, totalPayable, totalPayableRounded, interestRate;

                if (finance = element.querySelector("[data-divido-finance]")) {
                    finance.innerHTML = self.getFinance().text;
                }

                if (agreementDuration = element.querySelectorAll("[data-divido-agreement-duration]")) {
                    Array.prototype.forEach.call(agreementDuration, function(elmt, i) {
                        elmt.innerHTML = self.getAgreementDuration().toFixed(2).replace('.00', "");
                    });
                }

                if (monthlyInstalment = element.querySelectorAll("[data-divido-monthly-instalment]")) {
                    Array.prototype.forEach.call(monthlyInstalment, function(elmt, i) {
                        elmt.innerHTML = self.formatCurrency(self.getMonthlyInstalment().toFixed(2).replace('.00', ""));
                    });
                }

                if (monthlyInstalmentRounded = element.querySelectorAll("[data-divido-monthly-instalment-rounded]")) {
                    Array.prototype.forEach.call(monthlyInstalmentRounded, function(elmt, i) {
                        elmt.innerHTML = self.formatCurrency(self.getMonthlyInstalment().toFixed(0).replace('.00', ""));
                    });
                }

                if (deposit = element.querySelector("[data-divido-deposit]")) {
                    deposit.innerHTML = self.formatCurrency(self.getDepositAmount().toFixed(2).replace('.00', ""));
                }

                if (depositRounded = element.querySelector("[data-divido-deposit-rounded]")) {
                    depositRounded.innerHTML = self.formatCurrency(self.getDepositAmount().toFixed(0).replace('.00', ""));
                }

                // Hotfix
                var isOC = window.hasOwnProperty('cart') && window.cart.hasOwnProperty('add') && window.cart.hasOwnProperty('remove');
                if (creditAmount = element.querySelector("[data-divido-credit-amount]")) {
                    if (isOC) {
                        // Hotfix
                        creditAmount.innerHTML = self.formatCurrency(self.getFinanceCost().toFixed(2).replace('.00', ""));
                    } else {
                        creditAmount.innerHTML = self.formatCurrency(self.getCreditAmount().toFixed(2).replace('.00', ""));
                    }
                }

                if (creditAmountRounded = element.querySelector("[data-divido-credit-amount-rounded]")) {
                    if (isOC) {
                        // Hotfix
                        creditAmountRounded.innerHTML = self.formatCurrency(self.getFinanceCost().toFixed(0).replace('.00', ""));
                    } else {
                        creditAmountRounded.innerHTML = self.formatCurrency(self.getCreditAmount().toFixed(0).replace('.00', ""));
                    }
                }

                if (financeCost = element.querySelector("[data-divido-finance-cost]")) {
                    financeCost.innerHTML = self.formatCurrency(self.getFinanceCost().toFixed(2).replace('.00', ""));
                }

                if (financeCostRounded = element.querySelector("[data-divido-finance-cost-rounded]")) {
                    financeCostRounded.innerHTML = self.formatCurrency(self.getFinanceCost().toFixed(0).replace('.00', ""));
                }

                if (totalPayable = element.querySelector("[data-divido-total-payable]")) {
                    totalPayable.innerHTML = self.formatCurrency(self.getTotalPayable().toFixed(2).replace('.00', ""));
                }

                if (totalPayableRounded = element.querySelector("[data-divido-total-payable-rounded]")) {
                    totalPayableRounded.innerHTML = self.formatCurrency(self.getTotalPayable().toFixed(0).replace('.00', ""));
                }

                if (interestRate = element.querySelector("[data-divido-interest-rate]")) {
                    interestRate.innerHTML = self.getInterestRate() + '%';
                }
            });
        });

    }
    ;

    Divido.prototype.renderChooseDeposit = function(element, infoElements) {
        element.querySelector("[data-divido-choose-deposit]").innerHTML = '';

        if (this.getMinDeposit() != this.getMaxDeposit()) {
            var range, depositChooser, slideHandler, oldIE, _option, min, max, depositChangeHandler;

            oldIE = this.oldIE();

            if (this.getDepositPercentage() > this.getMaxDeposit()) {
                this.setDepositPercentage(this.getMaxDeposit());
            }

            var self = this;
            if (oldIE) {
                range = document.createElement('select');

                min = this.getMinDeposit();
                max = this.getMaxDeposit();
                for (var i = min; i <= max; i++) {
                    _option = document.createElement('option');
                    _option.setAttribute('value', i);
                    _option.innerHTML = i + '%';

                    if (i == this.getDepositPercentage()) {
                        _option.setAttribute('selected', 'selected');
                    }

                    range.appendChild(_option);
                }

                depositChangeHandler = function(event) {
                    self.setDepositPercentage(range.options[range.selectedIndex].value);
                    self.recalculate(element, infoElements);
                }
                ;

                range.addEventListener('change', depositChangeHandler);
            } else {
                range = document.createElement('input');
                range.setAttribute('type', 'range');
                range.setAttribute('max', this.getMaxDeposit());
                range.setAttribute('min', this.getMinDeposit());
                range.setAttribute('value', this.getDepositPercentage());

                slideHandler = function() {
                    window.requestAnimationFrame(function() {
                        var depositPercentage;

                        self.setDepositPercentage(range.value);
                        self.recalculate(element, infoElements);
                        self.updateFinancedAmount(element);

                        if (depositPercentage = element.querySelector("[data-divido-deposit-percentage]")) {
                            depositPercentage.innerHTML = range.value + '%';
                        }
                    });
                }
                ;

                depositChangeHandler = function(event) {
                    self.setDepositPercentage(range.value);
                    self.recalculate(element, infoElements);
                }
                ;

                range.addEventListener('mousedown', function() {
                    slideHandler();
                    range.addEventListener('mousemove', slideHandler);
                });
                range.addEventListener('mouseup', function() {
                    range.removeEventListener('mousemove', slideHandler);
                });
                range.addEventListener('change', slideHandler);

                slideHandler();
            }

            depositChoosers = element.querySelectorAll("[data-divido-choose-deposit]");
            var self = this;
            Array.prototype.forEach.call(depositChoosers, function(depositRange) {
                var form, style, prepend, _rangeDiv, _rangeDivClassName, _rangeSpan, percentageStyle, label, labelText, labelStyle, append;

                if (form = depositRange.getAttribute('data-divido-form')) {
                    range.setAttribute('name', form);
                }

                if (style = depositRange.getAttribute('data-divido-style')) {
                    range.setAttribute('style', style);
                }

                if (prepend = depositRange.getAttribute('data-divido-prepend')) {
                    depositRange.innerHTML = prepend;
                }

                _rangeDiv = document.createElement('span');
                _rangeDivClassName = 'divido-range-slider';

                if (_rangeDiv.classList) {
                    _rangeDiv.classList.add(_rangeDivClassName);
                } else {
                    _rangeDiv.className += ' ' + _rangeDivClassName;
                }

                _rangeDiv.appendChild(range);

                if (!oldIE) {
                    _rangeSpan = document.createElement('span');
                    _rangeSpan.className = "percentage";
                    _rangeSpan.innerHTML = self.getDepositPercentage() + '%';
                    _rangeSpan.setAttribute('data-divido-deposit-percentage', '');
                    if (percentageStyle = depositRange.getAttribute('data-divido-percentage-style')) {
                        _rangeSpan.setAttribute('style', percentageStyle);
                    }

                    _rangeDiv.appendChild(_rangeSpan);
                }

                label = document.createElement('label');
                label.innerHTML = self.getText('chooseDeposit');

                if (labelStyle = depositRange.getAttribute('data-divido-label-style')) {
                    label.setAttribute('style', labelStyle);
                }

                depositRange.appendChild(label);

                depositRange.appendChild(_rangeDiv);

                if (append = depositRange.getAttribute('data-divido-append')) {
                    depositRange.appendChild(append);
                }

            });
        }
    }
    ;

    Divido.prototype.renderChooseFinance = function(element, infoElements) {
        var self = this;
        var defaultFinance = this.getFinance();
        var filterPlans = element.getAttribute('data-divido-filter-plans');
        var minFinancedAmount = this.getAmount() - (this.getAmount() * this.getMinDeposit() / 100);

        if (this.getFinances().length == 1) {
            var finance = element.querySelector("[data-divido-choose-finance]");

            var chooseFinance = document.createElement('input');
            chooseFinance.setAttribute('type', 'hidden');
            chooseFinance.setAttribute('name', finance.getAttribute('data-divido-form'));
            chooseFinance.value = defaultFinance.id;

            finance.appendChild(chooseFinance);

        } else if (this.getFinances().length > 1) {
            var chooseFinance = document.createElement('select');
            var _finances = this.getFinances();
            Array.prototype.forEach.call(_finances, function(finance, i) {
                if (filterPlans && minFinancedAmount < finance.min_amount) {
                    return;
                }

                var selected = (typeof defaultFinance == 'object' && defaultFinance.id == finance.id);
                var financeOption = document.createElement('option');
                if (selected) {
                    financeOption.setAttribute('selected', 'selected');
                }
                financeOption.setAttribute('value', finance.id);
                financeOption.innerHTML = finance.text;
                chooseFinance.appendChild(financeOption);
            });

            chooseFinance.addEventListener('change', function(evt) {
                var caller, planId;
                caller = evt.target;

                planId = caller.options[caller.selectedIndex].value;

                self.setFinance(planId, function() {
                    self.renderChooseDeposit(element, infoElements);
                    self.recalculate(element, infoElements);
                });
            });

            var planChoosers = element.querySelectorAll("[data-divido-choose-finance]");
            Array.prototype.forEach.call(planChoosers, function(planChooser, i) {
                var form, style, labelText, labelStyle;

                planChooser.innerHTML = '';

                if (form = planChooser.getAttribute('data-divido-form')) {
                    chooseFinance.setAttribute('name', form);
                }

                if (style = planChooser.getAttribute('data-divido-style')) {
                    chooseFinance.setAttribute('style', style);
                }

                var label = document.createElement('label');
                label.innerHTML = self.getText('choosePlan');
                label.className = "lang-choosePlan";

                if (labelStyle = planChooser.getAttribute('data-divido-label-style')) {
                    label.setAttribute('style', labelStyle);
                }

                planChooser.appendChild(label);
                planChooser.appendChild(chooseFinance);
            });
        }
    }
    ;

    Divido.prototype.updateFinancedAmount = function(calculator) {
        var financedAmount;

        if (financedAmount = calculator.querySelector('[data-divido-financed-amount]')) {
            financedAmount.innerHTML = parseInt(this.getAmount() - this.getDepositAmount());
        }
    }
    ;

    Divido.prototype.renderChooseAmount = function(element, infoElements) {
        var amountChoosers = element.querySelectorAll('[data-divido-choose-amount]');

        if (amountChoosers.length == 0) {
            return false;
        }

        var amountMin = this.getAmountMin();
        var amountMax = this.getAmountMax();
        var amount = this.getAmount();
        if (amount < amountMin) {
            amount = amountMin;
            this.setAmount(amount);
        }

        var chooseAmount = document.createElement('input');
        chooseAmount.setAttribute('type', 'range');
        chooseAmount.setAttribute('value', amount);
        chooseAmount.setAttribute('min', amountMin);
        chooseAmount.setAttribute('max', amountMax);

        var self = this;

        var slideHandler = function(event) {
            window.requestAnimationFrame(function() {
                self.setAmount(event.target.value);
                self.recalculate(element, infoElements);
                self.updateFinancedAmount(element);
            });
        };

        this.updateFinancedAmount(element);

        var amountChangeHandler = function(event) {
            self.setAmount(event.target.value);
            self.recalculate(element, infoElements);
        };

        chooseAmount.addEventListener('mousedown', function(event) {
            slideHandler(event);
            chooseAmount.addEventListener('mousemove', slideHandler);
        });
        chooseAmount.addEventListener('mouseup', function() {
            chooseAmount.removeEventListener('mousemove', slideHandler);
        });
        chooseAmount.addEventListener('change', amountChangeHandler);

        Array.prototype.forEach.call(amountChoosers, function(amountChooser, i) {
            var form, style, label, labelText, labelStyle;

            if (form = amountChooser.getAttribute('data-divido-form')) {
                chooseAmount.setAttribute('name', form);
            }
            if (style = amountChooser.getAttribute('data-divido-style')) {
                chooseAmount.setAttribute('style', style);
            }

            if (labelText = amountChooser.getAttribute('data-divido-label')) {
                label = document.createElement('label');
                label.innerHTML = labelText;

                if (labelStyle = amountChooser.getAttribute('data-divido-label-style')) {
                    label.setAttribute('style', labelStyle);
                }

                amountChooser.innerHTML = '';
                amountChooser.appendChild(label);
                amountChooser.appendChild(chooseAmount);
            } else {
                amountChooser.innerHTML = '';
                amountChooser.appendChild(chooseAmount);
            }
        });
    }
    ;

    Divido.prototype.oldIE = function() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            var version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);

            if (version < 10) {
                return true;
            }
        }

        return false;
    }
    ;

    Divido.prototype.renderCalculator = function(elmt) {
        var infoElements = elmt.getElementsByClassName('divido-info');

        this.recalculate(elmt, infoElements);
        this.renderChooseFinance(elmt, infoElements);
        this.renderChooseDeposit(elmt, infoElements);
        this.renderChooseAmount(elmt, infoElements);
    }
    ;

    Divido.prototype.initWidgetStyles = function(override) {
        var styleTagId = 'divido-widget-styles';

        var styleTag = document.getElementById(styleTagId);
        if (styleTag != null) {
            return false;
        }

        var styles = {
            'container *': {
                'background-color': '#3e4f8b',
                'color': '#ffffff',
                'float': 'none',
                'font-family': 'verdana',
                'font-size': '11px',
            },

            'container': {
                'z-index': '99999999',
                'padding': '0',
                'max-width': '280px',
                'position': 'absolute',
            },

            'up': {
                'bottom': 0,
            },

            'wrapper': {
                'padding': '20px',
                'overflow': 'hidden',
                '-webkit-box-shadow': 'rgba(0, 0, 0, 0.2) 0 1px 2px 0, rgba(0, 0, 0, 0.05) 0 0 0 5px',
                '-moz-box-shadow': 'rgba(0, 0, 0, 0.2) 0 1px 2px 0, rgba(0, 0, 0, 0.05) 0 0 0 5px',
                '-ms-box-shadow': 'rgba(0, 0, 0, 0.2) 0 1px 2px 0, rgba(0, 0, 0, 0.05) 0 0 0 5px',
                '-o-box-shadow': 'rgba(0, 0, 0, 0.2) 0 1px 2px 0, rgba(0, 0, 0, 0.05) 0 0 0 5px',
                'box-shadow': 'rgba(0, 0, 0, 0.2) 0 1px 2px 0, rgba(0, 0, 0, 0.05) 0 0 0 5px',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                '-ms-border-radius': '5px',
                '-o-border-radius': '5px',
                'border-radius': '5px',
                'border': '1px solid #555',
                'border-color': '#d9d7ce #c8c6c1 #B0AEA6',
                'background-image': 'url(//cdn.divido.com/images/logo-white-40x12.png)',
                'background-repeat': 'no-repeat',
                'background-position': 'bottom right',
                'background-origin': 'content-box',
            },

            'close': {
                'position': 'absolute',
                'top': '5px',
                'right': '7px',
                'cursor': 'pointer',
            },

            'heading': {
                'font-size': '15px',
                'margin-bottom': '10px',
                'font-weight': 'bold'
            },

            'control:nth-of-type(2)': {
                'margin-top': '10px',
            },

            'control select': {
                'display': 'block',
                'border': '1px solid #fff',
                'font-size': '12px',
                'width': '100%',
                'text-transform': 'none',
                'margin-top': '3px',
            },

            'control input[type="range"]': {
                'display': 'inline-block',
                'width': '185px',
                'padding': '0px',
            },

            'control label': {
                'font-weight': 'bold',
                'margin-top': '15px'
            },

            'control .percentage': {
                'vertical-align': 'top',
                'line-height': '15px',
                'margin-left': '4px',
                'font-size': '10px',
                'font-weight': 'normal',
            },

            'summary': {
                'margin': '10px 0px',
                'padding': '0px',
            },

            'info': {
                'width': '100%',
                'overflow': 'hidden',
                'padding': '0',
                'margin': '0',
            },

            'info dt.divido-widget-term': {
                'float': 'left',
                'text-align': 'left',
                'width': '60%',
                'padding': '0',
                'margin': '0',
                'font-weight': 'bold',
                'line-height': '12px',
                'padding-top': '2px',
            },

            'info dd.divido-widget-data': {
                'float': 'left',
                'text-align': 'left',
                'width': '40%',
                'padding': '0',
                'margin': '0',
                'line-height': '12px',
                'padding-top': '2px',
            },

            'apply-link': {
                'float': 'left',
                'margin-top': '20px',
                'text-decoration': 'none',
                'font-size': '16px',
                'padding': '5px 12px',
                'text-transform': 'uppercase',
                'font-weight': '900',
                'border-radius': '3px',
                'vertical-align': 'text-bottom',
                'border': '2px solid #fff',
                'font-family': "'HelveticaNeue', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            },

            'footnote': {
                'float': 'left',
                'margin': '20px 0 20px 0',
            },

            '.divido-widget-launcher': {
                'margin-right': '10px',
            },

            '.divido-widget-logo-link img.divido-widget-logo': {
                'width': '40px',
                'height': '12px',
                'vertical-align': 'middle',
                'display': 'inline',
            },

            '.divido-widget-logo-link': {
                '-webkit-border-radius': '4',
                '-moz-border-radius': '4',
                'border-radius': '4px',
                'font-family': 'Arial',
                'font-size': '11px',
                'background': '#3E4F8B',
                'padding': '3px 7px 5px 7px !important',
                'text-decoration': 'none',
                'border': '1px solid #296AB3',
                'cursor': 'pointer',
            }
        };

        if (override != null) {
            try {
                override = JSON.parse(override);
            } catch (e) {
                override = {};
            }

            for (var selector in override) {
                if (!styles.hasOwnProperty(selector)) {
                    continue;
                }

                for (var prop in override[selector]) {
                    if (!styles[selector].hasOwnProperty(prop)) {
                        continue;
                    }

                    styles[selector][prop] = override[selector][prop];
                }
            }
        }

        var contents = [];
        for (var selector in styles) {
            var block = '';
            for (var prop in styles[selector]) {
                block += prop + ':' + styles[selector][prop] + ';';
            }

            var isRelative = selector.substring(0, 1) != '.';

            if (isRelative) {
                selector = '.divido-widget-' + selector;

                if (selector.substring(0, 24) != '.divido-widget-container') {
                    selector = '.divido-widget-container ' + selector;
                }
            }

            var rule = selector + " {" + block + "}";
            contents.push(rule);
        }

        var sheet = document.createElement('style');
        sheet.setAttribute('id', styleTagId);
        sheet.innerHTML = contents.join("\n");
        document.head.appendChild(sheet);
    }

    Divido.prototype.initPrevStyles = function(override) {
        var styleTagId = 'divido-preview-styles';

        var styleTag = document.getElementById(styleTagId);
        if (styleTag != null) {
            return false;
        }

        var styles = {
            'container *': {
                'background-color': '#3e4f8b',
                'color': '#ffffff',
                'float': 'none',
                'font-family': 'verdana',
                'font-size': '12px',
                'line-height': '18px',
            },
            'container': {
                'display': 'inline-block',
            },
            'content': {
                'overflow': 'auto',
                'padding': '20px',
                'position': 'relative',
            },
            'logo': {
                'position': 'absolute',
                'top': '10px',
                'right': '10px',
            },
            'header': {
                'display': 'block',
                'font-size': '24px',
                'margin-bottom': '20px',
            },
            'control': {
                'margin-bottom': '10px',
            },
            'control label': {
                'font-weight': 'bold',
            },
            'control input': {
                'width': '250px',
                'margin-left': '10px',
            },
            'control .percentage': {
                'margin-left': '5px',
            },
            'info': {
                'font-weight': 'bold',
                'font-size': '14px',
            },
            'info span': {
                'font-weight': 'normal',
            },
            'plan': {
                'width': '230px',
                'float': 'left',
                'margin-right': '20px',
            },
            'plan dl': {
                'overflow': 'auto',
            },
            'plan dt': {
                'float': 'left',
                'width': '150px',
                'font-weight': 'bold',
            },
            'plan dd': {
                'float': 'left',
                'width': '80px',
                'margin-left': '0',
            },
            'plan dd:first-of-type': {
                'width': '100%',
                'margin-bottom': '5px',
            },
            'plan dd .divido-prev-plan-name': {
                'font-weight': 'bold',
                'font-size': '14px',
            }
        };

        if (override != null) {
            try {
                override = JSON.parse(override);
            } catch (e) {
                override = {};
            }

            for (var selector in override) {
                if (!styles.hasOwnProperty(selector)) {
                    continue;
                }

                for (var prop in override[selector]) {
                    if (!styles[selector].hasOwnProperty(prop)) {
                        continue;
                    }

                    styles[selector][prop] = override[selector][prop];
                }
            }
        }

        var contents = [];
        for (var selector in styles) {
            var block = '';
            for (var prop in styles[selector]) {
                block += prop + ':' + styles[selector][prop] + ';';
            }

            var isRelative = selector.substring(0, 1) != '.';

            if (isRelative) {
                selector = '.divido-prev-' + selector;

                if (selector.substring(0, 22) != '.divido-prev-container') {
                    selector = '.divido-prev-container ' + selector;
                }
            }

            var rule = selector + " {" + block + "}";
            contents.push(rule);
        }

        var sheet = document.createElement('style');
        sheet.setAttribute('id', styleTagId);
        sheet.innerHTML = contents.join("\n");
        document.head.appendChild(sheet);
    }
    // Legacy public functions
    window.divido_widget = function(element, options) {
        new Divido(element,options);
    }

    window.divido_calculator = function(element, options) {
        new Divido(element,options);
    }

    Divido.prototype.attach();

    return Divido;

}
)();
// vim: set noexpandtab:
